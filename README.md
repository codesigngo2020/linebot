# LineBot 管理平台

## Getting Started

```
// Start Docker

$ cd linebot
$ docker compose up -d

// Migration & Seeding
$ docker-compose exec webserver php artisan migrate --seed
```

## 部署方案

**1. 前往方案頁**

選擇「LINE 聊天機器人」。

![](assets/page_1.png)

**2. 啟用／部署方案**

點擊「立即啟用」。

![](assets/page_2.png)

**3. 填寫 LineBot 資料**

![](assets/page_3.png)

## 頁面導覽

### 機器人儀表板

![](assets/page_4.png)

![](assets/page_5.png)

![](assets/page_6.png)

### 歡迎訊息

**歡迎訊息排班表**

![](assets/page_11.png)

**建立歡迎訊息**

![](assets/page_7.png)

![](assets/page_8.png)

**檢視歡迎訊息**

![](assets/page_9.png)

![](assets/page_10.png)

### 推播訊息

**推播訊息排班表**

![](assets/page_13.png)

**建立推播訊息**

![](assets/page_12.png)

### 訊息庫

**建立訊息**

![](assets/page_14.png)

![](assets/page_15.png)

![](assets/page_16.png)

![](assets/page_17.png)

### 訊息腳本

**建立訊息腳本**

![](assets/page_18.png)

![](assets/page_19.png)

![](assets/page_20.png)

### 聊天室

![](assets/page_21.png)

### 主選單

![](assets/page_22.png)
