<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Category;
use App\Models\Platform\Image;
use App\Models\Platform\User;

class MyProjectTablesSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */

    public function __construct()
    {
        $this->solutions = [];
        $this->categories = [];
    }

    public function run()
    {
        /* ========== 建立 solution ========== */

        $this->solutions[4] = Solution::create([
            'id' => 4,
            'name' => 'LINE 聊天機器人',
            'description' => '建立LINE聊天機器人。'
        ]);

        $this->solutions[5] = Solution::create([
            'id' => 5,
            'name' => 'EIP 管理系統',
            'description' => '建立公司內部EIP管理系統。'
        ]);

        /* ========== 建立 plans ========== */

        $this->solutions[4]->plans()->create([
            'name' => '免費方案',
            'price' => 0,
            'data' => [
                'broadcasts_count' => 'Unlimited',
            ],
        ]);

        $this->solutions[5]->plans()->create([
            'name' => '免費方案',
            'price' => 0,
            'data' => [],
        ]);

        /* ========== 建立 累別 ========== */

        $this->categories['icon'] = Category::create(['id' => 1, 'name' => 'icon']);

        /* ========== 建立 圖片 ========== */

        $this->solutions[4]->icon()->create([
            'name' => 'line',
            'path' => 'assets/image/svg/light/line.svg',
            'disk' => 'asset',
        ])
        ->categories()
        ->syncWithoutDetaching($this->categories['icon']->id);

        $this->solutions[5]->icon()->create([
            'name' => 'sitemap',
            'path' => 'assets/image/svg/regular/sitemap.svg',
            'disk' => 'asset',
        ])
        ->categories()
        ->syncWithoutDetaching($this->categories['icon']->id);

        /* ========== 建立 角色 & 權限 ========== */

        //Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        //create roles
        Role::create(['name' => 'developer', 'display_name' => 'developer']);


        /* ========== 建立 user ========== */

        $developer = User::create([
            'name' => 'Superadmin',
            'email' => 'scott0975369663@gmail.com',
            'password' => bcrypt('asdasdasd'),
        ])
        ->assignRole('developer');
    }
}
