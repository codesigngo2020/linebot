<?php

use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// Model
use App\Models\Platform\User;
use App\Models\Platform\Solution\Deployment;

// Helper
use App\Helpers\Basic\Migration\SetSolutionDBHelper;

class UpdateSolution4Tables20191030 extends Migration
{

    public function up()
    {
        /* ========== 角色資料表 ========== */

        if(!Schema::hasColumn('roles', 'deployment_id')){
            Schema::table('roles', function (Blueprint $table) {
                $table->unsignedInteger('deployment_id')->index();
            });
        }

        if(!Schema::hasColumn('roles', 'display_name')){
            Schema::table('roles', function (Blueprint $table) {
                $table->string('display_name');
            });
        }

        try{
            Schema::table('roles', function (Blueprint $table) {
                $table->index(['name', 'guard_name']);
            });
        }catch(\Exception $ex){

        }

        /* ========== 部署DB更新 ========== */

        $this->setSolutionDBHelper = new SetSolutionDBHelper;
        $this->roles = ['superadmin', 'social_media_manager', 'customer_service_agent'];

        Deployment::where('solution_id', 4)->get()
        ->each(function($deployment){

            /* ========== migration ========== */

            $this->setSolutionDBHelper->setSolutionDB($deployment);

            // statistics
            if(!Schema::connection('deployment_'.$deployment->id)->hasTable('statistics')){
                Schema::connection('deployment_'.$deployment->id)->create('statistics', function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->unsignedInteger('followers_count');
                    $table->unsignedInteger('blocks_count');
                    $table->unsignedInteger('active_users_count');
                    $table->unsignedInteger('broadcasts_count');
                    $table->unsignedInteger('pushes_count');
                    $table->unsignedInteger('multicasts_count');
                    $table->unsignedInteger('replies_count');
                    $table->timestamp('timestamp')->unique()->useCurrent();
                    $table->timestamps();
                });
            }

            /* ========== 角色權限 ========== */

            $roles = array_map(function($role) use($deployment){
                return 'deployment_'.$deployment->id.'_'.$role;
            }, $this->roles);

            $exist_roles = DB::table('roles')
            ->select('id', 'name')
            ->where('guard_name', 'web')
            ->whereIn('name', $roles)
            ->limit(count($roles))
            ->pluck('id', 'name');

            // create roles
            if(count($this->roles) != count($exist_roles)){
                foreach($roles as $roleIndex => $role){
                    if(!isset($exist_roles[$role])){
                        $role = Role::create(['name' => $role, 'display_name' => $this->roles[$roleIndex]], $deployment->id);
                        if($role->display_name == 'superadmin') $exist_roles['deployment_'.$deployment->id.'_superadmin'] = $role->id;
                    }
                }
            }

            // 檢查至少一人有supermin權限
            $supermin = DB::table('model_has_roles')
            ->select('model_id')
            ->where('role_id', $exist_roles['deployment_'.$deployment->id.'_superadmin'])
            ->first();

            if(!$supermin){
                $userId = DB::table('deployment_user')
                ->select('user_id')
                ->where('deployment_id', $deployment->id)
                ->orderBy('created_at')
                ->first();

                if($userId){
                    User::find($userId->user_id)->assignRole($exist_roles['deployment_'.$deployment->id.'_superadmin']);
                }
            }

        });


    }

    public function down()
    {
        // Schema::dropIfExists('users');
    }
}
