<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// Model
use App\Models\Platform\Solution\Deployment;

// Helper
use App\Helpers\Basic\Migration\SetSolutionDBHelper;

class UpdateSolution4Tables20191113 extends Migration
{

    public function up()
    {

        /* ========== 部署DB更新 ========== */

        $this->setSolutionDBHelper = new SetSolutionDBHelper;

        Deployment::where('solution_id', 4)->get()
        ->each(function($deployment){

            $this->setSolutionDBHelper->setSolutionDB($deployment);

            /* ========== categories ========== */

            if(!Schema::connection('deployment_'.$deployment->id)->hasTable('categories')){
                Schema::connection('deployment_'.$deployment->id)->create('categories', function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->string('name')->index();
                    $table->string('categoryable_type')->index();
                    $table->unsignedInteger('parent_id')->index();
                    $table->unsignedInteger('start')->index();
                    $table->unsignedInteger('end')->index();
                    $table->unsignedInteger('order')->index();
                    $table->timestamps();
                });
            };

            if(!Schema::connection('deployment_'.$deployment->id)->hasTable('categoryables')){
                Schema::connection('deployment_'.$deployment->id)->create('categoryables', function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->unsignedInteger('category_id')->index();
                    $table->string('categoryable_type')->index();
                    $table->unsignedInteger('categoryable_id')->index();
                    $table->timestamp('created_at')->useCurrent();
                });
            };



            foreach(['keywords', 'scripts', 'messages']as $tableName){

                // 新增 版本 & 刪除系列選單 欄位
                if(!Schema::connection('deployment_'.$deployment->id)->hasColumn($tableName, 'version')){
                    Schema::connection('deployment_'.$deployment->id)->table($tableName, function (Blueprint $table) {
                        $table->unsignedInteger('parent_id')->index();
                        $table->unsignedInteger('version')->default(1)->index();
                        $table->boolean('is_current_version')->default(1)->index();

                        $table->timestamp('deleted_all_at')->nullable();
                    });

                    DB::connection('deployment_'.$deployment->id)
                    ->table($tableName)
                    ->update([
                        'parent_id' => DB::raw('`id`'),
                    ]);
                };

                // 新增 啟用/停用 欄位
                if($tableName != 'messages' && !Schema::connection('deployment_'.$deployment->id)->hasColumn($tableName, 'active')){
                    Schema::connection('deployment_'.$deployment->id)->table($tableName, function (Blueprint $table) {
                        $table->boolean('active')->default(1)->index();
                    });
                }
            }

            /* ========== form_data 驗證欄位修正 ========== */

            foreach(['auto_answers', 'broadcasts', 'keywords', 'messages', 'welcome_messages'] as $table){
                DB::connection('solution_4')
                ->table($table)
                ->whereNotNull('messages_form_data')
                ->select('id', 'messages_form_data')
                ->get()
                ->each(function($row) use($table){
                    DB::connection('solution_4')
                    ->table($table)
                    ->where('id', $row->id)
                    ->update([
                        'messages_form_data' => preg_replace(['/"invalid"\s*:\s*"true"/', '/"invalid"\s*:\s*"false"/', '/"active"\s*:\s*"true"/', '/"active"\s*:\s*"false"/', '/"active"\s*:\s*"true"/', '/"active"\s*:\s*"false"/'], '"invalid":false', $row->messages_form_data),
                    ]);
                });
            }

            foreach(['richmenus' => 'areas_form_data', 'scripts' => 'nodes_form_data'] as $table => $column){
                DB::connection('solution_4')
                ->table($table)
                ->whereNotNull($column)
                ->select('id', $column)
                ->get()
                ->each(function($row) use($table, $column){
                    DB::connection('solution_4')
                    ->table($table)
                    ->where('id', $row->id)
                    ->update([
                        $column => preg_replace('/"(invalid|active|id|start|end|show|disabled)"\s*:\s*"(true|false|\d*)"/', '"$1":$2', $row->{$column}),
                    ]);
                });
            }

        });

    }

    public function down()
    {
        // Schema::dropIfExists('users');
    }
}
