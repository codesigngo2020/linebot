<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyprojectTables extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
        });

        Schema::create('categoryables', function (Blueprint $table) {
            $table->unsignedInteger('category_id')->index();
            $table->string('categoryable_type')->index();
            $table->unsignedInteger('categoryable_id')->index();
        });

        Schema::create('deployment_user', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('deployment_id')->index();
            $table->timestamps();
        });

        Schema::create('deployments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('owner')->index();
            $table->unsignedInteger('solution_id')->index();
            $table->string('name')->index();
            $table->string('description')->index();
            $table->json('data');
            $table->unsignedInteger('plan_id')->index();
            $table->enum('status', ['active','suspend','destroyed']);
            $table->timestamps();
            $table->timestamp('destroyed_at')->nullable();
        });

        Schema::create('imageables', function (Blueprint $table) {
            $table->unsignedInteger('image_id')->index();
            $table->string('imageable_type')->index();
            $table->unsignedInteger('imageable_id')->index();
        });

        Schema::create('images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('path');
            $table->enum('disk', ['asset','local','public','s3','gcs']);
            $table->timestamps();
        });

        Schema::create('plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('solution_id')->index();
            $table->string('name');
            $table->decimal('price', 8, 2);
            $table->json('data');
        });

        Schema::create('solutions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('description');
        });

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        $tables = ['categories','categoryables','deployment_user','deployments','imageables','images','plans','solutions','users'];

        foreach($tables as $table){
            Schema::dropIfExists($table);
        }
    }
}
