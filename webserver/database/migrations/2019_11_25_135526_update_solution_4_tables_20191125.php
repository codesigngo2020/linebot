<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// Model
use App\Models\Platform\Solution\Deployment;

// Helper
use App\Helpers\Basic\Migration\SetSolutionDBHelper;

class UpdateSolution4Tables20191125 extends Migration
{

    public function up()
    {

        /* ========== 部署DB更新 ========== */

        $this->setSolutionDBHelper = new SetSolutionDBHelper;

        Deployment::where('solution_id', 4)->get()
        ->each(function($deployment){

            $this->setSolutionDBHelper->setSolutionDB($deployment);

            /* ========== nodes -> script_nodes ========== */
            if(Schema::connection('deployment_'.$deployment->id)->hasTable('nodes')){
                Schema::connection('deployment_'.$deployment->id)->rename('nodes', 'script_nodes');
            }

            /* ========== tags.taggable_type App\Models\Platform\Solution\_4\Node -> App\Models\Platform\Solution\_4\ScriptNode ========== */

            DB::connection('solution_4')
            ->table('tags')
            ->where('taggable_type', 'App\Models\Platform\Solution\_4\Node')
            ->update([
                'taggable_type' => 'App\Models\Platform\Solution\_4\ScriptNode',
            ]);

            /* ========== 更新訊息庫欄位 ========== */

            if(!Schema::connection('deployment_'.$deployment->id)->hasColumn('messages', 'type')){
                Schema::connection('deployment_'.$deployment->id)->table('messages', function (Blueprint $table){
                    $table->enum('type', ['message', 'template'])->index();
                });
            }

            if(!Schema::connection('deployment_'.$deployment->id)->hasColumn('messages', 'template_type')){
                Schema::connection('deployment_'.$deployment->id)->table('messages', function (Blueprint $table){
                    $table->enum('template_type', ['coupon'])->nullable()->index();
                });
            }

            if(Schema::connection('deployment_'.$deployment->id)->hasColumn('messages', 'messages')){
                DB::connection('deployment_'.$deployment->id)
                ->statement("ALTER TABLE `messages` CHANGE `messages` `messages` JSON default NULL;");
            }

            if(Schema::connection('deployment_'.$deployment->id)->hasColumn('messages', 'messages_form_data')){
                DB::connection('deployment_'.$deployment->id)
                ->statement("ALTER TABLE `messages` CHANGE `messages_form_data` `messages_form_data` JSON default NULL;");
            }

            if(!Schema::connection('deployment_'.$deployment->id)->hasColumn('messages', 'nodes_form_data')){
                Schema::connection('deployment_'.$deployment->id)->table('messages', function (Blueprint $table){
                    $table->json('nodes_form_data')->nullable();
                });
            }

            if(!Schema::connection('deployment_'.$deployment->id)->hasColumn('messages', 'settings')){
                Schema::connection('deployment_'.$deployment->id)->table('messages', function (Blueprint $table){
                    $table->json('settings')->nullable();
                });
            }

            /* ========== 新增訊息庫條件節點 ========== */

            if(!Schema::connection('deployment_'.$deployment->id)->hasTable('message_nodes')){
                Schema::connection('deployment_'.$deployment->id)->create('message_nodes', function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->unsignedInteger('message_id')->index();
                    $table->unsignedInteger('start')->index();
                    $table->unsignedInteger('end')->index();
                    $table->unsignedInteger('parent_id')->nullable()->index();
                    $table->json('messages');
                    $table->timestamps();
                });
            }

            if(!Schema::connection('deployment_'.$deployment->id)->hasColumn('message_nodes', 'condition')){
                Schema::connection('deployment_'.$deployment->id)->table('message_nodes', function (Blueprint $table) {
                    $table->string('condition')->index();
                });
            }

            /* ========== 新增訊息模板欄位 ========== */

            foreach(['auto_answers', 'broadcasts', 'keywords', 'welcome_messages'] as $tableName){
                if(!Schema::connection('deployment_'.$deployment->id)->hasColumn($tableName, 'template_id')){
                    Schema::connection('deployment_'.$deployment->id)->table($tableName, function (Blueprint $table) {
                        $table->unsignedInteger('template_id')->nullable()->index();
                    });
                };

                DB::connection('deployment_'.$deployment->id)
                ->statement("ALTER TABLE `$tableName` CHANGE `messages` `messages` JSON default NULL;");
            }

            /* ========== script_nodes 新增 next_message ========== */

            if(!Schema::connection('deployment_'.$deployment->id)->hasColumn('script_nodes', 'next_message')){
                Schema::connection('deployment_'.$deployment->id)->table('script_nodes', function (Blueprint $table) {
                    $table->string('next_message')->nullable()->index();
                });
            }

            /* ========== script_nodes 新增 parent_id ========== */

            if(!Schema::connection('deployment_'.$deployment->id)->hasColumn('script_nodes', 'parent_id')){
                Schema::connection('deployment_'.$deployment->id)->table('script_nodes', function (Blueprint $table) {
                    $table->unsignedInteger('parent_id')->nullable()->index()->after('end');
                });
            }

            if(!Schema::connection('deployment_'.$deployment->id)->hasTable('tables')){
                Schema::connection('deployment_'.$deployment->id)->create('tables', function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->enum('type', ['normal', 'coupon'])->index();
                    $table->string('name')->index();
                    $table->string('description')->index();
                    $table->string('table_name')->index();
                    $table->json('editable_columns');
                    $table->json('settings')->nullable();
                    $table->timestamps();
                    $table->timestamp('deleted_at')->nullable();
                });
            }

        });

    }

    public function down()
    {
        // Schema::dropIfExists('users');
    }
}
