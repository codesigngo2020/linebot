<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Richmenu;

// Helper
use App\Helpers\Basic\Migration\SetSolutionDBHelper;

class UpdateSolution4Tables20191031 extends Migration
{

    public function up()
    {
        /* ========== invitation_codes ========== */

        if(!Schema::hasTable('invitation_codes')){
            Schema::create('invitation_codes', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedInteger('user_id')->index();
                $table->unsignedInteger('deployment_id')->index();
                $table->json('roles');
                $table->string('code');
                $table->timestamps();
            });
        }

        /* ========== 部署DB更新 ========== */

        $this->setSolutionDBHelper = new SetSolutionDBHelper;

        Deployment::where('solution_id', 4)->get()
        ->each(function($deployment){

            $this->setSolutionDBHelper->setSolutionDB($deployment);

            /* ========== richmenus ========== */

            // 新增版本欄位

            if(!Schema::connection('deployment_'.$deployment->id)->hasColumn('richmenus', 'version')){
                Schema::connection('deployment_'.$deployment->id)->table('richmenus', function (Blueprint $table) {
                    $table->unsignedInteger('parent_id')->index();
                    $table->unsignedInteger('version')->default(1)->index();
                    $table->boolean('is_current_version')->default(1)->index();
                });
            };

            // 新增 刪除系列選單 欄位
            if(!Schema::connection('deployment_'.$deployment->id)->hasColumn('richmenus', 'deleted_all_at')){
                Schema::connection('deployment_'.$deployment->id)->table('richmenus', function (Blueprint $table) {
                    $table->timestamp('deleted_all_at')->nullable();
                });
            };

            // 檢查name有沒有重複以及是否已經刪除
            $names = [];
            Richmenu::select('id', 'name', 'deleted_at')->where('parent_id', 0)->get()->each(function($richmenu) use(&$names){
                if(in_array($richmenu->name, $names)){
                    $i = 1;
                    while(true){
                        $name = $richmenu->name.'-'.$i;
                        if(!in_array($name, $names)){
                            $richmenu->name = $name;
                            array_push($names, $name);
                            break;
                        }
                        $i ++;
                    }
                }else{
                    array_push($names, $richmenu->name);
                }

                if($richmenu->deleted_at){
                    $richmenu->is_current_version = 0;
                }
                $richmenu->parent_id = $richmenu->id;
                $richmenu->save();
            });


        });

    }

    public function down()
    {
        // Schema::dropIfExists('users');
    }
}
