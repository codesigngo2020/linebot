<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// Model
use App\Models\Platform\Solution\Deployment;

// Helper
use App\Helpers\Basic\Migration\SetSolutionDBHelper;

class UpdateSolution4Tables20191027 extends Migration
{

    public function up()
    {
        $this->setSolutionDBHelper = new SetSolutionDBHelper;

        Deployment::where('solution_id', 4)->get()
        ->each(function($deployment){

            $this->setSolutionDBHelper->setSolutionDB($deployment);

            // auto_answers
            if(!Schema::connection('deployment_'.$deployment->id)->hasColumn('auto_answers', 'types')){
                Schema::connection('deployment_'.$deployment->id)->table('auto_answers', function (Blueprint $table) {
                    $table->unsignedSmallInteger('types')->index();
                });
            }

            // auto_answers
            Schema::connection('deployment_'.$deployment->id)->table('periods', function (Blueprint $table) {
                if(!Schema::connection('deployment_'.$deployment->id)->hasColumn('periods', 'periodable_id')){
                    $table->unsignedInteger('periodable_id')->index();
                }
                if(!Schema::connection('deployment_'.$deployment->id)->hasColumn('periods', 'periodable_type')){
                    $table->string('periodable_type')->index();
                }
                if(Schema::connection('deployment_'.$deployment->id)->hasColumn('periods', 'auto_answer_id')){
                    $table->dropColumn('auto_answer_id');
                }
            });

            // keywords
            if(!Schema::connection('deployment_'.$deployment->id)->hasColumn('keywords', 'QRcode_url')){
                Schema::connection('deployment_'.$deployment->id)->create('keywords', function (Blueprint $table) {
                    $table->string('QRcode_url');
                });
            }

            // videoables
            if(!Schema::connection('deployment_'.$deployment->id)->hasTable('videoables')){
                Schema::connection('deployment_'.$deployment->id)->create('videoables', function (Blueprint $table) {
                    $table->unsignedInteger('video_id')->index();
                    $table->string('videoable_type')->index();
                    $table->unsignedInteger('videoable_id')->index();
                });
            }

            // videos
            if(!Schema::connection('deployment_'.$deployment->id)->hasTable('videos')){
                Schema::connection('deployment_'.$deployment->id)->create('videos', function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->string('name');
                    $table->string('path');
                    $table->enum('disk', ['asset','local','public','s3','gcs']);
                    $table->timestamps();
                });
            }

            // welcome_messages
            if(!Schema::connection('deployment_'.$deployment->id)->hasTable('welcome_messages')){
                Schema::connection('deployment_'.$deployment->id)->create('welcome_messages', function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->string('name')->index();
                    $table->string('description')->index();
                    $table->json('messages');
                    $table->json('messages_form_data')->nullable();
                    $table->boolean('active');
                    $table->timestamps();
                });
            }

        });

    }

    public function down()
    {
        // Schema::dropIfExists('users');
    }
}
