<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Solution 4
|
*/

Route::group(['as'=>'solution.4.', 'prefix'=>'solution/4', 'namespace'=>'Web\Solution\_4', 'middleware'=>['auth', 'setSolution:4']], function(){
    Route::post('/', 'SolutionController@store')->name('store');

    Route::group(['as'=>'deployment.', 'prefix'=>'deployment/{deployment}', 'middleware'=>['checkDeploymentInSolution', 'setSolutionDB', 'hasDeployment']], function(){
        Route::get('/', 'DeploymentController@show')->name('show');
        Route::get('getChartData', 'DeploymentController@getChartData')->name('getChartData');

        Route::group(['as'=>'iamAdmin.', 'prefix'=>'iamAdmin', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'IamAdminController@index')->name('index');
            Route::get('getUsers', 'IamAdminController@getUsers')->name('getUsers');

            Route::post('assign', 'IamAdminController@assign')->name('assign');
            Route::post('invite', 'IamAdminController@invite')->name('invite');
            Route::delete('delete', 'IamAdminController@delete')->name('delete');
        });

        Route::group(['as'=>'richmenus.', 'prefix'=>'richmenus', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'RichmenuController@index')->name('index');
            Route::get('getRichmenus', 'RichmenuController@getRichmenus')->name('getRichmenus');
            Route::get('create', 'RichmenuController@create')->name('create');
            Route::post('/', 'RichmenuController@store')->name('store');
            Route::get('queryForSelect', 'RichmenuController@queryForSelect')->name('queryForSelect');

            Route::group(['prefix'=>'{richmenuId}'], function(){
                Route::get('/', 'RichmenuController@show')->name('show');
                Route::delete('/', 'RichmenuController@delete')->name('delete');
                Route::delete('deleteAll', 'RichmenuController@deleteAll')->name('deleteAll');
                Route::get('edit', 'RichmenuController@edit')->name('edit');
                Route::post('newVersion', 'RichmenuController@newVersion')->name('newVersion');
                Route::post('toggelDefault', 'RichmenuController@toggelDefault')->name('toggelDefault');
                Route::post('cancelBinding', 'RichmenuController@cancelBinding')->name('cancelBinding');
                Route::post('switchVersion', 'RichmenuController@switchVersion')->name('switchVersion');
                Route::get('getChartData', 'RichmenuController@getChartData')->name('getChartData');
            });
        });

        Route::group(['as'=>'welcomeMessages.', 'prefix'=>'welcomeMessages', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'WelcomeMessageController@index')->name('index');
            Route::get('getWelcomeMessageCalendar', 'WelcomeMessageController@getWelcomeMessageCalendar')->name('getWelcomeMessageCalendar');
            Route::get('getWelcomeMessageList', 'WelcomeMessageController@getWelcomeMessageList')->name('getWelcomeMessageList');
            Route::get('create', 'WelcomeMessageController@create')->name('create');
            Route::post('/', 'WelcomeMessageController@store')->name('store');

            Route::group(['prefix'=>'{welcomeMessageId}'], function(){
                Route::get('/', 'WelcomeMessageController@show')->name('show');
                Route::post('toggleActive', 'WelcomeMessageController@toggleActive')->name('toggleActive');
            });
        });

        Route::group(['as'=>'autoAnswers.', 'prefix'=>'autoAnswers', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'AutoAnswerController@index')->name('index');
            Route::get('getAutoAnswerCalendar', 'AutoAnswerController@getAutoAnswerCalendar')->name('getAutoAnswerCalendar');
            Route::get('getAutoAnswerList', 'AutoAnswerController@getAutoAnswerList')->name('getAutoAnswerList');
            Route::get('create', 'AutoAnswerController@create')->name('create');
            Route::post('/', 'AutoAnswerController@store')->name('store');

            Route::group(['prefix'=>'{autoAnswerId}'], function(){
                Route::get('/', 'AutoAnswerController@show')->name('show');
                Route::post('toggleActive', 'AutoAnswerController@toggleActive')->name('toggleActive');
            });
        });

        Route::group(['as'=>'broadcasts.', 'prefix'=>'broadcasts', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'BroadcastController@index')->name('index');
            Route::get('getBroadcastCalendar', 'BroadcastController@getBroadcastCalendar')->name('getBroadcastCalendar');
            Route::get('getBroadcastList', 'BroadcastController@getBroadcastList')->name('getBroadcastList');
            Route::get('create', 'BroadcastController@create')->name('create');
            Route::post('/', 'BroadcastController@store')->name('store');

            Route::group(['prefix'=>'{broadcastId}'], function(){
                Route::get('/', 'BroadcastController@show')->name('show');
                Route::delete('/', 'BroadcastController@delete')->name('delete');
            });
        });

        Route::group(['as'=>'messagesPool.', 'prefix'=>'messagesPool', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'MessagesPoolController@index')->name('index');
            Route::get('getMessages', 'MessagesPoolController@getMessages')->name('getMessages');
            Route::get('create', 'MessagesPoolController@create')->name('create');
            Route::post('store/message', 'MessagesPoolController@storeMessage')->name('storeMessage');
            Route::post('store/template', 'MessagesPoolController@storeTemplate')->name('storeTemplate');
            Route::get('queryForSelect', 'MessagesPoolController@queryForSelect')->name('queryForSelect');

            Route::group(['prefix'=>'{messageId}'], function(){
                Route::get('/', 'MessagesPoolController@show')->name('show');
                Route::get('edit', 'MessagesPoolController@edit')->name('edit');
                Route::post('newMessageVersion', 'MessagesPoolController@newMessageVersion')->name('newMessageVersion');
                Route::post('newTemplateVersion', 'MessagesPoolController@newTemplateVersion')->name('newTemplateVersion');
                Route::post('switchVersion', 'MessagesPoolController@switchVersion')->name('switchVersion');
                Route::delete('deleteAll', 'MessagesPoolController@deleteAll')->name('deleteAll');
                Route::get('getMessage', 'MessagesPoolController@getMessage')->name('getMessage');
            });
        });

        Route::group(['as'=>'keywords.', 'prefix'=>'keywords', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'KeywordController@index')->name('index');
            Route::get('getKeywords', 'KeywordController@getKeywords')->name('getKeywords');
            Route::get('getChartData', 'KeywordController@getChartData')->name('getChartData');
            Route::get('create', 'KeywordController@create')->name('create');
            Route::get('getRichmenu', 'KeywordController@getRichmenu')->name('getRichmenu');
            Route::post('/', 'KeywordController@store')->name('store');
            Route::get('queryForSelect', 'KeywordController@queryForSelect')->name('queryForSelect');

            Route::group(['prefix'=>'{keywordId}'], function(){
                Route::get('/', 'KeywordController@show')->name('show');
                Route::get('edit', 'KeywordController@edit')->name('edit');
                Route::post('newVersion', 'KeywordController@newVersion')->name('newVersion');
                Route::post('switchVersion', 'KeywordController@switchVersion')->name('switchVersion');
                Route::post('toggleActive', 'KeywordController@toggleActive')->name('toggleActive');
                Route::delete('deleteAll', 'KeywordController@deleteAll')->name('deleteAll');
            });
        });

        Route::group(['as'=>'scripts.', 'prefix'=>'scripts', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'ScriptController@index')->name('index');
            Route::get('getScripts', 'ScriptController@getScripts')->name('getScripts');
            Route::get('getChartData', 'ScriptController@getChartData')->name('getChartData');
            Route::get('create', 'ScriptController@create')->name('create');
            Route::post('/', 'ScriptController@store')->name('store');
            Route::get('queryForSelect', 'ScriptController@queryForSelect')->name('queryForSelect');

            Route::group(['prefix'=>'{scriptId}'], function(){
                Route::get('/', 'ScriptController@show')->name('show');
                Route::get('edit', 'ScriptController@edit')->name('edit');
                Route::post('newVersion', 'ScriptController@newVersion')->name('newVersion');
                Route::post('switchVersion', 'ScriptController@switchVersion')->name('switchVersion');
                Route::post('toggleActive', 'ScriptController@toggleActive')->name('toggleActive');
                Route::delete('deleteAll', 'ScriptController@deleteAll')->name('deleteAll');
            });
        });

        Route::group(['as'=>'liffs.', 'prefix'=>'liffs', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'LiffController@index')->name('index');
            Route::get('getLiffs', 'LiffController@getLiffs')->name('getLiffs');

            Route::group(['prefix'=>'{liffId}'], function(){
                Route::delete('/', 'LiffController@delete')->name('delete');
            });
        });

        Route::group(['as'=>'tags.', 'prefix'=>'tags', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'TagController@index')->name('index');
            Route::get('getTags', 'TagController@getTags')->name('getTags');

            Route::group(['prefix'=>'{tagId}'], function(){
                Route::get('/', 'TagController@show')->name('show');
                Route::get('getChartData', 'TagController@getChartData')->name('getChartData');
                Route::post('name', 'TagController@name')->name('name');
            });
        });

        Route::group(['as'=>'users.', 'prefix'=>'users', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'UserController@index')->name('index');
            Route::get('getUsers', 'UserController@getUsers')->name('getUsers');
            Route::get('queryForSelect', 'UserController@queryForSelect')->name('queryForSelect');

            Route::group(['prefix'=>'{userId}'], function(){
                Route::get('/', 'UserController@show')->name('show');
                Route::get('getHistory', 'UserController@getHistory')->name('getHistory');
            });
        });

        Route::group(['as'=>'groups.', 'prefix'=>'groups', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'GroupController@index')->name('index');
            Route::get('create', 'GroupController@create')->name('create');
            Route::get('getTags', 'GroupController@getTags')->name('getTags');
            Route::get('getUsers', 'GroupController@getUsers')->name('getUsers');
            Route::post('/', 'GroupController@store')->name('store');
            Route::get('getGroups', 'GroupController@getGroups')->name('getGroups');
            Route::get('queryForSelect', 'GroupController@queryForSelect')->name('queryForSelect');

            Route::group(['prefix'=>'{groupId}'], function(){
                Route::get('/', 'GroupController@show')->name('show');
                Route::post('sync', 'GroupController@sync')->name('sync');
                Route::get('getGroupUsers', 'GroupController@getGroupUsers')->name('getGroupUsers');
            });
        });

        Route::group(['as'=>'categories.', 'prefix'=>'categories', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'CategoryController@index')->name('index');
            Route::post('/', 'CategoryController@store')->name('store');
            Route::post('classify', 'CategoryController@classify')->name('classify');
            Route::get('queryForSelect', 'CategoryController@queryForSelect')->name('queryForSelect');

            Route::group(['prefix'=>'{categoryId}'], function(){
                Route::post('name', 'CategoryController@name')->name('name');
                Route::delete('delete', 'CategoryController@delete')->name('delete');
            });
        });

        Route::group(['as'=>'chatroom.', 'prefix'=>'chatroom', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'ChatroomController@index')->name('index');
            Route::post('imageUpload', 'ChatroomController@imageUpload')->name('imageUpload');
        });

        Route::group(['as'=>'tables.', 'prefix'=>'tables', 'namespace'=>'Deployment'], function(){
            Route::get('/', 'TableController@index')->name('index');
            Route::get('getTables', 'TableController@getTables')->name('getTables');
            Route::get('create', 'TableController@create')->name('create');
            Route::post('/', 'TableController@store')->name('store');
            Route::get('queryForSelect', 'TableController@queryForSelect')->name('queryForSelect');

            Route::group(['prefix'=>'{tableId}'], function(){
                Route::get('/', 'TableController@show')->name('show');
                Route::get('getRows', 'TableController@getRows')->name('getRows');
                Route::delete('delete', 'TableController@delete')->name('delete');
                Route::get('redeem', 'TableController@redeem')->name('redeem');
            });
        });

    });
});
