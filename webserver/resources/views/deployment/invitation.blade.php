@extends('layouts/app')

@section('title', '部署成員邀請')

@push('css')
@endpush

@section('content')
<div class="container-fluid pt-6 pb-6">
    <div class="row justify-content-center align-items-center">
        <div class="col-12 col-md-6 ml-xl-4 offset-md-0 order-md-2 mb-5 mb-md-0">

            <!-- Image -->
            <div class="text-center">
                <img src="{{ asset('assets/image/lost.svg') }}" alt="..." class="img-fluid">
            </div>

        </div>
        <div class="col-12 col-md-5 col-xl-4 order-md-1 my-5">

            <div class="text-center">

                <!-- Preheading -->
                <h6 class="text-uppercase text-muted mb-4">{{ $response['status'] }}</h6>

                <!-- Heading -->
                <h1 class="display-4 mb-3">{{ $response['status'] == 'success' ? 'Successfully joined' : 'Invalid invitation code' }}</h1>

                <!-- Subheading -->
                <p class="text-muted mb-4">{{ $response['message'] }}</p>

                <!-- Button -->
                @if($response['status'] == 'success')
                <a href="{{ route('solution.'.$response['deployment']->solution_id.'.deployment.show', $response['deployment']->id) }}" class="btn btn-lg btn-primary">Go to the deployment</a>
                @else
                <a href="{{ route('solution.index') }}" class="btn btn-lg btn-primary">Go to the solution list</a>
                @endif

            </div>

        </div>
    </div>
</div>
@endsection

@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
}
</script>
@endpush
