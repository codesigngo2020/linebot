@extends('layouts/app')

@section('title', '部署列表')

@push('css')
<link href="{{ asset('assets/css/solution/deployments.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div id="solution-{{ $solution->id }}" class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">


            {{-- Header --}}
            <div class="header mt-md-5">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Manage Deployments</h6>
                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>部署列表</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>
                        </div>
                        <div id="new_deployment" class="col-auto mr-3 mr-sm-0">
                            {{-- Button --}}
                            <a href="{{ route('solution.deploy', $solution) }}" class="btn btn-primary d-none d-sm-block">{{ __('views/solution/deployments.new_deployment') }}</a>
                            <a href="{{ route('solution.deploy', $solution) }}" class="btn btn-transparent nopadding rounded-circle d-block d-sm-none">
                                <span class="">
                                    <v-svg class="svg d-flex text-primary" src="{{ asset('assets/image/svg/light/plus-circle.svg') }}" width="22" height="22"></v-svg>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @if($deployments->isEmpty())
            <div id="undeploy-info">
                <p>{{ __('views/solution/deployments.undeploy_info') }}</p>
            </div>
            @else

            {{-- List --}}
            @include('components.tables.solution._'.$solution->id.'.deployments')

            @endif

        </div>
    </div>
</div>
@endsection

@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployments: {!! $deployments->toJson() !!},
    paginator : {!! $paginator->toJson() !!},
}
</script>
<script src="{{ asset('assets/js/solution/deployments-partials/solution_'.$solution->id.'_deployments.js?v='.time()) }}"></script>
@endpush
