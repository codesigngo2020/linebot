@extends('layouts/app')

@section('title', '方案內容')

@push('css')
<link href="{{ asset('assets/css/solution/detail.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12 col-lg-10 col-xl-8">

      {{-- Header --}}
      <div class="header mt-md-5">
        <div class="header-body">
          <div class="row align-items-center">
            <div class="col">
              {{-- Pretitle --}}
              <h6 class="header-pretitle">Solution Detail</h6>
              {{-- Title --}}
              <h1 class="header-title">{{ __('views/solution/index.solutions.'.$solution->id.'.title') }}</h1>
            </div>
            <div class="col-12 col-sm-auto mt-md-0 d-none d-sm-block">
              {{-- Button --}}
              @if($deployments_count > 0)
              <a href="{{ route('solution.deployments', $solution) }}" class="btn btn-secondary d-block d-md-inline-block">{{ __('views/solution/detail.activated_button') }}</a>
              @else
              <a href="{{ route('solution.deployments', $solution) }}" class="btn btn-primary d-block d-md-inline-block">{{ __('views/solution/detail.activate_button') }}</a>
              @endif
            </div>
          </div>
        </div>
      </div>

      {{-- solution info --}}
      @include('solution.detail-partials.solution_'.$solution->id.'_info')


      <div id="activate_button" class="d-block d-sm-none">
        {{-- Button --}}
        @if($deployments_count > 0)
        <a href="{{ route('solution.deployments', $solution) }}" class="btn btn-secondary d-block d-md-inline-block">{{ __('views/solution/detail.activated_button') }}</a>
        @else
        <a href="{{ route('solution.deployments', $solution) }}" class="btn btn-primary d-block d-md-inline-block">{{ __('views/solution/detail.activate_button') }}</a>
        @endif
      </div>


    </div>
  </div>
</div>
@endsection

@push('js')
<script src="{{ asset('assets/js/solution/detail.js?v='.time()) }}"></script>
@endpush
