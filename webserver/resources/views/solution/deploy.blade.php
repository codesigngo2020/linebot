@extends('layouts/app')

@section('title', '部署方案')

@push('css')
<link href="{{ asset('assets/css/solution/deploy.css?v='.time()) }}" rel="stylesheet">
<link href="{{ asset('assets/css/solution/deploy-partials/solution_'.$solution->id.'_deploy.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12 col-lg-10 col-xl-8">


      {{-- Header --}}
      <div class="header mt-md-5">
        <div class="header-body">
          <div class="row align-items-center">
            <div class="col">
              {{-- Pretitle --}}
              <h6 class="header-pretitle">Deploy New Solution</h6>
              {{-- Title --}}
              <h1 class="header-title d-flex align-items-center">
                <span>部署新方案</span>
                <span class="ml-2 mr-2">-</span>
                <span class="h2 nomargin">{{ $solution->name }}</span>
              </h1>
            </div>
          </div>
        </div>
      </div>

      @include('components.forms.solution._'.$solution->id.'.deploy')

    </div>
  </div>
</div>
@endsection


@push('js')
<script src="{{ asset('assets/js/solution/deploy-partials/solution_'.$solution->id.'_deploy.js?v='.time()) }}"></script>
@endpush
