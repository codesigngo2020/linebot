@extends('layouts/app')

@section('title', '解決方案')

@push('css')
<link href="{{ asset('assets/css/solution/index.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="container nopadding">
    <div id="solutions" class="d-flex flex-wrap my-6">
        @foreach($solutions as $solution)
        <div class="d-inine-flex col-12 col-sm-6 col-lg-4">
            <a class="card" href="{{ route('solution.detail', $solution) }}">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="icon">
                            <v-svg src="{{ $solution->icon[0]->getUrl() }}" width="30" height="30"></v-svg>
                        </div>
                        <div>
                            <p class="title card-title">{{ __('views/solution/index.solutions.'.$solution->id.'.title') }}</p>
                            <p class="category">{{ __('views/solution/index.solutions.'.$solution->id.'.category') }}</p>
                        </div>
                    </div>
                    <p class="description">{{ __('views/solution/index.solutions.'.$solution->id.'.description') }}</p>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection

@push('js')
<script src="{{ asset('assets/js/solution/index.js?v='.time()) }}"></script>
@endpush
