@extends('layouts/app')

@section('title', '文章管理')

@push('css')
<link href="{{ asset('assets/css/solution/_1/posts.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

  <div class="container-fluid mt-5">
    <div class="col-12 nopadding">
      <div class="header">
        <div class="header-body">
          <div class="row align-items-center">
            <div class="col">

              {{-- Pretitle --}}
              <h6 class="header-pretitle">Wordpress Posts</h6>

              {{-- Title --}}
              <h1 class="header-title">文章管理</h1>

            </div>
          </div> {{-- / .row --}}
          <div class="row align-items-center">
            <div class="col">

              {{-- Nav --}}
              <ul class="nav nav-tabs nav-overflow header-tabs" role="tablist">
                @foreach($crawlers as $key => $crawler)
                <li class="nav-item">
                  <a href="#nav-table-{{ $key + 1 }}" class="nav-link {{ $key == 0 ? 'active' : '' }} d-flex align-items-center white-space-nowrap" data-toggle="tab" role="tab" aria-controls="nav-table-{{ $key + 1 }}" aria-selected="{{ $key == 0 ? true : false }}">爬蟲 {{ $key + 1 }}<span class="badge badge-pill badge-soft-secondary ml-2">{{ $crawler->posts_count }}</span>
                  </a>
                </li>
                @endforeach
              </ul>
            </div>
          </div>

        </div>
      </div>


      <div class="tab-content">
        @foreach($crawlers as $key => $crawler)
        @include('components.tables.solution._1.posts')
        @endforeach
      </div>

    </div>

  </div>
</div>
@endsection


@push('plugin-js')
<script src="{{ asset('assets/plugins/list/list.min.js') }}"></script>
@endpush


@push('js')
<script src="{{ asset('assets/js/solution/_1/posts.js?v='.time()) }}"></script>
@endpush
