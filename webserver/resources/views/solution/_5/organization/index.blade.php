@extends('layouts/app')

@section('title', '組織架構')

@push('css')
<link href="{{ asset('assets/css/solution/_5/organization/index.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">

        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Organization</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>組織架構</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            @include('components.basic.solution._5.organization.organization-preview')
            @include('components.basic.solution._5.organization.branch')
            @include('components.basic.solution._5.organization.node')

            <div v-cloak id="organization-content">

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-header-title">組織架構圖</h4>
                        <a href="{{ route('solution.5.deployment.organization.edit', $deployment) }}" class="flex-grow-0">
                            <v-svg class="text-muted" src="{{ asset('assets/image/svg/light/pen.svg') }}" width="18" height="18"></v-svg>
                        </a>
                    </div>
                    <div class="card-body">

                        <organization-preview :organization="organization"></organization-preview>

                    </div>
                </div>



            </div>
        </div>

    </div>
</div>
@endsection


@push('plugin-js')
@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
    departments: {!! $departments->toJson() !!},
}
</script>
<script src="{{ asset('assets/js/solution/_5/organization/index.js?v='.time()) }}"></script>
@endpush
