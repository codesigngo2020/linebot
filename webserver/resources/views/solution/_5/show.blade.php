@extends('layouts/app')

@section('title', 'EIP 儀表板')

@push('css')
<link href="{{ asset('assets/css/solution/_5/show.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">
        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">EIP Dashboard</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>EIP 儀表板</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>
        </div>

    </div>
    @endsection


    @push('plugin-js')
    <!-- <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script> -->
    @endpush


    @push('js')
    <script>
    var args = {
        _token: "{{ csrf_token() }}",
        deployment: {{ $deployment->id }},
    }
</script>
<script src="{{ asset('assets/js/solution/_5/show.js?v='.time()) }}"></script>
@endpush
