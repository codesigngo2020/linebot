@extends('layouts/app')

@section('title', '好友資訊')

@push('css')
<link href="{{ asset('assets/css/solution/_4/users/show.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">

        @include('components.alerts.main')

        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">User Info</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>好友資訊</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            <div v-cloak id="user-content">

                <div class="card">
                    <div class="d-flex card-body">

                        <div class="d-flex align-items-center col-auto ml-n2">
                            <span class="profile-picture">
                                <img :src="user.picture_url" class="rounded-circle">
                            </span>
                        </div>

                        <div class="user-info col-auto flex-grow-1">
                            <p class="card-title d-flex align-items-center mb-3">
                                <span class="h4 mb-0 mr-3">@{{ '#'+user.id }}</span>
                                <span class="d-flex flex-grow-1 word-break-keep-all overflow-hidden h4 mb-0 mr-3 text-dark">
                                    <span class="mr-3">@{{ user.display_name }}</span>
                                    <span class="text-overflow-ellipsis overflow-hidden">@{{ user.user_id }}</span>
                                </span>
                                <span class="d-flex small">
                                    <span :class="[user.is_follow ? 'text-success' : 'text-warning', 'font-size-0750 mr-1']">●</span>
                                    <span>@{{ user.is_follow ? 'Follow' : 'Unfollow' }}</span>
                                </span>
                            </p>

                            <p :class="[groups.holding.length > 0 ? 'mb-3' : 'mb-0', 'text-muted']">@{{ user.status_message == '' ? '（好友未輸入狀態訊息）' : user.status_message }}</p>

                            <div v-if="groups.holding.length > 0" class="d-flex overflow-auto">
                                <a v-for="(group, groupIndex) in groups.holding" :class="[{'ml-3' : groupIndex != 0}, 'badge badge-soft-secondary']" :href="getGroupUrl(group.id)" target="_blank">@{{ group.name }}</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    {{-- 行為歷程 --}}
                    <div class="col-12 col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-header-title">行為歷程</h4>
                            </div>
                            <div :class="[user.history.length > 0 ? 'pt-5 pb-5 p-0' : 'p-0', 'card-body']">
                                <div v-if="user.history.length > 0" id="tags-history" @scroll=handleScroll($event)>
                                    <div class="pl-5 pr-5" ref="scrollInner">
                                        <div v-for="history in user.history" class="item">
                                            <div class="dot">
                                                <div class="inner-dot"></div>
                                            </div>
                                            <div class="info badge badge-light text-left w-100 p-3">
                                                <h5 class="d-flex align-items-center mb-2">
                                                    <span class="mr-2">@{{ '#'+history.id }}</span>
                                                    <span class="flex-grow-1">@{{ user.historyTags[history.tag_id].historyDescription.actionName }}</span>
                                                    <a class="d-inline-flex" :href="getTagUrl(history.tag_id)" target="_blank">
                                                        <v-svg class="svg text-warning" src="{{ asset('assets/image/svg/solid/tags.svg') }}" width="14" height="14"></v-svg>
                                                    </a>
                                                </h5>
                                                <p class="description text-secondary m-0" v-html="user.historyTags[history.tag_id].historyDescription.text"></p>
                                            </div>
                                            <div class="datetime small text-muted mb-0">
                                                <v-svg class="svg mr-1" src="{{ asset('assets/image/svg/light/clock.svg') }}" width="12" height="12"></v-svg>
                                                @{{ history.created_at }}
                                            </div>
                                        </div>
                                        <div v-if="history.isLoading" class="d-flex justify-content-center">
                                            <div class="spinner-grow text-secondary" role="status">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul v-else class="list-group">
                                    <li class="list-group-item no-items">No items</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6">
                        {{-- 所屬好友群組 --}}
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-header-title">所屬好友群組</h4>
                                <span class="badge badge-soft-secondary flex-grow-0 mr-3">持有中</span>
                                <span class="badge badge-soft-danger flex-grow-0">已移除</span>
                            </div>
                            <div :class="[user.groups.length > 0 ? 'pb-3' : 'p-0', 'card-body']">
                                <div v-if="user.groups.length > 0">
                                    <a v-for="(group, groupIndex) in user.groups" :class="[group.deleted_at ? 'badge-soft-danger' : 'badge-soft-secondary', 'badge mb-3 mr-3']" :href="getGroupUrl(group.id)" target="_blank">@{{ group.name }}</a>
                                </div>
                                <ul v-else class="list-group">
                                    <li class="list-group-item no-items">No items</li>
                                </ul>
                            </div>
                        </div>

                        {{-- 行為標籤 --}}
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-header-title">行為標籤</h4>
                            </div>
                            <div :class="[user.tags.length > 0 ? 'pb-3' : 'p-0', 'card-body']">
                                <div v-if="user.tags.length > 0">
                                    <a v-for="(tag, tagIndex) in user.tags" :class="['badge badge-soft-secondary mb-3 mr-3']" :href="getTagUrl(tag.id)" target="_blank">@{{ tag.name }}</a>
                                </div>
                                <ul v-else class="list-group">
                                    <li class="list-group-item no-items">No items</li>
                                </ul>
                            </div>
                        </div>

                        {{-- 附註 --}}
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-header-title">附註</h4>
                            </div>
                            <div class="card-body p-0">
                                <ul class="list-group">
                                    <li class="list-group-item no-items">No items</li>
                                </ul>
                            </div>
                        </div>

                        {{-- 案件列表 --}}
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-header-title">案件列表</h4>
                            </div>
                            <div class="card-body p-0">
                                <ul class="list-group">
                                    <li class="list-group-item no-items">No items</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>







            </div>
        </div>

    </div>
</div>
@endsection


@push('plugin-js')
@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
    user: {!! $user->toJson() !!}
}
</script>
<script src="{{ asset('assets/js/solution/_4/users/show.js?v='.time()) }}"></script>
@endpush
