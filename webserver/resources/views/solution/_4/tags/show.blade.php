@extends('layouts/app')

@section('title', '標籤內容')

@push('css')
<link href="{{ asset('assets/css/solution/_4/tags/show.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">

        @include('components.alerts.main')

        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Tag Info</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>標籤內容</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            <div v-cloak id="tag-content">

                <div class="card">
                    <div class="card-body">
                        <h3 class="d-flex align-items-center white-space-nowrap mb-3">
                            <span class="mr-3">@{{ '# '+tag.id }}</span>
                            <span v-if="tag.name" class="flex-grow-0 mr-3">@{{ tag.name }}</span>
                            <span class="text-overflow-ellipsis overflow-hidden white-space-nowrap flex-grow-1 mr-4">@{{ tag.uuid }}</span>
                            <a href="javascript:void(0);" class="btn btn-sm btn-white" data-toggle="modal" data-target="#tag-naming">標籤命名</a>
                        </h3>

                        <p v-html="tag.description" class="text-muted mb-4"></p>

                        {{-- 推播訊息人數占比 --}}
                        <div class="align-items-center mb-3">
                            <div class="align-items-center no-gutters">
                                <div class="small mb-2">
                                    <span class="mr-2">@{{ '觸及人數佔比：'+(tag.total_users_count == 0 ? 0 :Math.floor(tag.users_count*100/tag.total_users_count))+'%（'+tag.users_count+' / '+tag.total_users_count+'）' }}</span>
                                    <span>@{{ '總觸及人次：'+tag.total_count }}</span>
                                </div>
                                <div class="col-12">
                                    <!-- Progress -->
                                    <div class="progress progress-sm">
                                        <div class="progress-bar" role="progressbar" :style="'width:'+(tag.total_users_count == 0 ? 0 : Math.floor(tag.users_count*100/tag.total_users_count))+'%'"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {{-- 標籤命名modal --}}

                <div id="tag-naming" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tagNaming" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">輸入標籤名稱</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                {{-- 標籤名稱 --}}
                                <div class="form-group position-relative d-flex flex-column m-0">
                                    <div :class="[{'is-invalid' : form.name.invalid}, 'i-form-control d-flex align-items-center']">
                                        <input id="name" type="text" :class="[{'is-invalid' : form.name.invalid}, 'form-control pr-6']" v-model="form.name.name" placeholder="請輸入標籤名稱">
                                        <button type="button" :class="[{'progress-bar-striped progress-bar-animated' : submitting}, 'btn btn-primary btn-sm mr-2 position-absolute right']" @click="submit">送出</button>
                                    </div>
                                    <span class="invalid-feedback" role="alert"><strong>@{{ form.name.info }}</strong></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {{-- 累積觸發人數佔比圖 - 觸發人次圖＆累積觸發人次圖 --}}

                <div v-for="(chart, chartIndex) in charts" class="card">
                    <div class="card-header">
                        <h4 class="card-header-title">@{{ chart.cardName }}</h4>

                        <div v-if="chart.name == 'cumulativeUsersCountChart'" class="d-flex justify-content-end flex-grow-1">
                            <div class="btn-group">
                                <button type="button" :class="[{active : chart.type == 'usersCount'}, 'type btn btn-outline-secondary border-right-0 pl-3 pr-3 p-1 font-size-0875']" @click="changeChartType(chartIndex, 'usersCount')">
                                    觸發人次
                                </button>
                                <button type="button" :class="[{active : chart.type == 'cumulativeUsersCount'}, 'type btn btn-outline-secondary border-left-0 pl-3 pr-3 p-1 font-size-0875']" @click="changeChartType(chartIndex, 'cumulativeUsersCount')">
                                    累積觸發人次
                                </button>
                            </div>
                        </div>

                    </div>
                    <div class="card-body position-relative">

                        <div :class="[chart.isLoading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute top left justify-content-center align-items-center h-100 w-100 bg-secondary']">
                            <div class="spinner-grow text-secondary" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>

                        <div class="d-flex align-items-center mb-3">
                            <span class="card-header-title flex-grow-0 font-size-0875 mr-3">@{{ chart.datetime.date.year+' 年 '+('0'+chart.datetime.date.month).slice(-2)+' 月 '+('0'+chart.datetime.date.day).slice(-2)+' 日' }}</span>

                            <div class="col-auto flex-grow-0 p-0">
                                <a href="javascript:void(0);" class="btn btn-sm btn-white pr-1 pl-1 p-0" @click="last(chartIndex)">
                                    <span class="d-inline-flex">
                                        <v-svg class="svg" src="{{ asset('assets/image/svg/regular/chevron-left.svg') }}" width="8" height="8"></v-svg>
                                    </span>
                                </a>
                            </div>
                            <div class="col-auto flex-grow-0 pl-2 pr-2">
                                <a href="javascript:void(0);" class="btn btn-sm btn-white pl-2 pr-2 p-0" @click="toTodayOrThisWeek(chartIndex, true)">@{{ chart.datetime.type == 'week' ? 'This week' : 'Today' }}</a>
                            </div>
                            <div class="col-auto flex-grow-0 p-0">
                                <a href="javascript:void(0);" class="btn btn-sm btn-white pr-1 pl-1 p-0" @click="((chart.datetime.type == 'week' && chart.datetime.date.offset < datetimeSetting.thisWeekOffset) || (chart.datetime.type == 'hour' && chart.datetime.date.offset < 0)) ? next(chartIndex) : null">
                                    <span class="d-inline-flex">
                                        <v-svg class="svg" src="{{ asset('assets/image/svg/regular/chevron-right.svg') }}" width="8" height="8"></v-svg>
                                    </span>
                                </a>
                            </div>

                            <div class="d-flex justify-content-end flex-grow-1">
                                <div class="btn-group">
                                    <button type="button" :class="[{active : chart.datetime.type == 'week'}, 'type btn btn-outline-secondary border-right-0 pl-3 pr-3 p-1']" @click="changeDatetimeType(chartIndex, 'week')">
                                        <v-svg class="svg" src="{{ asset('assets/image/svg/light/calendar-week.svg') }}" width="14" height="14"></v-svg>
                                    </button>
                                    <button type="button" :class="[{active : chart.datetime.type == 'hour'}, 'type btn btn-outline-secondary border-left-0 pl-3 pr-3 p-1']" @click="changeDatetimeType(chartIndex, 'hour')">
                                        <v-svg class="svg" src="{{ asset('assets/image/svg/light/clock.svg') }}" width="14" height="14"></v-svg>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="chart">
                            <canvas :id="chart.id" class="chart-canvas"></canvas>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
@endsection


@push('plugin-js')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
    tag: {!! $tag->toJson() !!}
}
</script>
<script src="{{ asset('assets/js/solution/_4/tags/show.js?v='.time()) }}"></script>
@endpush
