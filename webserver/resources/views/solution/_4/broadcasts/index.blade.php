@extends('layouts/app')

@section('title', '推播訊息列表')

@push('plugin-css')
{{-- multiselect CSS --}}
<link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
@endpush

@push('css')
<link href="{{ asset('assets/css/solution/_4/broadcasts/index.css?v='.time()) }}" rel="stylesheet">
<link href="{{ asset('assets/css/solution/_4/basic/category/index.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">
        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Broadcast List</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>推播訊息列表</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                        <div id="new-broadcast" class="col-auto mr-3 mr-sm-0">
                            {{-- Button --}}
                            <a href="{{ route('solution.4.deployment.broadcasts.create', $deployment) }}" class="btn btn-primary d-none d-sm-block">新增推播訊息</a>
                            <a href="{{ route('solution.4.deployment.broadcasts.create', $deployment) }}" class="btn btn-transparent nopadding rounded-circle d-block d-sm-none">
                                <span>
                                    <v-svg class="svg d-flex text-primary" src="{{ asset('assets/image/svg/light/plus-circle.svg') }}" width="22" height="22"></v-svg>
                                </span>
                            </a>
                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            @include('components.tables.solution._4.broadcasts.calendar')

            <div v-cloak id="broadcasts-table">
                <div class="d-flex justify-content-end mb-3">
                    <div class="btn-group">
                        <button type="button" :class="[{active : type == 'calendar'}, 'type btn btn-outline-secondary border-right-0']" @click="changeType('calendar')">
                            <v-svg class="svg" src="{{ asset('assets/image/svg/light/calendar-alt.svg') }}" width="18" height="18"></v-svg>
                        </button>
                        <button type="button" :class="[{active : type == 'list'}, 'type btn btn-outline-secondary border-left-0']" @click="changeType('list')">
                            <v-svg class="svg" src="{{ asset('assets/image/svg/regular/list.svg') }}" width="18" height="18"></v-svg>
                        </button>
                    </div>
                </div>

                @include('components.tables.solution._4.broadcasts.table')

                <broadcast-calendar v-if="type == 'calendar'"></broadcast-calendar>
                <broadcast-list v-if="type == 'list'"></broadcast-list>


            </div>
        </div>









    </div>
</div>
@endsection


@push('plugin-js')
{{-- multiselect JS --}}
<script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
<script src="{{ asset('assets/components/select/select.js') }}"></script>
@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
}
</script>
<script src="{{ asset('assets/js/solution/_4/broadcasts/index.js?v='.time()) }}"></script>
@endpush
