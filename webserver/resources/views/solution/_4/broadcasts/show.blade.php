@extends('layouts/app')

@section('title', '推播訊息內容')

@push('css')
<link href="{{ asset('assets/css/solution/_4/basic/message/message/show.css?v='.time()) }}" rel="stylesheet">
<link href="{{ asset('assets/css/solution/_4/broadcasts/show.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">

        @include('components.alerts.main')

        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Broadcast Info</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>推播訊息內容</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            <div v-cloak id="broadcast-content">

                <div class="card">
                    <div class="card-body">
                        <h3 class="d-flex align-items-center mb-3">
                            <span class="mr-3">@{{ '# '+broadcast.id }}</span>
                            <span class="mr-4">@{{ broadcast.name }}</span>

                            <span class="d-inline-flex flex-grow-1">
                                <a class="d-inline-flex" :href="getTagUrl(broadcast.tag_id)" target="_blank">
                                    <v-svg :class="[tagId == broadcast.tag_id ? 'text-2451010' : 'text-warning', 'svg']" src="{{ asset('assets/image/svg/solid/tags.svg') }}" width="16" height="16"></v-svg>
                                </a>
                            </span>

                            <span class="sent-status position-relative d-flex align-items-center mr-3 small mr-3 pr-3">
                                <span :class="['badge-soft-'+sendStatus[broadcast.send_status].class, 'badge']">@{{ sendStatus[broadcast.send_status].status }}</span>
                            </span>

                            <span class="position-relative small">
                                <span class="badge badge-soft-secondary">@{{ targetTypes[broadcast.target_type] }}</span>
                            </span>

                            <span v-if="broadcast.deleteable" class="remove-btn position-relative d-inline-flex align-items-center cursor-pointer small ml-3 pl-3" @click="remove">
                                <v-svg class="svg text-muted" src="{{ asset('assets/image/svg/regular/trash-alt.svg') }}" width="16" height="16"></v-svg>
                            </span>
                        </h3>

                        <p class="text-muted mb-4">@{{ broadcast.description }}</p>

                        {{-- 推播訊息人數占比 --}}
                        <div class="align-items-center mb-3">
                            <div class="align-items-center no-gutters">
                                <div class="small mb-2">@{{ '已推播人數佔比：'+Math.floor(broadcast.users_count*100/broadcast.total_users_count)+'%（'+broadcast.users_count+' / '+broadcast.total_users_count+'）' }} </div>
                                <div class="col-12">
                                    <!-- Progress -->
                                    <div class="progress progress-sm">
                                        <div class="progress-bar" role="progressbar" :style="'width:'+Math.floor(broadcast.users_count*100/broadcast.total_users_count)+'%'"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @if($broadcast->template_id)

                <div class="text-right">
                    <a href="{{ route('solution.4.deployment.messagesPool.show', [$deployment, $broadcast->template_id]) }}" class="d-inline-flex align-items-center btn btn-secondary" target="_blank">
                        <span class="mr-3">前往訊息模組</span>
                        <v-svg class="svg" src="{{ asset('assets/image/svg/regular/long-arrow-right.svg') }}" width="18" height="18"></v-svg>
                    </a>
                </div>

                @else

                @include('components.basic.solution._4.message.messages')
                @include('components.basic.solution._4.message.message-preview')
                @include('components.basic.solution._4.message.action-list')

                <message-preview :tagid="tagId" :messages="broadcast.messages" :messagesformdata="broadcast.messages_form_data" :tagsdata="broadcast.tagsData" :userscount="broadcast.target_type == 'all' ? broadcast.total_users_count : broadcast.users_count" :showactions="true" :showtags="true"></message-preview>

                @endif

            </div>
        </div>

    </div>
</div>
@endsection


@push('plugin-js')
@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
    broadcast: {!! $broadcast->toJson() !!},
    tagId: {{ request()->input('tagId') ?? 0 }},
}
</script>
<script src="{{ asset('assets/js/solution/_4/broadcasts/show.js?v='.time()) }}"></script>
@endpush
