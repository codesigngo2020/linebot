@extends('layouts/app')

@section('title', '訊息內容')

@push('plugin-css')
{{-- multiselect CSS --}}
<link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
@endpush

@push('css')
<link href="{{ asset('assets/css/solution/_4/basic/message/message/show.css?v='.time()) }}" rel="stylesheet">
<link href="{{ asset('assets/css/solution/_4/basic/message/template/show.css?v='.time()) }}" rel="stylesheet">
<link href="{{ asset('assets/css/solution/_4/messages-pool/show.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">
        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Message Info</h6>

                            <div class="d-flex">

                                {{-- Title --}}
                                <h1 class="header-title d-flex align-items-center flex-grow-1">
                                    <span>訊息內容</span>
                                    <span class="ml-2 mr-2">-</span>
                                    <span class="h2 nomargin">{{ $solution->name }}</span>
                                </h1>

                                {{-- Version --}}
                                <div id="message-version" class="d-inline-flex">
                                    <vue-multiselect v-if="version.options.length > 1" class="i-form-control hidden-arrow multiselect-sm" v-model="version.value" track-by="value" label="name" open-direction="bottom" :options="version.options" :searchable="false" :show-labels="false" :allow-empty="false" :multiple="false" :internal-search="false" :show-no-results="false" :hide-selected="false">
                                        <template slot="singleLabel" slot-scope="props">
                                            <div class="d-flex justify-content-center">
                                                <span v-if="props.option.active" class="d-flex align-items-center text-info mr-2">
                                                    <v-svg src="{{ asset('assets/image/svg/light/check-circle.svg') }}" width="14" height="14"></v-svg>
                                                </span>
                                                <span>@{{ props.option.name }}</span>
                                            </div>
                                        </template>
                                        <template slot="option" slot-scope="props">
                                            <div class="d-flex justify-content-center">
                                                <span v-if="props.option.active" class="d-flex align-items-center text-info mr-2">
                                                    <v-svg src="{{ asset('assets/image/svg/light/check-circle.svg') }}" width="14" height="14"></v-svg>
                                                </span>
                                                <span>@{{ props.option.name }}</span>
                                            </div>
                                        </template>
                                    </vue-multiselect>
                                </div>

                            </div>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            <div v-cloak id="message-content">
                <div class="card">
                    <div class="card-body">
                        <h3 class="d-flex align-items-center mb-3">
                            <span class="mr-3">@{{ '# '+message.parent_id }}</span>
                            <span class="{{ $message->type == 'template' ? 'mr-4' : 'flex-grow-1' }}">@{{ message.name }}</span>

                            @if($message->type == 'template')
                            <span class="flex-grow-1">
                                <a class="d-inline-flex" :href="getTagUrl(message.tag_id)" target="_blank">
                                    <v-svg :class="[tagId == message.tag_id ? 'text-2451010' : 'text-warning', 'svg']" src="{{ asset('assets/image/svg/solid/tags.svg') }}" width="16" height="16"></v-svg>
                                </a>
                            </span>
                            @endif

                            @if($message->deleted_all_at)
                            <span :class="[{'deleted pr-3 mr-3' : message.versions.length > 1}, 'deleted position-relative d-flex align-items-center small']">
                                <span class="badge badge-soft-danger">Deleted</span>
                            </span>
                            @endif

                            @if(count($message->versions) > 1)
                            <span class="version position-relative d-flex align-items-center small">
                                <span class="badge badge-soft-info">@{{ 'v'+message.version }}</span>
                            </span>
                            @endif

                            @if(!$message->deleted_all_at)
                            <div class="dropdown d-flex ml-3">
                                <a href="#" class="d-inline-flex align-items-center cursor-pointer dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <v-svg class="svg text-muted" src="{{ asset('assets/image/svg/light/ellipsis-v.svg') }}" width="22" height="22"></v-svg>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right p-0">

                                    @if(!$message->is_current_version)
                                    <a href="javascript:void(0);" class="dropdown-item" @click="switchVersion(message.id)">切換為此版本</a>
                                    @endif

                                    <a href="{{ route('solution.4.deployment.messagesPool.edit', [$deployment, $message]) }}" class="dropdown-item" target="_blank">建立新版本</a>
                                </div>
                            </div>
                            @endif
                        </h3>

                        <p :class="[message.type == 'message' ? 'm-0' : 'mb-4', 'text-muted']">@{{ message.description }}</p>

                        {{-- 觸發訊息模組人數占比 --}}
                        @if($message->type == 'template')

                        <div class="align-items-center mb-3">
                            <div class="align-items-center no-gutters">
                                <div class="small mb-2">
                                    <span class="mr-2">@{{ '使用人數佔比：'+(message.total_users_count == 0 ? 0 : Math.floor(message.users_count*100/message.total_users_count))+'%（'+message.users_count+' / '+message.total_users_count+'）' }}</span>
                                    <span>@{{ '總使用人次：'+message.total_count }}</span>
                                </div>
                                <div class="col-12">
                                    <!-- Progress -->
                                    <div class="progress progress-sm">
                                        <div class="progress-bar" role="progressbar" :style="'width:'+Math.floor(message.users_count*100/message.total_users_count)+'%'"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endif

                    </div>
                </div>

                @include('components.basic.solution._4.message.messages')
                @include('components.basic.solution._4.message.message-preview')
                @include('components.basic.solution._4.message.action-list')

                @if($message->type == 'template')

                @include('components.basic.solution._4.messageTemplate.message-template-preview')
                @include('components.basic.solution._4.messageTemplate.branch')
                @include('components.basic.solution._4.messageTemplate.node')

                <div v-if="!expandMessage" id="display-switch" class="d-flex justify-content-end mb-4">
                    <div class="btn-group">
                        <button type="button" :class="[{active : !expandMessage && display == 'message'}, 'type btn btn-outline-secondary border-right-0 pl-3 pr-3 p-2']" @click="changeDisplay('message')">
                            模組 ／ 訊息
                        </button>
                        <button type="button" :class="[{active : !expandMessage && display == 'actions'}, 'type btn btn-outline-secondary border-left-0 pl-3 pr-3 p-2']" @click="changeDisplay('actions')">
                            訊息 ／ 觸發行為
                        </button>
                    </div>
                </div>

                <div class="row">

                    <div v-show="expandMessage || display == 'message'" id="message" :class="[expandMessage ? 'col-12' : 'unexpand col-auto flex-grow-1']">
                        <div class="card">
                            <div class="card-header">
                                <h4 :class="[expandMessage ? 'flex-grow-0' : 'flex-grow-1', 'card-header-title']">訊息模組預覽</h4>

                                {{-- 節點人流數據 --}}
                                <div v-if="expandMessage" class="d-flex justify-content-end flex-grow-1 small cursor-pointer">
                                    <div class="d-flex align-items-center mr-4" @click="toggleFlowType('users')">
                                        <div class="count-label badge badge-primary mr-2">25%</div>
                                        <div>觸發訊息人數 / 模組使用人數</div>
                                    </div>
                                    <div class="d-flex align-items-center mr-5" @click="toggleFlowType('total')">
                                        <div class="count-label badge badge-warning-white mr-2">50%</div>
                                        <div>觸發訊息人次 / 模組使用人次</div>
                                    </div>
                                </div>

                                {{-- 展開模組 --}}
                                <div class="d-flex flex-grow-0">
                                    <span v-show="!expandMessage" id="message-expand" class="text-muted cursor-pointer" @click="toggleExpandMessage(true)">
                                        <v-svg class="svg" src="{{ asset('assets/image/svg/regular/expand.svg') }}" width="18" height="18"></v-svg>
                                    </span>
                                    <span v-show="expandMessage" id="message-compress" class="text-muted cursor-pointer" @click="toggleExpandMessage(false)">
                                        <v-svg class="svg" src="{{ asset('assets/image/svg/regular/compress.svg') }}" width="18" height="18"></v-svg>
                                    </span>
                                </div>
                            </div>
                            <div class="card-body position-relative">
                                <a class="position-absolute top right d-inline-flex mt-4 mr-4 z-index-1" :href="getTagUrl(getTagIdByNodeFromDataId(showNodeDetailId))" target="_blank">
                                    <v-svg :class="[tagId == getTagIdByNodeFromDataId(showNodeDetailId) ? 'text-2451010' : 'text-warning', 'svg']" src="{{ asset('assets/image/svg/solid/tags.svg') }}" width="16" height="16"></v-svg>
                                </a>
                                <message-template-preview :expandmessage="expandMessage" :flowtype="flowType" :shownodedetailid="showNodeDetailId" :nodes="message.nodes" :nodesformdata="message.nodes_form_data" :nodetagsdata="message.nodeTagsData" :totalcount="message.total_count" :userscount="message.users_count" @shownodedetail="showNodeDetail"></message-template-preview>
                            </div>
                        </div>
                    </div>

                    <div :class="[expandMessage || display == 'actions' ? 'col-12' : 'col-auto']">
                        <message-preview :messages="getNodeMessages()" :messagesformdata="getNodeMessagesFormData()" :tagsdata="message.actionTagsData" :userscount="message.users_count" :showactions="expandMessage || display == 'actions'" :showtags="true" :tagid="tagId"></message-preview>
                    </div>

                </div>

                @else

                <message-preview :messages="message.messages" :messagesformdata="message.messages_form_data" :showtags="false"></message-preview>

                @endif

            </div>
        </div>

    </div>
</div>
@endsection


@push('plugin-js')
{{-- multiselect JS --}}
<script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
<script src="{{ asset('assets/components/select/select.js') }}"></script>
@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
    message: {!! $message->toJson() !!},
    tagId: {{ request()->input('tagId') ?? 0 }},
    nodeId: {{ request()->input('nodeId') ?? 0 }},
    expandScript: {{ request()->input('expandMessage') ?? 'false' }},
}
</script>
<script src="{{ asset('assets/js/solution/_4/messages-pool/show.js?v='.time()) }}"></script>
@endpush
