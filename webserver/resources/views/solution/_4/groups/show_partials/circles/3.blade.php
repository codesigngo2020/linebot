@push('js')

<script type="text/x-template" id="--circles-3">
    <div class="d-flex">
        <div v-if="circleIndex != 0" class="d-flex align-items-center text-muted">
            <v-svg class="svg" src="{{ asset('assets/image/svg/light/plus.svg') }}" width="22" height="22"></v-svg>
        </div>

        <div class="circles droppable border-radius-05">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350 260">

                <path v-for="area in areas" :key="area.id" :class="[{active : circle.selectedAreas.includes(area.id)}, 'area']" fill="transparent" stroke="#d2ddec" stroke-width="2px" :d="area.path"/>

                <polyline fill="none" stroke="#d2ddec" stroke-linecap="round" stroke-width="2px" points="8,29.5 122,29.5 136.8,53.5"/>

                <a :href="getTagUrl(circle.tagsId[0])" target="_blank">
                    <text x="8px" y="20px" width="114px" font-size="12">
                        <tspan>@{{ clipedTagsName(tags[circle.tagsId[0]].name) }}</tspan>
                    </text>
                </a>

                <polyline fill="none" stroke="#d2ddec" stroke-linecap="round" stroke-width="2px" points="3.35,243 120.35,243 133.5,219.5"/>

                <a :href="getTagUrl(circle.tagsId[1])" target="_blank">
                    <text x="8px" y="233.5px" width="114px" font-size="12">
                        <tspan>@{{ clipedTagsName(tags[circle.tagsId[1]].name) }}</tspan>
                    </text>
                </a>

                <polyline fill="none" stroke="#d2ddec" stroke-linecap="round" stroke-width="2px" points="346.65,243 229.65,243 216.5,219.5"/>

                <a :href="getTagUrl(circle.tagsId[2])" target="_blank">
                    <text x="231.65px" y="233.5px" width="114px" font-size="12">
                        <tspan>@{{ clipedTagsName(tags[circle.tagsId[2]].name) }}</tspan>
                    </text>
                </a>

            </svg>
        </div>
    </div>
</script>

<script>
Vue.component('circles-3', {
    template: '#--circles-3',
    props: ['circleIndex', 'circle', 'tags'],
    data(){
        return {
            areas: [
                {
                    id: '1',
                    path: 'M175,106.19a61.68,61.68,0,0,1,62.28-2c0-.84.07-1.68.07-2.53a61.65,61.65,0,0,0-123.3,0c0,.61,0,1.21,0,1.82a61.62,61.62,0,0,1,60.9,2.72Z',
                },
                {
                    id: '2',
                    path: 'M146.19,158.35c0-.85,0-1.69.07-2.53a61.63,61.63,0,0,1-32.16-52.35,61.64,61.64,0,1,0,60.9,107A61.59,61.59,0,0,1,146.19,158.35Z',
                },
                {
                    id: '3',
                    path: 'M237.28,104.18a61.61,61.61,0,0,1-33.52,52.35c0,.61.05,1.21.05,1.82A61.59,61.59,0,0,1,175,210.51a61.65,61.65,0,1,0,62.28-106.33Z',
                },
                {
                    id: '4',
                    path: 'M175,106.19a61.62,61.62,0,0,0-60.9-2.72,61.63,61.63,0,0,0,32.16,52.34A61.6,61.6,0,0,1,175,106.19Z',
                },
                {
                    id: '5',
                    path: 'M175.7,163.29a61.31,61.31,0,0,1-29.42-7.46c0,.84-.09,1.68-.09,2.52A61.59,61.59,0,0,0,175,210.51a61.59,61.59,0,0,0,28.81-52.16c0-.61-.05-1.21-.06-1.81A61.38,61.38,0,0,1,175.7,163.29Z',
                },
                {
                    id: '6',
                    path: 'M207.84,96.71A61.32,61.32,0,0,0,175,106.19a61.58,61.58,0,0,1,28.76,50.35,61.66,61.66,0,0,0,33.52-52.36A61.31,61.31,0,0,0,207.84,96.71Z',
                },
                {
                    id: '7',
                    path: 'M175,106.19a61.6,61.6,0,0,0-28.74,49.63,61.68,61.68,0,0,0,57.5.71A61.59,61.59,0,0,0,175,106.19Z',
                },
            ]
        };
    },
    methods: {
        clipedTagsName(tagName){
            div = document.createElement("div");
            div.textContent = tagName;
            div.style.display = 'inline-block';
            div.style.visibility = 'hidden';
            div.style.height = 0;
            document.body.appendChild(div);

            if(div.offsetWidth <= 114){
                div.remove();
                return tagName;
            }

            while(div.offsetWidth > 114 || tagName == ''){
                tagName = tagName.substr(0, tagName.length - 1);
                div.textContent = tagName+'...';
            }
            div.remove();
            return tagName+'...';
        },
        getTagUrl(id){
            return route('solution.4.deployment.tags.show', {
                deployment: args.deployment,
                tagId: id,
            });
        },
    }
});
</script>
@endpush
