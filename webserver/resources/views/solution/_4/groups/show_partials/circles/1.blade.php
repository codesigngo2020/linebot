@push('js')

<script type="text/x-template" id="--circles-1">
    <div class="d-flex">
        <div v-if="circleIndex != 0" class="d-flex align-items-center text-muted">
            <v-svg class="svg" src="{{ asset('assets/image/svg/light/plus.svg') }}" width="22" height="22"></v-svg>
        </div>

        <div class="circles droppable border-radius-05">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 275 260">

                <circle :class="[{active : circle.selectedAreas.includes('1')}, 'area']" fill="transparent" stroke="#d2ddec" stroke-width="2px" cx="137.5" cy="130" r="75"/>

                <polyline fill="none" stroke="#d2ddec" stroke-linecap="round" stroke-width="2px" points="8,29.5 122,29.5 136.8,54.5"/>
                <a :href="getTagUrl(circle.tagsId[0])" target="_blank">
                    <text x="8px" y="20px" width="114px" font-size="12">
                        <tspan>@{{ clipedTagsName(tags[circle.tagsId[0]].name) }}</tspan>
                    </text>
                </a>
            </svg>
        </div>
    </div>
</script>

<script>
Vue.component('circles-1', {
    template: '#--circles-1',
    props: ['circleIndex', 'circle', 'tags'],
    methods: {
        clipedTagsName(tagName){
            div = document.createElement("div");
            div.textContent = tagName;
            div.style.display = 'inline-block';
            div.style.visibility = 'hidden';
            div.style.height = 0;
            document.body.appendChild(div);

            if(div.offsetWidth <= 114){
                div.remove();
                return tagName;
            }

            while(div.offsetWidth > 114 || tagName == ''){
                tagName = tagName.substr(0, tagName.length - 1);
                div.textContent = tagName+'...';
            }
            div.remove();
            return tagName+'...';
        },
        getTagUrl(id){
            return route('solution.4.deployment.tags.show', {
                deployment: args.deployment,
                tagId: id,
            });
        },
    },
});
</script>
@endpush
