@push('js')

<script type="text/x-template" id="--circles-2">
    <div class="d-flex">
        <div v-if="circleIndex != 0" class="d-flex align-items-center text-muted">
            <v-svg class="svg" src="{{ asset('assets/image/svg/light/plus.svg') }}" width="22" height="22"></v-svg>
        </div>

        <div class="circles droppable border-radius-05">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350 260">

                <path v-for="area in areas" :key="area.id" :class="[{active : circle.selectedAreas.includes(area.id)}, 'area']" fill="transparent" stroke="#d2ddec" stroke-width="2px" :d="area.path"/>

                <polyline fill="none" stroke="#d2ddec" stroke-linecap="round" stroke-width="2px" points="8,29.5 122,29.5 136.8,54.5"/>
                <a :href="getTagUrl(circle.tagsId[0])" target="_blank">
                    <text x="8px" y="20px" width="114px" font-size="12">
                        <tspan>@{{ clipedTagsName(tags[circle.tagsId[0]].name) }}</tspan>
                    </text>
                </a>
                <polyline fill="none" stroke="#d2ddec" stroke-linecap="round" stroke-width="2px" points="345.65,243 229.65,243 212.3,205.5"/>
                <a :href="getTagUrl(circle.tagsId[1])" target="_blank">
                    <text x="231.65px" y="233.5px" width="114px" font-size="12">
                        <tspan>@{{ clipedTagsName(tags[circle.tagsId[1]].name) }}</tspan>
                    </text>
                </a>
            </svg>
        </div>
    </div>
</script>

<script>
Vue.component('circles-2', {
    template: '#--circles-2',
    props: ['circleIndex', 'circle', 'tags'],
    data(){
        return {
            areas: [
                {
                    id: '1',
                    path: 'M137.5,130c0-27.8,15.1-52,37.5-64.9C164,58.7,151.2,55,137.5,55c-41.4,0-75,33.6-75,75c0,41.4,33.6,75,75,75c13.7,0,26.5-3.7,37.5-10.1C152.6,182,137.5,157.8,137.5,130z',
                },
                {
                    id: '2',
                    path: 'M175,65.1c-22.4,13-37.5,37.2-37.5,64.9s15.1,52,37.5,64.9c22.4-13,37.5-37.2,37.5-64.9S197.4,78,175,65.1z',
                },
                {
                    id: '3',
                    path: 'M212.5,55c-13.7,0-26.5,3.7-37.5,10.1c22.4,13,37.5,37.2,37.5,64.9s-15.1,52-37.5,64.9c11,6.4,23.8,10.1,37.5,10.1c41.4,0,75-33.6,75-75C287.5,88.6,253.9,55,212.5,55z',
                },
            ]
        };
    },
    methods: {
        clipedTagsName(tagName){
            div = document.createElement("div");
            div.textContent = tagName;
            div.style.display = 'inline-block';
            div.style.visibility = 'hidden';
            div.style.height = 0;
            document.body.appendChild(div);

            if(div.offsetWidth <= 114){
                div.remove();
                return tagName;
            }

            while(div.offsetWidth > 114 || tagName == ''){
                tagName = tagName.substr(0, tagName.length - 1);
                div.textContent = tagName+'...';
            }
            div.remove();
            return tagName+'...';
        },
        getTagUrl(id){
            return route('solution.4.deployment.tags.show', {
                deployment: args.deployment,
                tagId: id,
            });
        },
    }
});
</script>
@endpush
