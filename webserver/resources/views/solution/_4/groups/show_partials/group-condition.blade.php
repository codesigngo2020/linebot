@push('js')

{{-- 好友組條件 --}}

<script type="text/x-template" id="--group-condition">
    <div id="group-condition-layout" class="position-relative overflow-hidden">

        <div class="d-flex overflow-auto">

            <component v-for="(circle, circleIndex) in circles" :key="circleIndex" :is="'circles-'+circle.tagsId.length" :circleIndex="circleIndex" :circle="circle" :tags="tags"></component>

        </div>

    </div>
</script>

<script>
Vue.component('group-condition', {
    template: '#--group-condition',
    props: ['circles', 'tags'],
});
</script>

@endpush
