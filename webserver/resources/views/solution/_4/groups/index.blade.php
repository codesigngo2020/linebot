@extends('layouts/app')

@section('title', '好友群組列表')

@push('css')
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">
        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Group List</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>好友群組列表</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                        <div id="new-group" class="col-auto mr-3 mr-sm-0">
                            {{-- Button --}}
                            <a href="{{ route('solution.4.deployment.groups.create', $deployment) }}" class="btn btn-primary d-none d-sm-block">新增好友群組</a>
                            <a href="{{ route('solution.4.deployment.groups.create', $deployment) }}" class="btn btn-transparent nopadding rounded-circle d-block d-sm-none">
                                <span>
                                    <v-svg class="svg d-flex text-primary" src="{{ asset('assets/image/svg/light/plus-circle.svg') }}" width="22" height="22"></v-svg>
                                </span>
                            </a>
                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            <div id="groups-table">
                @include('components.tables.solution._4.groups.table')

                <nav v-cloak v-if="paginator.lastPage > 1" class="d-flex justify-content-center" aria-label="Page navigation">
                    <ul class="pagination">
                        <li v-if="paginator.currentPage >= 4" class="page-item" @click="changePage(paginator.currentPage - 3)">
                            <a class="page-link" href="javascript:void(0);" aria-label="Previous">
                                <span aria-hidden="true">...</span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </li>

                        <li v-for="i in paginator.currentPage + 2" v-if="i >= 1 && i <= paginator.lastPage && i >= paginator.currentPage - 2" :class="['page-item', {'active': i == paginator.currentPage }]" @click="changePage(i)">
                            <a class="page-link" href="javascript:void(0);">@{{ i }}</a>
                        </li>

                        <li v-if="paginator.currentPage <= paginator.lastPage - 3" class="page-item" @click="changePage(paginator.currentPage + 3)">
                            <a class="page-link" href="javascript:void(0);" aria-label="Next">
                                <span aria-hidden="true">...</span>
                                <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav>

            </div>
        </div>









    </div>
</div>
@endsection


@push('plugin-js')
@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
}
</script>
<script src="{{ asset('assets/js/solution/_4/groups/index.js?v='.time()) }}"></script>
@endpush
