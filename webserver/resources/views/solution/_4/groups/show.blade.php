@extends('layouts/app')

@section('title', '好友群組內容')

@push('css')
<link href="{{ asset('assets/css/solution/_4/groups/show.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">

        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Group Info</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>好友群組內容</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            <div v-cloak id="group-content">

                <div class="card">
                    <div class="card-body">
                        <h3 class="d-flex align-items-center mb-3">
                            <span class="mr-3">@{{ '# '+group.id }}</span>
                            <span class="flex-grow-1 mr-4">@{{ group.name }}</span>
                            <span class="d-flex align-items-center small text-muted mb-0 mr-3 font-size-0750">
                                <v-svg class="svg mr-1" src="{{ asset('assets/image/svg/light/clock.svg') }}" width="14" height="14"></v-svg>
                                @{{ group.updated_at }}
                            </span>

                            <span id="sync-group" :class="[{loading : sync.is_loading}, 'd-flex align-items-center small mb-0 cursor-pointer']" @click="syncGroup">
                                <v-svg class="svg" src="{{ asset('assets/image/svg/regular/sync-alt.svg') }}" width="14" height="14"></v-svg>
                            </span>
                        </h3>

                        <p class="text-muted mb-4">@{{ group.description }}</p>

                        {{-- 人數占比 --}}
                        <div class="align-items-center mb-3">
                            <div class="align-items-center no-gutters">
                                <div class="small mb-2">@{{ '人數佔比：'+(group.total_users_count == 0 ? 0 : Math.floor(group.users_count*100/group.total_users_count))+'%（'+group.users_count+' / '+group.total_users_count+'）' }} </div>
                                <div class="col-12">
                                    <!-- Progress -->
                                    <div class="progress progress-sm">
                                        <div class="progress-bar" role="progressbar" :style="'width:'+Math.floor(group.users_count*100/group.total_users_count)+'%'"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @include('solution._4.groups.show_partials.group-condition')

                @for($i = 1; $i <= 3; $i ++)
                @include('solution._4.groups.show_partials.circles.'.$i)
                @endfor

                {{-- 群組條件 --}}
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-header-title">群組條件</h4>
                            </div>
                            <div :class="[{'p-0' : !group.circles}, 'card-body']">
                                <group-condition v-if="group.circles" :circles="group.circles" :tags="group.tags"></group-condition>

                                <ul v-else class="users-list list-group">
                                    <li class="list-group-item no-items">No conditions</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 新增＆移除 好友名單 --}}
                <div class="row">
                    <div v-for="type in ['add', 'remove']" class="col-6">
                        <div :id="type+'-users'" class="card">
                            <div class="card-header">
                                <h4 class="card-header-title">@{{ (type == 'add' ? '新增' : '移除') + '好友名單' }}</h4>
                            </div>
                            <div class="card-body p-0">
                                <ul class="users-list list-group">
                                    <li v-for="user in group[type+'Users']" class="list-group-item">
                                        <div class="row align-items-center flex-nowrap">
                                            <div class="col-auto">
                                                <img :src="user.picture_url" class="rounded-circle">
                                            </div>
                                            <div class="user-info col ml-n2 flex-nowrap">
                                                <h4 class="mb-1 name">
                                                    <a class="d-flex text-dark word-break-keep-all mb-3" :href="getUserUrl(user.id)" target="_blank">
                                                        <span class="mr-2">@{{ '#'+user.id }}</span>
                                                        <span class="overflow-hidden text-overflow-ellipsis">@{{ user.display_name }}</span>
                                                    </a>
                                                </h4>
                                                <p class="text-muted small mb-0 word-break-keep-all text-overflow-ellipsis overflow-hidden">@{{ user.status_message == '' ? '（好友未輸入狀態訊息）' : user.status_message }}</p>
                                            </div>
                                        </div>
                                    </li>

                                    <li v-if="group[type+'Users'].length == 0" class="list-group-item no-items">No items</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 好友群組內名單 --}}
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-header-title">好友群組內名單</h4>
                            </div>
                            <div class="card-body p-0">

                                <div :class="[users.is_loading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-bl-05 border-radius-br-05']">
                                    <div class="spinner-grow text-secondary" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>

                                <div id="search-from" class="pl-4 pr-4 p-2">
                                    {{-- Search --}}
                                    <form class="row align-items-center" @submit.prevent="searchUsers">
                                        <div class="col-auto pr-0">
                                            <span class="text-muted">
                                                <v-svg src="{{ asset('assets/image/svg/light/search.svg') }}" width="18" height="18"></v-svg>
                                            </span>
                                        </div>
                                        <div class="col">
                                            <input type="search" class="form-control form-control-flush search" placeholder="Search" v-model="users.query" @change="searchUsers">
                                        </div>
                                    </form>
                                </div>

                                <ul class="users-list list-group">
                                    <li v-for="users in group.users.chunk(3)" class="list-group-item">
                                        <div class="row align-items-center">
                                            <div v-for="user in users" class="col-4">
                                                <div class="row flex-nowrap">
                                                    <div class="col-auto">
                                                        <img :src="user.picture_url" class="rounded-circle">
                                                    </div>
                                                    <div class="user-info col ml-n2 flex-nowrap">
                                                        <h4 class="mb-1 name">
                                                            <a class="d-flex text-dark word-break-keep-all mb-3" :href="getUserUrl(user.id)" target="_blank">
                                                                <span class="mr-2">@{{ '#'+user.id }}</span>
                                                                <span class="overflow-hidden text-overflow-ellipsis">@{{ user.display_name }}</span>
                                                            </a>
                                                        </h4>
                                                        <p class="text-muted small mb-0 word-break-keep-all text-overflow-ellipsis overflow-hidden">@{{ user.status_message == '' ? '（好友未輸入狀態訊息）' : user.status_message }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li v-if="group.users.length == 0" class="list-group-item no-items">No items</li>
                                </ul>

                            </div>
                        </div>


                        <nav v-cloak v-if="group.paginator.lastPage > 1" class="d-flex justify-content-center" aria-label="Page navigation">
                            <ul class="pagination">
                                <li v-if="group.paginator.currentPage >= 4" class="page-item" @click="changePage(group.paginator.currentPage - 3)">
                                    <a class="page-link" href="javascript:void(0);" aria-label="Previous">
                                        <span aria-hidden="true">...</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>

                                <li v-for="i in group.paginator.currentPage + 2" v-if="i >= 1 && i <= group.paginator.lastPage && i >= group.paginator.currentPage - 2" :class="['page-item', {'active': i == group.paginator.currentPage }]" @click="changePage(i)">
                                    <a class="page-link" href="javascript:void(0);">@{{ i }}</a>
                                </li>

                                <li v-if="group.paginator.lastPage > 5 && group.paginator.currentPage < group.paginator.lastPage - 3" class="page-item" @click="changePage(group.paginator.currentPage + 3)">
                                    <a class="page-link" href="javascript:void(0);" aria-label="Next">
                                        <span aria-hidden="true">...</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>



                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
@endsection


@push('plugin-js')
@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
    group: {!! $group->toJson() !!},
}
</script>
<script src="{{ asset('assets/js/solution/_4/groups/show.js?v='.time()) }}"></script>
@endpush
