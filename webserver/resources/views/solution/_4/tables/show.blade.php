@extends('layouts/app')

@section('title', '資料表內容')

@push('plugin-css')
{{-- multiselect CSS --}}
<link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
@endpush

@push('css')
<link href="{{ asset('assets/css/solution/_4/tables/show.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">
        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Table Info</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center flex-grow-1">
                                <span>資料表內容</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            <div v-cloak id="table-content">
                <div class="card">
                    <div class="card-body">
                        <h3 class="d-flex align-items-center mb-3">
                            <span class="mr-3">@{{ '# '+table.id }}</span>
                            <span class="flex-grow-1">@{{ table.name }}</span>

                            {{-- 資料筆數 --}}
                            <span class="data-counts position-relative small mr-3 pr-3">@{{ '資料筆數：'+table.dataCounts }}</span>

                            {{-- 類型 --}}
                            <span class="badge badge-soft-secondary">@{{ type[table.type] }}</span>

                            <div class="dropdown d-flex ml-3">
                                <a href="#" class="d-inline-flex align-items-center cursor-pointer dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <v-svg class="svg text-muted" src="{{ asset('assets/image/svg/light/ellipsis-v.svg') }}" width="22" height="22"></v-svg>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right p-0">
                                    <a href="javascript:void(0);" class="dropdown-item" @click="remove(table.id)">刪除資料表</a>
                                </div>
                            </div>
                        </h3>

                        <p class="text-muted m-0">@{{ table.description }}</p>

                    </div>

                    <div v-if="table.type == 'coupon'" class="d-flex align-items-center card-footer">
                        <span class="small line-height-initial mr-1">抽獎規則：</span>
                        <span class="badge badge-soft-secondary mr-2">@{{ table.settings.random ? '隨機領取' : '依序領取' }}</span>
                        <span class="badge badge-soft-secondary mr-2">@{{ table.settings.limit.active ? '領取數量限制：'+table.settings.limit.count : '無領取數量限制' }}</span>
                        <span class="badge badge-soft-secondary">@{{ table.settings.issueDatetime.active ? '領取時間限制：'+table.settings.issueDatetime.date.from+' ~ '+table.settings.issueDatetime.date.to+' , '+table.settings.issueDatetime.time.from+' ~ '+table.settings.issueDatetime.time.to : '無領取時間限制' }}</span>
                    </div>
                </div>


                {{-- 資料表 --}}
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">

                                {{-- Search --}}
                                <form class="row align-items-center" @submit.prevent="search">
                                    <div class="col-auto pr-0">
                                        <span class="text-muted">
                                            <v-svg src="{{ asset('assets/image/svg/light/search.svg') }}" width="18" height="18"></v-svg>
                                        </span>
                                    </div>
                                    <div class="col">
                                        <input type="search" class="form-control form-control-flush search" placeholder="Search" v-model="query">
                                    </div>
                                </form>

                            </div>
                            <div class="col-auto">

                                {{-- Button --}}

                                <div class="dropdown">
                                    <button class="btn btn-sm btn-white" type="button" id="bulkActionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">批次動作</button>
                                    <div class="dropdown-menu dropdown-menu-right font-size-0875" aria-labelledby="bulkActionDropdown">
                                        <!-- <a class="dropdown-item" href="javascript:void(0);">重新刊登（刪除wp文章）</a>
                                        <a class="dropdown-item" href="javascript:void(0);" @click="repost">重新刊登（不刪除wp文章）</a>
                                        <a class="dropdown-item" href="javascript:void(0);" @click="ignore">忽略文章</a>
                                        <a class="dropdown-item" href="javascript:void(0);">刪除wp文章（不重新刊登）</a> -->
                                    </div>
                                </div>

                            </div>
                        </div> {{-- / .row --}}
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive position-relative">

                            <div :class="[is_loading ? 'd-flex' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-bl-05 border-radius-br-05 z-index-1']">
                                <div class="spinner-grow text-secondary" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>

                            <table class="table table-sm table-nowrap card-table text-left">
                                <thead>
                                    <tr>
                                        <th v-for="column in table.columnList" class="text-transform-none">@{{ column }}</th>
                                    </tr>
                                </thead>
                                <tbody class="list">

                                    <tr v-for="row in rows">
                                        <td v-for="column in table.columnList" :class="[{'text-muted' : row[column] === null}]">@{{ row[column] === null ? 'NULL' : row[column] }}</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <nav v-cloak v-if="paginator.lastPage > 1" class="d-flex justify-content-center" aria-label="Page navigation">
                    <ul class="pagination">
                        <li v-if="paginator.currentPage >= 4" class="page-item" @click="changePage(paginator.currentPage - 3)">
                            <a class="page-link" href="javascript:void(0);" aria-label="Previous">
                                <span aria-hidden="true">...</span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </li>

                        <li v-for="i in paginator.currentPage + 2" v-if="i >= 1 && i <= paginator.lastPage && i >= paginator.currentPage - 2" :class="['page-item', {'active': i == paginator.currentPage }]" @click="changePage(i)">
                            <a class="page-link" href="javascript:void(0);">@{{ i }}</a>
                        </li>

                        <li v-if="paginator.currentPage <= paginator.lastPage - 3" class="page-item" @click="changePage(paginator.currentPage + 3)">
                            <a class="page-link" href="javascript:void(0);" aria-label="Next">
                                <span aria-hidden="true">...</span>
                                <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav>







            </div>
        </div>

    </div>
</div>
@endsection


@push('plugin-js')

@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
    table: {!! $table->toJson() !!}
}
</script>
<script src="{{ asset('assets/js/solution/_4/tables/show.js?v='.time()) }}"></script>
@endpush
