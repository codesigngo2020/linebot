<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>兌換優惠券｜Solution</title>
    {{-- Google Font Nunito --}}
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,400i,600,600i" rel="stylesheet">
    <!-- stack('plugin-css') -->
    {{-- Theme CSS --}}
    <link href="{{ asset('assets/plugins/theme/theme.min.css?v='.time()) }}" rel="stylesheet">
    {{-- App CSS --}}
    <link href="{{ asset('css/app.css?v='.time()) }}" rel="stylesheet">
    <!-- stack('css') -->
</head>
<body class="p-5">

    <div id="app">

        <div class="row mb-5">
            <div class="col text-center">
                <!-- Logo -->
                <img src="{{ asset('assets/image/logo.svg') }}" alt="..." class="img-fluid mb-4" style="max-width: 2.5rem;">

                <!-- Title -->
                <h2 class="mb-2">{{ $res['success'] ? 'Successfully Redeemed' : 'Redemption Failed' }}</h2>

                @if($res['success'])
                <!-- Text -->
                <p class="d-inline-flex text-muted mb-0">
                    {{ 'Coupon #'.$res['coupon']->code }}
                    <span class="d-inline-flex align-items-center ml-2">
                        <v-svg class="text-success" src="{{ asset('assets/image/svg/regular/check-circle.svg') }}" width="16" height="16"></v-svg>
                    </span>
                </p>
                @else
                <p class="d-inline-flex text-muted mb-0">
                    {{ $res['message'] }}
                    <span class="d-inline-flex align-items-center ml-2">
                        <v-svg class="text-danger" src="{{ asset('assets/image/svg/regular/exclamation-triangle.svg') }}" width="16" height="16"></v-svg>
                    </span>
                </p>
                @endif

            </div>
        </div>

        @if($res['success'])

        {{-- 好友資訊 --}}
        <div class="mb-5">

            <h6 class="text-uppercase text-muted">User Info</h6>

            <div class="d-flex">

                <div class="mr-3">
                    <span class="profile-picture">
                        <img src="{{ $res['user']->picture_url }}" class="rounded-circle" style="width:45px; height:45px;">
                    </span>
                </div>

                <div class="d-flex flex-column justify-content-center overflow-hidden">
                    <strong class="mb-0">
                        <span class="mr-2">{{ '#'.$res['user']->id }}</span>
                        <span>{{ $res['user']->display_name }}</span>
                    </strong>
                    <p class="text-overflow-ellipsis overflow-hidden text-muted small mb-0">{{ $res['user']->user_id }}</p>
                </div>

            </div>

        </div>

        {{-- 優惠券資訊 --}}
        <div>

            <h6 class="text-uppercase text-muted">Coupon Info</h6>

            <div class="overflow-hidden mb-3">
                <strong class="mb-0">
                    <span class="mr-2">{{ '#'.$res['coupon']->id }}</span>
                    <span>{{ $res['coupon']->name }}</span>
                </strong>
            </div>

            <div class="d-flex justify-content-between mb-3">
                <div>
                    <h6 class="text-uppercase text-muted">Issued Date</h6>
                    <p class="small mb-0">{{ $res['coupon']->issued_at }}</p>
                </div>
                <div class="text-right">
                    <h6 class="text-uppercase text-muted">Redeemed Date</h6>
                    <p class="small mb-0">{{ $res['coupon']->redeemed_at }}</p>
                </div>
            </div>

            <div>
                <h6 class="text-uppercase text-muted">Staff Member</h6>
                <p class="small">
                    <span class="mr-2">{{ '#'.auth()->user()->id }}</span>
                    <span>{{ auth()->user()->name }}</span>
                </p>
            </div>

        </div>

        @else

        @endif

        <hr class="my-5">

        <h6 class="text-uppercase">Notes</h6>

        <p class="text-muted small mb-0">
            優惠券僅限使用一次。<br>兌換紀錄以及優惠券相關資料都在對應的「優惠券資料表」有完整呈現。
        </p>

    </div>











    <!-- include('layouts.navbars.main') -->

    <!-- yield('content') -->

    {{-- jQuery --}}
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/plugins/bootstrap/bootstrap.bundle.min.js') }}"></script>

    {{-- vue.js --}}
    {{--<script src="https://cdn.jsdelivr.net/npm/vue"></script>--}}
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    @routes
    <!-- stack('plugin-js') -->

    {{-- Theme JS --}}
    <script src="{{ asset('assets/plugins/theme/theme.min.js?v='.time()) }}"></script>

    {{-- App JS --}}
    <script src="{{ asset('js/app.js?v='.time()) }}"></script>

    <!-- stack('js') -->
    <script src="{{ asset('assets/js/solution/_4/tables/redeem.js?v='.time()) }}"></script>
</body>
</html>
