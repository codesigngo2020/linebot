@extends('layouts/app')

@section('title', '自動回覆內容')

@push('css')
<link href="{{ asset('assets/css/solution/_4/basic/message/message/show.css?v='.time()) }}" rel="stylesheet">
<link href="{{ asset('assets/css/solution/_4/auto-answers/show.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">

        @include('components.alerts.main')

        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Auto-Answer Info</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>自動回覆內容</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            <div v-cloak id="auto-answer-content">

                <div class="card">
                    <div class="card-body">
                        <h3 class="d-flex align-items-center mb-3">
                            <span class="mr-3">@{{ '# '+autoAnswer.id }}</span>
                            <span class="mr-4">@{{ autoAnswer.name }}</span>

                            <a class="d-inline-flex" :href="getTagUrl(autoAnswer.tag_id)" target="_blank">
                                <v-svg :class="[tagId == autoAnswer.tag_id ? 'text-2451010' : 'text-warning', 'svg']" src="{{ asset('assets/image/svg/solid/tags.svg') }}" width="16" height="16"></v-svg>
                            </a>

                            <span class="position-relative active-types d-flex justify-content-end align-items-center flex-grow-1 mr-3 pr-3 small">
                                <span v-for="type in types" v-if="type.active" class="position-relative ml-3 font-size-0875"><span class="badge-soft-secondary badge">@{{ type.name }}</span></span>
                            </span>

                            <span class="d-flex align-items-center small">
                                <span :class="[autoAnswer.active ? 'text-success' : 'text-warning', 'active-dot font-size-0750 mr-2']">●</span>
                                <span>@{{ autoAnswer.active ? 'Active' : 'Inactive' }}</span>
                            </span>
                        </h3>

                        <p class="text-muted mb-4">@{{ autoAnswer.description }}</p>

                        {{-- 推播訊息人數占比 --}}
                        <div class="align-items-center mb-3">
                            <div class="align-items-center no-gutters">
                                <div class="small mb-2">@{{ '收到自動回覆人數佔比：'+(autoAnswer.total_users_count == 0 ? 0 : Math.floor(autoAnswer.users_count*100/autoAnswer.total_users_count))+'%（'+autoAnswer.users_count+' / '+autoAnswer.total_users_count+'）' }} </div>
                                <div class="col-12">
                                    <!-- Progress -->
                                    <div class="progress progress-sm">
                                        <div class="progress-bar" role="progressbar" :style="'width:'+Math.floor(autoAnswer.users_count*100/autoAnswer.total_users_count)+'%'"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-header-title">有效時間區段</h4>
                    </div>
                    <div class="card-body p-0">

                        <div class="row">
                            <div id="periods-chart" class="col-12 col-lg-auto d-flex align-items-center flex-grow-1">
                                <div class="d-flex flex-column w-100 pl-4 pr-5 mr-3 mr-lg-0 pt-5 pb-5 pb-lg-6">
                                    <div v-for="(period, periodIndex) in autoAnswer.periods" class="mb-3" :style="'transform: translateX('+(period.offset * 100 / periodsChart.diffDays)+'%);'">
                                        <div :class="['bg-'+periodsChart.backgrounds[periodIndex], 'progress-bar']" :style="'width: '+( period.days == 0 ? '.25rem' : (period.days * 100 / periodsChart.diffDays) + '%' )+';'"></div>
                                    </div>

                                    <div id="time-bar" class="mt-4">
                                        <div  v-for="(label, labelIndex) in periodsChart.labels" class="datetime" :style="'left: '+(100 * labelIndex / (periodsChart.labels.length - 1))+'%'">
                                            <span>@{{ label }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr class="d-lg-none w-100 mt-5 mb-5">

                            <div id="periods-info" class="col-12 col-lg-auto font-size-0750">
                                <div class="d-flex flex-lg-column flex-wrap pl-4 pl-lg-0 pt-0 pt-lg-4 pb-4 pr-4">
                                    <div v-for="(period, periodIndex) in autoAnswer.periods" :class="[{'mt-3' : periodIndex != 0}, 'd-flex flex-lg-column mr-4 mr-lg-0']">
                                        <div class="d-flex align-items-center">
                                            <div :class="['bg-'+periodsChart.backgrounds[periodIndex], 'progress-bar border-radius-05 mr-3']" style="width: 1.5rem;"></div>
                                            <div>@{{ period.start_date.substr(0,10) + '～' + ( period.end_date ? period.end_date.substr(0,10) : '' ) }}</div>
                                        </div>
                                        <div class="d-flex align-items-center text-secondary ml-4 ml-lg-5">@{{ period.start_time + '～' + period.end_time }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                @if($autoAnswer->template_id)

                <div class="text-right">
                    <a href="{{ route('solution.4.deployment.messagesPool.show', [$deployment, $autoAnswer->template->id]) }}" class="d-inline-flex align-items-center btn btn-secondary" target="_blank">
                        <span class="mr-3">前往訊息模組</span>
                        <v-svg class="svg" src="{{ asset('assets/image/svg/regular/long-arrow-right.svg') }}" width="18" height="18"></v-svg>
                    </a>
                </div>

                @else

                @include('components.basic.solution._4.message.messages')
                @include('components.basic.solution._4.message.message-preview')
                @include('components.basic.solution._4.message.action-list')

                <message-preview :tagid="tagId" :messages="autoAnswer.messages" :messagesformdata="autoAnswer.messages_form_data" :tagsdata="autoAnswer.tagsData" :userscount="autoAnswer.total_users_count" :showactions="true" :showtags="true"></message-preview>

                @endif

            </div>
        </div>

    </div>
</div>
@endsection


@push('plugin-js')
@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
    autoAnswer: {!! $autoAnswer->toJson() !!},
    tagId: {{ request()->input('tagId') ?? 0 }},
}
</script>
<script src="{{ asset('assets/js/solution/_4/auto-answers/show.js?v='.time()) }}"></script>
@endpush
