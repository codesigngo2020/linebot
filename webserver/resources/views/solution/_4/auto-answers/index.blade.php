@extends('layouts/app')

@section('title', '自動回覆列表')

@push('css')
<link href="{{ asset('assets/css/solution/_4/auto-answers/index.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">

        @include('components.alerts.main')

        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Auto-Answer List</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>自動回覆列表</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                        <div id="new-auto-answer" class="col-auto mr-3 mr-sm-0">
                            {{-- Button --}}
                            <a href="{{ route('solution.4.deployment.autoAnswers.create', $deployment) }}" class="btn btn-primary d-none d-sm-block">新增自動回覆</a>
                            <a href="{{ route('solution.4.deployment.autoAnswers.create', $deployment) }}" class="btn btn-transparent nopadding rounded-circle d-block d-sm-none">
                                <span>
                                    <v-svg class="svg d-flex text-primary" src="{{ asset('assets/image/svg/light/plus-circle.svg') }}" width="22" height="22"></v-svg>
                                </span>
                            </a>
                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            @include('components.tables.solution._4.auto-answers.calendar')

            <div v-cloak id="auto-answers-table">
                <div class="mb-3">
                    <div class="row">
                        <div v-if="type == 'list' && currentAutoAnswer" class="col-12 col-xl-8">
                            <div class="card flex-grow-1 mb-0">
                                <div class="card-body p-0">
                                    <div class="row nomargin">
                                        <div class="col ml-n2 pt-4 pb-4 pl-4 pr-4">

                                            <!-- Title -->
                                            <p class="card-title d-flex mb-3">
                                                <span class="h4 mb-0 mr-3">@{{ '#'+currentAutoAnswer.id }}</span>
                                                <a class="h4 flex-grow-1 mb-0 text-dark" :href="getUrl(currentAutoAnswer.id)">@{{ currentAutoAnswer.name }}</a>
                                                <span class="tags_count position-relative small mr-3 pr-3">
                                                    <span class="mr-1">
                                                        <v-svg class="svg text-warning" src="{{ asset('assets/image/svg/solid/tags.svg') }}" width="16" height="16"></v-svg>
                                                    </span>
                                                    <span>@{{ 'x '+currentAutoAnswer.tags_count }}</span>
                                                </span>
                                                <span class="d-flex small">
                                                    <span :class="[currentAutoAnswer.active ? 'text-success' : 'text-warning', 'font-size-0750 mr-1']">●</span>
                                                    <span>@{{ currentAutoAnswer.active ? 'Active' : 'Inactive' }}</span>
                                                </span>
                                            </p>

                                            <!-- Text -->
                                            <p class="card-text small text-muted mb-2">@{{ currentAutoAnswer.description }}</p>

                                            <!-- percentage -->
                                            <div class="align-items-center">
                                                <div class="row align-items-center no-gutters">
                                                    <div class="col-auto">
                                                        <div class="small mr-2">@{{ (currentAutoAnswer.users_percentage ? currentAutoAnswer.users_percentage : 0)+'%' }}</div>
                                                    </div>
                                                    <div class="col">
                                                        <!-- Progress -->
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar" role="progressbar" :style="'width:'+currentAutoAnswer.users_percentage+'%'"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="message-content p-2">
                                            <ul class="list-group">
                                                <li v-for="messageContent in currentAutoAnswer.messages" :class="[{'has-quick-replay' : messageContent.hasQuickReplay}, 'list-group-item']">@{{ messageContent.type }}</li>
                                            </ul>
                                        </div>
                                    </div> <!-- / .row -->
                                </div> <!-- / .card-body -->
                            </div>
                        </div>



                        <div class="col-auto d-flex justify-content-end align-items-end flex-grow-1">
                            <div class="btn-group">
                                <button type="button" :class="[{active : type == 'calendar'}, 'type btn btn-outline-secondary border-right-0']" @click="changeType('calendar')">
                                    <v-svg class="svg" src="{{ asset('assets/image/svg/light/calendar-alt.svg') }}" width="18" height="18"></v-svg>
                                </button>
                                <button type="button" :class="[{active : type == 'list'}, 'type btn btn-outline-secondary border-left-0']" @click="changeType('list')">
                                    <v-svg class="svg" src="{{ asset('assets/image/svg/regular/list.svg') }}" width="18" height="18"></v-svg>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <hr v-if="type == 'list' && currentAutoAnswer" class="mt-5 mb-5">

                @include('components.tables.solution._4.auto-answers.table')

                <auto-answer-calendar v-if="type == 'calendar'"></auto-answer-calendar>
                <auto-answer-list v-if="type == 'list'"></auto-answer-list>


            </div>
        </div>









    </div>
</div>
@endsection


@push('plugin-js')
@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
    currentAutoAnswer: {!! json_encode($currentAutoAnswer) !!},
}
</script>
<script src="{{ asset('assets/js/solution/_4/auto-answers/index.js?v='.time()) }}"></script>
@endpush
