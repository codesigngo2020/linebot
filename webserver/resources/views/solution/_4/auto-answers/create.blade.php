@extends('layouts/app')

@section('title', '新增自動回覆')

@push('plugin-css')
{{-- multiselect CSS --}}
<link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
@endpush

@push('css')
<link href="{{ asset('assets/css/solution/_4/basic/message/message/create.css?v='.time()) }}" rel="stylesheet">
<link href="{{ asset('assets/css/solution/_4/auto-answers/create.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">

        @include('components.alerts.main')

        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Create Auto-Answer</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>新增自動回覆</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            @include('components.forms.solution._4.auto-answers.create')


        </div>
    </div>
</div>
@endsection


@push('plugin-js')
<script src="{{ asset('assets/plugins/dropzone/dropzone.min.js') }}"></script>
{{-- multiselect JS --}}
<script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
<script src="{{ asset('assets/components/select/select.js') }}"></script>
{{-- jQuery mask JS --}}
<script src="{{ asset('assets/plugins/mask/jquery.mask.js') }}"></script>
@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
}
</script>
<script src="{{ asset('assets/js/solution/_4/auto-answers/create.js?v='.time()) }}"></script>
@endpush
