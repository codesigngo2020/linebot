@extends('layouts/app')

@section('title', 'IAM')

@push('plugin-css')
{{-- multiselect CSS --}}
<link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
@endpush

@push('css')
<link href="{{ asset('assets/css/solution/_4/iam-admin/index.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">
        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">IAM</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>IAM</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            <div v-cloak id="users-table">
                <div class="d-flex align-items-end mb-4">
                    <div class="flex-grow-1">
                        <h3 class="mb-3">「{{ $deployment->name }}」部署權限</h3>
                        <span class="text-secondary font-size-0875">賦予平台使用者角色，即可執行角色權限。</span>
                    </div>
                    <a class="btn btn-outline-primary btn-sm d-flex align-items-center" href="javascript:void(0);" data-toggle="modal" data-target="#user-adding">
                        <v-svg class="svg mr-2" src="{{ asset('assets/image/svg/light/user-plus.svg') }}" width="18" height="18"></v-svg>
                        新增
                    </a>
                </div>

                {{-- 新增使用者modal --}}

                <div id="user-adding" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="userAdding" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">新增使用者</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <h4>使用者 Email</h4>
                                    <input id="account" type="text" :class="[{'is-invalid' : addUserForm.account.invalid}, 'form-control pr-6']" v-model="addUserForm.account.account" placeholder="請輸入 Email">
                                    <span class="invalid-feedback font-size-0750" role="alert"><strong>@{{ addUserForm.account.info }}</strong></span>
                                </div>

                                <div class="form-group">
                                    <h4>角色</h4>
                                    <vue-multiselect :class="[{'is-invalid' : addUserForm.roles.invalid}, 'hidden-arrow']" v-model="addUserForm.roles.value" track-by="value" label="name" placeholder="請選擇角色" open-direction="bottom" :options="roles.options" :searchable="false" :show-labels="false" :allow-empty="false" :multiple="true"></vue-multiselect>
                                    <span class="invalid-feedback font-size-0750" role="alert"><strong>@{{ addUserForm.roles.info }}</strong></span>
                                </div>

                                <div>
                                    <button type="button" :class="[{'progress-bar-striped progress-bar-animated' : addUserFormSumitting}, 'btn btn-primary text-center w-100']" @click="submitAddUserForm">送出</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @include('components.tables.solution._4.iam-admin.table')

                {{-- 編輯權限modal --}}

                <div id="role-editting" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="roleEditting" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">編輯權限</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div v-if="editForm.user" class="modal-body">
                                <h4>成員</h4>
                                <div class="text-secondary small mb-4">@{{ editForm.user.name+'（'+editForm.user.email+'）' }}</div>

                                <div class="form-group">
                                    <h4>角色</h4>
                                    <vue-multiselect :class="[{'is-invalid' : editForm.roles.invalid}, 'hidden-arrow']" v-model="editForm.roles.value" track-by="value" label="name" placeholder="請選擇角色" open-direction="bottom" :options="roles.options" :searchable="false" :show-labels="false" :allow-empty="false" :multiple="true"></vue-multiselect>
                                    <span class="invalid-feedback font-size-0750" role="alert"><strong>@{{ editForm.roles.info }}</strong></span>
                                </div>

                                <div>
                                    <button type="button" :class="[{'progress-bar-striped progress-bar-animated' : editFormSumitting}, 'btn btn-primary text-center w-100']" @click="submitEditFrom">送出</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <nav v-cloak v-if="paginator.lastPage > 1" class="d-flex justify-content-center" aria-label="Page navigation">
                    <ul class="pagination">
                        <li v-if="paginator.currentPage >= 4" class="page-item" @click="changePage(paginator.currentPage - 3)">
                            <a class="page-link" href="javascript:void(0);" aria-label="Previous">
                                <span aria-hidden="true">...</span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </li>

                        <li v-for="i in paginator.currentPage + 2" v-if="i >= 1 && i <= paginator.lastPage && i >= paginator.currentPage - 2" :class="['page-item', {'active': i == paginator.currentPage }]" @click="changePage(i)">
                            <a class="page-link" href="javascript:void(0);">@{{ i }}</a>
                        </li>

                        <li v-if="paginator.currentPage <= paginator.lastPage - 3" class="page-item" @click="changePage(paginator.currentPage + 3)">
                            <a class="page-link" href="javascript:void(0);" aria-label="Next">
                                <span aria-hidden="true">...</span>
                                <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav>

            </div>
        </div>









    </div>
</div>
@endsection


@push('plugin-js')
{{-- multiselect JS --}}
<script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
<script src="{{ asset('assets/components/select/select.js') }}"></script>
@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
    users: {!! $users->toJson() !!},
    paginator : {!! $paginator->toJson() !!},
    roles: {!! $roles->toJson() !!},
}
</script>
<script src="{{ asset('assets/js/solution/_4/iam-admin/index.js?v='.time()) }}"></script>
@endpush
