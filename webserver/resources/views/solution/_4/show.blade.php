@extends('layouts/app')

@section('title', '機器人儀表板')

@push('css')
<link href="{{ asset('assets/css/solution/_4/show.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">
        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Chatbot Dashboard</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>機器人儀表板</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>
        </div>

        <div v-cloak id="deployment-content">

            <div class="row lex-wrap-nowrap overflow-auto nomargin">
                <div class="col pl-0">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center flex-wrap-nowrap">
                                <div class="col">
                                    <h6 class="card-title text-uppercase text-muted mb-2 white-space-nowrap">好友總數</h6>
                                    <div class="d-flex align-items-center">
                                        <span class="h2 mb-0 mr-3">@{{ statistics.followers_count }}</span>
                                        <span class="h6 mb-0 text-primary">@{{ statistics.followers_count - statistics.blocks_count }}</span>
                                        <span class="h6 mb-0 ml-2 mr-2">/</span>
                                        <span class="h6 mb-0 text-warning mr-3">@{{ statistics.blocks_count }}</span>
                                        <span :class="[statistics.followers_count_rate >= 0 ? 'badge-soft-success' : 'badge-soft-warning' ,'badge']">@{{ statistics.followers_count_rate >= 0 ? '+'+statistics.followers_count_rate+'%' : statistics.followers_count_rate+'%' }}</span>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    {{-- Icon --}}
                                    <span class="h2 text-muted mb-0">
                                        <v-svg class="svg" src="{{ asset('assets/image/svg/light/users.svg') }}" width="26" height="26"></v-svg>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center flex-wrap-nowrap">
                                <div class="col">
                                    <h6 class="card-title text-uppercase text-muted mb-2 white-space-nowrap">活躍好友人數</h6>
                                    <div class="d-flex align-items-center">
                                        <span class="h2 mb-0 mr-3">@{{ statistics.active_users_count }}</span>
                                        <span :class="[statistics.active_users_count_rate >= 0 ? 'badge-soft-success' : 'badge-soft-warning' ,'badge']">@{{ statistics.active_users_count_rate >= 0 ? '+'+statistics.active_users_count_rate+'%' : statistics.active_users_count_rate+'%' }}</span>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    {{-- Icon --}}
                                    <span class="h2 text-muted mb-0">
                                        <v-svg class="svg" src="{{ asset('assets/image/svg/light/user-clock.svg') }}" width="26" height="26"></v-svg>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center flex-wrap-nowrap">
                                <div class="col">
                                    <h6 class="card-title text-uppercase text-muted mb-2 white-space-nowrap">推波訊息總數（預估）</h6>
                                    <div class="d-flex align-items-center">
                                        <span class="h2 mb-0 mr-3">@{{ statistics.broadcasts_count + statistics.pushes_count + statistics.multicasts_count }}</span>
                                        <span :class="[statistics.broadcasts_count_rate >= 0 ? 'badge-soft-success' : 'badge-soft-warning' ,'badge']">@{{ statistics.broadcasts_count_rate >= 0 ? '+'+statistics.broadcasts_count_rate+'%' : statistics.broadcasts_count_rate+'%' }}</span>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    {{-- Icon --}}
                                    <span class="h2 fe fe-briefcase text-muted mb-0">
                                        <v-svg class="svg" src="{{ asset('assets/image/svg/light/bullhorn.svg') }}" width="26" height="26"></v-svg>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col pr-0">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center flex-wrap-nowrap">
                                <div class="col">
                                    <h6 class="card-title text-uppercase text-muted mb-2 white-space-nowrap">本月推波訊息總數</h6>
                                    <div class="d-flex align-items-center">
                                        <span class="h2 mb-0 mr-3">@{{ statistics.this_month_broadcasts_count }}</span>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    {{-- Icon --}}
                                    <span class="h2 fe fe-briefcase text-muted mb-0">
                                        <v-svg class="svg" src="{{ asset('assets/image/svg/light/bullhorn.svg') }}" width="26" height="26"></v-svg>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                {{-- 三天內推播訊息 --}}
                <div id="broadcast-calendar" class="col-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-header-title">推播訊息</h4>
                            <div class="flex-initial">
                                <a href="{{ route('solution.4.deployment.broadcasts.index', $deployment) }}" class="btn btn-sm btn-white font-size-0750">more</a>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <table class="table card-table table-nowrap">
                                <tbody>
                                    <tr>
                                        <th v-for="(date, dateIndex) in dates">
                                            <div>
                                                <div class="date">@{{ dateIndex == 0 ? date.month + ' 月 ' : '' }}<span :class="[{today : dateIndex == 0}]">@{{ date.day }}</span>@{{' 日' }}</div>
                                                <div>
                                                    <div v-if="!showAll.broadcasts" class="broadcasts">
                                                        <a v-for="broadcast in date.broadcasts.slice(0,4)" class="badge badge-soft-secondary" :href="getUrl('broadcast', broadcast.id)" target="_blank">@{{ broadcast.name }}</a>
                                                    </div>
                                                    <div v-if="showAll.broadcasts" class="broadcasts">
                                                        <a v-for="broadcast in date.broadcasts" class="badge badge-soft-secondary" :href="getUrl('broadcast', broadcast.id)" target="_blank">@{{ broadcast.name }}</a>
                                                    </div>
                                                </div>
                                                <div v-if="!showAll.broadcasts && date.broadcasts.length > 4" class="more text-muted" @click="setShowAll('broadcasts', dateIndex)">...</div>
                                            </div>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                {{-- 三天內自動回覆訊息 --}}
                <div id="auto-answer-calendar" class="col-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-header-title">自動回覆訊息</h4>
                            <div class="flex-initial">
                                <a href="{{ route('solution.4.deployment.autoAnswers.index', $deployment) }}" class="btn btn-sm btn-white font-size-0750">more</a>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <table class="table card-table table-nowrap">
                                <tbody>
                                    <tr>
                                        <th v-for="(date, dateIndex) in dates">
                                            <div>
                                                <div class="date">@{{ dateIndex == 0 ? date.month + ' 月 ' : '' }}<span :class="[{today : dateIndex == 0}]">@{{ date.day }}</span>@{{' 日' }}</div>
                                                <div v-if="!showAll.autoAnswers" class="auto-answers">
                                                    <a v-for="autoAnswer in date.autoAnswers.slice(0,4)" :class="[{'space' : autoAnswer.index == -1}, {ml : !autoAnswer.b[0]}, {mr : !autoAnswer.b[1]}, 'badge badge-soft-info']" :href="autoAnswer.index == -1 ? 'javascript:void(0);' : getUrl('autoAnswer', autoAnswers[autoAnswer.index].id)" :target="autoAnswer.index == -1 ? '_self' : '_blank'">@{{ autoAnswer.index != -1 && autoAnswer.b[0] == 0 ? autoAnswers[autoAnswer.index].name : '&nbsp;' }}</a>
                                                </div>
                                                <div v-if="showAll.autoAnswers" class="auto-answers">
                                                    <a v-for="autoAnswer in date.autoAnswers" :class="[{'space' : autoAnswer.index == -1}, {ml : !autoAnswer.b[0]}, {mr : !autoAnswer.b[1]}, 'badge badge-soft-info']" :href="autoAnswer.index == -1 ? 'javascript:void(0);' : getUrl('autoAnswer', autoAnswers[autoAnswer.index].id)" :target="autoAnswer.index == -1 ? '_self' : '_blank'">@{{ autoAnswer.index != -1 && autoAnswer.b[0] == 0 ? autoAnswers[autoAnswer.index].name : '&nbsp;' }}</a>
                                                </div>
                                                <div v-if="!showAll.autoAnswers && date.autoAnswers.length > 4" :class="[{'cursor-pointer' : dateIndex == 1}, 'more text-muted']" @click="dateIndex == 1 ? setShowAll('autoAnswers', dateIndex) : null">@{{ dateIndex == 1 ? '...' : '' }}</div>
                                            </div>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                {{-- 好友圖表 --}}
                <div v-for="(chart, chartIndex) in charts" :class="'col-'+chart.col">

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-header-title">@{{ chart.cardName }}</h4>

                            <div v-if="chart.showTypesBtn" class="d-flex justify-content-end flex-grow-1">
                                <div v-if="chart.usersType" class="d-flex">
                                    <div class="btn-group">
                                        <button v-for="(type, typeKey) in chart.usersTypes" type="button" :class="[{active : chart.usersType == typeKey}, {'border-left-0 border-right-0' : typeKey == 'followers'}, 'type btn btn-outline-secondary pl-3 pr-3 p-1 font-size-0750']" @click="changeChartUserType(chartIndex, typeKey)">
                                            @{{ type.name }}
                                        </button>
                                    </div>
                                </div>

                                <div v-if="chart.usersType" class="d-flex align-items-center text-muted ml-4 mr-4">/</div>

                                <div class="d-flex">
                                    <div class="btn-group">
                                        <button v-for="(type, typeKey) in chart.types" type="button" :class="[{active : chart.type == typeKey}, {'border-left-0 border-right-0' : typeKey == 'followers'}, 'type btn btn-outline-secondary pl-3 pr-3 p-1 font-size-0750']" @click="changeChartType(chartIndex, typeKey)">
                                            @{{ type.name }}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-body position-relative">

                            <div :class="[chart.isLoading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute top left justify-content-center align-items-center h-100 w-100 bg-secondary']">
                                <div class="spinner-grow text-secondary" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>

                            <div class="chart" :style="'height: '+chart.height+'px;'">
                                <canvas :id="chart.id" class="chart-canvas"></canvas>
                            </div>

                        </div>
                        <div v-if="chart.cardFooter && chart.data.length > 0" class="card-footer d-flex flex-wrap pl-6 small">
                            <div v-for="(data, dataIndex) in chart.data" class="d-flex align-items-center mr-4">
                                <div role="progressbar" class="progress-bar border-radius-05 mr-3" :style="'width: 20px;height: 4px;background-color: '+borderColors[dataIndex]"></div>
                                <a class="text-dark" :href="getUrl(chart.urlPrefix, data.id)">@{{ data.name }}</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    @endsection


    @push('plugin-js')
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
    @endpush


    @push('js')
    <script>
    var args = {
        _token: "{{ csrf_token() }}",
        deployment: {{ $deployment->id }},
        statistics: {!! $statistics->toJson() !!},
        broadcasts: {!! $broadcasts->toJson() !!},
        autoAnswers: {!! $autoAnswers->toJson() !!},
    }
</script>
<script src="{{ asset('assets/js/solution/_4/show.js?v='.time()) }}"></script>
@endpush
