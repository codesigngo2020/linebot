@extends('layouts/app')

@section('title', '聊天室')

@push('css')
<link href="{{ asset('assets/css/solution/_4/basic/chatroom/index.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">
        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Chatroom</h6>

                            {{-- Title --}}
                            <h1 class="header-title d-flex align-items-center">
                                <span>聊天室</span>
                                <span class="ml-2 mr-2">-</span>
                                <span class="h2 nomargin">{{ $solution->name }}</span>
                            </h1>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            @foreach(['sidebar', 'users', 'dialogs', 'window'] as $view)
            @include('components.basic.solution._4.chatroom.'.$view)
            @endforeach

            @include('components.basic.solution._4.chatroom.messages')

            <div class="alert alert-light alert-dismissible fade show" role="alert">
                除了對「全部好友」發送的推播訊息，其餘訊息都會即時顯示。
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div v-cloak id="chatroom" class="d-flex border-radius-05 overflow-hidden bg-white mb-5">
                <chatroom-sidebar :type="listType" @change-list-type="changeListType"></chatroom-sidebar>

                <div id="chatroom-list">
                    <chatroom-users v-show="listType == 'users'" :active-user="user" :ws-response="wsResponse.getUsers" :webhook="wsResponse.webhook" @update-user="updateUser" @send-to-websocket="sendToWebsocket"></chatroom-users>
                    <chatroom-dialogs v-show="listType == 'dialogs'" :active-user="user" :ws-response="wsResponse.getDialogs" :webhook="wsResponse.webhook" @update-user="updateUser" @send-to-websocket="sendToWebsocket"></chatroom-dialogs>
                </div>

                <chatroom-window :user="user" :websocket-status="websocketStatus" :ws-response="wsResponse.getLogs" :webhook="wsResponse.webhook" @send-to-websocket="sendToWebsocket"></chatroom-window>

            </div>
        </div>









    </div>
</div>
@endsection


@push('plugin-js')
@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
    websocketDomain: '{{ $deployment->data->websocketDomain }}',
    users: {!! $users->toJson() !!},
    dialogs: {!! $dialogs->toJson() !!},
}
</script>
<script src="{{ asset('assets/js/solution/_4/chatroom/index.js?v='.time()) }}"></script>
@endpush
