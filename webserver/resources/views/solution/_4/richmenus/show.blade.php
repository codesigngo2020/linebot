@extends('layouts/app')

@section('title', '主選單內容')

@push('plugin-css')
{{-- multiselect CSS --}}
<link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
@endpush

@push('css')
<link href="{{ asset('assets/css/solution/_4/richmenus/show.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">
        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Richmenu Info</h6>

                            <div class="d-flex">
                                
                                {{-- Title --}}
                                <h1 class="header-title d-flex align-items-center flex-grow-1">
                                    <span>主選單內容</span>
                                    <span class="ml-2 mr-2">-</span>
                                    <span class="h2 nomargin">{{ $solution->name }}</span>
                                </h1>

                                {{-- Version --}}
                                <div id="richmenu-version" class="d-inline-flex">
                                    <vue-multiselect v-if="version.options.length > 1" class="i-form-control hidden-arrow multiselect-sm" v-model="version.value" track-by="value" label="name" open-direction="bottom" :options="version.options" :searchable="false" :show-labels="false" :allow-empty="false" :multiple="false" :internal-search="false" :show-no-results="false" :hide-selected="false">
                                        <template slot="singleLabel" slot-scope="props">
                                            <div class="d-flex justify-content-center">
                                                <span v-if="props.option.active" class="d-flex align-items-center text-info mr-2">
                                                    <v-svg src="{{ asset('assets/image/svg/light/check-circle.svg') }}" width="14" height="14"></v-svg>
                                                </span>
                                                <span>@{{ props.option.name }}</span>
                                            </div>
                                        </template>
                                        <template slot="option" slot-scope="props">
                                            <div class="d-flex justify-content-center">
                                                <span v-if="props.option.active" class="d-flex align-items-center text-info mr-2">
                                                    <v-svg src="{{ asset('assets/image/svg/light/check-circle.svg') }}" width="14" height="14"></v-svg>
                                                </span>
                                                <span>@{{ props.option.name }}</span>
                                            </div>
                                        </template>
                                    </vue-multiselect>
                                </div>
                            </div>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            <div v-cloak id="richmenu-content">

                <div class="row flex-nowrap">
                    {{-- 選單內容 --}}
                    <div class="col-auto">

                        <div class="width-300 cursor-pointer" @click="toggleShowGrids">
                            <div id="richmenu-layout" :class="[richmenu.size == '2500x1686' ? 'type-1' : 'type-2', 'position-relative']">
                                <img class="position-absolute top h-100 border-radius-0375" :src="richmenu.imageUrl">

                                {{-- 隔線 --}}
                                <div v-if="richmenu.is_created_by_platform" v-show="showGrids" class="position-absolute top w-100 h-100 overflow-hidden">
                                    <div id="grid" class="grid">
                                        <grid-tr v-for="(tds, index) in richmenu.areas_form_data" :tds="tds" :key="index"></grid-tr>
                                    </div>
                                </div>

                                {{-- 資訊條內容 --}}
                                <div id="chatBarTextPreview">@{{ richmenu.chatBarText }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-auto flex-grow-1 flex-shrink-1">
                        <div class="card overflow-visible">
                            <div class="card-body">
                                <span class="d-flex align-items-center mb-3">
                                    <h3 class="mr-3 mb-0 white-space-nowrap">@{{ '# '+richmenu.parent_id }}</h3>
                                    <h3 class="mr-4 mb-0 white-space-nowrap text-overflow-ellipsis overflow-hidden">@{{ richmenu.name }}</h3>

                                    <span class="d-flex flex-grow-1 mr-4">
                                        <a class="d-inline-flex" :href="getTagUrl(richmenu.tag_id)" target="_blank">
                                            <v-svg :class="[tagId == richmenu.tag_id ? 'text-2451010' : 'text-warning', 'svg']" src="{{ asset('assets/image/svg/solid/tags.svg') }}" width="16" height="16"></v-svg>
                                        </a>
                                    </span>

                                    @if($richmenu->default)
                                    <span class="default position-relative pr-3 mr-3 font-size-0875">
                                        <span class="badge badge-soft-primary">Dedault</span>
                                    </span>
                                    @endif

                                    @if($richmenu->deleted_at)
                                    <span class="deleted position-relative pr-3 mr-3 font-size-0875">
                                        <span class="badge badge-soft-danger">Deleted</span>
                                    </span>
                                    @endif

                                    @if(count($richmenu->versions) > 1)
                                    <span class="version position-relative pr-3 mr-3 font-size-0875">
                                        <span class="badge badge-soft-info">@{{ 'v'+richmenu.version }}</span>
                                    </span>
                                    @endif

                                    @if($richmenu->selected)
                                    <span v-if="richmenu.selected" class="position-relative font-size-0875 mr-3">
                                        <span class="badge badge-soft-info">自動開啟</span>
                                    </span>
                                    @endif

                                    @if(!$richmenu->deleted_all_at)
                                    <div class="dropdown d-flex">
                                        <a href="#" class="d-inline-flex align-items-center cursor-pointer dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <v-svg class="svg text-muted" src="{{ asset('assets/image/svg/light/ellipsis-v.svg') }}" width="22" height="22"></v-svg>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right p-0">
                                            @if(!$richmenu->is_current_version && !$richmenu->deleted_at)
                                            <a href="javascript:void(0);" class="dropdown-item" @click="switchVersion(richmenu.id)">切換為此版本</a>
                                            @endif

                                            @if($richmenu->is_current_version && !$richmenu->default)
                                            @if(!$richmenu->default)
                                            <a href="javascript:void(0);" class="dropdown-item" @click="toggleDefault(richmenu.id)">設為預設</a>
                                            @endif
                                            @if($richmenu->default)
                                            <a href="javascript:void(0);" class="dropdown-item" @click="toggleDefault(richmenu.id)">取消預設</a>
                                            @endif
                                            @endif

                                            @if($richmenu->is_created_by_platform)
                                            <a href="{{ route('solution.4.deployment.richmenus.edit', [$deployment, $richmenu]) }}" class="dropdown-item" target="_blank">建立新版本</a>
                                            @endif

                                            @if(!$richmenu->deleted_at)
                                            <a href="javascript:void(0);" class="dropdown-item" @click="remove(richmenu.id)">刪除版本</a>
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                </span>

                                <p class="text-muted mb-4">@{{ richmenu.description }}</p>

                                {{-- 目前使用人數佔比 --}}
                                <div v-if="!richmenu.default" class="align-items-center mb-3">
                                    <div class="align-items-center no-gutters">
                                        <div class="small mb-2">
                                            <div>@{{ '目前使用人數佔比：'+(richmenu.total_users_count == 0 ? 0 : Math.floor(richmenu.users_count*100/richmenu.total_users_count))+'%（'+richmenu.users_count+' / '+richmenu.total_users_count+'）' }}</div>
                                        </div>
                                        <div class="col-12">
                                            <!-- Progress -->
                                            <div class="progress progress-sm">
                                                <div class="progress-bar" role="progressbar" :style="'width:'+(richmenu.total_users_count == 0 ? 0 : Math.floor(richmenu.users_count*100/richmenu.total_users_count))+'%'"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- 曾經使用人數佔比 --}}
                                <div v-if="!richmenu.default" class="align-items-center">
                                    <div class="align-items-center no-gutters">
                                        <div class="small mb-2">
                                            <div>@{{ '曾經使用人數佔比：'+(richmenu.total_users_count == 0 ? 0 : Math.floor(richmenu.cumulative_users_count*100/richmenu.total_users_count))+'%（'+richmenu.cumulative_users_count+' / '+richmenu.total_users_count+'）' }}</div>
                                        </div>
                                        <div class="col-12">
                                            <!-- Progress -->
                                            <div class="progress progress-sm">
                                                <div class="progress-bar" role="progressbar" :style="'width:'+(richmenu.total_users_count == 0 ? 0 : Math.floor(richmenu.cumulative_users_count*100/richmenu.total_users_count))+'%'"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


                <div v-if="richmenu.is_created_by_platform" class="mt-5">
                    <div class="row">

                        {{-- 區域觸發行為 --}}
                        <div class="col-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-header-title">區域觸發行為</h4>
                                </div>
                                <div class="card-body p-2">
                                    <div id="area-list" class="table-responsive">
                                        <table class="table bg-transparent text-left">
                                            <tbody>
                                                <tr v-for="(area, areaIndex) in areaList">
                                                    <th class="align-top">@{{ '# '+area.id }}</th>
                                                    <td>
                                                        <p class="h5">
                                                            <span>@{{ area.type.name }}</span>

                                                            <a v-if="['message', 'liff', 'script', 'richmenu'].includes(area.type.value)" class="small ml-3" :href="getTagUrl(area.tag)" target="_blank">
                                                                <v-svg :class="[tagId == area.tag ? 'text-2451010' : 'text-warning', 'svg']" src="{{ asset('assets/image/svg/solid/tags.svg') }}" width="16" height="16"></v-svg>
                                                            </a>

                                                            <span v-if="['message', 'liff', 'script', 'richmenu'].includes(area.type.value)" class="small cursor-pointer ml-3" @click="toggleShowAreaDetail(areaIndex)">
                                                                <v-svg :class="[area.show ? 'text-primary' : 'text-muted', 'svg']" src="{{ asset('assets/image/svg/light/info-circle.svg') }}" width="16" height="16"></v-svg>
                                                            </span>
                                                        </p>
                                                        <div v-if="area.type.value == 'message'">
                                                            <p>
                                                                <span>訊息內容：</span>
                                                                <span>@{{ area.action.text.text }}</span>
                                                            </p>
                                                        </div>
                                                        <div v-else-if="area.type.value == 'keyword'">
                                                            <p>
                                                                <span>關鍵字名稱：</span>
                                                                <a v-if="area.action.keyword.value" class="text-dark text-decoration-underline m-0" :href="getKeywordUrl(area.action.keyword.value.value)" target="_blank">@{{ area.action.keyword.value.name }}</a>
                                                                <span v-else>@{{ area.action.text.text }}</span>
                                                            </p>
                                                        </div>
                                                        <div v-else-if="area.type.value == 'uri'">
                                                            <p>
                                                                <span>連結網址：</span>
                                                                <a class="text-dark text-decoration-underline m-0" :href="area.action.uri.uri" target="_blank">@{{ area.action.uri.uri }}</a>
                                                            </p>
                                                        </div>
                                                        <div v-else-if="area.type.value == 'liff'">
                                                            <p>
                                                                <span>Liff 尺寸：</span>
                                                                <span>@{{ area.action.size.value.value+' %' }}</span>
                                                            </p>
                                                            <p>
                                                                <span>連結網址：</span>
                                                                <a class="text-dark text-decoration-underline m-0" :href="area.action.uri.uri" target="_blank">@{{ area.action.uri.uri }}</a>
                                                            </p>
                                                        </div>
                                                        <div v-else-if="area.type.value == 'script'">
                                                            <p>
                                                                <span>腳本名稱：</span>
                                                                <a class="text-dark text-decoration-underline m-0" :href="getScriptUrl(area.action.script.value.value)" target="_blank">@{{ area.action.script.value.name }}</a>
                                                            </p>
                                                        </div>
                                                        <div v-else-if="area.type.value == 'richmenu'">
                                                            <p>
                                                                <span>主選單名稱：</span>
                                                                <a class="text-dark text-decoration-underline m-0" :href="getRichmenuUrl(area.action.richmenu.id)" target="_blank">@{{ area.action.richmenu.name + getRichmenuMessage(area.action.richmenu) }}</a>
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- 點擊人數佔比＆總點擊次數 --}}
                        <div class="col-6">
                            <div v-if="showTagDetail" class="card">
                                <div class="card-header">
                                    <h4 class="card-header-title">點擊人數佔比＆總點擊次數</h4>

                                    <div class="d-flex justify-content-end flex-grow-1 small">
                                        <div class="d-flex align-items-center mr-3">
                                            <div class="progress-bar bg-primary border-radius-05 mr-1" style="width: 20px;height: 4px;"></div>
                                            <div>使用中</div>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <div class="progress-bar bg-warning border-radius-05 mr-1" style="width: 20px;height: 4px;"></div>
                                            <div>曾經使用</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body p-2">
                                    <table class="table bg-transparent text-left small">
                                        <tbody>
                                            <tr v-for="areaIndex in showDetailAreaList">
                                                <th class="align-top border-top-0 pl-3 pr-0">@{{ '# '+areaList[areaIndex].id }}</th>
                                                <td class="border-top-0">
                                                    <div class="flex-column mb-3">
                                                        <p class="mb-2">
                                                            <span class="mr-2">@{{ '點擊人數佔比：'+(richmenu.users_count == 0 ? 0 : Math.floor(richmenu.tagsData[areaList[areaIndex].tag].users_count*100/richmenu.users_count))+'%（'+richmenu.tagsData[areaList[areaIndex].tag].users_count+' / '+richmenu.users_count+'）' }}</span>
                                                            <span>@{{ '總點擊次數：'+richmenu.tagsData[areaList[areaIndex].tag].total_count }}</span>
                                                        </p>
                                                        <div class="align-items-center">
                                                            <div class="align-items-center no-gutters">
                                                                <!-- Progress -->
                                                                <div class="progress progress-sm">
                                                                    <div class="progress-bar" role="progressbar" :style="'width:'+Math.floor(richmenu.tagsData[areaList[areaIndex].tag].users_count*100/richmenu.users_count)+'%'"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="flex-column">
                                                        <p class="mb-2">
                                                            <span class="mr-2">@{{ '點擊人數佔比：'+(richmenu.cumulative_users_count == 0 ? 0 : Math.floor(richmenu.tagsData[areaList[areaIndex].tag].cumulative_users_count*100/richmenu.cumulative_users_count))+'%（'+richmenu.tagsData[areaList[areaIndex].tag].cumulative_users_count+' / '+richmenu.cumulative_users_count+'）' }}</span>
                                                            <span>@{{ '總點擊次數：'+richmenu.tagsData[areaList[areaIndex].tag].cumulative_total_count }}</span>
                                                        </p>
                                                        <div class="align-items-center">
                                                            <div class="align-items-center no-gutters">
                                                                <!-- Progress -->
                                                                <div class="progress progress-sm">
                                                                    <div class="progress-bar bg-warning" role="progressbar" :style="'width:'+Math.floor(richmenu.tagsData[areaList[areaIndex].tag].cumulative_users_count*100/richmenu.cumulative_users_count)+'%'"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection


@push('plugin-js')
{{-- multiselect JS --}}
<script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
<script src="{{ asset('assets/components/select/select.js') }}"></script>
@endpush


@push('js')
<script type="text/x-template" id="--grid-tr">
    <div class="tr">
        <div v-for="td in tds" :key="td.id" class="td">
            <div v-if="td.id" class="index">
                <span class="id position-absolute">@{{ td.id }}</span>
            </div>
            <grid-tr v-else v-for="tds in td" :tds="tds" :key="td.id"></grid-tr>
        </div>
    </div>
</script>

<script>
Vue.component('grid-tr', {
    template: '#--grid-tr',
    props: ['tds'],
});
</script>

<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
    richmenu: {!! $richmenu->toJson() !!},
    tagId: {{ request()->input('tagId') ?? 0 }},
}
</script>
<script src="{{ asset('assets/js/solution/_4/richmenus/show.js?v='.time()) }}"></script>
@endpush
