@extends('layouts/app')

@section('title', '關鍵字內容')

@push('plugin-css')
{{-- multiselect CSS --}}
<link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
@endpush

@push('css')
<link href="{{ asset('assets/css/solution/_4/basic/message/message/show.css?v='.time()) }}" rel="stylesheet">
<link href="{{ asset('assets/css/solution/_4/keywords/show.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

    <div class="container-fluid mt-5">

        @include('components.alerts.main')

        <div class="col-12 nopadding">

            <div class="header">
                <div class="header-body">
                    <div class="row align-items-center mb-3">
                        <div class="col">

                            {{-- Pretitle --}}
                            <h6 class="header-pretitle">Keyword Info</h6>

                            <div class="d-flex">

                                {{-- Title --}}
                                <h1 class="header-title d-flex align-items-center flex-grow-1">
                                    <span>關鍵字內容</span>
                                    <span class="ml-2 mr-2">-</span>
                                    <span class="h2 nomargin">{{ $solution->name }}</span>
                                </h1>

                                {{-- Version --}}
                                <div id="keyword-version" class="d-inline-flex">
                                    <vue-multiselect v-if="version.options.length > 1" class="i-form-control hidden-arrow multiselect-sm" v-model="version.value" track-by="value" label="name" open-direction="bottom" :options="version.options" :searchable="false" :show-labels="false" :allow-empty="false" :multiple="false" :internal-search="false" :show-no-results="false" :hide-selected="false">
                                        <template slot="singleLabel" slot-scope="props">
                                            <div class="d-flex justify-content-center">
                                                <span v-if="props.option.active" class="d-flex align-items-center text-info mr-2">
                                                    <v-svg src="{{ asset('assets/image/svg/light/check-circle.svg') }}" width="14" height="14"></v-svg>
                                                </span>
                                                <span>@{{ props.option.name }}</span>
                                            </div>
                                        </template>
                                        <template slot="option" slot-scope="props">
                                            <div class="d-flex justify-content-center">
                                                <span v-if="props.option.active" class="d-flex align-items-center text-info mr-2">
                                                    <v-svg src="{{ asset('assets/image/svg/light/check-circle.svg') }}" width="14" height="14"></v-svg>
                                                </span>
                                                <span>@{{ props.option.name }}</span>
                                            </div>
                                        </template>
                                    </vue-multiselect>
                                </div>

                            </div>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            <div v-cloak id="keyword-content">

                <div class="card">
                    <div class="card-body d-table w-100 col-container p-0" ref="cardBody">
                        <div class="d-table-cell p-4">
                            <h3 class="d-flex align-items-center mb-3">
                                <span class="mr-3">@{{ '# '+keyword.parent_id }}</span>
                                <span class="mr-4">@{{ keyword.name }}</span>

                                <a class="d-inline-flex" :href="getTagUrl(keyword.tag_id)" target="_blank">
                                    <v-svg :class="[tagId == keyword.tag_id ? 'text-2451010' : 'text-warning', 'svg']" src="{{ asset('assets/image/svg/solid/tags.svg') }}" width="16" height="16"></v-svg>
                                </a>

                                <span class="trigger-type position-relative d-flex justify-content-end align-items-center flex-grow-1 pr-3 mr-3 small">
                                    <span class="badge badge-soft-secondary">@{{ triggerTypes[keyword.trigger_type] }}</span>
                                </span>

                                @if($keyword->deleted_all_at)
                                <span class="deleted position-relative d-flex align-items-center pr-3 mr-3 small">
                                    <span class="badge badge-soft-danger">Deleted</span>
                                </span>
                                @endif

                                @if(count($keyword->versions) > 1)
                                <span class="version position-relative d-flex align-items-center pr-3 mr-3 small">
                                    <span class="badge badge-soft-info">@{{ 'v'+keyword.version }}</span>
                                </span>
                                @endif

                                <span class="d-flex align-items-center small mr-3">
                                    <span :class="[keyword.active ? 'text-success' : 'text-warning', 'active-dot font-size-0750 mr-2']">●</span>
                                    <span>@{{ keyword.active ? 'Active' : 'Inactive' }}</span>
                                </span>

                                @if(!$keyword->deleted_all_at)
                                <div class="dropdown d-flex">
                                    <a href="#" class="d-inline-flex align-items-center cursor-pointer dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <v-svg class="svg text-muted" src="{{ asset('assets/image/svg/light/ellipsis-v.svg') }}" width="22" height="22"></v-svg>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right p-0">

                                        @if($keyword->is_current_version)
                                        <a href="javascript:void(0);" class="dropdown-item" @click="toggleActive(keyword.id)">@{{ keyword.active ? '停用' : '啟用' }}</a>
                                        @else
                                        <a href="javascript:void(0);" class="dropdown-item" @click="switchVersion(keyword.id)">切換為此版本</a>
                                        @endif

                                        <a href="{{ route('solution.4.deployment.keywords.edit', [$deployment, $keyword]) }}" class="dropdown-item" target="_blank">建立新版本</a>
                                    </div>
                                </div>
                                @endif
                            </h3>

                            <p class="text-muted mb-4">@{{ keyword.description }}</p>

                            {{-- 觸發人數占比 --}}
                            <div class="align-items-center mb-3">
                                <div class="align-items-center no-gutters">
                                    <div class="small mb-2">@{{ '已觸發人數佔比：'+(keyword.total_users_count == 0 ? 0 : Math.floor(keyword.users_count*100/keyword.total_users_count))+'%（'+keyword.users_count+' / '+keyword.total_users_count+'）' }} </div>
                                    <div class="col-12">
                                        <!-- Progress -->
                                        <div class="progress progress-sm">
                                            <div class="progress-bar" role="progressbar" :style="'width:'+Math.floor(keyword.users_count*100/keyword.total_users_count)+'%'"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- QR code --}}
                        <div class="QR-code d-table-cell" ref="QRcode">
                            <img :src="keyword.QRcode_url">
                        </div>
                    </div>
                </div>

                @if($keyword->trigger_type == 'reply')

                @if($keyword->template_id)

                <div class="text-right">
                    <a href="{{ route('solution.4.deployment.messagesPool.show', [$deployment, $keyword->template->id]) }}" class="d-inline-flex align-items-center btn btn-secondary" target="_blank">
                        <span class="mr-3">前往訊息模組</span>
                        <v-svg class="svg" src="{{ asset('assets/image/svg/regular/long-arrow-right.svg') }}" width="18" height="18"></v-svg>
                    </a>
                </div>

                @else

                @include('components.basic.solution._4.message.messages')
                @include('components.basic.solution._4.message.message-preview')
                @include('components.basic.solution._4.message.action-list')

                <message-preview :tagid="tagId" :messages="keyword.messages" :messagesformdata="keyword.messages_form_data" :tagsdata="keyword.tagsData" :userscount="keyword.total_users_count" :showactions="true" :showtags="true"></message-preview>

                @endif

                @elseif($keyword->trigger_type == 'script')
                <div class="text-right">
                    <a href="{{ route('solution.4.deployment.scripts.show', [$deployment, $keyword->script_id]) }}" class="d-inline-flex align-items-center btn btn-secondary" target="_blank">
                        <span class="mr-3">前往訊息腳本</span>
                        <v-svg class="svg" src="{{ asset('assets/image/svg/regular/long-arrow-right.svg') }}" width="18" height="18"></v-svg>
                    </a>
                </div>

                @elseif($keyword->trigger_type == 'richmenu')
                <div class="text-right">
                    <a href="{{ route('solution.4.deployment.richmenus.show', [$deployment, $keyword->richmenu->id]) }}" class="d-inline-flex align-items-center btn btn-secondary" target="_blank">
                        <span class="mr-3">@{{ getRichmenuMessage() }}</span>
                        <v-svg class="svg" src="{{ asset('assets/image/svg/regular/long-arrow-right.svg') }}" width="18" height="18"></v-svg>
                    </a>
                </div>
                @endif

            </div>

        </div>

    </div>
</div>
@endsection


@push('plugin-js')
{{-- multiselect JS --}}
<script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
<script src="{{ asset('assets/components/select/select.js') }}"></script>
@endpush


@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    deployment: {{ $deployment->id }},
    keyword: {!! $keyword->toJson() !!},
    tagId: {{ request()->input('tagId') ?? 0 }},
}
</script>
<script src="{{ asset('assets/js/solution/_4/keywords/show.js?v='.time()) }}"></script>
@endpush
