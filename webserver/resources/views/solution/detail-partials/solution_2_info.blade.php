<div id="solution-info">


  <div class="info-group">
    <h4>
      <i>
        <v-svg class="svg" src="{{ asset('assets/image/svg/light/suitcase.svg') }}" width="18" height="18"></v-svg>
      </i>
      <span>{{ __('views/solution/detail.info.title.service') }}</span>
    </h4>
    @for($i = 1; $i <= 3; $i ++)
    <div class="info-item d-flex flex-row">
      <div class="index">{{ $i }}.</div>
      <div class="d-flex flex-column">
        <span class="title strong">{{ __('views/solution/detail.info.content.'.$solution->id.'.service.'.$i.'.title') }}：</span>
        <span class="content">{{ __('views/solution/detail.info.content.'.$solution->id.'.service.'.$i.'.content') }}</span>
      </div>
    </div>
    @endfor
  </div>

  {{-- Divider --}}
  <hr class="mt-5 mb-5">

  <div class="info-group">
    <h4>
      <i>
        <v-svg src="{{ asset('assets/image/svg/light/server.svg') }}" width="18" height="18"></v-svg>
      </i>
      <span>{{ __('views/solution/detail.info.title.system') }}</span>
    </h4>
    <div class="info-item">
      <span class="title">{{ __('views/solution/detail.info.content.'.$solution->id.'.system') }}：</span>
      <span class="content">5.1.1 ~</span>
    </div>
  </div>

  <div class="info-group">
    <h4>
      <i>
        <v-svg class="svg" src="{{ asset('assets/image/svg/light/usd-circle.svg') }}" width="18" height="18"></v-svg>
      </i>
      <span>{{ __('views/solution/detail.info.title.payment') }}</span>
    </h4>
    <div class="info-item">
      <p class="content">{{ __('views/solution/detail.info.content.'.$solution->id.'.payment') }}</p>
    </div>
  </div>

  <div class="info-group">
    <h4>
      <i>
        <v-svg class="svg" src="{{ asset('assets/image/svg/light/book-open.svg') }}" width="18" height="18"></v-svg>
      </i>
      <span>{{ __('views/solution/detail.info.title.manual') }}</span>
    </h4>
    <div class="info-item">
      <p class="content">{{ __('views/solution/detail.info.content.'.$solution->id.'.manual') }}</p>
    </div>
  </div>
</div>
