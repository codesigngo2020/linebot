@extends('layouts/app')

@section('title', '爬蟲管理')

@push('plugin-css')
{{-- multiselect CSS --}}
<link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
@endpush

@push('css')
<link href="{{ asset('assets/css/solution/_2/crawlers.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

  <div class="container-fluid mt-5 mb-6">

    @include('components.alerts.deployment')

    <div id="app" class="col-12 nopadding">
      <div class="header">
        <div class="header-body border-0 pb-0">
          <div class="row align-items-center">
            <div class="col">

              {{-- Pretitle --}}
              <h6 class="header-pretitle">Instagram Crawlers</h6>

              {{-- Title --}}
              <h1 class="header-title d-flex align-items-center">
                <span>爬蟲管理</span>
                <span class="ml-2 mr-2">-</span>
                <span class="h2 nomargin">{{ $solution->name }}</span>
              </h1>

            </div>
          </div> {{-- / .row --}}
        </div>
      </div>


      <div class="tab-content">
        @include('components.forms.solution._2.deploy-partials.form3', ['action' => 'edit'])
        <nav-form-3 @update-form-data="updateFormData" ref="form3" :crawlers-count="crawlers_count" :posts-count="posts_count"></nav-form-3>
      </div>

      @if($deployment->status != 'destroyed')

      {!! Form::button('@{{ is_success ? "儲存成功" : "儲存設定" }}', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, is_success ? "btn-success" : "btn-primary", "btn btn-block mt-6"]', '@click'=>'submit', 'v-cloak']) !!}

      @endif

    </div>

  </div>
</div>
@endsection


@push('plugin-js')
{{-- multiselect JS --}}
<script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
<script src="{{ asset('assets/components/select/select.js') }}"></script>
{{-- jQuery mask JS --}}
<script src="{{ asset('assets/plugins/mask/jquery.mask.js') }}"></script>
@endpush


@push('js')
<script>
var args = {
  _token: "{{ csrf_token() }}",
  deployment: {{ $deployment->id }},

  plan: {{ $deployment->plan->id }},
  crawlers_count: {{ $deployment->plan->data->crawlers_count }},
  posts_count: {{ $deployment->plan->data->posts_count }},

  children_counts: {{ $crawlers->last()->id }},
  crawlers: {!! $crawlers->reverse()
    ->mapWithKeys(function($item, $key){
      return [($key + 1) => $item];
    })
    ->toJson() !!},
  }
</script>
<script src="{{ asset('assets/js/solution/_2/crawlers.js?v='.time()) }}"></script>
@endpush
