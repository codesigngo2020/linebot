@extends('layouts/app')

@section('title', '文章管理')

@push('css')
<link href="{{ asset('assets/css/solution/_2/posts.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

  <div class="container-fluid mt-5 mb-5">

    @include('components.alerts.deployment')
    
    <div class="col-12 nopadding">
      <div class="header">
        <div class="header-body">
          <div class="row align-items-center mb-3">
            <div class="col">

              {{-- Pretitle --}}
              <h6 class="header-pretitle">Wordpress Posts</h6>

              {{-- Title --}}
              <h1 class="header-title d-flex align-items-center">
                <span>文章管理</span>
                <span class="ml-2 mr-2">-</span>
                <span class="h2 nomargin">{{ $solution->name }}</span>
              </h1>

            </div>
          </div> {{-- / .row --}}

          @if($crawlers->count() > 1)
          <div class="row align-items-center">
            <div class="col">

              {{-- Nav --}}
              <ul class="nav nav-tabs nav-overflow header-tabs font-size-0875" role="tablist">
                @foreach($crawlers as $key => $crawler)
                <li class="nav-item">
                  <a href="#nav-table-{{ $key + 1 }}" class="nav-link {{ $key == 0 ? 'active' : '' }} d-flex align-items-center white-space-nowrap pb-4" data-toggle="tab" role="tab" aria-controls="nav-table-{{ $key + 1 }}" aria-selected="{{ $key == 0 ? true : false }}">{{ '@'.$crawler->account->username }}<span class="badge badge-pill badge-soft-secondary ml-2">{{ $crawler->posts_count }}</span>
                  </a>
                </li>
                @endforeach
              </ul>
            </div>
          </div>
          @endif

        </div>
      </div>


      <div class="tab-content">
        @foreach($crawlers as $key => $crawler)
        @include('components.tabs.solution._2.posts')
        @endforeach
      </div>

    </div>

  </div>
</div>
@endsection


@push('js')
<script>
var args = {
  _token: "{{ csrf_token() }}",
  deployment: {{ $deployment->id }},
}
</script>
<script src="{{ asset('assets/js/solution/_2/posts.js?v='.time()) }}"></script>
@endpush
