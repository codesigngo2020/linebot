@extends('layouts/app')

@section('title', 'Wordpress設定')

@push('css')
<link href="{{ asset('assets/css/solution/_2/wordpress.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

  <div class="container-fluid mt-5 mb-5">

    @include('components.alerts.deployment')

    <div class="col-12 nopadding">
      <div class="header">
        <div class="header-body">
          <div class="row align-items-center mb-3">
            <div class="col">

              {{-- Pretitle --}}
              <h6 class="header-pretitle">Wordpress Settings</h6>

              {{-- Title --}}
              <h1 class="header-title d-flex align-items-center">
                <span>Wordpress設定</span>
                <span class="ml-2 mr-2">-</span>
                <span class="h2 nomargin">{{ $solution->name }}</span>
              </h1>

            </div>
          </div> {{-- / .row --}}
        </div>
      </div>


      <div id="form" v-cloak>
        <h2 class="header-title mb-3">連結設定</h2>
        <div class="deployment-info mb-4">
          <div class="info-item">請點擊下方按鈕，下載並安裝Wordpress外掛，複製外掛中的金鑰貼至下方欄位，點擊<strong>測試連線</strong>確認成功連結至您的網站。</div>
          <a class="btn btn-block btn-secondary col-12 col-sm-4 col-md-3" href="{{ route('file.download', $solution->files()->whereHas('categories', function($q){
            $q->where('id', 4);
          })->first()) }}">下載外掛</a>
        </div>

        {{-- 網域名稱 --}}
        <div class="form-group">
          {!! Form::requiredLabel('domain', '網域名稱') !!}
          {{ Form::text('domain', old('domain'), [
          'id'=>'domain',
          'placeholder'=>'domain.com or 199.27.25.0/24',
          'v-model'=>'form[1].domain.domain',
          ':class'=>'[{"is-invalid" : form[1].domain.invalid}, "form-control"]',
          ':disabled' => 'form[1].is_success',
          ]) }}
          {!! Form::ajaxInvalidFeedback('form[1].domain.info') !!}
        </div>

        {{-- 金鑰 --}}
        <div class="form-group">
          {!! Form::requiredLabel('key', '金鑰') !!}
          {{ Form::text('key', old('key'), [
          'id'=>'key',
          'placeholder'=>'在Wordpress外掛上產生的金鑰',
          'v-model'=>'form[1].key.key',
          ':class'=>'[{"is-invalid" : form[1].key.invalid}, "form-control"]',
          ':disabled' => 'form[1].is_success',
          ]) }}
          {!! Form::ajaxInvalidFeedback('form[1].key.info') !!}
        </div>

        {{-- Buttons --}}
        {!! Form::button('@{{ form[1].is_success ? "成功連結 Wordpress" : "測試連線" }}', [
        'id'=>'domain-test',
        '@click' => 'testDomain', ':class'=>'[form[1].is_success ? "btn-success" : "btn-warning", {"progress-bar-striped progress-bar-animated" : form[1].testing}, "btn btn-block mt-5"]',
        ':disabled' => 'form[1].is_success',
        'v-cloak' => 'true'
        ]) !!}

        {{-- Divider --}}
        <hr class="mt-5 mb-5">

        <h2 class="header-title mb-3">刊登設定</h2>
        <div class="deployment-info mb-4">
          <div class="info-item">僅在<strong>刊登時間</strong>內依循<strong>刊登頻率</strong>在您的 Wordpress 上刊登文章。</div>
        </div>

        <div class="form-group">
          <label for="duration">文章刊登時間<span class="form-required">*</span></label>
          <input id="duration" type="text" :class="[{'is-invalid' : form[2].duration.invalid}, 'form-control']" data-mask="AB : CB - AB : CB" placeholder="00 : 00 - 23 : 59" ref="duration">
          <span class="invalid-feedback" role="alert"><strong>@{{ form[2].duration.info }}</strong></span>
        </div>

        <div class="form-group">
          <label for="postFrequency">文章刊登頻率<span class="form-required">*</span><span>：每 @{{ form[2].postFrequency.value }} 分鐘</span></label>
          <input type="range" :class="[{'is-invalid' : form[2].postFrequency.invalid}, 'custom-range']" min="1" max="360" step="1" id="postFrequency" v-model="form[2].postFrequency.value">
          <span class="invalid-feedback" role="alert"><strong>@{{ form[2].postFrequency.info }}</strong></span>
        </div>

        <div class="form-group">
          <label for="postFrequency">文章刊登順序<span class="form-required">*</span></label>
          <draggable v-model="form[2].order.order" :class="[{'is-invalid' : form[2].order.invalid}, 'd-flex form-control']">
            <div v-for="crawler in form[2].order.order" class="draggable-element" :key="crawler.id">@{{ crawler.name }}</div>
          </draggable>
          <span class="invalid-feedback" role="alert"><strong>@{{ form[2].order.info }}</strong></span>
        </div>

        @if($deployment->status != 'destroyed')

        {!! Form::button('@{{ is_success ? "儲存成功" : "儲存設定" }}', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, is_success ? "btn-success" : "btn-primary", "btn btn-block mt-6"]', '@click'=>'submit', 'v-cloak']) !!}

        @endif

      </div>

    </div>

  </div>
</div>
@endsection


@push('plugin-js')
{{-- jQuery mask JS --}}
<script src="{{ asset('assets/plugins/mask/jquery.mask.js') }}"></script>
{{-- Sortable --}}
<script src="//cdn.jsdelivr.net/npm/sortablejs@1.8.4/Sortable.min.js"></script>
{{-- Vue.Draggable --}}
<script src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.20.0/vuedraggable.umd.min.js"></script>
@endpush


@push('js')
<script>
var args = {
  _token: "{{ csrf_token() }}",
  deployment: {{ $deployment->id }},
  form: {
    1: {
      name: "{{ $deployment->name }}",
      domain: "{{ $deployment->data->domain }}",
      key: "{{ $deployment->data->key }}",
    },
    2: {
      start: "{{ $deployment->data->post->start }}",
      end: "{{ $deployment->data->post->end }}",
      frequency: "{{ $deployment->data->post->frequency }}",
      order: {!! $order !!},
    },
  },
}
</script>
<script src="{{ asset('assets/js/solution/_2/wordpress.js?v='.time()) }}"></script>
@endpush
