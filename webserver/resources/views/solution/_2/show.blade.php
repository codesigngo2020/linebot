@extends('layouts/app')

@section('title', '部署儀表板')

@push('css')
<link href="{{ asset('assets/css/solution/_2/show.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

  <div class="container-fluid mt-5">

    @include('components.alerts.deployment')
    
    <div class="col-12 nopadding">

      <div class="header">
        <div class="header-body">
          <div class="row align-items-center mb-3">
            <div class="col">

              {{-- Pretitle --}}
              <h6 class="header-pretitle">Deploymenet Dashboard</h6>

              {{-- Title --}}
              <h1 class="header-title d-flex align-items-center">
                <span>部署儀表版</span>
                <span class="ml-2 mr-2">-</span>
                <span class="h2 nomargin">{{ $solution->name }}</span>
              </h1>

            </div>
          </div> {{-- / .row --}}
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-12 col-xl-6">
        <div class="card">
          <div class="card-header">
            <div class="row align-items-center">
              <div class="col">
                {{-- Title --}}
                <h4 class="card-header-title">文章刊登數</h4>
              </div>
            </div> {{-- / .row --}}

          </div>
          <div class="card-body">

            <div class="chart">
              <canvas id="posts-count-chart" class="chart-canvas chartjs-render-monitor"></canvas>
            </div>

          </div>
        </div>

      </div>

      <div class="col-12 col-xl-6">
        <div class="card">
          <div class="card-header">
            <div class="row align-items-center">
              <div class="col">
                {{-- Title --}}
                <h4 class="card-header-title">IG 請求數</h4>
              </div>
            </div> {{-- / .row --}}

          </div>
          <div class="card-body">

            <div class="chart">
              <canvas id="requests-count-chart" class="chart-canvas chartjs-render-monitor"></canvas>
            </div>

          </div>
        </div>

      </div>
    </div>



    <div class="row">
      <div class="col-12 col-xl-8">
        @include('components.tables.solution._2.show.crawlers')
      </div>
      <div class="col-12 col-xl-4">

        {{-- Devices --}}
        <div class="card">
          <div class="card-header">
            <div class="row align-items-center">
              <div class="col">
                {{-- Title --}}
                <h4 class="card-header-title">文章分佈</h4>
              </div>
            </div> {{-- / .row --}}

          </div>
          <div class="card-body">

            {{-- Chart --}}
            <div class="chart chart-appended">
              <canvas id="posts-doughnut-chart" class="chart-canvas chartjs-render-monitor" data-target="#devicesChartLegend"></canvas>
            </div>

            {{-- Legend --}}
            <div id="posts-doughnut-chart-legend" class="chart-legend flex-column count-{{ count($crawlers) }}">
              <p class="{{ count($crawlers) > 3 ? '' : 'nomargin' }}">
                @foreach($crawlers as $crawler)
                <span class="chart-legend-item"><i class="chart-legend-indicator"></i>爬蟲 #{{ $crawler->id }}</span>

                @if($loop->index == 2 && $loop->last)
              </p>
              <p class="nomargin">
                @endif

                @endforeach
              </p>
            </div>

          </div>
        </div>

      </div>
    </div>

  </div>
</div>
@endsection


@push('plugin-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
@endpush


@push('js')
<script>
var args = {
  _token: "{{ csrf_token() }}",
  deployment: "{{ $deployment->id }}",
  'main-chart-data': {
    data: {!! json_encode($mainChartData) !!},
  },
  'posts-doughnut-chart-data': {
    data: {!! $crawlers->pluck('posts_count', 'id')->toJson() !!},
  }
}
</script>
<script src="{{ asset('assets/js/solution/_2/show.js?v='.time()) }}"></script>
@endpush
