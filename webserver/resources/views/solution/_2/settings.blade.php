@extends('layouts/app')

@section('title', '部署設定')

@push('css')
<link href="{{ asset('assets/css/solution/_2/settings.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

  <div class="container-fluid mt-5 mb-5">

    @include('components.alerts.deployment')

    <div class="col-12 nopadding">
      <div class="header">
        <div class="header-body">
          <div class="row align-items-center mb-3">
            <div class="col">

              {{-- Pretitle --}}
              <h6 class="header-pretitle">Deployment Settings</h6>

              {{-- Title --}}
              <h1 class="header-title d-flex align-items-center">
                <span>部署設定</span>
                <span class="ml-2 mr-2">-</span>
                <span class="h2 nomargin">{{ $solution->name }}</span>
              </h1>

            </div>
          </div> {{-- / .row --}}
        </div>
      </div>

      <div class="row">

        <div id="deployment-status" class="d-sm-flex flex-sm-column col-12 col-sm-6 mb-5 mb-sm-0">
          <h2 class="header-title mb-3">
            <span>部署狀態</span>
            @if($deployment->status == 'active')
            <span class="badge badge-soft-primary font-size-0875 ml-3">啟用中</span>
            @elseif($deployment->status == 'suspend')
            <span class="badge badge-soft-secondary font-size-0875 ml-3">暫停中</span>
            @elseif($deployment->status == 'destroyed')
            <span class="badge badge-soft-danger font-size-0875 ml-3">已刪除</span>
            @endif
          </h2>

          <div class="deployment-info mb-4 mb-sm-0 flex-sm-grow-1">
            <div class="info-item mb-sm-0">暫停部署，將會停止所有爬蟲爬取Instagram貼文，以及刊登文章至您的Wordpress網站。</div>
          </div>

          @if($deployment->status == 'suspend' || $deployment->status == 'active')

          {{ Form::open(['method'=>'patch', 'route'=>['solution.2.deployment.toggleStatus', $deployment]]) }}
          {{ Form::submit( $deployment->status == 'suspend' ? '重啟部署' : '暫停部署', ['class' => ($deployment->status == 'active' ? 'btn-secondary' : 'btn-primary').' btn btn-block mt-5']) }}
          {{ Form::close() }}

          @endif

        </div>

        <div id="deployment-name" class="col-12 col-sm-6" v-cloak>
          <h2 class="header-title mb-3">部署設定</h2>

          {{-- 部署名稱 --}}
          <div class="form-group">
            {!! Form::requiredLabel('name', '部署名稱') !!}
            {{ Form::text('name', old('name'), [
            'id'=>'name',
            ':class'=>'[{"is-invalid" : form.name.invalid}, "form-control"]',
            'placeholder'=>'My first crawler', 'v-model'=>'form.name.name'
            ]) }}
            {!! Form::ajaxInvalidFeedback('form.name.info') !!}
          </div>

          @if($deployment->status != 'destroyed')

          {!! Form::button('@{{ is_success ? "儲存成功" : "儲存設定" }}', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, is_success ? "btn-success" : "btn-primary", "btn btn-block mt-5"]', '@click'=>'submit', 'v-cloak']) !!}

          @endif

        </div>

      </div>

      {{-- Divider --}}
      <hr class="mt-5 mb-5">

      {{-- 方案資訊 & 帳務資訊 --}}
      <div class="row">
        <div class="col-12">
          <h2 class="header-title mb-3">帳務資訊</h2>
        </div>

        <div id="plan" class="col-12 col-lg-5 col-xl-4">
          {{-- Card --}}
          <div class="card">
            <div class="card-body">

              {{-- Title --}}
              <h6 class="text-uppercase text-center text-muted my-4">{{ $plan->name }}</h6>

              {{-- Price --}}
              <div class="row no-gutters align-items-center justify-content-center">
                <div class="col-auto">
                  <div class="h2 mb-0 mr-2">$</div>
                </div>
                <div class="col-auto d-flex">
                  <div class="display-2 mb-0 mr-2">{{ $plan->price }}</div>
                  <div class="d-flex align-items-end h5 mr-2 mb-3">USD</div>
                </div>
              </div> {{-- / .row --}}

              {{-- Period --}}
              <div class="h6 text-uppercase text-center text-muted mb-5">/ month</div>

              {{-- Features --}}
              <div class="mb-3">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                    <small>平台使用</small>
                    <v-svg class="text-success" src="{{ asset('assets/image/svg/light/check-circle.svg') }}" width="16" height="16"></v-svg>
                  </li>
                  <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                    <small>爬蟲數量</small>
                    <small>{{ $plan->data->crawlers_count }}</small>
                  </li>
                  <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                    <small>IG 請求次數</small>
                    <small>{{ $plan->data->instagram_requests_count }} / day</small>
                  </li>
                  <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                    <small>刊登文章數</small>
                    <small>{{ $plan->data->posts_count }} / day</small>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>


        @if($transactions)
        <div id="transaction" class="col-12 col-lg-7 col-xl-8">
          @include('components.tables.solution._2.settings.table')
        </div>
        @else
        <div id="payment" class="col-12 col-lg-7 col-xl-8">
          <div class="deployment-info mb-4">
            <div class="info-item">因為您尚未完成付費程序，無法啟用部署，請點擊下方<strong>前往付費</strong>按鈕前往付費。<br/>如連結已失效，請點擊<strong>產生新連結</strong>產生新付費連結，成功後請再次點擊<strong>前往付費</strong>按鈕。</div>
          </div>

          <div class="row">
            <div class="col-12 col-xl-6">
              <a class="btn btn-block btn-primary" href="https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token={{ $deployment->paypal_agreement_token }}" target="_blank" ref="link">前往付費</a>
            </div>

            <div class="col-12 col-xl-6 mt-4 mt-xl-0">
              {!! Form::button('@{{ is_success ? "產生成功" : is_error ? "產生失敗" : "產生新連結" }}', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, {"btn-danger" : is_error}, is_success ? "btn-success" : "btn-secondary", "btn btn-block"]', '@click'=>'submit', 'v-cloak']) !!}
            </div>
          </div>

        </div>
        @endif


      </div>

      {{-- Divider --}}
      <hr class="mt-5 mb-5">

      <div id="destroy">
        <h2 class="header-title mb-3">刪除部署</h2>
        <div class="deployment-info mb-4">
          @if($deployment->destroyed_at)
          <div class="info-item text-danger">再次刪除，將無法復原，無任何緩衝期限。</div>
          @else
          <div class="info-item">第一次刪除部署後，將停止收費，並獲得30天的緩衝期限，期限內可重新啟用部署；再次刪除後，將會直接刪除部署，無任何緩衝期限。</div>
          @endif
        </div>

        {{-- Form --}}

        @if($deployment->status == 'destroyed')

        {{ Form::open(['method'=>'patch', 'route'=>['solution.2.deployment.reactive', $deployment]]) }}
        {{ Form::submit('重啟部署', ['class'=>'btn-primary btn btn-block mt-5']) }}
        {{ Form::close() }}


        @else

        {{ Form::open(['method'=>'delete', 'route'=>['solution.2.deployment.destroy', $deployment]]) }}
        {{ Form::submit('刪除部署', ['class'=>'btn-danger btn btn-block mt-5']) }}
        {{ Form::close() }}

        @endif
      </div>

    </div>

  </div>
</div>
@endsection


@push('plugin-js')

@endpush


@push('js')
<script>
var args = {
  _token: "{{ csrf_token() }}",
  deployment: {{ $deployment->id }},
  name: "{{ $deployment->name }}",
}
</script>
<script src="{{ asset('assets/js/solution/_2/settings.js?v='.time()) }}"></script>
@endpush
