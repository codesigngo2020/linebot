@extends('layouts/app')

@section('title', '個人帳號分析')

@push('css')
<link href="{{ asset('assets/css/solution/_3/show.css?v='.time()) }}" rel="stylesheet">
@endpush

@section('content')
<div class="main-content">

  <div class="container-fluid mt-5">
    <div class="col-12 nopadding">

      <div class="header">
        <div class="header-body">
          <div class="row align-items-center mb-3">
            <div class="col">

              {{-- Pretitle --}}
              <h6 class="header-pretitle">Deploymenet Dashboard</h6>

              {{-- Title --}}
              <h1 class="header-title d-flex align-items-center">
                <span>個人帳號分析</span>
                <span class="ml-2 mr-2">-</span>
                <span class="h2 nomargin">{{ $solution->name }}</span>
              </h1>

            </div>
          </div> {{-- / .row --}}
        </div>
      </div>
    </div>


    <div id="top-bar" class="row overflow-auto flex-nowrap">
      <div class="col-5 col-sm-4 col-md-5 col-lg-4">

        <!-- Card -->
        <div class="card">
          <div class="card-body">
            <div class="row align-items-center">
              <div class="col">

                <!-- Title -->
                <h6 class="card-title text-uppercase text-muted mb-2">貼文數</h6>
                <!-- Heading -->
                <span class="h2 mb-0">65</span>

              </div>
              <div class="col-auto">
                <!-- Icon -->
                <span class="h2 fe fe-dollar-sign text-muted mb-0">
                  <v-svg src="{{ asset('assets/image/svg/light/images.svg') }}" width="35" height="35"></v-svg>
                </span>
              </div>
            </div> <!-- / .row -->

          </div>
        </div>
      </div>

      <div class="col-auto flex-grow-1">

        <!-- Card -->
        <div class="card">
          <div class="card-body">
            <div class="row align-items-center flex-nowrap">
              <div class="col">

                <!-- Title -->
                <h6 class="card-title text-uppercase text-muted mb-2 white-space-nowrap">紛絲人數 - 互相追蹤 - 追蹤中</h6>
                <!-- Heading -->
                <span class="h2 mb-0 white-space-nowrap">65 - 112 - 55</span>

              </div>
              <div class="col-auto">
                <!-- Icon -->
                <span class="h2 fe fe-dollar-sign text-muted mb-0">
                  <v-svg src="{{ asset('assets/image/svg/light/users.svg') }}" width="35" height="35"></v-svg>
                </span>
              </div>
            </div> <!-- / .row -->

          </div>
        </div>
      </div>






    </div>






  </div>
</div>
@endsection


@push('plugin-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
@endpush


@push('js')
<script>

</script>
<script src="{{ asset('assets/js/solution/_3/show.js?v='.time()) }}"></script>
@endpush
