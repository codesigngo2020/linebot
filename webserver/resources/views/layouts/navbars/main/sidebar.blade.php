<nav v-cloak id="sidebar" :class="[{'collapsed' : !show}, 'navbar navbar-vertical fixed-left navbar-expand-md navbar-light']">
    <div class="container-fluid">

        {{-- Toggler --}}
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#sidebarCollapse" aria-controls="sidebarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        {{-- Brand --}}
        <a class="navbar-brand" href="index.html">
            <img src="{{ asset('assets/image/logo.svg') }}" class="navbar-brand-img mx-auto" alt="...">
        </a>

        {{-- Collapse --}}
        <div class="navbar-collapse collapse" id="sidebarCollapse">

            {{-- Navigation --}}
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="mr-3">
                            <v-svg src="{{ asset('assets/image/svg/light/dashboard.svg') }}" width="20" height="20"></v-svg>
                        </i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('solution.index') }}">
                        <i class="mr-3">
                            <v-svg src="{{ asset('assets/image/svg/light/lightbulb-on.svg') }}" width="20" height="20"></v-svg>
                        </i>
                        <span>Solution</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('billing.index') }}">
                        <i class="mr-3">
                            <v-svg src="{{ asset('assets/image/svg/light/credit-card-font.svg') }}" width="20" height="20"></v-svg>
                        </i>
                        <span>Billing</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="mr-3">
                            <v-svg src="{{ asset('assets/image/svg/light/sign-out.svg') }}" width="20" height="17"></v-svg>
                        </i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
                        @csrf
                    </form>
                </li>
            </ul>

            {{-- Divider --}}
            <hr class="navbar-divider my-3">

            {{-- Navigation --}}
            @if($route == 'billing')
            @include('layouts.navbars.main.sidebars.billing')
            @elseif($route == 'deployment')
            @include('layouts.navbars.main.sidebars.solution_'.$solution->id)
            @endif

        </div> {{-- / .navbar-collapse --}}

        <ul id="show-sidebar-btn" class="navbar-nav mt-3">
            <li class="nav-item">
                <a class="d-inline-flex nav-link" href="javascript:void(0);" @click="showSidebar(false)">
                    <i class="mr-3">
                        <v-svg src="{{ asset('assets/image/svg/light/arrow-alt-to-left.svg') }}" width="20" height="20"></v-svg>
                    </i>
                </a>
            </li>
        </ul>

    </div>

    <div v-show="!show" id="hide-sidebar-btn" @click="showSidebar(true)">
        <v-svg src="{{ asset('assets/image/svg/regular/chevron-right.svg') }}" width="18" height="18"></v-svg>
    </div>
</nav>

@push('js')
<script src="{{ asset('assets/js/navbars/sidebar.js?v='.time()) }}"></script>
@endpush
