<nav class="navbar navbar-expand-lg navbar-light" id="topnav">
  <div class="container">

    {{-- Toggler --}}
    <button class="navbar-toggler mr-auto" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    {{-- Brand --}}
    <a class="navbar-brand mr-auto" href="index.html">
      <img src="{{ asset('assets/image/logo.svg') }}" alt="..." class="navbar-brand-img">
    </a>

    {{-- Collapse --}}
    <div class="collapse navbar-collapse" id="navbar">

      {{-- Navigation --}}
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="#modalDemo" data-toggle="modal">Dashboard</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('solution.index') }}">Solution</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('billing.index') }}" data-toggle="modal">Billing</a>
        </li>



        @guest
        <li class="nav-item">
          <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        @if(Route::has('register'))
        <li class="nav-item">
          <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
        </li>
        @endif
        @else
        <li class="nav-item">
          <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
            @csrf
          </form>
        </li>
        @endguest



      </ul>
    </div>
  </div>
</nav>
