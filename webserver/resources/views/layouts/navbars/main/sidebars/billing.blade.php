{{-- Heading --}}
<h6 class="navbar-heading">Billing</h6>

<ul id="dashboardSliders" class="navbar-nav">
  <li class="nav-item">
    <a class="nav-link" href="#">
      <span>總覽</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="#">
      <span>付款方式</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="#">
      <span>帳單列表</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="#">
      <span>交易記錄</span>
    </a>
  </li>

</ul>
