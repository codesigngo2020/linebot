{{-- Heading --}}
<h6 class="navbar-heading">Deployment</h6>

<ul id="dashboardSliders" class="navbar-nav">
  <li class="nav-item">
    <a class="nav-link" href="#">
      <span>個人帳號分析</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link collapsed" href="#sidebarFans" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarCourses">
      <span>粉絲分析</span>
      <span class="arrow">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
          <path fill="currentColor" d="M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"></path>
        </svg>
      </sapn>
    </a>
    <div class="collapse" id="sidebarFans">
      <ul class="nav nav-sm flex-column">
        <li class="nav-item">
          <a href="#" class="nav-link">
            <span>整體粉絲分析</span>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <span>單一粉絲分析</span>
          </a>
        </li>
      </ul>
    </div>
  </li>

  <li class="nav-item">
    <a class="nav-link collapsed" href="#sidebarNotFans" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarCourses">
      <span>非粉絲分析</span>
      <span class="arrow">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
          <path fill="currentColor" d="M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"></path>
        </svg>
      </sapn>
    </a>
    <div class="collapse" id="sidebarNotFans">
      <ul class="nav nav-sm flex-column">
        <li class="nav-item">
          <a href="#" class="nav-link">
            <span>整體非粉絲分析</span>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <span>單一非粉絲分析</span>
          </a>
        </li>
      </ul>
    </div>
  </li>

  <li class="nav-item">
    <a class="nav-link collapsed" href="#sidebarPosts" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarCourses">
      <span>貼文分析</span>
      <span class="arrow">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
          <path fill="currentColor" d="M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"></path>
        </svg>
      </sapn>
    </a>
    <div class="collapse" id="sidebarPosts">
      <ul class="nav nav-sm flex-column">
        <li class="nav-item">
          <a href="#" class="nav-link">
            <span>整體貼文分析</span>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <span>單一貼文分析</span>
          </a>
        </li>
      </ul>
    </div>
  </li>

  <li class="nav-item">
    <a class="nav-link collapsed" href="#sidebarAccounts" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarCourses">
      <span>關注帳號分析</span>
      <span class="arrow">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
          <path fill="currentColor" d="M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"></path>
        </svg>
      </sapn>
    </a>
    <div class="collapse" id="sidebarAccounts">
      <ul class="nav nav-sm flex-column">
        <li class="nav-item">
          <a href="#" class="nav-link">
            <span>...</span>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <span>...</span>
          </a>
        </li>
      </ul>
    </div>
  </li>



</ul>
