{{-- Heading --}}
<h6 class="navbar-heading">Deployment</h6>

<ul id="dashboardSliders" class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="{{ route('solution.4.deployment.show', $deployment) }}">
            <span>機器人儀表板</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('solution.4.deployment.richmenus.index', $deployment) }}">
            <span>主選單設定</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#sidebarMessage" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarMessage">
            <span>訊息設定</span>
            <span class="arrow">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                    <path fill="currentColor" d="M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"></path>
                </svg>
            </sapn>
        </a>
        <div class="collapse" id="sidebarMessage">
            <ul class="nav nav-sm flex-column">
                <li class="nav-item">
                    <a href="{{ route('solution.4.deployment.welcomeMessages.index', $deployment) }}" class="nav-link">
                        <span>歡迎訊息</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('solution.4.deployment.autoAnswers.index', $deployment) }}" class="nav-link">
                        <span>自動回覆</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('solution.4.deployment.broadcasts.index', $deployment) }}" class="nav-link">
                        <span>推播訊息</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('solution.4.deployment.messagesPool.index', $deployment) }}" class="nav-link">
                        <span>訊息庫</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('solution.4.deployment.keywords.index', $deployment) }}" class="nav-link">
                        <span>關鍵字</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('solution.4.deployment.scripts.index', $deployment) }}" class="nav-link">
                        <span>訊息腳本</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('solution.4.deployment.liffs.index', $deployment) }}">
            <span>LIFF App</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('solution.4.deployment.tags.index', $deployment) }}">
            <span>行為標籤</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('solution.4.deployment.tables.index', $deployment) }}">
            <span>資料庫</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#sidebarUser" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarUser">
            <span>好友管理</span>
            <span class="arrow">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                    <path fill="currentColor" d="M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"></path>
                </svg>
            </sapn>
        </a>
        <div class="collapse" id="sidebarUser">
            <ul class="nav nav-sm flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('solution.4.deployment.users.index', $deployment) }}">
                        <span>好友列表</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('solution.4.deployment.groups.index', $deployment) }}">
                        <span>好友群組</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('solution.4.deployment.chatroom.index', $deployment) }}">
                        <span>聊天室</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#sidebarSettings" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarSettings">
            <span>部署設定</span>
            <span class="arrow">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                    <path fill="currentColor" d="M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"></path>
                </svg>
            </sapn>
        </a>
        <div class="collapse" id="sidebarSettings">
            <ul class="nav nav-sm flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('solution.4.deployment.iamAdmin.index', $deployment) }}">
                        <span>IAM</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>

</ul>
