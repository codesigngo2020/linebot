{{-- Heading --}}
<h6 class="navbar-heading">Deployment</h6>

<ul id="dashboardSliders" class="navbar-nav">
  <li class="nav-item">
    <a class="nav-link" href="{{ route('solution.1.deployment.show', $deployment) }}">
      <span>部署儀表板</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="{{ route('solution.1.deployment.settings', $deployment) }}">
      <span>部署設定</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="{{ route('solution.1.deployment.crawlers', $deployment) }}">
      <span>爬蟲管理</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="{{ route('solution.1.deployment.posts', $deployment) }}">
      <span>文章管理</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="{{ route('solution.1.deployment.logs', $deployment) }}">
      <span>系統訊息記錄</span>
    </a>
  </li>

</ul>
