{{-- Heading --}}
<h6 class="navbar-heading">Deployment</h6>

<ul id="dashboardSliders" class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="{{ route('solution.5.deployment.show', $deployment) }}">
            <span>EIP 儀表板</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('solution.5.deployment.organization.index', $deployment) }}">
            <span>組織架構</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('solution.5.deployment.show', $deployment) }}">
            <span>個人資料</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#sidebarDocument" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarDocument">
            <span>文件管理</span>
            <span class="arrow">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                    <path fill="currentColor" d="M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"></path>
                </svg>
            </sapn>
        </a>
        <div class="collapse" id="sidebarDocument">
            <ul class="nav nav-sm flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('solution.5.deployment.show', $deployment) }}">
                        <span>文件列表</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('solution.5.deployment.show', $deployment) }}">
                        <span>簽署文件</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('solution.5.deployment.show', $deployment) }}">
            <span>檔案資源</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#sidebarBooking" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarBooking">
            <span>預約系統</span>
            <span class="arrow">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                    <path fill="currentColor" d="M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"></path>
                </svg>
            </sapn>
        </a>
        <div class="collapse" id="sidebarBooking">
            <ul class="nav nav-sm flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('solution.5.deployment.show', $deployment) }}">
                        <span>會議室</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>

</ul>
