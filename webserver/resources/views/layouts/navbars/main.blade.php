@if( preg_match('/^billing/i', request()->route()->action['as'], $match) )

@include('layouts.navbars.main.sidebar', ['route' => 'billing'])

@elseif( preg_match('/^solution.([0-9]+).deployment/i', request()->route()->action['as'], $match) )

@include('layouts.navbars.main.sidebar', ['route' => 'deployment'])

@else

@include('layouts.navbars.main.topbar')

@endif
