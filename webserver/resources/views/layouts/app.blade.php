<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')｜Solution</title>
    {{-- Google Font Nunito --}}
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,400i,600,600i" rel="stylesheet">
    @stack('plugin-css')
    {{-- Theme CSS --}}
    <link href="{{ asset('assets/plugins/theme/theme.min.css?v='.time()) }}" rel="stylesheet">
    {{-- App CSS --}}
    <link href="{{ asset('css/app.css?v='.time()) }}" rel="stylesheet">
    @stack('css')
</head>
<body>
    @include('layouts.navbars.main')

    @yield('content')

    {{-- jQuery --}}
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/plugins/bootstrap/bootstrap.bundle.min.js') }}"></script>

    {{-- vue.js --}}
    {{--<script src="https://cdn.jsdelivr.net/npm/vue"></script>--}}
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    @routes
    @stack('plugin-js')

    {{-- Theme JS --}}
    <script src="{{ asset('assets/plugins/theme/theme.min.js?v='.time()) }}"></script>

    {{-- App JS --}}
    <script src="{{ asset('js/app.js?v='.time()) }}"></script>

    @stack('js')
</body>
</html>
