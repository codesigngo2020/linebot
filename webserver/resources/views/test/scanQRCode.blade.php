@extends('layouts/app')

@section('title', '掃描QR-code')

@push('css')

@endpush

@section('content')
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12 col-lg-10 col-xl-8">

      <video id="video"></video>
      <input type="file" id="file-selector">
      <span id="cam-qr-result-timestamp">Wed May 22 2019 21:37:04 GMT+0800 (台北標準時間)</span>
      <span id="cam-qr-result"></span>

    </div>
  </div>
</div>
@endsection


@push('plugin-js')
<script src="{{ asset('assets/plugins/qr-scanner/qr-scanner.js?v='.time()) }}"></script>
@endpush

@push('js')
<script type="text/javascript">
const video = document.getElementById('video');
//const camHasCamera = document.getElementById('cam-has-camera');
const camQrResult = document.getElementById('cam-qr-result');
//const camQrResultTimestamp = document.getElementById('cam-qr-result-timestamp');
//const fileSelector = document.getElementById('file-selector');
//const fileQrResult = document.getElementById('file-qr-result');

function setResult(label, result) {
  label.textContent = result;
  camQrResultTimestamp.textContent = new Date().toString();
  label.style.color = 'teal';
  clearTimeout(label.highlightTimeout);
  label.highlightTimeout = setTimeout(() => label.style.color = 'inherit', 100);
}

// ####### Web Cam Scanning #######

const scanner = new QrScanner(video, result => setResult(camQrResult, result));
scanner.start();

// ####### File Scanning #######

/*fileSelector.addEventListener('change', event => {
const file = fileSelector.files[0];
if (!file) {
return;
}
QrScanner.scanImage(file)
.then(result => setResult(fileQrResult, result))
.catch(e => setResult(fileQrResult, e || 'No QR code found.'));
});*/
</script>
@endpush
