{{ Form::open(['route'=>['login'], 'method'=>'post', 'id' => 'login_form']) }}

{{-- Email address --}}
<div class="form-group">
  {{ Form::label('email', __('E-Mail Address')) }}
  {{ Form::email('email', old('email'), ['id'=>'email', 'class'=>'form-control'.($errors->has('email') ? ' is-invalid' : ''), 'placeholder'=>'name@address.com', 'autofocus' => true]) }}
  @if($errors->has('email'))
  {!! Form::invalidFeedback($errors->first('email')) !!}
  @endif
</div>

{{-- Password --}}
<div class="form-group">
  <div class="row">
    <div class="col">
      {{-- Label --}}
      {{ Form::label('password', __('Password')) }}
    </div>
    <div class="col-auto">
      {{-- Help text --}}
      <a href="password-reset.html" class="form-text small text-muted">Forgot password？</a>
    </div>
  </div> {{-- / .row --}}

  {{-- Input group --}}
  <div class="input-group input-group-merge">
    {{-- Input --}}
    {{ Form::password('password', ['id'=>'password', 'class'=>'form-control form-control-appended'.($errors->has('password') ? ' is-invalid' : ''), 'placeholder' => 'Enter your password', 'ref' => 'password']) }}
    {{-- Icon --}}
    <div id="show-password" class="input-group-append" v-bind:class="{ show: pass.show }" v-on:click="showPass">
      <span class="input-group-text">
        <v-svg src="{{ asset('assets/image/svg/light/eye.svg') }}" width="18" height="18"></v-svg>
      </span>
    </div>
    @if($errors->has('password'))
    {!! Form::invalidFeedback($errors->first('password')) !!}
    @endif
  </div>
</div>

{{-- Submit --}}
{{ Form::submit('Log in', ['class'=>'btn btn-lg btn-block btn-primary mb-3']) }}

{{-- Link --}}
<div class="text-center">
  <small class="text-muted text-center">Don't have an account yet？ <a href="{{ route('register') }}">Sign up</a>.</small>
</div>

{{ Form::close() }}
