{{ Form::open(['route'=>['register'], 'method'=>'post']) }}


{{-- Name --}}
<div class="form-group">
  {{ Form::label('name', __('Name')) }}
  {{ Form::text('name', old('name'), ['id'=>'name', 'class'=>'form-control'.($errors->has('name') ? ' is-invalid' : ''), 'placeholder'=>'name', 'autofocus' => true]) }}
  @if($errors->has('name'))
  {!! Form::invalidFeedback($errors->first('name')) !!}
  @endif
</div>

<div class="form-group">
  {{ Form::label('email', __('E-Mail Address')) }}
  {{ Form::email('email', old('email'), ['id'=>'email', 'class'=>'form-control'.($errors->has('email') ? ' is-invalid' : ''), 'placeholder'=>'name@address.com', 'autofocus' => true]) }}
  @if($errors->has('email'))
  {!! Form::invalidFeedback($errors->first('email')) !!}
  @endif
</div>

<div class="form-group">
  {{ Form::label('password', __('Password')) }}
  {{ Form::password('password', ['id'=>'password', 'class'=>'form-control'.($errors->has('password') ? ' is-invalid' : ''), 'placeholder' => 'Enter your password']) }}
  @if($errors->has('password'))
  {!! Form::invalidFeedback($errors->first('password')) !!}
  @endif
</div>

<div class="form-group">
  {{ Form::label('password_confirmation', __('Confirm Password')) }}
  {{ Form::password('password_confirmation', ['id'=>'password_confirmation', 'class'=>'form-control', 'placeholder' => 'Confirm your password']) }}
</div>

{{-- Submit --}}
{{ Form::submit('Register', ['class'=>'btn btn-lg btn-block btn-primary mb-3']) }}

{{ Form::close() }}
