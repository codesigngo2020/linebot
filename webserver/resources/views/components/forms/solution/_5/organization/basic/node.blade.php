@push('js')

{{-- 腳本節點 --}}

<script type="text/x-template" id="--node">
    <div class="node">
        <div class="message-label mb-0 d-flex flex-column">
            <p class="badge badge-soft-secondary">
                <pre>@{{ node.name.name }}</pre>
                <input type="text" class="text-center not-input" v-model="node.name.name" placeholder="部門名稱">
            </p>
        </div>
    </div>
</script>

<script>
Vue.component('node', {
    template: '#--node',
    props: ['node'],
    data(){
        return {
            // editting: false,
            // nodeWidth: 'normal',
            //
            // hasQuickReply: false,
            // hoverMessageId: null,
            //
            // counter: 0,
            // currentId: this.getCurrentId(),
            //
            // template: {
            //     isLoading: false,
            //     options: [],
            // },

            form: {
                node: this.node,
            },
        }
    },
    methods: {
        // editNode(){
        //     if(!this.editting){
        //         this.$emit('editNode', this.node.id);
        //         this.$emit('setHoverMessage', null);
        //     }
        // },
        // toggleMessageTemplate(){
        //     if(this.form.node.type == 'message'){
        //         this.form.node.type = 'template';
        //         this.nodeWidth = 'normal';
        //     }else{
        //         this.form.node.type = 'message';
        //         this.setNodeWidth();
        //     }
        // },
        // closeEditNode(){
        //     this.$emit('closeEditNode', this.node.id);
        //     this.$emit('setHoverMessage', null);
        // },
        // dragenter(event){
        //     this.counter ++;
        //     $(event.target).addClass('dragover');
        // },
        // dragleave(event){
        //     this.counter --;
        //     if(this.counter === 0) $(event.target).removeClass('dragover');
        // },
        // drop(index){
        //     this.counter = 0;
        //     this.newMessage(index);
        // },
        // isDropzoneActive(){
        //     if(this.newMessageType == 'quickreply-message' && (this.index == this.getMessagesCount() - 1 && !this.hasQuickReply)){
        //         return true;
        //     }else if(this.newMessageType != 'quickreply-message' && this.getMessagesCount() < 5){
        //         return true;
        //     }
        //     return false;
        // },
        // getCurrentId(){
        //     return this.node.messages.messages.reduce((max, message) => Math.max(max, message.id.split('-')[1]), 1);
        // },
        // getMessagesCount(){
        //     return this.node.messages.messages.filter(message => message.type != 'quickreply').length;
        // },
        // // updateMessageData(index, data){
        // //     this.$set(this.form.node.messages.messages[index], 'message', data);
        // //     this.$emit('updateNodeData', this.node.id, this.form.node);
        // // },
        // insertNewNode(parentEnd){
        //     this.$emit('insertNewNode', parentEnd)
        // },
        // removeNode(id){
        //     this.$emit('removeNode', id)
        // },
        // setNodeWidth(){
        //     node = this.form.node.messages.messages.find(function(node){
        //         return ['imagemap', 'confirm', 'buttons', 'carousel', 'imagecarousel', 'quickreply'].includes(node.type);
        //     });
        //     this.nodeWidth = node ? 'wide' : 'normal';
        // },
        // recurseImagemap(action, obj){
        //     for(key in obj){
        //         if(obj[key].id && (obj[key].type.value == 'next' || obj[key].type.value == 'next-with-message')){
        //             if(action == 'removeNode'){
        //                 this.removeNode(obj[key].action.nextId.nextId);
        //             }else if(action == 'setHoverMessage'){
        //                 this.$emit('setHoverMessage', obj[key].action.nextId.nextId);
        //             }
        //         }else if(obj[key][0]){
        //             this.recurseImagemap(action, obj[key]);
        //         }
        //     }
        // },
        // traverseMessages(actionName, index){
        //     var vm = this,
        //     message = this.form.node.messages.messages[index];
        //
        //     switch(message.type){
        //         case 'imagemap':
        //         this.recurseImagemap(actionName, message.message.area.area);
        //         break;
        //
        //         case 'buttons':
        //         case 'confirm':
        //         message.message.actions.forEach(function(action){
        //             if(action.type.value == 'next' || action.type.value == 'next-with-message') vm.$emit(actionName, action.action.nextId.nextId);
        //         });
        //         break;
        //
        //         case 'carousel':
        //         case 'imagecarousel':
        //         case 'quickreply':
        //         eval('message.message.' + (message.type == 'quickreply' ? 'items.items' : 'columns.columns')).forEach(function(column){
        //             column.actions.forEach(function(action){
        //                 if(action.type.value == 'next' || action.type.value == 'next-with-message') vm.$emit(actionName, action.action.nextId.nextId);
        //             });
        //         });
        //         break;
        //
        //         default:
        //     }
        // },
        // mouseenter(index, id){
        //     this.$emit('setHoverMessage', id);
        //     this.traverseMessages('setHoverMessage', index);
        // },
        // mouseleave(){
        //     this.$emit('setHoverMessage', null);
        // },
        // removeMessage(index){
        //     if(this.form.node.messages.messages[index].type == 'quickreply' || this.form.node.messages.messages.length > 1){
        //         if(this.form.node.messages.messages[index].type == 'quickreply') this.hasQuickReply = false;
        //         this.traverseMessages('removeNode', index);
        //
        //         this.form.node.messages.messages.splice(index, 1);
        //         this.$emit('updateNodeData', this.node.id, this.form.node);
        //         this.setNodeWidth();
        //
        //     }
        // },
        // setHoverNode(id){
        //     this.$emit('setHoverNode', id);
        // },
        // newMessage(index){
        //     if((this.getMessagesCount() < 5 && scriptForm.newMessageType) || this.newMessageType == 'quickreply-message'){
        //         messageType = scriptForm.newMessageType.match(/(.+)-message/);
        //         eval('this.new'+messageType[1].capitalize()+'Message')(index);
        //         // this.$emit('updateMessageData', this.form.node);
        //         this.setNodeWidth();
        //     }
        // },
        // newTextMessage(index){
        //     this.currentId ++;
        //     this.form.node.messages.messages.splice(index, 0, {
        //         id: this.node.id+'-'+this.currentId,
        //         type: 'text',
        //         message: {
        //             text: {
        //                 text: '',
        //                 info: '',
        //                 invalid: false,
        //             }
        //         }
        //     });
        // },
        // newImageMessage(index){
        //     this.currentId ++;
        //     this.form.node.messages.messages.splice(index, 0, {
        //         id: this.node.id+'-'+this.currentId,
        //         type: 'image',
        //         message: {
        //             image: this.defaultData.image,
        //         }
        //     });
        // },
        // newImagemapMessage(index){
        //     area = [];
        //     for(var i = 0; i < 2; i ++){
        //         area[i] = [];
        //         for(var j = 0; j < 2; j ++){
        //             area[i].push({
        //                 id: 2*i+j+1,
        //                 type: {
        //                     name: '好友回覆關鍵字',
        //                     value: 'keyword',
        //                 },
        //                 action: {
        //                     keyword: {
        //                         value: null,
        //                         info: '',
        //                         invalid: false,
        //                     },
        //                     text: {
        //                         text: '',
        //                         info: '',
        //                         invalid: false,
        //                     },
        //                 },
        //                 selected: false,
        //                 invalid: false,
        //             });
        //         }
        //     };
        //
        //     this.currentId ++;
        //     this.form.node.messages.messages.splice(index, 0, {
        //         id: this.node.id+'-'+this.currentId,
        //         type: 'imagemap',
        //         message: {
        //             altText: {
        //                 altText: '',
        //                 info: '',
        //                 invalid: false,
        //             },
        //             image: this.defaultData.image,
        //             show: {
        //                 active: true,
        //                 info: '',
        //                 invalid: false,
        //             },
        //             area: {
        //                 area: area,
        //                 info: '',
        //                 invalid: false,
        //             },
        //         }
        //     });
        // },
        // newButtonsMessage(index){
        //     this.currentId ++;
        //     this.form.node.messages.messages.splice(index, 0, {
        //         id: this.node.id+'-'+this.currentId,
        //         type: 'buttons',
        //         message: {
        //             title: {
        //                 title: '',
        //                 info: '',
        //                 invalid: false,
        //             },
        //             text: {
        //                 text: '',
        //                 info: '',
        //                 invalid: false,
        //             },
        //             altText: {
        //                 altText: '',
        //                 info: '',
        //                 invalid: false,
        //             },
        //             image: $.extend(true, {
        //                 active: {
        //                     active: true,
        //                     info: '',
        //                     invalid: false,
        //                 },
        //                 ratio: {
        //                     ratio: 'rectangle',
        //                     info: '',
        //                     invalid: false,
        //                 }
        //             }, this.defaultData.image),
        //             actions: [
        //                 $.extend(true, {id: 1}, this.defaultData.action),
        //             ],
        //             // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
        //         }
        //     });
        // },
        // newConfirmMessage(index){
        //     this.currentId ++;
        //     this.form.node.messages.messages.splice(index, 0, {
        //         id: this.node.id+'-'+this.currentId,
        //         type: 'confirm',
        //         message: {
        //             text: {
        //                 text: '',
        //                 info: '',
        //                 invalid: false,
        //             },
        //             altText: {
        //                 altText: '',
        //                 info: '',
        //                 invalid: false,
        //             },
        //             actions: [
        //                 $.extend(true, {id: 1}, this.defaultData.action),
        //                 $.extend(true, {id: 2}, this.defaultData.action),
        //             ],
        //             // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
        //         },
        //     });
        // },
        // newCarouselMessage(index){
        //     this.currentId ++;
        //     this.form.node.messages.messages.splice(index, 0, {
        //         id: this.node.id+'-'+this.currentId,
        //         type: 'carousel',
        //         message: {
        //             altText: {
        //                 altText: '',
        //                 info: '',
        //                 invalid: false,
        //             },
        //             columns: {
        //                 info: '',
        //                 invalid: false,
        //                 columns:[
        //                     {
        //                         id: 1,
        //                         title: {
        //                             title: '',
        //                             info: '',
        //                             invalid: false,
        //                         },
        //                         text: {
        //                             text: '',
        //                             info: '',
        //                             invalid: false,
        //                         },
        //                         image: this.defaultData.image,
        //                         actions: [
        //                             $.extend(true, {id: 1}, this.defaultData.action),
        //                         ],
        //                         // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
        //                     }
        //                 ],
        //             },
        //             title: {
        //                 active: {
        //                     active: true,
        //                     info: '',
        //                     invalid: false,
        //                 },
        //             },
        //             image: {
        //                 active: {
        //                     active: true,
        //                     info: '',
        //                     invalid: false,
        //                 },
        //                 ratio: {
        //                     ratio: 'rectangle',
        //                     info: '',
        //                     invalid: false,
        //                 }
        //             },
        //         },
        //     });
        // },
        // newImagecarouselMessage(index){
        //     this.currentId ++;
        //     this.form.node.messages.messages.splice(index, 0, {
        //         id: this.node.id+'-'+this.currentId,
        //         type: 'imagecarousel',
        //         message: {
        //             altText: {
        //                 altText: '',
        //                 info: '',
        //                 invalid: false,
        //             },
        //             columns: {
        //                 info: '',
        //                 invalid: false,
        //                 columns:[
        //                     {
        //                         id: 1,
        //                         image: this.defaultData.image,
        //                         actions: [
        //                             $.extend(true, {id: 1}, this.defaultData.action),
        //                         ],
        //                         // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
        //                     }
        //                 ],
        //             },
        //         },
        //     });
        // },
        // newQuickreplyMessage(index){
        //     this.hasQuickReply = true;
        //
        //     this.currentId ++;
        //     this.form.node.messages.messages.splice(index, 0, {
        //         id: this.node.id+'-'+this.currentId,
        //         type: 'quickreply',
        //         message: {
        //             items: {
        //                 info: '',
        //                 invalid: false,
        //                 items:[
        //                     {
        //                         id: 1,
        //                         image: this.defaultData.image,
        //                         actions: [
        //                             $.extend(true, {id: 1}, this.defaultData.action),
        //                         ],
        //                         // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
        //                     }
        //                 ],
        //             },
        //         },
        //     });
        // },
        // getTemplateUrl(id){
        //     return route('solution.4.deployment.templates.show', {
        //         deployment: args.deployment,
        //         templateId: id,
        //     });
        // },
        // templateslimitText(template){
        //     return `and ${template} other templates`;
        // },
        // getTemplates(query){
        //     this.template.isLoading = true;
        //     axios
        //     .get(route('solution.4.deployment.messagesPool.queryForSelect', {
        //         deployment: args.deployment,
        //         type: 'template',
        //         q: query,
        //     }))
        //     .then(response => {
        //         this.template.options = response.data;
        //         this.template.isLoading = false;
        //         console.log(response.data);
        //     }).catch(error => {
        //         this.template.options = [];
        //         this.template.isLoading = false;
        //         console.log(error);
        //     });
        // },
    },
    created(){
        // this.setNodeWidth();
        // this.getTemplates('');
    },
    watch: {
        // edittingNodesId(newValue){
        //     this.editting = newValue.includes(this.node.id)
        // }
    }
});
</script>
@endpush
