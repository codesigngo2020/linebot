@push('js')

{{-- 組織架構分支 --}}

<script type="text/x-template" id="--branch">
    <ul class="tree-ul">
        <li v-for="node in filterThisLayerNodes()" :key="node.id" class="tree-li">
            <node :node="node"></node>
            <branch v-if="node.end - node.start != 1" :nodes="filterSubNodes(node.start, node.end)"></branch>
        </li>
    </ul>
</script>

<script>
Vue.component('branch', {
    template: '#--branch',
    props: ['nodes'],
    data(){
        return {
            form: {
                organization: this.organization,
            },
        }
    },
    methods: {
        getMinStartOfNodes(){
            return this.nodes.reduce((min, node) => Math.min(min, node.start), 9999);
        },
        getMaxEndOfNodes(){
            return this.nodes.reduce((max, node) => Math.max(max, node.end), 0);
        },
        filterThisLayerNodes(){
            thisLayerNodes = [];
            start = this.getMinStartOfNodes() - 1;
            end = this.getMaxEndOfNodes();

            do{
                node = this.nodes.find(node => node.start == start + 1);
                thisLayerNodes.push(node);
                start = node.end;
            }while(end > node.end)

            return thisLayerNodes;
        },
        filterSubNodes(start, end){
            return this.nodes.filter(function(node){
                return node.start > start && node.end < end;
            });
        },
        // editNode(id){
        //     this.$emit('editNode', id);
        // },
        // closeEditNode(id){
        //     this.$emit('closeEditNode', id);
        // },
        // insertNewNode(parentEnd){
        //     this.$emit('insertNewNode', parentEnd);
        // },
        // removeNode(id){
        //     this.$emit('removeNode', id);
        // },
        // setHoverNode(id){
        //     this.$emit('setHoverNode', id);
        // },
        // setHoverMessage(id){
        //     this.$emit('setHoverMessage', id);
        // },
        // // updateNodeData(id, node){
        // //     this.$emit('updateNodeData', id, node);
        // // }
    }
});
</script>

@endpush
