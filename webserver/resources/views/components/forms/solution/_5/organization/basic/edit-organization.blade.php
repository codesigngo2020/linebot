@push('js')

{{-- 編輯組織架構 --}}

<script type="text/x-template" id="--edit-organization">
    <div id="edit-organization-layout" class="overflow-hidden pl-4">

        <div id="tree" class="overflow-auto">
            <!-- <ul class="tree-ul justify-content-start">
                <li class="tree-li"> -->

                    <!-- <div v-if="form.organization.organization.nodes.nodes.length == 0" class="node">
                        <div class="message-label mb-0">
                            <p class="badge badge-soft-secondary">
                                <pre ref="inputPre"></pre>
                                <input type="text" placeholder="部門名稱">
                            </p>
                        </div>
                    </div> -->


                    <!-- <branch :nodes="form.organization.organization.nodes.nodes"></branch>
                </li>
            </ul> -->

            <branch :nodes="form.organization.organization.nodes.nodes"></branch>
        </div>

        <!-- <component v-for="(message, index) in form.messages" :key="message.id" :is="message.type+'-message'" :index="index" :messagesCount="form.messages.length" :message="message.message" :actions="['buttons', 'imagemap', 'confirm', 'carousel'].includes(message.type) ? actions : null" @updateMessageData="updateMessageData" @removeMessage="removeMessage" @newMessage="newMessage"></component> -->

    </div>
</script>

<script>
Vue.component('edit-organization', {
    template: '#--edit-organization',
    props: ['organization'],
    data(){
        return {
            // edittingNodesId: [],
            //
            currentNodeId: 0,
            //
            // hoverNodeId: null,
            // hoverMessagesId: [],

            form: {
                organization: this.organization,
            },

        }
    },
    methods: {
        getCurrentNodeId(){
            return this.form.organization.organization.nodes.nodes.reduce((max, node) => Math.max(max, node.id), 0);
        },
        // editNode(id){
        //     if(this.edittingNodesId.length == 0){
        //         this.edittingNodesId.push(id);
        //     }else{
        //         var vm = this;
        //         newEdittingNodesId = [id];
        //         // 原有的node只留下在新node的父或子node
        //         newNode = this.form.script.script.nodes.nodes.find(node => node.id == id);
        //
        //         // 檢查父node
        //         filterdNodes = this.form.script.script.nodes.nodes.filter(function(node){
        //             return vm.edittingNodesId.includes(node.id) && node.start < newNode.start && node.end > newNode.end;
        //         });
        //
        //         if(filterdNodes.length != 0){
        //             parentNode = this.form.script.script.nodes.nodes.reduce(function(parentNode, node){
        //
        //                 if(node.start < newNode.start && node.end > newNode.end){
        //                     return (!parentNode || node.start > parentNode.start) ? node : parentNode;
        //                 }
        //                 return parentNode;
        //
        //             }, null);
        //             parentNode = filterdNodes.find(node => node.id == parentNode.id);
        //             if(parentNode) newEdittingNodesId.push(parentNode.id);
        //         }
        //
        //         this.edittingNodesId = newEdittingNodesId;
        //
        //         // // 檢查子node
        //         // if(newNode.end - newNode.start != 1){
        //         //     filterdNodes = this.form.script.script.nodes.nodes.filter(function(node){
        //         //         return vm.edittingNodesId.includes(node.id) && node.start > newNode.start && node.end < newNode.end;
        //         //     });
        //         //
        //         //     if(filterdNodes.length != 0){
        //         //         childNodes = this.filterSubNodes(newNode.start, newNode.end);
        //         //         thisLayerNodes = this.filterThisLayerNodes(childNodes);
        //         //
        //         //         thisLayerNodes.find('')
        //         //     }
        //         // }
        //     }
        // },
        // filterThisLayerNodes(start, end){
        //     thisLayerNodes = [];
        //
        //     do{
        //         node = this.form.script.script.nodes.nodes.find(node => node.start == start + 1);
        //         if(node){
        //             thisLayerNodes.push(node);
        //             start = node.end;
        //         }else{
        //             break;
        //         }
        //     }while(end > node.end)
        //
        //     return thisLayerNodes;
        // },
        // closeEditNode(id){
        //     this.edittingNodesId.splice(this.edittingNodesId.indexOf(id), 1);
        //     if(this.hoverNodeId){
        //         closedNode = this.form.script.script.nodes.nodes.find(node => node.id == id);
        //         if(this.filterThisLayerNodes(closedNode.start, closedNode.end).findIndex(node => node.id == this.hoverNodeId) != -1){
        //             this.hoverNodeId = null;
        //         }
        //     }
        // },
        // insertNewNode(parentEnd){
        //     this.currentNodeId ++;
        //
        //     this.form.script.script.nodes.nodes.forEach(function(node){
        //         if(node.end >= parentEnd){
        //             if(node.start > parentEnd) node.start += 2;
        //             node.end += 2;
        //         }
        //     });
        //
        //     this.form.script.script.nodes.nodes.push({
        //         id: this.currentNodeId,
        //         start: parentEnd,
        //         end: parentEnd + 1,
        //         messages: {
        //             messages: [
        //                 {
        //                     id: this.currentNodeId+'-1',
        //                     type: 'text',
        //                     message: {
        //                         text: {
        //                             text: '',
        //                             info: '',
        //                             invalid: false,
        //                         }
        //                     },
        //                     info: '',
        //                     invalid: false,
        //                 },
        //             ],
        //         }
        //     });
        // },
        // removeNode(id){
        //     var vm = this;
        //     index = this.form.script.script.nodes.nodes.findIndex(node => node.id == id);
        //     removedNode = this.form.script.script.nodes.nodes[index];
        //     minusNum = removedNode.end - removedNode.start + 1;
        //     removedNodes = [];
        //
        //     this.form.script.script.nodes.nodes.forEach(function(node, index){
        //         if(node.start >= removedNode.start && node.end <= removedNode.end){
        //             removedNodes.push(index);
        //         }else if(node.end >= removedNode.end){
        //             if(node.start > removedNode.start) node.start -= minusNum;
        //             node.end -= minusNum;
        //         }
        //     });
        //
        //     removedNodes.sort((a, b) => b - a).forEach(function(index){
        //         vm.form.script.script.nodes.nodes.splice(index, 1);
        //     })
        // },
        // setHoverNode(id){
        //     this.hoverNodeId = id;
        // },
        // setHoverMessage(id){
        //     if(id){
        //         this.hoverMessagesId.push(id);
        //     }else{
        //         this.hoverMessagesId = [];
        //     }
        // },
    },
    created(){
        this.currentNodeId = this.getCurrentNodeId();
    }
});
</script>

@endpush
