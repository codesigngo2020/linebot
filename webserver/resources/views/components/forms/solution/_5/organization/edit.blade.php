<div id="organization-form" class="mb-6" v-cloak>

    {{-- 編輯組織架構 --}}
    @include('components.forms.solution._5.organization.basic.edit-organization')
    @include('components.forms.solution._5.organization.basic.branch')
    @include('components.forms.solution._5.organization.basic.node')

    <label for="organization">組織架構<span class="form-required">*</span></label>

    <edit-organization :organization="form.organization"></edit-organization>

    <hr class="mt-5 mb-5">

    {{ Form::button('更新組織架構', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, "btn btn-block btn-primary"]', '@click'=>'submit']) }}

</div>
