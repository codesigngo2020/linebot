@push('js')
<script type="text/x-template" id="--nav-form-3">
  <div {!! $action == 'deploy' ? 'class="tab-pane fade" id="nav-form-3" role="tabpanel" aria-labelledby="nav-form-3-tab"' : '' !!}>
    @if($action == 'deploy')
    <div class="deployment-info mb-4">
      <div class="info-item">您可以建立至多5隻爬蟲，每隻爬蟲可以選擇不同的爬取方式。為爬取的文章分類並貼上標籤，方便您在Wordpress上管理。最後，請參考<a href="#"><strong>Wordpress文章格式說明文件</strong></a>，設定文章格式，爬蟲將以此格式建立文章。</div>
    </div>
    @endif

    <ul id="crawler-nav-tabs" class="nav nav-tabs nav-overflow mb-4" role="tablist">
      <li class="nav-item" v-for="(crawler, i) in form.crawlers">
        <a :class="[i == 1 ? 'active' : '', 'nav-link']" data-toggle="tab" :href="'#nav-crawler-'+i" role="tab" :aria-controls="'nav-crawler-'+i" aria-selected="true" :ref="'crawler'+i">爬蟲 @{{ i }}</a>
      </li>
      <li class="nav-item">
        <div id="new-crawler" class="nav-link" v-if="Object.keys(form.crawlers).length < crawlersCount" @click="newCrawler"><v-svg class="mr-2" src="{{ asset('assets/image/svg/light/plus-circle.svg') }}" width="18" height="18"></v-svg>新增爬蟲</div>
      </li>
    </ul>

    <div class="tab-content">
      {{-- 建立爬蟲 --}}
      <crawler-tab v-for="(crawler, i) in form.crawlers" :key="crawler.id" :id="crawler.id" :index="i" :postsCount="postsCount" @updateFormData="updateFormData" @destroyFormData="destroyFormData"></crawler-tab>
    </div>
  </div>
</script>


{{-- 建立爬蟲 選擇格式 --}}
<script type="text/x-template" id="crawler-tab">
  <div :class="[index == 1 ? 'show active' : '', 'position-relative pt-3 tab-pane fade']" :id="'nav-crawler-'+index" role="tabpanel" :aria-labelledby="'nav-form-'+index+'-tab'">
    <div class="form-group">
      <label :for="'crawler-type-'+index">爬蟲類型<span class="form-required">*</span></label>
      <vue-multiselect :class="[{'is-invalid' : form.crawler_type.invalid}]" v-model="form.crawler_type.value" track-by="value" label="name" placeholder="請選擇爬蟲類型" :options="form.crawler_type.options" :searchable="false" :show-labels="false" :allow-empty="false"></vue-multiselect>
      <span class="invalid-feedback" role="alert"><strong>@{{ form.crawler_type.info }}</strong></span>
    </div>

    <button type="button" class="position-absolute top right d-flex align-items-center btn btn-outline-danger btn-sm" @click="destroyCrawler">
      <v-svg src="{{ asset('assets/image/svg/light/trash-alt.svg') }}" width="14" height="14"></v-svg>
      <span class="ml-2">刪除爬蟲</span>
    </button>


    {{-- 建立爬蟲 爬蟲詳細資料 --}}
    <crawler-form :type="form.crawler_type.value.value" v-if="form.crawler_type.value != null" :index="index" :postsCount="postsCount" @updateCrawler="updateCrawler"></crawler-form>
  </div>
</script>


{{-- 建立爬蟲 表單 --}}
<script type="text/x-template" id="crawler-form">
  <div>
    <div class="form-group">
      <label :for="'username-'+index">IG 帳號名稱<span class="form-required">*</span></label>
      <div class="d-sm-flex i-form-control" :class="{'is-invalid' : crawler.username.invalid}">
        <input :id="'username-'+index" :class="[{'is-invalid' : crawler.username.invalid}, 'form-control']" v-model="crawler.username.username" placeholder="Instagram 帳號名稱" type="text">
      </div>
      <span class="invalid-feedback" role="alert"><strong>@{{ crawler.username.info }}</strong></span>
    </div>

    <div class="form-group">
      <label :for="'duration-'+index">執行時間<span class="form-required">*</span></label>
      <input :id="'duration-'+index" type="text" :class="[{'is-invalid' : crawler.duration.invalid}, 'form-control']" data-mask="AB : CB - AB : CB" placeholder="00 : 00 - 23 : 59" ref="duration">
      <span class="invalid-feedback" role="alert"><strong>@{{ crawler.duration.info }}</strong></span>
    </div>

    <div class="form-group">
      <label :for="'frequency-'+index">IG 請求頻率<span class="form-required">*</span><span>：每 @{{ crawler.frequency.value }} 分鐘</span></label>
      <input type="range" :class="[{'is-invalid' : crawler.frequency.invalid}, 'custom-range']" min="1" max="60" step="1" :id="'frequency-'+index" v-model="crawler.frequency.value">
      <span class="invalid-feedback" role="alert"><strong>@{{ crawler.frequency.info }}</strong></span>
    </div>

    <div class="form-group">
      <label :for="'postsLimit-'+index">刊登文章上限<span class="form-required">*</span><span>：每日 @{{ crawler.postsLimit.value }} 篇</span></label>
      <input type="range" :class="[{'is-invalid' : crawler.postsLimit.invalid}, 'custom-range']" min="1" :max="postsCount" step="1" :id="'postsLimit-'+index" v-model="crawler.postsLimit.value">
      <span class="invalid-feedback" role="alert"><strong>@{{ crawler.postsLimit.info }}</strong></span>
    </div>

    <div class="form-group">
      <label :for="'post-format-'+index">文章格式<span class="form-required">*</span></label>
      <textarea :id="'post-format-'+index" :class="[{'is-invalid' : crawler.post.format.invalid}, 'form-control']" v-model="crawler.post.format.content" placeholder="請參考「Wordpress文章格式說明文件」填寫您的文章格式。" rows="5"></textarea>
      <span class="invalid-feedback" role="alert"><strong>@{{ crawler.post.format.info }}</strong></span>
    </div>

    <div class="form-group">
      <label :for="'category-'+index">文章類別<span class="form-required">*</span></label>
      <vue-multiselect :class="[{'is-invalid' : crawler.post.category.invalid}, crawler.post.category.value.length > 0 ? 'selected' : '', 'taggable hidden-arrow']" :id="'category-'+index" v-model="crawler.post.category.value" placeholder="請輸入類別名稱" :options="crawler.post.category.options" :show-labels="false" :allow-empty="true" :multiple="true" :taggable="true" :limit="5" @tag="addCategory" @remove="removeCategory"></vue-multiselect>
      <span class="invalid-feedback" role="alert"><strong>@{{ crawler.post.category.info }}</strong></span>
    </div>

    <div class="form-group">
      <label :for="'tag-'+index">文章標籤<span class="form-required">*</span></label>
      <vue-multiselect :class="[{'is-invalid' : crawler.post.tag.invalid}, crawler.post.tag.value.length > 0 ? 'selected' : '', 'taggable hidden-arrow']" v-model="crawler.post.tag.value" placeholder="請輸入標籤名稱" :options="crawler.post.tag.options" :show-labels="false" :allow-empty="true" :multiple="true" :taggable="true" :limit="5" @tag="addTag" @remove="removeTag"></vue-multiselect>
      <span class="invalid-feedback" :id="'tag-'+index" role="alert"><strong>@{{ crawler.post.tag.info }}</strong></span>
    </div>

    <hr class="mt-4 mb-4">

    <p class="color-light">其他文章設定</p>

    <div class="form-group">
      <div :class="[{'is-invalid' : crawler.post.options.invalid}, 'i-form-control']">
        <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input" :id="'link-active-'+index" value="link-active" v-model="crawler.post.options.value">
          <label :class="[crawler.post.options.value.includes('link-active')? '' : 'color-light', 'custom-control-label']" :for="'link-active-'+index">啟用文章連結</label>
        </div>

        <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input" :id="'close-comment-'+index" value="close-comment" v-model="crawler.post.options.value">
          <label :class="[crawler.post.options.value.includes('close-comment')? '' : 'color-light', 'custom-control-label']" :for="'close-comment-'+index">關閉留言功能</label>
        </div>

        <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input" :id="'remove-tags-from-post-'+index" value="remove-tags-from-post" v-model="crawler.post.options.value">
          <label :class="[crawler.post.options.value.includes('remove-tags-from-post')? '' : 'color-light', 'custom-control-label']" :for="'remove-tags-from-post-'+index">移除instagram文章中的hashtag</label>
        </div>

        <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input" :id="'include-origin-tags-'+index" value="include-origin-tags" v-model="crawler.post.options.value">
          <label :class="[crawler.post.options.value.includes('include-origin-tags')? '' : 'color-light', 'custom-control-label']" :for="'include-origin-tags-'+index">文章標籤包含instagram文章自帶標籤</label>
        </div>

        <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input" :id="'publish-time-as-post-time-'+index" value="publish-time-as-post-time" v-model="crawler.post.options.value">
          <label :class="[crawler.post.options.value.includes('publish-time-as-post-time')? '' : 'color-light', 'custom-control-label']" :for="'publish-time-as-post-time-'+index">使用instagram文章發布時間作為文章建立時間，預設為爬取文章時間</label>
        </div>
      </div>


      <span class="invalid-feedback" role="alert"><strong>@{{ crawler.post.options.info }}</strong></span>
    </div>
  </div>
</script>


<script>
Vue.component('nav-form-3', {
  template: '#--nav-form-3',
  props: ['crawlersCount', 'postsCount'],
  data(){
    return {
      children_counts: args.children_counts ? args.children_counts : 1,
      form: {
        crawlers: args.crawlers ? args.crawlers : {
          1: {
            id: 1,
          }
        },
      }
    }
  },
  methods: {
    newCrawler(){
      var crawlers_count = this.getCrawlersCount();
      if(crawlers_count < this.crawlersCount){
        // 如果是編輯，新增時刪除初始資料
        if(args.crawlers) delete args.crawlers;

        this.children_counts ++;
        this.$set(this.form.crawlers, crawlers_count + 1, {
          id: this.children_counts,
        });
        this.setCrawlerOrderToForm4();
      }
    },
    setCrawlerOrderToForm4(){
      deploy_form.$refs.form4.setCrawlerOrder($.extend({},this.form.crawlers));
    },
    updateFormData(index, crawler){
      this.form.crawlers[index] = crawler;
      this.$emit('update-form-data', 3, this.form);
    },
    destroyFormData(index){
      var index = parseInt(index),
      crawlers_count = this.getCrawlersCount();
      if(crawlers_count > 1){
        delete this.form.crawlers[index];

        var vm = this,
        new_crawlers = {},
        _index = 1;
        Object.keys(vm.form.crawlers).forEach(function(key){
          new_crawlers[_index] = vm.form.crawlers[key];
          _index ++;
        });

        // 切換tab
        var tab_index = index == crawlers_count ? index - 1 : index;
        $(eval('this.$refs.crawler'+tab_index)).tab('show');

        this.$set(this.form, 'crawlers', new_crawlers);
        this.$emit('update-form-data', 3, this.form);
        this.setCrawlerOrderToForm4();
      }
    },
    getCrawlersCount(){
      return Object.keys(this.form.crawlers).length;
    }
  },
  created(){
    //this.$emit('update-form-data', 3, this.form);
  },
  updated(){
    this.$emit('update-form-data', 3, this.form);
  },
  watch: {
    crawlersCount(newValue, oldValue){
      if(newValue < oldValue){
        if(newValue < this.getCrawlersCount()){
          var vm = this;
          Object.keys(vm.form.crawlers).forEach(function(key){
            if(key > newValue) delete vm.form.crawlers[key];
          });
        }
        $(eval('this.$refs.crawler'+newValue)).tab('show');
      }
    },
  }
});


// 建立爬蟲 選擇格式
Vue.component('crawler-tab', {
  template: '#crawler-tab',
  props: ['id', 'index', 'type', 'postsCount'],
  data(){
    var options = {
      user: { name: '特定使用者', value: 'user' },
    },
    crawler = args.crawlers ? args.crawlers[this.index] : null;

    return {
      form: {
        id: this.id,
        crawler_type: {
          value: crawler ? options[crawler.type] : null,
          options: Object.values(options),
          info: '',
          invalid: false,
        },
        crawler: {},
      }
    }
  },
  methods: {
    updateCrawler(crawler){
      this.$set(this.form, 'crawler', crawler);
      this.$emit('updateFormData', this.index, this.form);
    },
    destroyCrawler(){
      this.$emit('destroyFormData', this.index);
    }
  },
  created(){
    //this.$emit('updateFormData', this.index, this.form);
  },
  updated(){
    this.$emit('updateFormData', this.index, this.form);
  },
});


// 建立爬蟲 表單
Vue.component('crawler-form', {
  template: '#crawler-form',
  props: ['index', 'type', 'postsCount'],
  data(){
    var crawler = args.crawlers ? args.crawlers[this.index] : null;

    return {
      crawler:{
        username: {
          username: crawler ? crawler.account.username : '',
          info: '',
          invalid: false,
        },
        duration: {
          duration: crawler ? crawler.start.replace(':', ' : ')+' - '+crawler.end.replace(':', ' : ') : '',
          info: '',
          invalid: false,
        },
        frequency: {
          value: crawler ? crawler.frequency : 5,
          info: '',
          invalid: false,
        },
        postsLimit: {
          value: crawler ? crawler.posts_limit : 1,
          info: '',
          invalid: false,
        },
        post: {
          format: {
            content: crawler ? crawler.format : '',
            info: '',
            invalid: false,
          },
          category: {
            value: crawler ? crawler.categories : ['Wordpress', 'Instagram爬蟲'],
            options: crawler ? crawler.categories : ['Wordpress', 'Instagram爬蟲'],
            info: '',
            invalid: false,
          },
          tag: {
            value: crawler ? crawler.tags : ['Wordpress', 'Instagram爬蟲'],
            options: crawler ? crawler.tags : ['Wordpress', 'Instagram爬蟲'],
            info: '',
            invalid: false,
          },
          options: {
            value: crawler ? crawler.options : [],
            info: '',
            invalid: false,
          }
        },
      }
    };
  },
  methods: {
    addCategory(category){
      if(this.crawler.post.category.value.length < 5){
        this.crawler.post.category.options.push(category);
        this.crawler.post.category.value.push(category);
      }
    },
    removeCategory(category){
      this.crawler.post.category.options.splice(this.crawler.post.category.options.indexOf(category), 1);
    },
    addTag(tag){
      if(this.crawler.post.tag.value.length < 5){
        this.crawler.post.tag.options.push(tag);
        this.crawler.post.tag.value.push(tag);
      }
    },
    removeTag(tag){
      this.crawler.post.tag.options.splice(this.crawler.post.tag.options.indexOf(tag), 1);
    },
    changeCrawlerType(type){

    }
  },
  created(){
    this.changeCrawlerType(this.type);
    this.$emit('updateCrawler', this.crawler);
  },
  mounted(){
    var vm = this;
    $(this.$refs.duration).mask(this.$refs.duration.getAttribute('data-mask'), {
      translation: {
        'A': {
          pattern: /[0-2]/
        },
        'B': {
          pattern: /[0-9]/
        },
        'C': {
          pattern: /[0-6]/
        }
      },
      onChange(cep){
        vm.crawler.duration.duration = cep;
      },
    });
    $(this.$refs.duration).val(this.crawler.duration.duration);
  },
  updated(){
    this.$emit('updateCrawler', this.crawler);
  },
  watch: {
    type(type){
      this.changeCrawlerType(type);
    },
    postsCount(newValue, oldValue){
      if(newValue < oldValue){
        if(newValue < this.crawler.postsLimit.value) this.crawler.postsLimit.value = newValue;
      }
    },
  }
});

</script>
@endpush
