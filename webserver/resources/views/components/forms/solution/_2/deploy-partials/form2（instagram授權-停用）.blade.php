@push('js')
<script type="text/x-template" id="--nav-form-2">
  <div class="tab-pane fade" id="nav-form-2" role="tabpanel" aria-labelledby="nav-form-2-tab">
    <div class="deployment-info mb-4">
      <div class="info-item">請根據<a href="#"><strong>Instagram授權說明文件</strong></a>，前往<a href="https://www.instagram.com/developer/" target="_blank"><strong>Instagram Developer</strong></a>註冊您的應用程式，請留意<strong>Valid redirect URIs</strong>輸入是否符合說明文件的要求。成功註冊後，複製貼上<strong>CLIENT ID</strong>以及<strong>CLIENT Secret</strong>到下方欄位，並點擊<strong>Instagram 授權</strong>。</div>
    </div>

    {{-- Client ID --}}
    <div class="form-group">
      <label for="client-id">Client ID<span class="form-required">*</span></label>
      <div class="d-sm-flex i-form-control" :class="{'is-invalid' : form.clientId.invalid}">
        <input id="client-id" :class="[{'is-invalid' : form.clientId.invalid}, 'form-control']" v-model="form.clientId.clientId" placeholder="Instagram client id" type="text" :disabled="form.is_success">
      </div>
      <span class="invalid-feedback" role="alert"><strong>@{{ form.clientId.info }}</strong></span>
    </div>

    {{-- Client Secret --}}
    <div class="form-group">
      <label for="client-secret">Client Secret<span class="form-required">*</span></label>
      <div class="d-sm-flex i-form-control" :class="{'is-invalid' : form.clientSecret.invalid}">
        <input id="client-secret" :class="[{'is-invalid' : form.clientSecret.invalid}, 'form-control']" v-model="form.clientSecret.clientSecret" placeholder="Instagram client secret" type="text" :disabled="form.is_success">
      </div>
      <span class="invalid-feedback" role="alert"><strong>@{{ form.clientSecret.info }}</strong></span>
    </div>

    {{-- Divider --}}
    <hr class="mt-5 mb-5">

    {{-- Buttons --}}
    {!! Form::button('@{{ form.is_success ? "授權成功" : "Instagram 授權" }}', [
    'id'=>'authorize',
    '@click' => 'authorize', ':class'=>'[{"is-invalid" : form.accessToken.invalid}, {"btn-danger" : form.accessToken.invalid}, form.is_success ? "btn-success" : "btn-warning", {"progress-bar-striped progress-bar-animated" : form.authorizing}, "btn btn-block i-form-control"]',
    ':disabled' => 'form.is_success',
    'v-cloak' => 'true'
    ]) !!}
    <span class="invalid-feedback" role="alert"><strong>@{{ form.accessToken.info }}</strong></span>

  </div>
</script>


<script>
Vue.component('nav-form-2', {
  template: '#--nav-form-2',
  data(){
    return {
      form: {
        is_success: false,
        authorizing: false,
        clientId: {
          clientId: '',
          info: '',
          invalid: false,
        },
        clientSecret: {
          clientSecret: '',
          info: '',
          invalid: false,
        },
        accessToken: {
          accessToken: '',
          info: '',
          invalid: false,
        },
      },
      timer: {
        authorize: {
          timeOut: true,
          timer: null,
        },
      },
    };
  },
  methods: {
    authorize(){
      var invalid = false;
      if(this.form.clientId.clientId == ''){
        this.form.clientId.info = 'Client id is required';
        this.form.clientId.invalid = true;
        invalid = true;
      }

      if(this.form.clientSecret.clientSecret == ''){
        this.form.clientSecret.info = 'Client secret is required';
        this.form.clientSecret.invalid = true;
        invalid = true;
      }

      if(!invalid){
        this.form.authorizing = true;
        this.form.clientId.info = '';
        this.form.clientId.invalid = false;
        this.form.clientSecret.info = '';
        this.form.clientSecret.invalid = false;
        this.setAuthorizeTimer();
        window.open(route('solution.2.authorize', {
          clientId: this.form.clientId.clientId,
          clientSecret: this.form.clientSecret.clientSecret,
        }), '_blank');
      }
    },
    setAccessToken(accessToken){
      if(!this.timer.authorize.timeOut){
        this.form.accessToken.accessToken = accessToken;
        this.stopAuthorizeTimer();
        if(accessToken != ''){
          this.form.is_success = true;
          this.form.authorizing = false;
          this.form.clientId.info = '';
          this.form.clientId.invalid = false;
          this.form.clientSecret.info = '';
          this.form.clientSecret.invalid = false;
          this.form.accessToken.info = '';
          this.form.accessToken.invalid = false;
        }else{
          this.form.is_success = false;
          this.form.authorizing = false;
          this.form.clientId.info = '';
          this.form.clientId.invalid = false;
          this.form.clientSecret.info = 'Client secret is wrong';
          this.form.clientSecret.invalid = true;
          this.form.accessToken.info = '';
        }
      }
    },
    authorizeTimer(){
      let vm = this;
      this.timer.authorize.timer = setTimeout(function(){
        vm.timer.authorize.timeOut = true;

        vm.form.is_success = false;
        vm.form.authorizing = false;
        vm.form.clientId.info = 'Authorizing is time out, please check your client id and secret are correct';
        vm.form.clientId.invalid = true;
        vm.form.clientSecret.info = 'Authorizing is time out, please check your client id and secret are correct';
        vm.form.clientSecret.invalid = true;

      }, 5000);
    },
    setAuthorizeTimer(){
      this.stopAuthorizeTimer();
      this.timer.authorize.timeOut = false;
      this.authorizeTimer();
    },
    stopAuthorizeTimer(){
      this.timer.authorize.timeOut = true;
      if(this.timer.authorize.timer) clearTimeout(this.timer.authorize.timer);
    }
  },
  created(){
    this.$emit('update-form-data', 2, this.form);
  },
  updated(){
    this.$emit('update-form-data', 2, this.form);
  },
});
</script>
@endpush
