@push('js')
<script type="text/x-template" id="--nav-form-4">
  <div class="tab-pane fade" id="nav-form-4" role="tabpanel" aria-labelledby="nav-form-4-tab">
    <div class="deployment-info mb-4">
      <div class="info-item">僅在<strong>刊登時間</strong>內依循<strong>刊登頻率</strong>在您的 Wordpress 上刊登文章。<br/>提交部署後，將立即轉跳Paypal付款畫面，授權付款後即啟用部署，任何設定在成功部署後都能更改。</div>
    </div>

    <div class="form-group">
      <label for="duration">文章刊登時間<span class="form-required">*</span></label>
      <input id="duration" type="text" :class="[{'is-invalid' : form.duration.invalid}, 'form-control']" data-mask="AB : CB - AB : CB" placeholder="00 : 00 - 23 : 59" ref="duration">
      <span class="invalid-feedback" role="alert"><strong>@{{ form.duration.info }}</strong></span>
    </div>

    <div class="form-group">
      <label for="postFrequency">文章刊登頻率<span class="form-required">*</span><span>：每 @{{ form.postFrequency.value }} 分鐘</span></label>
      <input type="range" :class="[{'is-invalid' : form.postFrequency.invalid}, 'custom-range']" min="1" max="360" step="1" id="postFrequency" v-model="form.postFrequency.value">
      <span class="invalid-feedback" role="alert"><strong>@{{ form.postFrequency.info }}</strong></span>
    </div>


    <div class="form-group">
      <label for="postFrequency">文章刊登順序<span class="form-required">*</span></label>
      <draggable v-model="form.order.order" :class="[{'is-invalid' : form.order.invalid}, 'd-flex form-control']">
        <div v-for="crawler in form.order.order" class="draggable-element" :key="crawler.id">@{{ crawler.name }}</div>
      </draggable>
      <span class="invalid-feedback" role="alert"><strong>@{{ form.order.info }}</strong></span>
    </div>





    <hr class="mt-5 mb-5">

    {{ Form::button('提交部署', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, "btn btn-block btn-primary"]', '@click'=>'submit']) }}
  </div>
</script>

<script>
Vue.component('nav-form-4', {
  template: '#--nav-form-4',
  props: ['_form'],
  data(){
    return {
      submitting: false,
      form: {
        duration: {
          duration: '',
          info: '',
          invalid: false,
        },
        postFrequency: {
          value: 5,
          info: '',
          invalid: false,
        },
        order: {
          order: [
            {
              name: '爬蟲 1',
              id: 1,
            },
          ],
          info: '',
          invalid: false,
        }
      }
    }
  },
  methods: {
    setCrawlerOrder(crawlers){
      var vm = this,
      newOrder = [];

      for(j = 0; j < vm.form.order.order.length; j ++){
        Object.keys(crawlers).forEach(function(index){
          if(crawlers[index].id == vm.form.order.order[j].id){
            newOrder.push({
              id: crawlers[index].id,
              name: '爬蟲 '+index,
            });
            delete crawlers[index];
            return false;
          };
        });
      };

      Object.keys(crawlers).forEach(function(index){
        newOrder.push({
          id: crawlers[index].id,
          name: '爬蟲 '+index,
        });
      });

      this.$set(this.form.order, 'order', newOrder);
    },
    AllInvalidToFalse(obj){
      for(key in obj){
        if(key == 'invalid'){
          obj[key] = false;
          obj['info'] = '';
        }else if(typeof(obj[key]) === 'object'){
          this.AllInvalidToFalse(obj[key]);
        }
      }
    },
    submit(){
      if(!this.submitting){
        this.submitting = true;
        //this.form[4] = this.form_;
        this.$set(this._form, 4, this.form);
        this.AllInvalidToFalse(this._form);
        axios
        .post(route('solution.2.store'), {
          _token: args._token,
          form: this._form,
        })
        .then(response => {
          console.log(response);
          if(response.data.status == 'success'){
            if(response.data.approvalUrl){
              redirectUrl = response.data.approvalUrl;
            }else{
              redirectUrl = route('solution.2.deployment.show', {
                deployment: response.data.deployment,
              });
            }
            window.open(redirectUrl, '_self');
          };

          this.submitting = false;
        }).catch(error => {
          var errors = error.response.data.errors;
          this.submitting = false;
          var invalid_form = null;
          for(error in errors){
            console.log(error);
            var invalid_form_index = error.match(/^form\.([0-9]+)\./)[1];
            if(!invalid_form || invalid_form_index < invalid_form) invalid_form = invalid_form_index;

            // 找出最後一個「.」後的字串
            var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
            // 如果是value，表示是select，再往前找一個value
            if(last == 'value'){
              select_last = error.match(/\.(value\.value)$/);
              // 如果符合「.value.value」，表示是多層value
              if(select_last) last = select_last.slice(-1)[0];
            }

            // 替換form成_form
            error_ref = error.replace(new RegExp('form', 'g'), '_form');

            // 替換數字成以[]包覆表示
            error_ref = error_ref.replace(new RegExp('.([0-9]+).', 'g'), '[$1].');

            info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
            invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
            try{
              this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
              this.$set(eval('this.'+invalid_ref), 'invalid', true);
            }catch(e){

            }
          }

          if(invalid_form) $(deploy_form.$refs['nav-link-'+invalid_form]).tab('show');
        });

        this.$emit('update-form-data', null, this._form);
      }
    }
  },
  mounted(){
    var vm = this;
    $(this.$refs.duration).mask(this.$refs.duration.getAttribute('data-mask'), {
      translation: {
        'A': {
          pattern: /[0-2]/
        },
        'B': {
          pattern: /[0-9]/
        },
        'C': {
          pattern: /[0-6]/
        }
      },
      onChange(cep){
        vm.form.duration.duration = cep;
      },
    });
  }
});
</script>
@endpush
