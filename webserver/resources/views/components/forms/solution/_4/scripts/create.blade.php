<div id="script-form" class="mb-6" v-cloak>

    <div class="row">
        {{-- 腳本名稱 --}}
        <div class="form-group col-4">
            <label for="name">腳本名稱<span class="form-required">*</span></label>
            <input id="name" type="text" :class="[{'is-invalid' : form.name.invalid}, 'form-control']" v-model="form.name.name" {{ $action == 'edit' ? 'disabled' : '' }}>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.name.info }}</strong></span>
        </div>

        {{-- 腳本描述 --}}
        <div class="form-group col-8">
            <label for="description">腳本描述<span class="form-required">*</span></label>
            <input id="description" type="text" :class="[{'is-invalid' : form.description.invalid}, 'form-control']" v-model="form.description.description" {{ $action == 'edit' ? 'disabled' : '' }}>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.description.info }}</strong></span>
        </div>
    </div>


    {{-- 啟用狀態 --}}
    <div class="row">
        <div class="form-group col-12">
            <label for="active">啟用狀態<span class="form-required">*</span></label>

            <div :class="[{'is-invalid' : form.active.invalid}, 'i-form-control d-flex btn-group-toggle']">
                <label :class="[{active : form.active.active == '1'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="active" value="1" v-model="form.active.active" :checked="form.active.active == '1'" {{ $action == 'edit' ? 'disabled' : '' }}>
                    <span>Active</span>
                </label>

                <label :class="[{active : form.active.active == '0'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="active" value="0" v-model="form.active.active" :checked="form.active.active == '0'" {{ $action == 'edit' ? 'disabled' : '' }}>
                    <span>Inactive</span>
                </label>

            </div>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.active.info }}</strong></span>
        </div>
    </div>

    {{-- 建立訊息 --}}
    @include('components.forms.solution._4.scripts.basic.new-script')
    @include('components.forms.solution._4.scripts.basic.branch')
    @include('components.forms.solution._4.scripts.basic.node')
    @include('components.forms.solution._4.scripts.basic.message-types')
    @include('components.forms.solution._4.scripts.basic.messages')

    <label for="script">腳本流程<span class="form-required">*</span></label>

    <new-script :script="form.script" :newmessagetype="newMessageType"></new-script>
    <message-types></message-types>

    <hr class="mt-5 mb-5">

    {{ Form::button('新增訊息腳本', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, "btn btn-block btn-primary"]', '@click'=>'submit']) }}

</div>
