{{-- Image Carousel message --}}

@push('js')

<script type="text/x-template" id="--imagecarousel-message">
    <div>
        <div class="d-flex message imagecarousel-message">
            <div class="buttons mr-2">
                <div v-if="messagesCount > 1" class="text-muted text-danger-hover cursor-pointer" @click="remove">
                    <v-svg src="{{ asset('assets/image/svg/light/trash-alt.svg') }}" width="12" height="12"></v-svg>
                </div>
            </div>
            <div class="w-100">
                <div class="type text-muted pl-2 mb-2">Image Carousel message</div>

                <div class="content d-flex flex-nowrap row ml-0 pt-2 mt-n2 overflow-auto w-100">
                    <div v-for="(column, columnIndex) in form.columns.columns" :key="column.id" class="d-flex flex-nowrap row col-auto">
                        <div class="position-relative pr-4">

                            {{-- 新增/移除 column --}}
                            <div class="position-absolute right mr-1">
                                <div :class="[{'d-none' : form.columns.columns.length == 1}, 'text-muted mr-1 mb-2 cursor-pointer text-danger-hover']" @click.stop="removeColumn(columnIndex)">
                                    <v-svg src="{{ asset('assets/image/svg/light/minus-circle.svg') }}" width="16" height="16"></v-svg>
                                </div>
                                <div :class="[{'d-none' : form.columns.columns.length >= 10}, 'text-muted mr-1 cursor-pointer text-primary-hover']" @click.stop="newColumn(columnIndex)">
                                    <v-svg src="{{ asset('assets/image/svg/light/plus-circle.svg') }}" width="16" height="16"></v-svg>
                                </div>
                            </div>

                            {{-- 訊息內容 --}}
                            <div :class="[{'is-invalid' : column.image.invalid || column.actions[0].action.label.invalid}, 'i-form-control card mb-0']">

                                {{-- 圖片上傳 --}}
                                <div class="image-layout position-relative">

                                    <div :class="[{'i-dz-max-files-reached' : column.image.src != ''}, 'position-absolute top w-100 h-100 overflow-hidden border-radius-0375 idropzone dropzone-single mb-3']" :ref="'image-'+column.id">
                                        <div :id="'dz-preview-'+index+'-'+column.id" class="dz-preview dz-preview-single">
                                            <div class="dz-preview-cover">
                                                <img :class="[{ 'd-none' : column.image.src == '' }, 'dz-preview-img rounded-0']" :src="column.image.src" :alt="column.image.alt"/>
                                            </div>
                                        </div>
                                        <div class="dz-default dz-message">
                                            <span class="mb-3">
                                                <v-svg src="{{ asset('assets/image/svg/light/image.svg') }}" width="20" height="20"></v-svg>
                                            </span>
                                            <span>Drop image here to upload</span>
                                        </div>
                                    </div>
                                </div>

                                {{-- 按鈕 --}}
                                <div class="button position-absolute" @click="selectButton(columnIndex, column.actions[0].id)">
                                    <input :class="[{'bg-danger' : column.actions[0].invalid}, {valued : column.actions[0].action.label.label != ''}, 'not-input text-center text-white cursor-pointer p-1']" placeholder="輸入名稱及行為" v-model="column.actions[0].action.label.label">
                                </div>

                            </div>

                            <span v-if="column.image.invalid" class="invalid-feedback" role="alert"><strong>@{{ column.image.info }}</strong></span>
                            <span v-if="column.actions[0].action.label.invalid" class="invalid-feedback" role="alert"><strong>@{{ column.actions[0].action.label.info }}</strong></span>

                        </div>
                        <div v-if="actionType.currentColumnId && actionType.currentColumnId == column.id && actionType.currentButtonId" class="col-300 min-height-200 m-0 pl-0 pr-4">
                            {{-- 按鈕行為 --}}
                            <div class="position-relative nopadding">
                                <button type="button" class="close-edit-block position-absolute top right d-flex align-items-center btn btn-outline-danger btn-sm" @click="closeEditBlock(columnIndex)">
                                    <span class="d-flex align-items-center">
                                        <v-svg src="{{ asset('assets/image/svg/light/times.svg') }}" width="14" height="14"></v-svg>
                                    </span>
                                    <span>收合視窗</span>
                                </button>
                                <div class="form-group mb-3">
                                    <label for="actionType">按鈕行為<span class="form-required">*</span><span>@{{ '（正在編輯按鈕 '+actionType.currentButtonId+'）' }}</span></label>
                                    <vue-multiselect :class="[{'is-invalid' : actionType.invalid}, 'hidden-arrow multiselect-sm']" v-model="actionType.value" track-by="value" label="name" placeholder="請選擇行為" openDirection="bottom" :options="actionTypes" :searchable="false" :show-labels="false" :allow-empty="false" :disabled="!actionType.currentButtonId" @input="changeActionType(columnIndex)"></vue-multiselect>
                                    <span class="invalid-feedback" role="alert"><strong>@{{ actionType.info }}</strong></span>
                                </div>

                                <component v-if="actionType.currentButtonId && actionType.value && actionType.value.show" :is="actionType.value.value+'-action'" :id="actionType.currentButtonId" :action="actionData" :columnIndex="columnIndex" @updateActionData="updateActionData"></component>
                            </div>
                        </div>
                    </div>



                    <div class="col-300 m-0 pb-3 pl-0 pr-0">
                        {{-- 替代文字 --}}
                        <div class="form-group">
                            <label for="altText">替代文字<span class="form-required">*</span><span>（訊息通知中顯示）</span></label>
                            <input id="altText" type="text" :class="[{'is-invalid' : form.altText.invalid}, 'form-control form-control-sm']" v-model="form.altText.altText">
                            <span class="invalid-feedback" role="alert"><strong>@{{ form.altText.info }}</strong></span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="droppable" @dragover.prevent @dragenter.stop.prevent="isDropzoneActive() ? dragenter($event) : null" @dragleave.stop.prevent="isDropzoneActive() ? dragleave($event) : null" @drop.stop.prevent="isDropzoneActive() ? drop(index + 1) : null">
            <div></div>
        </div>
    </div>
</script>

<script>
Vue.component('imagecarousel-message', {
    template: '#--imagecarousel-message',
    props: ['index', 'currentNodeId', 'node', 'messagesCount', 'message', 'actions', 'defaultAction', 'actionTypes', 'newMessageType', 'hasQuickReply'],
    data(){
        return {
            counter: 0,
            currentColumnId: this.getCurrentColumnId(),
            currentButtonId: this.getCurrentButtonId(),

            gottenColumnIndex: 0,
            gottenButtonIndex: 0,
            actionData: null,

            form: this.message,

            default: {
                action: this.defaultAction,
                // actionType: this.actionType,
            },
            actionType: {
                currentColumnId: null,
                currentButtonId: null,
                value: null,
                info: '',
                invalid: false,
            }
        }
    },
    methods: {
        remove(){
            this.$emit('removeMessage', this.index);
        },
        dragenter(event){
            this.counter ++;
            $(event.target).addClass('dragover');
        },
        dragleave(event){
            this.counter --;
            if(this.counter === 0) $(event.target).removeClass('dragover');
        },
        drop(index){
            this.counter = 0;
            this.$emit('newMessage', index);
        },
        isDropzoneActive(){
            if(this.newMessageType == 'quickreply-message' && (this.index == this.messagesCount - 1 && !this.hasQuickReply)){
                return true;
            }else if(this.newMessageType != 'quickreply-message' && this.messagesCount < 5){
                return true;
            }
            return false;
        },
        getCurrentColumnId(){
            return this.message.columns.columns.reduce((max, column) => Math.max(max, column.id), 1);
        },
        getCurrentButtonId(){
            return this.message.columns.columns.reduce(function(max, column){
                return Math.max(max, column.actions.reduce((max, action) => Math.max(max, action.id), 1));
            }, 1);
        },
        getColumn(columnId){
            var vm = this;
            this.form.columns.columns.forEach(function(column, columnIndex){
                if(column.id && column.id == columnId){
                    vm.gottenColumnIndex = columnIndex;
                    return false;
                }
            });
        },
        getButton(buttonId, columnIndex){
            var vm = this;
            if(columnIndex){
                this.form.columns.columns[columnIndex].actions.forEach(function(action, actionIndex){
                    if(action.id && action.id == buttonId){
                        vm.gottenColumnIndex = columnIndex;
                        vm.gottenButtonIndex = actionIndex;
                        return false;
                    }
                });
            }else{
                this.form.columns.columns.forEach(function(column, columnIndex){
                    column.actions.forEach(function(action, actionIndex){
                        if(action.id && action.id == buttonId){
                            vm.gottenColumnIndex = columnIndex;
                            vm.gottenButtonIndex = actionIndex;
                            return false;
                        }
                    });
                });
            }
        },
        removeColumn(columnIndex){
            var vm = this;
            this.form.columns.columns[columnIndex].actions.forEach(function(action){
                if(['next', 'next-with-message'].includes(action.type.value)){
                    vm.$emit('removeNode', action.action.nextId.nextId);
                }
            });

            this.form.columns.columns.splice(columnIndex, 1);
        },
        newColumn(columnIndex){
            if(this.form.columns.columns.length < 10){
                this.currentColumnId ++;
                this.currentButtonId ++;

                this.form.columns.columns.splice(columnIndex + 1, 0, {
                    id: this.currentColumnId,
                    image: {
                        src: '',
                        alt: '',
                        file: null,
                        info: '',
                        invalid: false
                    },
                    actions: [
                        $.extend(true, {id: this.currentButtonId}, this.default.action),
                    ],
                    // actionType: $.extend(true, {currentButtonId: null}, this.default.actionType),
                });
                var vm = this;
                this.$nextTick(function(){
                    vm.initiateImageDropzone(vm.currentColumnId);
                });
            }
        },
        selectButton(columnIndex, id){
            if(this.actionType.currentButtonId != id){
                this.saveAndClearAction(id, false);
                this.refreshActionData(id);
            }
        },
        saveAndClearAction(newId, removed){
            if(removed){
                // 原本的區域已刪除
                this.actionType.value = null;
            }else{
                if(this.actionType.currentButtonId){
                    // 原本的區域沒刪除，儲存類型
                    this.getButton(this.actionType.currentButtonId);
                    this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].type = this.actionType.value;
                    this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].selected = false;
                }
                this.actionType.currentColumnId = null;
                this.actionType.currentButtonId = null;
            }

            if(newId){
                this.getButton(newId);

                // 新button的資料
                newAreaData = this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex];

                this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].invalid = false;
                this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].selected = true;
                this.actionType.value = newAreaData.type;

                this.actionType.currentColumnId = this.form.columns.columns[this.gottenColumnIndex].id;
                this.actionType.currentButtonId = newId;
            }else{
                this.actionType.currentColumnId = null;
                this.actionType.currentButtonId = null;
            }
        },
        updateActionData(data, columnIndex){
            this.getButton(this.actionType.currentButtonId, columnIndex);
            this.$set(this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex], 'action', data);
        },
        refreshActionData(id){
            this.getButton(id);
            this.actionData = this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].action;

            if(['next', 'next-with-message'].includes(this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].type.value)){
                this.$emit('setHoverNode', this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].action.nextId.nextId);
            }else{
                this.$emit('setHoverNode', null);
            }
        },
        closeEditBlock(){
            this.saveAndClearAction(null, false);
            this.$emit('setHoverNode', null);
        },
        initiateImageDropzone(columnId){
            var vm = this;
            this['dropzone-' + columnId] = new Dropzone(vm.$refs['image-' + columnId][0], {
                url: "#",
                autoProcessQueue: false,
                maxFiles: 1,
                acceptedFiles: ".jpeg,.jpg,.png,.JPEG,.JPG,.PNG",
                thumbnailWidth: null,
                thumbnailHeight: null,
                addedfile: function(file){
                    vm.getColumn(columnId);
                    vm.form.columns.columns[vm.gottenColumnIndex].image.info = '';
                    vm.form.columns.columns[vm.gottenColumnIndex].image.invalid = false;

                    var img = new Image();

                    img.src = window.URL.createObjectURL(file);
                    img.onload = function(){
                        var width = img.naturalWidth,
                        height = img.naturalHeight;

                        if(width > 1024 || height > 1024){
                            vm.$set(vm.form.columns.columns[vm.gottenColumnIndex].image, 'info', '圖片最大尺寸：1024 x 1024');
                            vm.$set(vm.form.columns.columns[vm.gottenColumnIndex].image, 'invalid', true);
                            return false;
                        }

                        if(width != height){
                            vm.$set(vm.form.columns.columns[vm.gottenColumnIndex].image, 'info', '請上傳正方形圖片');
                            vm.$set(vm.form.columns.columns[vm.gottenColumnIndex].image, 'invalid', true);
                            return false;
                        }

                        vm.$set(vm.form.columns.columns[vm.gottenColumnIndex], 'image', {
                            src: window.URL.createObjectURL(file),
                            alt: file.name,
                            file: file,
                            info: '',
                            invalid: false,
                        });
                    };
                    return false;
                },
            });
        },
        changeActionType(columnIndex){
            this.gottenColumnIndex = columnIndex;
            this.getButton(this.actionType.currentButtonId, columnIndex);
            newValue = this.actionType.value;

            if(!['next', 'next-with-message'].includes(this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].type.value) && ['next', 'next-with-message'].includes(newValue.value)){
                // 從不是next換到next
                this.$emit('insertNewNode', this.node.end);
            }else if(['next', 'next-with-message'].includes(this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].type.value) && !['next', 'next-with-message'].includes(newValue.value)){
                // 從next換到不是next
                this.$emit('removeNode', this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].action.nextId.nextId);
            }

            this.$set(this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex], 'type', newValue);

            if(newValue.value == 'next'){
                this.$set(this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex], 'action', {
                    label: this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].action.label,
                    nextId: {
                        nextId: this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].action.nextId ? this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].action.nextId.nextId : this.currentNodeId + 1,
                        info: '',
                        invalid: false,
                    }
                });
            }else if(newValue.value == 'next-with-message'){
                this.$set(this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex], 'action', $.extend(true, {
                    label: this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].action.label,
                    nextId: {
                        nextId: this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].action.nextId ? this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].action.nextId.nextId : this.currentNodeId + 1,
                        info: '',
                        invalid: false,
                    }
                }, this.actions[newValue.value]));
            }else{
                this.$set(this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex], 'action', $.extend(true, {
                    label: this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].action.label
                }, this.actions[newValue.value]));
            }

            this.refreshActionData(this.actionType.currentButtonId);
        }
    },
    mounted(){
        var vm = this;
        this.form.columns.columns.forEach(function(column, columnIndex){
            vm.initiateImageDropzone(column.id);
        });
    },
    // updated(){
    //     this.$emit('updateMessageData', this.index, this.form);
    // },
});
</script>

@endpush
