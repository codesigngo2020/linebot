{{-- Buttons message --}}

@push('js')

<script type="text/x-template" id="--buttons-message">
    <div>
        <div class="d-flex message buttons-message">
            <div class="buttons mr-1">
                <div v-if="messagesCount > 1" class="text-muted text-danger-hover cursor-pointer" @click="remove">
                    <v-svg src="{{ asset('assets/image/svg/light/trash-alt.svg') }}" width="12" height="12"></v-svg>
                </div>
            </div>
            <div class="w-100">
                <div class="type text-muted pl-1 mb-1">Buttons message</div>

                <div class="alert alert-dismissible fade show alert-light text-justify mt-2 mb-2" role="alert">
                    「文字說明內容」基本上限為60字元；若選擇去除「圖片」，可增加至160字元
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="content d-flex flex-nowrap row ml-0 pt-2 mt-n2 overflow-auto w-100">
                    <div class="d-flex flex-nowrap row col-auto">
                        <div class="pr-4">

                            {{-- 按鈕訊息 --}}
                            <div :class="[{'is-invalid' : form.image.invalid || form.title.invalid || form.text.invalid}, 'i-form-control card mb-0']">

                                {{-- 圖片上傳 --}}
                                <div :class="[{'d-none' : !form.image.active.active}, form.image.ratio.ratio, 'image-layout position-relative']">

                                    <div :class="[{'i-dz-max-files-reached' : form.image.src != ''}, 'position-absolute top w-100 h-100 overflow-hidden border-radius-0375 idropzone dropzone-single mb-3']" ref="image">
                                        <div :id="'dz-preview-'+index" class="d-flex align-items-center overflow-hidden dz-preview dz-preview-single">
                                            <div class="dz-preview-cover">
                                                <img :class="[{ 'd-none' : form.image.src == '' }, 'dz-preview-img rounded-0']" :src="form.image.src" :alt="form.image.alt"/>
                                            </div>
                                        </div>
                                        <div class="dz-default dz-message">
                                            <span class="mb-3">
                                                <v-svg src="{{ asset('assets/image/svg/light/image.svg') }}" width="20" height="20"></v-svg>
                                            </span>
                                            <span>Drop image here to upload</span>
                                        </div>
                                    </div>

                                    {{-- 圖片尺寸 --}}
                                    <div v-if="form.image.active.active" class="position-absolute top right z-index-999 d-flex align-items-center mt-2 mr-2">
                                        <div :class="[form.image.ratio.ratio == 'rectangle' ? 'text-primary' : 'text-muted','d-flex cursor-pointer']" @click="setRatio('rectangle')">
                                            <v-svg src="{{ asset('assets/image/svg/light/rectangle-landscape.svg') }}" width="18" height="18"></v-svg>
                                        </div>

                                        <div class="text-muted ml-3 mr-3">/</div>

                                        <div :class="[form.image.ratio.ratio == 'square' ? 'text-primary' : 'text-muted', 'd-flex cursor-pointer']" @click="setRatio('square')">
                                            <v-svg src="{{ asset('assets/image/svg/light/square.svg') }}" width="18" height="18"></v-svg>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body text-justify pt-2 pb-0 pl-3 pr-3">
                                    {{-- title --}}
                                    <input class="title not-input mb-2" placeholder="請輸入訊息標題" v-model="form.title.title">

                                    {{-- text --}}
                                    <div class="texarea-not-input text position-relative">
                                        <pre ref="inputPre"></pre>
                                        <textarea class="not-input" placeholder="請輸入文字說明內容" v-model="form.text.text"></textarea>
                                    </div>
                                </div>

                                <hr class="m-0">

                                <div class="actions mt-1 mb-1">

                                    <div v-for="(action, index) in form.actions" :key="action.id" class="position-relative d-flex align-items-center cursor-pointer" @click="selectButton(action.id)">
                                        <input class="not-input text-center pt-2 pb-2 pl-3 pr-3" placeholder="編輯名稱及按鈕行為" v-model="action.action.label.label">
                                        <div class="position-absolute left d-flex mr-1 bg-white">
                                            <div :class="[action.invalid ? 'text-danger' : 'text-muted', ' ml-2']">@{{ action.id }}</div>
                                        </div>
                                        <div class="position-absolute right d-flex mr-2 pl-1 bg-white">
                                            <div :class="[{'d-none' : form.actions.length == 1}, 'text-muted mr-1 cursor-pointer text-danger-hover']" @click.stop="removeButton(index)">
                                                <v-svg src="{{ asset('assets/image/svg/light/minus-circle.svg') }}" width="16" height="16"></v-svg>
                                            </div>
                                            <div :class="[{'d-none' : form.actions.length >= 4}, 'text-muted cursor-pointer text-primary-hover']" @click.stop="newButton(index)">
                                                <v-svg src="{{ asset('assets/image/svg/light/plus-circle.svg') }}" width="16" height="16"></v-svg>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <span v-for="input in ['image', 'title', 'text']" v-if="form[input].invalid" class="invalid-feedback" role="alert"><strong>@{{ form[input].info }}</strong></span>
                            <span v-for="action in form.actions" v-if="action.action.label.invalid" class="invalid-feedback" role="alert"><strong>@{{ action.action.label.info }}</strong></span>

                        </div>

                        <div v-if="actionType.currentButtonId" class="col-300 min-height-200 m-0 pl-0 pb-5 pr-4">
                            {{-- 按鈕行為 --}}
                            <div class="position-relative nopadding">
                                <button type="button" class="close-edit-block position-absolute top right d-flex align-items-center btn btn-outline-danger btn-sm" @click="closeEditBlock">
                                    <span class="d-flex align-items-center">
                                        <v-svg src="{{ asset('assets/image/svg/light/times.svg') }}" width="14" height="14"></v-svg>
                                    </span>
                                    <span>收合視窗</span>
                                </button>
                                <div class="form-group">
                                    <label for="actionType">按鈕行為<span class="form-required">*</span><span>@{{ '（正在編輯按鈕 '+actionType.currentButtonId+'）' }}</span></label>
                                    <vue-multiselect :class="[{'is-invalid' : actionType.invalid}, 'hidden-arrow multiselect-sm']" v-model="actionType.value" track-by="value" label="name" placeholder="請選擇行為" openDirection="bottom" :options="actionTypes" :searchable="false" :show-labels="false" :allow-empty="false" :disabled="!actionType.currentButtonId" @input="changeActionType"></vue-multiselect>
                                    <span class="invalid-feedback" role="alert"><strong>@{{ actionType.info }}</strong></span>
                                </div>

                                <component v-if="actionType.currentButtonId && actionType.value && actionType.value.show" :is="actionType.value.value+'-action'" :id="actionType.currentButtonId" :action="actionData" @updateActionData="updateActionData"></component>
                            </div>
                        </div>
                    </div>



                    <div class="col-300 m-0 pb-5 pl-0 pr-0">

                        {{-- 替代文字 --}}
                        <div class="form-group mb-3">
                            <label for="altText">替代文字<span class="form-required">*</span><span>（訊息通知中顯示）</span></label>
                            <input id="altText" type="text" :class="[{'is-invalid' : form.altText.invalid}, 'form-control form-control-sm']" v-model="form.altText.altText">
                            <span class="invalid-feedback" role="alert"><strong>@{{ form.altText.info }}</strong></span>
                        </div>

                        <hr class="mt-3 mb-3">

                        {{-- 圖片設定 --}}
                        <div class="form-group mb-3">
                            <div class="custom-control custom-switch custom-switch-sm pt-2">
                                <input type="checkbox" class="custom-control-input" :id="'image-'+node.id+'-'+index" v-model="form.image.active.active">
                                <label :class="[{ 'text-muted' : !form.image.active.active }, 'custom-control-label']" :for="'image-'+node.id+'-'+index">@{{ form.image.active.active ? '訊息上方包含一張圖片，「文字說明內容」上限減少至60字元' : '訊息上方不包含「圖片」，「文字說明內容」可輸入上限160字元' }}</label>
                            </div>

                            <span class="invalid-feedback" role="alert"><strong>@{{ form.image.active.info }}</strong></span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="droppable" @dragover.prevent @dragenter.stop.prevent="isDropzoneActive() ? dragenter($event) : null" @dragleave.stop.prevent="isDropzoneActive() ? dragleave($event) : null" @drop.stop.prevent="isDropzoneActive() ? drop(index + 1) : null">
            <div></div>
        </div>
    </div>
</script>

<script>
Vue.component('buttons-message', {
    template: '#--buttons-message',
    props: ['index', 'currentNodeId', 'node', 'messagesCount', 'message', 'actions', 'newMessageType', 'hasQuickReply', 'defaultAction', 'actionTypes'],
    data(){
        return {
            counter: 0,
            currentButtonId: this.getCurrentButtonId(),

            gottenButtonIndex: 0,
            actionData: null,

            form: this.message,
            default: {
                action: this.defaultAction,
            },
            actionType: {
                currentButtonId: null,
                value: null,
                info: '',
                invalid: false,
            }
        }
    },
    methods: {
        remove(){
            this.$emit('removeMessage', this.index);
        },
        dragenter(event){
            this.counter ++;
            $(event.target).addClass('dragover');
        },
        dragleave(event){
            this.counter --;
            if(this.counter === 0) $(event.target).removeClass('dragover');
        },
        drop(index){
            this.counter = 0;
            this.$emit('newMessage', index);
        },
        isDropzoneActive(){
            if(this.newMessageType == 'quickreply-message' && (this.index == this.messagesCount - 1 && !this.hasQuickReply)){
                return true;
            }else if(this.newMessageType != 'quickreply-message' && this.messagesCount < 5){
                return true;
            }
            return false;
        },
        getCurrentButtonId(){
            return this.message.actions.reduce((max, action) => Math.max(max, action.id), 1);
        },
        clearImageData(){
            this.form.image.src = '';
            this.form.image.alt = '';
            this.form.image.file = null;
            this.form.image.info = '';
            this.form.image.invalid = false;
        },
        getButton(buttonId){
            for(key in this.form.actions){
                if(this.form.actions[key].id && this.form.actions[key].id == buttonId){
                    this.gottenButtonIndex = key;
                }
            }
        },
        removeButton(index){
            buttonId = this.form.actions[index].id;
            this.gottenButtonIndex = index;

            if(['next', 'next-with-message'].includes(this.form.actions[index].type.value)){
                this.$emit('removeNode', this.form.actions[index].action.nextId.nextId);
            }

            this.form.actions.splice(index, 1);
            if(this.actionType.currentButtonId == buttonId) this.saveAndClearAction(null, true);
        },
        newButton(index){
            if(this.form.actions.length < 4){
                this.currentButtonId ++;
                this.form.actions.splice(index + 1, 0, $.extend(true, {
                    id: this.currentButtonId
                }, this.default.action));
            }
        },
        selectButton(id){
            // if(this.actionType.currentButtonId != id){
            this.saveAndClearAction(id, false);
            this.refreshActionData();
            // }
        },
        saveAndClearAction(newId, removed){
            if(removed){
                // 原本的區域已刪除
                this.actionType.value = null;
            }else if(this.actionType.currentButtonId){
                // 原本的區域沒刪除，儲存類型
                this.getButton(this.actionType.currentButtonId);
                this.form.actions[this.gottenButtonIndex].type = this.actionType.value;
                this.form.actions[this.gottenButtonIndex].selected = this.actionType.false;
            }

            if(newId){
                this.actionType.currentButtonId = newId;
                // 新button的資料
                this.getButton(newId);
                newAreaData = this.form.actions[this.gottenButtonIndex];
                this.form.actions[this.gottenButtonIndex].invalid = false;
                this.form.actions[this.gottenButtonIndex].selected = true;
                this.actionType.value = newAreaData.type;

            }else{
                this.actionType.currentButtonId = null;
            }
        },
        changeActionType(){
            this.getButton(this.actionType.currentButtonId);

            if(!['next', 'next-with-message'].includes(this.form.actions[this.gottenButtonIndex].type.value) && ['next', 'next-with-message'].includes(this.actionType.value.value)){
                // 從不是next換到next
                this.$emit('insertNewNode', this.node.end);
            }else if(['next', 'next-with-message'].includes(this.form.actions[this.gottenButtonIndex].type.value) && !['next', 'next-with-message'].includes(this.actionType.value.value)){
                // 從next換到不是next
                this.$emit('removeNode', this.form.actions[this.gottenButtonIndex].action.nextId.nextId);
            }

            this.$set(this.form.actions[this.gottenButtonIndex], 'type', this.actionType.value);

            if(this.actionType.value.value == 'next'){
                this.$set(this.form.actions[this.gottenButtonIndex], 'action', {
                    label: this.form.actions[this.gottenButtonIndex].action.label,
                    nextId: {
                        nextId: this.form.actions[this.gottenButtonIndex].action.nextId ? this.form.actions[this.gottenButtonIndex].action.nextId.nextId : this.currentNodeId + 1,
                        info: '',
                        invalid: false,
                    }
                });
            }else if(this.actionType.value.value == 'next-with-message'){
                this.$set(this.form.actions[this.gottenButtonIndex], 'action', $.extend(true, {
                    label: this.form.actions[this.gottenButtonIndex].action.label,
                    nextId: {
                        nextId: this.form.actions[this.gottenButtonIndex].action.nextId ? this.form.actions[this.gottenButtonIndex].action.nextId.nextId : this.currentNodeId + 1,
                        info: '',
                        invalid: false,
                    }
                }, this.actions[this.actionType.value.value]));
            }else{
                this.$set(this.form.actions[this.gottenButtonIndex], 'action', $.extend(true, {
                    label: this.form.actions[this.gottenButtonIndex].action.label
                }, this.actions[this.actionType.value.value]));
            }

            this.refreshActionData();
        },
        updateActionData(data){
            this.getButton(this.actionType.currentButtonId);
            this.$set(this.form.actions[this.gottenButtonIndex], 'action', data);
        },
        refreshActionData(){
            this.getButton(this.actionType.currentButtonId);
            this.actionData = this.form.actions[this.gottenButtonIndex].action;

            if(['next', 'next-with-message'].includes(this.form.actions[this.gottenButtonIndex].type.value)){
                this.$emit('setHoverNode', this.form.actions[this.gottenButtonIndex].action.nextId.nextId);
            }else{
                this.$emit('setHoverNode', null);
            }
        },
        closeEditBlock(){
            this.saveAndClearAction(null, false);
            this.$emit('setHoverNode', null);
        },
        setRatio(size){
            this.form.image.ratio.ratio = size;
        }
    },
    created(){
        this.closeEditBlock();
    },
    mounted(){
        var vm = this;
        this.dropzone = new Dropzone(this.$refs.image, {
            url: "#",
            autoProcessQueue: false,
            maxFiles: 1,
            acceptedFiles: ".jpeg,.jpg,.png,.JPEG,.JPG,.PNG",
            thumbnailWidth: null,
            thumbnailHeight: null,
            addedfile: function(file){
                vm.form.image.info = '';
                vm.form.image.invalid = false;

                var img = new Image();

                img.src = window.URL.createObjectURL(file);
                img.onload = function(){
                    var width = img.naturalWidth,
                    height = img.naturalHeight;

                    if(width > 1024 || height > 1024){
                        vm.$set(vm.form.image, 'info', '圖片最大尺寸：1024 x 1024');
                        vm.$set(vm.form.image, 'invalid', true);
                        return false;
                    }

                    if(vm.form.image.ratio.ratio == 'square'){
                        if(width != height){
                            vm.$set(vm.form.image, 'info', '請上傳正方形圖片');
                            vm.$set(vm.form.image, 'invalid', true);
                            return false;
                        }
                    }

                    vm.$set(vm.form.image, 'src', window.URL.createObjectURL(file));
                    vm.$set(vm.form.image, 'alt', file.name);
                    vm.$set(vm.form.image, 'file', file);
                    vm.$set(vm.form.image, 'info', '');
                    vm.$set(vm.form.image, 'invalid', false);
                };
                return false;
            },
        });
    },
    updated(){
        this.$emit('updateMessageData', this.index, this.form);
    },
    watch: {
        'form.image.active.active': function(newValue){
            if(!newValue){
                this.clearImageData();

                this.form.image.ratio.ratio = '';
                this.form.image.ratio.info = '';
                this.form.image.ratio.invalid = false;
            }else{
                this.form.image.ratio.ratio = 'rectangle';
                this.form.image.ratio.info = '';
                this.form.image.ratio.invalid = false;
            }
        },
        'form.image.ratio.ratio': function(){
            this.clearImageData();
        },
        'form.text.text': function(newValue){
            this.$refs.inputPre.innerHTML = newValue.replaceAll(/\r\n|\r|\n/g, '<br>')+'<br>';
        },
    }
});
</script>

@endpush
