{{-- Messages --}}

@foreach(['text', 'image', 'imagemap', 'buttons', 'confirm', 'carousel', 'imagecarousel', 'quickreply'] as $type)
@include('components.forms.solution._4.scripts.basic.messages.'.$type)
@endforeach



{{-- Actions --}}

@foreach(['next-with-message', 'message', 'keyword', 'uri', 'liff', 'richmenu'] as $type)
@include('components.forms.solution._4.scripts.basic.actions.'.$type)
@endforeach
