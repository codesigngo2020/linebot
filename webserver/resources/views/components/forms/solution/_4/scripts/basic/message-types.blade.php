@push('js')

{{-- 選擇訊息類型 --}}

<script type="text/x-template" id="--message-types">
    <div id="message-types" :class="[{active : active}, 'z-index-999']">
        <div id="message-types-switch" @click="toggleActive">
            <v-svg v-show="!active" src="{{ asset('assets/image/svg/regular/chevron-left.svg') }}" width="18" height="18"></v-svg>
            <v-svg v-show="active" src="{{ asset('assets/image/svg/regular/chevron-right.svg') }}" width="18" height="18"></v-svg>
        </div>


        <div class="position-relative w-100 h-100 p-4 z-index-1 overflow-auto">
            <div id="text-message" class="type mb-4" draggable="true" @dragstart.stop="dragstart($event)" @dragend.stop="dragend($event)">Text</div>
            <div id="image-message" class="type mb-4" draggable="true" @dragstart.stop="dragstart($event)" @dragend.stop="dragend($event)">Image</div>
            <div id="imagemap-message" class="type mb-4" draggable="true" @dragstart.stop="dragstart($event)" @dragend.stop="dragend($event)">Imagemap</div>
            <div id="buttons-message" class="type mb-4" draggable="true" @dragstart.stop="dragstart($event)" @dragend.stop="dragend($event)">Buttons</div>
            <div id="confirm-message" class="type mb-4" draggable="true" @dragstart.stop="dragstart($event)" @dragend.stop="dragend($event)">Confirm</div>
            <div id="carousel-message" class="type mb-4" draggable="true" @dragstart.stop="dragstart($event)" @dragend.stop="dragend($event)">Carousel</div>
            <div id="imagecarousel-message" class="type mb-4" draggable="true" @dragstart.stop="dragstart($event)" @dragend.stop="dragend($event)">Image Carousel</div>
            <div id="quickreply-message" class="type" draggable="true" @dragstart.stop="dragstart($event)" @dragend.stop="dragend($event)">Quick Reply</div>
        </div>



    </div>
</script>

<script>
Vue.component('message-types', {
    template: '#--message-types',
    data(){
        return {
            active: false
        }
    },
    methods: {
        toggleActive(){
            this.active = !this.active;
        },
        dragstart(event){
            scriptForm.newMessageType = event.currentTarget.id;
        },
        dragend(event){
            scriptForm.newMessageType = null;
            $(".droppable", $("#new-script-layout")).removeClass('dragover');
        }
    },
});
</script>

@endpush
