{{-- 切換選單 --}}

@push('js')

<script type="text/x-template" id="--richmenu-action">
    <div>
        <div class="form-group">
            <label for="richmenu">選擇主選單<span class="form-required">*</span><a v-if="form.richmenu.value" class="text-muted ml-2" :href="getRichmenuUrl(form.richmenu.value.id)" target="_blank">（預覽主選單）</a></label>
            <vue-multiselect :class="[{'is-invalid' : form.richmenu.invalid}, 'i-form-control hidden-arrow multiselect-sm']" v-model="form.richmenu.value" track-by="value" label="name" placeholder="搜尋主選單名稱" open-direction="bottom" :options="richmenu.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="false" :loading="richmenu.isLoading" :internal-search="false" :options-limit="10" :limit-text="richmenuslimitText" :max-height="600" :show-no-results="false" :hide-selected="true" @search-change="getRichmenus">
                <span slot="noResult">沒有相符的搜尋結果。</span>
            </vue-multiselect>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.richmenu.info }}</strong></span>
        </div>
    </div>
</script>

<script>
Vue.component('richmenu-action', {
    template: '#--richmenu-action',
    props: ['id', 'action', 'columnIndex'],
    data: function(){
        return {
            actionData: null,
            form: this.action,
            richmenu: {
                isLoading: false,
                options: [],
            }
        };
    },
    methods: {
        getRichmenuUrl(id){
            return route('solution.4.deployment.richmenus.show', {
                deployment: args.deployment,
                richmenuId: id,
            });
        },
        richmenuslimitText(richmenu){
            return `and ${richmenu} other richmenus`;
        },
        getRichmenus(query){
            this.richmenu.isLoading = true;
            axios
            .get(route('solution.4.deployment.richmenus.queryForSelect', {
                deployment: args.deployment,
                q: query,
            }))
            .then(response => {
                this.richmenu.options = response.data;
                this.richmenu.isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this.richmenu.options = [];
                this.richmenu.isLoading = false;
                console.log(error);
            });
        },
    },
    created(){
        this.getRichmenus('');
    },
    updated(){
        this.$emit('updateActionData', this.form, this.columnIndex);
    },
    watch: {
        action(newValue){
            this.$set(this, 'form', newValue);
        },
    }
});
</script>

@endpush
