@push('js')

{{-- 腳本節點 --}}

<script type="text/x-template" id="--node">
    <div :class="[{editting : editting}, {'w-600' : nodeWidth == 'wide'}, {hover : hoverNodeId == node.id}, 'node']" @click="editNode">
        <div v-if="!editting" class="message-label mb-0 d-flex flex-column">
            <p v-for="(message, index) in node.messages.messages" :key="message.id" :class="[message.invalid && !(hoverMessagesId.includes(message.id) || hoverMessagesId.includes(node.id)) ? 'badge-soft-danger' : 'badge-soft-secondary', {hover : hoverMessagesId.includes(message.id) || hoverMessagesId.includes(node.id)} ,'badge']" @mouseenter.stop="mouseenter(index, message.id)" @mouseleave.stop="mouseleave">@{{ message.type }}</p>
        </div>

        <div v-if="editting" class="position-relative w-100 overflow-hidden">

            <div>
                <div class="droppable" @dragover.prevent @dragenter.stop.prevent="isDropzoneActive() ? dragenter($event) : null" @dragleave.stop.prevent="isDropzoneActive() ? dragleave($event) : null" @drop.stop.prevent="isDropzoneActive() ? drop(0) : null">
                    <div></div>
                </div>
            </div>

            <button type="button" class="toggle-message-template position-absolute top right d-flex align-items-center z-index-1 btn btn-outline-info btn-sm" @click.stop="toggleMessageTemplate">
                @{{ form.node.type == 'message' ? '使用訊息模組' : '使用一般訊息' }}
            </button>

            <button type="button" class="close-edit-block position-absolute top right d-flex align-items-center z-index-1 btn btn-outline-danger btn-sm" @click.stop="closeEditNode">
                <span class="d-flex align-items-center">
                    <v-svg src="{{ asset('assets/image/svg/light/times.svg') }}" width="14" height="14"></v-svg>
                </span>
                <span>收合節點</span>
            </button>

            <div v-if="form.node.type == 'message'">
                <component v-for="(message, index) in form.node.messages.messages" :key="message.id" :is="message.type+'-message'" :index="index" :currentNodeId="currentNodeId" :node="node" :messagesCount="getMessagesCount()" :message="message.message" :actions="['buttons', 'imagemap', 'confirm', 'carousel', 'imagecarousel', 'quickreply'].includes(message.type) ? actions : null" :defaultAction="['buttons', 'carousel', 'imagecarousel', 'quickreply'].includes(message.type) ? defaultData.action : null" :actionTypes="['buttons', 'imagemap', 'confirm', 'carousel', 'imagecarousel'].includes(message.type) ? actionTypes : null" :quickReplyActionTypes="message.type == 'quickreply' ? quickReplyActionTypes : null" :newMessageType="newMessageType" :hasQuickReply="hasQuickReply" @removeMessage="removeMessage" @newMessage="newMessage" @insertNewNode="insertNewNode" @removeNode="removeNode" @setHoverNode="setHoverNode"></component>
            </div>

            <div v-else class="row mb-6">
                <div class="form-group col-300 mb-3 ml-3">
                    <label>選擇訊息模組<span class="form-required">*</span><a v-if="form.node.template.value" class="text-muted ml-2" :href="getTemplateUrl(form.node.template.value.id)" target="_blank">（預覽訊息模組）</a></label>
                    <vue-multiselect :class="[{'is-invalid' : form.node.template.invalid}, 'hidden-arrow multiselect-sm']" v-model="form.node.template.value" track-by="value" label="name" placeholder="搜尋訊息模組名稱" open-direction="bottom" :options="template.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="false" :loading="template.isLoading" :internal-search="false" :options-limit="10" :limit-text="templateslimitText" :max-height="300" :show-no-results="false" :hide-selected="true" @search-change="getTemplates">
                        <span slot="noResult">沒有相符的搜尋結果。</span>
                    </vue-multiselect>

                    <span class="invalid-feedback" role="alert"><strong>@{{ form.node.template.info }}</strong></span>
                </div>
            </div>

        </div>
    </div>
</script>

<script>
Vue.component('node', {
    template: '#--node',
    props: ['node', 'edittingNodesId', 'currentNodeId', 'hoverNodeId', 'hoverMessagesId', 'newMessageType', 'actionTypes' ,'quickReplyActionTypes', 'actions', 'defaultData'],
    data(){
        return {
            editting: false,
            nodeWidth: 'normal',

            hasQuickReply: false,
            hoverMessageId: null,

            counter: 0,
            currentId: this.getCurrentId(),

            template: {
                isLoading: false,
                options: [],
            },

            form: {
                node: this.node,
            },
        }
    },
    methods: {
        editNode(){
            if(!this.editting){
                this.$emit('editNode', this.node.id);
                this.$emit('setHoverMessage', null);
            }
        },
        toggleMessageTemplate(){
            if(this.form.node.type == 'message'){
                this.form.node.type = 'template';
                this.nodeWidth = 'normal';
            }else{
                this.form.node.type = 'message';
                this.setNodeWidth();
            }
        },
        closeEditNode(){
            this.$emit('closeEditNode', this.node.id);
            this.$emit('setHoverMessage', null);
        },
        dragenter(event){
            this.counter ++;
            $(event.target).addClass('dragover');
        },
        dragleave(event){
            this.counter --;
            if(this.counter === 0) $(event.target).removeClass('dragover');
        },
        drop(index){
            this.counter = 0;
            this.newMessage(index);
        },
        isDropzoneActive(){
            if(this.newMessageType == 'quickreply-message' && (this.index == this.getMessagesCount() - 1 && !this.hasQuickReply)){
                return true;
            }else if(this.newMessageType != 'quickreply-message' && this.getMessagesCount() < 5){
                return true;
            }
            return false;
        },
        getCurrentId(){
            return this.node.messages.messages.reduce((max, message) => Math.max(max, message.id.split('-')[1]), 1);
        },
        getMessagesCount(){
            return this.node.messages.messages.filter(message => message.type != 'quickreply').length;
        },
        // updateMessageData(index, data){
        //     this.$set(this.form.node.messages.messages[index], 'message', data);
        //     this.$emit('updateNodeData', this.node.id, this.form.node);
        // },
        insertNewNode(parentEnd){
            this.$emit('insertNewNode', parentEnd)
        },
        removeNode(id){
            this.$emit('removeNode', id)
        },
        setNodeWidth(){
            node = this.form.node.messages.messages.find(function(node){
                return ['imagemap', 'confirm', 'buttons', 'carousel', 'imagecarousel', 'quickreply'].includes(node.type);
            });
            this.nodeWidth = node ? 'wide' : 'normal';
        },
        recurseImagemap(action, obj){
            for(key in obj){
                if(obj[key].id && (obj[key].type.value == 'next' || obj[key].type.value == 'next-with-message')){
                    if(action == 'removeNode'){
                        this.removeNode(obj[key].action.nextId.nextId);
                    }else if(action == 'setHoverMessage'){
                        this.$emit('setHoverMessage', obj[key].action.nextId.nextId);
                    }
                }else if(obj[key][0]){
                    this.recurseImagemap(action, obj[key]);
                }
            }
        },
        traverseMessages(actionName, index){
            var vm = this,
            message = this.form.node.messages.messages[index];

            switch(message.type){
                case 'imagemap':
                this.recurseImagemap(actionName, message.message.area.area);
                break;

                case 'buttons':
                case 'confirm':
                message.message.actions.forEach(function(action){
                    if(action.type.value == 'next' || action.type.value == 'next-with-message') vm.$emit(actionName, action.action.nextId.nextId);
                });
                break;

                case 'carousel':
                case 'imagecarousel':
                case 'quickreply':
                eval('message.message.' + (message.type == 'quickreply' ? 'items.items' : 'columns.columns')).forEach(function(column){
                    column.actions.forEach(function(action){
                        if(action.type.value == 'next' || action.type.value == 'next-with-message') vm.$emit(actionName, action.action.nextId.nextId);
                    });
                });
                break;

                default:
            }
        },
        mouseenter(index, id){
            this.$emit('setHoverMessage', id);
            this.traverseMessages('setHoverMessage', index);
        },
        mouseleave(){
            this.$emit('setHoverMessage', null);
        },
        removeMessage(index){
            if(this.form.node.messages.messages[index].type == 'quickreply' || this.form.node.messages.messages.length > 1){
                if(this.form.node.messages.messages[index].type == 'quickreply') this.hasQuickReply = false;
                this.traverseMessages('removeNode', index);

                this.form.node.messages.messages.splice(index, 1);
                this.$emit('updateNodeData', this.node.id, this.form.node);
                this.setNodeWidth();

            }
        },
        setHoverNode(id){
            this.$emit('setHoverNode', id);
        },
        newMessage(index){
            if((this.getMessagesCount() < 5 && scriptForm.newMessageType) || this.newMessageType == 'quickreply-message'){
                messageType = scriptForm.newMessageType.match(/(.+)-message/);
                eval('this.new'+messageType[1].capitalize()+'Message')(index);
                // this.$emit('updateMessageData', this.form.node);
                this.setNodeWidth();
            }
        },
        newTextMessage(index){
            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'text',
                message: {
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    }
                }
            });
        },
        newImageMessage(index){
            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'image',
                message: {
                    image: this.defaultData.image,
                }
            });
        },
        newImagemapMessage(index){
            area = [];
            for(var i = 0; i < 2; i ++){
                area[i] = [];
                for(var j = 0; j < 2; j ++){
                    area[i].push({
                        id: 2*i+j+1,
                        type: {
                            name: '好友回覆關鍵字',
                            value: 'keyword',
                        },
                        action: {
                            keyword: {
                                value: null,
                                info: '',
                                invalid: false,
                            },
                            text: {
                                text: '',
                                info: '',
                                invalid: false,
                            },
                        },
                        selected: false,
                        invalid: false,
                    });
                }
            };

            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'imagemap',
                message: {
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    image: this.defaultData.image,
                    show: {
                        active: true,
                        info: '',
                        invalid: false,
                    },
                    area: {
                        area: area,
                        info: '',
                        invalid: false,
                    },
                }
            });
        },
        newButtonsMessage(index){
            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'buttons',
                message: {
                    title: {
                        title: '',
                        info: '',
                        invalid: false,
                    },
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    },
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    image: $.extend(true, {
                        active: {
                            active: true,
                            info: '',
                            invalid: false,
                        },
                        ratio: {
                            ratio: 'rectangle',
                            info: '',
                            invalid: false,
                        }
                    }, this.defaultData.image),
                    actions: [
                        $.extend(true, {id: 1}, this.defaultData.action),
                    ],
                    // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
                }
            });
        },
        newConfirmMessage(index){
            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'confirm',
                message: {
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    },
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    actions: [
                        $.extend(true, {id: 1}, this.defaultData.action),
                        $.extend(true, {id: 2}, this.defaultData.action),
                    ],
                    // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
                },
            });
        },
        newCarouselMessage(index){
            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'carousel',
                message: {
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    columns: {
                        info: '',
                        invalid: false,
                        columns:[
                            {
                                id: 1,
                                title: {
                                    title: '',
                                    info: '',
                                    invalid: false,
                                },
                                text: {
                                    text: '',
                                    info: '',
                                    invalid: false,
                                },
                                image: this.defaultData.image,
                                actions: [
                                    $.extend(true, {id: 1}, this.defaultData.action),
                                ],
                                // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
                            }
                        ],
                    },
                    title: {
                        active: {
                            active: true,
                            info: '',
                            invalid: false,
                        },
                    },
                    image: {
                        active: {
                            active: true,
                            info: '',
                            invalid: false,
                        },
                        ratio: {
                            ratio: 'rectangle',
                            info: '',
                            invalid: false,
                        }
                    },
                },
            });
        },
        newImagecarouselMessage(index){
            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'imagecarousel',
                message: {
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    columns: {
                        info: '',
                        invalid: false,
                        columns:[
                            {
                                id: 1,
                                image: this.defaultData.image,
                                actions: [
                                    $.extend(true, {id: 1}, this.defaultData.action),
                                ],
                                // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
                            }
                        ],
                    },
                },
            });
        },
        newQuickreplyMessage(index){
            this.hasQuickReply = true;

            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'quickreply',
                message: {
                    items: {
                        info: '',
                        invalid: false,
                        items:[
                            {
                                id: 1,
                                image: this.defaultData.image,
                                actions: [
                                    $.extend(true, {id: 1}, this.defaultData.action),
                                ],
                                // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
                            }
                        ],
                    },
                },
            });
        },
        getTemplateUrl(id){
            return route('solution.4.deployment.templates.show', {
                deployment: args.deployment,
                templateId: id,
            });
        },
        templateslimitText(template){
            return `and ${template} other templates`;
        },
        getTemplates(query){
            this.template.isLoading = true;
            axios
            .get(route('solution.4.deployment.messagesPool.queryForSelect', {
                deployment: args.deployment,
                type: 'template',
                q: query,
            }))
            .then(response => {
                this.template.options = response.data;
                this.template.isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this.template.options = [];
                this.template.isLoading = false;
                console.log(error);
            });
        },
    },
    created(){
        this.setNodeWidth();
        this.getTemplates('');
    },
    watch: {
        edittingNodesId(newValue){
            this.editting = newValue.includes(this.node.id)
        }
    }
});
</script>
@endpush
