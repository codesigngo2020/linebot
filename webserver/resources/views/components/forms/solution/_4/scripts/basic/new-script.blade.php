@push('js')

{{-- 新建腳本 --}}

<script type="text/x-template" id="--new-script">
    <div id="new-script-layout" class="overflow-hidden pl-4">

        <div id="tree" class="overflow-auto">
            <ul class="tree-ul justify-content-start">
                <li class="tree-li">
                    <div class="node start">
                        <div class="message-label mb-0">
                            <p class="badge badge-soft-dark">start</p>
                        </div>
                    </div>

                    <branch :nodes="form.script.script.nodes.nodes" :edittingNodesId="edittingNodesId" :currentNodeId="currentNodeId" :hoverNodeId="hoverNodeId" :hoverMessagesId="hoverMessagesId" :newMessageType="newmessagetype" :actionTypes="actionTypes" :quickReplyActionTypes="quickReplyActionTypes" :actions="actions" :defaultData="defaultData" @editNode="editNode" @closeEditNode="closeEditNode" @insertNewNode="insertNewNode" @removeNode="removeNode" @setHoverNode="setHoverNode" @setHoverMessage="setHoverMessage"></branch>
                </li>
            </ul>
        </div>

        <!-- <component v-for="(message, index) in form.messages" :key="message.id" :is="message.type+'-message'" :index="index" :messagesCount="form.messages.length" :message="message.message" :actions="['buttons', 'imagemap', 'confirm', 'carousel'].includes(message.type) ? actions : null" @updateMessageData="updateMessageData" @removeMessage="removeMessage" @newMessage="newMessage"></component> -->

    </div>
</script>

<script>
Vue.component('new-script', {
    template: '#--new-script',
    props: ['script', 'newmessagetype'],
    data(){
        return {
            edittingNodesId: [],

            currentNodeId: 0,

            hoverNodeId: null,
            hoverMessagesId: [],

            form: {
                script: this.script,
            },

            actionTypes: [
                {
                    name: '直接前往下一個節點',
                    value: 'next',
                    show: false,
                },
                {
                    name: '好友回覆訊息後前往下一個節點',
                    value: 'next-with-message',
                    show: true,
                },
                {
                    name: '回覆好友訊息',
                    value: 'message',
                    show: true,
                },
                {
                    name: '好友回覆關鍵字',
                    value: 'keyword',
                    show: true,
                },
                {
                    name: '直接開啟網址',
                    value: 'uri',
                    show: true,
                },
                {
                    name: '透過 Liff 開啟網址',
                    value: 'liff',
                    show: true,
                },
                {
                    name: '切換主選單',
                    value: 'richmenu',
                    show: true,
                },
            ],

            quickReplyActionTypes: [
                {
                    name: '直接前往下一個節點',
                    value: 'next',
                    show: false,
                },
                {
                    name: '好友回覆訊息後前往下一個節點',
                    value: 'next-with-message',
                    show: true,
                },
                {
                    name: '回覆好友訊息',
                    value: 'message',
                    show: true,
                },
                {
                    name: '好友回覆關鍵字',
                    value: 'keyword',
                    show: true,
                },
                {
                    name: '切換主選單',
                    value: 'richmenu',
                    show: true,
                },
                {
                    name: '開啟相機',
                    value: 'camera',
                    show: false,
                },
                {
                    name: '開啟相簿',
                    value: 'cameraRoll',
                    show: false,
                },
            ],

            actions: {
                'next-with-message': {
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    }
                },
                message: {
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    }
                },
                keyword: {
                    keyword: {
                        value: null,
                        info: '',
                        invalid: false,
                    },
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    }
                },
                uri: {
                    uri: {
                        uri: '',
                        info: '',
                        invalid: false,
                    }
                },
                liff: {
                    size: {
                        value: null,
                        info: '',
                        invalid: false,
                    },
                    uri: {
                        uri: '',
                        info: '',
                        invalid: false,
                    }
                },
                richmenu: {
                    richmenu: {
                        value: null,
                        info: '',
                        invalid: false,
                    }
                },
            },

            defaultData: {
                image: {
                    src: '',
                    alt: '',
                    file: null,
                    info: '',
                    invalid: false,
                },
                action: {
                    type: {
                        name: '回覆好友訊息',
                        value: 'message',
                        show: true,
                    },
                    action: {
                        label: {
                            label: '',
                            info: '',
                            invalid: false,
                        },
                        text: {
                            text: '',
                            info: '',
                            invalid: false,
                        }
                    },
                    selected: false,
                    invalid: false,
                },
            }
        }
    },
    methods: {
        // getMinStartOfNodes(nodes){
        //     return nodes.reduce((min, node) => Math.min(min, node.start), 9999);
        // },
        // getMaxEndOfNodes(nodes){
        //     return nodes.reduce((max, node) => Math.max(max, node.end), 0);
        // },
        // filterThisLayerNodes(nodes){
        //     thisLayerNodes = [];
        //     start = this.getMinStartOfNodes(nodes) - 1;
        //     end = this.getMaxEndOfNodes(nodes);
        //
        //     do{
        //         node = nodes.find(node => node.start == start + 1);
        //         thisLayerNodes.push(node);
        //         start = node.end;
        //     }while(end > node.end)
        //
        //     return thisLayerNodes;
        // },
        // filterSubNodes(start, end){
        //     return this.form.script.script.nodes.nodes.filter(function(node){
        //         return node.start > start && node.end < end;
        //     });
        // },
        getCurrentNodeId(){
            return this.form.script.script.nodes.nodes.reduce((max, node) => Math.max(max, node.id), 0);
        },
        editNode(id){
            if(this.edittingNodesId.length == 0){
                this.edittingNodesId.push(id);
            }else{
                var vm = this;
                newEdittingNodesId = [id];
                // 原有的node只留下在新node的父或子node
                newNode = this.form.script.script.nodes.nodes.find(node => node.id == id);

                // 檢查父node
                filterdNodes = this.form.script.script.nodes.nodes.filter(function(node){
                    return vm.edittingNodesId.includes(node.id) && node.start < newNode.start && node.end > newNode.end;
                });

                if(filterdNodes.length != 0){
                    parentNode = this.form.script.script.nodes.nodes.reduce(function(parentNode, node){

                        if(node.start < newNode.start && node.end > newNode.end){
                            return (!parentNode || node.start > parentNode.start) ? node : parentNode;
                        }
                        return parentNode;

                    }, null);
                    parentNode = filterdNodes.find(node => node.id == parentNode.id);
                    if(parentNode) newEdittingNodesId.push(parentNode.id);
                }

                this.edittingNodesId = newEdittingNodesId;

                // // 檢查子node
                // if(newNode.end - newNode.start != 1){
                //     filterdNodes = this.form.script.script.nodes.nodes.filter(function(node){
                //         return vm.edittingNodesId.includes(node.id) && node.start > newNode.start && node.end < newNode.end;
                //     });
                //
                //     if(filterdNodes.length != 0){
                //         childNodes = this.filterSubNodes(newNode.start, newNode.end);
                //         thisLayerNodes = this.filterThisLayerNodes(childNodes);
                //
                //         thisLayerNodes.find('')
                //     }
                // }
            }
        },
        filterThisLayerNodes(start, end){
            thisLayerNodes = [];

            do{
                node = this.form.script.script.nodes.nodes.find(node => node.start == start + 1);
                if(node){
                    thisLayerNodes.push(node);
                    start = node.end;
                }else{
                    break;
                }
            }while(end > node.end)

            return thisLayerNodes;
        },
        closeEditNode(id){
            this.edittingNodesId.splice(this.edittingNodesId.indexOf(id), 1);
            if(this.hoverNodeId){
                closedNode = this.form.script.script.nodes.nodes.find(node => node.id == id);
                if(this.filterThisLayerNodes(closedNode.start, closedNode.end).findIndex(node => node.id == this.hoverNodeId) != -1){
                    this.hoverNodeId = null;
                }
            }
        },
        insertNewNode(parentEnd){
            this.currentNodeId ++;

            this.form.script.script.nodes.nodes.forEach(function(node){
                if(node.end >= parentEnd){
                    if(node.start > parentEnd) node.start += 2;
                    node.end += 2;
                }
            });

            this.form.script.script.nodes.nodes.push({
                id: this.currentNodeId,
                start: parentEnd,
                end: parentEnd + 1,
                messages: {
                    messages: [
                        {
                            id: this.currentNodeId+'-1',
                            type: 'text',
                            message: {
                                text: {
                                    text: '',
                                    info: '',
                                    invalid: false,
                                }
                            },
                            info: '',
                            invalid: false,
                        },
                    ],
                }
            });
        },
        removeNode(id){
            var vm = this;
            index = this.form.script.script.nodes.nodes.findIndex(node => node.id == id);
            removedNode = this.form.script.script.nodes.nodes[index];
            minusNum = removedNode.end - removedNode.start + 1;
            removedNodes = [];

            this.form.script.script.nodes.nodes.forEach(function(node, index){
                if(node.start >= removedNode.start && node.end <= removedNode.end){
                    removedNodes.push(index);
                }else if(node.end >= removedNode.end){
                    if(node.start > removedNode.start) node.start -= minusNum;
                    node.end -= minusNum;
                }
            });

            removedNodes.sort((a, b) => b - a).forEach(function(index){
                vm.form.script.script.nodes.nodes.splice(index, 1);
            })
        },
        setHoverNode(id){
            this.hoverNodeId = id;
        },
        setHoverMessage(id){
            if(id){
                this.hoverMessagesId.push(id);
            }else{
                this.hoverMessagesId = [];
            }
        },
        // updateNodeData(id, node){
        //     // node = this.nodes.find(node => node.id == start + 1);
        //     console.log(id)
        //     console.log(node)
        // }
    },
    created(){
        this.currentNodeId = this.getCurrentNodeId();
    }
});
</script>

@endpush
