{{-- Image message --}}

@push('js')

<script type="text/x-template" id="--image-message">
    <div>
        <div class="d-flex message image-message">
            <div class="buttons mr-1">
                <div v-if="messagesCount > 1" class="text-muted text-danger-hover cursor-pointer" @click="remove">
                    <v-svg src="{{ asset('assets/image/svg/light/trash-alt.svg') }}" width="12" height="12"></v-svg>
                </div>
            </div>
            <div>
                <div class="type text-muted pl-1 mb-1">Image message</div>
                <div class="content">

                    <div :class="[{'is-invalid' : form.image.invalid}, form.image.src == '' ? 'un-upload' : 'uploaded', 'position-relative d-flex']">
                        <div :class="[{'i-dz-max-files-reached' : form.image.src != ''}, 'w-100 h-100 overflow-hidden border-radius-0375 idropzone dropzone-single mb-3']" ref="image">
                            <div :id="'dz-preview-'+index" class="dz-preview dz-preview-single">
                                <div class="dz-preview-cover">
                                    <img :class="[{ 'd-none' : form.image.src == '' }, 'dz-preview-img']" :src="form.image.src" :alt="form.image.alt"/>
                                </div>
                            </div>
                            <div class="dz-default dz-message">
                                <span class="mb-3">
                                    <v-svg src="{{ asset('assets/image/svg/light/image.svg') }}" width="20" height="20"></v-svg>
                                </span>
                                <span>Drop image here to upload</span>
                            </div>
                        </div>
                    </div>

                    <span class="invalid-feedback" role="alert"><strong>@{{ form.image.info }}</strong></span>

                </div>
            </div>
        </div>
        <div class="droppable" @dragover.prevent @dragenter.stop.prevent="isDropzoneActive() ? dragenter($event) : null" @dragleave.stop.prevent="isDropzoneActive() ? dragleave($event) : null" @drop.stop.prevent="isDropzoneActive() ? drop(index + 1) : null">
            <div></div>
        </div>
    </div>
</script>

<script>
Vue.component('image-message', {
    template: '#--image-message',
    props: ['index', 'node', 'messagesCount', 'message', 'newMessageType', 'hasQuickReply'],
    data(){
        return {
            counter: 0,
            form: this.message,
        }
    },
    methods: {
        remove(){
            this.$emit('removeMessage', this.index);
        },
        dragenter(event){
            this.counter ++;
            $(event.target).addClass('dragover');
        },
        dragleave(event){
            this.counter --;
            if(this.counter === 0) $(event.target).removeClass('dragover');
        },
        drop(index){
            this.counter = 0;
            this.$emit('newMessage', index);
        },
        isDropzoneActive(){
            if(this.newMessageType == 'quickreply-message' && (this.index == this.messagesCount - 1 && !this.hasQuickReply)){
                return true;
            }else if(this.newMessageType != 'quickreply-message' && this.messagesCount < 5){
                return true;
            }
            return false;
        },
    },
    mounted(){
        var vm = this;
        this.dropzone = new Dropzone(this.$refs.image, {
            url: "#",
            autoProcessQueue: false,
            maxFiles: 1,
            acceptedFiles: ".jpeg,.jpg,.png,.JPEG,.JPG,.PNG",
            thumbnailWidth: null,
            thumbnailHeight: null,
            addedfile: function(file){
                vm.form.image.info = '';
                vm.form.invalid = false;

                var img = new Image();

                img.src = window.URL.createObjectURL(file);
                img.onload = function(){
                    var width = img.naturalWidth,
                    height = img.naturalHeight;

                    if(width > 4096 || height > 4096){
                        vm.$set(vm.form.image, 'info', '圖片最大尺寸：4096 x 4096');
                        vm.$set(vm.form.image, 'invalid', true);
                        return false;
                    }

                    vm.$set(vm.form, 'image', {
                        src: window.URL.createObjectURL(file),
                        alt: file.name,
                        file: file,
                        info: '',
                        invalid: false,
                    });
                };
                return false;
            },
        });
    },
    // updated(){
    //     this.$emit('updateMessageData', this.index, this.form);
    // }
});
</script>

@endpush
