<div id="table-form" class="mb-6" v-cloak>

    <div class="row">
        {{-- 群組名稱 --}}
        <div class="form-group col-4">
            <label for="name">資料表名稱<span class="form-required">*</span></label>
            <input id="name" type="text" :class="[{'is-invalid' : form.name.invalid}, 'form-control']" v-model="form.name.name">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.name.info }}</strong></span>
        </div>

        {{-- 群組敘述 --}}
        <div class="form-group col-8">
            <label for="description">資料表敘述<span class="form-required">*</span></label>
            <input id="description" type="text" :class="[{'is-invalid' : form.description.invalid}, 'form-control']" v-model="form.description.description">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.description.info }}</strong></span>
        </div>
    </div>

    {{-- 資料表類型 --}}
    <div class="form-group">
        <label for="type">資料表類型<span class="form-required">*</span></label>
        <div :class="[{'is-invalid' : form.type.invalid}, 'i-form-control d-flex btn-group-toggle']">
            <label :class="[{active : form.type.type == 'normal'}, 'd-flex btn btn-white mr-3']">
                <input type="radio" name="type" value="normal" v-model="form.type.type" :checked="form.type.type == 'normal'">
                <span class="d-flex align-items-center mr-2">
                    <v-svg src="{{ asset('assets/image/svg/light/th-large.svg') }}" width="20" height="20"></v-svg>
                </span>
                <span>一般資料表</span>
            </label>

            <label :class="[{active : form.type.type == 'coupon'}, 'd-flex btn btn-white']">
                <input type="radio" name="type" value="coupon" v-model="form.type.type" :checked="form.type.type == 'coupon'">
                <span class="d-flex align-items-center mr-2">
                    <v-svg src="{{ asset('assets/image/svg/light/ticket.svg') }}" width="22" height="20"></v-svg>
                </span>
                <span>優惠券資料表</span>
            </label>

        </div>
        <span class="invalid-feedback" role="alert"><strong>@{{ form.type.info }}</strong></span>
    </div>

    {{-- 抽獎規則 --}}
    <div v-if="form.type.type == 'coupon'">
        <label for="type">抽獎規則<span class="form-required">*</span></label>

        <div class="row">
            {{-- 隨機領取 --}}
            <div class="form-group col-auto">
                <div class="custom-control custom-switch pt-2">
                    <input type="checkbox" class="custom-control-input" id="random" v-model="form.random.random">
                    <label :class="[{ 'text-muted' : !form.random.random }, 'custom-control-label']" for="random">@{{ form.random.random ? '隨機領取' : '依序領取' }}</label>
                </div>
                <span class="invalid-feedback" role="alert"><strong>@{{ form.random.info }}</strong></span>
            </div>

            <div class="col-auto">
                <div class="row">
                    {{-- 限制領取數量 --}}
                    <div class="form-group col-auto">
                        <div class="custom-control custom-switch pt-2">
                            <input type="checkbox" class="custom-control-input" id="limit" v-model="form.limit.active.active">
                            <label :class="[{ 'text-muted' : !form.limit.active.active }, 'custom-control-label']" for="limit">@{{ form.limit.active.active ? '限制領取數量' : '不限制領取數量' }}</label>
                        </div>
                        <span class="invalid-feedback white-space-nowrap" role="alert"><strong>@{{ form.limit.active.info }}</strong></span>
                    </div>

                    {{-- 領取數量上限 --}}
                    <div v-if="form.limit.active.active" class="form-group col-4">
                        <input id="limit-count" type="text" :class="[{'is-invalid' : form.limit.count.invalid}, 'form-control text-center']" v-model="form.limit.count.count" :disabled="!form.limit.active.active" placeholder="領取數量">
                        <span class="invalid-feedback" role="alert"><strong>@{{ form.limit.count.info }}</strong></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            {{-- 領取時間限制 --}}
            <div class="form-group col-auto">
                <div class="custom-control custom-switch pt-2">
                    <input type="checkbox" class="custom-control-input" id="issue-datetime" v-model="form.issueDatetime.active.active">
                    <label :class="[{ 'text-muted' : !form.issueDatetime.active.active }, 'custom-control-label']" for="issue-datetime">@{{ form.issueDatetime.active.active ? '啟用領取時間限制' : '停用領取時間限制' }}</label>
                </div>
                <span class="invalid-feedback" role="alert"><strong>@{{ form.issueDatetime.active.info }}</strong></span>
            </div>

            <div v-if="form.issueDatetime.active.active" class="col">
                <div class="row">
                    {{-- 限制領取日期 --}}
                    <div class="form-group col-4">
                        <input id="date" type="text" :class="[{'is-invalid' : form.issueDatetime.date.invalid}, 'form-control text-center']" aria-describedby="date" data-mask="ABCC-DC-EC ~ ABCC-DC-EC" placeholder="2019-01-01 ~ 2019-12-31" ref="date">
                        <span class="invalid-feedback" role="alert"><strong>@{{ form.issueDatetime.date.info }}</strong></span>
                    </div>

                    {{-- 限制領取時間 --}}
                    <div class="form-group col-3">
                        <input id="time" type="text" :class="[{'is-invalid' : form.issueDatetime.time.invalid}, 'form-control text-center']" aria-describedby="time" data-mask="FC:GC ~ FC:GC" placeholder="00:00 ~ 23:59" ref="time">
                        <span class="invalid-feedback" role="alert"><strong>@{{ form.issueDatetime.time.info }}</strong></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- 上傳資料 --}}
    <div v-show="!file.file">
        <label for="type">上傳資料<span class="form-required">*</span></label>
        {{-- 包含欄位名稱 --}}
        <div class="form-group">
            <div class="custom-control custom-switch pt-2">
                <input type="checkbox" class="custom-control-input" id="selected" v-model="form.hasColumnName.hasColumnName">
                <label :class="[{ 'text-muted' : !form.hasColumnName.hasColumnName }, 'custom-control-label']" for="selected">@{{ form.hasColumnName.hasColumnName ? '包含欄位名稱' : '不包含欄位名稱' }}</label>
            </div>

            <span class="invalid-feedback" role="alert"><strong>@{{ form.hasColumnName.info }}</strong></span>
        </div>

        {{-- 上傳csv --}}
        <div :class="[{'is-invalid' : file.invalid}, !file.file ? 'un-upload' : 'uploaded', 'i-form-control position-relative d-flex']">
            <div :class="[{'i-dz-max-files-reached' : file.file}, 'w-100 h-100 overflow-hidden border-radius-0375 idropzone dropzone-single mb-3']" ref="file">
                <div class="d-flex justify-content-center align-items-center dz-default dz-message">
                    <span class="mr-3">
                        <v-svg src="{{ asset('assets/image/svg/light/file-csv.svg') }}" width="25" height="25"></v-svg>
                    </span>
                    <span>Drop csv file here to upload</span>
                </div>
            </div>
        </div>
        <span class="invalid-feedback" role="alert"><strong>@{{ file.info }}</strong></span>
    </div>

    {{-- 欄位設定／新增欄位 --}}
    @foreach(['columns', 'additionalColumns'] as $type)
    <div v-if="form.rows.rows.length > 0">
        @if($type == 'columns')
        <label>欄位設定<span class="form-required">*</span></label>
        <span v-if="form.columns.invalid" class="d-flex invalid-feedback mb-3" role="alert"><strong>@{{ form.columns.info }}</strong></span>

        <div role="alert" class="alert alert-light alert-dismissible fade show">
            <span v-if="form.type.type == 'normal'">請保留特殊欄位：id, created_at, updated_at。</span>
            <span v-else-if="form.type.type == 'coupon'">上傳資料欄位必須包含：name（優惠券名稱）。<br>請保留特殊欄位：id, created_at, updated_at, user_id, redeemed_at, redemption_url, staff_member_id, qrcode_url, code。</span>
            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
        </div>
        @endif

        <div v-for="(column, columnIndex) in form.{{ $type }}.{{ $type }}" class="d-flex">

            {{-- 欄位名稱 --}}
            <div class="column-name form-group mr-4">
                <input type="text" :class="[{'is-invalid' : column.name.invalid}, 'form-control']" v-model="column.name.name" placeholder="欄位名稱" :disabled="{{ $type == 'columns' ? 'column.deleted.deleted' : 'false' }}">
                <span class="invalid-feedback" role="alert"><strong>@{{ column.name.info }}</strong></span>
            </div>

            {{-- 欄位類型 --}}
            <div class="column-type form-group mr-4">
                <vue-multiselect :class="[{'is-invalid' : column.type.invalid}, 'i-form-control hidden-arrow']" v-model="column.type.value" group-values="types" group-label="group" track-by="value" label="name" placeholder="選擇欄位類型" open-direction="bottom" :options="options.type" :show-labels="false" :allow-empty="false" :multiple="false" :internal-search="false" :max-height="300" :show-no-results="false" :hide-selected="true" :disabled="{{ $type == 'columns' ? 'column.deleted.deleted' : 'false' }}"></vue-multiselect>
                <span class="invalid-feedback" role="alert"><strong>@{{ column.type.info }}</strong></span>
            </div>

            {{-- 欄位可為空 --}}
            <div {{ $type == 'columns' ? 'v-if="!column.deleted.deleted"' : '' }} :class="[{'visibility-hidden' : column.type.value && column.type.value.value == 'timestamp'}, 'column-can-be-null form-group mr-4']">
                <div class="custom-control custom-switch pt-2">
                    <input type="checkbox" class="custom-control-input" :id="'{{ $type }}-canBeNull-'+columnIndex" v-model="column.canBeNull.canBeNull" :disabled="{{ $type == 'columns' ? 'column.deleted.deleted' : 'false' }}">
                    <label :class="[{ 'text-muted' : !column.canBeNull.canBeNull }, 'custom-control-label']" :for="'{{ $type }}-canBeNull-'+columnIndex">@{{ column.canBeNull.canBeNull ? '可為空' : '不可為空' }}</label>
                </div>
                <span class="invalid-feedback" role="alert"><strong>@{{ column.canBeNull.info }}</strong></span>
            </div>

            {{-- 欄位為唯一值 --}}
            <div v-if="{{ $type == 'columns' ? '!column.deleted.deleted' : 'true' }} && (!column.type.value || column.type.value.value != 'text')" class="column-can-be-null form-group mr-4">
                <div class="custom-control custom-switch pt-2">
                    <input type="checkbox" class="custom-control-input" :id="'{{ $type }}-isUnique-'+columnIndex" v-model="column.isUnique.isUnique" :disabled="{{ $type == 'columns' ? 'column.deleted.deleted' : 'false' }}">
                    <label :class="[{ 'text-muted' : !column.isUnique.isUnique }, 'custom-control-label']" :for="'{{ $type }}-isUnique-'+columnIndex">@{{ column.isUnique.isUnique ? '唯一值' : '非唯一值' }}</label>
                </div>
                <span class="invalid-feedback" role="alert"><strong>@{{ column.isUnique.info }}</strong></span>
            </div>

            {{-- 欄位刪除／復原 --}}
            <div class="d-flex justify-content-end align-items-center flex-grow-1 form-group text-muted noselect">
                <div {{ $type == 'columns' ? 'v-show="!column.deleted.deleted"' : '' }} class="cursor-pointer" @click="{{ $type == 'columns' ? 'deleteColumn(column)' : 'deleteAdditionalColumn(columnIndex)' }}">
                    <v-svg src="{{ asset('assets/image/svg/light/trash-alt.svg') }}" width="16" height="16"></v-svg>
                </div>
                @if($type == 'columns')
                <div v-show="column.deleted.deleted" class="cursor-pointer" @click="restoreColumn(column)">
                    <v-svg src="{{ asset('assets/image/svg/light/undo.svg') }}" width="16" height="16"></v-svg>
                </div>
                @endif
            </div>

        </div>
        @endforeach

        <div class="d-flex justify-content-center mt-n1 mb-3">
            <div class="text-muted cursor-pointer" @click="newColumn">
                <v-svg src="{{ asset('assets/image/svg/light/plus-circle.svg') }}" width="16" height="16"></v-svg>
            </div>
        </div>
    </div>








    {{-- 資料表預覽 --}}
    <div v-if="form.rows.rows.length > 0">
        <label>資料表預覽（前 10 筆）</label>
        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive position-relative">

                    <div :class="[is_loading ? 'd-flex' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-bl-05 border-radius-br-05 z-index-1']">
                        <div class="spinner-grow text-secondary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>

                    <table class="table table-sm table-nowrap card-table text-left">
                        <thead>
                            <tr>
                                <th v-for="column in form.columns.columns" v-if="!column.deleted.deleted" class="text-transform-none">@{{ column.name.name }}</th>
                                <th v-for="column in form.additionalColumns.additionalColumns" class="text-transform-none">@{{ column.name.name }}</th>
                            </tr>
                        </thead>
                        <tbody class="list">
                            <tr v-for="row in form.rows.rows.slice(0, 10)">
                                <td v-for="column in form.columns.columns" v-if="!column.deleted.deleted">@{{ row[column.originalName.originalName] }}</td>
                                <td v-for="column in form.additionalColumns.additionalColumns"></td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="small text-right">@{{ '共 '+form.rows.rows.length+' 筆資料' }}</div>
    </div>

    <div v-if="dataErrors.length > 0" class="small text-danger">
        <span v-for="error in dataErrors" class="mr-2"><strong>@{{ error }}</strong></span>
    </div>






    <hr class="mt-5 mb-5">

    {{ Form::button('新增資料表', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, "btn btn-block btn-primary"]', '@click'=>'submit']) }}

</div>
