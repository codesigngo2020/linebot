@push('js')

{{-- 回覆訊息 --}}
<script type="text/x-template" id="--reply">
    <div>
        <div class="row">
            {{-- 訊息設定 --}}
            <div class="form-group col-12">
                <label for="messageType">訊息設定<span class="form-required">*</span></label>

                <div v-if="form.messageType.messageType == 'pool'" class="alert alert-dismissible fade show alert-light text-justify font-size-0875 mb-3" role="alert">
                    選擇「從訊息庫中複製」，可對複製的訊息進行更改，並不會影響原本訊息庫中的訊息
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div :class="[{'is-invalid' : form.messageType.invalid}, 'i-form-control d-flex btn-group-toggle']">
                    <label :class="[{active : form.messageType.messageType == 'pool'}, 'd-flex btn btn-white mr-3']">
                        <input type="radio" name="messageType" value="pool" v-model="form.messageType.messageType" :checked="form.messageType.messageType == 'pool'">
                        <span class="d-flex align-items-center mr-2">
                            <v-svg src="{{ asset('assets/image/svg/light/box-open.svg') }}" width="21" height="21"></v-svg>
                        </span>
                        <span>從訊息庫中複製</span>
                    </label>

                    <label :class="[{active : form.messageType.messageType == 'template'}, 'd-flex btn btn-white mr-3']">
                        <input type="radio" name="messageType" value="template" v-model="form.messageType.messageType" :checked="form.messageType.messageType == 'template'">
                        <span class="d-flex align-items-center mr-2">
                            <v-svg src="{{ asset('assets/image/svg/regular/cubes.svg') }}" width="20" height="20"></v-svg>
                        </span>
                        <span>使用訊息模組</span>
                    </label>

                    <label :class="[{active : form.messageType.messageType == 'new'}, 'd-flex btn btn-white']">
                        <input type="radio" name="messageType" value="new" v-model="form.messageType.messageType" :checked="form.messageType.messageType == 'new'">
                        <span class="d-flex align-items-center mr-2">
                            <v-svg src="{{ asset('assets/image/svg/light/comment-alt-lines.svg') }}" width="20" height="18"></v-svg>
                        </span>
                        <span>新建訊息</span>
                    </label>

                </div>
                <span class="invalid-feedback" role="alert"><strong>@{{ form.messageType.info }}</strong></span>
            </div>
        </div>

        {{-- 從訊息庫中複製／使用訊息模組->選擇訊息 --}}
        <div v-if="form.messageType.messageType == 'pool' || form.messageType.messageType == 'template'" class="row">
            {{-- 選擇訊息／使用訊息模組 --}}
            <div class="form-group col-12 col-md-6">
                <label for="actionType">@{{ form.messageType.messageType == 'pool' ? '選擇訊息' : '選擇訊息模組' }}<span class="form-required">*</span><a v-if="form.messageType.messageType == 'template' && form.message.value" class="text-muted ml-2" :href="getMessageUrl(form.message.value.id)" target="_blank">（預覽訊息模組）</a></label>
                <vue-multiselect :class="[{'is-invalid' : form.message.invalid}, 'i-form-control hidden-arrow']" v-model="form.message.value" track-by="value" label="name" :placeholder="form.messageType.messageType == 'pool' ? '搜尋訊息名稱' : '搜尋訊息模組名稱'" open-direction="bottom" :options="message.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="false" :loading="message.isLoading" :internal-search="false" :options-limit="10" :limit-text="messageslimitText" :max-height="400" :show-no-results="false" :hide-selected="true" @search-change="getMessages">
                    <span slot="noResult">沒有相符的搜尋結果。</span>
                </vue-multiselect>
                <span class="invalid-feedback" role="alert"><strong>@{{ form.message.info }}</strong></span>
            </div>
        </div>

        <new-message v-if="form.messageType.messageType == 'new' || (form.messageType.messageType == 'pool' && form.message.value)" :isloading="isLoading" :newmessagetype="newMessageType" :messages="form.messages.messages" :hasquickreply="hasQuickReply" @updateisloading="updateIsLoading" @updatemessagesdata="updateMessagesData" @updatehasquickreply="updateHasQuickReply"></new-message>

        <message-types v-if="form.messageType.messageType == 'new' || (form.messageType.messageType == 'pool' && form.message.value)" formtype="message" :hasquickreply="hasQuickReply" @updatenewmessagetype="updateNewMessageType"></message-types>
    </div>
</script>

<script>
Vue.component('trigger-reply', {
    template: '#--reply',
    props: ['initial', 'type', '_form'],
    data(){
        return {
            isLoading: false,
            newMessageType: null,
            hasQuickReply: false,

            message: {
                isLoading: false,
                options: [],
            },

            form: this.initial && this.type == 'reply' ? this._form : {
                messageType: {
                    messageType: 'new',
                    info: '',
                    invalid: false,
                },
                message: {
                    value: null,
                    info: '',
                    invalid: false,
                },
                messages: {
                    messages: [
                        {
                            id: 1,
                            type: 'text',
                            message: {
                                text: {
                                    text: '',
                                    info: '',
                                    invalid: false,
                                }
                            }
                        },
                    ],
                    info: '',
                    invalid: false,
                }
            }
        }
    },
    methods: {
        updateIsLoading(isLoading){
            this.isLoading = isLoading;
        },
        updateMessagesData(data){
            this.$set(this.form.messages, 'messages', data);
        },
        updateHasQuickReply(hasQuickReply){
            this.hasQuickReply = hasQuickReply;
        },
        updateNewMessageType(type){
            this.newMessageType = type;
        },
        messageslimitText(message){
            return `and ${message} other messages`;
        },
        getMessages(query){
            this.message.isLoading = true;
            axios
            .get(route('solution.4.deployment.messagesPool.queryForSelect', {
                deployment: args.deployment,
                type: this.form.messageType.messageType == 'pool' ? 'message' : 'template',
                q: query,
            }))
            .then(response => {
                this.message.options = response.data;
                this.message.isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this.message.options = [];
                this.message.isLoading = false;
                console.log(error);
            });
        },
        getMessageUrl(id){
            return route('solution.4.deployment.messagesPool.show', {
                deployment: args.deployment,
                messageId: id,
            });
        },
    },
    created(){
        this.$emit('update-trigger', this.form);
        this.getMessages('');
    },
    updated(){
        this.$emit('update-trigger', this.form);
    },
    watch: {
        'form.messageType.messageType': function(newValue){
            this.message.options = [];
            this.form.message.value = null;
            if(newValue == 'new'){
                this.form.message.value = null;
            }else{
                this.getMessages('');
            }
            this.form.message.info = '';
            this.form.message.invalid = false;
        },
        'form.message.value': function(newValue){
            if(newValue && this.form.messageType.messageType == 'pool'){
                this.isLoading = true;
                axios
                .get(route('solution.4.deployment.messagesPool.getMessage', {
                    deployment: args.deployment,
                    messageId: newValue.value,
                }))
                .then(response => {
                    console.log(response)
                    this.form.messages.messages = response['data'];

                    this.hasQuickReply = this.form.messages.messages[this.form.messages.messages.length - 1].type == 'quickreply';

                    var vm = this;
                    this.$nextTick(function(){
                        vm.isLoading = false;
                    })
                }).catch(error => {
                    this.isLoading = false;
                    console.log(error);
                });
            }
        }
    }
});
</script>

@endpush
