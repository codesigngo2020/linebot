@push('js')

{{-- 切換選單 --}}
<script type="text/x-template" id="--richmenu">
    <div>
        <div class="row">
            <div class="form-group col-12 col-lg-6">
                <label for="select-richmenu">選擇選單<span class="form-required">*</span><a v-if="form.richmenu.value" class="text-muted ml-2" :href="getRichmenuUrl(form.richmenu.value.id)" target="_blank">（預覽選單）</a></label>
                <vue-multiselect :class="[{'is-invalid' : form.richmenu.invalid}, 'i-form-control hidden-arrow']" v-model="form.richmenu.value" track-by="value" label="name" placeholder="搜尋腳本名稱" open-direction="bottom" :options="richmenu.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="false" :loading="richmenu.isLoading" :internal-search="false" :options-limit="10" :limit-text="richmenuslimitText" :max-height="600" :show-no-results="false" :hide-selected="true" @search-change="getRichmenus">
                    <span slot="noResult">沒有相符的搜尋結果。</span>
                </vue-multiselect>
                <span class="invalid-feedback" role="alert"><strong>@{{ form.richmenu.info }}</strong></span>
            </div>
            <div v-if="isLoading" class="d-flex align-items-center mt-30 height-40">
                <div class="spinner-grow text-secondary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
        <div v-if="richmenu.richmenu.image != ''" class="row">
            <div class="form-group col-12">
                <label>預覽選單</label>
                <div class="richmenu-preview">
                    <div :class="[richmenu.richmenu.size == '2500x1686' ? 'type-1' : 'type-2', 'position-relative imege']">
                        <img :src="richmenu.richmenu.image[0].url">
                    </div>
                    <p class="chatbar-text-preview">
                        <span>@{{ richmenu.richmenu.chatBarText }}</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</script>

<script>
Vue.component('trigger-richmenu', {
    template: '#--richmenu',
    props: ['initial', 'type', '_form'],
    data(){
        return {
            isLoading: false,
            form: this.initial && this.type == 'richmenu' ? this._form : {
                richmenu: {
                    value: null,
                    info: '',
                    invalid: false,
                },
            },
            richmenu: {
                isLoading: false,
                options: [],
                richmenu: {
                    image: '',
                    size: '',
                    chatBarText: '',
                }
            }
        }
    },
    methods: {
        getRichmenuUrl(id){
            return route('solution.4.deployment.richmenus.show', {
                deployment: args.deployment,
                richmenuId: id,
            });
        },
        richmenuslimitText(richmenu){
            return `and ${richmenu} other richmenus`;
        },
        getRichmenus(query){
            this.richmenu.isLoading = true;
            axios
            .get(route('solution.4.deployment.richmenus.queryForSelect', {
                deployment: args.deployment,
                q: query,
            }))
            .then(response => {
                this.richmenu.options = response.data;
                this.richmenu.isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this.richmenu.options = [];
                this.richmenu.isLoading = false;
                console.log(error);
            });
        },
    },
    created(){
        this.$emit('update-trigger', this.form);
        this.getRichmenus('');
    },
    updated(){
        this.$emit('update-trigger', this.form);
    },
    watch: {
        'form.richmenu.value': function(newValue, oldValue){
            if(newValue != oldValue){
                this.isLoading = true;
                this.richmenu.richmenu.image = '';

                axios
                .get(route('solution.4.deployment.keywords.getRichmenu', {
                    deployment: args.deployment,
                    parent_id: newValue.value,
                }))
                .then(response => {
                    console.log(response)
                    this.richmenu.richmenu = response['data'];
                    this.isLoading = false;
                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            }
        }
    }
});
</script>

@endpush
