<div id="keyword-form" class="mb-6" v-cloak>

    <div class="row">
        {{-- 關鍵字名稱 --}}
        <div class="form-group col-12 col-lg-4">
            <label for="name">關鍵字名稱<span class="form-required">*</span></label>
            <input id="name" type="text" :class="[{'is-invalid' : form.name.invalid}, 'form-control']" v-model="form.name.name" {{ $action == 'edit' ? 'disabled' : '' }}>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.name.info }}</strong></span>
        </div>

        {{-- 關鍵字描述 --}}
        <div class="form-group col-12 col-lg-8">
            <label for="description">關鍵字描述<span class="form-required">*</span></label>
            <input id="description" type="text" :class="[{'is-invalid' : form.description.invalid}, 'form-control']" v-model="form.description.description" {{ $action == 'edit' ? 'disabled' : '' }}>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.description.info }}</strong></span>
        </div>
    </div>

    {{-- 啟用狀態 --}}
    <div class="row">
        <div class="form-group col-12">
            <label for="active">啟用狀態<span class="form-required">*</span></label>

            <div :class="[{'is-invalid' : form.active.invalid}, 'i-form-control d-flex btn-group-toggle']">
                <label :class="[{active : form.active.active == '1'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="active" value="1" v-model="form.active.active" :checked="form.active.active == '1'" {{ $action == 'edit' ? 'disabled' : '' }}>
                    <span>Active</span>
                </label>

                <label :class="[{active : form.active.active == '0'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="active" value="0" v-model="form.active.active" :checked="form.active.active == '0'" {{ $action == 'edit' ? 'disabled' : '' }}>
                    <span>Inactive</span>
                </label>

            </div>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.active.info }}</strong></span>
        </div>
    </div>

    <div class="row">
        {{-- 觸發行為 --}}
        <div class="form-group col-12">
            <label for="triggerType">觸發行為<span class="form-required">*</span></label>
            <div :class="[{'is-invalid' : form.triggerType.invalid}, 'i-form-control d-flex btn-group-toggle']">
                <label :class="[{active : form.triggerType.triggerType == 'reply'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="triggerType" value="reply" v-model="form.triggerType.triggerType" :checked="form.triggerType.triggerType == 'reply'">
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/light/paper-plane.svg') }}" width="20" height="18"></v-svg>
                    </span>
                    <span>回覆訊息</span>
                </label>

                <label :class="[{active : form.triggerType.triggerType == 'script'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="triggerType" value="script" v-model="form.triggerType.triggerType" :checked="form.triggerType.triggerType == 'script'">
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/regular/network-wired.svg') }}" width="18" height="18"></v-svg>
                    </span>
                    <span>開啟腳本</span>
                </label>

                <label :class="[{active : form.triggerType.triggerType == 'richmenu'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="richmenu" value="richmenu" v-model="form.triggerType.triggerType" :checked="form.triggerType.triggerType == 'richmenu'">
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/light/info-circle.svg') }}" width="18" height="18"></v-svg>
                    </span>
                    <span>切換選單</span>
                </label>

            </div>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.triggerType.info }}</strong></span>
        </div>
    </div>

    {{-- 觸發行為 --}}
    @include('components.forms.solution._4.keywords.create-partials.reply')
    @include('components.forms.solution._4.keywords.create-partials.script')
    @include('components.forms.solution._4.keywords.create-partials.richmenu')

    {{-- 建立訊息 --}}
    @include('components.forms.solution._4.basic.message.new-message')
    @include('components.forms.solution._4.basic.message.message-types')
    @include('components.forms.solution._4.basic.message.messages')

    <component :is="'trigger-'+form.triggerType.triggerType" :initial="isInitial" :type="form.triggerType.triggerType" :_form="form.trigger.trigger" @update-trigger="updateTrigger"></component>

    <hr class="mt-5 mb-5">

    {{ Form::button($action == 'create' ? '新增關鍵字' : '新增版本', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, "btn btn-block btn-primary"]', '@click'=>'submit']) }}

</div>
