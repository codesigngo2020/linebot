@push('js')

{{-- 觸發腳本 --}}
<script type="text/x-template" id="--script">
    <div class="row">
        <div class="form-group col-12 col-lg-6">
            <label for="select-script">選擇腳本<span class="form-required">*</span><a v-if="form.script.value" class="text-muted ml-2" :href="getScriptUrl(form.script.value.value)" target="_blank">（預覽腳本）</a></label>
            <vue-multiselect :class="[{'is-invalid' : form.script.invalid}, 'i-form-control hidden-arrow']" v-model="form.script.value" track-by="value" label="name" placeholder="搜尋腳本名稱" open-direction="bottom" :options="script.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="false" :loading="script.isLoading" :internal-search="false" :close-on-select="false" :options-limit="10" :limit-text="scriptslimitText" :max-height="600" :show-no-results="false" :hide-selected="true" @search-change="getScripts">
                <span slot="noResult">沒有相符的搜尋結果。</span>
            </vue-multiselect>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.script.info }}</strong></span>
        </div>
    </div>
</script>

<script>
Vue.component('trigger-script', {
    template: '#--script',
    props: ['initial', 'type', '_form'],
    data(){
        return {
            script: {
                isLoading: false,
                options: [],
            },
            form: this.initial && this.type == 'script' ? this._form : {
                script: {
                    value: null,
                    info: '',
                    invalid: false,
                }
            }
        }
    },
    methods: {
        getScriptUrl(id){
            return route('solution.4.deployment.scripts.show', {
                deployment: args.deployment,
                scriptId: id,
            });
        },
        scriptslimitText(script){
            return `and ${script} other scripts`;
        },
        getScripts(query){
            this.script.isLoading = true;
            axios
            .get(route('solution.4.deployment.scripts.queryForSelect', {
                deployment: args.deployment,
                q: query,
            }))
            .then(response => {
                this.script.options = response.data;
                this.script.isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this.script.options = [];
                this.script.isLoading = false;
                console.log(error);
            });
        },
    },
    created(){
        this.$emit('update-trigger', this.form);
        this.getScripts('');
    },
    updated(){
        this.$emit('update-trigger', this.form);
    },
});
</script>

@endpush
