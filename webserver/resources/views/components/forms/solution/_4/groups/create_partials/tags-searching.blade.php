@push('js')

{{-- 搜尋標籤 --}}

<script type="text/x-template" id="--tags-searching">
    <div id="tags-searching" :class="[{active : active}, 'z-index-999']">
        <div id="tags-searching-switch" @click="toggleActive">
            <v-svg v-show="!active" src="{{ asset('assets/image/svg/regular/chevron-left.svg') }}" width="18" height="18"></v-svg>
            <v-svg v-show="active" src="{{ asset('assets/image/svg/regular/chevron-right.svg') }}" width="18" height="18"></v-svg>
        </div>

        <div :class="[isLoading ? 'd-flex z-index-999' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-05']">
            <div class="spinner-grow text-secondary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>

        <div class="position-relative w-100 h-100 p-4 z-index-1 overflow-auto">

            {{-- Search --}}
            <form class="d-flex align-items-center border-radius-0375 bg-white mb-3" @submit.prevent="search">
                <div class="col-auto pr-0">
                    <span class="color-d2ddec">
                        <v-svg src="{{ asset('assets/image/svg/light/search.svg') }}" width="18" height="18"></v-svg>
                    </span>
                </div>
                <div class="col">
                    <input type="search" class="form-control form-control-flush search" placeholder="搜尋標籤名稱" v-model="query" @change="search">
                </div>
            </form>

            <vue-multiselect class="hidden-arrow" v-model="category.value" track-by="value" label="name" placeholder="請選擇類別" openDirection="bottom" :options="category.options" :searchable="false" :show-labels="false" :allow-empty="false"></vue-multiselect>

            <hr>

            <div v-for="tag in tags" class="type mb-3 border-radius-0375 bg-white cursor-pointer p-3" draggable="true" @dragstart.stop="dragstart(tag.id, tag.name)" @dragend.stop="dragend">
                <h5 class="text-overflow-ellipsis white-space-nowrap overflow-hidden">
                    <a :href="getTagUrl(tag.id)" target="_blank">
                        <v-svg class="svg text-warning" src="{{ asset('assets/image/svg/solid/tags.svg') }}" width="14" height="14"></v-svg>
                    </a>
                    <span>@{{ tag.name }}</span>
                </h5>
                <div class="small text-muted" v-html="tag.description"></div>
            </div>

            <div v-show="paginator.lastPage > 1" class="paginator d-flex justify-content-end text-secondary">
                <div v-show="paginator.currentPage != 1" :class="{'mr-2' : paginator.currentPage != paginator.lastPage}" @click="changePage(paginator.currentPage - 1)">
                    <v-svg src="{{ asset('assets/image/svg/regular/chevron-left.svg') }}" width="14" height="14"></v-svg>
                </div>
                <div v-show="paginator.currentPage != paginator.lastPage" @click="changePage(paginator.currentPage + 1)">
                    <v-svg src="{{ asset('assets/image/svg/regular/chevron-right.svg') }}" width="14" height="14"></v-svg>
                </div>
            </div>

        </div>


    </div>
</script>

<script>
Vue.component('tags-searching', {
    template: '#--tags-searching',
    data(){
        return {
            active: false,
            isLoading: true,

            query: '',
            page: 0,
            category: {
                value: {
                    name: '全部類別',
                    value: 0,
                },
                options: [
                    {
                        name: '全部類別',
                        value: 0,
                    }
                ],
            },

            tags: [],
            paginator: {
                currentPage: 1,
                lastPage: 1,
            }
        }
    },
    methods: {
        toggleActive(){
            this.active = !this.active;
        },
        changePage(page){
            this.getTags(page);
        },
        getTags(page){
            this.isLoading = true;
            axios
            .get(route('solution.4.deployment.groups.getTags', {
                deployment: args.deployment,
                page: page,
                q: this.query,
            }))
            .then(response => {
                this.paginator = response.data.paginator;
                this.tags = response.data.tags;
                this.isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this.isLoading = false;
                console.log(error);
            });
        },
        search(){
            this.getTags(1);
        },
        getTagUrl(id){
            return route('solution.4.deployment.tags.show', {
                deployment: args.deployment,
                tagId: id,
            });
        },
        dragstart(tagId, tagName){
            this.$emit('updatedraggingtag', tagId, tagName);
            // this.$emit('updatenewmessagetype', event.currentTarget.id);
        },
        dragend(){
            this.$emit('updatedraggingtag', 0, '');
            $(".droppable", $("#group-condition-layout")).removeClass('dragover');
            // this.$emit('updatenewmessagetype', null);
            // $(".droppable", $("#new-message-layout")).removeClass('dragover');
        }
    },
    created(){
        this.getTags();
    }
});
</script>

@endpush
