<div id="group-form" class="mb-6" v-cloak>

    <div class="row">
        {{-- 群組名稱 --}}
        <div class="form-group col-4">
            <label for="name">群組名稱<span class="form-required">*</span></label>
            <input id="name" type="text" :class="[{'is-invalid' : form.name.invalid}, 'form-control']" v-model="form.name.name">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.name.info }}</strong></span>
        </div>

        {{-- 群組敘述 --}}
        <div class="form-group col-8">
            <label for="description">群組敘述<span class="form-required">*</span></label>
            <input id="description" type="text" :class="[{'is-invalid' : form.description.invalid}, 'form-control']" v-model="form.description.description">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.description.info }}</strong></span>
        </div>
    </div>


    @include('components.forms.solution._4.groups.create_partials.tags-searching')
    @include('components.forms.solution._4.groups.create_partials.group-condition')
    @include('components.forms.solution._4.groups.create_partials.circles.init')

    @for($i = 1; $i <= 3; $i ++)
    @include('components.forms.solution._4.groups.create_partials.circles.'.$i)
    @endfor

    <tags-searching @updatedraggingtag="updateDraggingTag"></tags-searching>

    {{-- 群組條件 --}}
    <div class="row">
        <div class="form-group col-12">
            <label for="conditions">群組條件<span class="form-required">*</span></label>

            <div class="alert alert-light alert-dismissible fade show" role="alert">
                運算邏輯：點選交聯集組合中的區域，每組交聯集計算出集合後，再與其他組進行聯集。
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <group-condition :class="[{'is-invalid' : form.circles.invalid}, 'i-form-control']" :circles="form.circles" :draggingtag="draggingTag"></group-condition>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.circles.info }}</strong></span>
        </div>
    </div>

    <div class="alert alert-light alert-dismissible fade show" role="alert">
        運算邏輯：在「群組條件」中篩選出好友集合後，加上「新增好友名單」，最後移除「移除好友名單」。
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>

    {{-- 新增好友名單 --}}
    <div class="row">
        <div class="form-group col-12">
            <label for="add-users">新增好友名單<span class="form-required">*</span></label>
            <vue-multiselect :class="[{'is-invalid' : form.addUsers.invalid}, 'i-form-control hidden-arrow']" v-model="form.addUsers.value" track-by="value" label="name" placeholder="搜尋好友名稱" open-direction="bottom" :options="addUsers.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="true" :loading="addUsers.isLoading" :internal-search="false" :clear-on-select="false" :close-on-select="false" :options-limit="10" :limit-text="limitText" :max-height="600" :show-no-results="false" :hide-selected="true" @search-change="getAddUsers">
                <span slot="noResult">沒有相符的搜尋結果。</span>
            </vue-multiselect>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.addUsers.info }}</strong></span>
        </div>
    </div>

    {{-- 移除好友名單 --}}
    <div class="row">
        <div class="form-group col-12">
            <label for="remove-users">移除好友名單<span class="form-required">*</span></label>
            <vue-multiselect :class="[{'is-invalid' : form.removeUsers.invalid}, 'i-form-control hidden-arrow']" v-model="form.removeUsers.value" track-by="value" label="name" placeholder="搜尋好友名稱" open-direction="bottom" :options="removeUsers.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="true" :loading="removeUsers.isLoading" :internal-search="false" :clear-on-select="false" :close-on-select="false" :options-limit="10" :limit-text="limitText" :max-height="600" :show-no-results="false" :hide-selected="true" @search-change="getRemoveUsers">
                <span slot="noResult">沒有相符的搜尋結果。</span>
            </vue-multiselect>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.removeUsers.info }}</strong></span>
        </div>
    </div>






    <hr class="mt-5 mb-5">

    {{ Form::button('新增群組', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, "btn btn-block btn-primary"]', '@click'=>'submit']) }}

</div>
