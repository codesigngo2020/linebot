@push('js')

{{-- 好友組條件 --}}

<script type="text/x-template" id="--group-condition">
    <div id="group-condition-layout" class="position-relative overflow-hidden pl-4 noselect">

        <div class="d-flex overflow-auto">

            <div v-for="(circle, circleIndex) in form.circles.circles">
                <component :class="[{'is-invalid' : circle.selectedAreas.invalid}, 'i-form-control']" :key="circle.id" :is="'circles-'+circle.circlesCount" :circleIndex="circleIndex" :circle="circle" @addCircle="addCircle" @addSelectedArea="addSelectedArea" @removeSelectedArea="removeSelectedArea"></component>
                <span v-if="circle.selectedAreas.invalid" class="invalid-feedback" role="alert"><strong>@{{ circle.selectedAreas.info }}</strong></span>
            </div>

            <circles-init v-if="circles.circles.length < 3" @newCircles="newCircles"></circles-init>

        </div>

    </div>
</script>

<script>
Vue.component('group-condition', {
    template: '#--group-condition',
    props: ['circles', 'draggingtag'],
    data(){
        return {
            currentId: 0,
            form: {
                circles: this.circles,
            }
        };
    },
    methods: {
        newCircles(){
            this.currentId ++;
            this.form.circles.circles.push({
                id: this.currentId,
                circlesCount: 1,
                areas: {
                    areas: [
                        {
                            tagId: this.draggingtag.id,
                            tagName: this.draggingtag.name,
                        }
                    ],
                    info: '',
                    invalid: false,
                },
                selectedAreas: {
                    selectedAreas: [1],
                    info: '',
                    invalid: false,
                },
            });
        },
        addCircle(circleIndex){
            if(!this.form.circles.circles[circleIndex].areas.areas.find(area => area.tagId == this.draggingtag.id)){
                this.form.circles.circles[circleIndex].areas.areas.push({
                    tagId: this.draggingtag.id,
                    tagName: this.draggingtag.name,
                });
                this.form.circles.circles[circleIndex].selectedAreas.selectedAreas = [];
                this.form.circles.circles[circleIndex].circlesCount ++;
            }
        },
        addSelectedArea(circleIndex, areaId){
            this.form.circles.circles[circleIndex].selectedAreas.selectedAreas.push(areaId);
            this.form.circles.circles[circleIndex].selectedAreas.selectedAreas = this.form.circles.circles[circleIndex].selectedAreas.selectedAreas.sort();
        },
        removeSelectedArea(circleIndex, areaIndex){
            this.form.circles.circles[circleIndex].selectedAreas.selectedAreas.splice(areaIndex, 1);
        },
        setCurrentId(){
            maxId = this.form.circles.circles.reduce((max, circle) => Math.max(max, circle.id), 1);
            this.currentId = maxId;
        }
    },
    created(){
        this.setCurrentId();
    },
    watch: {
        // messages(newValue){
        //     if(this.isloading){
        //         var vm = this;
        //         this.form.messages = [];
        //
        //         newValue.forEach(function(message){
        //             vm.currentId ++;
        //             vm.form.messages.push($.extend(true, {id: vm.currentId}, message));
        //         });
        //         this.$emit('updateisloading', false);
        //         this.$emit('updatemessagesdata', this.form.messages);
        //     }
        // }
    }
});
</script>

@endpush
