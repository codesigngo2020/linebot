@push('js')

<script type="text/x-template" id="--circles-init">
    <div class="d-flex">
        <div class="circles droppable init border-radius-05" @dragover.prevent @dragenter.stop.prevent="dragenter($event)" @dragleave.stop.prevent="dragleave($event)" @drop.prevent="drop">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350 260">
                <path class="area" fill="transparent" stroke="#d2ddec" stroke-width="2px" d="M137.5,130c0-27.8,15.1-52,37.5-64.9C164,58.7,151.2,55,137.5,55
                c-41.4,0-75,33.6-75,75c0,41.4,33.6,75,75,75c13.7,0,26.5-3.7,37.5-10.1C152.6,182,137.5,157.8,137.5,130z"/>
                <path class="area" fill="transparent" stroke="#d2ddec" stroke-width="2px" d="M212.5,55c-13.7,0-26.5,3.7-37.5,10.1c22.4,13,37.5,37.2,37.5,64.9
                s-15.1,52-37.5,64.9c11,6.4,23.8,10.1,37.5,10.1c41.4,0,75-33.6,75-75C287.5,88.6,253.9,55,212.5,55z"/>
                <path class="area" fill="transparent" stroke="#d2ddec" stroke-width="2px" d="M175,65.1c-22.4,13-37.5,37.2-37.5,64.9s15.1,52,37.5,64.9
                c22.4-13,37.5-37.2,37.5-64.9S197.4,78,175,65.1z"/>

                <polyline fill="none" stroke="#d2ddec" stroke-linecap="round" stroke-width="2px" points="46,29.5 122,29.5 136.8,54.5"/>
                <text x="46px" y="20px" width="114px" fill="#95aac9" font-size="12">
                    <tspan>標籤名稱</tspan>
                </text>
                <polyline fill="none" stroke="#d2ddec" stroke-linecap="round" stroke-width="2px" points="307.45,243 229.65,243 212.3,205.5"/>
                <text x="231.65px" y="233.5px" width="114px" fill="#95aac9" font-size="12">
                    <tspan>標籤名稱</tspan>
                </text>
            </svg>
        </div>
    </div>
</script>

<script>
Vue.component('circles-init', {
    template: '#--circles-init',
    data(){
        return {
            counter: 0,
        };
    },
    methods: {
        dragenter(event){
            this.counter ++;
            if(this.counter > 0) $(event.currentTarget).addClass('dragover');
        },
        dragleave(event){
            this.counter --;
            if(this.counter <= 0) $(event.currentTarget).removeClass('dragover');
        },
        drop(){
            this.counter = 0;
            this.$emit('newCircles');
        },
    },
});
</script>
@endpush
