@push('js')

<script type="text/x-template" id="--message-action">
    <div class="message-action">
        <div class="form-group">
            <label for="text">訊息內容<span class="form-required">*</span></label>
            <div :class="[{'is-invalid' : form.text.invalid}, 'i-form-control card mb-0 w-auto']">
                <div class="card-body text-justify p-3">
                    <div class="texarea-not-input position-relative">
                        <pre ref="inputPre"></pre>
                        <textarea class="not-input" placeholder="請輸入訊息內容" v-model="form.text.text"></textarea>
                    </div>
                </div>
            </div>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.text.info }}</strong></span>
        </div>
    </div>
</script>

<script>
Vue.component('message-action', {
    template: '#--message-action',
    props: ['id'],
    data: function(){
        return {
            form: {
                text: {
                    text: '',
                    info: '',
                    invalid: false,
                }
            }
        };
    },
    methods: {
        refreshData(){
            actionData = richmenuForm.getActionData();
            if(actionData){
                this.$set(this, 'form', actionData);
            }else{
                this.$set(this, 'form', {
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    }
                });
            }
        }
    },
    created(){
        this.refreshData();
    },
    updated(){
        richmenuForm.updateActionData(this.form)
    },
    watch: {
        id(){
            this.refreshData();
        },
        'form.text.text': function(newValue){
            this.$refs.inputPre.innerHTML = newValue.replaceAll(/\r\n|\r|\n/g, '<br>')+'<br>';
        },
    }
});
</script>
@endpush
