@push('js')

<script type="text/x-template" id="--uri-action">
    <div>
        <div class="form-group">
            <label for="uri">開啟網址<span class="form-required">*</span></label>
            <input id="uri" type="url" :class="[{'is-invalid' : form.uri.invalid}, 'form-control']" v-model="form.uri.uri">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.uri.info }}</strong></span>
        </div>
    </div>
</script>

<script>
Vue.component('uri-action', {
    template: '#--uri-action',
    props: ['id'],
    data: function(){
        return {
            form: {
                uri: {
                    uri: '',
                    info: '',
                    invalid: false,
                }
            }
        };
    },
    methods: {
        refreshData(){
            actionData = richmenuForm.getActionData();
            if(actionData){
                this.$set(this, 'form', actionData);
            }else{
                this.$set(this, 'form', {
                    uri: {
                        uri: '',
                        info: '',
                        invalid: false,
                    }
                });
            }
        }
    },
    created(){
        this.refreshData();
    },
    updated(){
        richmenuForm.updateActionData(this.form)
    },
    watch: {
        id(){
            this.refreshData();
        },
    }
});
</script>
@endpush
