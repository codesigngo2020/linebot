@push('js')

<script type="text/x-template" id="--liff-action">
    <div>
        <div class="form-group">
            <label for="actionType">Liff App 尺寸<span class="form-required">*</span></label>
            <vue-multiselect :class="[{'is-invalid' : form.size.invalid}, 'hidden-arrow']" v-model="form.size.value" track-by="value" label="name" placeholder="請選擇尺寸" openDirection="bottom" :options="size.options" :searchable="false" :show-labels="false" :allow-empty="false"></vue-multiselect>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.size.info }}</strong></span>
        </div>
        <div class="form-group">
            <label for="text">開啟網址<span class="form-required">*</span></label>
            <input id="text" type="url" :class="[{'is-invalid' : form.uri.invalid}, 'form-control']" v-model="form.uri.uri">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.uri.info }}</strong></span>
        </div>
    </div>
</script>

<script>
Vue.component('liff-action', {
    template: '#--liff-action',
    props: ['id'],
    data: function(){
        return {
            form: {
                size: {
                    value: null,
                    info: '',
                    invalid: false,
                },
                uri: {
                    uri: '',
                    info: '',
                    invalid: false,
                }
            },
            size: {
                options: {!! $liffSizes->toJson() !!},
            }
        };
    },
    methods: {
        refreshData(){
            actionData = richmenuForm.getActionData();
            if(actionData){
                this.$set(this, 'form', actionData);
            }else{
                this.$set(this, 'form', {
                    size: {
                        value: null,
                        info: '',
                        invalid: false,
                    },
                    uri: {
                        uri: '',
                        info: '',
                        invalid: false,
                    }
                });
            }
        }
    },
    created(){
        this.refreshData();
    },
    updated(){
        richmenuForm.updateActionData(this.form)
    },
    watch: {
        id(){
            this.refreshData();
        },
    }
});
</script>
@endpush
