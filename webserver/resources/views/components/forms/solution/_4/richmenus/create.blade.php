<div id="richmenu-form" class="mb-6" v-cloak>

    <div class="row">
        {{-- 選單名稱 --}}
        <div class="form-group col-12 col-lg-6">
            <label for="name">選單名稱<span class="form-required">*</span></label>
            <input id="name" type="text" :class="[{'is-invalid' : form.name.invalid}, 'form-control col-xl-11']" v-model="form.name.name" {{ $action == 'edit' ? 'disabled' : '' }}>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.name.info }}</strong></span>
        </div>

        {{-- 選單敘述 --}}
        <div class="form-group col-12 col-lg-6">
            <label for="description">選單敘述<span class="form-required">*</span></label>
            <input id="description" type="text" :class="[{'is-invalid' : form.description.invalid}, 'form-control col-xl-11']" v-model="form.description.description" {{ $action == 'edit' ? 'disabled' : '' }}>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.description.info }}</strong></span>
        </div>
    </div>

    <div class="row">
        {{-- 資訊條內容 --}}
        <div class="form-group col-12 col-xl-6">
            <label for="chatBarText">資訊條內容<span class="form-required">*</span></label>
            <input id="chatBarText" type="text" :class="[{'is-invalid' : form.chatBarText.invalid}, 'form-control col-xl-11']" v-model="form.chatBarText.chatBarText">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.chatBarText.info }}</strong></span>
        </div>

        {{-- 自動開啟 --}}
        <div class="form-group col-12 col-lg-6">
            <label for="selected">自動開啟<span class="form-required">*</span></label>

            <div class="custom-control custom-switch pt-2">
                <input type="checkbox" class="custom-control-input" id="selected" v-model="form.selected.selected">
                <label :class="[{ 'text-muted' : !form.selected.selected }, 'custom-control-label']" for="selected">@{{ form.selected.selected ? '進入聊天機器人，自動開啟主選單' : '點擊下方資訊條，才會開啟主選單' }}</label>
            </div>

            <span class="invalid-feedback" role="alert"><strong>@{{ form.selected.info }}</strong></span>
        </div>
    </div>

    <div class="row">
        {{-- 尺寸選擇 --}}
        <div class="form-group col-12 col-xl-6">
            <label for="size">尺寸選擇<span class="form-required">*</span></label>

            <div class="d-flex btn-group-toggle col-xl-11 nopadding">
                <label :class="[{ active : form.size.size == '2500x1686' }, 'btn btn-white flex-grow-1 pl-4 pr-4 mr-4']">
                    <input type="radio" name="size" value="2500x1686" v-model="form.size.size">2500 x 1686
                </label>
                <label :class="[{ active : form.size.size == '2500x843' }, 'btn btn-white flex-grow-1 pl-4 pr-4']">
                    <input type="radio" name="size" value="2500x843" v-model="form.size.size">2500 x 843
                </label>
            </div>

            <span class="invalid-feedback" role="alert"><strong>@{{ form.size.info }}</strong></span>
        </div>
    </div>

    <hr class="mt-5 mb-5">

    <div class="row">
        {{-- 選單內容 --}}
        <div class="form-group col-12 col-lg-6">
            <label>選單內容<span class="form-required">*</span></label>
            <div :class="[{'is-invalid' : form.image.invalid}, 'i-form-control row mt-2']">
                <div class="col-xl-11">

                    <div id="richmenu-layout" :class="[form.size.size == '2500x1686' ? 'type-1' : 'type-2', 'position-relative']">

                        {{-- 圖片上傳 --}}
                        <div id="richmenuImage" :class="[{'i-dz-max-files-reached' : form.image.src != ''}, 'position-absolute top w-100 h-100 overflow-hidden border-radius-0375 idropzone dropzone-single mb-3']">
                            <div id="dz-preview" class="dz-preview dz-preview-single">
                                <div class="dz-preview-cover">
                                    <img id="preview-img" :class="[{ 'd-none' : form.image.src == '' }, 'dz-preview-img']" :src="form.image.src" :alt="form.image.alt"/>
                                </div>
                            </div>
                            <div :class="[{'z-index--1' : form.image.src != ''}, 'dz-default dz-message']">
                                <span>Drop image here to upload</span>
                            </div>
                            <div id="image-input" ref="imageInput"></div>
                        </div>

                        {{-- 隔線 --}}
                        <div class="position-absolute top w-100 h-100 overflow-hidden cursor-pointer">
                            <div id="grid" class="grid">
                                <grid-tr v-for="(tds, index) in form.area.area" :tds="tds" :key="index"></grid-tr>
                            </div>
                        </div>

                        {{-- 資訊條內容 --}}
                        <div id="chatBarTextPreview">@{{ form.chatBarText.chatBarText }}</div>

                    </div>
                </div>
                <div id="change-image" v-if="form.image.src != ''" class="col-xl-11 text-center text-muted cursor-pointer mt-3" @click="changeImage">change image</div>
            </div>

            <span class="invalid-feedback" role="alert"><strong>@{{ form.image.info }}</strong></span>
        </div>

        {{-- 區域動作 --}}
        <div class="form-group col-12 col-lg-6">
            <label>區域分割<span class="form-required">*</span><span>@{{ '（區域數：'+currentAreaCount+'，上限 20）' }}</span></label>

            <div class="d-flex mb-4 mt-2">
                <div v-for="i in [2,3,5]" :id="'new-row-'+i" class="new-grid grid mr-4 bg-transparent" draggable="true" @dragstart.stop="dragstart($event)" @dragend.stop="dragend($event)">
                    <div v-for="j in i" class="tr">
                        <div class="td"></div>
                    </div>
                </div>

                <div v-for="i in [2,3,5]" :id="'new-column-'+i" :class="[{'mr-4' : i != 5} ,'new-grid grid bg-transparent']" draggable="true" @dragstart.stop="dragstart($event)" @dragend.stop="dragend($event)">
                    <div class="tr">
                        <div v-for="j in i" class="td"></div>
                    </div>
                </div>

            </div>

            <hr class="mt-3 mb-4">

            <div class="form-group">
                <label for="actionType">區域行為<span class="form-required">*</span><span>@{{ !form.actionType.currentAreaId ? '（請先選擇編輯區域）' : '（正在編輯區域 '+form.actionType.currentAreaId+'）' }}</span></label>
                <vue-multiselect :class="[{'is-invalid' : form.actionType.invalid}, 'hidden-arrow']" v-model="form.actionType.value" track-by="value" label="name" placeholder="請選擇行為" :options="form.actionType.options" :searchable="false" :show-labels="false" :allow-empty="false" :disabled="!form.actionType.currentAreaId" @input="changeActionType"></vue-multiselect>
                <span class="invalid-feedback" role="alert"><strong>@{{ form.actionType.info }}</strong></span>
            </div>

            <component v-if="form.actionType.currentAreaId && form.actionType.value" :is="form.actionType.value.value+'-action'" :id="form.actionType.currentAreaId"></component>

        </div>
    </div>


    <hr class="mt-5 mb-5">

    {{ Form::button($action == 'create' ? '新增選單' : '新增版本', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, "btn btn-block btn-primary"]', '@click'=>'submit']) }}

</div>

@push('js')
<script type="text/x-template" id="--grid-tr">
    <div class="tr">
        <div v-for="td in tds" :key="td.id" :class="[{'has-error' : td.id && td.invalid}, {'selected' : td.id && td.selected}, 'td']" :data-id="td.id ? td.id : ''" @dragover.prevent @dragenter.stop.prevent="td.id ? dragenter($event) : null" @dragleave.stop.prevent="td.id ? dragleave($event) : null" @drop.stop.prevent="td.id ? drop($event) : null" @click.stop="td.id ? selectArea($event, td.id) : null" @mouseover.stop="td.id ? mouseover($event) : null" @mouseleave.stop="td.id ? mouseleave($event) : null">
            <div v-if="td.id" class="index">
                <span class="id position-absolute">@{{ td.id }}</span>
                <span class="times position-absolute" @click.stop="removeArea(td.id)">
                    <v-svg src="{{ asset('assets/image/svg/light/times.svg') }}" width="16" height="16"></v-svg>
                </span>
            </div>
            <grid-tr v-else v-for="tds in td" :tds="tds" :key="td.id"></grid-tr>
        </div>
    </div>
</script>

<script>
Vue.component('grid-tr', {
    template: '#--grid-tr',
    props: ['tds'],
    methods: {
        dragenter(event){
            $(event.target).addClass('dragover');
        },
        dragleave(event){
            $(event.target).removeClass('dragover');
        },
        drop(event){
            richmenuForm.newArea(event.target.getAttribute('data-id'));
            $(event.target).removeClass('dragover');
        },
        removeArea(id){
            richmenuForm.removeArea(id)
        },
        mouseover(event){
            $(event.target).addClass('hover');
        },
        mouseleave(event){
            $(event.target).removeClass('hover');
        },
        selectArea(event, id){
            if(richmenuForm.form.actionType.currentAreaId != id){
                richmenuForm.saveAndClearAction(id, false);
            }
        }
    }
});
</script>
@endpush

@foreach(['message', 'keyword', 'uri', 'liff', 'script', 'richmenu'] as $type)
@include('components.forms.solution._4.richmenus.create_partials.actions.'.$type)
@endforeach
