@push('js')
<script type="text/x-template" id="--nav-form-2">
    <div class="tab-pane fade" id="nav-form-2" role="tabpanel" aria-labelledby="nav-form-2-tab">
        <div class="deployment-info mb-4">
            <div class="info-item">您即將部署一個新的<strong>LINE 聊天機器人方案</strong>，請依照指示完成機器人設定。</div>
        </div>

        <div class="row">
            {{-- 機器人名稱 --}}
            <div class="form-group col-12 col-sm-6">
                <label for="name">機器人名稱<span class="form-required">*</span></label>
                <input id="name" type="text" :class="[{'is-invalid' : form.name.invalid}, 'form-control']" v-model="form.name.name">
                <span class="invalid-feedback" role="alert"><strong>@{{ form.name.info }}</strong></span>
            </div>

            {{-- 機器人描述 --}}
            <div class="form-group col-12 col-sm-6">
                <label for="description">機器人描述<span class="form-required">*</span></label>
                <input id="description" type="text" :class="[{'is-invalid' : form.description.invalid}, 'form-control']" v-model="form.description.description">
                <span class="invalid-feedback" role="alert"><strong>@{{ form.description.info }}</strong></span>
            </div>
        </div>

        <div class="row">
            {{-- Channel ID --}}
            <div class="form-group col-12 col-sm-6">
                <label for="channelId">Channel ID<span class="form-required">*</span></label>
                <input id="channelId" type="text" :class="[{'is-invalid' : form.channelId.invalid}, 'form-control']" v-model="form.channelId.channelId">
                <span class="invalid-feedback" role="alert"><strong>@{{ form.channelId.info }}</strong></span>
            </div>

            {{-- Channel Secret --}}
            <div class="form-group col-12 col-sm-6">
                <label for="channelSecret">Channel Secret<span class="form-required">*</span></label>
                <input id="channelSecret" type="text" :class="[{'is-invalid' : form.channelSecret.invalid}, 'form-control']" v-model="form.channelSecret.channelSecret">
                <span class="invalid-feedback" role="alert"><strong>@{{ form.channelSecret.info }}</strong></span>
            </div>
        </div>

        {{-- Channel Access Token --}}
        <div class="form-group">
            <label for="channelAccessToken">Channel Access Token<span class="form-required">*</span></label>
            <input id="channelAccessToken" type="text" :class="[{'is-invalid' : form.channelAccessToken.invalid}, 'form-control']" v-model="form.channelAccessToken.channelAccessToken">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.channelAccessToken.info }}</strong></span>
        </div>

        <div class="row">
            {{-- Basic ID --}}
            <div class="form-group col-12 col-sm-6">
                <label for="basicId">Basic ID<span class="form-required">*</span></label>
                <input id="basicId" type="text" :class="[{'is-invalid' : form.basicId.invalid}, 'form-control']" v-model="form.basicId.basicId" placeholder="@xxxxxxxx">
                <span class="invalid-feedback" role="alert"><strong>@{{ form.basicId.info }}</strong></span>
            </div>

            {{-- Your User ID --}}
            <div class="form-group col-12 col-sm-6">
                <label for="yourUserID">Your User ID<span class="form-required">*</span></label>
                <input id="yourUserID" type="text" :class="[{'is-invalid' : form.yourUserID.invalid}, 'form-control']" v-model="form.yourUserID.yourUserID">
                <span class="invalid-feedback" role="alert"><strong>@{{ form.yourUserID.info }}</strong></span>
            </div>
        </div>

        {{-- 機器人頭貼 --}}
        <div>
            <label for="profile">機器人頭貼<span class="form-required">*</span></label>
            <div :class="[{'is-invalid' : form.image.invalid}, form.image.src == '' ? 'un-upload' : 'uploaded', 'i-form-control position-relative d-flex']" style="height: 200px;width: 200px;">
                <div :class="[{'i-dz-max-files-reached' : form.image.src != ''}, 'w-100 h-100 overflow-hidden border-radius-0375 idropzone dropzone-single mb-3']" ref="image">
                    <div id="dz-preview" class="dz-preview dz-preview-single">
                        <div class="dz-preview-cover">
                            <img :class="[{ 'd-none' : form.image.src == '' }, 'dz-preview-img']" :src="form.image.src" :alt="form.image.alt"/>
                        </div>
                    </div>
                    <div class="dz-default dz-message">
                        <span class="mb-3">
                            <v-svg src="{{ asset('assets/image/svg/light/image.svg') }}" width="25" height="25"></v-svg>
                        </span>
                        <span class="font-size-0875">Drop image here to upload</span>
                    </div>
                </div>
            </div>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.image.info }}</strong></span>
        </div>

        <hr class="mt-5 mb-5">

        {{ Form::button('提交部署', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, "btn btn-block btn-primary"]', '@click'=>'submit']) }}

    </div>
</script>

<script>
Vue.component('nav-form-2', {
    template: '#--nav-form-2',
    props: ['_form'],
    data(){
        return {
            submitting: false,
            form: {
                name: {
                    name: '',
                    info: '',
                    invalid: false,
                },
                description: {
                    description: '',
                    info: '',
                    invalid: false,
                },
                channelId: {
                    channelId: '',
                    info: '',
                    invalid: false,
                },
                channelSecret: {
                    channelSecret: '',
                    info: '',
                    invalid: false,
                },
                channelAccessToken: {
                    channelAccessToken: '',
                    info: '',
                    invalid: false,
                },
                basicId: {
                    basicId: '',
                    info: '',
                    invalid: false,
                },
                yourUserID: {
                    yourUserID: '',
                    info: '',
                    invalid: false,
                },
                image: {
                    src: '',
                    alt: '',
                    file: null,
                    info: '',
                    invalid: false,
                },
            }
        }
    },
    methods: {
        AllInvalidToFalse(obj){
            for(key in obj){
                if(key == 'invalid'){
                    obj[key] = false;
                    obj['info'] = '';
                }else if(typeof(obj[key]) === 'object'){
                    this.AllInvalidToFalse(obj[key]);
                }
            }
        },
        buildFormData(formData, data, parentKey){
            var vm = this;
            if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                Object.keys(data).forEach(key => {
                    vm.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                });
            } else {
                const value = data == null ? '' : data;
                formData.append(parentKey, value);
            }
        },
        jsonToFormData(data){
            let formData = new FormData();
            this.buildFormData(formData, data, 'form');
            return formData;
        },
        submit(){
            if(!this.submitting){
                this.submitting = true;
                //this.form[4] = this.form_;
                this.$set(this._form, 2, this.form);
                this.AllInvalidToFalse(this._form);

                formData = this.jsonToFormData(this._form);
                formData.append('_token', args._token);

                axios.post(route('solution.4.store', args.deployment),
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    console.log(response);
                    if(response.data.status == 'success'){
                        if(response.data.approvalUrl){
                            redirectUrl = response.data.approvalUrl;
                        }else{
                            redirectUrl = route('solution.4.deployment.show', {
                                deployment: response.data.deployment,
                            });
                        }
                        window.open(redirectUrl, '_self');
                    }
                    this.submitting = false;

                }).catch(error => {
                    var errors = error.response.data.errors;
                    this.submitting = false;
                    var invalid_form = null;
                    for(error in errors){
                        console.log(error);
                        var invalid_form_index = error.match(/^form\.([0-9]+)\./)[1];
                        if(!invalid_form || invalid_form_index < invalid_form) invalid_form = invalid_form_index;

                        // 找出最後一個「.」後的字串
                        var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                        // 如果是value，表示是select，再往前找一個value
                        if(last == 'value'){
                            select_last = error.match(/\.(value\.value)$/);
                            // 如果符合「.value.value」，表示是多層value
                            if(select_last) last = select_last.slice(-1)[0];
                        }

                        // 替換form成_form
                        error_ref = error.replace(new RegExp('form', 'g'), '_form');

                        // 替換數字成以[]包覆表示
                        error_ref = error_ref.replace(new RegExp('.([0-9]+).', 'g'), '[$1].');

                        info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        try{
                            this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                            this.$set(eval('this.'+invalid_ref), 'invalid', true);
                        }catch(e){

                        }
                    }

                    if(invalid_form) $(deploy_form.$refs['nav-link-'+invalid_form]).tab('show');
                });

                this.$emit('update-form-data', null, this._form);
            }
        }
    },
    mounted(){
        var vm = this;
        this.dropzone = new Dropzone(this.$refs.image, {
            url: "#",
            autoProcessQueue: false,
            maxFiles: 1,
            acceptedFiles: ".jpeg,.jpg,.png,.JPEG,.JPG,.PNG",
            thumbnailWidth: null,
            thumbnailHeight: null,
            addedfile: function(file){
                vm.form.image.info = '';
                vm.form.invalid = false;

                var img = new Image();

                img.src = window.URL.createObjectURL(file);
                img.onload = function(){
                    var width = this.naturalWidth,
                    height = this.naturalHeight;

                    if(width > 1024 || height > 1024){
                        vm.$set(vm.form.image, 'info', '圖片最大尺寸：1024 x 1024');
                        vm.$set(vm.form.image, 'invalid', true);
                        return false;
                    }

                    if(width != height){
                        vm.$set(vm.form.image, 'info', '圖片尺寸必須是正方形');
                        vm.$set(vm.form.image, 'invalid', true);
                        return false;
                    }

                    vm.$set(vm.form, 'image', {
                        src: window.URL.createObjectURL(file),
                        alt: file.name,
                        file: file,
                        info: '',
                        invalid: false,
                    });
                };
                return false;
            },
        });
    }
});
</script>
@endpush
