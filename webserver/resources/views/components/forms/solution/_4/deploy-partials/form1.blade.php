@push('js')
<script type="text/x-template" id="--nav-form-1">
    <div class="tab-pane fade show active" id="nav-form-1" role="tabpanel" aria-labelledby="nav-form-1-tab">

        <div class="deployment-info mb-4">
            <div class="info-item">您僅限使用一次免費方案。</div>
        </div>

        {{-- 方案選擇 --}}
        <div class="row">
            @foreach($plans as $plan)
            <div class="col-12 col-lg-4">
                {{-- Card --}}
                <div class="card">

                    @if(isset($plan->disabled) && $plan->disabled)
                    <!-- <div class="mask"></div> -->
                    @endif

                    <div class="card-body">

                        {{-- Title --}}
                        <h6 class="text-uppercase text-center text-muted my-4">{{ $plan->name }}</h6>

                        {{-- Price --}}
                        <div class="row no-gutters align-items-center justify-content-center">
                            <div class="col-auto">
                                <div class="h2 mb-0 mr-2">$</div>
                            </div>
                            <div class="col-auto d-flex">
                                <div class="display-2 mb-0 mr-2">{{ $plan->price }}</div>
                                <div class="d-flex align-items-end h5 mr-2 mb-3">USD</div>
                            </div>
                        </div> {{-- / .row --}}

                        {{-- Period --}}
                        <div class="h6 text-uppercase text-center text-muted mb-5">/ month</div>

                        {{-- Features --}}
                        <div class="mb-3">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                                    <small>平台使用</small>
                                    <v-svg class="text-success" src="{{ asset('assets/image/svg/light/check-circle.svg') }}" width="16" height="16"></v-svg>
                                </li>
                                <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                                    <small>訊息腳本</small>
                                    <v-svg class="text-success" src="{{ asset('assets/image/svg/light/check-circle.svg') }}" width="16" height="16"></v-svg>
                                </li>
                                <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                                    <small>聊天室</small>
                                    <v-svg class="text-success" src="{{ asset('assets/image/svg/light/check-circle.svg') }}" width="16" height="16"></v-svg>
                                </li>
                                <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                                    <small>訊息推播數</small>
                                    <small>{{ $plan->data->broadcasts_count }} / month</small>
                                </li>
                            </ul>
                        </div>

                        {{-- Button --}}
                        <button type="button" :class="[form.plan == {{ $plan->id }} ? 'btn-primary' : 'btn-light', 'btn btn-block']" @click="changePlan({{ $plan->id }})">選用{{ $plan->name }}</button>
                    </div>
                </div>
            </div>
            @endforeach
        </div> {{-- / .row --}}


        {{-- 客制方案申請 --}}
        <div class="row">
            <div class="col-12">
                {{-- Card --}}
                <div class="card card-inactive">
                    <div class="card-body">
                        {{-- Title --}}
                        <h3 class="text-center">想要客製化方案嗎？</h3>
                        {{-- Text --}}
                        <p class="text-muted text-center">我們提供適當的客製化服務，如果以上方案或部署方法無法滿足您的新奇點子，歡迎告訴我們，將有專人盡快與您聯繫。</p>
                        {{-- Button --}}
                        <div class="text-center">
                            <a href="#!" class="btn btn-outline-secondary">客製方案申請</a>
                        </div>
                    </div>
                </div>

            </div>
        </div> {{-- / .row --}}

    </div>
</script>

<script>
Vue.component('nav-form-1', {
    template: '#--nav-form-1',
    props: ['form'],
    methods: {
        changePlan(id){
            this.form.plan = id;
        }
    },
    created(){
        this.$emit('update-form-data', 1, this.form);
    },
    updated(){
        this.$emit('update-form-data', 1, this.form);
    }
});
</script>
@endpush
