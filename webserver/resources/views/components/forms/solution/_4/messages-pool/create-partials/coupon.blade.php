@push('js')

{{-- 優惠券模組 --}}
<script type="text/x-template" id="--coupon">
    <div class="row">
        <div class="form-group col-6">
            <label for="select-table">選擇優惠券資料表<span class="form-required">*</span><a v-if="form.table.value" class="text-muted ml-2" :href="getTableUrl(form.table.value.value)" target="_blank">（資料表內容）</a></label>
            <vue-multiselect :class="[{'is-invalid' : form.table.invalid}, 'i-form-control hidden-arrow']" v-model="form.table.value" track-by="value" label="name" placeholder="搜尋資料表名稱" open-direction="bottom" :options="table.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="false" :loading="table.isLoading" :internal-search="false" :close-on-select="false" :options-limit="10" :limit-text="tableslimitText" :max-height="300" :show-no-results="false" :hide-selected="true" @search-change="getTables">
                <span slot="noResult">沒有相符的搜尋結果。</span>
            </vue-multiselect>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.table.info }}</strong></span>
        </div>
    </div>
</script>

<script>
Vue.component('coupon', {
    template: '#--coupon',
    props: ['templateSettings'],
    data(){
        return {
            table: {
                isLoading: false,
                options: [],
            },
            form: this.templateSettings,
        }
    },
    methods: {
        getTableUrl(id){
            return route('solution.4.deployment.tables.show', {
                deployment: args.deployment,
                tableId: id,
            });
        },
        tableslimitText(table){
            return `and ${table} other tables`;
        },
        getTables(query){
            this.table.isLoading = true;
            axios
            .get(route('solution.4.deployment.tables.queryForSelect', {
                deployment: args.deployment,
                type: 'coupon',
                fields: ['settings'],
                q: query,
            }))
            .then(response => {
                this.table.options = response.data;
                this.table.isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this.table.options = [];
                this.table.isLoading = false;
                console.log(error);
            });
        },
    },
    created(){
        this.getTables('');
    },
});
</script>

@endpush
