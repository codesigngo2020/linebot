<div id="message-form" class="mb-6" v-cloak>

    <div class="row">
        {{-- 訊息名稱 --}}
        <div class="form-group col-4">
            <label for="name">訊息名稱<span class="form-required">*</span></label>
            <input id="name" type="text" :class="[{'is-invalid' : form.name.invalid}, 'form-control']" v-model="form.name.name" {{ $action == 'edit' ? 'disabled' : '' }}>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.name.info }}</strong></span>
        </div>

        {{-- 訊息描述 --}}
        <div class="form-group col-8">
            <label for="description">訊息描述<span class="form-required">*</span></label>
            <input id="description" type="text" :class="[{'is-invalid' : form.description.invalid}, 'form-control']" v-model="form.description.description" {{ $action == 'edit' ? 'disabled' : '' }}>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.description.info }}</strong></span>
        </div>
    </div>

    {{-- 訊息類型 --}}
    <div class="row">
        <div class="form-group col-auto">
            <label for="type">訊息類型<span class="form-required">*</span></label>
            <div :class="[{'is-invalid' : form.type.invalid}, 'i-form-control d-flex btn-group-toggle']">
                <label :class="[{active : form.type.type == 'message'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="type" value="message" v-model="form.type.type" :checked="form.type.type == 'message'" {{ $action == 'edit' ? 'disabled' : '' }}>
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/light/comment-alt-lines.svg') }}" width="20" height="18"></v-svg>
                    </span>
                    <span>一般訊息</span>
                </label>

                <label :class="[{active : form.type.type == 'template'}, 'd-flex btn btn-white']">
                    <input type="radio" name="type" value="template" v-model="form.type.type" :checked="form.type.type == 'template'" {{ $action == 'edit' ? 'disabled' : '' }}>
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/regular/cubes.svg') }}" width="20" height="20"></v-svg>
                    </span>
                    <span>訊息模組</span>
                </label>

            </div>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.type.info }}</strong></span>
        </div>

        {{-- 選擇訊息模組 --}}
        <div v-if="form.type.type == 'template'" class="flex-column align-items-end form-group with-fake-label col-4 pl-0">
            <vue-multiselect :class="[{'is-invalid' : form.templateType.invalid}, 'hidden-arrow']" v-model="form.templateType.value" track-by="value" label="name" placeholder="請選擇模組類型" open-direction="bottom" :options="templateType.options" :searchable="false" :show-labels="false" :allow-empty="false"></vue-multiselect>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.templateType.info }}</strong></span>
        </div>
    </div>

    @include('components.forms.solution._4.messages-pool.create-partials.coupon')

    <div v-if="form.type.type == 'template' && form.templateType.value">
        <conponent :is="form.templateType.value.value" :template-settings="form.templateSettings"></conponent>
    </div>

    {{-- 建立訊息／建立訊息模組 --}}
    @foreach(['new-message', 'message-types', 'new-template', 'branch', 'node'] as $file)
    @include('components.forms.solution._4.basic.message.'.$file)
    @endforeach

    @include('components.forms.solution._4.basic.message.messages', ['template' => true])

    <label>@{{ form.type.type == 'message' ? '建立訊息' : '建立訊息模組' }}<span class="form-required">*</span></label>

    <new-message v-if="form.type.type == 'message'" :isloading="false" :newmessagetype="newMessageType" :messages="form.messages.messages" :hasquickreply="hasQuickReply" @updatehasquickreply="updateHasQuickReply"></new-message>
    <new-template v-if="form.type.type == 'template'" :template-type="form.templateType.value" :template="form.template" :template-settings="form.templateSettings" :newmessagetype="newMessageType"></new-template>

    <message-types :formtype="form.type.type" :hasquickreply="hasQuickReply" @updatenewmessagetype="updateNewMessageType"></message-types>

    <hr class="mt-5 mb-5">

    {{ Form::button($action == 'create' ? '新增訊息' : '新增版本', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, "btn btn-block btn-primary"]', '@click'=>'submit']) }}

</div>
