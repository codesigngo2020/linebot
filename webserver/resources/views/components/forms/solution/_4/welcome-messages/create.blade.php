<div id="welcome-message-form" class="mb-6" v-cloak>

    <div class="row">
        {{-- 歡迎訊息名稱 --}}
        <div class="form-group col-12 col-lg-5">
            <label for="name">歡迎訊息名稱<span class="form-required">*</span></label>
            <input id="name" type="text" :class="[{'is-invalid' : form.name.invalid}, 'form-control']" v-model="form.name.name">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.name.info }}</strong></span>
        </div>

        {{-- 歡迎訊息敘述 --}}
        <div class="form-group col-12 col-lg-7">
            <label for="description">歡迎訊息敘述<span class="form-required">*</span></label>
            <input id="description" type="text" :class="[{'is-invalid' : form.description.invalid}, 'form-control']" v-model="form.description.description">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.description.info }}</strong></span>
        </div>
    </div>

    <div class="row">
        {{-- 啟用狀態 --}}
        <div class="form-group col-12">
            <label for="active">啟用狀態<span class="form-required">*</span></label>

            <div :class="[{'is-invalid' : form.active.invalid}, 'i-form-control d-flex btn-group-toggle']">
                <label :class="[{active : form.active.active == '1'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="active" value="1" v-model="form.active.active" :checked="form.active.active == '1'">
                    <span>Active</span>
                </label>

                <label :class="[{active : form.active.active == '0'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="active" value="0" v-model="form.active.active" :checked="form.active.active == '0'">
                    <span>Inactive</span>
                </label>

            </div>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.active.info }}</strong></span>
        </div>
    </div>

    {{-- 時間區段 --}}
    <div class="form-group">
        <label for="period">時間區段<span class="form-required">*</span></label>

        <div class="alert alert-light alert-dismissible fade show" role="alert">
            可以至多建立5個時間區段，時段間可以互相重疊，覆蓋時間內自動回覆都將有效。
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>

        <div id="period-layout" class="position-relative overflow-hidden pt-0 pb-0">

            <div v-for="(period, periodIndex) in form.periods.periods" :key="period.id" class="mt-4 mb-4">
                <div class="type text-muted pl-2 mb-2">@{{ '時間區段 '+(periodIndex+1) }}</div>
                <div class="card bg-transparent mb-0">
                    <div class="card-body">

                        <div class="position-absolute top right mt-4 mr-4 text-muted cursor-pointer z-index-1" @click="removePeriod(periodIndex)">
                            <v-svg class="svg" src="{{ asset('assets/image/svg/regular/trash-alt.svg') }}" width="16" height="16"></v-svg>
                        </div>

                        {{-- 結束日期 --}}
                        <div class="form-group mb-2">
                            <div :class="[{'is-invalid' : period.hasEnd.invalid}, 'i-form-control custom-control custom-switch pt-2']">
                                <input type="checkbox" class="custom-control-input" :id="'hasEnd-'+period.id" v-model="period.hasEnd.hasEnd">
                                <label :class="[{ 'text-muted' : !period.hasEnd.hasEnd }, 'custom-control-label']" :for="'hasEnd-'+period.id">結束時間</label>
                            </div>
                            <span class="invalid-feedback" role="alert"><strong>@{{ period.hasEnd.info }}</strong></span>
                        </div>

                        <div class="row">

                            {{-- 開始日期 --}}
                            <div class="form-group col-12 col-lg-6 mb-2">
                                <div :class="[{'is-invalid' : period.date.start.invalid}, 'i-form-control input-group mb-3']">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" :id="'date-start-addon-'+period.id">開始日期</span>
                                    </div>
                                    <input type="text" class="form-control" :id="'date-start-'+period.id" :aria-describedby="'date-start-addon-'+period.id" data-mask="ABCC-DC-EC" placeholder="2019-12-25" :ref="'date-start-'+period.id">
                                </div>
                                <span class="invalid-feedback" role="alert"><strong>@{{ period.date.start.info }}</strong></span>
                            </div>

                            {{-- 結束日期 --}}
                            <div v-show="period.hasEnd.hasEnd" class="form-group col-12 col-lg-6 mb-2">
                                <div :class="[{'is-invalid' : period.date.end.invalid}, 'i-form-control input-group mb-3']">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" :id="'date-end-addon-'+period.id">結束日期</span>
                                    </div>
                                    <input type="text" class="form-control" :id="'date-end-'+period.id" :aria-describedby="'date-end-addon-'+period.id" data-mask="ABCC-DC-EC" placeholder="2019-12-25" :ref="'date-end-'+period.id">
                                </div>
                                <span class="invalid-feedback" role="alert"><strong>@{{ period.date.end.info }}</strong></span>
                            </div>

                        </div>

                        <div class="row">

                            {{-- 開始時間 --}}
                            <div class="form-group col-12 col-lg-6 mb-0">
                                <div :class="[{'is-invalid' : period.time.start.invalid}, 'i-form-control input-group mb-3']">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" :id="'time-start-addon-'+period.id">開始時間</span>
                                    </div>
                                    <input type="text" class="form-control" :id="'time-start-'+period.id" :aria-describedby="'time-start-addon-'+period.id" data-mask="FC:GC" placeholder="00:00" :ref="'time-start-'+period.id">
                                </div>
                                <span class="invalid-feedback" role="alert"><strong>@{{ period.time.start.info }}</strong></span>
                            </div>

                            {{-- 結束時間 --}}
                            <div class="form-group col-12 col-lg-6 mb-0">
                                <div :class="[{'is-invalid' : period.time.end.invalid}, 'i-form-control input-group mb-3']">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" :id="'time-end-addon-'+period.id">結束時間</span>
                                    </div>
                                    <input type="text" class="form-control" :id="'time-end-'+period.id" :aria-describedby="'time-end-addon-'+period.id" data-mask="FC:GC" placeholder="23:59" :ref="'time-end-'+period.id">
                                </div>
                                <span class="invalid-feedback" role="alert"><strong>@{{ period.time.end.info }}</strong></span>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div v-if="form.periods.periods.length < 5" class="d-flex justify-content-center align-items-center m-4" @click="newPeriod">
                <div class="d-inline-flex align-items-center text-muted cursor-pointer">
                    <span class="d-inline-flex mr-2">
                        <v-svg class="svg" src="{{ asset('assets/image/svg/light/plus-circle.svg') }}" width="18" height="18"></v-svg>
                    </span>
                    <span>新增時間區段</span>
                </div>
            </div>

        </div>
        <span class="invalid-feedback" role="alert"><strong>@{{ form.periods.info }}</strong></span>
    </div>

    {{-- 建立訊息 --}}
    @include('components.forms.solution._4.basic.message.new-message')
    @include('components.forms.solution._4.basic.message.message-types')
    @include('components.forms.solution._4.basic.message.messages')

    <div class="row">
        {{-- 訊息設定 --}}
        <div class="form-group col-12">
            <label for="messageType">訊息設定<span class="form-required">*</span></label>

            <div v-if="form.messageType.messageType == 'pool'" class="alert alert-dismissible fade show alert-light text-justify font-size-0875 mb-3" role="alert">
                選擇「從訊息庫中複製」，可對複製的訊息進行更改，並不會影響原本訊息庫中的訊息
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div :class="[{'is-invalid' : form.messageType.invalid}, 'i-form-control d-flex btn-group-toggle']">
                <label :class="[{active : form.messageType.messageType == 'pool'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="messageType" value="pool" v-model="form.messageType.messageType" :checked="form.messageType.messageType == 'pool'">
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/light/box-open.svg') }}" width="21" height="21"></v-svg>
                    </span>
                    <span>從訊息庫中複製</span>
                </label>

                <label :class="[{active : form.messageType.messageType == 'template'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="messageType" value="template" v-model="form.messageType.messageType" :checked="form.messageType.messageType == 'template'">
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/regular/cubes.svg') }}" width="20" height="20"></v-svg>
                    </span>
                    <span>使用訊息模組</span>
                </label>

                <label :class="[{active : form.messageType.messageType == 'new'}, 'd-flex btn btn-white']">
                    <input type="radio" name="messageType" value="new" v-model="form.messageType.messageType" :checked="form.messageType.messageType == 'new'">
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/light/comment-alt-lines.svg') }}" width="20" height="18"></v-svg>
                    </span>
                    <span>新建訊息</span>
                </label>

            </div>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.messageType.info }}</strong></span>
        </div>
    </div>

    {{-- 從訊息庫中複製／使用訊息模組->選擇訊息 --}}
    <div v-if="form.messageType.messageType == 'pool' || form.messageType.messageType == 'template'" class="row">
        {{-- 選擇訊息／使用訊息模組 --}}
        <div class="form-group col-12 col-md-6">
            <label for="actionType">@{{ form.messageType.messageType == 'pool' ? '選擇訊息' : '選擇訊息模組' }}<span class="form-required">*</span><a v-if="form.messageType.messageType == 'template' && form.message.value" class="text-muted ml-2" :href="getMessageUrl(form.message.value.id)" target="_blank">（預覽訊息模組）</a></label>
            <vue-multiselect :class="[{'is-invalid' : form.message.invalid}, 'i-form-control hidden-arrow']" v-model="form.message.value" track-by="value" label="name" :placeholder="form.messageType.messageType == 'pool' ? '搜尋訊息名稱' : '搜尋訊息模組名稱'" open-direction="bottom" :options="message.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="false" :loading="message.isLoading" :internal-search="false" :options-limit="10" :limit-text="messageslimitText" :max-height="400" :show-no-results="false" :hide-selected="true" @search-change="getMessages">
                <span slot="noResult">沒有相符的搜尋結果。</span>
            </vue-multiselect>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.message.info }}</strong></span>
        </div>
    </div>

    <new-message v-if="form.messageType.messageType == 'new' || (form.messageType.messageType == 'pool' && form.message.value)" :isloading="newMessages.isLoading" :newmessagetype="newMessages.newMessageType" :messages="form.messages.messages" :hasquickreply="newMessages.hasQuickReply" @updateisloading="updateIsLoading" @updatemessagesdata="updateMessagesData" @updatehasquickreply="updateHasQuickReply"></new-message>

    <message-types v-if="form.messageType.messageType == 'new' || (form.messageType.messageType == 'pool' && form.message.value)" formtype="message" :hasquickreply="newMessages.hasQuickReply" @updatenewmessagetype="updateNewMessageType"></message-types>


    <hr class="mt-5 mb-5">

    {{ Form::button('新增歡迎訊息', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, "btn btn-block btn-primary"]', '@click'=>'submit']) }}

</div>
