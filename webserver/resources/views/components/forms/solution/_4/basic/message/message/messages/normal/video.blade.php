{{-- Video message --}}

@push('js')

<script type="text/x-template" id="--video-message">
    <div>
        <div class="d-flex message video-message">
            <div class="buttons mr-2">
                <div v-if="messagesCount > 1" class="text-muted text-danger-hover cursor-pointer" @click="remove">
                    <v-svg src="{{ asset('assets/image/svg/light/trash-alt.svg') }}" width="16" height="16"></v-svg>
                </div>
            </div>
            <div>
                <div class="type text-muted pl-2 mb-2">Video message</div>
                <div class="content d-flex align-items-start">

                    <div class="d-flex">
                        <div>
                            <div :class="[{'is-invalid' : form.video.invalid}, form.video.src == '' ? 'un-upload' : 'uploaded', 'i-form-control position-relative d-flex']">
                                <div :class="[{'i-dz-max-files-reached' : form.video.src != ''}, 'w-100 h-100 overflow-hidden border-radius-0375 idropzone dropzone-single']" ref="video">
                                    <div :id="'dz-preview-'+index" class="dz-preview dz-preview-single">
                                        <div class="dz-preview-cover d-flex">
                                            <video class="border-radius-05" :alt="form.video.alt" ref="videoPreview" controls>
                                                <source :src="form.video.src" type="video/mp4"/>
                                                Your browser does not support HTML5 video.
                                            </video>
                                        </div>
                                    </div>
                                    <div ref="hiddenInputContainer"></div>
                                    <div :class="[{'z-index--1' : form.video.src != ''}, 'dz-default dz-message']">
                                        <span class="mb-3">
                                            <v-svg src="{{ asset('assets/image/svg/light/film.svg') }}" width="25" height="25"></v-svg>
                                        </span>
                                        <span>Drop video here to upload</span>
                                    </div>
                                </div>
                            </div>
                            <div v-if="form.video.src != ''" class="change-video text-center text-muted cursor-pointer mt-3" @click="changeVideo">change video</div>
                            <span class="invalid-feedback" role="alert"><strong>@{{ form.video.info }}</strong></span>
                        </div>

                        <div :class="[form.fixedRatio.active ? 'text-primary' : 'text-muted', 'd-flex align-items-center ml-3 mr-3 cursor-pointer']" @click="toggleFixedRatio">
                            <span>...</span>
                            <span class="ml-2 mr-1">
                                <v-svg v-show="form.fixedRatio.active" src="{{ asset('assets/image/svg/light/link.svg') }}" width="18" height="18"></v-svg>
                                <v-svg v-show="!form.fixedRatio.active" src="{{ asset('assets/image/svg/light/unlink.svg') }}" width="18" height="18"></v-svg>
                            </span>
                            <span>...</span>
                        </div>
                    </div>

                    <div>
                        <div :class="[{'is-invalid' : form.image.invalid}, form.image.src == '' ? 'un-upload' : 'uploaded', 'i-form-control position-relative d-flex overflow-hidden']" :style="form.fixedRatio.active ? (form.video.ratio > 1 ? 'width:350px;height:'+350/form.video.ratio+'px;' : 'width:'+350*form.video.ratio+'px;height:350px;') : ''">
                            <div :class="[{'i-dz-max-files-reached' : form.image.src != ''}, 'w-100 h-100 overflow-hidden border-radius-0375 idropzone dropzone-single mb-3']" ref="image">
                                <div :id="'dz-preview-'+index" class="dz-preview dz-preview-single d-flex h-100">
                                    <div :class="[form.fixedRatio.active && form.image.ratio > form.video.ratio ? 'h-100' : 'w-100', 'dz-preview-cover']">
                                        <img :class="[{ 'd-none' : form.image.src == '' }, 'dz-preview-img']" :src="form.image.src" :alt="form.image.alt"/>
                                    </div>
                                </div>
                                <div class="dz-default dz-message">
                                    <span class="mb-3">
                                        <v-svg src="{{ asset('assets/image/svg/light/image.svg') }}" width="25" height="25"></v-svg>
                                    </span>
                                    <span>Drop preview image here to upload</span>
                                </div>
                            </div>
                        </div>
                        <span class="invalid-feedback" role="alert"><strong>@{{ form.image.info }}</strong></span>
                    </div>


                </div>
            </div>
        </div>
        <div class="droppable p-4" @dragover.prevent @dragenter.stop.prevent="isDropzoneActive() ? dragenter($event) : null" @dragleave.stop.prevent="isDropzoneActive() ? dragleave($event) : null" @drop.stop.prevent="isDropzoneActive() ? drop(index + 1) : null">
            <div></div>
        </div>
    </div>
</script>

<script>
Vue.component('video-message', {
    template: '#--video-message',
    props: ['index', 'messagesCount', 'newMessageType', 'message'],
    data(){
        return {
            counter: 0,
            form: this.message,
        }
    },
    methods: {
        remove(){
            this.$emit('removeMessage', this.index);
        },
        dragenter(event){
            this.counter ++;
            $(event.target).addClass('dragover');
        },
        dragleave(event){
            this.counter --;
            if(this.counter === 0) $(event.target).removeClass('dragover');
        },
        drop(index){
            this.counter = 0;
            this.$emit('newMessage', index);
        },
        isDropzoneActive(){
            if(this.newMessageType == 'quickreply-message' && (this.index == this.messagesCount - 1 && !this.hasQuickReply)){
                return true;
            }else if(this.newMessageType != 'quickreply-message' && this.messagesCount < 5){
                return true;
            }
            return false;
        },
        changeVideo(){
            this.$refs.hiddenInputContainer.getElementsByTagName('input')[0].click();
        },
        toggleFixedRatio(){
            this.form.fixedRatio.active =  !this.form.fixedRatio.active;
        }
    },
    mounted(){
        var vm = this;
        new Dropzone(this.$refs.video, {
            url: "#",
            autoProcessQueue: false,
            maxFiles: 1,
            maxFilesize: 10,
            acceptedFiles: ".mp4",
            thumbnailWidth: null,
            thumbnailHeight: null,
            hiddenInputContainer: vm.$refs.hiddenInputContainer,
            addedfile: function(file){
                vm.form.video.info = '';
                vm.form.invalid = false;

                var video = document.createElement('video');
                video.file = file;

                video.onloadedmetadata = function() {
                    if(this.duration > 600){
                        vm.form.video.info = '影片最長時間：10分鐘';
                        vm.form.video.invalid = true;
                        return
                    }

                    vm.form.video.src = this.src;
                    vm.form.video.alt = this.file.name;
                    vm.form.video.file = this.file;
                    vm.form.video.width = this.videoWidth;
                    vm.form.video.height =this.videoHeight;
                    vm.form.video.ratio = this.videoWidth/this.videoHeight;
                    vm.form.video.info = '';
                    vm.form.video.invalid = false;

                    vm.$refs.videoPreview.load();
                }
                video.src = URL.createObjectURL(video.file);
                return false;
            },
        });

        new Dropzone(this.$refs.image, {
            url: "#",
            autoProcessQueue: false,
            maxFiles: 1,
            acceptedFiles: ".jpeg,.jpg,.png,.JPEG,.JPG,.PNG",
            thumbnailWidth: null,
            thumbnailHeight: null,
            addedfile: function(file){
                vm.form.image.info = '';
                vm.form.invalid = false;

                var img = new Image();

                img.src = window.URL.createObjectURL(file);
                img.onload = function(){
                    var width = this.naturalWidth,
                    height = this.naturalHeight;

                    vm.form.image.src = window.URL.createObjectURL(file);
                    vm.form.image.alt = file.name;
                    vm.form.image.file = file;
                    vm.form.image.ratio = width/height;
                    vm.form.image.info = '';
                    vm.form.image.invalid = false;
                };
                return false;
            },
        });
    },
});
</script>

@endpush
