{{-- Message action --}}

@push('js')

<script type="text/x-template" id="--message-action">
    <div class="message-action">
        <div class="form-group">
            <label for="text">訊息內容<span class="form-required">*</span></label>
            <div :class="[{'is-invalid' : form.text.invalid}, 'i-form-control card mb-0 w-auto']">
                <div class="card-body text-justify p-3">
                    <div class="texarea-not-input position-relative">
                        <pre ref="inputPre"></pre>
                        <textarea class="not-input" placeholder="請輸入訊息內容" v-model="form.text.text"></textarea>
                    </div>
                </div>
            </div>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.text.info }}</strong></span>
        </div>
    </div>
</script>

<script>
Vue.component('message-action', {
    template: '#--message-action',
    props: ['id', 'action', 'columnIndex'],
    data: function(){
        return {
            actionData: null,
            form: this.action,
        };
    },
    updated(){
        this.$emit('updateActionData', this.form, this.columnIndex);
    },
    watch: {
        action(newValue){
            this.$set(this, 'form', newValue);
        },
        'form.text.text': function(newValue){
            this.$refs.inputPre.innerHTML = newValue.replaceAll(/\r\n|\r|\n/g, '<br>')+'<br>';
        },
    }
});
</script>

@endpush
