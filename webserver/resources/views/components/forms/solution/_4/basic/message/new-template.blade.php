@push('js')

{{-- 新建腳本 --}}

<script type="text/x-template" id="--new-template">
    <div id="new-template-layout" class="overflow-hidden pl-4">

        <div v-if="templateType" id="tree" class="overflow-auto">
            <ul class="tree-ul justify-content-start">
                <li class="tree-li">
                    <div class="node start">
                        <div class="mb-0">
                            <div class="message-labels">
                                <p class="badge badge-soft-dark">start</p>
                            </div>
                        </div>
                    </div>

                    <branch :nodes="form.template.template.nodes.nodes" :edittingNodesId="edittingNodesId" :currentNodeId="currentNodeId" :newMessageType="newmessagetype" :actionTypes="actionTypes" :quickReplyActionTypes="quickReplyActionTypes" :actions="actions" :defaultData="defaultData" @editNode="editNode" @closeEditNode="closeEditNode" @insertNewNode="insertNewNode" @removeNode="removeNode"></branch>
                </li>
            </ul>
        </div>

        <div v-else class="text-muted">請選擇模組類型</div>

    </div>
</script>

<script>
Vue.component('new-template', {
    template: '#--new-template',
    props: ['templateType', 'template', 'templateSettings', 'newmessagetype'],
    data(){
        return {
            edittingNodesId: [],

            currentNodeId: 0,

            form: {
                template: this.template,
            },

            actionTypes: [
                {
                    name: '回覆好友訊息',
                    value: 'message',
                    show: true,
                },
                {
                    name: '好友回覆關鍵字',
                    value: 'keyword',
                    show: true,
                },
                {
                    name: '直接開啟網址',
                    value: 'uri',
                    show: true,
                },
                {
                    name: '透過 Liff 開啟網址',
                    value: 'liff',
                    show: true,
                },
                {
                    name: '切換主選單',
                    value: 'richmenu',
                    show: true,
                },
            ],

            quickReplyActionTypes: [
                {
                    name: '回覆好友訊息',
                    value: 'message',
                    show: true,
                },
                {
                    name: '好友回覆關鍵字',
                    value: 'keyword',
                    show: true,
                },
                {
                    name: '切換主選單',
                    value: 'richmenu',
                    show: true,
                },
                {
                    name: '開啟相機',
                    value: 'camera',
                    show: false,
                },
                {
                    name: '開啟相簿',
                    value: 'cameraRoll',
                    show: false,
                },
            ],

            actions: {
                message: {
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    }
                },
                keyword: {
                    keyword: {
                        value: null,
                        info: '',
                        invalid: false,
                    },
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    }
                },
                uri: {
                    uri: {
                        uri: '',
                        info: '',
                        invalid: false,
                    }
                },
                liff: {
                    size: {
                        value: null,
                        info: '',
                        invalid: false,
                    },
                    uri: {
                        uri: '',
                        info: '',
                        invalid: false,
                    }
                },
                richmenu: {
                    richmenu: {
                        value: null,
                        info: '',
                        invalid: false,
                    }
                },
            },

            defaultData: {
                image: {
                    src: '',
                    alt: '',
                    file: null,
                    info: '',
                    invalid: false,
                },
                action: {
                    type: {
                        name: '回覆好友訊息',
                        value: 'message',
                        show: true,
                    },
                    action: {
                        label: {
                            label: '',
                            info: '',
                            invalid: false,
                        },
                        text: {
                            text: '',
                            info: '',
                            invalid: false,
                        }
                    },
                    selected: false,
                    invalid: false,
                },
                message: {
                    text: {
                        type: 'text',
                        message: {
                            text: {
                                text: '',
                                info: '',
                                invalid: false,
                            }
                        },
                        info: '',
                        invalid: false,
                    },
                    coupon: {
                        type: 'coupon',
                        message: {
                            type: {
                                type: 'imagemap',
                                info: '',
                                invalid: false,
                            },
                            altText: {
                                altText: '',
                                info: '',
                                invalid: false,
                            },
                            image: {
                                active: {
                                    active: true,
                                    info: '',
                                    invalid: false,
                                },
                                image: {
                                    src: '',
                                    alt: '',
                                    width: 0,
                                    height: 0,
                                    widthBase: 195,
                                    heightBase: 195,
                                    file: null,
                                    info: '',
                                    invalid: false,
                                }
                            },
                            position: {
                                x: {
                                    x: 0,
                                    info: '',
                                    invalid: false,
                                },
                                y: {
                                    y: 0,
                                    info: '',
                                    invalid: false,
                                },
                                w: {
                                    w: 1,
                                    info: '',
                                    invalid: false,
                                },
                            }
                        }
                    }
                },
                template: {
                    coupon: [
                        {
                            name: '領取時間之外',
                            value: 'invalid-time',
                        },
                        {
                            name: '達領取數量上限',
                            value: 'reach-limit',
                        },
                        {
                            name: '已領取完畢',
                            value: 'finish-giving',
                        },
                        {
                            name: '領取成功',
                            value: 'success',
                        }
                    ],
                },
            },
        }
    },
    methods: {
        getCurrentNodeId(){
            return this.form.template.template.nodes.nodes.reduce((max, node) => Math.max(max, node.id), 0);
        },
        editNode(id){
            if(this.edittingNodesId.length == 0){
                this.edittingNodesId.push(id);
            }else{
                var vm = this;
                newEdittingNodesId = [id];
                // 原有的node只留下在新node的父或子node
                newNode = this.form.template.template.nodes.nodes.find(node => node.id == id);

                // 檢查父node
                filterdNodes = this.form.template.template.nodes.nodes.filter(function(node){
                    return vm.edittingNodesId.includes(node.id) && node.start < newNode.start && node.end > newNode.end;
                });

                if(filterdNodes.length != 0){
                    parentNode = this.form.template.template.nodes.nodes.reduce(function(parentNode, node){

                        if(node.start < newNode.start && node.end > newNode.end){
                            return (!parentNode || node.start > parentNode.start) ? node : parentNode;
                        }
                        return parentNode;

                    }, null);
                    parentNode = filterdNodes.find(node => node.id == parentNode.id);
                    if(parentNode) newEdittingNodesId.push(parentNode.id);
                }

                this.edittingNodesId = newEdittingNodesId;
            }
        },
        filterThisLayerNodes(start, end){
            thisLayerNodes = [];

            do{
                node = this.form.template.template.nodes.nodes.find(node => node.start == start + 1);
                if(node){
                    thisLayerNodes.push(node);
                    start = node.end;
                }else{
                    break;
                }
            }while(end > node.end)

            return thisLayerNodes;
        },
        closeEditNode(id){
            this.edittingNodesId.splice(this.edittingNodesId.indexOf(id), 1);
        },
        insertNewNode(parentEnd){
            this.currentNodeId ++;

            this.form.template.template.nodes.nodes.forEach(function(node){
                if(node.end >= parentEnd){
                    if(node.start > parentEnd) node.start += 2;
                    node.end += 2;
                }
            });

            this.form.template.template.nodes.nodes.push({
                id: this.currentNodeId,
                start: parentEnd,
                end: parentEnd + 1,
                messages: {
                    messages: [
                        {
                            id: this.currentNodeId+'-1',
                            type: 'text',
                            message: {
                                text: {
                                    text: '',
                                    info: '',
                                    invalid: false,
                                }
                            },
                            info: '',
                            invalid: false,
                        },
                    ],
                }
            });
        },
        removeNode(id){
            var vm = this;
            index = this.form.template.template.nodes.nodes.findIndex(node => node.id == id);
            removedNode = this.form.template.template.nodes.nodes[index];
            minusNum = removedNode.end - removedNode.start + 1;
            removedNodes = [];

            this.form.template.template.nodes.nodes.forEach(function(node, index){
                if(node.start >= removedNode.start && node.end <= removedNode.end){
                    removedNodes.push(index);
                }else if(node.end >= removedNode.end){
                    if(node.start > removedNode.start) node.start -= minusNum;
                    node.end -= minusNum;
                }
            });

            removedNodes.sort((a, b) => b - a).forEach(function(index){
                vm.form.template.template.nodes.nodes.splice(index, 1);
            })
        },
        newCouponTemplate(){
            var vm = this;
            this.defaultData.template.coupon.forEach(function(condition, index){
                vm.currentNodeId ++;
                vm.form.template.template.nodes.nodes.push({
                    id: vm.currentNodeId,
                    start: (index+1)*2,
                    end: (index+1)*2+1,
                    condition: condition,
                    disabled: false,
                    messages: {
                        messages: [
                            $.extend(true, {
                                id: vm.currentNodeId+'-1',
                                deleteable: condition.value == 'success',
                            }, vm.defaultData.message[condition.value == 'success' ? 'coupon' : 'text']),
                        ],
                    }
                });
            });
        }
        // }
    },
    created(){
        this.currentNodeId = this.getCurrentNodeId();
        // this.form.template.template.nodes.nodes = [];
        if(!this.form.template.template.nodes.nodes && this.templateType){
            this['new'+this.templateType.value.capitalize()+'Template']();
        }
    },
    watch: {
        templateType(newValue){
            this.form.template.template.nodes.nodes = [];
            if(newValue) this['new'+newValue.value.capitalize()+'Template']();
        },
        templateSettings: {
            handler(newValue){
                this.edittingNodesId = [];
            },
            deep: true,
        }
    }
});
</script>

@endpush
