@push('js')

{{-- 新建訊息 --}}

<script type="text/x-template" id="--new-message">
    <div id="new-message-layout" class="position-relative overflow-hidden pl-4 pt-0">

        <div :class="[isloading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute top left justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-bl-05 border-radius-br-05']">
            <div class="spinner-grow text-secondary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>

        <div>
            <div class="droppable p-4" @dragover.prevent @dragenter.stop.prevent="isDropzoneActive() ? dragenter($event) : null" @dragleave.stop.prevent="isDropzoneActive() ? dragleave($event) : null" @drop.stop.prevent="isDropzoneActive() ? drop(0) : null">
                <div></div>
            </div>
        </div>

        <component v-for="(message, index) in form.messages" :key="message.id" :is="message.type+'-message'" :index="index" :messagesCount="getMessagesCount()" :newMessageType="newmessagetype" :message="message.message" :actions="['buttons', 'imagemap', 'confirm', 'carousel', 'imagecarousel', 'quickreply'].includes(message.type) ? actions : null" :defaultAction="['buttons', 'carousel', 'imagecarousel', 'quickreply'].includes(message.type) ? defaultData.action : null" :actionTypes="['buttons', 'imagemap', 'confirm', 'carousel', 'imagecarousel'].includes(message.type) ? actionTypes : null" :quickReplyActionTypes="message.type == 'quickreply' ? quickReplyActionTypes : null" @removeMessage="removeMessage" @newMessage="newMessage"></component>

    </div>
</script>

<script>
Vue.component('new-message', {
    template: '#--new-message',
    props: ['isloading', 'newmessagetype', 'messages', 'hasquickreply'],
    data(){
        return {
            counter: 0,
            currentId: this.getCurrentId(),

            form: {
                messages: this.messages,
            },

            actionTypes: [
                {
                    name: '回覆好友訊息',
                    value: 'message',
                    show: true,
                },
                {
                    name: '好友回覆關鍵字',
                    value: 'keyword',
                    show: true,
                },
                {
                    name: '直接開啟網址',
                    value: 'uri',
                    show: true,
                },
                {
                    name: '透過 Liff 開啟網址',
                    value: 'liff',
                    show: true,
                },
                {
                    name: '開啟訊息腳本',
                    value: 'script',
                    show: true,
                },
                {
                    name: '切換主選單',
                    value: 'richmenu',
                    show: true,
                },
            ],

            quickReplyActionTypes: [
                {
                    name: '回覆好友訊息',
                    value: 'message',
                    show: true,
                },
                {
                    name: '好友回覆關鍵字',
                    value: 'keyword',
                    show: true,
                },
                {
                    name: '切換主選單',
                    value: 'richmenu',
                    show: true,
                },
                {
                    name: '開啟相機',
                    value: 'camera',
                    show: false,
                },
                {
                    name: '開啟相簿',
                    value: 'cameraRoll',
                    show: false,
                },
            ],

            actions: {
                message: {
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    }
                },
                keyword: {
                    keyword: {
                        value: null,
                        info: '',
                        invalid: false,
                    },
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    }
                },
                uri: {
                    uri: {
                        uri: '',
                        info: '',
                        invalid: false,
                    }
                },
                liff: {
                    size: {
                        value: null,
                        info: '',
                        invalid: false,
                    },
                    uri: {
                        uri: '',
                        info: '',
                        invalid: false,
                    }
                },
                script: {
                    script: {
                        value: null,
                        info: '',
                        invalid: false,
                    }
                },
                richmenu: {
                    richmenu: {
                        value: null,
                        info: '',
                        invalid: false,
                    }
                },
            },

            defaultData: {
                image: {
                    src: '',
                    alt: '',
                    file: null,
                    info: '',
                    invalid: false,
                },
                action: {
                    type: {
                        name: '回覆好友訊息',
                        value: 'message',
                        show: true,
                    },
                    action: {
                        label: {
                            label: '',
                            info: '',
                            invalid: false,
                        },
                        text: {
                            text: '',
                            info: '',
                            invalid: false,
                        }
                    },
                    selected: false,
                    invalid: false,
                },
                // actionType: {
                //     value: null,
                //     info: '',
                //     invalid: false,
                // }
            }
        }
    },
    methods: {
        dragenter(event){
            this.counter ++;
            $(event.target).addClass('dragover');
        },
        dragleave(event){
            this.counter --;
            if(this.counter === 0) $(event.target).removeClass('dragover');
        },
        drop(index){
            this.counter = 0;
            this.newMessage(index);
        },
        getCurrentId(){
            return this.messages.reduce((max, message) => Math.max(max, message.id), 1);
        },
        getMessagesCount(){
            return this.form.messages.filter(message => message.type != 'quickreply').length;
        },
        isDropzoneActive(){
            if(this.newmessagetype == 'quickreply-message' && (this.index == this.getMessagesCount() - 1 && !this.hasquickreply)){
                return true;
            }else if(this.newmessagetype != 'quickreply-message' && this.getMessagesCount() < 5){
                return true;
            }
            return false;
        },
        // updateMessageData(index, data){
        //     this.$set(this.form.messages[index], 'message', data);
        //     this.$emit('update-messages-data', this.form.messages);
        // },
        removeMessage(index){
            if(this.form.messages[index].type == 'quickreply'){
                this.$emit('updatehasquickreply', false);
                this.form.messages.splice(index, 1);

            }else if(this.getMessagesCount() > 1){
                this.form.messages.splice(index, 1);
            }
            // this.$emit('update-messages-data', this.form.messages);
        },
        newMessage(index){
            if(this.newmessagetype){
                if(this.getMessagesCount() < 5 || this.newmessagetype == 'quickreply-message'){
                    messageType = this.newmessagetype.match(/(.+)-message/);
                    eval('this.new'+messageType[1].capitalize()+'Message')(index);
                    // this.$emit('update-messages-data', this.form.messages);
                }
            }
        },
        newTextMessage(index){
            this.currentId ++;
            this.form.messages.splice(index, 0, {
                id: this.currentId,
                type: 'text',
                message: {
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    }
                }
            });
        },
        newImageMessage(index){
            this.currentId ++;
            this.form.messages.splice(index, 0, {
                id: this.currentId,
                type: 'image',
                message: {
                    image: Object.assign({}, this.defaultData.image),
                }
            });
        },
        newVideoMessage(index){
            this.currentId ++;
            this.form.messages.splice(index, 0, {
                id: this.currentId,
                type: 'video',
                message: {
                    video: {
                        src: '',
                        alt: '',
                        file: null,
                        width: 0,
                        height: 0,
                        ratio: 350/182,
                        info: '',
                        invalid: false,
                    },
                    image: Object.assign({ratio: 350/182}, this.defaultData.image),
                    fixedRatio: {
                        active: true,
                        info: '',
                        invalid: false,
                    },
                }
            });
        },
        newImagemapMessage(index){
            area = [];
            for(var i = 0; i < 2; i ++){
                area[i] = [];
                for(var j = 0; j < 2; j ++){
                    area[i].push({
                        id: 2*i+j+1,
                        type: {
                            name: '好友回覆關鍵字',
                            value: 'keyword',
                        },
                        action: {
                            keyword: {
                                value: null,
                                info: '',
                                invalid: false,
                            },
                            text: {
                                text: '',
                                info: '',
                                invalid: false,
                            }
                        },
                        selected: false,
                        invalid: false,
                    });
                }
            };

            this.currentId ++;
            this.form.messages.splice(index, 0, {
                id: this.currentId,
                type: 'imagemap',
                message: {
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    image: Object.assign({}, this.defaultData.image),
                    show: {
                        active: true,
                        info: '',
                        invalid: false,
                    },
                    area: {
                        area: area,
                        info: '',
                        invalid: false,
                    },
                }
            });
        },
        newButtonsMessage(index){
            this.currentId ++;
            this.form.messages.splice(index, 0, {
                id: this.currentId,
                type: 'buttons',
                message: {
                    title: {
                        title: '',
                        info: '',
                        invalid: false,
                    },
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    },
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    image: $.extend(true, {
                        active: {
                            active: true,
                            info: '',
                            invalid: false,
                        },
                        ratio: {
                            ratio: 'rectangle',
                            info: '',
                            invalid: false,
                        }
                    }, this.defaultData.image),
                    actions: [
                        $.extend(true, {id: 1}, this.defaultData.action),
                    ],
                }
            });
        },
        newConfirmMessage(index){
            this.currentId ++;
            this.form.messages.splice(index, 0, {
                id: this.currentId,
                type: 'confirm',
                message: {
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    },
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    actions: [
                        $.extend(true, {id: 1}, this.defaultData.action),
                        $.extend(true, {id: 2}, this.defaultData.action),
                    ],
                },
            });
        },
        newCarouselMessage(index){
            this.currentId ++;
            this.form.messages.splice(index, 0, {
                id: this.currentId,
                type: 'carousel',
                message: {
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    columns: {
                        info: '',
                        invalid: false,
                        columns:[
                            {
                                id: 1,
                                title: {
                                    title: '',
                                    info: '',
                                    invalid: false,
                                },
                                text: {
                                    text: '',
                                    info: '',
                                    invalid: false,
                                },
                                image: Object.assign({}, this.defaultData.image),
                                actions: [
                                    $.extend(true, {id: 1}, this.defaultData.action),
                                ],
                            }
                        ],
                    },
                    title: {
                        active: {
                            active: true,
                            info: '',
                            invalid: false,
                        },
                    },
                    image: {
                        active: {
                            active: true,
                            info: '',
                            invalid: false,
                        },
                        ratio: {
                            ratio: 'rectangle',
                            info: '',
                            invalid: false,
                        }
                    },
                },
            });
        },
        newImagecarouselMessage(index){
            this.currentId ++;
            this.form.messages.splice(index, 0, {
                id: this.currentId,
                type: 'imagecarousel',
                message: {
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    columns: {
                        info: '',
                        invalid: false,
                        columns:[
                            {
                                id: 1,
                                image: Object.assign({}, this.defaultData.image),
                                actions: [
                                    $.extend(true, {id: 1}, this.defaultData.action),
                                ],
                            }
                        ],
                    },
                },
            });
        },
        newQuickreplyMessage(index){
            this.$emit('updatehasquickreply', true);

            this.currentId ++;
            this.form.messages.splice(index, 0, {
                id: this.currentId,
                type: 'quickreply',
                message: {
                    items: {
                        info: '',
                        invalid: false,
                        items:[
                            {
                                id: 1,
                                image: Object.assign({}, this.defaultData.image),
                                actions: [
                                    $.extend(true, {id: 1}, this.defaultData.action),
                                ],
                            }
                        ],
                    },
                },
            });
        },
        // newCouponMessage(index){
        //     this.updateOnlyPush(1);
        //     this.currentId ++;
        //     this.form.messages.splice(index, 0, {
        //         id: this.currentId,
        //         type: 'coupon',
        //         message: {
        //             type: {
        //                 type: 'imagemap',
        //                 info: '',
        //                 invalid: false,
        //             },
        //             altText: {
        //                 altText: '',
        //                 info: '',
        //                 invalid: false,
        //             },
        //             table: {
        //                 value: null,
        //                 info: '',
        //                 invalid: false,
        //             },
        //             random: {
        //                 random: true,
        //                 info: '',
        //                 invalid: false,
        //             },
        //             limit: {
        //                 active: {
        //                     active: true,
        //                     info: '',
        //                     invalid: false,
        //                 },
        //                 count: {
        //                     count: 1,
        //                     info: '',
        //                     invalid: false,
        //                 }
        //             },
        //             finishGiving: Object.assign({
        //                 altText: {
        //                     altText: '',
        //                     info: '',
        //                     invalid: false,
        //                 }
        //             }, this.defaultData.image),
        //             reachLimit: Object.assign({
        //                 altText: {
        //                     altText: '',
        //                     info: '',
        //                     invalid: false,
        //                 },
        //             }, this.defaultData.image),
        //         }
        //     });
        // },
    },
    watch: {
        messages(newValue){
            if(this.isloading){
                var vm = this;
                this.form.messages = [];

                newValue.forEach(function(message){
                    vm.currentId ++;
                    vm.form.messages.push($.extend(true, message, {id: vm.currentId}));

                    if(message.type == 'quickreply') vm.$emit('updatehasquickreply', true);
                });
                this.$emit('updateisloading', false);
                this.$emit('updatemessagesdata', this.form.messages);
            }
        }
    }
});
</script>

@endpush
