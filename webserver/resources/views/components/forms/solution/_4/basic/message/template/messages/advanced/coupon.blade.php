{{-- Coupon message --}}

@push('js')

<script type="text/x-template" id="--template-coupon-message">
    <div>
        <div class="d-flex message coupon-message">
            <div class="buttons mr-1">
                <div v-if="messagesCount > 1" :class="[{'visibility-hidden' : !form.deleteable}, 'text-muted text-danger-hover cursor-pointer']" @click="form.deleteable ? remove : null">
                    <v-svg src="{{ asset('assets/image/svg/light/trash-alt.svg') }}" width="12" height="12"></v-svg>
                </div>
            </div>
            <div class="w-100">
                <div class="type text-muted pl-1 mb-1">Coupon message</div>
                <div class="content d-flex flex-nowrap row ml-0 pt-2 mt-n2 overflow-auto">
                    <div v-show="form.image.active.active" class="i-d-flex flex-nowrap row col-auto">

                        <div class="pr-4">
                            <div :class="[form.type.type+'-type', {'is-invalid' : form.image.image.invalid}, form.image.image.src == '' ? 'un-upload' : 'uploaded', 'i-form-control']">
                                <div class="image-layout position-relative">

                                    {{-- 圖片上傳 --}}
                                    <div :class="[{'i-dz-max-files-reached' : form.image.image.src != ''}, 'position-absolute top w-100 h-100 overflow-hidden border-radius-0375 idropzone dropzone-single']" ref="image">
                                        <div :id="'dz-preview-'+index" class="dz-preview dz-preview-single">
                                            <div class="dz-preview-cover">
                                                <img :class="[{ 'd-none' : form.image.image.src == '' }, 'dz-preview-img']" :src="form.image.image.src" :alt="form.image.image.alt"/>
                                            </div>
                                        </div>
                                        <div ref="hiddenInputContainer"></div>
                                        <div :class="[{'z-index--1' : form.image.image.src != ''}, 'dz-default dz-message']">
                                            <span class="mb-3">
                                                <v-svg src="{{ asset('assets/image/svg/light/image.svg') }}" width="20" height="20"></v-svg>
                                            </span>
                                            <span>Drop image here to upload</span>
                                        </div>
                                    </div>

                                    {{-- QRcode 位置 --}}
                                    <div v-if="form.image.image.src != ''" class="position-absolute top w-100 h-100 overflow-hidden cursor-pointer">
                                        <img class="position-absolute" :style="'left:'+((form.position.x.x/form.image.image.width)*form.image.image.widthBase)+'px; top:'+((form.position.y.y/form.image.image.height)*form.image.image.heightBase)+'px; width:'+((form.position.w.w/form.image.image.width)*form.image.image.widthBase)+'px; height:'+((form.position.w.w/form.image.image.height)*form.image.image.heightBase)+'px;'" src="{{ asset('assets/image/QRcode-sample.png') }}">
                                    </div>

                                </div>
                                <div v-if="form.image.image.src != ''" class="change-image text-center text-muted cursor-pointer mt-3" @click="changeImage">change image</div>
                            </div>
                            <span class="invalid-feedback" role="alert"><strong>@{{ form.image.image.info }}</strong></span>
                        </div>

                    </div>

                    <div :class="[form.image.active.active ? 'pl-0' : 'pl-1', 'col-300 m-0 pb-3 pr-0']">

                        <div class="form-group mb-3">
                            <label>訊息類型<span class="form-required">*</span></label>
                            <div :class="[{'is-invalid' : form.type.invalid}, 'type i-form-control d-flex btn-group-toggle']">
                                <label :class="[{active : form.type.type == 'image'}, 'd-flex btn btn-white pt-2 pb-2 pl-3 pr-3 mr-3 font-size-0750']">
                                    <input type="radio" name="type" value="image" v-model="form.type.type" :checked="form.type.type == 'image'">
                                    <span>Image</span>
                                </label>

                                <label :class="[{active : form.type.type == 'imagemap'}, 'type d-flex btn btn-white pt-2 pb-2 pl-3 pr-3 font-size-0750']">
                                    <input type="radio" name="type" value="imagemap" v-model="form.type.type" :checked="form.type.type == 'imagemap'">
                                    <span>Imagemap</span>
                                </label>
                            </div>
                            <span class="invalid-feedback" role="alert"><strong>@{{ form.type.info }}</strong></span>
                        </div>

                        {{-- 替代文字 --}}
                        <div v-if="form.type.type == 'imagemap'" class="form-group mb-3">
                            <label>替代文字<span class="form-required">*</span><span>（訊息通知中顯示）</span></label>
                            <input type="text" :class="[{'is-invalid' : form.altText.invalid}, 'form-control form-control-sm']" v-model="form.altText.altText">
                            <span class="invalid-feedback" role="alert"><strong>@{{ form.altText.info }}</strong></span>
                        </div>

                        {{-- QRcode 圖片合成 --}}
                        <div class="form-group mb-3">
                            <div class="custom-control custom-switch custom-switch-sm pt-2 pb-2">
                                <input type="checkbox" class="custom-control-input" :id="'qrcode-'+node.id+'-'+index" v-model="form.image.active.active">
                                <label :class="[{ 'text-muted' : !form.image.active.active }, 'custom-control-label']" :for="'qrcode-'+node.id+'-'+index">@{{ form.image.active.active ? '上傳QRcode背景圖片' : '圖片僅QRcode' }}</label>
                            </div>
                            <span class="invalid-feedback" role="alert"><strong>@{{ form.image.active.info }}</strong></span>
                        </div>

                        <div v-if="form.image.active.active && form.image.image.src != ''" class="d-flex form-group mb-0">
                            <div class="mr-3">
                                <div class="d-flex align-items-center">
                                    <label class="mb-0 mr-2">x :</label>
                                    <input type="number" :class="[{'is-invalid' : form.position.x.invalid}, 'position-input form-control form-control-sm text-center appearance-none']" v-model.number="form.position.x.x">
                                </div>
                                <span class="invalid-feedback" role="alert"><strong>@{{ form.position.x.info }}</strong></span>
                            </div>
                            <div class="mr-3">
                                <div class="d-flex align-items-center">
                                    <label class="mb-0 mr-2">y :</label>
                                    <input type="number" :class="[{'is-invalid' : form.position.y.invalid}, 'position-input form-control form-control-sm text-center appearance-none']" v-model.number="form.position.y.y">
                                </div>
                                <span class="invalid-feedback" role="alert"><strong>@{{ form.position.y.info }}</strong></span>
                            </div>
                            <div class="mr-3">
                                <div class="d-flex align-items-center">
                                    <label class="mb-0 mr-2">w :</label>
                                    <input type="number" :class="[{'is-invalid' : form.position.w.invalid}, 'position-input form-control form-control-sm text-center appearance-none']" v-model.number="form.position.w.w">
                                </div>
                                <span class="invalid-feedback" role="alert"><strong>@{{ form.position.w.info }}</strong></span>
                            </div>

                            <div class="d-flex align-items-center text-muted cursor-pointer mr-3" @click="alighCenter('v')">
                                <v-svg src="{{ asset('assets/image/svg/others/vertical_align_center.svg') }}" width="18" height="18"></v-svg>
                            </div>

                            <div class="d-flex align-items-center text-muted cursor-pointer" @click="alighCenter('h')">
                                <v-svg src="{{ asset('assets/image/svg/others/vertical_align_center.svg') }}" width="18" height="18" style=" transform:rotate(90deg);"></v-svg>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="droppable" @dragover.prevent @dragenter.stop.prevent="isDropzoneActive() ? dragenter($event) : null" @dragleave.stop.prevent="isDropzoneActive() ? dragleave($event) : null" @drop.stop.prevent="isDropzoneActive() ? drop(index + 1) : null">
            <div></div>
        </div>
    </div>
</script>

<script>
Vue.component('template-coupon-message', {
    template: '#--template-coupon-message',
    props: ['index', 'node', 'messagesCount', 'message', 'newMessageType', 'hasQuickReply'],
    data(){
        return {
            counter: 0,
            form: this.message,
        }
    },
    methods: {
        remove(){
            this.$emit('removeMessage', this.index);
        },
        dragenter(event){
            this.counter ++;
            $(event.target).addClass('dragover');
        },
        dragleave(event){
            this.counter --;
            if(this.counter === 0) $(event.target).removeClass('dragover');
        },
        drop(index){
            this.counter = 0;
            this.$emit('newMessage', index);
        },
        isDropzoneActive(){
            if(this.newMessageType == 'quickreply-message' && (this.index == this.messagesCount - 1 && !this.hasQuickReply)){
                return true;
            }else if(this.newMessageType != 'quickreply-message' && this.messagesCount < 5){
                return true;
            }
            return false;
        },
        changeImage(){
            this.$refs.hiddenInputContainer.getElementsByTagName('input')[0].click();
        },
        alighCenter(dir){
            if(dir == 'v'){
                this.form.position.y.y = (this.form.image.image.height - this.form.position.w.w) / 2;
            }else{
                this.form.position.x.x = (this.form.image.image.width - this.form.position.w.w) / 2;
            }
        }
    },
    mounted(){
        var vm = this;
        this.dropzone = new Dropzone(this.$refs.image, {
            url: "#",
            autoProcessQueue: false,
            maxFiles: 1,
            acceptedFiles: ".jpeg,.jpg,.png,.JPEG,.JPG,.PNG",
            thumbnailWidth: null,
            thumbnailHeight: null,
            hiddenInputContainer: vm.$refs.hiddenInputContainer,
            addedfile: function(file){
                vm.form.image.image.info = '';
                vm.form.image.image.invalid = false;

                var img = new Image();

                img.src = window.URL.createObjectURL(file);
                img.onload = function(){
                    var width = this.naturalWidth,
                    height = this.naturalHeight;

                    if(vm.form.type.type == 'image' && (width > 4096 || height > 4096)){
                        vm.form.image.image.info = '圖片最大尺寸：4096 x 4096';
                        vm.form.image.image.invalid = true;
                        return false;
                    }else if(vm.form.type.type == 'imagemap' && (width != 1040 || height != 1040)){
                        vm.form.image.image.info = '請符合圖片尺寸：1040 x 1040';
                        vm.form.image.image.invalid = true;
                        return false;
                    }

                    vm.form.image.image.src = window.URL.createObjectURL(file);
                    vm.form.image.image.alt = file.name;
                    vm.form.image.image.width = width;
                    vm.form.image.image.height = height;
                    vm.form.image.image.file = file;
                    vm.form.image.image.info = '';
                    vm.form.image.image.invalid = false;

                    vm.form.image.image.widthBase = 195;
                    vm.form.image.image.heightBase = 195;

                    if(vm.form.type.type == 'image'){
                        if(width > height){
                            vm.form.image.image.heightBase = 195*(height/width);
                        }else{
                            vm.form.image.image.widthBase = 195*(width/height);
                        }
                    }
                    vm.form.position.x.x = 0;
                    vm.form.position.y.y = 0;
                    vm.form.position.w.w = width / 2;
                };
                return false;
            },
        });
    },
    watch: {
        'form.type.type': function(newValue){
            this.form.image.image.src = '';
            this.form.image.image.alt = '';
            this.form.image.image.file = null;
            this.form.image.image.info = '';
            this.form.image.image.invalid = false;

            if(newValue == 'image') this.form.altText.altText = '';
        },
        'form.position.x.x': function(newValue, oldValue){
            if(newValue < 0){
                this.form.position.x.x = 0;
            }else if(isNaN(newValue) || newValue > this.form.image.image.width - 1 || (newValue + this.form.position.w.w > this.form.image.image.width)){
                this.form.position.x.x = oldValue;
            }
        },
        'form.position.y.y': function(newValue, oldValue){
            if(newValue < 0){
                this.form.position.y.y = 0;
            }else if(isNaN(newValue) || newValue > this.form.image.image.height - 1 || (newValue + this.form.position.w.w > this.form.image.image.height)){
                this.form.position.y.y = oldValue;
            }
        },
        'form.position.w.w': function(newValue, oldValue){
            if(newValue < 0){
                this.form.position.w.w = 1;
            }else if(isNaN(newValue) || newValue > this.form.image.image.width || newValue > this.form.image.image.height || (newValue + this.form.position.x.x > this.form.image.image.width) || (newValue + this.form.position.y.y > this.form.image.image.height)){
                this.form.position.w.w = oldValue;
            }
        },
    }
});
</script>

@endpush
