{{-- Imagemap message --}}

@push('js')

<script type="text/x-template" id="--template-imagemap-message">
    <div>
        <div class="d-flex message imagemap-message">
            <div class="buttons mr-2">
                <div v-if="messagesCount > 1" class="text-muted text-danger-hover cursor-pointer" @click="remove">
                    <v-svg src="{{ asset('assets/image/svg/light/trash-alt.svg') }}" width="12" height="12"></v-svg>
                </div>
            </div>
            <div class="w-100">
                <div class="type text-muted pl-1 mb-1">Imagemap message</div>
                <div class="content d-flex overflow-auto">
                    <div class="d-flex flex-nowrap">
                        <div class="pr-4">

                            <div :class="[{'is-invalid' : form.image.invalid}, 'i-form-control']">
                                <div class="image-layout position-relative">

                                    {{-- 圖片上傳 --}}
                                    <div :class="[{'i-dz-max-files-reached' : form.image.src != ''}, 'position-absolute top w-100 h-100 overflow-hidden border-radius-0375 idropzone dropzone-single']" ref="image">
                                        <div :id="'dz-preview-'+index" class="dz-preview dz-preview-single">
                                            <div class="dz-preview-cover">
                                                <img :class="[{ 'd-none' : form.image.src == '' }, 'dz-preview-img']" :src="form.image.src" :alt="form.image.alt"/>
                                            </div>
                                        </div>
                                        <div ref="hiddenInputContainer"></div>
                                        <div :class="[{'z-index--1' : form.image.src != ''}, 'dz-default dz-message']">
                                            <span class="mb-3">
                                                <v-svg src="{{ asset('assets/image/svg/light/image.svg') }}" width="20" height="20"></v-svg>
                                            </span>
                                            <span>Drop image here to upload</span>
                                        </div>
                                    </div>

                                    {{-- 隔線 --}}
                                    <div v-show="form.show.active" class="position-absolute top w-100 h-100 overflow-hidden cursor-pointer">
                                        <div id="grid" class="grid">
                                            <grid-tr v-for="(tds, index) in form.area.area" :tds="tds" :key="index" @removeArea="removeArea" @newArea="newArea" @selectArea="selectArea"></grid-tr>
                                        </div>
                                    </div>

                                </div>
                                <div v-if="form.image.src != ''" class="change-image text-center text-muted cursor-pointer mt-3" @click="changeImage">change image</div>
                            </div>
                            <span class="invalid-feedback" role="alert"><strong>@{{ form.image.info }}</strong></span>
                        </div>

                        <div v-show="actionType.currentAreaId" class="col-300 min-height-200 m-0 pl-0 pb-5 pr-4">
                            {{-- 按鈕行為 --}}
                            <div class="position-relative nopadding">
                                <button type="button" class="close-edit-block position-absolute top right d-flex align-items-center btn btn-outline-danger btn-sm" @click="closeEditBlock">
                                    <span class="d-flex align-items-center">
                                        <v-svg src="{{ asset('assets/image/svg/light/times.svg') }}" width="14" height="14"></v-svg>
                                    </span>
                                    <span>收合視窗</span>
                                </button>
                                <div class="form-group mb-3">
                                    <label for="actionType">區域行為<span class="form-required">*</span><span>@{{ !actionType.currentAreaId ? '（請先選擇編輯區域）' : '（正在編輯區域 '+actionType.currentAreaId+'）' }}</span></label>
                                    <vue-multiselect :class="[{'is-invalid' : actionType.invalid}, 'hidden-arrow multiselect-sm']" v-model="actionType.value" track-by="value" label="name" placeholder="請選擇行為" openDirection="bottom" :options="actionType.options" :searchable="false" :show-labels="false" :allow-empty="false" :disabled="!actionType.currentAreaId" @input="changeActionType"></vue-multiselect>
                                    <span class="invalid-feedback" role="alert"><strong>@{{ actionType.info }}</strong></span>
                                </div>

                                <component v-if="actionType.currentAreaId && actionType.value && actionType.value.value != 'next'" :is="'template-'+actionType.value.value+'-action'" :id="actionType.currentAreaId" :action="actionData" @updateActionData="updateActionData"></component>
                            </div>
                        </div>
                    </div>

                    <div class="col-300 m-0 pb-3 pl-0 pr-0">

                        {{-- 替代文字 --}}
                        <div class="form-group mb-3">
                            <label for="altText">替代文字<span class="form-required">*</span><span>（訊息通知中顯示）</span></label>
                            <input id="altText" type="text" :class="[{'is-invalid' : form.altText.invalid}, 'form-control form-control-sm']" v-model="form.altText.altText">
                            <span class="invalid-feedback" role="alert"><strong>@{{ form.altText.info }}</strong></span>
                        </div>

                        <hr class="mt-3 mb-3">

                        {{-- 隱藏點擊區域 --}}
                        <div class="form-group">
                            <div class="custom-control custom-switch custom-switch-sm pt-2">
                                <input type="checkbox" class="custom-control-input" :id="'show-'+index" v-model="form.show.active">
                                <label :class="[{ 'text-muted' : !form.show.active }, 'custom-control-label']" :for="'show-'+index">@{{ form.show.active ? '開啟點擊區域' : '隱藏點擊區域' }}</label>
                            </div>

                            <span class="invalid-feedback" role="alert"><strong>@{{ form.show.info }}</strong></span>
                        </div>

                        {{-- 區域動作 --}}
                        <div v-show="form.show.active" class="form-group">
                            <label>區域分割<span class="form-required">*</span><span>@{{ '（區域數：'+currentAreaCount+'，上限 50）' }}</span></label>

                            <div class="d-flex mb-4 mt-2">
                                <div v-for="i in [2,3,5]" :id="'new-row-'+i" class="new-grid grid mr-3 bg-transparent" draggable="true" @dragstart.stop="dragstart($event)" @dragend.stop="dragend($event)">
                                    <div v-for="j in i" class="tr">
                                        <div class="td"></div>
                                    </div>
                                </div>

                                <div v-for="i in [2,3,5]" :id="'new-column-'+i" :class="[{'mr-3' : i != 5} ,'new-grid grid bg-transparent']" draggable="true" @dragstart.stop="dragstart($event)" @dragend.stop="dragend($event)">
                                    <div class="tr">
                                        <div v-for="j in i" class="td"></div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="droppable" @dragover.prevent @dragenter.stop.prevent="isDropzoneActive() ? dragenter($event) : null" @dragleave.stop.prevent="isDropzoneActive() ? dragleave($event) : null" @drop.stop.prevent="isDropzoneActive() ? drop(index + 1) : null">
            <div></div>
        </div>
    </div>
</script>

<script>
Vue.component('template-imagemap-message', {
    template: '#--template-imagemap-message',
    props: ['index', 'currentNodeId', 'node', 'messagesCount', 'message', 'actions', 'newMessageType', 'hasQuickReply'],
    data(){
        return {
            counter: 0,
            form: this.message,

            currentAreaId: 4,
            currentAreaCount: 4,

            gottenAreaPath: '',
            gottenAreaIndex: 0,

            newAreaType: null,
            actionData: null,

            actionType: {
                currentAreaId: null,
                value: null,
                options: [
                    {
                        name: '好友回覆關鍵字',
                        value: 'keyword',
                    },
                    {
                        name: '直接開啟網址',
                        value: 'uri',
                    },
                    {
                        name: '透過 Liff 開啟網址',
                        value: 'liff',
                    }
                ],
                info: '',
                invalid: false,
            },
        }
    },
    methods: {
        remove(){
            this.$emit('removeMessage', this.index);
        },
        dragenter(event){
            this.counter ++;
            $(event.target).addClass('dragover');
        },
        dragleave(event){
            this.counter --;
            if(this.counter === 0) $(event.target).removeClass('dragover');
        },
        drop(index){
            this.counter = 0;
            this.$emit('newMessage', index);
        },
        isDropzoneActive(){
            if(this.newMessageType == 'quickreply-message' && (this.index == this.messagesCount - 1 && !this.hasQuickReply)){
                return true;
            }else if(this.newMessageType != 'quickreply-message' && this.messagesCount < 5){
                return true;
            }
            return false;
        },
        recurseArea(action, obj){
            for(key in obj){
                if(obj[key].id){
                    if(action == 'getCurrentAreaId' && obj[key].id > this.currentAreaId){
                        this.currentAreaId = obj[key].id;
                    }
                }else if(obj[key][0]){
                    this.recurseArea(action, obj[key]);
                }
            }
        },
        getCurrentAreaId(){
            this.recurseArea('getCurrentAreaId' ,this.form.area.area);
        },
        changeImage(){
            this.$refs.hiddenInputContainer.getElementsByTagName('input')[0].click();
        },
        dragstart(event){
            this.saveAndClearAction(null, false)
            this.newAreaType = event.currentTarget.id;
        },
        dragend(event){
            this.newAreaType = null;
        },
        newArea(areaId){
            if(this.currentAreaCount < 40){
                this.getArea(this.form.area.area, areaId, 'this.form.area.area');

                if(this.gottenAreaPath && this.gottenAreaIndex){
                    // 如果區域存在
                    newArea = null;
                    if(this.newAreaType){
                        if(typeMatch = this.newAreaType.match(/^new-row-(2|3|5)$/)){

                            newArea = [
                                [
                                    eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']'),
                                ],
                            ];

                            for(var i = 1; i < typeMatch[1]; i ++){
                                if(this.currentAreaCount >= 40) continue;

                                this.currentAreaId ++;
                                this.currentAreaCount ++;

                                newArea.push([
                                    {
                                        id: this.currentAreaId,
                                        type: {
                                            name: '好友回覆關鍵字',
                                            value: 'keyword',
                                        },
                                        action: {
                                            keyword: {
                                                value: null,
                                                info: '',
                                                invalid: false,
                                            },
                                            text: {
                                                text: '',
                                                info: '',
                                                invalid: false,
                                            },
                                        },
                                        selected: false,
                                        invalid: false,
                                    }
                                ]);
                            };

                            parentPath = this.gottenAreaPath.match(/^this.form.area.area\[([0-9]+)\]$/)
                            if(eval(this.gottenAreaPath).length == 1 && this.form.area.area.length == 1 && parentPath){
                                this.$set(this.form.area, 'area', newArea);
                            }else{
                                this.$set(eval(this.gottenAreaPath), this.gottenAreaIndex, newArea);
                            }

                        }else if(typeMatch = this.newAreaType.match(/^new-column-(2|3|5)$/)){

                            parentPath = this.gottenAreaPath.match(/^(.*)\[([0-9]+)\]$/)

                            newArea = [
                                eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']'),
                            ];

                            for(var i = 1; i < typeMatch[1]; i ++){
                                if(this.currentAreaCount >= 40) continue;

                                this.currentAreaId ++;
                                this.currentAreaCount ++;
                                newArea.push({
                                    id: this.currentAreaId,
                                    type: {
                                        name: '好友回覆關鍵字',
                                        value: 'keyword',
                                    },
                                    action: {
                                        keyword: {
                                            value: null,
                                            info: '',
                                            invalid: false,
                                        },
                                        text: {
                                            text: '',
                                            info: '',
                                            invalid: false,
                                        },
                                    },
                                    selected: false,
                                    invalid: false,
                                });
                            };
                            if(eval(this.gottenAreaPath).length == 1){
                                this.$set(eval(parentPath[1]), parentPath[2], newArea);
                            }else{
                                this.$set(eval(this.gottenAreaPath), this.gottenAreaIndex, [newArea]);
                            }
                        }
                    }
                };
            }
        },
        getArea(obj, areaId, path){
            for(key in obj){
                if(obj[key].id && obj[key].id == areaId){
                    this.gottenAreaPath = path;
                    this.gottenAreaIndex = key;
                }else if(obj[key][0]){
                    currentPath = path+'['+key+']';
                    this.getArea(obj[key], areaId, currentPath);
                }
            }
        },
        removeArea(areaId){
            this.getArea(this.form.area.area, areaId, 'this.form.area.area');

            if(eval(this.gottenAreaPath).length == 1){
                // 1個tr刪掉，往上提一層
                if(parentPath = this.gottenAreaPath.match(/^this.form.area.area\[([0-9]+)\]$/)){
                    // 第一層
                    if(this.form.area.area.length > 1){
                        this.form.area.area.splice(parentPath[1], 1);
                        this.currentAreaCount --;
                    }

                }else if(parentPath = this.gottenAreaPath.match(/^(((.*)\[([0-9]+)\])\[([0-9]+)\])\[([0-9]+)\]$/)){
                    // 內層
                    if(eval(parentPath[1]).length == 2){
                        // 只有兩個tr
                        if(eval(parentPath[1]+'['+(1 - parseInt(parentPath[6]))+']').length == 1){
                            this.$set(eval(parentPath[2]), parentPath[5], eval(parentPath[1]+'['+(1 - parseInt(parentPath[6]))+'][0]'));
                        }else{
                            eval(parentPath[1]).splice(parentPath[6], 1);
                        }

                    }else{
                        // 兩個tr以上
                        eval(parentPath[1]).splice(parentPath[6], 1);
                    }
                    this.currentAreaCount --;
                }

            }else if(eval(this.gottenAreaPath).length == 2){
                // 2個td刪掉一個
                if(parentPath = this.gottenAreaPath.match(/^this.form.area.area\[([0-9]+)\]$/)){
                    // 第一層
                    eval(this.gottenAreaPath).splice(this.gottenAreaIndex, 1);
                }else if(parentPath = this.gottenAreaPath.match(/^((.*)\[([0-9]+)\])\[([0-9]+)\]$/)){
                    // 內層
                    if(eval(parentPath[1]).length == 1){
                        this.$set(eval(parentPath[2]), parentPath[3], eval(this.gottenAreaPath+'['+(1 - this.gottenAreaIndex)+']'));
                    }else{
                        eval(this.gottenAreaPath).splice(this.gottenAreaIndex, 1);
                    }

                }
                this.currentAreaCount --;

            }else{
                // 多個td刪除一個
                eval(this.gottenAreaPath).splice(this.gottenAreaIndex, 1);
                this.currentAreaCount --;
            }

            if(this.actionType.currentAreaId == areaId) this.saveAndClearAction(null, true);
        },
        selectArea(id){
            // if(this.actionType.currentAreaId != id){
            this.saveAndClearAction(id, false);
            this.refreshActionData();
            // }
        },
        saveAndClearAction(newId, removed){
            if(removed){
                // 原本的區域已刪除
                this.actionType.value = null;
            }else if(this.actionType.currentAreaId){
                // 原本的區域沒刪除，儲存類型
                this.getArea(this.form.area.area, this.actionType.currentAreaId, 'this.form.area.area');
                eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']').type = this.actionType.value;
                eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']').selected = false;
            }

            if(newId){
                this.actionType.currentAreaId = newId;
                // 新area的資料
                this.getArea(this.form.area.area, newId, 'this.form.area.area');
                newAreaData = eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']');
                eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']').invalid = false;
                eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']').selected = true;
                this.actionType.value = newAreaData.type;
            }else{
                this.$set(this.actionType, 'currentAreaId', null);
            }
        },
        updateActionData(data){
            this.getArea(this.form.area.area, this.actionType.currentAreaId, 'this.form.area.area');
            this.$set(eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']'), 'action', data);
        },
        refreshActionData(){
            this.getArea(this.form.area.area, this.actionType.currentAreaId, 'this.form.area.area');
            this.actionData = eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']').action;
        },
        closeEditBlock(){
            this.saveAndClearAction(null, false);
        },
        changeActionType(){
            this.getArea(this.actionType.currentAreaId);
            // if(this.actionType.value.value == 'next'){
            //     // 從不是next換到next
            //     this.$emit('insertNewNode', this.node.end);
            // }else if(eval(this.gottenAreaPath+'['+this.gottenAreaIndex+'].type.value') == 'next'){
            //     // 從next換到不是next
            //     this.$emit('removeNode', eval(this.gottenAreaPath+'['+this.gottenAreaIndex+'].action.nextId.nextId'));
            // }

            this.$set(eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']'), 'type', this.actionType.value);

            // if(this.actionType.value.value == 'next'){
            //     this.$set(eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']'), 'action', {
            //         label: eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']').action.label,
            //         nextId: {
            //             nextId: this.currentNodeId + 1,
            //             info: '',
            //             invalid: false,
            //         }
            //     });
            // }else{
                this.$set(eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']'), 'action', $.extend(true, {
                    label: eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']').action.label
                }, this.actions[this.actionType.value.value]));
            // }

            this.refreshActionData();
        }
    },
    created(){
        this.getCurrentAreaId();
        this.closeEditBlock();
    },
    mounted(){
        var vm = this;
        this.dropzone = new Dropzone(this.$refs.image, {
            url: "#",
            autoProcessQueue: false,
            maxFiles: 1,
            acceptedFiles: ".jpeg,.jpg,.png,.JPEG,.JPG,.PNG",
            thumbnailWidth: null,
            thumbnailHeight: null,
            hiddenInputContainer: vm.$refs.hiddenInputContainer,
            addedfile: function(file){
                vm.form.image.info = '';
                vm.form.image.invalid = false;

                var img = new Image();

                img.src = window.URL.createObjectURL(file);
                img.onload = function(){
                    var width = img.naturalWidth,
                    height = img.naturalHeight;

                    if(width != 1040 || height != 1040){
                        vm.$set(vm.form.image, 'info', '請符合圖片尺寸：1040 x 1040');
                        vm.$set(vm.form.image, 'invalid', true);
                        return false;
                    }

                    vm.$set(vm.form, 'image', {
                        src: window.URL.createObjectURL(file),
                        alt: file.name,
                        file: file,
                        info: '',
                        invalid: false,
                    });
                };
                return false;
            },
        });
    },
    updated(){
        this.$emit('updateMessageData', this.index, this.form);
    },
});
</script>

{{-- 隔線系統 --}}

<script type="text/x-template" id="--grid-tr">
    <div class="tr">
        <div v-for="td in tds" :key="td.id" :class="[{'has-error' : td.id && td.invalid}, {'selected' : td.id && td.selected}, 'td']" :data-id="td.id ? td.id : ''" @dragover.prevent @dragenter.stop.prevent="td.id ? dragenter($event) : null" @dragleave.stop.prevent="td.id ? dragleave($event) : null" @drop.stop.prevent="td.id ? drop($event) : null" @click.stop="td.id ? selectArea(td.id) : null" @mouseover.stop="td.id ? mouseover($event) : null" @mouseleave.stop="td.id ? mouseleave($event) : null">
            <div v-if="td.id" class="index">
                <span class="id position-absolute">@{{ td.id }}</span>
                <span class="times position-absolute" @click.stop="removeArea(td.id)">
                    <v-svg src="{{ asset('assets/image/svg/light/times.svg') }}" width="12" height="12"></v-svg>
                </span>
            </div>
            <grid-tr v-else v-for="tds in td" :tds="tds" :key="td.id" @removeArea="removeArea" @newArea="newArea" @selectArea="selectArea"></grid-tr>
        </div>
    </div>
</script>

<script>
Vue.component('grid-tr', {
    template: '#--grid-tr',
    props: ['tds'],
    methods: {
        dragenter(event){
            $(event.target).addClass('dragover');
        },
        dragleave(event){
            $(event.target).removeClass('dragover');
        },
        drop(event){
            this.newArea(event.target.getAttribute('data-id'));
            $(event.target).removeClass('dragover');
        },
        newArea(id){
            this.$emit('newArea', id);
        },
        removeArea(id){
            this.$emit('removeArea', id);
        },
        mouseover(event){
            $(event.target).addClass('hover');
        },
        mouseleave(event){
            $(event.target).removeClass('hover');
        },
        selectArea(id){
            this.$emit('selectArea', id)
        }
    }
});
</script>

@endpush
