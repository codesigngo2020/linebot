{{-- Uri action 直接開啟網址 --}}

@push('js')

<script type="text/x-template" id="--template-uri-action">
    <div>
        <div class="form-group mb-3">
            <label for="text">開啟網址<span class="form-required">*</span></label>
            <input id="text" type="url" :class="[{'is-invalid' : form.uri.invalid}, 'form-control form-control-sm']" v-model="form.uri.uri">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.uri.info }}</strong></span>
        </div>
    </div>
</script>

<script>
Vue.component('template-uri-action', {
    template: '#--template-uri-action',
    props: ['id', 'action', 'columnIndex'],
    data: function(){
        return {
            actionData: null,
            form: this.action,
        };
    },
    updated(){
        this.$emit('updateActionData', this.form, this.columnIndex);
    },
    watch: {
        action(newValue){
            this.$set(this, 'form', newValue);
        },
    }
});
</script>

@endpush
