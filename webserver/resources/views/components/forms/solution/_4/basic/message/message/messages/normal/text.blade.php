{{-- Text message --}}

@push('js')

<script type="text/x-template" id="--text-message">
    <div>
        <div class="d-flex message text-message">
            <div class="buttons mr-2">
                <div v-if="messagesCount > 1" class="text-muted text-danger-hover cursor-pointer" @click="remove">
                    <v-svg src="{{ asset('assets/image/svg/light/trash-alt.svg') }}" width="16" height="16"></v-svg>
                </div>
            </div>
            <div>
                <div class="type text-muted pl-2 mb-2">Text message</div>
                <div class="content">

                    <div :class="[{'is-invalid' : form.text.invalid}, 'i-form-control card mb-0']">
                        <div class="card-body text-justify">
                            <div class="texarea-not-input position-relative">
                                <pre ref="inputPre"></pre>
                                <textarea class="not-input" placeholder="請輸入訊息內容" v-model="form.text.text"></textarea>
                            </div>
                        </div>
                    </div>
                    <span class="invalid-feedback" role="alert"><strong>@{{ form.text.info }}</strong></span>

                </div>
            </div>
        </div>

        <div class="droppable p-4" @dragover.prevent @dragenter.stop.prevent="isDropzoneActive() ? dragenter($event) : null" @dragleave.stop.prevent="isDropzoneActive() ? dragleave($event) : null" @drop.stop.prevent="isDropzoneActive() ? drop(index + 1) : null">
            <div></div>
        </div>
    </div>
</script>

<script>
Vue.component('text-message', {
    template: '#--text-message',
    props: ['index', 'messagesCount', 'newMessageType', 'message'],
    data(){
        return {
            counter: 0,
            form: this.message,
        }
    },
    methods: {
        remove(){
            this.$emit('removeMessage', this.index);
        },
        dragenter(event){
            this.counter ++;
            $(event.target).addClass('dragover');
        },
        dragleave(event){
            this.counter --;
            if(this.counter === 0) $(event.target).removeClass('dragover');
        },
        drop(index){
            this.counter = 0;
            this.$emit('newMessage', index);
        },
        isDropzoneActive(){
            if(this.newMessageType == 'quickreply-message' && (this.index == this.messagesCount - 1 && !this.hasQuickReply)){
                return true;
            }else if(this.newMessageType != 'quickreply-message' && this.messagesCount < 5){
                return true;
            }
            return false;
        }
    },
    // updated(){
    //     this.$emit('updateMessageData', this.index, this.form);
    // }
    watch: {
        'form.text.text': function(newValue){
            this.$refs.inputPre.innerHTML = newValue.replaceAll(/\r\n|\r|\n/g, '<br>')+'<br>';
        },
    }
});
</script>

@endpush
