{{-- Confirm message --}}

@push('js')

<script type="text/x-template" id="--confirm-message">
    <div>
        <div class="d-flex message confirm-message">
            <div class="buttons mr-2">
                <div v-if="messagesCount > 1" class="text-muted text-danger-hover cursor-pointer" @click="remove">
                    <v-svg src="{{ asset('assets/image/svg/light/trash-alt.svg') }}" width="16" height="16"></v-svg>
                </div>
            </div>
            <div class="w-100">
                <div class="type text-muted pl-2 mb-2">Confirm message</div>
                <div class="content d-flex flex-nowrap row ml-0 pt-2 mt-n2 overflow-auto">
                    <div class="d-flex flex-nowrap row col-auto">
                        <div class="pr-5">

                            <div :class="[{'is-invalid' : form.text.invalid}, 'i-form-control card mb-0']">
                                <div class="card-body text-justify">
                                    <div class="texarea-not-input position-relative">
                                        <pre ref="inputPre"></pre>
                                        <textarea class="not-input" placeholder="請輸入訊息內容" v-model="form.text.text"></textarea>
                                    </div>
                                </div>

                                <hr class="m-0">

                                <div class="actions d-flex mt-2 mb-2">

                                    <div v-for="(action, index) in form.actions" :key="action.id" class="position-relative d-flex align-items-center cursor-pointer" @click="selectButton(action.id)">
                                        <input class="not-input text-center pt-3 pb-3 pl-4 pr-4" placeholder="輸入名稱及行為" v-model="action.action.label.label">
                                        <div class="position-absolute left d-flex mr-2 pl-2 bg-white">
                                            <div :class="[action.invalid ? 'text-danger' : 'text-muted', 'ml-2']">@{{ action.id }}</div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <span v-if="form.text.invalid" class="invalid-feedback" role="alert"><strong>@{{ form.text.info }}</strong></span>
                            <span v-for="action in form.actions" v-if="action.action.label.invalid" class="invalid-feedback" role="alert"><strong>@{{ action.action.label.info }}</strong></span>

                        </div>


                        <div v-if="actionType.currentButtonId" class="col-370 m-0 pl-0 pb-5 pr-5">
                            {{-- 按鈕行為 --}}
                            <div class="position-relative nopadding">
                                <button type="button" class="close-edit-block position-absolute top right d-flex align-items-center btn btn-outline-danger btn-sm" @click="closeEditBlock">
                                    <span class="d-flex align-items-center">
                                        <v-svg src="{{ asset('assets/image/svg/light/times.svg') }}" width="14" height="14"></v-svg>
                                    </span>
                                    <span>收合視窗</span>
                                </button>
                                <div class="form-group">
                                    <label for="actionType">按鈕行為<span class="form-required">*</span><span>@{{ '（正在編輯按鈕 '+actionType.currentButtonId+'）' }}</span></label>
                                    <vue-multiselect :class="[{'is-invalid' : actionType.invalid}, 'hidden-arrow']" v-model="actionType.value" track-by="value" label="name" placeholder="請選擇行為" openDirection="bottom" :options="actionTypes" :searchable="false" :show-labels="false" :allow-empty="false" :disabled="!actionType.currentButtonId" @input="changeActionType"></vue-multiselect>
                                    <span class="invalid-feedback" role="alert"><strong>@{{ actionType.info }}</strong></span>
                                </div>

                                <component v-if="actionType.currentButtonId && actionType.value && actionType.value.show" :is="actionType.value.value+'-action'" :id="actionType.currentButtonId" :action="actionData" @updateActionData="updateActionData"></component>
                            </div>
                        </div>
                    </div>

                    <div class="w-420 col-auto m-0 pb-5 pl-0 pr-0">

                        {{-- 替代文字 --}}
                        <div class="form-group">
                            <label for="altText">替代文字<span class="form-required">*</span><span>（訊息通知中顯示）</span></label>
                            <input id="altText" type="text" :class="[{'is-invalid' : form.altText.invalid}, 'form-control']" v-model="form.altText.altText">
                            <span class="invalid-feedback" role="alert"><strong>@{{ form.altText.info }}</strong></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="droppable p-4" @dragover.prevent @dragenter.stop.prevent="isDropzoneActive() ? dragenter($event) : null" @dragleave.stop.prevent="isDropzoneActive() ? dragleave($event) : null" @drop.stop.prevent="isDropzoneActive() ? drop(index + 1) : null">
            <div></div>
        </div>
    </div>
</script>

<script>
Vue.component('confirm-message', {
    template: '#--confirm-message',
    props: ['index', 'messagesCount', 'newMessageType', 'message', 'actions', 'actionTypes'],
    data(){
        return {
            counter: 0,
            currentId: this.getCurrentButtonId(),

            gottenButtonIndex: 0,
            actionData: null,

            form: this.message,
            actionType: {
                currentButtonId: null,
                value: null,
                info: '',
                invalid: false,
            }
        }
    },
    methods: {
        remove(){
            this.$emit('removeMessage', this.index);
        },
        dragenter(event){
            this.counter ++;
            $(event.target).addClass('dragover');
        },
        dragleave(event){
            this.counter --;
            if(this.counter === 0) $(event.target).removeClass('dragover');
        },
        drop(index){
            this.counter = 0;
            this.$emit('newMessage', index);
        },
        isDropzoneActive(){
            if(this.newMessageType == 'quickreply-message' && (this.index == this.messagesCount - 1 && !this.hasQuickReply)){
                return true;
            }else if(this.newMessageType != 'quickreply-message' && this.messagesCount < 5){
                return true;
            }
            return false;
        },
        getCurrentButtonId(){
            return this.message.actions.reduce((max, action) => Math.max(max, action.id), 1);
        },
        getButton(buttonId){
            for(key in this.form.actions){
                if(this.form.actions[key].id && this.form.actions[key].id == buttonId){
                    this.gottenButtonIndex = key;
                }
            }
        },
        selectButton(id){
            if(this.actionType.currentButtonId != id){
                this.saveAndClearAction(id, false);
                this.refreshActionData();
            }
        },
        saveAndClearAction(newId, removed){
            if(removed){
                // 原本的區域已刪除
                this.actionType.value = null;
            }else if(this.actionType.currentButtonId){
                // 原本的區域沒刪除，儲存類型
                this.getButton(this.actionType.currentButtonId);
                this.form.actions[this.gottenButtonIndex].type = this.actionType.value;
                this.form.actions[this.gottenButtonIndex].selected = this.actionType.false;
            }

            if(newId){
                this.actionType.currentButtonId = newId;
                // 新button的資料
                this.getButton(newId);
                newAreaData = this.form.actions[this.gottenButtonIndex];
                this.form.actions[this.gottenButtonIndex].invalid = false;
                this.form.actions[this.gottenButtonIndex].selected = true;
                this.actionType.value = newAreaData.type;

            }else{
                this.actionType.currentButtonId = null;
            }
        },
        updateActionData(data){
            this.getButton(this.actionType.currentButtonId);
            this.$set(this.form.actions[this.gottenButtonIndex], 'action', data);
        },
        refreshActionData(){
            this.getButton(this.actionType.currentButtonId);
            this.actionData = this.form.actions[this.gottenButtonIndex].action;
        },
        closeEditBlock(){
            this.saveAndClearAction(null, false);
        },
        changeActionType(){
            this.getButton(this.actionType.currentButtonId);
            this.$set(this.form.actions[this.gottenButtonIndex], 'type', this.actionType.value);
            this.$set(this.form.actions[this.gottenButtonIndex], 'action', $.extend(true, {
                label: this.form.actions[this.gottenButtonIndex].action.label
            }, this.actions[this.actionType.value.value]));
            this.refreshActionData();
        },
    },
    // updated(){
    //     this.$emit('updateMessageData', this.index, this.form);
    // }
    watch: {
        'form.text.text': function(newValue){
            this.$refs.inputPre.innerHTML = newValue.replaceAll(/\r\n|\r|\n/g, '<br>')+'<br>';
        },
    }
});
</script>

@endpush
