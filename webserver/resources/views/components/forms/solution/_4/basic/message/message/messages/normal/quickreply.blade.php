{{-- Quick Reply message --}}

@push('js')

<script type="text/x-template" id="--quickreply-message">
    <div>
        <div class="d-flex message quickreply-message">
            <div class="buttons mr-2">
                <div class="text-muted text-danger-hover cursor-pointer" @click="remove">
                    <v-svg src="{{ asset('assets/image/svg/light/trash-alt.svg') }}" width="16" height="16"></v-svg>
                </div>
            </div>
            <div class="w-100">
                <div class="type text-muted pl-2 mb-2">Quick Reply message</div>

                <div class="alert alert-dismissible fade show alert-light text-justify font-size-0875 mt-3 mb-3" role="alert">
                    Quick Reply 的圖片為選擇性上傳，若不上傳圖片，則該按鈕僅有文字
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="content d-flex flex-nowrap row ml-0 pt-2 mt-n2 overflow-auto">
                    <div v-for="(item, itemIndex) in form.items.items" :key="item.id" class="d-flex flex-nowrap row col-auto">
                        <div class="position-relative insert-or-remove-buttons-layout">

                            {{-- 新增/移除 item --}}
                            <div class="position-absolute insert-or-remove-buttons">
                                <div :class="[{'d-none' : form.items.items.length == 1}, 'text-muted mr-1 cursor-pointer text-danger-hover']" @click.stop="removeItem(itemIndex)">
                                    <v-svg src="{{ asset('assets/image/svg/light/minus-circle.svg') }}" width="20" height="20"></v-svg>
                                </div>
                                <div :class="[{'d-none' : form.items.items.length >= 10}, 'text-muted cursor-pointer text-primary-hover']" @click.stop="newItem(itemIndex)">
                                    <v-svg src="{{ asset('assets/image/svg/light/plus-circle.svg') }}" width="20" height="20"></v-svg>
                                </div>
                            </div>

                            {{-- 按鈕 --}}
                            <div :class="[{'is-invalid' : item.image.invalid || item.actions[0].action.label.invalid}, 'button i-form-control']" @click="selectButton(itemIndex, item.actions[0].id)">

                                {{-- 名稱 --}}
                                <input :class="[{'bg-danger' : item.actions[0].invalid}, {valued : item.actions[0].action.label.label != ''}, 'not-input text-center text-white cursor-pointer pt-2 pb-2 pr-1']" placeholder="輸入名稱及行為" v-model="item.actions[0].action.label.label">

                                {{-- 圖片上傳 --}}
                                <div class="image-layout position-absolute">

                                    <div :class="[{'i-dz-max-files-reached' : item.image.src}, 'position-absolute top w-100 h-100 overflow-hidden rounded-circle idropzone dropzone-single']" :ref="'image-'+item.id">
                                        <div :id="'dz-preview-'+index" class="dz-preview dz-preview-single rounded-circle">
                                            <div class="dz-preview-cover">
                                                <img :class="[{ 'd-none' : !item.image.src }, 'dz-preview-img']" :src="item.image.src" :alt="item.image.alt"/>
                                            </div>
                                        </div>
                                        <div class="dz-default dz-message rounded-circle border-0">
                                            <span class="d-flex">
                                                <v-svg src="{{ asset('assets/image/svg/light/image.svg') }}" width="13" height="13"></v-svg>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <span v-if="item.image.invalid" class="invalid-feedback" role="alert"><strong>@{{ item.image.info }}</strong></span>
                            <span v-if="item.actions[0].action.label.invalid" class="invalid-feedback" role="alert"><strong>@{{ item.actions[0].action.label.info }}</strong></span>

                        </div>

                        <div v-if="actionType.currentItemId && actionType.currentItemId == item.id && actionType.currentButtonId" class="col-370 m-0 pl-0 pb-5 pr-5">
                            {{-- 按鈕行為 --}}
                            <div class="position-relative nopadding">
                                <button type="button" class="close-edit-block position-absolute top right d-flex align-items-center btn btn-outline-danger btn-sm" @click="closeEditBlock(itemIndex)">
                                    <span class="d-flex align-items-center">
                                        <v-svg src="{{ asset('assets/image/svg/light/times.svg') }}" width="14" height="14"></v-svg>
                                    </span>
                                    <span>收合視窗</span>
                                </button>
                                <div class="form-group">
                                    <label for="actionType">按鈕行為<span class="form-required">*</span></label>
                                    <vue-multiselect :class="[{'is-invalid' : actionType.invalid}, 'hidden-arrow']" v-model="actionType.value" track-by="value" label="name" placeholder="請選擇行為" openDirection="bottom" :options="quickReplyActionTypes" :searchable="false" :show-labels="false" :allow-empty="false" :disabled="!actionType.currentButtonId" @input="changeActionType(itemIndex)"></vue-multiselect>
                                    <span class="invalid-feedback" role="alert"><strong>@{{ actionType.info }}</strong></span>
                                </div>

                                <component v-if="actionType.currentButtonId && actionType.value && actionType.value.show" :is="actionType.value.value+'-action'" :id="actionType.currentButtonId" :action="actionData" :columnIndex="itemIndex" @updateActionData="updateActionData"></component>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script>
Vue.component('quickreply-message', {
    template: '#--quickreply-message',
    props: ['index', 'messagesCount', 'message', 'actions', 'defaultAction', 'quickReplyActionTypes'],
    data(){
        return {
            counter: 0,
            currentItemId: this.getCurrentItemId(),
            currentButtonId: this.getCurrentButtonId(),

            gottenItemIndex: 0,
            gottenButtonIndex: 0,
            actionData: null,

            form: this.message,

            default: {
                action: this.defaultAction,
                // actionType: this.actionType,
            },
            actionType: {
                currentItemId: null,
                currentButtonId: null,
                value: null,
                info: '',
                invalid: false,
            }
        }
    },
    methods: {
        remove(){
            this.$emit('removeMessage', this.index);
        },
        getCurrentItemId(){
            return this.message.items.items.reduce((max, item) => Math.max(max, item.id), 1);
        },
        getCurrentButtonId(){
            return this.message.items.items.reduce(function(max, item){
                return Math.max(max, item.actions.reduce((max, action) => Math.max(max, action.id), 1));
            }, 1);
        },
        getItem(itemId){
            var vm = this;
            this.form.items.items.forEach(function(item, itemIndex){
                if(item.id && item.id == itemId){
                    vm.gottenItemIndex = itemIndex;
                    return false;
                }
            });
        },
        getButton(buttonId, itemIndex){
            var vm = this;
            if(itemIndex){
                this.form.items.items[itemIndex].actions.forEach(function(action, actionIndex){
                    if(action.id && action.id == buttonId){
                        vm.gottenItemIndex = itemIndex;
                        vm.gottenButtonIndex = actionIndex;
                        return false;
                    }
                });
            }else{
                this.form.items.items.forEach(function(item, itemIndex){
                    item.actions.forEach(function(action, actionIndex){
                        if(action.id && action.id == buttonId){
                            vm.gottenItemIndex = itemIndex;
                            vm.gottenButtonIndex = actionIndex;
                            return false;
                        }
                    });
                });
            }
        },
        removeItem(itemIndex){
            this.form.items.items.splice(itemIndex, 1);
        },
        newItem(itemIndex){
            if(this.form.items.items.length < 10){
                this.currentItemId ++;
                this.currentButtonId ++;

                this.form.items.items.splice(itemIndex + 1, 0, {
                    id: this.currentItemId,
                    image: {
                        src: '',
                        alt: '',
                        file: null,
                        info: '',
                        invalid: false
                    },
                    actions: [
                        $.extend(true, {id: this.currentButtonId}, this.default.action),
                    ],
                    // actionType: $.extend(true, {currentButtonId: null}, this.default.actionType),
                });
                var vm = this;
                this.$nextTick(function(){
                    vm.initiateImageDropzone(vm.currentItemId);
                });
            }
        },
        selectButton(itemIndex, id){
            if(this.actionType.currentButtonId != id){
                this.saveAndClearAction(id, false);
                this.refreshActionData(id);
            }
        },
        saveAndClearAction(newId, removed){
            if(removed){
                // 原本的區域已刪除
                this.actionType.value = null;
            }else{
                if(this.actionType.currentButtonId){
                    // 原本的區域沒刪除，儲存類型
                    this.getButton(this.actionType.currentButtonId);
                    this.form.items.items[this.gottenItemIndex].actions[this.gottenButtonIndex].type = this.actionType.value;
                    this.form.items.items[this.gottenItemIndex].actions[this.gottenButtonIndex].selected = false;
                }
                this.actionType.currentItemId = null;
                this.actionType.currentButtonId = null;
            }

            if(newId){
                this.getButton(newId);

                // 新button的資料
                newAreaData = this.form.items.items[this.gottenItemIndex].actions[this.gottenButtonIndex];

                this.form.items.items[this.gottenItemIndex].actions[this.gottenButtonIndex].invalid = false;
                this.form.items.items[this.gottenItemIndex].actions[this.gottenButtonIndex].selected = true;
                this.actionType.value = newAreaData.type;

                this.actionType.currentItemId = this.form.items.items[this.gottenItemIndex].id;
                this.actionType.currentButtonId = newId;
            }else{
                this.actionType.currentItemId = null;
                this.actionType.currentButtonId = null;
            }
        },
        updateActionData(data, itemIndex){
            this.getButton(this.actionType.currentButtonId, itemIndex);
            this.$set(this.form.items.items[this.gottenItemIndex].actions[this.gottenButtonIndex], 'action', data);
        },
        refreshActionData(id){
            this.getButton(id);
            this.actionData = this.form.items.items[this.gottenItemIndex].actions[this.gottenButtonIndex].action;
        },
        closeEditBlock(){
            this.saveAndClearAction(null, false);
        },
        initiateImageDropzone(itemId){
            var vm = this;
            this['dropzone-' + itemId] = new Dropzone(vm.$refs['image-' + itemId][0], {
                url: "#",
                autoProcessQueue: false,
                maxFiles: 1,
                acceptedFiles: ".jpeg,.jpg,.png,.JPEG,.JPG,.PNG",
                thumbnailWidth: null,
                thumbnailHeight: null,
                addedfile: function(file){
                    vm.getItem(itemId);
                    vm.form.items.items[vm.gottenItemIndex].image.info = '';
                    vm.form.items.items[vm.gottenItemIndex].image.invalid = false;

                    var img = new Image();

                    img.src = window.URL.createObjectURL(file);
                    img.onload = function(){
                        var width = this.naturalWidth,
                        height = this.naturalHeight;

                        if(width != height){
                            vm.form.items.items[vm.gottenItemIndex].image.info = '請上傳正方形圖片';
                            vm.form.items.items[vm.gottenItemIndex].image.invalid = true;
                            return false;
                        }

                        vm.form.items.items[vm.gottenItemIndex].image.src = window.URL.createObjectURL(file);
                        vm.form.items.items[vm.gottenItemIndex].image.alt = file.name;
                        vm.form.items.items[vm.gottenItemIndex].image.file = file;
                        vm.form.items.items[vm.gottenItemIndex].image.info = '';
                        vm.form.items.items[vm.gottenItemIndex].image.invalid = false;
                    };
                    return false;
                },
            });
        },
        changeActionType(itemIndex){
            this.gottenItemIndex = itemIndex;
            this.getButton(this.actionType.currentButtonId, itemIndex);
            newValue = this.actionType.value;

            this.$set(this.form.items.items[this.gottenItemIndex].actions[this.gottenButtonIndex], 'type', newValue);
            this.$set(this.form.items.items[this.gottenItemIndex].actions[this.gottenButtonIndex], 'action', $.extend(true, {
                label: this.form.items.items[this.gottenItemIndex].actions[this.gottenButtonIndex].action.label
            }, this.actions[newValue.value]));
            this.refreshActionData(this.actionType.currentButtonId);
        }
    },
    mounted(){
        var vm = this;
        this.form.items.items.forEach(function(item, itemIndex){
            vm.initiateImageDropzone(item.id);
        });
    },
    // updated(){
    //     this.$emit('updateMessageData', this.index, this.form);
    // },
});
</script>

@endpush
