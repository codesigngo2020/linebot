@push('js')

{{-- 腳本節點 --}}

<script type="text/x-template" id="--node">
    <div :class="[{editting : editting}, {'w-600' : nodeWidth == 'wide'}, 'node']" @click="node.disabled ? null : editNode()">
        <div v-if="!editting" class="labels mb-0 d-flex flex-column">
            <div class="condition-labels">
                <p class="badge badge-soft-info">@{{ node.condition.name }}</p>
            </div>
            <div class="message-labels pb-0">
                <p v-for="(message, index) in node.messages.messages" class="d-block" :key="message.id" :class="[message.invalid ? 'badge-soft-danger' : 'badge-soft-secondary','badge']">@{{ message.type }}</p>
            </div>
        </div>

        <div v-if="!node.disabled && editting" class="position-relative w-100 overflow-hidden">

            <div>
                <div class="droppable" @dragover.prevent @dragenter.stop.prevent="isDropzoneActive() ? dragenter($event) : null" @dragleave.stop.prevent="isDropzoneActive() ? dragleave($event) : null" @drop.stop.prevent="isDropzoneActive() ? drop(0) : null">
                    <div></div>
                </div>
            </div>

            <button type="button" class="close-edit-block position-absolute top right d-flex align-items-center z-index-1 btn btn-outline-danger btn-sm" @click.stop="closeEditNode">
                <span class="d-flex align-items-center">
                    <v-svg src="{{ asset('assets/image/svg/light/times.svg') }}" width="14" height="14"></v-svg>
                </span>
                <span>收合節點</span>
            </button>

            <component v-for="(message, index) in form.node.messages.messages" :key="message.id" :is="'template-'+message.type+'-message'" :index="index" :currentNodeId="currentNodeId" :node="node" :messagesCount="getMessagesCount()" :message="message.message" :actions="['buttons', 'imagemap', 'confirm', 'carousel', 'imagecarousel', 'quickreply'].includes(message.type) ? actions : null" :defaultAction="['buttons', 'carousel', 'imagecarousel', 'quickreply'].includes(message.type) ? defaultData.action : null" :actionTypes="['buttons', 'imagemap', 'confirm', 'carousel', 'imagecarousel'].includes(message.type) ? actionTypes : null" :quickReplyActionTypes="message.type == 'quickreply' ? quickReplyActionTypes : null" :newMessageType="newMessageType" :hasQuickReply="hasQuickReply" @removeMessage="removeMessage" @newMessage="newMessage" @insertNewNode="insertNewNode" @removeNode="removeNode"></component>

        </div>
    </div>
</script>

<script>
Vue.component('node', {
    template: '#--node',
    props: ['node', 'edittingNodesId', 'currentNodeId', 'newMessageType', 'actionTypes' ,'quickReplyActionTypes', 'actions', 'defaultData'],
    data(){
        return {
            editting: false,
            nodeWidth: 'normal',

            hasQuickReply: false,

            counter: 0,
            currentId: this.getCurrentId(),

            form: {
                node: this.node,
            },
        }
    },
    methods: {
        editNode(){
            if(!this.editting){
                this.$emit('editNode', this.node.id);
            }
        },
        closeEditNode(){
            this.$emit('closeEditNode', this.node.id);
        },
        dragenter(event){
            this.counter ++;
            $(event.target).addClass('dragover');
        },
        dragleave(event){
            this.counter --;
            if(this.counter === 0) $(event.target).removeClass('dragover');
        },
        drop(index){
            this.counter = 0;
            this.newMessage(index);
        },
        isDropzoneActive(){
            if(this.newMessageType == 'quickreply-message' && (this.index == this.getMessagesCount() - 1 && !this.hasQuickReply)){
                return true;
            }else if(this.newMessageType != 'quickreply-message' && this.getMessagesCount() < 5){
                return true;
            }
            return false;
        },
        getCurrentId(){
            return this.node.messages.messages.reduce((max, message) => Math.max(max, message.id.split('-')[1]), 1);
        },
        getMessagesCount(){
            return this.node.messages.messages.filter(message => message.type != 'quickreply').length;
        },
        // updateMessageData(index, data){
        //     this.$set(this.form.node.messages.messages[index], 'message', data);
        //     this.$emit('updateNodeData', this.node.id, this.form.node);
        // },
        insertNewNode(parentEnd){
            this.$emit('insertNewNode', parentEnd)
        },
        removeNode(id){
            this.$emit('removeNode', id)
        },
        setNodeWidth(){
            node = this.form.node.messages.messages.find(function(node){
                return ['imagemap', 'confirm', 'buttons', 'carousel', 'imagecarousel', 'quickreply', 'coupon'].includes(node.type);
            });
            this.nodeWidth = node ? 'wide' : 'normal';
        },
        // recurseImagemap(action, obj){
        //     for(key in obj){
        //         if(obj[key].id && obj[key].type.value == 'next'){
        //             if(action == 'removeNode'){
        //                 this.removeNode(obj[key].action.nextId.nextId);
        //             }
        //         }else if(obj[key][0]){
        //             this.recurseImagemap(action, obj[key]);
        //         }
        //     }
        // },
        // traverseMessages(actionName, index){
        //     var vm = this,
        //     message = this.form.node.messages.messages[index];
        //
        //     switch(message.type){
        //         case 'imagemap':
        //         this.recurseImagemap(actionName, message.message.area.area);
        //         break;
        //
        //         case 'buttons':
        //         case 'confirm':
        //         message.message.actions.forEach(function(action){
        //             if(action.type.value == 'next') vm.$emit(actionName, action.action.nextId.nextId);
        //         });
        //         break;
        //
        //         case 'carousel':
        //         case 'imagecarousel':
        //         case 'quickreply':
        //         eval('message.message.' + (message.type == 'quickreply' ? 'items.items' : 'columns.columns')).forEach(function(column){
        //             column.actions.forEach(function(action){
        //                 if(action.type.value == 'next') vm.$emit(actionName, action.action.nextId.nextId);
        //             });
        //         });
        //         break;
        //
        //         default:
        //     }
        // },
        removeMessage(index){
            if(this.form.node.messages.messages[index].type == 'quickreply' || this.form.node.messages.messages.length > 1){
                if(this.form.node.messages.messages[index].type == 'quickreply') this.hasQuickReply = false;
                // this.traverseMessages('removeNode', index);

                this.form.node.messages.messages.splice(index, 1);
                this.$emit('updateNodeData', this.node.id, this.form.node);
                this.setNodeWidth();

            }
        },
        newMessage(index){
            if((this.getMessagesCount() < 5 && messageForm.newMessageType) || this.newMessageType == 'quickreply-message'){
                messageType = messageForm.newMessageType.match(/(.+)-message/);
                eval('this.new'+messageType[1].capitalize()+'Message')(index);
                this.setNodeWidth();
            }
        },
        newTextMessage(index){
            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'text',
                deleteable: true,
                message: {
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    }
                }
            });
        },
        newImageMessage(index){
            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'image',
                deleteable: true,
                message: {
                    image: this.defaultData.image,
                }
            });
        },
        newImagemapMessage(index){
            area = [];
            for(var i = 0; i < 2; i ++){
                area[i] = [];
                for(var j = 0; j < 2; j ++){
                    area[i].push({
                        id: 2*i+j+1,
                        type: {
                            name: '好友回覆關鍵字',
                            value: 'keyword',
                        },
                        action: {
                            keyword: {
                                value: null,
                                info: '',
                                invalid: false,
                            },
                            text: {
                                text: '',
                                info: '',
                                invalid: false,
                            },
                        },
                        selected: false,
                        invalid: false,
                    });
                }
            };

            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'imagemap',
                deleteable: true,
                message: {
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    image: this.defaultData.image,
                    show: {
                        active: true,
                        info: '',
                        invalid: false,
                    },
                    area: {
                        area: area,
                        info: '',
                        invalid: false,
                    },
                }
            });
        },
        newButtonsMessage(index){
            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'buttons',
                deleteable: true,
                message: {
                    title: {
                        title: '',
                        info: '',
                        invalid: false,
                    },
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    },
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    image: $.extend(true, {
                        active: {
                            active: true,
                            info: '',
                            invalid: false,
                        },
                        ratio: {
                            ratio: 'rectangle',
                            info: '',
                            invalid: false,
                        }
                    }, this.defaultData.image),
                    actions: [
                        $.extend(true, {id: 1}, this.defaultData.action),
                    ],
                    // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
                }
            });
        },
        newConfirmMessage(index){
            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'confirm',
                deleteable: true,
                message: {
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    },
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    actions: [
                        $.extend(true, {id: 1}, this.defaultData.action),
                        $.extend(true, {id: 2}, this.defaultData.action),
                    ],
                    // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
                },
            });
        },
        newCarouselMessage(index){
            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'carousel',
                deleteable: true,
                message: {
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    columns: {
                        info: '',
                        invalid: false,
                        columns:[
                            {
                                id: 1,
                                title: {
                                    title: '',
                                    info: '',
                                    invalid: false,
                                },
                                text: {
                                    text: '',
                                    info: '',
                                    invalid: false,
                                },
                                image: this.defaultData.image,
                                actions: [
                                    $.extend(true, {id: 1}, this.defaultData.action),
                                ],
                                // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
                            }
                        ],
                    },
                    title: {
                        active: {
                            active: true,
                            info: '',
                            invalid: false,
                        },
                    },
                    image: {
                        active: {
                            active: true,
                            info: '',
                            invalid: false,
                        },
                        ratio: {
                            ratio: 'rectangle',
                            info: '',
                            invalid: false,
                        }
                    },
                },
            });
        },
        newImagecarouselMessage(index){
            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'imagecarousel',
                deleteable: true,
                message: {
                    altText: {
                        altText: '',
                        info: '',
                        invalid: false,
                    },
                    columns: {
                        info: '',
                        invalid: false,
                        columns:[
                            {
                                id: 1,
                                image: this.defaultData.image,
                                actions: [
                                    $.extend(true, {id: 1}, this.defaultData.action),
                                ],
                                // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
                            }
                        ],
                    },
                },
            });
        },
        newQuickreplyMessage(index){
            this.hasQuickReply = true;

            this.currentId ++;
            this.form.node.messages.messages.splice(index, 0, {
                id: this.node.id+'-'+this.currentId,
                type: 'quickreply',
                deleteable: true,
                message: {
                    items: {
                        info: '',
                        invalid: false,
                        items:[
                            {
                                id: 1,
                                image: this.defaultData.image,
                                actions: [
                                    $.extend(true, {id: 1}, this.defaultData.action),
                                ],
                                // actionType: $.extend(true, {currentButtonId: null}, this.defaultData.actionType),
                            }
                        ],
                    },
                },
            });
        }
    },
    created(){
        this.setNodeWidth();
    },
    watch: {
        edittingNodesId(newValue){
            this.editting = newValue.includes(this.node.id)
        }
    }
});
</script>
@endpush
