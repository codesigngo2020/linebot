{{-- Normal messages --}}

@foreach(['text', 'image', 'video', 'imagemap', 'buttons', 'confirm', 'carousel', 'imagecarousel', 'quickreply'] as $type)
@include('components.forms.solution._4.basic.message.message.messages.normal.'.$type)
@endforeach

@if(isset($template) && $template)
@foreach(['text', 'image', 'imagemap', 'buttons', 'confirm', 'carousel', 'imagecarousel', 'quickreply'] as $type)
@include('components.forms.solution._4.basic.message.template.messages.normal.'.$type)
@endforeach
@endif



{{-- Advanced messages --}}

@if(isset($template) && $template)
@foreach(['coupon'] as $type)
@include('components.forms.solution._4.basic.message.template.messages.advanced.'.$type)
@endforeach
@endif



{{-- Actions --}}

@foreach(['message', 'keyword', 'uri', 'liff', 'script', 'richmenu'] as $type)
@include('components.forms.solution._4.basic.message.message.actions.'.$type)
@endforeach

@if(isset($template) && $template)
@foreach(['message', 'keyword', 'uri', 'liff', 'richmenu'] as $type)
@include('components.forms.solution._4.basic.message.template.actions.'.$type)
@endforeach
@endif
