{{-- 開啟腳本 --}}

@push('js')

<script type="text/x-template" id="--script-action">
    <div>
        <div class="form-group">
            <label for="script">選擇腳本<span class="form-required">*</span><a v-if="form.script.value" class="text-muted ml-2" :href="getScriptUrl(form.script.value.id)" target="_blank">（預覽腳本）</a></label>
            <vue-multiselect class="hidden-arrow" v-model="form.script.value" track-by="value" label="name" placeholder="搜尋腳本名稱" open-direction="bottom" :options="script.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="false" :loading="script.isLoading" :internal-search="false" :options-limit="10" :limit-text="scriptslimitText" :max-height="600" :show-no-results="false" :hide-selected="true" @search-change="getScripts">
                <span slot="noResult">沒有相符的搜尋結果。</span>
            </vue-multiselect>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.script.info }}</strong></span>
        </div>
    </div>
</script>

<script>
Vue.component('script-action', {
    template: '#--script-action',
    props: ['id', 'action', 'columnIndex'],
    data: function(){
        return {
            actionData: null,
            form: this.action,
            script: {
                isLoading: false,
                options: [],
            }
        };
    },
    methods: {
        getScriptUrl(id){
            return route('solution.4.deployment.scripts.show', {
                deployment: args.deployment,
                scriptId: id,
            });
        },
        scriptslimitText(script){
            return `and ${script} other scripts`;
        },
        getScripts(query){
            this.script.isLoading = true;
            axios
            .get(route('solution.4.deployment.scripts.queryForSelect', {
                deployment: args.deployment,
                q: query,
            }))
            .then(response => {
                this.script.options = response.data;
                this.script.isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this.script.options = [];
                this.script.isLoading = false;
                console.log(error);
            });
        },
    },
    created(){
        this.getScripts('');
    },
    updated(){
        this.$emit('updateActionData', this.form, this.columnIndex);
    },
    watch: {
        action(newValue){
            this.$set(this, 'form', newValue);
        },
    }
});
</script>

@endpush
