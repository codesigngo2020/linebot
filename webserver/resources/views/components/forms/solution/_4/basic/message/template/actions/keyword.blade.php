{{-- Liff action Liff App 開啟網址 --}}

@push('js')

<script type="text/x-template" id="--template-keyword-action">
    <div class="keyword-action">
        <div class="form-group mb-3">
            <label for="actionType">選擇關鍵字<a v-if="form.keyword.value" class="text-muted ml-2" :href="getKeywordUrl(form.keyword.value.id)" target="_blank">（預覽關鍵字）</a></label>
            <vue-multiselect :class="[{'is-invalid' : form.keyword.invalid}, 'hidden-arrow multiselect-sm']" v-model="form.keyword.value" track-by="value" label="name" placeholder="搜尋關鍵字名稱" open-direction="bottom" :options="keyword.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="false" :loading="keyword.isLoading" :internal-search="false" :options-limit="10" :limit-text="keywordslimitText" :max-height="600" :show-no-results="false" :hide-selected="true" @search-change="getKeywords" :disabled="form.text.text != ''">
                <span slot="noResult">沒有相符的搜尋結果。</span>
            </vue-multiselect>

            <span class="invalid-feedback" role="alert"><strong>@{{ form.keyword.info }}</strong></span>
        </div>
        <div class="form-group mb-3">
            <label for="text">關鍵字名稱</label>
            <div :class="[{'is-invalid' : form.text.invalid}, 'i-form-control card mb-0 w-auto']">
                <div :class="[{disabled : form.keyword.value}, 'card-body text-justify p-2']">
                    <div class="texarea-not-input position-relative">
                        <pre ref="inputPre"></pre>
                        <textarea class="not-input bg-transparent" placeholder="若關鍵字尚未建立，您可以選擇在此輸入。若後續未建立相對應的關鍵字，將可能觸發自動回覆。" v-model="form.text.text" :disabled="form.keyword.value"></textarea>
                    </div>
                </div>
            </div>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.text.info }}</strong></span>
        </div>
    </div>
</script>

<script>
Vue.component('template-keyword-action', {
    template: '#--template-keyword-action',
    props: ['id', 'action', 'columnIndex'],
    data: function(){
        return {
            actionData: null,
            form: this.action,
            keyword: {
                isLoading: false,
                options: [],
            }
        };
    },
    methods: {
        getKeywordUrl(id){
            return route('solution.4.deployment.keywords.show', {
                deployment: args.deployment,
                keywordId: id,
            });
        },
        keywordslimitText(keyword){
            return `and ${keyword} other keywords`;
        },
        getKeywords(query){
            this.keyword.isLoading = true;
            axios
            .get(route('solution.4.deployment.keywords.queryForSelect', {
                deployment: args.deployment,
                q: query,
            }))
            .then(response => {
                this.keyword.options = response.data;
                this.keyword.isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this.keyword.options = [];
                this.keyword.isLoading = false;
                console.log(error);
            });
        },
    },
    created(){
        this.getKeywords('');
    },
    updated(){
        this.$emit('updateActionData', this.form, this.columnIndex);
    },
    watch: {
        action(newValue){
            this.$set(this, 'form', newValue);
        },
        'form.keyword.value': function(newValue){
            if(newValue) this.form.text.text = '';
        },
        'form.text.text': function(newValue){
            if(newValue != '') this.form.keyword.value = null;
        }
    }
});
</script>

@endpush
