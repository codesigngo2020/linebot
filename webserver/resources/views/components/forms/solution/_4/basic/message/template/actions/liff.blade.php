{{-- Liff action Liff App 開啟網址 --}}

@push('js')

<script type="text/x-template" id="--template-liff-action">
    <div>
        <div class="form-group mb-3">
            <label for="actionType">Liff App 尺寸<span class="form-required">*</span></label>
            <vue-multiselect :class="[{'is-invalid' : form.size.invalid}, 'hidden-arrow multiselect-sm']" v-model="form.size.value" track-by="value" label="name" placeholder="請選擇尺寸" openDirection="bottom" :options="size.options" :searchable="false" :show-labels="false" :allow-empty="false"></vue-multiselect>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.size.info }}</strong></span>
        </div>
        <div class="form-group mb-3">
            <label for="text">開啟網址<span class="form-required">*</span></label>
            <input id="text" type="url" :class="[{'is-invalid' : form.uri.invalid}, 'form-control form-control-sm']" v-model="form.uri.uri">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.uri.info }}</strong></span>
        </div>
    </div>
</script>

<script>
Vue.component('template-liff-action', {
    template: '#--template-liff-action',
    props: ['id', 'action', 'columnIndex'],
    data: function(){
        return {
            actionData: null,
            form: this.action,
            size: {
                options: {!! $liffSizes->toJson() !!},
            }
        };
    },
    updated(){
        this.$emit('updateActionData', this.form, this.columnIndex);
    },
    watch: {
        action(newValue){
            this.$set(this, 'form', newValue);
        },
    }
});
</script>

@endpush
