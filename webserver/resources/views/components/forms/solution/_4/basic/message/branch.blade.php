@push('js')

{{-- 腳本分支 --}}

<script type="text/x-template" id="--branch">
    <ul class="tree-ul">
        <li v-for="node in filterThisLayerNodes()" :key="node.id" class="tree-li">
            <node :class="[{disabled : node.disabled}]" :currentNodeId="currentNodeId" :node="node" :edittingNodesId="edittingNodesId" :newMessageType="newMessageType" :actionTypes="actionTypes" :quickReplyActionTypes="quickReplyActionTypes" :actions="actions" :defaultData="defaultData" @editNode="editNode" @closeEditNode="closeEditNode" @insertNewNode="insertNewNode" @removeNode="removeNode"></node>
            <branch v-if="node.end - node.start != 1" :nodes="filterSubNodes(node.start, node.end)" :edittingNodesId="edittingNodesId" :currentNodeId="currentNodeId" :newMessageType="newMessageType" :actionTypes="actionTypes" :quickReplyActionTypes="quickReplyActionTypes" :actions="actions" :defaultData="defaultData" @editNode="editNode" @closeEditNode="closeEditNode" @insertNewNode="insertNewNode" @removeNode="removeNode"></branch>
        </li>
    </ul>
</script>

<script>
Vue.component('branch', {
    template: '#--branch',
    props: ['nodes', 'edittingNodesId', 'currentNodeId', 'newMessageType', 'actionTypes' ,'quickReplyActionTypes', 'actions', 'defaultData'],
    data(){
        return {
            form: {
                script: this.script,
            },
        }
    },
    methods: {
        getMinStartOfNodes(){
            return this.nodes.reduce((min, node) => Math.min(min, node.start), 9999);
        },
        getMaxEndOfNodes(){
            return this.nodes.reduce((max, node) => Math.max(max, node.end), 0);
        },
        filterThisLayerNodes(){
            thisLayerNodes = [];
            start = this.getMinStartOfNodes() - 1;
            end = this.getMaxEndOfNodes();

            do{
                node = this.nodes.find(node => node.start == start + 1);
                thisLayerNodes.push(node);
                start = node.end;
            }while(end > node.end)

            return thisLayerNodes;
        },
        filterSubNodes(start, end){
            return this.nodes.filter(function(node){
                return node.start > start && node.end < end;
            });
        },
        editNode(id){
            this.$emit('editNode', id);
        },
        closeEditNode(id){
            this.$emit('closeEditNode', id);
        },
        insertNewNode(parentEnd){
            this.$emit('insertNewNode', parentEnd);
        },
        removeNode(id){
            this.$emit('removeNode', id);
        },
        // updateNodeData(id, node){
        //     this.$emit('updateNodeData', id, node);
        // }
    }
});
</script>

@endpush
