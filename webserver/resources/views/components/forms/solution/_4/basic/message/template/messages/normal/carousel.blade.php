{{-- Carousel message --}}

@push('js')

<script type="text/x-template" id="--template-carousel-message">
    <div>
        <div class="d-flex message carousel-message">
            <div class="buttons mr-1">
                <div v-if="messagesCount > 1" class="text-muted text-danger-hover cursor-pointer" @click="remove">
                    <v-svg src="{{ asset('assets/image/svg/light/trash-alt.svg') }}" width="12" height="12"></v-svg>
                </div>
            </div>
            <div class="w-100">
                <div class="type text-muted pl-1 mb-1">Carousel message</div>

                <div class="alert alert-dismissible fade show alert-light text-justify mt-2 mb-2" role="alert">
                    「文字說明內容」基本上限為60字元；若擇一去除「圖片」或「標題」，可增加至120字元
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="content d-flex flex-nowrap row ml-0 pt-2 mt-n2 overflow-auto w-100">
                    <div v-for="(column, columnIndex) in form.columns.columns" :key="column.id" class="d-flex flex-nowrap row col-auto">
                        <div class="position-relative pr-4">

                            {{-- 新增/移除 column --}}
                            <div class="position-absolute right mr-1">
                                <div :class="[{'d-none' : form.columns.columns.length == 1}, 'text-muted mr-1 mb-2 cursor-pointer text-danger-hover']" @click.stop="removeColumn(columnIndex)">
                                    <v-svg src="{{ asset('assets/image/svg/light/minus-circle.svg') }}" width="16" height="16"></v-svg>
                                </div>
                                <div :class="[{'d-none' : form.columns.columns.length >= 10}, 'text-muted mr-1 cursor-pointer text-primary-hover']" @click.stop="newColumn(columnIndex)">
                                    <v-svg src="{{ asset('assets/image/svg/light/plus-circle.svg') }}" width="16" height="16"></v-svg>
                                </div>
                            </div>

                            {{-- 按鈕訊息 --}}
                            <div :class="[{'is-invalid' : form.columns.invalid || column.image.invalid || column.title.invalid || column.text.invalid}, 'i-form-control card mb-0']">

                                {{-- 圖片上傳 --}}
                                <div :class="[form.image.ratio.ratio, 'image-layout position-relative']">

                                    <div :class="[{'i-dz-max-files-reached' : column.image.src != ''}, 'position-absolute top w-100 h-100 overflow-hidden border-radius-0375 idropzone dropzone-single mb-3']" :ref="'image-'+column.id">
                                        <div :id="'dz-preview-'+index+'-'+column.id" class="d-flex align-items-center overflow-hidden dz-preview dz-preview-single">
                                            <div class="dz-preview-cover">
                                                <img :class="[{ 'd-none' : column.image.src == '' }, 'dz-preview-img rounded-0']" :src="column.image.src" :alt="column.image.alt"/>
                                            </div>
                                        </div>
                                        <div class="dz-default dz-message">
                                            <span class="mb-3">
                                                <v-svg src="{{ asset('assets/image/svg/light/image.svg') }}" width="20" height="20"></v-svg>
                                            </span>
                                            <span>Drop image here to upload</span>
                                        </div>
                                    </div>

                                    {{-- 圖片尺寸 --}}
                                    <div v-if="columnIndex == 0 && form.image.active.active" class="position-absolute top right z-index-999 d-flex align-items-center mt-2 mr-2">
                                        <div :class="[form.image.ratio.ratio == 'rectangle' ? 'text-primary' : 'text-muted','d-flex cursor-pointer']" @click="setRatio('rectangle')">
                                            <v-svg src="{{ asset('assets/image/svg/light/rectangle-landscape.svg') }}" width="18" height="18"></v-svg>
                                        </div>

                                        <div class="text-muted ml-3 mr-3">/</div>

                                        <div :class="[form.image.ratio.ratio == 'square' ? 'text-primary' : 'text-muted', 'd-flex cursor-pointer']" @click="setRatio('square')">
                                            <v-svg src="{{ asset('assets/image/svg/light/square.svg') }}" width="18" height="18"></v-svg>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body text-justify pt-2 pb-0 pl-3 pr-3">
                                    {{-- title --}}
                                    <input v-if="form.title.active.active" class="title not-input mb-2" placeholder="請輸入訊息標題" v-model="column.title.title">

                                    {{-- text --}}
                                    <div class="texarea-not-input text position-relative">
                                        <pre :ref="'inputPre'+column.id"></pre>
                                        <textarea class="not-input" placeholder="請輸入文字說明內容" v-model="column.text.text" @input="textChange(column.id)"></textarea>
                                    </div>
                                </div>

                                <hr class="m-0">

                                <div class="actions mt-2 mb-2">

                                    <div v-for="(action, index) in column.actions" :key="action.id" class="position-relative d-flex align-items-center cursor-pointer" @click="selectButton(columnIndex, action.id)">
                                        <input class="not-input text-center pt-2 pb-2 pl-3 pr-3" placeholder="編輯名稱及按鈕行為" v-model="action.action.label.label">
                                        <div class="position-absolute left d-flex mr-1 bg-white">
                                            <div :class="[action.invalid ? 'text-danger' : 'text-muted', 'ml-2']">@{{ action.id }}</div>
                                        </div>
                                        <div class="position-absolute right d-flex mr-2 pl-1 bg-white">
                                            <div :class="[{'d-none' : column.actions.length == 1}, 'text-muted mr-1 cursor-pointer text-danger-hover']" @click.stop="removeButton(columnIndex, index)">
                                                <v-svg src="{{ asset('assets/image/svg/light/minus-circle.svg') }}" width="16" height="16"></v-svg>
                                            </div>
                                            <div :class="[{'d-none' : column.actions.length >= 3}, 'text-muted cursor-pointer text-primary-hover']" @click.stop="newButton(columnIndex, index)">
                                                <v-svg src="{{ asset('assets/image/svg/light/plus-circle.svg') }}" width="16" height="16"></v-svg>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <span v-if="form.columns.invalid" class="invalid-feedback" role="alert"><strong>@{{ form.columns.info }}</strong></span>
                            <span v-for="input in ['image', 'title', 'text']" v-if="column[input].invalid" class="invalid-feedback" role="alert"><strong>@{{ column[input].info }}</strong></span>
                            <span v-for="action in column.actions" v-if="action.action.label.invalid" class="invalid-feedback" role="alert"><strong>@{{ action.action.label.info }}</strong></span>

                        </div>

                        <div v-if="actionType.currentColumnId && actionType.currentColumnId == column.id && actionType.currentButtonId" class="col-300 min-height-200 m-0 pl-0 pr-4">
                            {{-- 按鈕行為 --}}
                            <div class="position-relative nopadding">
                                <button type="button" class="close-edit-block position-absolute top right d-flex align-items-center btn btn-outline-danger btn-sm" @click="closeEditBlock(columnIndex)">
                                    <span class="d-flex align-items-center">
                                        <v-svg src="{{ asset('assets/image/svg/light/times.svg') }}" width="14" height="14"></v-svg>
                                    </span>
                                    <span>收合視窗</span>
                                </button>
                                <div class="form-group mb-3">
                                    <label for="actionType">按鈕行為<span class="form-required">*</span><span>@{{ '（正在編輯按鈕 '+actionType.currentButtonId+'）' }}</span></label>
                                    <vue-multiselect :class="[{'is-invalid' : actionType.invalid}, 'hidden-arrow multiselect-sm']" v-model="actionType.value" track-by="value" label="name" placeholder="請選擇行為" openDirection="bottom" :options="actionTypes" :searchable="false" :show-labels="false" :allow-empty="false" :disabled="!actionType.currentButtonId" @input="changeActionType(columnIndex)"></vue-multiselect>
                                    <span class="invalid-feedback" role="alert"><strong>@{{ actionType.info }}</strong></span>
                                </div>

                                <component v-if="actionType.currentButtonId && actionType.value && actionType.value.show" :is="'template-'+actionType.value.value+'-action'" :id="actionType.currentButtonId" :action="actionData" :columnIndex="columnIndex" @updateActionData="updateActionData"></component>
                            </div>
                        </div>
                    </div>



                    <div class="col-300 m-0 pb-3 pl-0 pr-0">
                        {{-- 替代文字 --}}
                        <div class="form-group mb-3">
                            <label for="altText">替代文字<span class="form-required">*</span><span>（訊息通知中顯示）</span></label>
                            <input id="altText" type="text" :class="[{'is-invalid' : form.altText.invalid}, 'form-control form-control-sm']" v-model="form.altText.altText">
                            <span class="invalid-feedback" role="alert"><strong>@{{ form.altText.info }}</strong></span>
                        </div>

                        <hr class="mt-3 mb-3">

                        {{-- 標題設定 --}}
                        <div class="form-group mb-3">
                            <div class="custom-control custom-switch custom-switch-sm pt-2">
                                <input type="checkbox" class="custom-control-input" :id="'title-'+node.id+'-'+index" v-model="form.title.active.active">
                                <label :class="[{ 'text-muted' : !form.title.active.active }, 'custom-control-label']" :for="'title-'+node.id+'-'+index">@{{ form.title.active.active ? '訊息方包含「標題」' : '訊息不方包含「標題」' }}</label>
                            </div>

                            <span class="invalid-feedback" role="alert"><strong>@{{ form.title.active.info }}</strong></span>
                        </div>

                        {{-- 圖片設定 --}}
                        <div class="form-group mb-3">
                            <div class="custom-control custom-switch custom-switch-sm pt-2">
                                <input type="checkbox" class="custom-control-input" :id="'image-'+node.id+'-'+index" v-model="form.image.active.active">
                                <label :class="[{ 'text-muted' : !form.image.active.active }, 'custom-control-label']" :for="'image-'+node.id+'-'+index">@{{ form.image.active.active ? '訊息上方包含「一張圖片」' : '訊息上方不包含「圖片」' }}</label>
                            </div>

                            <span class="invalid-feedback" role="alert"><strong>@{{ form.image.active.info }}</strong></span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="droppable" @dragover.prevent @dragenter.stop.prevent="isDropzoneActive() ? dragenter($event) : null" @dragleave.stop.prevent="isDropzoneActive() ? dragleave($event) : null" @drop.stop.prevent="isDropzoneActive() ? drop(index + 1) : null">
            <div></div>
        </div>
    </div>
</script>

<script>
Vue.component('template-carousel-message', {
    template: '#--template-carousel-message',
    props: ['index', 'currentNodeId', 'node', 'messagesCount', 'message', 'actions', 'defaultAction', 'actionTypes', 'newMessageType', 'hasQuickReply'],
    data(){
        return {
            counter: 0,
            currentColumnId: this.getCurrentColumnId(),
            currentButtonId: this.getCurrentButtonId(),

            gottenColumnIndex: 0,
            gottenButtonIndex: 0,
            actionData: null,

            form: this.message,

            default: {
                action: this.defaultAction,
            },
            actionType: {
                currentColumnId: null,
                currentButtonId: null,
                value: null,
                info: '',
                invalid: false,
            }
        }
    },
    methods: {
        remove(){
            this.$emit('removeMessage', this.index);
        },
        dragenter(event){
            this.counter ++;
            $(event.target).addClass('dragover');
        },
        dragleave(event){
            this.counter --;
            if(this.counter === 0) $(event.target).removeClass('dragover');
        },
        drop(index){
            this.counter = 0;
            this.$emit('newMessage', index);
        },
        isDropzoneActive(){
            if(this.newMessageType == 'quickreply-message' && (this.index == this.messagesCount - 1 && !this.hasQuickReply)){
                return true;
            }else if(this.newMessageType != 'quickreply-message' && this.messagesCount < 5){
                return true;
            }
            return false;
        },
        getCurrentColumnId(){
            return this.message.columns.columns.reduce((max, column) => Math.max(max, column.id), 1);
        },
        getCurrentButtonId(){
            return this.message.columns.columns.reduce(function(max, column){
                return Math.max(max, column.actions.reduce((max, action) => Math.max(max, action.id), 1));
            }, 1);
        },
        clearTitle(){
            this.form.columns.columns.forEach(function(column, columnIndex){
                column.title.title = '';
            });
        },
        clearImageData(){
            this.form.columns.columns.forEach(function(column, columnIndex){
                column.image.src = '';
                column.image.alt = '';
                column.image.file = null;
                column.image.info = '';
                column.image.invalid = false;
            });
        },
        getColumn(columnId){
            var vm = this;
            this.form.columns.columns.forEach(function(column, columnIndex){
                if(column.id && column.id == columnId){
                    vm.gottenColumnIndex = columnIndex;
                    return false;
                }
            });
        },
        getButton(buttonId, columnIndex){
            var vm = this;
            if(columnIndex){
                this.form.columns.columns[columnIndex].actions.forEach(function(action, actionIndex){
                    if(action.id && action.id == buttonId){
                        vm.gottenColumnIndex = columnIndex;
                        vm.gottenButtonIndex = actionIndex;
                        return false;
                    }
                });
            }else{
                this.form.columns.columns.forEach(function(column, columnIndex){
                    column.actions.forEach(function(action, actionIndex){
                        if(action.id && action.id == buttonId){
                            vm.gottenColumnIndex = columnIndex;
                            vm.gottenButtonIndex = actionIndex;
                            return false;
                        }
                    });
                });
            }
        },
        removeColumn(columnIndex){
            var vm = this;
            // this.form.columns.columns[columnIndex].actions.forEach(function(action){
            //     if(action.type.value == 'next'){
            //         vm.$emit('removeNode', action.action.nextId.nextId);
            //     }
            // });

            this.form.columns.columns.splice(columnIndex, 1);
        },
        removeButton(columnIndex, index){
            buttonId = this.form.columns.columns[columnIndex].actions[index].id;
            this.gottenColumnIndex = columnIndex;
            this.gottenButtonIndex = index;

            // if(this.form.columns.columns[columnIndex].actions[index].type.value == 'next'){
            //     this.$emit('removeNode', this.form.columns.columns[columnIndex].actions[index].action.nextId.nextId);
            // }

            this.form.columns.columns[columnIndex].actions.splice(index, 1);
            if(this.actionType.currentButtonId == buttonId) this.saveAndClearAction(null, true);
        },
        newColumn(columnIndex){
            if(this.form.columns.columns.length < 10){
                this.currentColumnId ++;
                this.currentButtonId ++;

                this.form.columns.columns.splice(columnIndex + 1, 0, {
                    id: this.currentColumnId,
                    title: {
                        title: '',
                        info: '',
                        invalid: false,
                    },
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    },
                    image: {
                        src: '',
                        alt: '',
                        file: null,
                        info: '',
                        invalid: false
                    },
                    actions: [
                        $.extend(true, {id: this.currentButtonId}, this.default.action),
                    ],
                    // actionType: $.extend(true, {currentButtonId: null}, this.default.actionType),
                });
                var vm = this;
                this.$nextTick(function(){
                    vm.initiateImageDropzone(vm.currentColumnId);
                });
            }
        },
        newButton(columnIndex, index){
            if(this.form.columns.columns[columnIndex].actions.length < 3){
                this.currentButtonId ++;
                this.form.columns.columns[columnIndex].actions.splice(index + 1, 0, $.extend(true, {
                    id: this.currentButtonId
                }, this.default.action));
            }
        },
        selectButton(columnIndex, id){
            // if(this.form.columns.columns[columnIndex].actionType.currentButtonId != id){
            this.saveAndClearAction(id, false);
            this.refreshActionData(id);
            // }
        },
        saveAndClearAction(newId, removed){
            if(removed){
                // 原本的區域已刪除
                this.actionType.value = null;
            }else{
                if(this.actionType.currentButtonId){
                    // 原本的區域沒刪除，儲存類型
                    this.getButton(this.actionType.currentButtonId);
                    this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].type = this.actionType.value;
                    this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].selected = false;
                }
                this.actionType.currentColumnId = null;
                this.actionType.currentButtonId = null;
            }

            if(newId){
                this.getButton(newId);

                // 新button的資料
                newAreaData = this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex];

                this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].invalid = false;
                this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].selected = true;
                this.actionType.value = newAreaData.type;

                this.actionType.currentColumnId = this.form.columns.columns[this.gottenColumnIndex].id;
                this.actionType.currentButtonId = newId;
            }else{
                this.actionType.currentColumnId = null;
                this.actionType.currentButtonId = null;
            }
        },
        updateActionData(data, columnIndex){
            this.getButton(this.actionType.currentButtonId, columnIndex);
            this.$set(this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex], 'action', data);
        },
        refreshActionData(id){
            this.getButton(id);
            this.actionData = this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].action;
        },
        closeEditBlock(){
            this.saveAndClearAction(null, false);
        },
        initiateImageDropzone(columnId){
            var vm = this;
            this['dropzone-' + columnId] = new Dropzone(vm.$refs['image-' + columnId][0], {
                url: "#",
                autoProcessQueue: false,
                maxFiles: 1,
                acceptedFiles: ".jpeg,.jpg,.png,.JPEG,.JPG,.PNG",
                thumbnailWidth: null,
                thumbnailHeight: null,
                addedfile: function(file){
                    vm.getColumn(columnId);
                    vm.form.columns.columns[vm.gottenColumnIndex].image.info = '';
                    vm.form.columns.columns[vm.gottenColumnIndex].image.invalid = false;

                    var img = new Image();

                    img.src = window.URL.createObjectURL(file);
                    img.onload = function(){
                        var width = img.naturalWidth,
                        height = img.naturalHeight;

                        if(width > 1024 || height > 1024){
                            vm.$set(vm.form.columns.columns[vm.gottenColumnIndex].image, 'info', '圖片最大尺寸：1024 x 1024');
                            vm.$set(vm.form.columns.columns[vm.gottenColumnIndex].image, 'invalid', true);
                            return false;
                        }

                        if(vm.form.image.ratio.ratio == 'square'){
                            if(width != height){
                                vm.$set(vm.form.columns.columns[vm.gottenColumnIndex].image, 'info', '請上傳正方形圖片');
                                vm.$set(vm.form.columns.columns[vm.gottenColumnIndex].image, 'invalid', true);
                                return false;
                            }
                        }

                        vm.$set(vm.form.columns.columns[vm.gottenColumnIndex], 'image', {
                            src: window.URL.createObjectURL(file),
                            alt: file.name,
                            file: file,
                            info: '',
                            invalid: false,
                        });
                    };
                    return false;
                },
            });
        },
        changeActionType(columnIndex){
            this.gottenColumnIndex = columnIndex;
            this.getButton(this.actionType.currentButtonId, columnIndex);
            newValue = this.actionType.value;

            // if(newValue.value == 'next'){
            //     // 從不是next換到next
            //     this.$emit('insertNewNode', this.node.end);
            // }else if(this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].type.value == 'next'){
            //     // 從next換到不是next
            //     this.$emit('removeNode', this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].action.nextId.nextId);
            // }

            this.$set(this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex], 'type', newValue);

            // if(newValue.value == 'next'){
            //     this.$set(this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex], 'action', {
            //         label: this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].action.label,
            //         nextId: {
            //             nextId: this.currentNodeId + 1,
            //             info: '',
            //             invalid: false,
            //         }
            //     });
            // }else{
            this.$set(this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex], 'action', $.extend(true, {
                label: this.form.columns.columns[this.gottenColumnIndex].actions[this.gottenButtonIndex].action.label
            }, this.actions[newValue.value]));
            // }

            this.refreshActionData(this.actionType.currentButtonId);
        },
        textChange(columnId){
            column = this.form.columns.columns.find(column => column.id == columnId);
            this.$refs['inputPre'+columnId][0].innerHTML = column.text.text.replaceAll(/\r\n|\r|\n/g, '<br>')+'<br>';
        },
        setRatio(size){
            this.form.image.ratio.ratio = size;
        }
    },
    created(){
        this.closeEditBlock();
    },
    mounted(){
        var vm = this;
        this.form.columns.columns.forEach(function(column, columnIndex){
            vm.initiateImageDropzone(column.id);
        });
    },
    updated(){
        this.$emit('updateMessageData', this.index, this.form);
    },
    watch: {
        'form.title.active.active': function(newValue){
            if(!newValue) this.clearTitle();
        },
        'form.image.active.active': function(newValue){
            if(!newValue){
                this.clearImageData();

                this.form.image.ratio.ratio = '';
                this.form.image.ratio.info = '';
                this.form.image.ratio.invalid = false;
            }else{
                this.form.image.ratio.ratio = 'rectangle';
                this.form.image.ratio.info = '';
                this.form.image.ratio.invalid = false;
            }
        },
        'form.image.ratio.ratio': function(){
            this.clearImageData();
        }
    }
});
</script>

@endpush
