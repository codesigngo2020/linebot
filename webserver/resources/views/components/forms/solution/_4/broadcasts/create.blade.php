<div id="broadcast-form" class="mb-6" v-cloak>

    <div class="row">
        {{-- 推播訊息名稱 --}}
        <div class="form-group col-12 col-md-5">
            <label for="name">推播訊息名稱<span class="form-required">*</span></label>
            <input id="name" type="text" :class="[{'is-invalid' : form.name.invalid}, 'form-control']" v-model="form.name.name">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.name.info }}</strong></span>
        </div>

        {{-- 推播訊息描述 --}}
        <div class="form-group col-12 col-md-7">
            <label for="description">推播訊息描述<span class="form-required">*</span></label>
            <input id="description" type="text" :class="[{'is-invalid' : form.description.invalid}, 'form-control']" v-model="form.description.description">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.description.info }}</strong></span>
        </div>

    </div>

    <div class="row">
        {{-- 發送對象 --}}
        <div class="form-group col-12">
            <label for="target">發送對象<span class="form-required">*</span></label>
            <div :class="[{'is-invalid' : form.target.invalid}, 'i-form-control d-flex btn-group-toggle']">
                <label :class="[{active : form.target.target_type == 'all'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="target" value="all" v-model="form.target.target_type" :checked="form.target.target_type == 'all'" :disabled="disableBroadcast">
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/regular/users.svg') }}" width="20" height="20"></v-svg>
                    </span>
                    <span>全部好友</span>
                </label>

                <label :class="[{active : form.target.target_type == 'groups'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="target" value="groups" v-model="form.target.target_type" :checked="form.target.target_type == 'groups'">
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/regular/tags.svg') }}" width="20" height="20"></v-svg>
                    </span>
                    <span>好友群組</span>
                </label>

                <label :class="[{active : form.target.target_type == 'users'}, 'd-flex btn btn-white']">
                    <input type="radio" name="target" value="users" v-model="form.target.target_type" :checked="form.target.target_type == 'users'">
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/regular/user.svg') }}" width="20" height="16"></v-svg>
                    </span>
                    <span>特定好友</span>
                </label>

            </div>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.target.info }}</strong></span>
        </div>

    </div>

    {{-- 發送對象->好友群組 --}}
    <div v-if="form.target.target_type == 'groups'" class="row">
        <div class="form-group col-12">
            <label for="users-groups">選擇好友群組<span class="form-required">*</span></label>
            <vue-multiselect :class="[{'is-invalid' : form.usersGroups.invalid}, 'i-form-control hidden-arrow']" v-model="form.usersGroups.value" track-by="value" label="name" placeholder="搜尋好友名稱" open-direction="bottom" :options="usersGroups.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="true" :loading="usersGroups.isLoading" :internal-search="false" :options-limit="10" :limit-text="usersGroupsLimitText" :max-height="600" :show-no-results="false" :hide-selected="true" @search-change="getUsersGroups">
                <span slot="noResult">沒有相符的搜尋結果。</span>
            </vue-multiselect>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.usersGroups.info }}</strong></span>
        </div>
    </div>

    {{-- 發送對象->特定好友 --}}
    <div v-if="form.target.target_type == 'users'" class="row">
        <div class="form-group col-12">
            <label for="users">選擇好友<span class="form-required">*</span></label>
            <vue-multiselect :class="[{'is-invalid' : form.users.invalid}, 'i-form-control hidden-arrow']" v-model="form.users.value" track-by="value" label="name" placeholder="搜尋好友名稱" open-direction="bottom" :options="users.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="true" :loading="users.isLoading" :internal-search="false" :options-limit="10" :limit-text="usersLimitText" :max-height="600" :show-no-results="false" :hide-selected="true" @search-change="getUsers">
                <span slot="noResult">沒有相符的搜尋結果。</span>
            </vue-multiselect>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.users.info }}</strong></span>
        </div>
    </div>

    <div class="row">
        {{-- 發送時間 --}}
        <div class="form-group col-auto pr-0">
            <label for="timeType">發送時間<span class="form-required">*</span></label>
            <div :class="[{'is-invalid' : form.timeType.invalid}, 'i-form-control d-flex btn-group-toggle']">
                <label :class="[{active : form.timeType.timeType == 'now'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="timeType" value="now" v-model="form.timeType.timeType" :checked="form.timeType.timeType == 'now'">
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/light/paper-plane.svg') }}" width="20" height="18"></v-svg>
                    </span>
                    <span>立即發送</span>
                </label>

                <label :class="[{active : form.timeType.timeType == 'schedule'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="timeType" value="schedule" v-model="form.timeType.timeType" :checked="form.timeType.timeType == 'schedule'">
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/light/calendar-alt.svg') }}" width="20" height="19"></v-svg>
                    </span>
                    <span>排程發送</span>
                </label>
            </div>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.timeType.info }}</strong></span>
        </div>

        <div v-show="form.timeType.timeType == 'schedule'" :class="[{'d-flex' : form.timeType.timeType == 'schedule'}, 'flex-column align-items-end form-group with-fake-label col-4 pl-0']">
            <input id="datetime" type="text" :class="[{'is-invalid' : form.datetime.invalid}, 'form-control']" data-mask="ABCC-DC-EC FC:GC" placeholder="2019-12-25 23:59" ref="datetime">
            <span class="invalid-feedback" role="alert"><strong>@{{ form.datetime.info }}</strong></span>
        </div>

    </div>

    <hr class="mt-3 mb-5">

    <div class="row">
        {{-- 訊息設定 --}}
        <div class="form-group col-12">
            <label for="messageType">訊息設定<span class="form-required">*</span></label>

            <div v-if="form.messageType.messageType == 'pool'" class="alert alert-dismissible fade show alert-light text-justify font-size-0875 mb-3" role="alert">
                選擇「從訊息庫中複製」，可對複製的訊息進行更改，並不會影響原本訊息庫中的訊息
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div :class="[{'is-invalid' : form.messageType.invalid}, 'i-form-control d-flex btn-group-toggle']">
                <label :class="[{active : form.messageType.messageType == 'pool'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="messageType" value="pool" v-model="form.messageType.messageType" :checked="form.messageType.messageType == 'pool'">
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/light/box-open.svg') }}" width="21" height="21"></v-svg>
                    </span>
                    <span>從訊息庫中複製</span>
                </label>

                <label :class="[{active : form.messageType.messageType == 'template'}, 'd-flex btn btn-white mr-3']">
                    <input type="radio" name="messageType" value="template" v-model="form.messageType.messageType" :checked="form.messageType.messageType == 'template'">
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/regular/cubes.svg') }}" width="20" height="20"></v-svg>
                    </span>
                    <span>使用訊息模組</span>
                </label>

                <label :class="[{active : form.messageType.messageType == 'new'}, 'd-flex btn btn-white']">
                    <input type="radio" name="messageType" value="new" v-model="form.messageType.messageType" :checked="form.messageType.messageType == 'new'">
                    <span class="d-flex align-items-center mr-2">
                        <v-svg src="{{ asset('assets/image/svg/light/comment-alt-lines.svg') }}" width="20" height="18"></v-svg>
                    </span>
                    <span>新建訊息</span>
                </label>

            </div>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.messageType.info }}</strong></span>
        </div>
    </div>

    {{-- 從訊息庫中複製／使用訊息模組->選擇訊息 --}}
    <div v-if="form.messageType.messageType == 'pool' || form.messageType.messageType == 'template'" class="row">
        {{-- 選擇訊息／使用訊息模組 --}}
        <div class="form-group col-12 col-md-6">
            <label for="actionType">@{{ form.messageType.messageType == 'pool' ? '選擇訊息' : '選擇訊息模組' }}<span class="form-required">*</span><a v-if="form.messageType.messageType == 'template' && form.message.value" class="text-muted ml-2" :href="getMessageUrl(form.message.value.id)" target="_blank">（預覽訊息模組）</a></label>
            <vue-multiselect :class="[{'is-invalid' : form.message.invalid}, 'i-form-control hidden-arrow']" v-model="form.message.value" track-by="value" label="name" :placeholder="form.messageType.messageType == 'pool' ? '搜尋訊息名稱' : '搜尋訊息模組名稱'" open-direction="bottom" :options="message.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="false" :loading="message.isLoading" :internal-search="false" :options-limit="10" :limit-text="messageslimitText" :max-height="400" :show-no-results="false" :hide-selected="true" @search-change="getMessages">
                <span slot="noResult">沒有相符的搜尋結果。</span>
            </vue-multiselect>
            <span class="invalid-feedback" role="alert"><strong>@{{ form.message.info }}</strong></span>
        </div>
    </div>

    {{-- 建立訊息 --}}
    @include('components.forms.solution._4.basic.message.new-message')
    @include('components.forms.solution._4.basic.message.message-types')
    @include('components.forms.solution._4.basic.message.messages')

    <new-message v-if="form.messageType.messageType == 'new' || (form.messageType.messageType == 'pool' && form.message.value)" :isloading="isLoading" :newmessagetype="newMessageType" :messages="form.messages.messages" :hasquickreply="hasQuickReply" @updateonlypush="updateOnlyPush" @updateisloading="updateIsLoading" @updatemessagesdata="updateMessagesData" @updatehasquickreply="updateHasQuickReply"></new-message>

    <message-types v-if="form.messageType.messageType == 'new' || (form.messageType.messageType == 'pool' && form.message.value)" formtype="message" :hasquickreply="hasQuickReply" @updatenewmessagetype="updateNewMessageType"></message-types>

    <hr class="mt-5 mb-5">

    {{ Form::button('新增推播訊息', [':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, "btn btn-block btn-primary"]', '@click'=>'submit']) }}

</div>
