@push('plugin-css')
{{-- multiselect CSS --}}
<link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
@endpush

<div id="deploy-form">
    <ul id="form-nav-tabs" class="nav nav-tabs nav-overflow mb-4" role="tablist">
        @foreach(['訂閱方案', 'LINE 設定'] as $key => $nav)
        <li class="nav-item">
            <a class="nav-link {{ $key == 0 ? 'active' : '' }}" data-toggle="tab" href="#nav-form-{{ $key + 1 }}" role="tab" aria-controls="nav-form-{{ $key + 1 }}" aria-selected="{{ $key == 0 ? true : false }}" ref="nav-link-{{ $key + 1 }}">{{ $nav }}</a>
        </li>
        @endforeach
    </ul>


    {{-- Form --}}
    {{ Form::open(['method'=>'post', 'files' => 'true', 'class' => 'mb-6']) }}
    <div class="tab-content">
        @for($i = 1; $i <= 2; $i ++)
        @include('components.forms.solution._4.deploy-partials.form'.$i, ['action' => 'deploy'])

        @if($i == 1)
        <nav-form-{{ $i }} @update-form-data="updateFormData" ref="form{{ $i }}" :form="form[1]"></nav-form-{{ $i }}>
        @elseif($i == 2)
        <nav-form-{{ $i }} @update-form-data="updateFormData" ref="form{{ $i }}" :_form="form"></nav-form-{{ $i }}>
        @else
        <!-- <nav-form-{{ $i }} @update-form-data="updateFormData" ref="form{{ $i }}"></nav-form-{{ $i }}> -->
        @endif

        @endfor
    </div>
    {{ Form::close() }}
</div>

@push('plugin-js')
<script src="{{ asset('assets/plugins/dropzone/dropzone.min.js') }}"></script>
@endpush

@push('js')
<script>
var args = {
    _token: "{{ csrf_token() }}",
    form: {
        1: {
            plan: {{ $plans[0]->id }},
        }
    }
}
</script>
@endpush
