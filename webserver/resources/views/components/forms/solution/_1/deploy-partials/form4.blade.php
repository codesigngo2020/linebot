@push('js')
<script type="text/x-template" id="--nav-form-4">
  <div class="tab-pane fade" id="nav-form-4" role="tabpanel" aria-labelledby="nav-form-4-tab">
    <div class="deployment-info mb-4">
      <div class="info-item">提交部署。</div>
    </div>

    {{ Form::button('提交部署', ['id'=>'deploymeny-submit', ':class'=>'[{"progress-bar-striped progress-bar-animated" : submitting}, "btn btn-block btn-primary"]', '@click'=>'submit']) }}
  </div>
</script>

<script>
Vue.component('nav-form-4', {
  template: '#--nav-form-4',
  props: ['form'],
  data(){
    return {
      submitting: false,
    }
  },
  methods: {
    AllInvalidToFalse(obj){
      for(key in obj){
        if(key == 'invalid'){
          obj[key] = false;
          obj['info'] = '';
        }else if(typeof(obj[key]) === 'object'){
          this.AllInvalidToFalse(obj[key]);
        }
      }
    },
    submit(){
      this.submitting = true;
      this.AllInvalidToFalse(this.form);

      axios
      .post(route('solution.1.store'), {
        _token: args._token,
        form: this.form,
      })
      .then(response => {
        console.log(response);
        if(response.data.status == 'success') window.open(route('solution.1.deployment.show', {
          deployment: response.data.deployment,
        }), '_self');

        this.submitting = false;
      }).catch(error => {
        var errors = error.response.data.errors;
        console.log(errors)
        this.submitting = false;
        var invalid_form = null;
        for(error in errors){
          var invalid_form_index = error.match(/^form\.([0-9]+)\./)[1];
          if(!invalid_form || invalid_form_index < invalid_form) invalid_form = invalid_form_index;

          // 找出最後一個「.」後的字串
          var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
          // 如果是value，表示是select，再往前找一個value
          if(last == 'value'){
            select_last = error.match(/\.(value\.value)$/);
            // 如果符合「.value.value」，表示是多層value
            if(select_last) last = select_last.slice(-1)[0];
          }

          // 替換數字成以[]包覆表示
          error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

          info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
          invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');

          this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
          this.$set(eval('this.'+invalid_ref), 'invalid', true);
        }
        for(var i = 1; i <= 4; i ++){
          var navLink = eval('deploy_form.$refs[\'nav-link-'+i+'\']');
          if(i == invalid_form) $(navLink).tab('show');
        }
      });

      this.$emit('update-form-data', null, this.form);
    }
  }
});
</script>
@endpush
