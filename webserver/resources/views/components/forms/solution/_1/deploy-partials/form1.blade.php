@push('js')
<script type="text/x-template" id="--nav-form-1">
  <div class="tab-pane fade show active" id="nav-form-1" role="tabpanel" aria-labelledby="nav-form-1-tab">
    <div class="deployment-info mb-4">
      <div class="info-item">您即將部署一個新的<strong>Youtube 爬蟲方案</strong>，請點擊下方按鈕，下載並安裝Wordpress外掛，複製外掛中的金鑰貼至下方欄位，點擊<strong>測試連線</strong>確認成功連結至您的網站。</div>
      <a class="btn btn-block btn-secondary col-12 col-sm-4 col-md-3" href="{{ route('file.download', $solution->files()->whereHas('categories', function($q){
        $q->where('id', 4);
      })->first()) }}" target="_blank">下載外掛</a>
    </div>

    {{-- 部署名稱 --}}
    <div class="form-group">
      {!! Form::requiredLabel('name', '部署名稱') !!}
      {{ Form::text('name', old('name'), [
      'id'=>'name',
      ':class'=>'[{"is-invalid" : form.name.invalid}, "form-control"]',
      'placeholder'=>'My first crawler', 'v-model'=>'form.name.name'
      ]) }}
      {!! Form::ajaxInvalidFeedback('form.name.info') !!}
    </div>

    {{-- 網域名稱 --}}
    <div class="form-group">
      {!! Form::requiredLabel('domain', '網域名稱') !!}
      {{ Form::text('domain', old('domain'), [
      'id'=>'domain',
      'placeholder'=>'domain.com or 199.27.25.0/24',
      'v-model'=>'form.domain.domain',
      ':class'=>'[{"is-invalid" : form.domain.invalid}, "form-control"]',
      ':disabled' => 'form.is_success',
      ]) }}
      {!! Form::ajaxInvalidFeedback('form.domain.info') !!}
    </div>

    {{-- 金鑰 --}}
    <div class="form-group">
      {!! Form::requiredLabel('key', '金鑰') !!}
      {{ Form::text('key', old('key'), [
      'id'=>'key',
      'placeholder'=>'在Wordpress外掛上產生的金鑰',
      'v-model'=>'form.key.key',
      ':class'=>'[{"is-invalid" : form.key.invalid}, "form-control"]',
      ':disabled' => 'form.is_success',
      ]) }}
      {!! Form::ajaxInvalidFeedback('form.key.info') !!}
    </div>

    {{-- Divider --}}
    <hr class="mt-5 mb-5">

    {{-- Buttons --}}
    {!! Form::button('@{{ form.is_success ? "成功連結 Wordpress" : "測試連線" }}', [
    'id'=>'domain-test',
    '@click' => 'testDomain', ':class'=>'[form.is_success ? "btn-success" : "btn-warning", {"progress-bar-striped progress-bar-animated" : form.testing}, "btn btn-block"]',
    ':disabled' => 'form.is_success',
    'v-cloak' => 'true'
    ]) !!}
  </div>
</script>

<script>
Vue.component('nav-form-1', {
  template: '#--nav-form-1',
  data(){
    return {
      form: {
        is_success: false,
        name: {
          name: '',
          info: '',
          invalid: false,
        },
        domain: {
          domain: '',
          info: '',
          invalid: false,
        },
        key: {
          key: '',
          info: '',
          invalid: false,
        },
        testing: false,
      }
    }
  },
  methods: {
    testDomain(){
      this.form.testing = true;
      this.form.domain.info = '';
      this.form.key.info = '';
      this.form.domain.invalid = false;
      this.form.key.invalid = false;
      axios
      .post(route('solution.1.testDomain'), {
        _token: args._token,
        domain: this.form.domain.domain,
        key: this.form.key.key,
      })
      .then(response => {
        if(response.data == 'associated'){
          this.form.testing = false;
          this.form.is_success = true;
          this.form.domain.info = '';
          this.form.key.info = '';
          this.form.domain.invalid = false;
          this.form.key.invalid = false;
        }else{
          this.form.testing = false;
          this.form.domain.info = 'The domain or the key is wrong';
          this.form.key.info = 'The domain or the key is wrong';
          this.form.domain.invalid = true;
          this.form.key.invalid = true;
        }
      }).catch(error => {
        this.form.testing = false;
        var errors = error.response.data.errors;
        if(errors.domain){
          this.form.domain.info = errors.domain[0];
          this.form.domain.invalid = true;
        }else{
          this.form.domain.info = '';
          this.form.domain.invalid = false;
        }
        if(errors.key){
          this.form.key.info = errors.key[0];
          this.form.key.invalid = true;
        }else{
          this.form.key.info = '';
          this.form.key.invalid = false;
        }
      });
    },
  },
  created(){
    this.$emit('update-form-data', 1, this.form);
  },
  updated(){
    this.$emit('update-form-data', 1, this.form);
  }
});
</script>
@endpush
