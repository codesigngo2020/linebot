@push('js')
<script type="text/x-template" id="--nav-form-3">
  <div class="tab-pane fade" id="nav-form-3" role="tabpanel" aria-labelledby="nav-form-3-tab">
    <div class="deployment-info mb-4">
      <div class="info-item">您可以建立至多5隻爬蟲，每隻爬蟲可以選擇不同的爬取方式，包含：選擇頻道以及播放清單。為爬取的文章分類並貼上標籤，方便您在Wordpress上管理。最後，請參考<a href="#"><strong>Wordpress文章格式說明文件</strong></a>，設定文章格式，爬蟲將以此格式建立文章。</div>
    </div>

    <ul id="crawler-nav-tabs" class="nav nav-tabs nav-overflow mb-4" role="tablist">
      <li class="nav-item" v-for="i in form.crawlers_count">
        <a :class="[i == 1 ? 'active' : '', 'nav-link']" data-toggle="tab" :href="'#nav-crawler-'+i" role="tab" :aria-controls="'nav-crawler-'+i" aria-selected="true">爬蟲 @{{ i }}</a>
      </li>
      <li class="nav-item">
        <div id="new-crawler" class="nav-link" v-if="form.crawlers_count < 5" @click="newCrawler"><v-svg class="mr-2" src="{{ asset('assets/image/svg/light/plus-circle.svg') }}" width="18" height="18"></v-svg>新增爬蟲</div>
      </li>
    </ul>

    <div class="tab-content">
      {{-- 建立爬蟲 --}}
      <crawler-tab v-for="i in form.crawlers_count" :key="i" :index="i" @updateFormData="updateFormData"></crawler-tab>
    </div>
  </div>
</script>


{{-- 建立爬蟲 選擇格式 --}}
<script type="text/x-template" id="crawler-tab">
  <div :class="[index == 1 ? 'show active' : '', 'tab-pane fade']" :id="'nav-crawler-'+index" role="tabpanel" :aria-labelledby="'nav-form-'+index+'-tab'">
    <div class="form-group">
      <label :for="'crawler-type-'+index">爬蟲類型<span class="form-required">*</span></label>
      <vue-multiselect :class="[{'is-invalid' : form.crawler_type.invalid}]" v-model="form.crawler_type.value" track-by="name" label="name" placeholder="請選擇爬蟲類型" :options="form.crawler_type.options" :searchable="false" :show-labels="false" :allow-empty="false"></vue-multiselect>
      <span class="invalid-feedback" role="alert"><strong>@{{ form.crawler_type.info }}</strong></span>
    </div>

    {{-- 建立爬蟲 爬蟲詳細資料 --}}
    <crawler-form :type="form.crawler_type.value.value" v-if="form.crawler_type.value != null" :index="index" @updateCrawler="updateCrawler"></crawler-form>
  </div>
</script>


{{-- 建立爬蟲 表單 --}}
<script type="text/x-template" id="crawler-form">
  <div>
    <div class="form-group">
      <label :for="'channel-'+index">頻道 ID<span class="form-required">*</span></label>
      <input :id="'channel-'+index" :class="[{'is-invalid' : crawler.channel.invalid}, 'form-control']" v-model="crawler.channel.id" placeholder="頻道 ID" :name="'channel-'+index" type="text">
      <span class="invalid-feedback" role="alert"><strong>@{{ crawler.channel.info }}</strong></span>
    </div>

    <div v-if="type == 'list'" class="form-group">
      <label :for="'channel-'+index">播放清單 ID<span class="form-required">*</span></label>
      <input :id="'channel-'+index" :class="[{'is-invalid' : crawler.list.invalid}, 'form-control']" v-model="crawler.list.id" placeholder="播放清單 ID" :name="'list-'+index" type="text">
      <span class="invalid-feedback" role="alert"><strong>@{{ crawler.list.info }}</strong></span>
    </div>

    <div class="form-group">
      <label :for="'post-format-'+index">文章格式<span class="form-required">*</span></label>
      <textarea :id="'post-format-'+index" :class="[{'is-invalid' : crawler.post.format.invalid}, 'form-control']" v-model="crawler.post.format.content" placeholder="請參考「Wordpress文章格式說明文件」填寫您的文章格式。" rows="5"></textarea>
      <span class="invalid-feedback" role="alert"><strong>@{{ crawler.post.format.info }}</strong></span>
    </div>

    <div class="form-group">
      <label :for="'category-'+index">文章類別<span class="form-required">*</span></label>
      <vue-multiselect :class="[{'is-invalid' : crawler.post.category.invalid}, crawler.post.category.value.length > 0 ? 'selected' : '', 'taggable hidden-arrow']" :id="'category-'+index" v-model="crawler.post.category.value" placeholder="請輸入類別名稱" :options="crawler.post.category.options" :show-labels="false" :allow-empty="true" :multiple="true" :taggable="true" :limit="5" @tag="addCategory" @remove="removeCategory"></vue-multiselect>
      <span class="invalid-feedback" role="alert"><strong>@{{ crawler.post.category.info }}</strong></span>
    </div>

    <div class="form-group">
      <label :for="'tag-'+index">文章標籤<span class="form-required">*</span></label>
      <vue-multiselect :class="[{'is-invalid' : crawler.post.tag.invalid}, crawler.post.tag.value.length > 0 ? 'selected' : '', 'taggable hidden-arrow']" v-model="crawler.post.tag.value" placeholder="請輸入標籤名稱" :options="crawler.post.tag.options" :show-labels="false" :allow-empty="true" :multiple="true" :taggable="true" :limit="5" @tag="addTag" @remove="removeTag"></vue-multiselect>
      <span class="invalid-feedback" :id="'tag-'+index" role="alert"><strong>@{{ crawler.post.tag.info }}</strong></span>
    </div>

    <div class="form-group">
      <label :for="'frequency-'+index">請求頻率<span>：每 @{{ crawler.post.frequency.value }} 分鐘</span></label>
      <input type="range" :class="[{'is-invalid' : crawler.post.frequency.invalid}, 'custom-range']" min="1" max="60" step="1" :id="'frequency-'+index" v-model="crawler.post.frequency.value">
      <span class="invalid-feedback" role="alert"><strong>@{{ crawler.post.frequency.info }}</strong></span>
    </div>

    <hr class="mt-4 mb-4">

    <p class="color-light">其他文章設定</p>

    <div class="form-group">
      <div :class="[{'is-invalid' : crawler.post.options.invalid}, 'i-form-control']">
        <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input" :id="'link-active-'+index" value="link-active" v-model="crawler.post.options.value">
          <label :class="[crawler.post.options.value.includes('link-active')? '' : 'color-light', 'custom-control-label']" :for="'link-active-'+index">啟用文章連結</label>
        </div>

        <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input" :id="'close-comment-'+index" value="close-comment" v-model="crawler.post.options.value">
          <label :class="[crawler.post.options.value.includes('close-comment')? '' : 'color-light', 'custom-control-label']" :for="'close-comment-'+index">關閉留言功能</label>
        </div>

        <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input" :id="'include-origin-tags-'+index" value="include-origin-tags" v-model="crawler.post.options.value">
          <label :class="[crawler.post.options.value.includes('include-origin-tags')? '' : 'color-light', 'custom-control-label']" :for="'include-origin-tags-'+index">文章標籤包含youtube影片自帶標籤</label>
        </div>

        <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input" :id="'publish-time-as-post-time-'+index" value="publish-time-as-post-time" v-model="crawler.post.options.value">
          <label :class="[crawler.post.options.value.includes('publish-time-as-post-time')? '' : 'color-light', 'custom-control-label']" :for="'publish-time-as-post-time-'+index">使用youtube影片發布時間作為文章建立時間，預設為爬取文章時間</label>
        </div>
      </div>


      <span class="invalid-feedback" role="alert"><strong>@{{ crawler.post.options.info }}</strong></span>
    </div>

  </div>
</script>


<script>
Vue.component('nav-form-3', {
  template: '#--nav-form-3',
  data(){
    return {
      form: {
        crawlers_count: 1,
        crawlers: {},
      }
    }
  },
  methods: {
    newCrawler(){
      if(this.form.crawlers_count < 5){
        this.form.crawlers_count ++;
      }
    },
    updateFormData(index, data){
      this.$set(this.form.crawlers, index, data);
      this.$emit('update-form-data', 3, this.form);
    }
  },
  created(){
    this.$emit('update-form-data', 3, this.form);
  },
  updated(){
    this.$emit('update-form-data', 3, this.form);
  }
});


// 建立爬蟲 選擇格式
Vue.component('crawler-tab', {
  template: '#crawler-tab',
  props: ['index'],
  data(){
    return {
      form: {
        crawler_type: {
          value: null,
          options: [
            { name: '整個頻道', value: 'channel' },
            { name: '整個播放清單', value: 'list' },
          ],
          info: '',
          invalid: false,
        },
        crawler: {},
      }
    }
  },
  methods: {
    updateCrawler(crawler){
      this.$set(this.form, 'crawler', crawler);
      this.$emit('updateFormData', this.index, this.form);
    },
  },
  created(){
    this.$emit('updateFormData', this.index, this.form);
  },
  updated(){
    this.$emit('updateFormData', this.index, this.form);
  }
});

// 建立爬蟲 表單
Vue.component('crawler-form', {
  template: '#crawler-form',
  props: ['index', 'type'],
  data(){
    return {
      crawler:{
        channel: {
          id: '',
          info: '',
          invalid: false,
        },
        list: {
          id: '',
          info: '',
          invalid: false,
        },
        post: {
          format: {
            content: '',
            info: '',
            invalid: false,
          },
          category: {
            value: ['Wordpress', 'Youtube爬蟲'],
            options: ['Wordpress', 'Youtube爬蟲'],
            info: '',
            invalid: false,
          },
          tag: {
            value: ['Wordpress', 'Youtube爬蟲'],
            options: ['Wordpress', 'Youtube爬蟲'],
            info: '',
            invalid: false,
          },
          frequency: {
            value: 5,
            info: '',
            invalid: false,
          },
          options: {
            value: [],
            info: '',
            invalid: false,
          }
        },
      }
    };
  },
  methods: {
    addCategory(category){
      if(this.crawler.post.category.value.length < 5){
        this.crawler.post.category.options.push(category);
        this.crawler.post.category.value.push(category);
      }
    },
    removeCategory(category){
      this.crawler.post.category.options.splice(this.crawler.post.category.options.indexOf(category), 1);
    },
    addTag(tag){
      if(this.crawler.post.tag.value.length < 5){
        this.crawler.post.tag.options.push(tag);
        this.crawler.post.tag.value.push(tag);
      }
    },
    removeTag(tag){
      this.crawler.post.tag.options.splice(this.crawler.post.tag.options.indexOf(tag), 1);
    },
    changeCrawlerType(type){
      if(type == 'list'){
        this.crawler.list = {
          id: '',
          info: '',
          invalid: false,
        };
      }else{
        this.crawler.list = null;
      }
    }
  },
  created(){
    this.changeCrawlerType(this.type);
    this.$emit('updateCrawler', this.crawler);
  },
  updated(){
    this.$emit('updateCrawler', this.crawler);
  },
  watch: {
    type(type){
      this.changeCrawlerType(type);
    }
  }
});

</script>
@endpush
