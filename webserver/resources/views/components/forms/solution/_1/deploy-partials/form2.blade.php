@push('js')
<script type="text/x-template" id="--nav-form-2">
  <div class="tab-pane fade" id="nav-form-2" role="tabpanel" aria-labelledby="nav-form-2-tab">
    <div class="deployment-info mb-4">
      <div class="info-item">請根據<a href="#"><strong>API申請說明文件</strong></a>，前往<a href="#"><strong>Google Cloud Platform</strong></a>啟用<strong>YouTube Data API v3</strong>以及申請憑證金鑰。每一個Google帳號可以至多申請10個Google Cloud Platform專案，每一個專案可以申請一個API憑證金鑰，由於每個API憑證金鑰有請求額度限制且是獨立的，若您的爬蟲需要大量的爬取資料，建議多申請幾個憑證金鑰或向Google申請提高額度，我們將會在達額度上限時，切換API憑證金鑰。</div>
    </div>

    {{-- 憑證金鑰 --}}
    <key-input v-for="i in form.keys_count" :key="i" :index="i" @updateFormData="updateFormData"></key-input>
    <div id="new-key" v-if="form.keys_count < 5" @click="newKey"><v-svg class="mr-2" src="{{ asset('assets/image/svg/light/plus-circle.svg') }}" width="18" height="18"></v-svg>新增憑證金鑰</div>
  </div>
</script>


{{-- 憑證金耀 輸入匡 --}}
<script type="text/x-template" id="key-input">
  <div class="form-group">
    <label :for="'key-'+index">憑證金鑰 @{{ index }}<span class="form-required">*</span></label>
    <div class="d-sm-flex i-form-control" :class="{'is-invalid' : form.key.invalid}">
      <input :id="'key-'+index" :class="[{'is-invalid' : form.key.invalid}, 'form-control']" v-model="form.key.key" placeholder="youtube data api v3 key" :name="'key-'+index" type="text" :disabled="form.is_success">
      <button :id="'key-'+index+'-test'" @click="testKey" type="button" :class="[form.is_success ? 'btn-success' : 'btn-warning', {'progress-bar-striped progress-bar-animated' : form.key.testing}, 'btn btn-block col col-sm-2 mt-4 mt-sm-0 ml-sm-3']" :disabled="form.is_success">@{{ form.is_success ? '有效金鑰' : '測試金鑰' }}</button>
    </div>
    <span class="invalid-feedback" role="alert"><strong>@{{ form.key.info }}</strong></span>
  </div>
</script>

<script>
Vue.component('nav-form-2', {
  template: '#--nav-form-2',
  data(){
    return {
      form: {
        keys_count: 1,
        keys: {},
      }
    }
  },
  methods: {
    newKey(){
      if(this.form.keys_count < 5){
        this.form.keys_count ++;
      }
    },
    updateFormData(index, data){
      this.$set(this.form.keys, index, data);
      this.$emit('update-form-data', 2, this.form);
    }
  },
  created(){
    this.$emit('update-form-data', 2, this.form);
  },
  updated(){
    this.$emit('update-form-data', 2, this.form);
  },
});


// 憑證金耀 輸入匡
Vue.component('key-input', {
  template: '#key-input',
  props: ['index'],
  data(){
    return {
      form: {
        is_success: false,
        key: {
          key: '',
          info: '',
          invalid: false,
          testing: false,
        }
      }
    }
  },
  methods: {
    testKey(index){
      this.form.key.testing = true;
      this.form.key.info = '';
      this.form.key.invalid = false;
      axios
      .post(route('solution.1.test-key'), {
        _token: args._token,
        key: this.form.key.key,
      })
      .then(response => {
        if(response.data == 'success'){
          this.form.key.testing = false;
          this.form.is_success = true;
          this.form.key.info = '';
          this.form.key.invalid = false;
        }else{
          this.form.key.testing = false;
          this.form.key.info = 'The key is invalid';
          this.form.key.invalid = true;
        }
      }).catch(error => {
        this.form.key.testing = false;
        var errors = error.response.data.errors;
        if(errors.key){
          this.form.key.info = errors.key[0];
          this.form.key.invalid = true;
        }else{
          this.form.key.info = '';
          this.form.key.invalid = false;
        }
      });
    }
  },
  created(){
    this.$emit('updateFormData', this.index, this.form);
  },
  updated(){
    this.$emit('updateFormData', this.index, this.form);
  }
});
</script>
@endpush
