@if($paginator->hasPages())
<nav aria-label="Page navigation">
  <ul class="pagination">
    @if($paginator->currentPage() >= 4)
    <li class="page-item">
      <a class="page-link" href="{{ $paginator->url($paginator->currentPage() - 3) }}" aria-label="Previous">
        <span aria-hidden="true">...</span>
        <span class="sr-only">Previous</span>
      </a>
    </li>
    @endif
    @foreach(range($paginator->currentPage() - 2, $paginator->currentPage() + 2) as $i)
    @if($i >= 1 && $i <= $paginator->lastPage())
    <li class="page-item{{ $i == $paginator->currentPage() ? ' active' : '' }}">
      <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
    </li>
    @endif
    @endforeach
    @if($paginator->lastPage() > 5 && $paginator->currentPage() < $paginator->lastPage() - 3 )
    <li class="page-item">
      <a class="page-link" href="{{ $paginator->url($paginator->currentPage() + 3) }}" aria-label="Next">
        <span aria-hidden="true">...</span>
        <span class="sr-only">Next</span>
      </a>
    </li>
    @endif
  </ul>
</nav>
@endif
