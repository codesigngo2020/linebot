<div class="table-responsive">
  <table id="posts-list-{{ $key + 1 }}" class="table table-sm table-nowrap card-table">
    <thead>
      <tr>
        <th>
          <div class="custom-control custom-checkbox table-checkbox">
            <input type="checkbox" class="custom-control-input" name="ordersSelect" id="ordersSelectAll">
            <label class="custom-control-label" for="ordersSelectAll">&nbsp;</label>
          </div>
        </th>
        <th>
          <a href="javascript:void(0);" class="text-muted sort" data-sort="id">#</a>
        </th>
        <th>
          <a href="javascript:void(0);" class="text-muted sort" data-sort="id">WP文章ID</a>
        </th>
        <th>
          <a href="javascript:void(0);" class="text-muted sort" data-sort="title">影片標題</a>
        </th>
        <th>
          <a href="javascript:void(0);" class="text-muted sort" data-sort="created_at">爬取時間</a>
        </th>
      </tr>
    </thead>
    <tbody class="list">

      <tr v-for="post in data.posts">
        <td>
          <div class="custom-control custom-checkbox table-checkbox">
            <input type="checkbox" class="custom-control-input" name="ordersSelect" id="ordersSelectOne">
            <label class="custom-control-label" for="ordersSelectOne">&nbsp;</label>
          </div>
        </td>
        <td class="orders-order">@{{ post.id }}</td>
        <td class="orders-order">@{{ post.post_id }}</td>
        <td class="orders-product">@{{ post.video.title }}</td>
        <td class="orders-date">2019-02-03 05:33</td>
      </tr>

    </tbody>
  </table>
</div>
