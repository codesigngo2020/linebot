<div id="nav-table-{{ $key + 1 }}" class="tab-pane fade {{ $key == 0 ? 'show active' : '' }}" role="tabpanel" aria-labelledby="nav-table-{{ $key + 1 }}-tab">
  <div class="card">

    @include('components.tables.solution._1.posts-partials.header')

    @include('components.tables.solution._1.posts-partials.table')

  </div>
</div>

@push('js')
<script>
var nav_table_{{ $key + 1 }} = new Vue({
  el: '#nav-table-{{ $key + 1 }}',
  data: {
    data: {!! $crawler->toJson() !!},
  },
  created(){
    //this.table = new List('posts-list-{{ $key + 1 }}');
  }
});


/*var osts_list_{{ $key + 1 }} = new List('posts-list-{{ $key + 1 }}', {
  valueNames: [ 'orders-order', 'orders-product']
},[
  { 'orders-order': 'Jonny', 'orders-product':'Stockholm' },
  { 'orders-order': 'Jonas', 'orders-product':'Berlin' }
]);*/
</script>
@endpush
