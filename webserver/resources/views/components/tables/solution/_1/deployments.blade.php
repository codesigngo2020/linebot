<div id="deployment_list" class="table-responsive card">
  <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">部署名稱</a></th>
        <th scope="col">網域名稱</th>
        <th scope="col">爬蟲數量</th>
        <th scope="col">管理權限</th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody class="list">
      @foreach($deployments as $deployment)
      <tr>
        <th scope="row">{{ $deployment->id }}</th>
        <td>
          <p class="title">
            <a href="{{ route('solution.1.deployment.show', $deployment) }}">{{ $deployment->name }}</a>
          </p>
        </td>
        <td>
          <p>{{ $deployment->data->domain }}</p>
        </td>
        <td>
          <p>{{ $deployment->solution_1_crawlers_count }}</p>
        </td>
        <td>
          @role('superadmin_for_deployment_'.$deployment->id)
          <p class="ibadge badge-soft-danger">superadmin</p>
          @else
          <p class="ibadge badge-soft-warning">admin</p>
          @endrole
        </td>
        <td>
          <div class="dropdown">
            <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <v-svg class="svg" src="{{ asset('assets/image/svg/light/ellipsis-v.svg') }}" width="25" height="25"></v-svg>
            </a>
            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end">
              <a href="{{ route('solution.1.deployment.show', $deployment) }}" class="dropdown-item">查看</a>
              <a href="#!" class="dropdown-item">編輯</a>
            </div>
          </div>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  {{ $deployments->links('components.paginations.default') }}
</div>
