<div id="keys_table" class="col-12 card nopadding">
  <div class="card-header">
    <div class="row align-items-center">
      <div class="col">
        {{-- Title --}}
        <h4 class="card-header-title">Youtube 金鑰列表</h4>
      </div>
      <div class="col-auto">
        {{-- Button --}}
        <a href="#!" class="btn btn-sm btn-white">新增金鑰</a>
      </div>
    </div>
  </div>
  <div class="table-responsive">
    <table class="table card-table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Youtube金鑰</th>
          <th scope="col">狀態</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody class="list">
        @foreach($keys as $key)
        <tr>
          <th scope="row">{{ $loop->index + 1 }}</th>
          <td>
            <p>{{ $key->key }}</p>
          </td>
          <td>
            <p>{{ __('models/platform/solution/_1/key.status.'.$key->status) }}</p>
          </td>
          <td>
            <div class="dropdown">
              <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <v-svg class="svg" src="{{ asset('assets/image/svg/light/ellipsis-v.svg') }}" width="25" height="25"></v-svg>
              </a>
              <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end">
                <a href="{{ route('solution.1.deployment.show', $deployment) }}" class="dropdown-item">查看</a>
                <a href="#!" class="dropdown-item">編輯</a>
              </div>
            </div>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
