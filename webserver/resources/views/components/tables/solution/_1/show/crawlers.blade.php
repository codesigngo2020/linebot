<div id="crawlers_table" class="col-12 card nopadding">
  <div class="card-header">
    <div class="row align-items-center">
      <div class="col">
        {{-- Title --}}
        <h4 class="card-header-title">爬蟲列表</h4>
      </div>
    </div>
  </div>
  <div class="table-responsive">
    <table class="table card-table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">爬蟲名稱</th>
          <th scope="col">類型</th>
          <th scope="col">已爬／總文章數</th>
          <th scope="col">頻率</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody class="list">
        @foreach($crawlers as $crawler)
        <tr>
          <th scope="row">{{ $crawler->id }}</th>
          <td>
            <p class="title">爬蟲 {{ $loop->index + 1 }}</p>
          </td>
          <td>
            <p>{{ __('models/platform/solution/_1/crawler.type.'.$crawler->type) }}</p>
          </td>
          <td>
            <p>{{ $crawler->posts_count - $crawler->uncrawl_posts_count }}／{{ $crawler->posts_count }}</p>
          </td>
          <td>
            <p>每 {{ $crawler->frequency }} 分鐘</p>
          </td>
          <td>
            <div class="dropdown">
              <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <v-svg class="svg" src="{{ asset('assets/image/svg/light/ellipsis-v.svg') }}" width="25" height="25"></v-svg>
              </a>
              <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end">
                <a href="{{ route('solution.1.deployment.show', $deployment) }}" class="dropdown-item">查看</a>
                <a href="#!" class="dropdown-item">編輯</a>
              </div>
            </div>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
