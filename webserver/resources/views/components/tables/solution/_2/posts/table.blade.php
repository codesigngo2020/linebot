<div v-cloak class="table-responsive position-relative">

  <div :class="[is_loading ? 'd-flex' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-bl-05 border-radius-br-05 z-index-1']">
    <div class="spinner-grow text-secondary" role="status">
      <span class="sr-only">Loading...</span>
    </div>
  </div>

  <table id="posts-list-{{ $key + 1 }}" class="table table-sm table-nowrap card-table">
    <thead>
      <tr>
        <th>
          <div class="custom-control custom-checkbox table-checkbox">
            <input type="checkbox" class="custom-control-input" name="ordersSelect" id="selectAll" v-model="selectAll" @change="changeSelectAll">
            <label class="custom-control-label" for="selectAll">&nbsp;</label>
          </div>
        </th>
        <th>
          <a href="javascript:void(0);" :class="[order.column == 'id' ? order.dir : '' ,'text-muted sort']" data-sort="id" @click="sort('id')">#</a>
        </th>
        <th>
          <a href="javascript:void(0);" :class="[order.column == 'status' ? order.dir : '' ,'text-muted sort']" data-sort="status" @click="sort('status')">WP 文章連結</a>
        </th>
        <th>
          <a href="javascript:void(0);" class="text-muted">IG 文章連結</a>
        </th>
        <th>
          <a href="javascript:void(0);" :class="[order.column == 'posted_at' ? order.dir : '' ,'text-muted sort']" data-sort="posted_at" @click="sort('posted_at')">刊登時間</a>
        </th>
      </tr>
    </thead>
    <tbody class="list">

      <tr v-for="post in data.posts">
        <td>
          <div class="custom-control custom-checkbox table-checkbox">
            <input type="checkbox" class="custom-control-input" name="ordersSelect" :id="post.id" :value="post.id" v-model="selectedPosts">
            <label class="custom-control-label" :for="post.id">&nbsp;</label>
          </div>
        </td>
        <td class="orders-order">@{{ post.id }}</td>
        <td class="orders-order">
          <span v-if="post.post_id" class="badge badge-soft-success">@{{ post.post_id }}</span>
          <span v-else-if="post.status == 'standBy'" class="badge badge-soft-secondary">等候刊登</span>
          <span v-else-if="post.status == 'posting'" v-else class="badge badge-soft-warning">刊登中</span>
          <span v-else-if="post.status == 'ignore'" v-else class="badge badge-soft-light">忽略文章</span>
        </td>
        <td class="orders-product">
          <a :href="'https://www.instagram.com/p/'+post.shortcode+'/'" target="_blank">
            <span class="badge badge-soft-secondary">@{{ post.content }}</span>
          </a>
        </td>
        <td class="orders-date">@{{ post.posted_at ? post.posted_at.substr(0,16) : '-' }}</td>
      </tr>

    </tbody>
  </table>
</div>
