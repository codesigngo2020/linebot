<div class="card-header">
  <div class="row align-items-center">
    <div class="col">

      {{-- Search --}}
      <form class="row align-items-center" @submit.prevent="search">
        <div class="col-auto pr-0">
          <span class="text-muted">
            <v-svg src="{{ asset('assets/image/svg/light/search.svg') }}" width="18" height="18"></v-svg>
          </span>
        </div>
        <div class="col">
          <input type="search" class="form-control form-control-flush search" placeholder="Search" v-model="query">
        </div>
      </form>

    </div>
    <div class="col-auto">

      {{-- Button --}}

      <div class="dropdown">
        <button class="btn btn-sm btn-white" type="button" id="bulkActionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">批次動作</button>
        <div class="dropdown-menu dropdown-menu-right font-size-0875" aria-labelledby="bulkActionDropdown">
          <a class="dropdown-item" href="javascript:void(0);">重新刊登（刪除wp文章）</a>
          <a class="dropdown-item" href="javascript:void(0);" @click="repost">重新刊登（不刪除wp文章）</a>
          <a class="dropdown-item" href="javascript:void(0);" @click="ignore">忽略文章</a>
          <a class="dropdown-item" href="javascript:void(0);">刪除wp文章（不重新刊登）</a>
        </div>
      </div>

    </div>
  </div> {{-- / .row --}}
</div>
