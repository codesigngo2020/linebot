<div v-cloak class="table-responsive position-relative">

  <div :class="[is_loading ? 'd-flex' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-bl-05 border-radius-br-05']">
    <div class="spinner-grow text-secondary" role="status">
      <span class="sr-only">Loading...</span>
    </div>
  </div>

  <table id="logs-list" class="table table-sm table-nowrap card-table">
    <thead>
      <tr>
        <th>
          <div class="custom-control custom-checkbox table-checkbox">
            <input type="checkbox" class="custom-control-input" name="ordersSelect" id="ordersSelectAll">
            <label class="custom-control-label" for="ordersSelectAll">&nbsp;</label>
          </div>
        </th>
        <th>
          <a href="javascript:void(0);" :class="[order.column == 'id' ? order.dir : '' ,'text-muted sort']" data-sort="id" @click="sort('id')">#</a>
        </th>
        <th>
          <a href="javascript:void(0);" :class="[order.column == 'type' ? order.dir : '' ,'text-muted sort']" data-sort="type" @click="sort('type')">狀態</a>
        </th>
        <th>
          <a href="javascript:void(0);" class="text-muted">訊息記錄</a>
        </th>
        <th>
          <a href="javascript:void(0);" :class="[order.column == 'created_at' ? order.dir : '' ,'text-muted sort']" data-sort="created_at" @click="sort('created_at')">時間戳記</a>
        </th>
      </tr>
    </thead>
    <tbody class="list">

      <tr v-for="log in logs">
        <td>
          <div class="custom-control custom-checkbox table-checkbox">
            <input type="checkbox" class="custom-control-input" name="ordersSelect" id="ordersSelectOne">
            <label class="custom-control-label" for="ordersSelectOne">&nbsp;</label>
          </div>
        </td>
        <td class="orders-order">@{{ log.id }}</td>
        <td class="orders-order">
          <span :class="['badge-soft-'+log.class ,'badge']">@{{ typeFormat[log.type] }}</span>
        </td>
        <td class="orders-product text-left white-space-pre">@{{ log.message }}</td>
        <td class="orders-date">@{{ log.created_at.substr(0,16) }}</td>
      </tr>

    </tbody>
  </table>
</div>
