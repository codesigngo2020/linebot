<div class="card-header">
  <div class="row align-items-center">
    <div class="col">

      {{-- Search --}}
      <form class="row align-items-center" @submit.prevent="search">
        <div class="col-auto pr-0">
          <span class="text-muted">
            <v-svg src="{{ asset('assets/image/svg/light/search.svg') }}" width="18" height="18"></v-svg>
          </span>
        </div>
        <div class="col">
          <input type="search" class="form-control form-control-flush search" placeholder="Search" v-model="query">
        </div>
      </form>

    </div>
  </div> {{-- / .row --}}
</div>
