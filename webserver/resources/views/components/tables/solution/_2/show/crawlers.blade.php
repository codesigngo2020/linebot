<div id="crawlers_table" class="col-12 card nopadding">
  <div class="card-header">
    <div class="row align-items-center">
      <div class="col">
        {{-- Title --}}
        <h4 class="card-header-title">爬蟲列表</h4>
      </div>
    </div>
  </div>
  <div class="table-responsive">
    <table class="table card-table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">IG 帳號</th>
          <th scope="col">已爬／總文章數</th>
          <th scope="col">啟用狀態</th>
        </tr>
      </thead>
      <tbody class="list">
        @foreach($crawlers as $crawler)
        <tr>
          <th scope="row">{{ $crawler->id }}</th>
          <td>
            <p class="title">
              <a href="https://www.instagram.com/{{ $crawler->account->username }}/" target="_blank">
                <span class="badge badge-soft-primary">{{ '@'.$crawler->account->username }}</span>
              </a>
            </p>
          </td>
          <td>
            <p>{{ $crawler->posts_count - $crawler->uncrawl_posts_count }}／{{ $crawler->posts_count }}</p>
          </td>
          <td>
            <form>
              <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="crawler-{{ $crawler->id }}" {{ $crawler->active == 1 ? 'checked' : '' }} ref="crawler-{{ $crawler->id }}" @change="crawlerActiveChange({{ $crawler->id }}, $event.target.checked)">
                <label class="custom-control-label" for="crawler-{{ $crawler->id }}"></label>
              </div>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
