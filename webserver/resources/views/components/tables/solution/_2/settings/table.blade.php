<div class="card nopadding">
  <div class="card-header">
    <div class="row align-items-center">
      <div class="col">
        {{-- Title --}}
        <h4 class="card-header-title">帳單資訊（1年）</h4>
      </div>
    </div>
  </div>
  <div class="table-responsive">
    <table class="table card-table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">金額</th>
          <th scope="col">日期</th>
        </tr>
      </thead>
      <tbody class="list">
        @foreach($transactions as $transaction)
        <tr>
          <th scope="row">{{ $transaction['id'] }}</th>
          <td>{{ $transaction['amount'] }}</td>
          <td>{{ $transaction['date'] }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
