<div id="deployment_list" class="table-responsive card">
  <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">部署名稱</a></th>
        <th scope="col">網域名稱</th>
        <th scope="col">爬蟲數量</th>
        <th scope="col">管理權限</th>
        <th scope="col">狀態</th>
      </tr>
    </thead>
    <tbody class="list">
      @foreach($deployments as $deployment)
      <tr>
        <th scope="row">{{ $deployment->id }}</th>
        <td>
          <p class="title">
            <a href="{{ route('solution.2.deployment.show', $deployment) }}">{{ $deployment->name }}</a>
          </p>
        </td>
        <td>
          <p>{{ $deployment->data->domain }}</p>
        </td>
        <td>
          <p>{{ $deployment->solution_2_crawlers_count }}</p>
        </td>
        <td>
          @role('superadmin_for_deployment_'.$deployment->id)
          <p class="ibadge badge-soft-danger">superadmin</p>
          @else
          <p class="ibadge badge-soft-warning">admin</p>
          @endrole
        </td>
        <td>
          @if($deployment->status == 'active')
          <p class="ibadge badge-soft-primary">運作中</p>
          @elseif($deployment->status == 'suspend')
          <p class="ibadge badge-soft-secondary">暫停</p>
          @elseif($deployment->status == 'destroyed')
          <p class="ibadge badge-soft-warning">待刪除</p>
          @endif
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  {{ $deployments->links('components.paginations.default') }}
</div>
