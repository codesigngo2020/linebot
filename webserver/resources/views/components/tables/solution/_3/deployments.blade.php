<div id="deployment_list" class="table-responsive card">
  <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">部署名稱</a></th>
        <th scope="col">IG 帳號</th>
        <th scope="col" class="text-left">IG 名稱</th>
        <th scope="col">管理權限</th>
        <th scope="col">狀態</th>
      </tr>
    </thead>
    <tbody class="list">
      @foreach($deployments as $deployment)
      <tr>
        <th scope="row">{{ $deployment->id }}</th>
        <td>
          <p class="title">
            <a href="{{ route('solution.3.deployment.show', $deployment) }}">{{ $deployment->name }}</a>
          </p>
        </td>
        <td>
          <p class="ibadge badge-soft-primary">{{ '@'.$deployment->solution_3_account->username }}</p>
        </td>
        <td class="text-left">
          <p class="d-flex align-items-center">
            <span class="rounded-circle overflow-hidden mr-3"><img src="{{ $deployment->solution_3_account->profile_pic }}" style="width:35px;height:35px;"></span>
            <span class="text-muted">{{ $deployment->solution_3_account->full_name }}</span></p>
        </td>
        <td>
          @role('superadmin_for_deployment_'.$deployment->id)
          <p class="ibadge badge-soft-danger">superadmin</p>
          @else
          <p class="ibadge badge-soft-warning">admin</p>
          @endrole
        </td>
        <td>
          @if($deployment->status == 'active')
          <p class="ibadge badge-soft-primary">運作中</p>
          @elseif($deployment->status == 'suspend')
          <p class="ibadge badge-soft-secondary">暫停</p>
          @elseif($deployment->status == 'destroyed')
          <p class="ibadge badge-soft-warning">待刪除</p>
          @endif
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  {{ $deployments->links('components.paginations.default') }}
</div>
