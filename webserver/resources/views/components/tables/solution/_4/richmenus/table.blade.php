<div v-cloak class="position-relative row mb-3">

    <div :class="[is_loading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-05']">
        <div class="spinner-grow text-secondary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    <div class="col-12">

        @include('components.tables.solution._4.richmenus.header')

        <div v-for="richmenu in richmenus" class="card mb-3">
            <div class="card-body p-0">
                <div class="row align-items-center">
                    <div class="col-auto preview">
                        <div :class="[richmenu.size == '2500x1686' ? 'type-1' : 'type-2', 'position-relative imege']">
                            <img :src="richmenu.imageUrl">
                        </div>
                        <p class="chatbar-text-preview">
                            <span>@{{ richmenu.chatBarText }}</span>
                        </p>
                    </div>

                    <div class="col ml-n2 pt-4 pb-4 pr-5">

                        <!-- Title -->
                        <div :class="[{'mt-n1' : !richmenu.default}, 'card-title d-flex align-items-center mb-3']">
                            <span class="h4 mb-0 mr-3">@{{ '#'+( richmenu.parent_id ? richmenu.parent_id : richmenu.id ) }}</span>

                            <span class="flex-grow-1">
                                <a class="h4 mb-0 text-dark" :href="getUrl(richmenu.id)">@{{ richmenu.name }}</a>
                            </span>

                            {{-- 預設選單 --}}
                            <span v-if="richmenu.default" class="default position-relative small mr-3 pr-3">
                                <span class="badge badge-soft-primary">Dedault</span>
                            </span>

                            {{-- 無指定版本 --}}
                            <span v-if="richmenu.deleted_at || !richmenu.is_current_version" class="version position-relative small mr-3 pr-3">
                                <span v-if="richmenu.deleted_at" class="badge badge-soft-danger">All Deleted</span>
                                <span v-else-if="!richmenu.is_current_version" class="badge badge-soft-warning">No Active Version</span>
                            </span>

                            {{-- 版本數 --}}
                            <span v-if="richmenu.versions_count > 1 && richmenu.is_current_version" class="version position-relative small mr-3 pr-3">
                                <span class="badge badge-soft-info">@{{ 'v'+richmenu.version }}</span>
                            </span>

                            {{-- 標籤數 --}}
                            <span class="position-relative d-flex align-items-center small mr-4">
                                <span class="mr-1">
                                    <v-svg class="svg text-warning" src="{{ asset('assets/image/svg/solid/tags.svg') }}" width="16" height="16"></v-svg>
                                </span>
                                <span>@{{ 'x '+richmenu.tags_count }}</span>
                            </span>

                            <div class="dropdown d-flex">
                                <a href="#" class="d-inline-flex align-items-center cursor-pointer dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <v-svg class="svg text-muted" src="{{ asset('assets/image/svg/light/ellipsis-v.svg') }}" width="22" height="22"></v-svg>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right p-0">
                                    <a v-if="!richmenu.default" href="javascript:void(0);" class="dropdown-item" @click="toggleDefault(richmenu.id)">設為預設</a>
                                    <a v-if="richmenu.default" href="javascript:void(0);" class="dropdown-item" @click="toggleDefault(richmenu.id)">取消預設</a>
                                    <a href="javascript:void(0);" class="dropdown-item" @click="cancelBinding(richmenu.id)">取消綁定</a>
                                    <a href="javascript:void(0);" class="dropdown-item" @click="deleteAll(richmenu.id)">刪除所有版本</a>
                                </div>
                            </div>
                        </div>

                        <!-- Text -->
                        <p class="card-text small text-muted mb-2">@{{ richmenu.description }}</p>

                        <!-- percentage -->
                        <div v-if="!richmenu.default" class="align-items-center">
                            <div class="row align-items-center no-gutters">
                                <div class="col-auto">
                                    <div class="small mr-2">@{{ (richmenu.users_percentage ? richmenu.users_percentage : 0)+'%' }}</div>
                                </div>
                                <div class="col">
                                    <!-- Progress -->
                                    <div class="progress progress-sm">
                                        <div class="progress-bar" role="progressbar" :style="'width:'+richmenu.users_percentage+'%'"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div> <!-- / .row -->
            </div> <!-- / .card-body -->
        </div>
    </div>
</div>
