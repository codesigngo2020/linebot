@push('js')

{{-- 推播日曆 --}}

<script type="text/x-template" id="--welcome-message-list">


    <div id="welcome-message-list">
        <div v-cloak class="position-relative row mb-3">

            <div :class="[isLoading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-05']">
                <div class="spinner-grow text-secondary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>

            <div class="col-8">

                @include('components.tables.solution._4.welcome-messages.header')

                <div v-for="welcomeMessage in welcomeMessages" class="card mb-3">
                    <div class="card-body p-0">
                        <div class="row nomargin">
                            <div class="col ml-n2 pt-4 pb-4 pl-4 pr-4">

                                <!-- Title -->
                                <div class="card-title d-flex align-items-center mb-3">
                                    <span class="h4 mb-0 mr-3">@{{ '#'+welcomeMessage.id }}</span>
                                    <a class="h4 flex-grow-1 mb-0 text-dark" :href="getUrl(welcomeMessage.id)">@{{ welcomeMessage.name }}</a>
                                    <span class="tags_count position-relative d-flex align-items-center small mr-3 pr-3">
                                        <span class="mr-1">
                                            <v-svg class="svg text-warning" src="{{ asset('assets/image/svg/solid/tags.svg') }}" width="16" height="16"></v-svg>
                                        </span>
                                        <span>@{{ 'x '+welcomeMessage.tags_count }}</span>
                                    </span>
                                    <span class="d-flex small mr-3">
                                        <span :class="[welcomeMessage.active ? 'text-success' : 'text-warning', 'font-size-0750 mr-1']">●</span>
                                        <span>@{{ welcomeMessage.active ? 'Active' : 'Inactive' }}</span>
                                    </span>

                                    <div class="dropdown d-flex">
                                        <a href="#" class="d-inline-flex align-items-center cursor-pointer dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <v-svg class="svg text-muted" src="{{ asset('assets/image/svg/light/ellipsis-v.svg') }}" width="22" height="22"></v-svg>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right p-0">
                                            <a v-if="!welcomeMessage.active" href="javascript:void(0);" class="dropdown-item" @click="toggleActive(welcomeMessage.id)">啟用</a>
                                            <a v-if="welcomeMessage.active" href="javascript:void(0);" class="dropdown-item" @click="toggleActive(welcomeMessage.id)">停用</a>
                                        </div>
                                    </div>
                                </div>

                                <!-- Text -->
                                <p class="card-text small text-muted mb-2 mt-n1">@{{ welcomeMessage.description }}</p>

                                <!-- percentage -->
                                <div class="align-items-center">
                                    <div class="row align-items-center no-gutters">
                                        <div class="col-auto">
                                            <div class="small mr-2">@{{ (welcomeMessage.users_percentage ? welcomeMessage.users_percentage : 0)+'%' }}</div>
                                        </div>
                                        <div class="col">
                                            <!-- Progress -->
                                            <div class="progress progress-sm">
                                                <div class="progress-bar" role="progressbar" :style="'width:'+welcomeMessage.users_percentage+'%'"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="message-content p-2">
                                <ul class="list-group">
                                    <li v-for="messageContent in welcomeMessage.messages" :class="[{'has-quick-replay' : messageContent.hasQuickReplay}, 'list-group-item']">@{{ messageContent.type }}</li>
                                </ul>
                            </div>
                        </div> <!-- / .row -->
                    </div> <!-- / .card-body -->
                </div>
            </div>
        </div>

        <nav v-cloak v-if="paginator.lastPage > 1" class="d-flex justify-content-center" aria-label="Page navigation">
            <ul class="pagination">
                <li v-if="paginator.currentPage >= 4" class="page-item" @click="changePage(paginator.currentPage - 3)">
                    <a class="page-link" href="javascript:void(0);" aria-label="Previous">
                        <span aria-hidden="true">...</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>

                <li v-for="i in paginator.currentPage + 2" v-if="i >= 1 && i <= paginator.lastPage && i >= paginator.currentPage - 2" :class="['page-item', {'active': i == paginator.currentPage }]" @click="changePage(i)">
                    <a class="page-link" href="javascript:void(0);">@{{ i }}</a>
                </li>

                <li v-if="paginator.currentPage <= paginator.lastPage - 3" class="page-item" @click="changePage(paginator.currentPage + 3)">
                    <a class="page-link" href="javascript:void(0);" aria-label="Next">
                        <span aria-hidden="true">...</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            </ul>
        </nav>

    </div>
</script>

<script>
Vue.component('welcome-message-list', {
    template: '#--welcome-message-list',
    data(){
        return {
            isLoading: false,

            query: '',

            welcomeMessages: [],
            paginator: {
                currentPage: 1,
                lastPage: 1,
            },
            order: {
                column: 'id',
                dir: 'desc',
            },
        };
    },
    methods: {
        changePage(page){
            this.getWelcomeMessages(page);
        },
        search(){
            this.getWelcomeMessages(1);
        },
        getWelcomeMessages(page){
            this.isLoading = true;
            axios
            .get(route('solution.4.deployment.welcomeMessages.getWelcomeMessageList', {
                deployment: args.deployment,
                page: page,
                column: this.order.column,
                dir: this.order.dir,
                q: this.query,
            }))
            .then(response => {
                this.paginator = response.data.paginator;
                this.welcomeMessages = response.data.welcomeMessages;
                this.isLoading = false;
            }).catch(error => {
                this.isLoading = false;
                console.log(error);
            });
        },
        getUrl(id){
            return route('solution.4.deployment.welcomeMessages.show', {
                deployment: args.deployment,
                welcomeMessageId: id,
            });
        },
        toggleActive(id){
            this.isLoading = true;

            axios
            .post(route('solution.4.deployment.welcomeMessages.toggleActive', {
                deployment: args.deployment,
                welcomeMessageId: id,
            }), {
                _token: args._token,
            })
            .then(response => {
                if(response.data.status == 'success'){
                    this.getWelcomeMessages(this.paginator.currentPage);
                }else{
                    this.isLoading = false;
                }
            })
            .catch(error => {
                this.isLoading = false;
            });
        },
    },
    created(){
        this.getWelcomeMessages(1);
    }
});
</script>
@endpush
