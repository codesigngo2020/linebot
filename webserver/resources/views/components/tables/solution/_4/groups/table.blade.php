<div v-cloak class="position-relative row mb-3">

    <div :class="[is_loading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-05']">
        <div class="spinner-grow text-secondary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    <div class="col-8">

        @include('components.tables.solution._4.groups.header')

        <div v-for="(group, index) in groups" class="card mb-3">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col ml-n2">

                        <!-- Title -->
                        <p class="card-title d-flex mb-3">
                            <span class="h4 mb-0 mr-3">@{{ '#'+group.id }}</span>
                            <a class="h4 flex-grow-1 mb-0 text-dark" :href="getUrl(group.id)">@{{ group.name }}</a>
                            <span class="d-flex align-items-center small text-muted mb-0 font-size-0750">
                                <v-svg class="svg mr-1" src="{{ asset('assets/image/svg/light/clock.svg') }}" width="14" height="14"></v-svg>
                                @{{ group.updated_at }}
                            </span>
                        </p>

                        <!-- Text -->
                        <p class="card-text small text-muted mb-2">@{{ group.description }}</p>

                        <!-- percentage -->
                        <div class="align-items-center">
                            <div class="row align-items-center no-gutters">
                                <div class="col-auto">
                                    <div class="small mr-2">@{{ group.users_percentage+'%' }}</div>
                                </div>
                                <div class="col">
                                    <!-- Progress -->
                                    <div class="progress progress-sm">
                                        <div class="progress-bar" role="progressbar" :style="'width:'+group.users_percentage+'%'"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div> <!-- / .row -->
            </div> <!-- / .card-body -->

        </div>
    </div>
</div>
