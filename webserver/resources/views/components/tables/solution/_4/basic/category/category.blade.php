@push('js')

{{-- categories --}}

<script type="text/x-template" id="--categories">
    <div class="categories col-4">

        <div :class="[{active : category == 0}, 'category card cursor-pointer mb-2']" @click="changeCategory(0)">
            <div class="card-body d-flex flex-column p-0 small">
                <div class="d-flex pl-4 p-3">
                    <span class="flex-grow-1 white-space-nowrap overflow-hidden text-overflow-ellipsis">全部</span>
                </div>
            </div>
        </div>

        <div :class="[{active : category == -1}, {dragover : dragoverCategory == -1}, 'category card cursor-pointer mb-2']" @click="changeCategory(-1)" @dragover.prevent @dragenter.stop.prevent="dragenter(-1)" @dragleave.stop.prevent="dragleave(-1)" @drop.stop.prevent="drop(-1)">
            <div class="card-body d-flex flex-column p-0 small">
                <div class="d-flex pl-4 p-3">
                    <span class="flex-grow-1 white-space-nowrap overflow-hidden text-overflow-ellipsis">未分類</span>
                </div>
                <div class="new-category-btns row m-0">
                    <button type="button" class="col btn btn-light font-size-0750 rounded-0 p-0" @click.stop="createCategory('insert', -1)">
                        <span>插入類別</span>
                    </button>
                </div>
            </div>
        </div>

        <div v-for="cat in categories" :key="cat.category.id" :class="[{'d-none' : !cat.show} , {active : category == cat.category.id}, {extend : extend[cat.layer] && cat.category.id == extend[cat.layer].category.id}, {dragover : dragoverCategory == cat.category.id}, 'layer-'+cat.layer, 'category card cursor-pointer mb-2']" @click="changeCategory(cat.category.id)" @dragover.prevent @dragenter.stop.prevent="dragenter(cat.category.id)" @dragleave.stop.prevent="dragleave(cat.category.id)" @drop.stop.prevent="drop(cat.category.id)">
            <div class="card-body d-flex flex-column p-0 small">
                <div class="d-flex pl-4 p-3">
                    <span class="flex-grow-1 white-space-nowrap overflow-hidden text-overflow-ellipsis mr-2">@{{ cat.category.name }}</span>
                    <span class="align-items-center i-btn noselect text-muted mr-2 p-0" @click.stop="editName(cat.category)">
                        <v-svg src="{{ asset('assets/image/svg/light/pen.svg') }}" width="14" height="14"></v-svg>
                    </span>
                    <span class="align-items-center i-btn noselect text-muted mr-2 p-0" @click.stop="remove(cat.category)">
                        <v-svg src="{{ asset('assets/image/svg/light/trash-alt.svg') }}" width="14" height="14"></v-svg>
                    </span>
                    <span v-if="cat.category.start + 1 != cat.category.end" class="extend-btn d-inline-flex align-items-center noselect text-muted p-0" @click.stop="toggleExtend(cat)">
                        <v-svg src="{{ asset('assets/image/svg/regular/chevron-down.svg') }}" width="10" height="10"></v-svg>
                    </span>
                </div>
                <div class="new-category-btns row m-0">
                    <button type="button" class="col btn btn-light font-size-0750 rounded-0 p-0" @click.stop="createCategory('insert', cat.category.id)">
                        <span>插入類別</span>
                    </button>
                    <button v-if="cat.layer != 3 && cat.category.start + 1 == cat.category.end" type="button" class="col btn btn-light font-size-0750 rounded-0 p-0" @click.stop="createCategory('child', cat.category.id)">
                        <span>新增子類別</span>
                    </button>
                </div>
            </div>
        </div>

        {{-- 建立類別 modal --}}
        <div id="category-creating" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="categoryCreating" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">建立類別</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{-- 類別名稱 --}}
                        <div class="form-group position-relative d-flex flex-column m-0">
                            <div :class="[{'is-invalid' : createForm.name.invalid}, 'i-form-control d-flex align-items-center']">
                                <input id="name" type="text" :class="[{'is-invalid' : createForm.name.invalid}, 'form-control pr-6']" v-model="createForm.name.name" placeholder="請輸入類別名稱">
                                <button type="button" :class="[{'progress-bar-striped progress-bar-animated' : createFormSubmitting}, 'btn btn-primary btn-sm mr-2 position-absolute right']" @click="createFormSubmit">送出</button>
                            </div>
                            <span class="invalid-feedback" role="alert"><strong>@{{ createForm.name.info }}</strong></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- 編輯類別名稱 modal --}}
        <div id="category-naming" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="categoryNaming" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">編輯類別名稱</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{-- 類別名稱 --}}
                        <div class="form-group position-relative d-flex flex-column m-0">
                            <div :class="[{'is-invalid' : editForm.name.invalid}, 'i-form-control d-flex align-items-center']">
                                <input id="name" type="text" :class="[{'is-invalid' : editForm.name.invalid}, 'form-control pr-6']" v-model="editForm.name.name" placeholder="請輸入類別名稱">
                                <button type="button" :class="[{'progress-bar-striped progress-bar-animated' : editFormSubmitting}, 'btn btn-primary btn-sm mr-2 position-absolute right']" @click="editFormSubmit">送出</button>
                            </div>
                            <span class="invalid-feedback" role="alert"><strong>@{{ editForm.name.info }}</strong></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- 刪除類別 modal --}}
        <div id="category-removing" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="categoryRemoving" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">刪除類別</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <p>
                            <span>請選擇您要將該類別底下的項目移轉至哪一類別？</span>
                            <br>
                            <span class="text-warning">註：刪除父類別，子類別將一併刪除以及移轉。</span>
                        </p>

                        {{-- 移轉類別 --}}
                        <div class="form-group">
                            <h4>選擇移轉類別</h4>
                            <vue-multiselect :class="[{'is-invalid' : removeForm.category.invalid}, 'i-form-control hidden-arrow']" v-model="removeForm.category.value" track-by="value" label="name" placeholder="搜尋類別名稱" open-direction="bottom" :options="selectCategory.options" :searchable="true" :show-labels="false" :allow-empty="true" :multiple="false" :loading="selectCategory.isLoading" :internal-search="false" :options-limit="20" :limit-text="categorieslimitText" :max-height="200" :show-no-results="false" :hide-selected="true" @search-change="getTrasnferCategories">
                                <span slot="noResult">沒有相符的搜尋結果。</span>
                            </vue-multiselect>
                            <span class="invalid-feedback" role="alert"><strong>@{{ removeForm.category.info }}</strong></span>
                        </div>

                        <div>
                            <button type="button" :class="[{'progress-bar-striped progress-bar-animated' : removeFormSubmitting}, 'btn btn-primary text-center w-100']" @click="removeFormSubmit">刪除</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</script>

<script>
Vue.component('categories', {
    template: '#--categories',
    props: ['type', 'category', 'draggable'],
    data(){
        return {
            categories: [],

            dragoverCategory: 0,
            extend: {
                1: null,
                2: null,
            },

            counter: 0,

            // 建立類別
            createFormSubmitting: false,
            createForm: {
                type: {
                    type: '',
                },
                category: {
                    category: 0,
                },
                name: {
                    name: '',
                    info: '',
                    invalid: false,
                }
            },

            // 編輯類別名稱
            editFormSubmitting: false,
            edittingCategory: null,
            editForm: {
                name: {
                    name: '',
                    info: '',
                    invalid: false,
                }
            },

            // 刪除類別
            removeFormSubmitting: false,
            removingCategory: null,
            selectCategory: {
                isLoading: false,
                options: [],
            },
            removeForm: {
                category: {
                    value: null,
                    info: '',
                    invalid: false,
                },
            }
        };
    },
    methods: {
        getCategories(){
            this.$emit('set-is-category-loading', true);
            axios
            .get(route('solution.4.deployment.categories.index', {
                deployment: args.deployment,
                type: this.type,
            }))
            .then(response => {
                var vm = this,
                layer = 0,
                ends = [];
                this.categories = [];
                response.data.categories.forEach(function(category){
                    if(category.start == 1){
                        layer = 1;
                    }else{
                        layer ++;
                    }
                    ends.unshift(category.end);

                    vm.categories.push({
                        layer: layer,
                        category: category,
                        show: layer == 1,
                    });

                    if(category.start + 1 == category.end || category.end + 1 == ends[0]){
                        do{
                            end = ends.shift();
                            layer --;
                        }while(end + 1 == ends[0]);
                    }
                });

                if(this.extend[1]){
                    this.categories.filter(function(cat){
                        return cat.category.parent_id == vm.extend[1].category.parent_id && cat.layer == 2;
                    }).forEach(cat => cat.show = true);

                    if(this.extend[2]){
                        this.categories.filter(function(cat){
                            return cat.category.parent_id == vm.extend[1].category.parent_id && cat.category.start > vm.extend[1].category.start && cat.category.end < vm.extend[1].category.end;
                        }).forEach(cat => cat.show = true);
                    }
                }
                this.$emit('set-is-category-loading', false);
            }).catch(error => {
                this.$emit('set-is-category-loading', false);
                console.log(error);
            });
        },
        changeCategory(id){
            this.$emit('change-category', id);
        },
        toggleExtend(cat){
            if(cat.layer == 1) this.extend[2] = null;
            if(this.extend[cat.layer] && this.extend[cat.layer].category.id == cat.category.id){
                this.extend[cat.layer] = null;
            }else{
                this.extend[cat.layer] = cat;
            }

            // 隱藏 category
            if(cat.layer == 1){
                this.categories.filter(cat => cat.layer != 1 && cat.show).forEach(cat => cat.show = false);
            }else{
                this.categories.filter(cat => cat.layer == 3 && cat.show).forEach(cat => cat.show = false);
            }

            if(this.extend[1]){
                var vm = this;
                if(cat.layer == 1){
                    this.categories.filter(function(cat){
                        return cat.category.parent_id == vm.extend[1].category.parent_id && cat.layer == 2;
                    }).forEach(cat => cat.show = true);
                }else if(this.extend[2]){
                    this.categories.filter(function(cat){
                        return cat.category.parent_id == vm.extend[2].category.parent_id && cat.category.start > vm.extend[2].category.start && cat.category.end < vm.extend[2].category.end;
                    }).forEach(cat => cat.show = true);
                }
            }
        },
        dragenter(category_id){
            if(this.dragoverCategory != category_id) this.counter = 0;
            this.dragoverCategory = category_id;
            this.counter ++;
        },
        dragleave(category_id){
            this.counter --;
            if(this.counter <= 0) this.dragoverCategory = 0;
        },
        drop(category_id){
            this.counter = 0;
            this.dragoverCategory = 0;
            if(this.draggable.category_id != category_id){
                this.$emit('set-is-category-loading', true);

                axios.post(route('solution.4.deployment.categories.classify', args.deployment),
                {
                    _token: args._token,
                    category_id: category_id,
                    categoryable_type: this.type,
                    categoryable_id: this.draggable.id,
                }).then(response => {
                    this.$emit('refresh');
                    this.$emit('set-is-category-loading', false);
                }).catch(error => {
                    this.$emit('set-is-category-loading', false);
                    console.log(error);
                });
            }
        },
        createCategory(type, categoryId){
            this.createForm.type.type = type;
            this.createForm.category.category = categoryId;

            this.createForm.name.name = '';
            this.createForm.name.info = '';
            this.createForm.name.invalid = false;
            $("#category-creating").modal('show');
        },
        createFormSubmit(){
            if(!this.createFormSubmitting){
                this.createFormSubmitting = true;
                this.createForm.name.info = '';
                this.createForm.name.invalid = false;

                axios
                .post(route('solution.4.deployment.categories.store', args.deployment),
                {
                    _token: args._token,
                    type: this.type,
                    createForm: this.createForm,
                }).then(response => {
                    console.log(response);
                    if(response.data.success){
                        this.getCategories();
                        this.$emit('refresh');
                        $("#category-creating").modal('hide');
                    };
                    this.createFormSubmitting = false;
                })
                .catch(error => {
                    this.createFormSubmitting = false;
                    var errors = error.response.data.errors;
                    var invalid_crawler = null;
                    for(error in errors){
                        console.log(error);

                        // 找出最後一個「.」後的字串
                        var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                        // 如果是value，表示是select，再往前找一個value
                        if(last == 'value'){
                            select_last = error.match(/\.(value\.value)$/);
                            // 如果符合「.value.value」，表示是多層value
                            if(select_last) last = select_last.slice(-1)[0];
                        }

                        // 替換數字成以[]包覆表示
                        error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                        // 如果是area，更新invalid = true
                        if(area = error_ref.match(/^(.*)\.action/)){
                            eval('this.'+area.slice(-1)[0]).invalid = true;
                        }

                        info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        try{
                            this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                            this.$set(eval('this.'+invalid_ref), 'invalid', true);
                        }catch(e){

                        }
                    }
                });
            }
        },
        editName(category){
            this.edittingCategory = category;
            this.editForm.name.name = category.name;
            this.editForm.name.info = '';
            this.editForm.name.invalid = false;
            $("#category-naming").modal('show');
        },
        editFormSubmit(){
            if(!this.editFormSubmitting){
                this.editFormSubmitting = true;
                this.editForm.name.info = '';
                this.editForm.name.invalid = false;

                axios.post(route('solution.4.deployment.categories.name', [args.deployment, this.edittingCategory.id]),
                {
                    _token: args._token,
                    editForm: this.editForm,
                }).then(response => {
                    console.log(response);
                    if(response.data.success){
                        this.edittingCategory.name = response.data.name;
                        $("#category-naming").modal('hide');
                    };
                    this.editFormSubmitting = false;
                })
                .catch(error => {
                    this.editFormSubmitting = false;
                    var errors = error.response.data.errors;
                    var invalid_crawler = null;
                    for(error in errors){
                        console.log(error);

                        // 找出最後一個「.」後的字串
                        var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                        // 如果是value，表示是select，再往前找一個value
                        if(last == 'value'){
                            select_last = error.match(/\.(value\.value)$/);
                            // 如果符合「.value.value」，表示是多層value
                            if(select_last) last = select_last.slice(-1)[0];
                        }

                        // 替換數字成以[]包覆表示
                        error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                        // 如果是area，更新invalid = true
                        if(area = error_ref.match(/^(.*)\.action/)){
                            eval('this.'+area.slice(-1)[0]).invalid = true;
                        }

                        info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        try{
                            this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                            this.$set(eval('this.'+invalid_ref), 'invalid', true);
                        }catch(e){

                        }
                    }
                });
            }
        },
        categorieslimitText(category){
            return `and ${category} other categories`;
        },
        getTrasnferCategories(query){
            this.selectCategory.isLoading = true;
            axios
            .get(route('solution.4.deployment.categories.queryForSelect', {
                deployment: args.deployment,
                q: query,
                categoryable_type: this.type,
                exclude: this.removingCategory.id,
            }))
            .then(response => {
                this.selectCategory.options = [{
                    value: -1,
                    name: '未分類',
                }].concat(response.data);
                this.selectCategory.isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this.selectCategory.options = [];
                this.selectCategory.isLoading = false;
                console.log(error);
            });
        },
        remove(category){
            this.removingCategory = category;
            this.selectCategory.options = [];
            this.getTrasnferCategories();

            this.removeForm.category.value = null;
            this.removeForm.category.info = '';
            this.removeForm.category.invalid = false;
            $("#category-removing").modal('show');
        },
        removeFormSubmit(){
            if(!this.removeFormSubmitting){
                this.removeFormSubmitting = true;
                this.removeForm.category.info = '';
                this.removeForm.category.invalid = false;

                axios({
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': args._token,
                    },
                    url: route('solution.4.deployment.categories.delete', {
                        deployment: args.deployment,
                        categoryId: this.removingCategory.id,
                    }),
                    data: {
                        removeForm: this.removeForm,
                    },
                })
                .then(response => {
                    console.log(response);
                    if(response.data.success){
                        cat = this.categories.find(cat => cat.category.id == this.category);
                        if(cat && cat.category.parent_id == this.removingCategory.parent_id && cat.category.start >= this.removingCategory.start && cat.category.end <= this.removingCategory.end) this.changeCategory(0);

                        if(this.extend[1] && this.extend[1].category.id == this.removingCategory.id){
                            this.extend[1] = null;
                            this.extend[2] = null;
                        }else if(this.extend[2] && this.extend[2].category.id == this.removingCategory.id){
                            this.extend[2] = null;
                        }

                        $("#category-removing").modal('hide');
                        this.getCategories();
                        this.$emit('refresh');
                    }
                    this.removeFormSubmitting = false;
                })
                .catch(error => {
                    this.removeFormSubmitting = false;
                    var errors = error.response.data.errors;
                    var invalid_crawler = null;
                    for(error in errors){
                        console.log(error);

                        // 找出最後一個「.」後的字串
                        var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                        // 如果是value，表示是select，再往前找一個value
                        if(last == 'value'){
                            select_last = error.match(/\.(value\.value)$/);
                            // 如果符合「.value.value」，表示是多層value
                            if(select_last) last = select_last.slice(-1)[0];
                        }

                        // 替換數字成以[]包覆表示
                        error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                        // 如果是area，更新invalid = true
                        if(area = error_ref.match(/^(.*)\.action/)){
                            eval('this.'+area.slice(-1)[0]).invalid = true;
                        }

                        info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        try{
                            this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                            this.$set(eval('this.'+invalid_ref), 'invalid', true);
                        }catch(e){

                        }
                    }
                });
            }
        }
    },
    created(){
        this.getCategories();
    }
});
</script>

@endpush
