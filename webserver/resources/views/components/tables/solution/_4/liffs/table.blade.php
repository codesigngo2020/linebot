<div v-cloak class="table-responsive position-relative overflow-hidden">

    <div :class="[is_loading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-05']">
        <div class="spinner-grow text-secondary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    <div id="liffs-list" class="row">

        <div v-for="liff in liffs" class="col-12 col-lg-6">
            <div class="card">
                <div class="d-flex card-header">
                    <span class="font-weight-bold">@{{ liff.description + (liff.is_created_by_platform ? '（由平台建立）' : '') }}</span>
                    <!-- <div class="flex-initial">
                        <a href="#" class="btn btn-sm btn-white">編輯</a>
                    </div> -->
                    <div v-if="!liff.is_created_by_platform" class="flex-initial cursor-pointer ml-3" @click="remove(liff.id)">
                        <v-svg class="svg text-muted" src="{{ asset('assets/image/svg/regular/trash-alt.svg') }}" width="16" height="16"></v-svg>
                    </div>
                </div>
                <div class="card-body p-0">
                    <table class="table table-sm card-table border-0 white-space-nowrap table-layout-fixed text-left mt-3 mb-3">
                        <tbody class="list">

                            <tr>
                                <td class="text-muted p-2">Endpoint URL</td>
                                <td class="p-2">
                                    <p>@{{ 'line://app/'+liff.liff_id }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted p-2">LIFF URL</td>
                                <td class="p-2">
                                    <p>@{{ liff.view.url }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted p-2">Size</td>
                                <td class="p-2">
                                    <span class="text-uppercase">@{{ liff.view.type }}</span>
                                    <span v-if="liff.view.type == 'full'">（100% of device screen height）</span>
                                    <span v-else-if="liff.view.type == 'tall'">（80% of device screen height）</span>
                                    <span v-else-if="liff.view.type == 'compact'">（50% of device screen height）</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted p-2">BLE feature</td>
                                <td class="p-2">@{{ liff.features ? 'ON' : 'OFF' }}</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
