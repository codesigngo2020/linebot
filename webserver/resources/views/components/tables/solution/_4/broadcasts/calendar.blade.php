@push('js')

{{-- 推播日曆 --}}

<script type="text/x-template" id="--broadcast-calendar">

    <div id="broadcast-calendar" class="position-relative">

        <div :class="[isLoading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-05']">
            <div class="spinner-grow text-secondary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h4 class="card-header-title">@{{ date.year+' 年 '+date.month+' 月' }}</h4>

                <div class="col-auto flex-grow-0 p-0">
                    <a href="javascript:void(0);" class="btn btn-sm btn-white p-1" @click="lastMonth">
                        <v-svg class="svg" src="{{ asset('assets/image/svg/regular/chevron-left.svg') }}" width="12" height="12"></v-svg>
                    </a>
                </div>
                <div class="col-auto flex-grow-0 pl-2 pr-2">
                    <a href="javascript:void(0);" class="btn btn-sm btn-white pt-1 pb-1 pl-2 pr-2" @click="toToday">Today</a>
                </div>
                <div class="col-auto flex-grow-0 p-0">
                    <a href="javascript:void(0);" class="btn btn-sm btn-white p-1" @click="nextMonth">
                        <v-svg class="svg" src="{{ asset('assets/image/svg/regular/chevron-right.svg') }}" width="12" height="12"></v-svg>
                    </a>
                </div>
            </div>

            <div class="card-body p-0">
                <table class="table card-table table-nowrap">
                    <thead>
                        <tr>
                            <th v-for="week in weeks" scope="col">@{{ week }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(week, weekIndex) in calendar.dates.chunk(7)">
                            <th v-for="(day, dayIndex) in week">
                                <div>
                                    <div :class="[{'text-muted' : day.relation !== 0}, 'date']">@{{ (day.day == 1 || (day.relation == -1 && dayIndex == 0) ? day.month + ' 月 ' : '') }}<span :class="[{today : day.year == today.year && day.month == today.month && day.day == today.day}]">@{{ day.day }}</span>@{{' 日' }}</div>
                                    <div v-if="broadcasts[day.year+'-'+day.month+'-'+day.day]">
                                        <div v-if="!day.showAll" class="broadcasts">
                                            <a v-for="broadcast in broadcasts[day.year+'-'+day.month+'-'+day.day].slice(0,2)" class="badge badge-soft-secondary" :href="getUrl(broadcast.id)" target="_blank">@{{ broadcast.name }}</a>
                                        </div>
                                        <div v-if="day.showAll" class="broadcasts">
                                            <a v-for="broadcast in broadcasts[day.year+'-'+day.month+'-'+day.day]" class="badge badge-soft-secondary" :href="getUrl(broadcast.id)" target="_blank">@{{ broadcast.name }}</a>
                                        </div>
                                    </div>
                                    <div v-if="!day.showAll && broadcasts[day.year+'-'+day.month+'-'+day.day] && broadcasts[day.year+'-'+day.month+'-'+day.day].length > 2" class="more text-muted" @click="showAll(weekIndex * 7 + dayIndex)">...</div>
                                </div>
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</script>

<script>
Vue.component('broadcast-calendar', {
    template: '#--broadcast-calendar',
    data(){
        return {
            isLoading: false,

            weeks: ['MON','TUE','WED','THU','FRI','SAT','SUN'],
            days: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],

            startData: {year: 2001, month: 1, day: 1},
            date: {year: 2001, month: 1, day: 1},
            today: {year: 2001, month: 1, day: 1},

            calendar: {
                startWeekDay: 0,
                dates: [],
            },

            broadcasts: [],
        };
    },
    methods: {
        isLeapYear(year){
            return (year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0));
        },
        getWeekDayIndex(year, month, day){
            var yearsCount = year - this.startData.year - 1,
            leapYearsCount = Math.floor(yearsCount/4) - Math.floor(yearsCount/100) + Math.floor(yearsCount/400);

            // 2001/01/01到前年底的總天數
            daysCount = yearsCount * 365 + leapYearsCount

            // 今年到上個月的總天數
            for(var i = 0; i < month - 1; i ++){
                daysCount += this.days[i];
            }
            // 閏年月份大於2者，+1天
            if(month > 2 && this.isLeapYear(year)) daysCount ++;

            // 加上本月天數
            daysCount += day;
            return daysCount%7;
        },
        setToday(){
            d = new Date();
            this.today.year = d.getFullYear();
            this.today.month = d.getMonth() + 1;
            this.today.day = d.getDate();
        },
        lastMonth(){
            this.isLoading = true;
            if(this.date.month == 1){
                this.date.year --;
                this.date.month = 1;
            }else{
                this.date.month --;
            }
            this.date.day = 1;
            this.updateCalendar();
        },
        toToday(){
            this.isLoading = true;
            this.date.year = this.today.year;
            this.date.month = this.today.month;
            this.date.day = this.today.day;
            this.updateCalendar();
        },
        nextMonth(){
            this.isLoading = true;
            if(this.date.month == 12){
                this.date.year ++;
                this.date.month = 1;
            }else{
                this.date.month ++;
            }
            this.date.day = 1;
            this.updateCalendar();
        },
        getBroadcasts(){
            this.broadcasts = [];
            datesCount = this.calendar.dates.length;
            axios
            .get(route('solution.4.deployment.broadcasts.getBroadcastCalendar', {
                deployment: args.deployment,
                start: this.calendar.dates[0].year+'-'+this.calendar.dates[0].month+'-'+this.calendar.dates[0].day,
                end: this.calendar.dates[datesCount-1].year+'-'+this.calendar.dates[datesCount-1].month+'-'+this.calendar.dates[datesCount-1].day,
            }))
            .then(response => {
                this.broadcasts = response.data.broadcasts;
                this.isLoading = false;
            }).catch(error => {
                this.isLoading = false;
                console.log(error);
            });
        },
        updateCalendar(){
            this.calendar.dates = [];
            this.calendar.startWeekDay = this.getWeekDayIndex(this.date.year, this.date.month, 1);

            lastMonthYear = this.date.month == 1 ? this.date.year - 1 : this.date.year;
            lastMonth = this.date.month == 1 ? 12 : this.date.month - 1;

            for(var i = 0; i < this.calendar.startWeekDay; i ++){
                this.calendar.dates.push({
                    year: lastMonthYear,
                    month: lastMonth,
                    day: this.days[lastMonth-1] - this.calendar.startWeekDay + i + 1,
                    relation: -1,
                    showAll: false,
                });
            }

            thisMonthDays = this.days[this.date.month-1] + (this.date.month == 2 && this.isLeapYear(this.date.year) ? 1 : 0);
            for(var i = 1; i <= thisMonthDays; i ++){
                this.calendar.dates.push({
                    year: this.date.year,
                    month: this.date.month,
                    day: i,
                    relation: 0,
                    showAll: false,
                });
            }

            startWeekDayOfNextMonth = (this.calendar.startWeekDay + this.days[this.date.month-1] % 7) % 7;
            daysCountOfNextMonth = startWeekDayOfNextMonth === 0 ? 7 : 7 - startWeekDayOfNextMonth;

            nextMonthYear = this.date.month == 12 ? this.date.year + 1 : this.date.year;
            nextMonth = this.date.month == 12 ? 1 : this.date.month + 1;

            for(var i = 1; i <= daysCountOfNextMonth; i ++){
                this.calendar.dates.push({
                    year: nextMonthYear,
                    month: nextMonth,
                    day: i,
                    relation: 1,
                    showAll: false,
                });
            }

            this.getBroadcasts();
        },
        getUrl(id){
            return route('solution.4.deployment.broadcasts.show', {
                deployment: args.deployment,
                broadcastId: id,
            });
        },
        showAll(dateIndex){
            this.calendar.dates[dateIndex].showAll = true;
        }
    },
    created(){
        this.setToday();
        this.toToday();
    }
});
</script>

@endpush
