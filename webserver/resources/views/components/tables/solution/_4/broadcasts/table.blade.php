@push('js')

{{-- 推播日曆 --}}

<script type="text/x-template" id="--broadcast-list">


    <div id="broadcast-list">
        <div v-cloak class="position-relative row mb-3">

            <div :class="[isTableLoading || isCategoryLoading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-05']">
                <div class="spinner-grow text-secondary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>

            <div class="col-8">

                @include('components.tables.solution._4.broadcasts.header')

                <div v-for="broadcast in broadcasts" class="card mb-3" :draggable="draggable == broadcast">
                    <div class="card-body p-0">
                        <div class="row nomargin">
                            <div class="col ml-n2 pt-4 pb-4 pl-4 pr-4">

                                <!-- Title -->
                                <p class="card-title d-flex mb-3">
                                    <span class="h4 mb-0 mr-3">@{{ '#'+broadcast.id }}</span>
                                    <a class="h4 flex-grow-1 mb-0 text-dark" :href="getUrl(broadcast.id)">@{{ broadcast.name }}</a>
                                    <span class="target_type position-relative small mr-3 pr-3">
                                        <span class="badge badge-soft-primary">@{{ targetTypes[broadcast.target_type] }}</span>
                                    </span>
                                    <span class="position-relative small">
                                        <span class="mr-1">
                                            <v-svg class="svg text-warning" src="{{ asset('assets/image/svg/solid/tags.svg') }}" width="16" height="16"></v-svg>
                                        </span>
                                        <span>@{{ 'x '+broadcast.tags_count }}</span>
                                    </span>
                                </p>

                                <!-- Text -->
                                <p class="card-text small text-muted mb-2">@{{ broadcast.description }}</p>

                                <!-- percentage -->
                                <div class="align-items-center">
                                    <div class="row align-items-center no-gutters">
                                        <div class="col-auto">
                                            <div class="small mr-2">@{{ broadcast.users_percentage+'%' }}</div>
                                        </div>
                                        <div class="col">
                                            <!-- Progress -->
                                            <div class="progress progress-sm">
                                                <div class="progress-bar" role="progressbar" :style="'width:'+broadcast.users_percentage+'%'"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="message-content p-2">
                                <ul class="list-group">
                                    <li v-for="messageContent in broadcast.messages" :class="[{'has-quick-replay' : messageContent.hasQuickReplay}, 'list-group-item']">@{{ messageContent.type }}</li>
                                </ul>
                            </div>

                            <div class="category-draggable d-flex align-items-center text-muted cursor-grab noselect mr-2" @mouseenter="mouseenter(broadcast)" @mouseleave="mouseleave">
                                <v-svg src="{{ asset('assets/image/svg/light/grip-lines-vertical.svg') }}" width="16" height="22"></v-svg>
                            </div>

                        </div> <!-- / .row -->
                    </div> <!-- / .card-body -->
                </div>
            </div>

            @include('components.tables.solution._4.basic.category.category')
            <categories type="Broadcast" :category="category" :draggable="draggable" @set-is-category-loading="setIsCategoryLoading" @change-category="switchCategory" @refresh="refresh"></categories>
        </div>

        <nav v-cloak v-if="paginator.lastPage > 1" class="d-flex justify-content-center" aria-label="Page navigation">
            <ul class="pagination">
                <li v-if="paginator.currentPage >= 4" class="page-item" @click="changePage(paginator.currentPage - 3)">
                    <a class="page-link" href="javascript:void(0);" aria-label="Previous">
                        <span aria-hidden="true">...</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>

                <li v-for="i in paginator.currentPage + 2" v-if="i >= 1 && i <= paginator.lastPage && i >= paginator.currentPage - 2" :class="['page-item', {'active': i == paginator.currentPage }]" @click="changePage(i)">
                    <a class="page-link" href="javascript:void(0);">@{{ i }}</a>
                </li>

                <li v-if="paginator.currentPage <= paginator.lastPage - 3" class="page-item" @click="changePage(paginator.currentPage + 3)">
                    <a class="page-link" href="javascript:void(0);" aria-label="Next">
                        <span aria-hidden="true">...</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            </ul>
        </nav>

    </div>
</script>

<script>
Vue.component('broadcast-list', {
    template: '#--broadcast-list',
    data(){
        return {
            isTableLoading: false,

            isCategoryLoading: false,
            category: 0,

            draggable: null,

            query: '',
            broadcasts: [],
            paginator: {
                currentPage: 1,
                lastPage: 1,
            },
            order: {
                column: 'id',
                dir: 'desc',
            },

            targetTypes: {
                all: '全部好友',
                tags: '分眾標籤',
                users: '特定好友',
            },
        };
    },
    methods: {
        changePage(page){
            this.getBroadcasts(page);
        },
        search(){
            this.getBroadcasts(1);
        },
        refresh(){
            this.getBroadcasts(this.paginator.currentPage);
        },
        getBroadcasts(page){
            this.isTableLoading = true;
            axios
            .get(route('solution.4.deployment.broadcasts.getBroadcastList', {
                deployment: args.deployment,
                page: page,
                category: this.category,
                column: this.order.column,
                dir: this.order.dir,
                q: this.query,
            }))
            .then(response => {
                this.paginator = response.data.paginator;
                this.broadcasts = response.data.broadcasts;
                this.isTableLoading = false;
            }).catch(error => {
                this.isTableLoading = false;
                console.log(error);
            });
        },
        getUrl(id){
            return route('solution.4.deployment.broadcasts.show', {
                deployment: args.deployment,
                broadcastId: id,
            });
        },

        // category
        setIsCategoryLoading(isLoading){
            this.isCategoryLoading = isLoading;
        },
        switchCategory(id){
            this.category = id;
            this.getBroadcasts(1);
        },
        mouseenter(broadcast){
            this.draggable = broadcast;
        },
        mouseleave(){
            this.draggable = null;
        },
    },
    created(){
        this.getBroadcasts(1);
    }
});
</script>
@endpush
