@push('js')

{{-- 推播日曆 --}}

<script type="text/x-template" id="--auto-answer-calendar">

    <div id="auto-answer-calendar" class="position-relative">

        <div :class="[isLoading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-05']">
            <div class="spinner-grow text-secondary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h4 class="card-header-title">@{{ date.year+' 年 '+date.month+' 月' }}</h4>

                <div :class="[showActive ? 'badge-soft-info' : 'badge-soft-light', 'badge badge-soft-info text-dark flex-grow-0 mr-3 pl-3 pr-3 cursor-pointer']" @click="toggleActive">啟用中</div>
                <div :class="[showUnactive ? 'badge-light' : 'badge-soft-light', 'badge text-dark flex-grow-0 mr-4 pl-3 pr-3 cursor-pointer']" @click="toggleUnactive">已停用</div>

                <div class="col-auto flex-grow-0 p-0">
                    <a href="javascript:void(0);" class="btn btn-sm btn-white p-1" @click="lastMonth">
                        <v-svg class="svg" src="{{ asset('assets/image/svg/regular/chevron-left.svg') }}" width="12" height="12"></v-svg>
                    </a>
                </div>
                <div class="col-auto flex-grow-0 pl-2 pr-2">
                    <a href="javascript:void(0);" class="btn btn-sm btn-white pt-1 pb-1 pl-2 pr-2" @click="toToday">Today</a>
                </div>
                <div class="col-auto flex-grow-0 p-0">
                    <a href="javascript:void(0);" class="btn btn-sm btn-white p-1" @click="nextMonth">
                        <v-svg class="svg" src="{{ asset('assets/image/svg/regular/chevron-right.svg') }}" width="12" height="12"></v-svg>
                    </a>
                </div>
            </div>

            <div class="card-body p-0">
                <table class="table card-table table-nowrap">
                    <thead>
                        <tr>
                            <th v-for="week in weeks" scope="col">@{{ week }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="week in calendar.dates.chunk(7)">
                            <th v-for="(day, dayIndex) in week">
                                <div>
                                    <div :class="[{'text-muted' : day.relation !== 0}, 'date']">@{{ (day.day == 1 || (day.relation == -1 && dayIndex == 0) ? day.month + ' 月 ' : '') }}<span :class="[{today : day.year == today.year && day.month == today.month && day.day == today.day}]">@{{ day.day }}</span>@{{' 日' }}</div>
                                    <div v-if="day.autoAnswers" class="auto-answers">
                                        <a v-for="autoAnswer in day.autoAnswers" :class="[{'space' : autoAnswer.index == -1}, {ml : !autoAnswer.b[0]}, {mr : !autoAnswer.b[1]}, autoAnswer.index != -1 && autoAnswers[autoAnswer.index].active ? 'badge-soft-info' : 'badge-light', 'badge']" :href="autoAnswer.index == -1 ? 'javascript:void(0);' : getUrl(autoAnswers[autoAnswer.index].id)" :target="autoAnswer.index == -1 ? '_self' : '_blank'">@{{ autoAnswer.index != -1 && autoAnswer.b[0] == 0 ? autoAnswers[autoAnswer.index].name : '&nbsp;' }}</a>
                                    </div>
                                </div>
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</script>

<script>
Vue.component('auto-answer-calendar', {
    template: '#--auto-answer-calendar',
    data(){
        return {
            isLoading: false,

            showActive: true,
            showUnactive: false,

            weeks: ['MON','TUE','WED','THU','FRI','SAT','SUN'],
            days: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],

            startData: {year: 2001, month: 1, day: 1},
            date: {year: 2001, month: 1, day: 1},
            today: {year: 2001, month: 1, day: 1},

            calendar: {
                startWeekDay: 0,
                dates: [],
            },

            autoAnswers: [],
        };
    },
    methods: {
        isLeapYear(year){
            return (year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0));
        },
        getWeekDayIndex(year, month, day){
            var yearsCount = year - this.startData.year - 1,
            leapYearsCount = Math.floor(yearsCount/4) - Math.floor(yearsCount/100) + Math.floor(yearsCount/400);

            // 2001/01/01到前年底的總天數
            daysCount = yearsCount * 365 + leapYearsCount

            // 今年到上個月的總天數
            for(var i = 0; i < month - 1; i ++){
                daysCount += this.days[i];
            }
            // 閏年月份大於2者，+1天
            if(month > 2 && this.isLeapYear(year)) daysCount ++;

            // 加上本月天數
            daysCount += day;
            return daysCount%7;
        },
        setToday(){
            d = new Date();
            this.today.year = d.getFullYear();
            this.today.month = d.getMonth() + 1;
            this.today.day = d.getDate();
        },
        lastMonth(){
            this.isLoading = true;
            if(this.date.month == 1){
                this.date.year --;
                this.date.month = 1;
            }else{
                this.date.month --;
            }
            this.date.day = 1;
            this.updateCalendar();
        },
        toToday(){
            this.isLoading = true;
            this.date.year = this.today.year;
            this.date.month = this.today.month;
            this.date.day = this.today.day;
            this.updateCalendar();
        },
        nextMonth(){
            this.isLoading = true;
            if(this.date.month == 12){
                this.date.year ++;
                this.date.month = 1;
            }else{
                this.date.month ++;
            }
            this.date.day = 1;
            this.updateCalendar();
        },
        getAutoAnswers(){
            this.autoAnswers = [];
            datesCount = this.calendar.dates.length;
            axios
            .get(route('solution.4.deployment.autoAnswers.getAutoAnswerCalendar', {
                deployment: args.deployment,
                start: this.calendar.dates[0].year+'-'+this.calendar.dates[0].month+'-'+this.calendar.dates[0].day,
                end: this.calendar.dates[datesCount-1].year+'-'+this.calendar.dates[datesCount-1].month+'-'+this.calendar.dates[datesCount-1].day,
            }))
            .then(response => {
                this.autoAnswers = response.data.autoAnswers;
                this.renderCalendar();
                this.isLoading = false;
                console.log(response.data.autoAnswers);
            }).catch(error => {
                this.isLoading = false;
                console.log(error);
            });
        },
        renderCalendar(){
            var vm = this;
            this.autoAnswers.forEach(function(autoAnswer, autoAnswerIndex){

                if((vm.showActive && autoAnswer.active) || (vm.showUnactive && !autoAnswer.active)){

                    var started = 0,
                    existsInThisWeek = 0,
                    emptyDays = [];

                    vm.calendar.dates.forEach(function(date, dateIndex){

                        if(dateIndex % 7 == 0){
                            started = 0;
                            existsInThisWeek = 0;
                        }
                        existsToday = 0;

                        autoAnswer.periods.every(function(period){
                            datetime = date.year+'-'+('0'+date.month).slice(-2)+'-'+('0'+date.day).slice(-2)+' 00:00:00';

                            if(period.start_date <= datetime && (!period.end_date || period.end_date >= datetime)){
                                date.autoAnswers.push({
                                    index: autoAnswerIndex,
                                    b: [started, dateIndex % 7 == 6 ? 0 : 1],
                                });

                                started = 1;
                                existsInThisWeek = 1;
                                existsToday = 1;
                                return false;
                            }
                            return true;
                        });

                        if(!existsToday){
                            emptyDays.push(dateIndex);
                            if(started){
                                vm.calendar.dates[dateIndex-1].autoAnswers[vm.calendar.dates[dateIndex-1].autoAnswers.length-1].b[1] = 0;
                                started = 0;
                            }
                        }

                        if(dateIndex % 7 == 6 && existsInThisWeek){
                            emptyDays.forEach(function(day){
                                vm.calendar.dates[day].autoAnswers.push({
                                    index: -1,
                                    b: [0, 0],
                                });
                            });
                            emptyDays = [];
                        }

                    });

                }
            });
        },
        updateCalendar(){
            this.calendar.dates = [];
            this.calendar.startWeekDay = this.getWeekDayIndex(this.date.year, this.date.month, 1);

            lastMonthYear = this.date.month == 1 ? this.date.year - 1 : this.date.year;
            lastMonth = this.date.month == 1 ? 12 : this.date.month - 1;

            for(var i = 0; i < this.calendar.startWeekDay; i ++){
                this.calendar.dates.push({
                    year: lastMonthYear,
                    month: lastMonth,
                    day: this.days[lastMonth-1] - this.calendar.startWeekDay + i + 1,
                    relation: -1,
                    autoAnswers: [],
                });
            }

            thisMonthDays = this.days[this.date.month-1] + (this.date.month == 2 && this.isLeapYear(this.date.year) ? 1 : 0);
            for(var i = 1; i <= thisMonthDays; i ++){
                this.calendar.dates.push({
                    year: this.date.year,
                    month: this.date.month,
                    day: i,
                    relation: 0,
                    autoAnswers: [],
                });
            }

            startWeekDayOfNextMonth = (this.calendar.startWeekDay + this.days[this.date.month-1] % 7) % 7;
            daysCountOfNextMonth = startWeekDayOfNextMonth === 0 ? 7 : 7 - startWeekDayOfNextMonth;

            nextMonthYear = this.date.month == 12 ? this.date.year + 1 : this.date.year;
            nextMonth = this.date.month == 12 ? 1 : this.date.month + 1;

            for(var i = 1; i <= daysCountOfNextMonth; i ++){
                this.calendar.dates.push({
                    year: nextMonthYear,
                    month: nextMonth,
                    day: i,
                    relation: 1,
                    autoAnswers: [],
                });
            }

            this.getAutoAnswers();
        },
        toggleActive(){
            if(!this.showActive || this.showUnactive){
                this.showActive = !this.showActive;
                this.activeChanged();
            }
        },
        toggleUnactive(){
            if(!this.showUnactive || this.showActive){
                this.showUnactive = !this.showUnactive;
                this.activeChanged();
            }
        },
        activeChanged(){
            this.isLoading = true;
            this.calendar.dates.forEach(function(date){
                date.autoAnswers = [];
            });
            this.renderCalendar();
            this.isLoading = false;
        },
        getUrl(id){
            return route('solution.4.deployment.autoAnswers.show', {
                deployment: args.deployment,
                autoAnswerId: id,
            });
        },
    },
    created(){
        this.setToday();
        this.toToday();
    }
});
</script>

@endpush
