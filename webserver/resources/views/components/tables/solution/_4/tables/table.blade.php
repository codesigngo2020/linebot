<div v-cloak class="position-relative row mb-3">

    <div :class="[is_loading || isCategoryLoading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-05']">
        <div class="spinner-grow text-secondary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    <div class="col-8">

        @include('components.tables.solution._4.messages-pool.header')

        <div v-for="table in tables" class="card mb-3" :draggable="draggable == table">
            <div class="card-body p-0">
                <div class="row m-0">
                    <div class="col pl-4 pr-3 pt-4 pb-4">

                        <!-- Title -->
                        <div class="card-title d-flex align-items-center mb-3">
                            <span class="h4 mb-0 mr-3">@{{ '#'+table.id }}</span>
                            <span class="flex-grow-1">
                                <a class="h4 flex-grow-1 mb-0 text-dark" :href="getUrl(table.id)">@{{ table.name }}</a>
                            </span>

                            {{-- 資料筆數 --}}
                            <span class="data-counts position-relative small mr-3 pr-3">@{{ '資料筆數：'+table.dataCounts }}</span>

                            {{-- 類型 --}}
                            <span class="badge badge-soft-secondary">@{{ type[table.type] }}</span>

                            <div class="dropdown d-flex ml-3">
                                <a href="#" class="d-inline-flex align-items-center cursor-pointer dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <v-svg class="svg text-muted" src="{{ asset('assets/image/svg/light/ellipsis-v.svg') }}" width="22" height="22"></v-svg>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right p-0">
                                    <a href="javascript:void(0);" class="dropdown-item" @click="remove(table.id)">刪除資料表</a>
                                </div>
                            </div>
                        </div>

                        <!-- Text -->
                        <p class="card-text small text-muted mb-2">@{{ table.description }}</p>

                    </div>

                    <div class="category-draggable d-flex align-items-center text-muted cursor-grab noselect mr-2" @mouseenter="mouseenter(table)" @mouseleave="mouseleave">
                        <v-svg src="{{ asset('assets/image/svg/light/grip-lines-vertical.svg') }}" width="16" height="22"></v-svg>
                    </div>

                </div> <!-- / .row -->
            </div> <!-- / .card-body -->
        </div>
    </div>

    @include('components.tables.solution._4.basic.category.category')
    <categories type="Table" :category="category" :draggable="draggable" @set-is-category-loading="setIsCategoryLoading" @change-category="changeCategory" @refresh="refresh"></categories>
</div>
