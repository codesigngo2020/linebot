<div v-cloak id="deployments-table">

    <div class="position-relative row mb-3">

        <div :class="[is_loading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-05']">
            <div class="spinner-grow text-secondary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>

        <div class="col-12">

            <div class="card mt-3">
                <div class="card-header border-bottom-0">
                    <div class="row align-items-center">
                        <div class="col">

                            {{-- Search --}}
                            <form class="row align-items-center" @submit.prevent="search">
                                <div class="col-auto pr-0">
                                    <span class="text-muted">
                                        <v-svg src="{{ asset('assets/image/svg/light/search.svg') }}" width="18" height="18"></v-svg>
                                    </span>
                                </div>
                                <div class="col">
                                    <input type="search" class="form-control form-control-flush search" placeholder="Search" v-model="query" @change="search">
                                </div>
                            </form>

                        </div>
                    </div> {{-- / .row --}}
                </div>
            </div>

            <div v-for="deployment in deployments" class="card mb-3">
                <div class="card-body p-0">
                    <div class="row align-items-center flex-nowrap overflow-hidden">

                        <div class="col-auto ml-2 pt-4 pb-4 pl-4">
                            <span class="profile-picture">
                                <img :src="deployment.picture_url" class="rounded-circle">
                            </span>
                        </div>

                        <div class="deployment-info col flex-grow-1">

                            <!-- Title -->
                            <p class="card-title d-flex align-items-center flex-grow-1 mb-3">
                                <a class="d-inline-flex word-break-keep-all overflow-hidden h4 mb-0 mr-3 text-dark" :href="getUrl(deployment.id)">
                                    <span class="mr-3">@{{ deployment.name }}</span>
                                    <span class="d-inline-flex align-items-end font-size-0750">@{{ deployment.basic_id }}</span>
                                </a>
                            </p>

                            <!-- Text -->
                            <p class="card-text small text-muted mb-0">@{{ deployment.description }}</p>

                        </div>

                        {{-- QR code --}}
                        <div class="QR-code col-auto pr-4 mr-2">
                            <img :src="deployment.QRcodeUrl">
                        </div>
                    </div> <!-- / .row -->
                </div> <!-- / .card-body -->

            </div>
        </div>
    </div>

    <nav v-cloak v-if="paginator.lastPage > 1" class="d-flex justify-content-center" aria-label="Page navigation">
        <ul class="pagination">
            <li v-if="paginator.currentPage >= 4" class="page-item" @click="changePage(paginator.currentPage - 3)">
                <a class="page-link" href="javascript:void(0);" aria-label="Previous">
                    <span aria-hidden="true">...</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>

            <li v-for="i in paginator.currentPage + 2" v-if="i >= 1 && i <= paginator.lastPage && i >= paginator.currentPage - 2" :class="['page-item', {'active': i == paginator.currentPage }]" @click="changePage(i)">
                <a class="page-link" href="javascript:void(0);">@{{ i }}</a>
            </li>

            <li v-if="paginator.currentPage <= paginator.lastPage - 3" class="page-item" @click="changePage(paginator.currentPage + 3)">
                <a class="page-link" href="javascript:void(0);" aria-label="Next">
                    <span aria-hidden="true">...</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>
    </nav>

</div>
