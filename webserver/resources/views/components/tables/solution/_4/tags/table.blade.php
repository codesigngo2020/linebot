<div v-cloak class="position-relative row mb-3">

    <div :class="[is_loading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-05']">
        <div class="spinner-grow text-secondary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    <div class="col-8">

        @include('components.tables.solution._4.tags.header')

        <div v-for="tag in tags" class="card mb-3">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col ml-n2">

                        <!-- Title -->
                        <p class="card-title d-flex mb-3">
                            <span class="h4 mb-0 mr-3">@{{ '#'+tag.id }}</span>
                            <a class="h4 flex-grow-1 mb-0 text-dark" :href="getUrl(tag.id)">@{{ tag.name }}</a>
                            <span class="total-counts position-relative small">@{{ '總使用次數：'+tag.total_count }}</span>
                        </p>

                        <!-- Text -->
                        <p v-html="tag.description" class="card-text small text-muted mb-2"></p>

                        <!-- percentage -->
                        <div class="align-items-center">
                            <div class="row align-items-center no-gutters">
                                <div class="col-auto">
                                    <div class="small mr-2">@{{ tag.users_percentage+'%' }}</div>
                                </div>
                                <div class="col">
                                    <!-- Progress -->
                                    <div class="progress progress-sm">
                                        <div class="progress-bar" role="progressbar" :style="'width:'+tag.users_percentage+'%'"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div> <!-- / .row -->
            </div> <!-- / .card-body -->

        </div>
    </div>
</div>
