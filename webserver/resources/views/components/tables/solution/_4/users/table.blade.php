<div v-cloak class="position-relative row mb-3">

    <div :class="[is_loading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-05']">
        <div class="spinner-grow text-secondary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    <div class="col-8">

        @include('components.tables.solution._4.users.header')

        <div v-for="user in users" class="card mb-3">
            <div class="card-body">
                <div class="row align-items-center flex-nowrap overflow-hidden">

                    <div class="col-auto ml-n2">
                        <span class="profile-picture">
                            <img :src="user.picture_url" class="rounded-circle">
                        </span>
                    </div>

                    <div class="user-info col-auto flex-grow-1">

                        <!-- Title -->
                        <p class="card-title d-flex align-items-center mb-3">
                            <span class="h4 mb-0 mr-3">@{{ '#'+user.id }}</span>
                            <a class="d-flex flex-grow-1 word-break-keep-all overflow-hidden h4 mb-0 mr-3 text-dark" :href="getUrl(user.id)">
                                <span class="mr-3 white-space-nowrap">@{{ user.display_name }}</span>
                                <span class="text-overflow-ellipsis overflow-hidden">@{{ user.user_id }}</span>
                            </a>
                            <span class="d-flex small">
                                <span :class="[user.is_follow ? 'text-success' : 'text-warning', 'font-size-0750 mr-1']">●</span>
                                <span>@{{ user.is_follow ? 'Follow' : 'Unfollow' }}</span>
                            </span>
                        </p>

                        <!-- Text -->
                        <p :class="[user.groups.length > 0 ? 'mb-2' : 'mb-0', 'card-text small text-muted']">@{{ user.status_message == '' ? '（好友未輸入狀態訊息）' : user.status_message }}</p>

                        <div v-if="user.groups.length > 0" class="d-flex overflow-auto">
                            <span v-for="(group, groupIndex) in user.groups" :class="[{'ml-3' : groupIndex != 0}, 'badge badge-soft-secondary']">@{{ group.name }}</span>
                        </div>

                    </div>
                </div> <!-- / .row -->
            </div> <!-- / .card-body -->

        </div>
    </div>
</div>
