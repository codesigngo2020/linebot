<div class="card">
    <div class="card-body p-0">
        <div class="table-responsive position-relative">

            <div :class="[is_loading ? 'd-flex' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-bl-05 border-radius-br-05 z-index-1']">
                <div class="spinner-grow text-secondary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>

            <table class="table table-sm table-nowrap card-table text-left">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Roles</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="list">

                    <tr v-for="user in users">
                        <td>@{{ user.name }}</td>
                        <td>@{{ user.email }}</td>
                        <td>
                            <div v-for="role in user.roles">@{{ role.name }}</div>
                        </td>
                        <td>
                            <div class="text-secondary" @click="editUser(user)">
                                <v-svg class="svg" src="{{ asset('assets/image/svg/light/pen.svg') }}" width="16" height="16"></v-svg>
                            </div>
                        </td>
                        <td>
                            <div class="text-danger" @click="removeUser(user)">
                                <v-svg class="svg" src="{{ asset('assets/image/svg/light/user-minus.svg') }}" width="20" height="20"></v-svg>
                            </div>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>
