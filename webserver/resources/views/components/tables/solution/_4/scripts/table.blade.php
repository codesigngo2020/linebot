<div v-cloak class="position-relative row mb-3">

    <div :class="[is_loading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-05']">
        <div class="spinner-grow text-secondary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    <div class="col-8">

        @include('components.tables.solution._4.scripts.header')

        <div v-for="script in scripts" class="card mb-3" :draggable="draggable == script" @dragstart="dragstart">
            <div class="card-body p-0">
                <div class="row m-0">
                    <div class="col pl-4 pr-3 pt-4 pb-4">

                        <!-- Title -->
                        <div class="card-title d-flex align-items-center mb-3">
                            <span class="h4 mb-0 mr-3">@{{ '#'+script.parent_id }}</span>

                            <span class="h4 flex-grow-1 mb-0">
                                <a class="text-dark" :href="getUrl(script.id)">@{{ script.name }}</a>
                            </span>
                            <span class="total-counts position-relative small mr-3 pr-3">@{{ '總使用次數：'+script.total_count }}</span>

                            {{-- 版本數 --}}
                            <span v-if="script.versions_count > 1 && script.is_current_version" class="version position-relative small mr-3 pr-3">
                                <span class="badge badge-soft-info">@{{ 'v'+script.version }}</span>
                            </span>

                            {{-- 啟用狀態 --}}
                            <span class="d-flex align-items-center mr-2">
                                <span :class="[script.active ? 'text-success' : 'text-warning', 'font-size-0750']">●</span>
                            </span>

                            <div class="dropdown d-flex">
                                <a href="#" class="d-inline-flex align-items-center cursor-pointer dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <v-svg class="svg text-muted" src="{{ asset('assets/image/svg/light/ellipsis-v.svg') }}" width="22" height="22"></v-svg>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right p-0">
                                    <a href="javascript:void(0);" class="dropdown-item" @click="toggleActive(script.id)">@{{ script.active ? '停用' : '啟用' }}</a>
                                    <a href="javascript:void(0);" class="dropdown-item" @click="deleteAll(script.id)">刪除所有版本</a>
                                </div>
                            </div>
                        </div>

                        <!-- Text -->
                        <p class="card-text small text-muted mb-2">@{{ script.description }}</p>

                        <!-- percentage -->
                        <div class="align-items-center">
                            <div class="row align-items-center no-gutters">
                                <div class="col-auto">
                                    <div class="small mr-2">@{{ (script.users_percentage ? script.users_percentage : 0)+'%' }}</div>
                                </div>
                                <div class="col">
                                    <!-- Progress -->
                                    <div class="progress progress-sm">
                                        <div class="progress-bar" role="progressbar" :style="'width:'+script.users_percentage+'%'"></div>
                                    </div>
                                </div>
                                <div class="col-auto ml-5">
                                    <div :class="[{'cursor-not-allowed' : detail.isLoading} ,'small text-muted cursor-pointer']" @click="detail.isLoading ? null : showDetail(script.id)">more detail</div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="category-draggable d-flex align-items-center text-muted cursor-grab noselect mr-2" @mouseenter="mouseenter(script)" @mouseleave="mouseleave">
                        <v-svg src="{{ asset('assets/image/svg/light/grip-lines-vertical.svg') }}" width="16" height="22"></v-svg>
                    </div>

                </div> <!-- / .row -->
            </div> <!-- / .card-body -->

            <hr v-if="detail.id == script.id" class="m-0">

            <div v-if="detail.id == script.id" class="position-relative card-body pt-0">

                <div :class="[detail.isLoading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute top left justify-content-center align-items-center h-100 w-100 bg-secondary']">
                    <div class="spinner-grow text-secondary" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>

                <div class="d-flex justify-content-center">
                    <div class="close-detail" @click="detail.isLoading ? null : closeDetail()">
                        <v-svg class="svg" src="{{ asset('assets/image/svg/regular/chevron-up.svg') }}" width="12" height="12"></v-svg>
                    </div>
                </div>

                <div class="d-flex align-items-center mb-3">
                    <span class="card-header-title flex-grow-0 font-size-0875 mr-3">@{{ detail.date.year+' 年 '+('0'+detail.date.month).slice(-2)+' 月 '+('0'+detail.date.day).slice(-2)+' 日' }}</span>

                    <div class="col-auto flex-grow-0 p-0">
                        <a href="javascript:void(0);" class="btn btn-sm btn-white pr-1 pl-1 p-0" @click="last">
                            <span class="d-inline-flex">
                                <v-svg class="svg" src="{{ asset('assets/image/svg/regular/chevron-left.svg') }}" width="8" height="8"></v-svg>
                            </span>
                        </a>
                    </div>
                    <div class="col-auto flex-grow-0 pl-2 pr-2">
                        <a href="javascript:void(0);" class="btn btn-sm btn-white pl-2 pr-2 p-0" @click="toTodayOrThisWeek(true)">@{{ detail.type == 'week' ? 'This week' : 'Today' }}</a>
                    </div>
                    <div class="col-auto flex-grow-0 p-0">
                        <a href="javascript:void(0);" class="btn btn-sm btn-white pr-1 pl-1 p-0" @click="(detail.type == 'week' && detail.date.offset < detail.thisWeekOffset) || (detail.type == 'hour' && detail.date.offset < 0) ? next() : null">
                            <span class="d-inline-flex">
                                <v-svg class="svg" src="{{ asset('assets/image/svg/regular/chevron-right.svg') }}" width="8" height="8"></v-svg>
                            </span>
                        </a>
                    </div>

                    <div class="d-flex justify-content-end flex-grow-1">
                        <div class="btn-group">
                            <button type="button" :class="[{active : detail.type == 'week'}, 'type btn btn-outline-secondary border-right-0 pl-2 pr-2 p-1']" @click="changeType('week')">
                                <v-svg class="svg" src="{{ asset('assets/image/svg/light/calendar-week.svg') }}" width="14" height="14"></v-svg>
                            </button>
                            <button type="button" :class="[{active : detail.type == 'hour'}, 'type btn btn-outline-secondary border-left-0 pl-2 pr-2 p-1']" @click="changeType('hour')">
                                <v-svg class="svg" src="{{ asset('assets/image/svg/light/clock.svg') }}" width="14" height="14"></v-svg>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="chart">
                    <canvas id="script-chart" class="chart-canvas"></canvas>
                </div>
            </div> <!-- / .card-body -->
        </div>
    </div>

    @include('components.tables.solution._4.basic.category.category')
    <categories type="Script" :category="category" :draggable="draggable" @set-is-category-loading="setIsCategoryLoading" @change-category="changeCategory" @refresh="refresh"></categories>
</div>
