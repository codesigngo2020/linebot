@foreach(
$deployment->notifications()
->select('title', 'text', 'class')
->where('status', 'active')
->where('start_at', '<', date('Y-m-d H:i:s'))
->where(function($q){
    $q->whereNull('end_at')
    ->orWhere('end_at', '>', date('Y-m-d H:i:s'));
})
->get() as $notification)

<div class="alert alert-{{ $notification->class }} alert-dismissible fade show" role="alert">
    <h4 class="alert-heading">{{ $notification->title }}</h4>
    <p class="nomargin">{{ $notification->text }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>

@endforeach
