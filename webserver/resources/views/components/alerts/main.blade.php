{{-- solution4 開始，每頁上方載入的提示訊息 --}}

@if(Session::has('alerts'))

@foreach(Session::get('alerts') as $alert)

<div class="alert alert-{{ $alert['class'] }} alert-dismissible fade show" role="alert">

    @if(isset($alert['title']))
    <h4 class="alert-heading">{{ $alert['title'] }}</h4>
    @endif

    <p class="d-flex align-items-center nomargin">{!! $alert['text'] !!}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>

@endforeach

@php
Session::forget('alerts');
@endphp

@endif
