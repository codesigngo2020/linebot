<div id="nav-table-{{ $key + 1 }}" class="tab-pane fade {{ $key == 0 ? 'show active' : '' }}" role="tabpanel" aria-labelledby="nav-table-{{ $key + 1 }}-tab">
  <div class="card">
    @include('components.tables.solution._2.posts.header')
    @include('components.tables.solution._2.posts.table')
  </div>

  <nav v-cloak v-if="data.paginator.lastPage > 1" class="d-flex justify-content-center" aria-label="Page navigation">
    <ul class="pagination">
      <li v-if="data.paginator.currentPage >= 4" class="page-item" @click="changePage(data.paginator.currentPage - 3)">
        <a class="page-link" href="javascript:void(0);" aria-label="Previous">
          <span aria-hidden="true">...</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>

      <li v-for="i in data.paginator.currentPage + 2" v-if="i >= 1 && i <= data.paginator.lastPage && i >= data.paginator.currentPage - 2" :class="['page-item', {'active': i == data.paginator.currentPage }]" @click="changePage(i)">
        <a class="page-link" href="javascript:void(0);">@{{ i }}</a>
      </li>

      <li v-if="data.paginator.lastPage > 5 && data.paginator.currentPage < data.paginator.lastPage - 3" class="page-item" @click="changePage(data.paginator.currentPage + 3)">
        <a class="page-link" href="javascript:void(0);" aria-label="Next">
          <span aria-hidden="true">...</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav>

</div>

@push('js')
<script>
var nav_table_{{ $key + 1 }} = new Vue({
  el: '#nav-table-{{ $key + 1 }}',
  data: {
    query: '',
    data: {!! $crawler->toJson() !!},
    is_loading: false,
    order: {
      column: 'id',
      dir: 'desc',
    },
    selectAll: false,
    selectedPosts: [],
  },
  methods: {
    changePage(page){
      this.getPosts(page, false);
    },
    search(){
      this.getPosts(1, false);
    },
    sort(column){
      if(column != this.order.column){
        this.order.column = column;
        this.order.dir = 'asc';
      }else{
        this.order.dir = this.order.dir == 'asc' ? 'desc' : 'asc';
      }
      this.getPosts(1, false);
    },
    getPosts(page, force){
      if(!this.is_loading || force){
        this.is_loading = true;
        this.selectAll = false;
        this.selectedPosts = [];
        axios
        .get(route('solution.2.deployment.posts.getPosts', {
          deployment: args.deployment,
          crawler: this.data.id,
          page: page,
          column: this.order.column,
          dir: this.order.dir,
          q: this.query,
        }))
        .then(response => {
          this.data.paginator = response.data.paginator;
          this.data.posts = response.data.posts;
          this.is_loading = false;
        }).catch(error => {
          this.is_loading = false;
          console.log(error);
        });
      }
    },
    changeSelectAll(){
      if(this.selectAll){
        this.selectedPosts = this.data.posts.map(post => post.id);
      }else{
        this.selectedPosts = [];
      }
    },
    repost(){
      this.action('repost');
    },
    ignore(){
      this.action('ignore');
    },
    action(action){
      if(!this.is_loading){
        this.is_loading = true;
        axios
        .patch(route('solution.2.deployment.posts.action', {
          deployment: args.deployment,
        }), {
          _token: args._token,
          action: action,
          posts: this.selectedPosts,
        })
        .then(response => {
          this.getPosts(this.data.paginator.currentPage, true);
        }).catch(error => {
          this.is_loading = false;
          console.log(error);
        });
      }
    }
  },
  watch: {
    selectedPosts(newValue, oldValue){
      if(newValue.length < oldValue.length){
        this.selectAll = false;
      }else if(newValue.length == this.data.posts.length){
        this.selectAll = true;
      }
    }
  }
});
</script>
@endpush
