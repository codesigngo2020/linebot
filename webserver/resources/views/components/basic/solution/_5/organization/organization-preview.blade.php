@push('js')

{{-- 編輯組織架構 --}}

<script type="text/x-template" id="--organization-preview">
    <div id="organization-preview" class="overflow-hidden">

        <div id="tree" class="overflow-auto">
            <branch :nodes="organization"></branch>
        </div>

    </div>
</script>

<script>
Vue.component('organization-preview', {
    template: '#--organization-preview',
    props: ['organization'],
    data(){
        return {}
    },
    methods: {
        getCurrentNodeId(){
            return this.organization.reduce((max, node) => Math.max(max, node.id), 0);
        },
    },
    created(){
        this.currentNodeId = this.getCurrentNodeId();
    }
});
</script>

@endpush
