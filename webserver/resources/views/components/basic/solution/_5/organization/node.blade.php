@push('js')

{{-- 腳本節點 --}}

<script type="text/x-template" id="--node">
    <div class="node">
        <div class="message-label mb-0 d-flex flex-column">
            <p class="badge badge-soft-secondary">@{{ node.name }}</p>
        </div>
    </div>
</script>

<script>
Vue.component('node', {
    template: '#--node',
    props: ['node'],
});
</script>
@endpush
