{{-- Image message --}}

@push('js')

<script type="text/x-template" id="--image-message">
    <div class="d-flex message image-message row nomargin">
        <div class="col-auto width-400 pt-0">
            <div :class="[{'border-radius-tl-05 border-radius-tr-05' : index == 0}, 'bg-edf2f9 pl-4 pt-5 h-100']">
                <div class="type text-muted mb-2 font-size-0875">Image message</div>
                <div class="content d-flex">

                    <div class="i-form-control card mb-0 border-0">
                        <div class="card-body text-justify p-0">
                            <img :src="message.originalContentUrl">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</script>

<script>
Vue.component('image-message', {
    template: '#--image-message',
    props: ['index', 'tagId', 'message', 'messageFormData', 'tagsData', 'usersCount', 'showActions', 'showTags'],
});
</script>

@endpush
