{{-- Text message --}}

@push('js')

<script type="text/x-template" id="--text-message">
    <div class="d-flex message text-message row nomargin">
        <div :class="[{'pt-0' : index == 0}, 'col-auto width-400']">
            <div class="bg-edf2f9 border-radius-tl-05 border-radius-tr-05 pl-4 pt-5">
                <div class="type text-muted mb-2 font-size-0875">Text message</div>
                <div class="content">

                    <div class="i-form-control card width-326 mb-0">
                        <div class="card-body text-justify pl-4 pr-4 p-3">
                            <div class="white-space-pre-line">@{{ message.text }}</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</script>

<script>
Vue.component('text-message', {
    template: '#--text-message',
    props: ['index', 'tagId', 'message', 'messageFormData', 'tagsData', 'usersCount', 'showActions', 'showTags'],
});
</script>

@endpush
