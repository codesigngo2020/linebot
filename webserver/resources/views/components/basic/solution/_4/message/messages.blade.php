{{-- Normal Messages --}}

@foreach(['text', 'image', 'imagemap', 'video', 'imagemap', 'buttons', 'confirm', 'carousel', 'imagecarousel', 'quickreply'] as $type)
@include('components.basic.solution._4.message.messages.normal.'.$type)
@endforeach



{{-- Advanced Messages --}}

@foreach(['coupon'] as $type)
@include('components.basic.solution._4.message.messages.advanced.'.$type)
@endforeach
