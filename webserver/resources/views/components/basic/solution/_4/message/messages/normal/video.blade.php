{{-- Video message --}}

@push('js')

<script type="text/x-template" id="--video-message">
    <div class="d-flex message video-message row nomargin">
        <div class="col-auto width-400 pt-0">
            <div :class="[{'border-radius-tl-05 border-radius-tr-05' : index == 0}, 'bg-edf2f9 pl-4 pt-5 h-100']">
                <div class="type text-muted mb-2 font-size-0875">Video message</div>
                <div class="content d-flex">

                    <div class="i-form-control card mb-0 border-0">
                        <div class="card-body d-flex text-justify p-0">
                            <video class="border-radius-05" controls>
                                <source :src="message.originalContentUrl" type="video/mp4"/>
                                Your browser does not support HTML5 video.
                            </video>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</script>

<script>
Vue.component('video-message', {
    template: '#--video-message',
    props: ['index', 'tagId', 'message', 'messageFormData', 'tagsData', 'usersCount', 'showActions', 'showTags'],
});
</script>

@endpush
