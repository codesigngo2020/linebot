@push('js')

{{-- 行為動作 --}}

<script type="text/x-template" id="--action-list">
    <div class="action-list col-auto flex-grow-1 pl-4">
        <div class="card">
            <div class="card-header">
                <h4 class="card-header-title">觸發行為</h4>
            </div>
            <div class="card-body p-2">
                <div class="table-responsive">
                    <table class="table bg-transparent text-left">
                        <tbody>
                            <tr v-for="(action, actionIndex) in actionList">
                                <th :class="[{hover : hoverId == action.id}, 'align-top']">@{{ '# '+action.id }}</th>
                                <td>
                                    <p :class="[{ 'm-0' : ['camera', 'cameraRoll'].includes(action.type.value) }, 'h5']">
                                        <span>@{{ action.type.name }}</span>
                                        <a v-if="showTags && ['message', 'liff', 'script', 'richmenu', 'next'].includes(action.type.value)" class="small ml-3" :href="getTagUrl(action.tag)" target="_blank">
                                            <v-svg :class="[tagId == action.tag ? 'text-2451010' : 'text-warning', 'svg']" src="{{ asset('assets/image/svg/solid/tags.svg') }}" width="16" height="16"></v-svg>
                                        </a>
                                        <span v-if="showTags && ['message', 'liff', 'script', 'richmenu', 'next'].includes(action.type.value)" class="small cursor-pointer ml-3" @click="toggleShowActionDetail(actionIndex)">
                                            <v-svg :class="[action.show ? 'text-primary' : 'text-muted', 'svg']" src="{{ asset('assets/image/svg/light/info-circle.svg') }}" width="16" height="16"></v-svg>
                                        </span>
                                    </p>
                                    <div v-if="action.type.value == 'next-with-message'">
                                        <p>
                                            <span>訊息內容：</span>
                                            <span class="white-space-pre-line">@{{ action.action.text.text }}</span>
                                        </p>
                                    </div>
                                    <div v-else-if="action.type.value == 'message'">
                                        <p>
                                            <span>訊息內容：</span>
                                            <span class="white-space-pre-line">@{{ action.action.text.text }}</span>
                                        </p>
                                        {{-- 點擊人數佔比 --}}
                                        <div v-if="action.show" class="align-items-center no-gutters mt-2">
                                            <div class="small mb-2">
                                                <span class="mr-2">@{{ '點擊人數佔比：'+( usersCount == 0 ? 0 : Math.floor(tagsData[action.tag].users_count*100/usersCount) )+'%（'+tagsData[action.tag].users_count+' / '+usersCount+'）' }}</span>
                                                <span>@{{ '總點擊次數：'+tagsData[action.tag].total_count }}</span>
                                            </div>
                                            <div class="col-12">
                                                <!-- Progress -->
                                                <div class="progress progress-sm">
                                                    <div class="progress-bar" role="progressbar" :style="'width:'+( usersCount == 0 ? 0 : Math.floor(tagsData[action.tag].users_count*100/usersCount) )+'%'"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div v-else-if="action.type.value == 'keyword'">
                                        <p>
                                            <span>關鍵字名稱：</span>
                                            <a v-if="action.action.keyword" class="text-dark text-decoration-underline m-0" :href="getKeywordUrl(action.action.keyword.id)" target="_blank">@{{ action.action.keyword.name + getVersionMessage('關鍵字', action.action.keyword) }}</a>
                                            <span v-else>@{{ action.action.text.text }}</span>
                                        </p>
                                    </div>
                                    <div v-else-if="action.type.value == 'uri'">
                                        <p>
                                            <span>連結網址：</span>
                                            <a class="text-dark text-decoration-underline m-0" :href="action.action.uri.uri" target="_blank">@{{ action.action.uri.uri }}</a>
                                        </p>
                                    </div>
                                    <div v-else-if="action.type.value == 'liff'">
                                        <p>
                                            <span>Liff 尺寸：</span>
                                            <span>@{{ action.action.size.value.value+' %' }}</span>
                                        </p>
                                        <p>
                                            <span>連結網址：</span>
                                            <a class="text-dark text-decoration-underline m-0" :href="action.action.uri.uri" target="_blank">@{{ action.action.uri.uri }}</a>
                                        </p>
                                        {{-- 點擊人數佔比 --}}
                                        <div v-if="action.show" class="align-items-center no-gutters mt-2">
                                            <div class="small mb-2">
                                                <span class="mr-2">@{{ '點擊人數佔比：'+( usersCount == 0 ? 0 : Math.floor(tagsData[action.tag].users_count*100/usersCount) )+'%（'+tagsData[action.tag].users_count+' / '+usersCount+'）' }}</span>
                                                <span>@{{ '總點擊次數：'+tagsData[action.tag].total_count }}</span>
                                            </div>
                                            <div class="col-12">
                                                <!-- Progress -->
                                                <div class="progress progress-sm">
                                                    <div class="progress-bar" role="progressbar" :style="'width:'+( usersCount == 0 ? 0 : Math.floor(tagsData[action.tag].users_count*100/usersCount) )+'%'"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div v-else-if="action.type.value == 'script'">
                                        <p>
                                            <span>腳本名稱：</span>
                                            <a class="text-dark text-decoration-underline m-0" :href="getScriptUrl(action.action.script.value.value)" target="_blank">@{{ action.action.script.value.name }}</a>
                                        </p>

                                        {{-- 點擊人數佔比 --}}
                                        <div v-if="action.show" class="align-items-center no-gutters mt-2">
                                            <div class="small mb-2">
                                                <span class="mr-2">@{{ '點擊人數佔比：'+( usersCount == 0 ? 0 : Math.floor(tagsData[action.tag].users_count*100/usersCount) )+'%（'+tagsData[action.tag].users_count+' / '+usersCount+'）' }}</span>
                                                <span>@{{ '總點擊次數：'+tagsData[action.tag].total_count }}</span>
                                            </div>
                                            <div class="col-12">
                                                <!-- Progress -->
                                                <div class="progress progress-sm">
                                                    <div class="progress-bar" role="progressbar" :style="'width:'+( usersCount == 0 ? 0 :Math.floor(tagsData[action.tag].users_count*100/usersCount) )+'%'"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div v-else-if="action.type.value == 'richmenu'">
                                        <p>
                                            <span>主選單名稱：</span>
                                            <a class="text-dark text-decoration-underline m-0" :href="getRichmenuUrl(action.action.richmenu.id)" target="_blank">@{{ action.action.richmenu.name + getVersionMessage('選單', action.action.richmenu) }}</a>
                                        </p>

                                        {{-- 點擊人數佔比 --}}
                                        <div v-if="action.show" class="align-items-center no-gutters mt-2">
                                            <div class="small mb-2">
                                                <span class="mr-2">@{{ '點擊人數佔比：'+( usersCount == 0 ? 0 : Math.floor(tagsData[action.tag].users_count*100/usersCount) )+'%（'+tagsData[action.tag].users_count+' / '+usersCount+'）' }}</span>
                                                <span>@{{ '總點擊次數：'+tagsData[action.tag].total_count }}</span>
                                            </div>
                                            <div class="col-12">
                                                <!-- Progress -->
                                                <div class="progress progress-sm">
                                                    <div class="progress-bar" role="progressbar" :style="'width:'+( usersCount == 0 ? 0 :Math.floor(tagsData[action.tag].users_count*100/usersCount) )+'%'"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div v-else-if="action.type.value == 'next'">
                                        {{-- 點擊人數佔比 --}}
                                        <div v-if="action.show" class="align-items-center no-gutters mt-2">
                                            <div class="small mb-2">
                                                <span class="mr-2">@{{ '點擊人數佔比：'+( usersCount == 0 ? 0 : Math.floor(tagsData[action.tag].users_count*100/usersCount) )+'%（'+tagsData[action.tag].users_count+' / '+usersCount+'）' }}</span>
                                                <span>@{{ '總點擊次數：'+tagsData[action.tag].total_count }}</span>
                                            </div>
                                            <div class="col-12">
                                                <!-- Progress -->
                                                <div class="progress progress-sm">
                                                    <div class="progress-bar" role="progressbar" :style="'width:'+( usersCount == 0 ? 0 :Math.floor(tagsData[action.tag].users_count*100/usersCount) )+'%'"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</script>

<script>
Vue.component('action-list', {
    template: '#--action-list',
    props: ['tagId' ,'actionList', 'hoverId', 'tagsData', 'usersCount', 'showTags'],
    methods: {
        toggleShowActionDetail(actionIndex){
            this.$emit('toggleShowActionDetail', actionIndex);
        },
        getTagUrl(id){
            return route('solution.4.deployment.tags.show', {
                deployment: args.deployment,
                tagId: id,
            });
        },
        getKeywordUrl(id){
            return route('solution.4.deployment.keywords.show', {
                deployment: args.deployment,
                keywordId: id,
            });
        },
        getScriptUrl(id){
            return route('solution.4.deployment.scripts.show', {
                deployment: args.deployment,
                scriptId: id,
            });
        },
        getRichmenuUrl(id){
            return route('solution.4.deployment.richmenus.show', {
                deployment: args.deployment,
                richmenuId: id,
            });
        },
        getVersionMessage(name, model){
            if(model.deleted_all_at){
                return '（'+name+'已刪除）';
            }else if(!model.is_current_version){
                return '（未指定'+name+'版本）';
            }else{
                return '';
            }
        }
    }
});
</script>

@endpush
