{{-- Confirm message --}}

@push('js')

<script type="text/x-template" id="--confirm-message">
    <div class="d-flex message confirm-message row nomargin">

        <div class="col-auto width-400 pt-0">
            <div :class="[{'border-radius-tl-05 border-radius-tr-05' : index == 0}, 'bg-edf2f9 pl-4 pt-5 h-100']">
                <div class="type text-muted mb-2 font-size-0875">Confirm message</div>
                <div class="content">

                    <div class="i-form-control card mb-0 width-326">
                        <div class="card-body text-justify p-0">

                            <div class="text white-space-pre-line pl-4 pr-4 p-3">@{{ message.template.text }}</div>

                            <hr class="m-0">

                            <div class="actions d-flex mt-2 mb-2">
                                <div v-for="action in messageFormData.message.actions" class="position-relative d-flex justify-content-center align-items-center flex-grow-1 pl-4 pr-4 p-2" @mouseenter="mouseenter(action.id)" @mouseleave="mouseleave">
                                    <div class="label">@{{ action.action.label.label }}</div>
                                    <div class="position-absolute left d-flex mr-2 pl-2 bg-white">
                                        <div class="text-muted ml-2 font-size-0875">@{{ '#'+action.id }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        {{-- 行為動作 --}}
        <action-list v-if="showActions && actionList.length > 0" :tagId="tagId" :actionList="actionList" :hoverId="hoverId" :tagsData="tagsData" :usersCount="usersCount" :showTags="showTags" @toggleShowActionDetail="toggleShowActionDetail"></action-list>

    </div>
</script>

<script>
Vue.component('confirm-message', {
    template: '#--confirm-message',
    props: ['index', 'tagId', 'message', 'messageFormData', 'tagsData', 'usersCount', 'showActions', 'showTags'],
    data(){
        return {
            actionList: [],
            hoverId: 0,
        };
    },
    methods: {
        mouseenter(id){
            this.hoverId = id;
        },
        mouseleave(){
            this.hoverId = 0;
        },
        toggleShowActionDetail(actionIndex){
            this.actionList[actionIndex].show = !this.actionList[actionIndex].show;
        }
    },
    created(){
        var vm = this;
        this.messageFormData.message.actions.forEach(function(action){
            vm.actionList.push($.extend(true, {show: false}, action));
        });

        this.actionList.sort(function(a, b){
            return parseInt(a.id) > parseInt(b.id) ? 1 : -1;
        });
    }
});
</script>

@endpush
