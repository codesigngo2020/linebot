@push('js')

{{-- 訊息瀏覽 --}}

<script type="text/x-template" id="--message-preview">
    <div id="message-preview" class="card border-0 bg-transparent">
        <div class="card-header bg-white width-400 pt-4 pb-4 border-bottom-0 border-radius-tl-05 border-radius-tr-05">
            <h3 class="card-header-title">訊息內容</h3>
        </div>
        <div class="card-body p-0">

            <div class="position-relative overflow-hidden pt-0">

                <component v-for="(message, index) in messages" :key="nodeId+'-'+index" :is="getMessageType(index)+'-message'" :index="index" :tagId="tagid" :message="message" :messageFormData="messagesformdata[index]" :tagsData="tagsdata" :usersCount="userscount" :showActions="showactions" :showTags="showtags"></component>

                <quickreply-message v-if="messages[messages.length-1].quickReply" :tagId="tagid" :quickReply="messages[messages.length-1].quickReply" :quickReplyFormData="messagesformdata[messagesformdata.length-1]" :tagsData="tagsdata" :usersCount="userscount" :showActions="showactions" :showTags="showtags"></quickreply-message>

            </div>

        </div>
    </div>
</script>

<script>
Vue.component('message-preview', {
    template: '#--message-preview',
    props: ['tagid', 'messages', 'messagesformdata', 'tagsdata', 'userscount', 'showactions', 'showtags'],
    data(){
        return {
            nodeId: 1,
        };
    },
    methods: {
        getMessageType(index){
            if(this.messages[index].type != 'template') return this.messages[index].type;
            return this.messages[index].template.type == 'image_carousel' ? 'imagecarousel' : this.messages[index].template.type;
        }
    },
    watch: {
        messages(){
            this.nodeId ++;
        }
    }
});
</script>

@endpush
