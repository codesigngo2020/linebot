{{-- Carousel message --}}

@push('js')

<script type="text/x-template" id="--carousel-message">
    <div class="d-flex message carousel-message row nomargin">

        <div class="col-auto width-400 pt-0">
            <div :class="[{'border-radius-tl-05 border-radius-tr-05' : index == 0}, 'bg-edf2f9 pl-4 pt-5 h-100']">

                <div class="type text-muted mb-2 font-size-0875">Carousel message</div>
                <div class="content d-flex flex-nowrap overflow-auto">

                    <div class="d-flex">
                        <div v-for="(column, columnIndex) in message.template.columns" :class="[{'ml-3' : columnIndex != 0}, 'i-form-control card mb-0 width-326 box-shadow-none mr-3']">
                            <div class="card-body text-justify p-0">

                                <div v-if="column.thumbnailImageUrl" :class="[message.template.imageAspectRatio, 'position-relative overflow-hidden d-flex align-items-center image-layout']">
                                    <img class="position-absolute w-100" :src="column.thumbnailImageUrl">
                                </div>

                                <div v-if="column.title" class="title pl-4 pr-4 mt-2">@{{ column.title }}</div>
                                <div class="text white-space-pre-line pl-4 pr-4 mt-2 mb-2">@{{ column.text }}</div>

                                <hr class="m-0">

                                <div class="mt-2 mb-2">

                                    <div v-for="action in messageFormData.message.columns.columns[columnIndex].actions" class="position-relative d-flex justify-content-center align-items-center pl-4 pr-4 p-2" @mouseenter="mouseenter(action.id)" @mouseleave="mouseleave">
                                        <div class="label">@{{ action.action.label.label }}</div>
                                        <div class="position-absolute left d-flex mr-2 pl-2 bg-white">
                                            <div class="text-muted ml-2 font-size-0875">@{{ '#'+action.id }}</div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        {{-- 行為動作 --}}
        <action-list v-if="showActions && actionList.length > 0" :tagId="tagId" :actionList="actionList" :hoverId="hoverId" :tagsData="tagsData" :usersCount="usersCount" :showTags="showTags" @toggleShowActionDetail="toggleShowActionDetail"></action-list>

    </div>
</script>

<script>
Vue.component('carousel-message', {
    template: '#--carousel-message',
    props: ['index', 'tagId', 'message', 'messageFormData', 'tagsData', 'usersCount', 'showActions', 'showTags'],
    data(){
        return {
            actionList: [],
            hoverId: 0,
        };
    },
    methods: {
        setActionList(){
            var vm = this;
            this.messageFormData.message.columns.columns.forEach(function(column){
                column.actions.forEach(function(action){
                    vm.actionList.push($.extend(true, {show: false}, action));
                });
            });
        },
        mouseenter(id){
            this.hoverId = id;
        },
        mouseleave(){
            this.hoverId = 0;
        },
        toggleShowActionDetail(actionIndex){
            this.actionList[actionIndex].show = !this.actionList[actionIndex].show;
        }
    },
    created(){
        this.setActionList();
        this.actionList.sort(function(a, b){
            return parseInt(a.id) > parseInt(b.id) ? 1 : -1;
        });
    }
});
</script>

@endpush
