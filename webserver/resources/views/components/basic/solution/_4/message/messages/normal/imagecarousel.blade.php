{{-- Image Carousel message --}}

@push('js')

<script type="text/x-template" id="--imagecarousel-message">
    <div class="d-flex message imagecarousel-message row nomargin">
        <div class="col-auto width-400 pt-0">

            <div :class="[{'border-radius-tl-05 border-radius-tr-05' : index == 0}, 'bg-edf2f9 pl-4 pt-5 h-100']">
                <div class="type text-muted mb-2 font-size-0875">Image Carousel message</div>
                <div class="content d-flex flex-nowrap overflow-auto">

                    <div class="d-flex">
                        <div v-for="(column, columnIndex) in message.template.columns" :class="[{'ml-3' : columnIndex != 0}, 'i-form-control card mb-0 width-326 border-0 box-shadow-none mr-3']"  @mouseenter="mouseenter(messageFormData.message.columns.columns[columnIndex].actions[0].id)" @mouseleave="mouseleave">

                            <div class="index">
                                <span class="text-white z-index-1">@{{ '#'+messageFormData.message.columns.columns[columnIndex].actions[0].id }}</span>
                            </div>

                            <div class="card-body text-justify p-0">
                                <img :src="column.imageUrl">
                            </div>

                            {{-- 按鈕 --}}
                            <div class="button d-flex justify-content-center w-100 position-absolute">
                                <div class="pl-3 pr-3 p-2 text-white">@{{ column.action.label }}</div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        {{-- 行為動作 --}}
        <action-list v-if="showActions && actionList.length > 0" :tagId="tagId" :actionList="actionList" :hoverId="hoverId" :tagsData="tagsData" :usersCount="usersCount" :showTags="showTags" @toggleShowActionDetail="toggleShowActionDetail"></action-list>
    </div>
</script>

<script>
Vue.component('imagecarousel-message', {
    template: '#--imagecarousel-message',
    props: ['index', 'tagId', 'message', 'messageFormData', 'tagsData', 'usersCount', 'showActions', 'showTags'],
    data(){
        return {
            actionList: [],
            hoverId: 0,
        };
    },
    methods: {
        setActionList(){
            var vm = this;
            this.messageFormData.message.columns.columns.forEach(function(column){
                vm.actionList.push($.extend(true, {show: false}, column.actions[0]));
            });
        },
        mouseenter(id){
            this.hoverId = id;
        },
        mouseleave(){
            this.hoverId = 0;
        },
        toggleShowActionDetail(actionIndex){
            this.actionList[actionIndex].show = !this.actionList[actionIndex].show;
        }
    },
    created(){
        this.setActionList();
        this.actionList.sort(function(a, b){
            return parseInt(a.id) > parseInt(b.id) ? 1 : -1;
        });
    }
});
</script>

@endpush
