{{-- Quicl Reply message --}}

@push('js')

<script type="text/x-template" id="--quickreply-message">
    <div class="d-flex message quickreply-message row nomargin">
        <div class="col-auto width-400 pt-0">
            <div :class="[{'border-radius-tl-05 border-radius-tr-05' : index == 0}, 'bg-edf2f9 pl-4 pt-5 h-100']">
                <div class="type text-muted mb-2 font-size-0875">Quick Reply message</div>
                <div class="content d-flex flex-nowrap overflow-auto">

                    <div class="d-flex">
                        <div v-for="(item, itemIndex) in quickReply.items" :class="[{'ml-3' : itemIndex != 0}, 'button i-form-control card mb-0 border-0 bg-transparent']" @mouseenter="mouseenter(quickReplyFormData.message.items.items[itemIndex].id)" @mouseleave="mouseleave">

                            <img v-if="item.imageUrl" :src="item.imageUrl">
                            <div :class="[item.imageUrl ? 'hasImage' : 'pl-3', 'pr-3 p-2 text-white']">@{{ item.action.label }}</div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        {{-- 行為動作 --}}
        <action-list v-if="showActions && actionList.length > 0" :tagId="tagId" :actionList="actionList" :hoverId="hoverId" :tagsData="tagsData" :usersCount="usersCount" :showTags="showTags" @toggleShowActionDetail="toggleShowActionDetail"></action-list>

    </div>
</script>

<script>
Vue.component('quickreply-message', {
    template: '#--quickreply-message',
    props: ['index', 'tagId', 'quickReply', 'quickReplyFormData', 'tagsData', 'usersCount', 'showActions', 'showTags'],
    data(){
        return {
            actionList: [],
            hoverId: 0,
        };
    },
    methods: {
        setActionList(){
            var vm = this;
            this.quickReplyFormData.message.items.items.forEach(function(item){
                vm.actionList.push($.extend(true, {show: false}, item.actions[0]));
            });
        },
        mouseenter(id){
            this.hoverId = id;
        },
        mouseleave(){
            this.hoverId = 0;
        },
        toggleShowActionDetail(actionIndex){
            this.actionList[actionIndex].show = !this.actionList[actionIndex].show;
        }
    },
    created(){
        this.setActionList();
        this.actionList.sort(function(a, b){
            return parseInt(a.id) > parseInt(b.id) ? 1 : -1;
        });
    }
});
</script>

@endpush
