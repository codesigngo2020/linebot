{{-- Imagemap message --}}

@push('js')

<script type="text/x-template" id="--imagemap-message">
    <div class="d-flex message imagemap-message row nomargin">

        {{-- 訊息內容 --}}
        <div class="col-auto width-400 pt-0">
            <div :class="[{'border-radius-tl-05 border-radius-tr-05' : index == 0}, 'bg-edf2f9 pl-4 pt-5 h-100']">
                <div class="type text-muted mb-2 font-size-0875">Imagemap message</div>

                <div class="content d-flex">

                    <div class="i-form-control card mb-0 cursor-pointer border-0" @click="toggleShowGrids">
                        <div class="card-body text-justify p-0">

                            {{--  圖片 --}}
                            <img :src="message.baseUrl+'/1040'">

                            {{-- 隔線 --}}
                            <div v-if="messageFormData.message.show.active == 'true'" v-show="showGrids" class="position-absolute top w-100 h-100 overflow-hidden">
                                <div id="grid" class="grid">
                                    <grid-tr v-for="(tds, index) in messageFormData.message.area.area" :tds="tds" :key="index"></grid-tr>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        {{-- 行為動作 --}}
        <action-list v-if="messageFormData.message.show.active == 'true' && showActions && actionList.length > 0" :tagId="tagId" :actionList="actionList" :tagsData="tagsData" :usersCount="usersCount" :showTags="showTags" @toggleShowActionDetail="toggleShowActionDetail"></action-list>

    </div>
</script>

<script>
Vue.component('imagemap-message', {
    template: '#--imagemap-message',
    props: ['index', 'tagId', 'message', 'messageFormData', 'tagsData', 'usersCount', 'showActions', 'showTags'],
    data(){
        return {
            actionList: [],
            showGrids: true,
        };
    },
    methods: {
        toggleShowGrids(){
            this.showGrids = !this.showGrids;
        },
        recurseArea(action, obj){
            for(key in obj){
                if(obj[key].id){
                    if(action == 'getAreaList'){
                        this.actionList.push($.extend(true, {show: false}, obj[key]));
                    }
                }else if(obj[key][0]){
                    this.recurseArea(action, obj[key]);
                }
            }
        },
        toggleShowActionDetail(actionIndex){
            this.actionList[actionIndex].show = !this.actionList[actionIndex].show;
        }
    },
    created(){
        if(this.messageFormData.message.show.active == 'true'){
            this.recurseArea('getAreaList', this.messageFormData.message.area.area);
            this.actionList.sort(function(a, b){
                return parseInt(a.id) > parseInt(b.id) ? 1 : -1;
            });
        }
    }
});
</script>

{{-- 隔線系統 --}}

<script type="text/x-template" id="--grid-tr">
    <div class="tr">
        <div v-for="td in tds" :key="td.id" :class="[{'has-error' : td.id && td.invalid}, {'selected' : td.id && td.selected}, 'td']" :data-id="td.id ? td.id : ''">
            <div v-if="td.id" class="index">
                <span class="id position-absolute">@{{ td.id }}</span>
            </div>
            <grid-tr v-else v-for="tds in td" :tds="tds" :key="td.id"></grid-tr>
        </div>
    </div>
</script>

<script>
Vue.component('grid-tr', {
    template: '#--grid-tr',
    props: ['tds'],
});
</script>

@endpush
