@push('js')

{{-- 腳本瀏覽 --}}

<script type="text/x-template" id="--script-preview">
    <div id="script-preview" class="overflow-auto" ref="scriptPreview">

        <div id="tree" ref="tree" :style="'margin-left: '+treeLeftOffset+'px;'">
            <ul class="tree-ul">
                <li class="tree-li" ref="li">
                    <div class="node start">
                        <div class="message-label mb-0">
                            <p class="badge badge-soft-dark">start</p>
                        </div>
                    </div>

                    <branch :expandScript="expandscript" :flowType="flowtype" :nodes="nodes" :nodesFormData="nodesformdata" :showNodeDetailId="shownodedetailid" :nodeTagsData="nodetagsdata" :totalCount="totalcount" :usersCount="userscount" @showNodeDetail="showNodeDetail"></branch>
                </li>
            </ul>
        </div>

    </div>
</script>

<script>
Vue.component('script-preview', {
    template: '#--script-preview',
    props: ['expandscript', 'flowtype', 'shownodedetailid', 'nodes', 'nodesformdata', 'nodetagsdata', 'totalcount', 'userscount'],
    data(){
        return {
            nodesCount: this.nodesformdata.length,
            treeLeftOffset: 0,
        }
    },
    methods: {
        showNodeDetail(nodeId){
            this.$emit('shownodedetail', nodeId);
        },
        setTreeToCenter(){
            setTimeout(()=>{
                layoutWidth = this.$refs.tree.offsetWidth;
                treeWidth = this.$refs.li.offsetWidth;

                this.treeLeftOffset = treeWidth > layoutWidth ? treeWidth - layoutWidth : 0;
                if(treeWidth > layoutWidth) this.$refs.scriptPreview.scrollTo(treeWidth / 2, 0)
            }, 0);
        }
    },
    mounted(){
        this.setTreeToCenter();
    },
    watch: {
        expandscript(){
            this.setTreeToCenter();
        }
    }
});
</script>

@endpush
