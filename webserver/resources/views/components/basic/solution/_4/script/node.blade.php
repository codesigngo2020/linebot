@push('js')

{{-- 腳本節點 --}}

<script type="text/x-template" id="--node">
    <div class="node" @click="showNodeDetail">

        <div v-if="expandScript" :class="[flowType == 'users' ? 'badge-primary' : 'badge-warning-white', 'count-label badge']">@{{ percentage + '%' }}</div>

        <div class="message-label mb-0 d-flex flex-column">
            <p v-for="(message, index) in nodeFormData.messages.messages" :key="message.id" :class="[{hover : nodeFormData.id == showNodeDetailId}, 'badge badge-soft-secondary']">@{{ message.type }}</p>
        </div>

    </div>
</script>

<script>
Vue.component('node', {
    template: '#--node',
    props: ['expandScript', 'flowType', 'node', 'nodeFormData', 'showNodeDetailId', 'nodeTagData', 'totalCount', 'usersCount'],
    data(){
        return {
            percentage: 0,
        };
    },
    methods: {
        getPercentage(){
            count = this.flowType == 'users' ? this.nodeTagData.users_count : this.nodeTagData.total_count;
            baseCount = this.flowType == 'users' ? this.usersCount : this.totalCount;

            this.percentage = baseCount == 0 ? 0 : Math.floor(count*100/baseCount);
        },
        showNodeDetail(){
            this.$emit('showNodeDetail', this.nodeFormData.id);
        }
    },
    created(){
        this.getPercentage();
    },
    watch: {
        flowType(){
            this.getPercentage();
        }
    }
});
</script>
@endpush
