@push('js')

{{-- 腳本分支 --}}

<script type="text/x-template" id="--branch">
    <ul class="tree-ul">
        <li v-for="nodeFormData in filterThisLayerNodes()" :key="nodeFormData.id" class="tree-li">
            <node :class="[{disabled : nodeFormData.disabled}]" :expandMessage="expandMessage" :flowType="flowType" :node="nodeFormData.disabled ? null : getNode(nodeFormData.start, nodeFormData.end)" :nodeFormData="nodeFormData" :showNodeDetailId="showNodeDetailId" :nodeTagData="nodeFormData.disabled ? null : nodeTagsData[getNode(nodeFormData.start, nodeFormData.end).id]" :totalCount="totalCount" :usersCount="usersCount" @showNodeDetail="showNodeDetail"></node>
            <branch v-if="nodeFormData.end - nodeFormData.start != 1" :expandMessage="expandMessage" :flowType="flowType" :nodes="nodes" :nodesFormData="filterSubNodes(nodeFormData.start, nodeFormData.end)" :showNodeDetailId="showNodeDetailId" :nodeTagsData="nodeTagsData" :totalCount="totalCount" :usersCount="usersCount" @showNodeDetail="showNodeDetail"></branch>
        </li>
    </ul>
</script>

<script>
Vue.component('branch', {
    template: '#--branch',
    props: ['expandMessage', 'flowType', 'nodes', 'nodesFormData', 'showNodeDetailId', 'nodeTagsData', 'totalCount', 'usersCount'],
    methods: {
        getNode(start, end){
            return this.nodes.find(node => node.start == start && node.end == end);
        },
        getMinStartOfNodes(){
            return this.nodesFormData.reduce((min, node) => Math.min(min, node.start), 9999);
        },
        getMaxEndOfNodes(){
            return this.nodesFormData.reduce((max, node) => Math.max(max, node.end), 0);
        },
        filterThisLayerNodes(){
            thisLayerNodes = [];
            start = this.getMinStartOfNodes() - 1;
            end = this.getMaxEndOfNodes();

            do{
                node = this.nodesFormData.find(node => node.start == start + 1);
                thisLayerNodes.push(node);
                start = parseInt(node.end);
            }while(end > node.end)

            return thisLayerNodes;
        },
        filterSubNodes(start, end){
            start = parseInt(start);
            end = parseInt(end);

            return this.nodesFormData.filter(function(node){
                return node.start > start && node.end < end;
            });
        },
        showNodeDetail(nodeId){
            this.$emit('showNodeDetail', nodeId);
        }
    },
});
</script>

@endpush
