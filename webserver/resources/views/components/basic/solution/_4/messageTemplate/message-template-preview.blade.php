@push('js')

{{-- 腳本瀏覽 --}}

<script type="text/x-template" id="--message-template-preview">
    <div id="message-template-preview" class="overflow-auto" ref="messageTemplatePreview">

        <div id="tree" ref="tree" :style="'margin-left: '+treeLeftOffset+'px;'">
            <ul class="tree-ul">
                <li class="tree-li" ref="li">
                    <div class="node start">
                        <div class="message-labels mb-0">
                            <p class="badge badge-soft-dark">start</p>
                        </div>
                    </div>

                    <branch :expandMessage="expandmessage" :flowType="flowtype" :nodes="nodes" :nodesFormData="nodesformdata" :showNodeDetailId="shownodedetailid" :nodeTagsData="nodetagsdata" :totalCount="totalcount" :usersCount="userscount" @showNodeDetail="showNodeDetail"></branch>
                </li>
            </ul>
        </div>

    </div>
</script>

<script>
Vue.component('message-template-preview', {
    template: '#--message-template-preview',
    props: ['expandmessage', 'flowtype', 'shownodedetailid', 'nodes', 'nodesformdata', 'nodetagsdata', 'totalcount', 'userscount'],
    data(){
        return {
            nodesCount: this.nodesformdata.length,
            treeLeftOffset: 0,
        }
    },
    methods: {
        showNodeDetail(nodeId){
            this.$emit('shownodedetail', nodeId);
        },
        setTreeToCenter(){
            setTimeout(()=>{
                layoutWidth = this.$refs.tree.offsetWidth;
                treeWidth = this.$refs.li.offsetWidth;

                this.treeLeftOffset = treeWidth > layoutWidth ? treeWidth - layoutWidth : 0;
                if(treeWidth > layoutWidth) this.$refs.messageTemplatePreview.scrollTo(treeWidth / 2, 0)
            }, 0);
        }
    },
    mounted(){
        this.setTreeToCenter();
    },
    watch: {
        expandmessage(){
            this.setTreeToCenter();
        }
    }
});
</script>

@endpush
