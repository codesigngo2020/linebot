@push('js')

{{-- 腳本節點 --}}

<script type="text/x-template" id="--node">
    <div class="node" @click="nodeFormData.disabled ? null : showNodeDetail()">

        <div class="labels">

            <div v-if="!nodeFormData.disabled && expandMessage" :class="[flowType == 'users' ? 'badge-primary' : 'badge-warning-white', 'count-label badge']">@{{ percentage + '%' }}</div>

            <div class="condition-labels">
                <p class="badge badge-soft-info">@{{ nodeFormData.condition.name }}</p>
            </div>

            <div class="message-labels d-flex flex-column">
                <p v-for="(message, index) in nodeFormData.messages.messages" :key="message.id" :class="[{hover : nodeFormData.id == showNodeDetailId}, 'badge badge-soft-secondary']">@{{ message.type }}</p>
            </div>

        </div>

    </div>
</script>

<script>
Vue.component('node', {
    template: '#--node',
    props: ['expandMessage', 'flowType', 'node', 'nodeFormData', 'showNodeDetailId', 'nodeTagData', 'totalCount', 'usersCount'],
    data(){
        return {
            percentage: 0,
        };
    },
    methods: {
        getPercentage(){
            if(!this.nodeFormData.disabled){
                count = this.flowType == 'users' ? this.nodeTagData.users_count : this.nodeTagData.total_count;
                baseCount = this.flowType == 'users' ? this.usersCount : this.totalCount;

                this.percentage = baseCount == 0 ? 0 : Math.floor(count*100/baseCount);
            }
        },
        showNodeDetail(){
            this.$emit('showNodeDetail', this.nodeFormData.id);
        }
    },
    created(){
        this.getPercentage();
    },
    watch: {
        flowType(){
            this.getPercentage();
        }
    }
});
</script>
@endpush
