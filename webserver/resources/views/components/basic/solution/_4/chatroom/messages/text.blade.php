{{-- Text message --}}

@push('js')

<script type="text/x-template" id="--text-message">
    <div :id="logId+'-'+messageIndex" :class="[{'justify-content-end' : sender == 'bot'}, 'd-flex message text-message p-3']">
        <div class="d-flex align-items-start">

            <div v-if="sender != 'bot'" class="profile mr-3">
                <img :src="userPictureUrl" class="rounded-circle">
            </div>

            <div :class="[{'flex-row-reverse' : sender == 'bot'}, 'content d-flex font-size-0875']">
                <div :class="[{'badge-soft-warning' : highLight}, sender == 'bot' ? 'ml-3' : 'mr-3', 'i-form-control card max-width-250 mb-0']">
                    <div class="card-body text-justify pl-3 pr-3 p-2">
                        <div class="white-space-pre-line">@{{ message.text }}</div>
                    </div>
                </div>

                <div class="d-flex align-items-end">
                    <span class="d-flex align-items-center text-muted font-size-0750">
                        <v-svg class="svg mr-1" src="{{ asset('assets/image/svg/light/clock.svg') }}" width="12" height="12"></v-svg>
                        @{{ timestamp.substr(11,8) }}
                    </span>
                </div>
            </div>

        </div>
    </div>
</script>

<script>
Vue.component('text-message', {
    template: '#--text-message',
    props: ['highLight', 'logId' ,'sender' , 'recipient', 'messageIndex', 'message', 'timestamp', 'userPictureUrl'],
});
</script>

@endpush
