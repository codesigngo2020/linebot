{{-- Date --}}

@push('js')

<script type="text/x-template" id="--date">
    <div class="d-flex justify-content-center m-1">
        <span class="badge badge-light text-muted border-radius-1 pl-05 pr-05 letter-spacing-08">@{{ date }}</span>
    </div>
</script>

<script>
Vue.component('date', {
    template: '#--date',
    props: ['date'],
});
</script>

@endpush
