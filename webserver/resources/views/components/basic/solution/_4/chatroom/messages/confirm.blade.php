{{-- Confirm message --}}

@push('js')

<script type="text/x-template" id="--confirm-message">
    <div :id="logId+'-'+messageIndex" :class="[{'justify-content-end' : sender == 'bot'}, 'd-flex message confirm-message p-3']">
        <div class="d-flex align-items-start">

            <div v-if="sender != 'bot'" class="profile mr-3">
                <img :src="userPictureUrl" class="rounded-circle">
            </div>

            <div :class="[{'flex-row-reverse' : sender == 'bot'}, 'content d-flex font-size-0875']">

                <div :class="[sender == 'bot' ? 'ml-3' : 'mr-3', 'i-form-control card width-250 mb-0']">
                    <div class="card-body text-justify p-0">

                        <div class="text white-space-pre-line pl-4 pr-4 p-3">@{{ message.template.text }}</div>

                        <hr class="m-0">

                        <div class="actions d-flex mt-2 mb-2">
                            <div v-for="action in message.template.actions" class="d-flex justify-content-center align-items-center flex-grow-1 p-1">@{{ action.label }}</div>
                        </div>
                    </div>
                </div>

                <div class="d-flex align-items-end">
                    <span class="d-flex align-items-center text-muted font-size-0750">
                        <v-svg class="svg mr-1" src="{{ asset('assets/image/svg/light/clock.svg') }}" width="12" height="12"></v-svg>
                        @{{ timestamp.substr(11,8) }}
                    </span>
                </div>

            </div>
        </div>

    </div>
</script>

<script>
Vue.component('confirm-message', {
    template: '#--confirm-message',
    props: ['highLight', 'logId' ,'sender' , 'recipient', 'messageIndex', 'message', 'timestamp', 'userPictureUrl'],
});
</script>

@endpush
