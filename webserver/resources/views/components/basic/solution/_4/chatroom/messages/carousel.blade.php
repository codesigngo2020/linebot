{{-- Carousel message --}}

@push('js')

<script type="text/x-template" id="--carousel-message">
    <div :id="logId+'-'+messageIndex" :class="[{'justify-content-end' : sender == 'bot'}, 'd-flex message carousel-message p-3']">
        <div class="d-flex align-items-start">

            <div v-if="sender != 'bot'" class="profile mr-3">
                <img :src="userPictureUrl" class="rounded-circle">
            </div>

            <div :class="[{'flex-row-reverse' : sender == 'bot'}, 'content d-flex font-size-0875']">

                <div :class="[sender == 'bot' ? 'ml-3' : 'mr-3', message.template.columns.length > 1 ? 'width-300' : 'width-250', 'd-flex overflow-auto']">
                    <div class="d-flex">
                        <div v-for="(column, columnIndex) in message.template.columns" :class="[{'ml-3' : columnIndex != 0}, 'i-form-control card mb-0 width-250 box-shadow-none']">
                            <div class="card-body text-justify p-0">

                                <div v-if="column.thumbnailImageUrl" :class="[message.template.imageAspectRatio, 'image-layout']">
                                    <img class="position-absolute top left w-100" :src="column.thumbnailImageUrl">
                                </div>

                                <div v-if="column.title" class="title pl-4 pr-4 mt-2">@{{ column.title }}</div>
                                <div class="text white-space-pre-line pl-4 pr-4 mt-2 mb-2">@{{ column.text }}</div>

                                <hr class="m-0">

                                <div class="mt-2 mb-2">

                                    <div v-for="action in message.template.columns[columnIndex].actions" class="d-flex justify-content-center align-items-center p-1">@{{ action.label }}</div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="d-flex align-items-end">
                    <span class="d-flex align-items-center text-muted font-size-0750">
                        <v-svg class="svg mr-1" src="{{ asset('assets/image/svg/light/clock.svg') }}" width="12" height="12"></v-svg>
                        @{{ timestamp.substr(11,8) }}
                    </span>
                </div>

            </div>

        </div>

    </div>
</script>

<script>
Vue.component('carousel-message', {
    template: '#--carousel-message',
    props: ['highLight', 'logId' ,'sender' , 'recipient', 'messageIndex', 'message', 'timestamp', 'userPictureUrl'],
});
</script>

@endpush
