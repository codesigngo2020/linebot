{{-- Image Carousel message --}}

@push('js')

<script type="text/x-template" id="--imagecarousel-message">
    <div :id="logId+'-'+messageIndex" :class="[{'justify-content-end' : sender == 'bot'}, 'd-flex message imagecarousel-message p-3']">
        <div class="d-flex align-items-start">

            <div v-if="sender != 'bot'" class="profile mr-3">
                <img :src="userPictureUrl" class="rounded-circle">
            </div>

            <div :class="[{'flex-row-reverse' : sender == 'bot'}, 'content d-flex font-size-0875']">

                <div :class="[sender == 'bot' ? 'ml-3' : 'mr-3', 'd-flex overflow-auto width-300']">

                    <div class="d-flex">
                        <div v-for="(column, columnIndex) in message.template.columns" :class="[{'ml-3' : columnIndex != 0}, 'i-form-control card mb-0 width-300 box-shadow-none border-0']">

                            <div class="card-body text-justify p-0">
                                <img :src="column.imageUrl">
                            </div>

                            {{-- 按鈕 --}}
                            <div class="button d-flex justify-content-center w-100 position-absolute">
                                <div class="pl-3 pr-3 p-2 text-white">@{{ column.action.label }}</div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="d-flex align-items-end">
                    <span class="d-flex align-items-center text-muted font-size-0750">
                        <v-svg class="svg mr-1" src="{{ asset('assets/image/svg/light/clock.svg') }}" width="12" height="12"></v-svg>
                        @{{ timestamp.substr(11,8) }}
                    </span>
                </div>

            </div>
        </div>

    </div>
</script>

<script>
Vue.component('imagecarousel-message', {
    template: '#--imagecarousel-message',
    props: ['highLight', 'logId' ,'sender' , 'recipient', 'messageIndex', 'message', 'timestamp', 'userPictureUrl'],
});
</script>

@endpush
