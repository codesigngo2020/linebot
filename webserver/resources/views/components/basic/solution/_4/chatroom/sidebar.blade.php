@push('js')

{{-- sidebar --}}

<script type="text/x-template" id="--chatroom-sidebar">
    <div id="chatroom-sidebar" class="pt-4 pb-4">
        <div :class="[{'text-white' : type == 'users'}, 'd-flex justify-content-center cursor-pointer']" @click="changeListType('users')">
            <v-svg class="svg" src="{{ asset('assets/image/svg/light/user.svg') }}" width="23" height="23"></v-svg>
        </div>
        <div :class="[{'text-white' : type == 'dialogs'}, 'cursor-pointer']" @click="changeListType('dialogs')">
            <v-svg class="svg" src="{{ asset('assets/image/svg/light/comment-dots.svg') }}" width="25" height="25"></v-svg>
        </div>
        <div :class="[{'text-white' : type == 'customer-service'}, 'cursor-pointer']" @click="changeListType('customer-service')">
            <v-svg class="svg" src="{{ asset('assets/image/svg/light/user-headset.svg') }}" width="25" height="25"></v-svg>
        </div>
    </div>
</script>

<script>
Vue.component('chatroom-sidebar', {
    template: '#--chatroom-sidebar',
    props: ['type'],
    methods: {
        changeListType(type){
            this.$emit('change-list-type', type);
        }
    }
});
</script>

@endpush
