@push('js')

{{-- dialogs --}}

<script type="text/x-template" id="--chatroom-dialogs">
    <div id="chatroom-dialogs" class="h-100">

        <!-- <div :class="[is_loading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-bl-05 border-radius-br-05']">
        <div class="spinner-grow text-secondary" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</div> -->

<div class="d-flex overflow-hidden">
    <div class="w-100 flex-shrink-0 bg-white">
        <div class="search-from pl-4 pr-4 p-2">
            {{-- Search --}}
            <form class="row align-items-center" @submit.prevent>
                <div class="col-auto pr-0">
                    <span class="text-muted">
                        <v-svg src="{{ asset('assets/image/svg/light/search.svg') }}" width="18" height="18"></v-svg>
                    </span>
                </div>
                <div class="col">
                    <input type="search" class="form-control form-control-flush search" placeholder="Search" v-model="query" @input="searchDialogs">
                </div>
            </form>
        </div>

        <div class="overflow-auto hidden-scrollbar" style="height: calc(100vh - 3rem - 55.5px);" @scroll="handleScroll($event)">
            <ul class="users-list list-group" ref="scrollInner">
                <li v-for="dialog in isQuery ? searchedDialogs : dialogs" :class="[{'i-active' : dialog.id == activeUser.id}, 'list-group-item cursor-pointer']" @click="isQuery ? getResultDialogs(dialog) : updateUser(dialog)">
                    <div class="row align-items-center flex-nowrap">
                        <div class="col-auto">
                            <img :src="dialog.picture_url" class="rounded-circle">
                            <span v-if="!dialog.is_follow" class="d-flex dot">
                                <span class="font-size-0750">●</span>
                            </span>
                        </div>
                        <div class="user-info col ml-n2 flex-nowrap">
                            <h5 class="name mb-2">@{{ dialog.display_name }}</h5>
                            <p class="d-flex text-muted small mb-0 font-size-0750">
                                <span class="word-break-keep-all text-overflow-ellipsis white-space-nowrap flex-grow-1 overflow-hidden">@{{ isQuery ? getSearchedDialogMessage(dialog.logs_count) : getDialogMessage(dialog.messages) }}</span>
                                <span v-if="!isQuery" class="ml-2">@{{ dialog.timestamp }}</span>
                            </p>
                        </div>
                    </div>
                </li>
                <li v-if="(!isQuery && dialogs.length == 0 || isQuery && searchedDialogs.length == 0)" class="list-group-item no-items">No items</li>
            </ul>
        </div>
    </div>
    <div id="dialog-serch-result" :class="[{active : showResultDialogs} ,'w-100 flex-shrink-0 bg-white z-index-1']">
        <div class="result-info d-flex text-secondary small">
            <div class="d-flex mr-2 cursor-pointer" @click="hideResult">
                <v-svg src="{{ asset('assets/image/svg/light/arrow-alt-left.svg') }}" width="18" height="18"></v-svg>
            </div>
            <div class="white-space-nowrap text-overflow-ellipsis overflow-hidden">@{{ '有 '+resultDialogs.length+' 則訊息包含「'+query+'」' }}</div>
        </div>
        <div class="overflow-auto hidden-scrollbar" style="height: calc(100vh - 3rem - 45px);" @scroll="handleScroll($event)">
            <ul class="users-list list-group" ref="scrollInner">
                <li v-for="dialog in resultDialogs" :class="[{'i-active' : dialog.log_id == activeUser.log_id}, 'list-group-item cursor-pointer']" @click="updateUser(dialog)">
                    <div class="row align-items-center flex-nowrap">
                        <div class="col-auto">
                            <img :src="dialog.picture_url" class="rounded-circle">
                            <span v-if="!dialog.is_follow" class="d-flex dot">
                                <span class="font-size-0750">●</span>
                            </span>
                        </div>
                        <div class="user-info col ml-n2 flex-nowrap">
                            <h5 class="name mb-2">@{{ dialog.display_name }}</h5>
                            <p class="d-flex text-muted small mb-0 font-size-0750">
                                <span class="word-break-keep-all text-overflow-ellipsis white-space-nowrap flex-grow-1 overflow-hidden">@{{ dialog.messages[dialog.message_index].text }}</span>
                                <span class="ml-2">@{{ dialog.timestamp }}</span>
                            </p>
                        </div>
                    </div>
                </li>
                <li v-if="(!isQuery && dialogs.length == 0 || isQuery && searchedDialogs.length == 0)" class="list-group-item no-items">No items</li>
            </ul>
        </div>
    </div>
</div>

</div>
</script>

<script>
Vue.component('chatroom-dialogs', {
    template: '#--chatroom-dialogs',
    props: ['activeUser', 'wsResponse', 'webhook'],
    data(){
        return {
            // 是否在搜尋狀態
            isQuery: false,

            query: '',
            is_loading: false,

            // 未搜尋訊息欄
            dialogs: args.dialogs.dialogs,
            dialogsHasNext: args.dialogs.hasNext,
            dialogsLastLogId: 0,

            // 搜尋訊息欄
            searchedDialogs: [],
            searchedDialogshasNext: false,

            // 搜尋特定好友訊息欄
            showResultDialogs: false,
            resultDialogs: [],
        };
    },
    methods: {
        getDialogMessage(messages){
            switch(messages[messages.length - 1].type){
                case 'text':
                message = messages[messages.length - 1].text;
                break;

                case 'image':
                message = '圖片訊息';
                break;

                case 'video':
                message = '影片訊息';
                break;

                case 'imagemap':
                case 'template':
                message = messages[messages.length - 1].altText;
                break;

                default:
                message = '';
            }
            return message;
        },
        getSearchedDialogMessage(count){
            return '找到 '+count+' 則訊息';
        },


        searchDialogs(){
            this.query = this.query.trim();
            this.resultDialogs = [];

            if(this.query){
                this.getDialogs('search');
            }else{
                this.isQuery = false;
            }
        },
        getDialogs(type){
            if(!this.is_loading){
                this.is_loading = true;

                var json = {type: 'getDialogs', action: type};
                if(this.query){
                    json['q'] = this.query;
                    if(type == 'scroll') json['after'] = this.searchedDialogs[this.searchedDialogs.length - 1].id;
                }else{
                    json['before'] = this.dialogsLastLogId; // 沒有搜尋字串->都是下滑觸發的
                }

                this.$emit('send-to-websocket', JSON.stringify(json));
            }
        },
        handleScroll(event){
            if(this[this.isQuery ? 'searchedDialogsHasNext' : 'dialogsHasNext'] && event.target.scrollTop + event.target.clientHeight >= this.$refs.scrollInner.clientHeight - 15){
                this.getDialogs('scroll');
            }
        },
        updateUser(dialog){
            console.log(dialog)
            this.$emit('update-user', {
                id: dialog.id,
                display_name: dialog.display_name,
                is_follow: dialog.is_follow,
                log_id: dialog.log_id ? dialog.log_id : 0,
                message_index: dialog.message_index ? dialog.message_index : 0,
                picture_url: dialog.picture_url,
                reply_token: dialog.reply_token,
                status_message: dialog.status_message,
            });
        },
        getResultDialogs(dialog){
            var json = {
                type: 'getResultDialogs',
                q: this.query,
                userId: dialog.id,
            };
            this.$emit('send-to-websocket', JSON.stringify(json));
        },
        hideResult(){
            this.showResultDialogs = false;
        }
    },
    created(){
        dialogKeys = Object.keys(this.dialogs);
        this.dialogsLastLogId = this.dialogs.length > 0 ? this.dialogs[dialogKeys[dialogKeys.length - 1]].last_log_id : 0;
        this.dialogs.forEach(function(dialog){
            dialog.messages = JSON.parse(dialog.messages);
        });
    },
    watch: {
        wsResponse(newValue){
            if(newValue.type == 'getDialogs'){
                var vm = this;
                if(newValue.q){
                    if(newValue.action == 'search') this.searchedDialogs = [];

                    if(newValue.dialogs){
                        newValue.dialogs.forEach(function(dialog){
                            d = vm.dialogs.find(d => d.id == dialog.user.id);
                            vm.searchedDialogs.push({
                                id: dialog.user.id,
                                display_name: dialog.user.display_name,
                                is_follow: dialog.user.is_follow,
                                picture_url: dialog.user.picture_url,
                                reply_token: (d ? d.reply_token : ''),
                                status_message: dialog.user.status_message,

                                logs_count: dialog.logs_count,
                            });
                        });
                    }
                    this.searchedDialogsHasNext = newValue.hasNext;
                }else if(newValue.dialogs){
                    newValue.dialogs.forEach(function(dialog){
                        d = vm.dialogs.find(d => d.id == dialog.user.id);
                        if(!d){
                            vm.dialogs.push({
                                id: dialog.user.id,
                                display_name: dialog.user.display_name,
                                is_follow: dialog.user.is_follow,
                                picture_url: dialog.user.picture_url,
                                reply_token: '',
                                status_message: dialog.user.status_message,

                                messages: JSON.parse(dialog.messages),
                                timestamp: dialog.timestamp,
                            });
                        }
                    });

                    this.dialogsHasNext = newValue.hasNext;
                    this.dialogsLastLogId -= 1000;
                    if(this.dialogsLastLogId <= 0) this.dialogsHasNext = false;
                }
                this.isQuery = newValue.q ? true : false;

            }else if(newValue.type == 'getResultDialogs'){
                var vm = this;
                this.resultDialogs = [];
                dialog = this.searchedDialogs.find(d => d.id == newValue.dialogs[0].user.id)
                replyToken = dialog ? dialog.reply_token : '';

                newValue.dialogs.forEach(function(dialog){
                    resultDialogs = {
                        id: dialog.user.id,
                        display_name: dialog.user.display_name,
                        is_follow: dialog.user.is_follow,
                        picture_url: dialog.user.picture_url,
                        reply_token: replyToken,
                        status_message: dialog.user.status_message,

                        messages: JSON.parse(dialog.messages),
                        log_id: dialog.log_id,
                        timestamp: dialog.timestamp,
                    };

                    if(resultDialogs.messages.length == 1){
                        resultDialogs.message_index = 0;
                    }else{
                        resultDialogs.message_index = resultDialogs.messages.findIndex(function(m){
                            return m.type == 'text' && (m.text.indexOf(vm.query) != -1);
                        });
                    }
                    vm.resultDialogs.push(resultDialogs);
                });
                this.showResultDialogs = true;
            }
            this.is_loading = false;
        },
        webhook(newValue){
            // 更新 dialogs
            dIndex = this.dialogs.findIndex(d => d.id == newValue.user.id);
            old_reply_token = '';
            if(dIndex != -1){
                old_reply_token = this.dialogs[dIndex].reply_token;
                this.dialogs.splice(dIndex, 1);
            }
            this.dialogs.unshift({
                id: newValue.user.id,
                reply_token: newValue.reply_token ? newValue.reply_token : old_reply_token,
                display_name: newValue.user.display_name,
                picture_url: newValue.user.picture_url,
                is_follow: newValue.user.is_follow,
                messages: JSON.parse(newValue.messages),
            });

            // 更新 searchedDialogs
            if(newValue.reply_token){
                dIndex = this.searchedDialogs.findIndex(d => d.id == newValue.user.id);
                if(dIndex != -1)this.searchedDialogs[dIndex].reply_token = newValue.reply_token;
            }

            // 更新 resultDialogs
            if(this.activeUser.id == newValue.user.id && this.activeUser.log_id){
                this.resultDialogs.forEach(function(dialog){
                    dialog.reply_token = newValue.reply_token ? newValue.reply_token : old_reply_token;
                });
            }

            // 更新 聊天室的好友
            // if(this.activeUser && this.activeUser.id == newValue.user.id){
            //     this.updateUser(this.dialogs[0]);
            // }
        }
    }
});
</script>

@endpush
