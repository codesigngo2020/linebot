{{-- Messages --}}


{{-- Text message --}}
@include('components.basic.solution._4.chatroom.messages.text')

{{-- Image message --}}
@include('components.basic.solution._4.chatroom.messages.image')

{{-- Video message --}}
@include('components.basic.solution._4.chatroom.messages.video')

{{-- Imagemap message --}}
@include('components.basic.solution._4.chatroom.messages.imagemap')

{{-- Buttons message --}}
@include('components.basic.solution._4.chatroom.messages.buttons')

{{-- Confirm message --}}
@include('components.basic.solution._4.chatroom.messages.confirm')

{{-- Carousel message --}}
@include('components.basic.solution._4.chatroom.messages.carousel')

{{-- Imagecarousel message --}}
@include('components.basic.solution._4.chatroom.messages.imagecarousel')





{{-- Date --}}
@include('components.basic.solution._4.chatroom.messages.date')
