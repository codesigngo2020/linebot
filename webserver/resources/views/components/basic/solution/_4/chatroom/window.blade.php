@push('js')

{{-- window --}}

<script type="text/x-template" id="--chatroom-window">
    <div id="chatroom-window" class="position-relative d-flex flex-column flex-grow-1">

        <!-- <div :class="[isLoading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute top left justify-content-center align-items-center h-100 w-100 bg-secondary']">
            <div class="spinner-grow text-secondary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div> -->

        {{-- 聊天室好友資訊 --}}
        <div v-if="user.id !== 0" id="top-bar" class="d-flex pl-1125 pr-3">
            <h5 class="d-flex align-items-center flex-grow-1 mb-0 text-secondary font-weight-normal">@{{ user.display_name }}</h5>
        </div>

        <div v-show="user.id !== 0" class="flex-grow-1 overflow-auto hidden-scrollbar m-2" ref="chatroomWindow" @scroll="hasNext ? handleScroll($event) : null">

            <div v-for="(log, logIndex) in logs">
                <date v-if="log.date" :date="log.date"></date>
                <component v-else v-for="(message, messageIndex) in log.messages" :key="log.id+'-'+messageIndex" :is="getMessageType(logIndex, messageIndex)+'-message'" :highLight="user.log_id && user.log_id == log.id && user.message_index == messageIndex" :logId="log.id" :sender="log.sender" :recipient="log.recipient" :messageIndex="messageIndex" :message="message" :timestamp="log.timestamp" :userPictureUrl="user.picture_url"></component>
            </div>

        </div>

        {{-- 輸入訊息 --}}
        <div v-if="user && user.is_follow" id="input-bar" class="pl-1125 pr-1125 p-3">

            <div class="d-flex align-items-end">
                <div class="i-d-flex align-items-center text-secondary mr-3 cursor-pointer" style="height: 28px;" @click="toggleImagesInput">
                    <v-svg v-show="!showImagesSelector" class="svg" src="{{ asset('assets/image/svg/light/images.svg') }}" width="20" height="20"></v-svg>
                    <v-svg v-show="showImagesSelector" class="svg" src="{{ asset('assets/image/svg/light/keyboard.svg') }}" width="20" height="20"></v-svg>
                </div>

                <div v-show="!showImagesSelector" class="position-relative flex-grow-1 texarea-not-input text">
                    <pre class="p-1" ref="inputPre"></pre>
                    <textarea class="not-input pl-3 p-1 border-radius-0375" placeholder="輸入訊息" v-model="form.text"></textarea>
                </div>

                <div v-show="showImagesSelector" class="text-muted text-center flex-grow-1">選擇傳送圖片順序</div>

                <div v-show="(!showImagesSelector && form.text) || (showImagesSelector && imageSelected)" class="i-d-flex align-items-center text-secondary ml-3 cursor-pointer" style="height: 28px;" @click="send">
                    <v-svg class="svg" src="{{ asset('assets/image/svg/solid/paper-plane.svg') }}" width="20" height="20"></v-svg>
                </div>
            </div>

            <div v-show="showImagesSelector" id="images-selector" class="position-relative i-d-flex mt-3 mb-2">
                <input class="position-absolute" type="file" accept=".jpeg,.jpg,.png,.JPEG,.JPG,.PNG" ref="imagesInput" multiple style="width: 0; height: 0;" @change="imagesChange($event)">
                <div v-for="(image, imageIndex) in form.images" class="col-20 cursor-pointer" @click="selectImage(imageIndex)">
                    <div class="image-layout">
                        <img :src="image.url" :class="[image.class, 'position-absolute']">
                    </div>
                    <div :class="[{'bg-primary' : image.index != 0}, 'checkbox']">@{{ image.index != 0 ? image.index : '' }}</div>
                </div>
            </div>

        </div>

        <div v-show="user.id === 0" class="i-d-flex flex-column justify-content-center align-items-center h-100 text-muted">
            <div class="mb-3">
                <v-svg class="svg" src="{{ asset('assets/image/svg/light/comment-dots.svg') }}" width="45" height="45"></v-svg>
            </div>
            <div>選擇好友，開始聊天吧！</div>
        </div>

        <div v-show="websocketStatus" class="position-absolute bottom i-d-flex justify-content-center w-100">
            <div class="card">
                <div class="card-body text-secondary pt-3 pb-3 pl-4 pr-4">@{{ websocketStatus }}</div>
            </div>
        </div>

    </div>
</script>

<script>
Vue.component('chatroom-window', {
    template: '#--chatroom-window',
    props: ['user', 'websocketStatus', 'wsResponse', 'webhook'],
    data(){
        return {
            isLoading: false,

            query: '',
            logs: [],
            hasNext: true,

            isInit: true,

            // 訊息輸入框
            form: {
                text: '',
                images: [],
            },
            imageSelected: false,
            showImagesSelector: false,
        };
    },
    methods: {
        getLogs(init){
            if(!this.isLoading){
                this.isLoading = true;

                this.isInit = init;
                var json = {
                    type: 'getLogs',
                    userId: this.user.id,
                };

                if(init && this.user.log_id) json['logId'] = this.user.log_id;

                if(!this.isInit) json['before'] = this.logs[0].id;

                this.$emit('send-to-websocket', JSON.stringify(json));
            }
        },
        getMessageType(logIndex, messageIndex){
            if(this.logs[logIndex].messages[messageIndex].type != 'template') return this.logs[logIndex].messages[messageIndex].type;
            return this.logs[logIndex].messages[messageIndex].template.type == 'image_carousel' ? 'imagecarousel' : this.logs[logIndex].messages[messageIndex].template.type;
        },
        handleScroll(event){
            if(this.hasNext && event.target.scrollTop < 30){
                this.getLogs(false);
            }
        },
        toggleImagesInput(){
            if(this.showImagesSelector){
                this.form.images = [];
                this.imageSelected = false;
            }else{
                this.$refs.imagesInput.click();
            }
        },
        imagesChange(event){

            if(event.target.files.length != 0){
                var vm = this,
                images = [],
                counter = 0;

                for(var i = 0; i < event.target.files.length; i ++){
                    images[counter] = new Image(),
                    images[counter].fileData = event.target.files[i];

                    images[counter].src = window.URL.createObjectURL(images[counter].fileData);
                    images[counter].onload = function(){

                        var width = this.naturalWidth,
                        height = this.naturalHeight;

                        if(width <= 4096 && height <= 4096){
                            if(counter >= 5) return;
                            counter ++;
                            vm.form.images.push({
                                index: 0,
                                url: this.src,
                                file: this.fileData,
                                class: width > height ? 'h-100' : 'w-100',
                            });
                        }
                    };
                }
            }
        },
        selectImage(imageIndex){
            if(this.form.images[imageIndex]){
                if(this.form.images[imageIndex].index == 0){
                    this.imageSelected = true;
                    maxIndex = this.form.images.reduce((max, image) => {
                        return image.index > max ? image.index : max;
                    }, 0);
                    this.form.images[imageIndex].index = maxIndex + 1;

                }else{
                    var vm = this;
                    this.form.images.forEach(function(image){
                        if(image.index > vm.form.images[imageIndex].index) image.index -= 1;
                    });

                    this.form.images[imageIndex].index = 0;

                    if(this.form.images.filter(image => image.index != 0).length == 0){
                        this.imageSelected = false;
                    };
                }
            }
        },
        buildFormData(formData, data, parentKey) {
            if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                var vm = this;
                Object.keys(data).forEach(key => {
                    vm.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                });
            } else {
                const value = data == null ? '' : data;
                formData.append(parentKey, value);
            }
        },
        jsonToFormData(data){
            let formData = new FormData();
            this.buildFormData(formData, data, 'form');
            return formData;
        },
        sendImageMessage(user_id, reply_token, images){
            var vm = this;

            formData = this.jsonToFormData({images: images});
            formData.append('_token', args._token);

            axios.post(route('solution.4.deployment.chatroom.imageUpload', args.deployment),
            formData,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(response => {
                console.log(response);
                if(response.data.status == 'success' && response.data.images){
                    json = {
                        type: 'sendMessage',
                        userId: user_id,
                        replyToken: reply_token,
                        message: [],
                    };

                    response.data.images.forEach(function(image){
                        json.message.push({
                            type: 'image',
                            originalContentUrl: image,
                            previewImageUrl: image,
                        });
                    });

                    vm.$emit('send-to-websocket', JSON.stringify(json));
                };
            }).catch(error => {

            });
        },
        send(){
            if(!this.showImagesSelector && this.form.text){
                this.$emit('send-to-websocket', JSON.stringify({
                    type: 'sendMessage',
                    userId: this.user.id,
                    replyToken: this.user.reply_token,
                    message: [{
                        type: 'text',
                        text: this.form.text,
                    }],
                }));
                this.form.text = '';
            }else if(this.showImagesSelector && this.imageSelected && this.form.images.length > 0){
                images = Object.assign(this.form.images);
                images = this.form.images.filter(function(image){
                    return image.index != 0;
                }).sort(function(a, b){
                    return a.index < b.index ? -1 : 1;
                }).map(function(image){
                    return image.file;
                })
                this.sendImageMessage(this.user.id, this.user.reply_token, images);

                this.imageSelected = false;
                this.form.images = [];
            }
        },
    },
    watch: {
        'user.id': function(){
            this.getLogs(true);
        },
        'user.log_id': function(newValue){
            if(newValue) this.getLogs(true);
        },
        wsResponse(newValue){

            if(newValue.logs){
                newValue.logs.forEach(function(log){
                    if(!log.date) log.messages = JSON.parse(log.messages);
                })
                if(this.isInit){
                    this.logs = newValue.logs.reverse();
                }else{
                    var scrollToaMessageId = this.logs[0].id+'-'+(this.logs[0].messages.length-1);
                    this.logs = newValue.logs.reverse().concat(this.logs);
                }
                this.hasNext = newValue.hasNext;

                var vm = this;
                this.$nextTick(function(){
                    if(vm.isInit){
                        if(this.user.log_id){
                            document.getElementById(vm.user.log_id+'-'+vm.user.message_index).scrollIntoView();
                        }else{
                            vm.$refs.chatroomWindow.scrollTo(0, vm.$refs.chatroomWindow.scrollHeight);
                        }
                    }else{
                        document.getElementById(scrollToaMessageId).scrollIntoView();
                    }
                });
            }
            this.isLoading = false;
        },
        webhook(newValue){
            if(this.user && this.user.id == newValue.user.id){
                this.logs.push({
                    id: newValue.log_id,
                    sender: newValue.sender,
                    recipient: newValue.recipient,
                    messages: JSON.parse(newValue.messages),
                    timestamp: newValue.timestamp,
                });

                var vm = this;
                this.$nextTick(function(){
                    vm.$refs.chatroomWindow.scrollTo(0, vm.$refs.chatroomWindow.scrollHeight);
                });
            }
        },
        'form.text': function(newValue){
            this.$refs.inputPre.innerHTML = newValue.replaceAll(/\r\n|\r|\n/g, '<br>')+'<br>';
        },
        'form.images': function(newValue){
            this.showImagesSelector = newValue.length > 0;
            if(!this.showImagesSelector) this.$refs.imagesInput.value = '';
        },
    },
});
</script>

@endpush
