@push('js')

{{-- users --}}

<script type="text/x-template" id="--chatroom-users">
    <div id="chatroom-users" class="h-100">

        <div :class="[is_loading ? 'd-flex z-index-1' : 'd-none', 'loading-layout position-absolute justify-content-center align-items-center h-100 w-100 bg-secondary border-radius-bl-05 border-radius-br-05']">
            <div class="spinner-grow text-secondary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>

        <div class="search-from pl-4 pr-4 p-2">
            {{-- Search --}}
            <form class="row align-items-center" @submit.prevent="searchUsers">
                <div class="col-auto pr-0">
                    <span class="text-muted">
                        <v-svg src="{{ asset('assets/image/svg/light/search.svg') }}" width="18" height="18"></v-svg>
                    </span>
                </div>
                <div class="col">
                    <input type="search" class="form-control form-control-flush search" placeholder="Search" v-model="query" @change="searchUsers">
                </div>
            </form>
        </div>

        <div class="overflow-auto hidden-scrollbar" style="height: calc(100vh - 3rem - 55.5px);" @scroll="handleScroll($event)">
            <ul class="users-list list-group" ref="scrollInner">
                <li v-for="user in isQuery ? searchedUsers : users" :class="[{'i-active' : user.id == activeUser.id}, 'list-group-item cursor-pointer']" @click="updateUser(user)">
                    <div class="row align-items-center flex-nowrap">
                        <div class="col-auto">
                            <img :src="user.picture_url" class="rounded-circle">
                            <span v-if="!user.is_follow" class="d-flex dot">
                                <span class="font-size-0750">●</span>
                            </span>
                        </div>
                        <div class="user-info col ml-n2 flex-nowrap">
                            <h5 :class="[{'mb-2' : user.status_message}, 'name']">@{{ user.display_name }}</h5>
                            <p v-if="user.status_message" class="text-muted small mb-0 word-break-keep-all text-overflow-ellipsis white-space-nowrap overflow-hidden font-size-0750">@{{ user.status_message }}</p>
                        </div>
                    </div>
                </li>
                <li v-if="(!isQuery && users.length == 0 || isQuery && searchedUsers.length == 0)" class="list-group-item no-items">No items</li>
            </ul>
        </div>

    </div>
</script>

<script>
Vue.component('chatroom-users', {
    template: '#--chatroom-users',
    props: ['activeUser', 'wsResponse', 'webhook'],
    data(){
        return {
            isQuery: false,

            query: '',
            is_loading: false,

            users: args.users.users,
            usersHasNext: args.users.hasNext,

            searchedUsers: [],
            searchedUsershasNext: false,

            getUsersType: null,

        };
    },
    methods: {
        searchUsers(){
            this.query = this.query.trim();

            if(this.query){
                this.getUsers('search');
            }else{
                this.isQuery = false;
            }
        },
        getUsers(type){
            if(!this.is_loading){
                this.is_loading = true;

                this.getUsersType = type;

                var json = {type: 'getUsers'};

                if(this.query) json['q'] = this.query;
                if(type == 'scroll') json['after'] = this.users[this.users.length-1].id;

                this.$emit('send-to-websocket', JSON.stringify(json));
            }
        },
        handleScroll(event){
            if(this[this.isQuery ? 'searchedUsersHasNext' : 'usersHasNext'] && !this.is_loading && event.target.scrollTop + event.target.clientHeight >= this.$refs.scrollInner.clientHeight - 15){
                this.getUsers('scroll');
            }
        },
        updateUser(user){
            this.$emit('update-user', {
                id: user.id,
                display_name: user.display_name,
                is_follow: user.is_follow,
                log_id: 0,
                message_index: 0,
                picture_url: user.picture_url,
                reply_token: user.reply_token,
                status_message: user.status_message,
            });
        }
    },
    watch: {
        wsResponse(newValue){
            var vm = this;
            if(this.query){

                if(newValue.users){

                    if(this.getUsersType == 'search') this.searchedUsers = [];
                    newValue.users.forEach(function(user){
                        u = vm.users.find(d => d.id == user.id);
                        vm.searchedUsers.push(Object.assign(user, {reply_token: (u ? u.reply_token : '')}));
                    });

                }else if(this.getUsersType == 'search'){
                    this.searchedUsers = [];
                }
                this.searchedUsersHasNext = newValue.hasNext;

            }else if(newValue.users){

                newValue.users.forEach(function(user){
                    u = vm.users.find(u => u.id == user.id);
                    if(!u) vm.users.push(Object.assign(user, {reply_token: ''}));
                });
                this.usersHasNext = newValue.hasNext;
            }

            this.isQuery = this.query ? true : false;
            this.is_loading = false;
        },
        webhook(newValue){
            if(newValue.reply_token){
                // 更新 users
                uIndex = this.users.findIndex(u => u.id == newValue.user.id);

                if(uIndex != -1){ // 在 users 有找到
                    this.users[uIndex].reply_token = newValue.reply_token;
                }else if(!this.usersHasNext){ // 已經到底了
                    this.users.push({
                        display_name: newValue.user.display_name,
                        id: newValue.user.id,
                        is_follow: newValue.user.is_follow,
                        picture_url: newValue.user.picture_url,
                        reply_token: newValue.reply_token ? newValue.reply_token : '',
                        status_message: newValue.user.status_message,
                    });
                }

                // 更新 searchedUsers
                uIndex = this.searchedUsers.findIndex(u => u.id == newValue.user.id);
                if(uIndex != -1) this.searchedUsers[uIndex].reply_token = newValue.reply_token;
            }
        }
    }
});
</script>

@endpush
