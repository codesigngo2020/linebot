@extends('layouts/app')

@section('title', '註冊')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-12 col-md-6 col-xl-4 my-6">

      {{-- Heading --}}
      <h1 class="display-4 text-center mb-3">Register</h1>

      {{-- Subheading --}}
      <p class="text-muted text-center mb-5">Register to activate your functions.</p>

      {{-- Form --}}
      @include('components.forms.auth.register')

    </div>
  </div>
</div>
@endsection
