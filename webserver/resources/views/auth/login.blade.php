@extends('layouts/app')

@section('title', '登入')

@push('css')
<link href="{{ asset('assets/css/auth/login.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-12 col-md-6 col-xl-4 my-6">

      {{-- Heading --}}
      <h1 class="display-4 text-center mb-3">Login</h1>

      {{-- Subheading --}}
      <p class="text-muted text-center mb-5">Login to activate your functions.</p>

      {{-- Form --}}
      @include('components.forms.auth.login')

    </div>
  </div>
</div>
@endsection

@push('js')
<script src="{{ asset('assets/js/auth/login.js?v='.time()) }}"></script>
@endpush
