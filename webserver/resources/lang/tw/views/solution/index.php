<?php

return [
    'solutions' => [
        1 => [
            'title' => 'Youtube 爬蟲',
            'category' => 'Wordpress',
            'description' => '爬取Youtube影片產生Wordpress文章。'
        ],
        2 => [
            'title' => 'Instagram 爬蟲',
            'category' => 'Wordpress',
            'description' => '爬取Instagram文章產生Wordpress文章。'
        ],
        3 => [
            'title' => 'Instagram 分析',
            'category' => 'Instagram',
            'description' => '進行Instagram全方位的分析。'
        ],
        4 => [
            'title' => 'LINE 聊天機器人',
            'category' => 'LINE、Chatbot',
            'description' => '建立LINE聊天機器人，精準掌握好友。'
        ],
        5 => [
            'title' => 'EIP 管理系統',
            'category' => 'System、In-house',
            'description' => '建立公司內部EIP管理系統，溝通無障礙。'
        ],
    ]
];
