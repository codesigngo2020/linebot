<?php

namespace App\Http\Middleware\Web\Solution;

use Closure;
use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Solution;

class CheckDeploymentNotDestroyed
{
  public function handle(Request $request, Closure $next)
  {
    if($request->deployment->status != 'destroyed') return $next($request);
    abort(404);
  }
}
