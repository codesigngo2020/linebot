<?php

namespace App\Http\Middleware\Web\Solution;

use Closure;
use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Solution;

class HasDeployment
{
    public function handle(Request $request, Closure $next)
    {
        if(auth()->user()->deployments()->where('id', $request->deployment->id)->first()) return $next($request);
        abort(404);
    }
}
