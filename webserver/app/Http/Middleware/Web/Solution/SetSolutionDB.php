<?php

namespace App\Http\Middleware\Web\Solution;

use Config;
use Closure;
use PDO;
use Illuminate\Http\Request;

class SetSolutionDB
{
    public function handle(Request $request, Closure $next)
    {
        Config::set('database.connections.solution_'.$request->solution->id, [
            'driver' => env('DB_Solution_4_CONNECTION'),
            'host' => env('DB_Solution_4_HOST'),
            'port' => env('DB_Solution_4_PORT'),
            'database' => 'deployment_'.$request->deployment->id,
            'username' => env('DB_Solution_4_USERNAME'),
            'password' => env('DB_Solution_4_PASSWORD'),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => false,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),]) : [],
        ]);

        return $next($request);
    }
}
