<?php

namespace App\Http\Middleware\Web\Solution;

use Closure;
use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Solution;

class SetSolution
{
  public function handle(Request $request, Closure $next, $solution)
  {
    $request->request->add([
      'solution' => Solution::find($solution),
    ]);
    return $next($request);
  }
}
