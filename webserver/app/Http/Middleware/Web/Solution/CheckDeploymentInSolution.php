<?php

namespace App\Http\Middleware\Web\Solution;

use Closure;
use Illuminate\Http\Request;

// Model
// use App\Models\Platform\Solution\Solution;

class CheckDeploymentInSolution
{
    public function handle(Request $request, Closure $next)
    {
        if($request->solution->id != $request->deployment->solution_id) abort(404);
        return $next($request);
    }
}
