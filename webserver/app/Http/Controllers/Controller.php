<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->registerClass();
    }

    private function registerClass()
    {
        spl_autoload_register(function($class){
            if(preg_match('/^[a-zA-Z]+$/', $class, $match)){
                include app_path().'/Http/Requests/Web/Solution/_'.$this->solution.'/Deployment/'.ucfirst($this->type).'/'.$match[0].'.php';
            }
        });
    }
}
