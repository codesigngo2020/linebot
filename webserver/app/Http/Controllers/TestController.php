<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


// Model
use App\Models\Platform\Solution\Solution;

// Service
use App\Services\Solution\_1\YoutubeService as YoutubeService_1;
use App\Services\Solution\_1\WordprssService as WordprssService_1;

use App\Services\Solution\_2\InstagramService as InstagramService_2;
use App\Services\Solution\_2\WordprssService as WordprssService_2;

use App\Services\Solution\_3\InstagramService as InstagramService_3;



class TestController extends Controller
{

  /* ========== solution 1 ========== */

  public function solution1Crawler()
  {
    $youtubeService = new YoutubeService_1;
    return $youtubeService->crawlAll();
  }

  public function solution1CreatePost()
  {
    $wordprssService = new WordprssService_1;
    return $wordprssService->createAll();
  }



  /* ========== solution 2 ========== */

  public function solution2Crawler()
  {
    $instagramService = new InstagramService_2;
    return $instagramService->crawlAll();
  }

  public function solution2CreatePost()
  {
    $wordprssService = new WordprssService_2;
    return $wordprssService->createAll();
  }



  /* ========== solution 3 ========== */

  public function solution3Crawler()
  {
    $instagramService = new InstagramService_3;
    return $instagramService->crawlAll();
  }















  public function scanQRCode()
  {
    return view('test.scanQRCode');
  }


}
