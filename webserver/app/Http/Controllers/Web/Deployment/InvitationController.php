<?php

namespace App\Http\Controllers\Web\Deployment;

use Hash;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\Solution\Deployment;

class InvitationController extends Controller
{

    public function invitation(Request $request, $code)
    {
        $response = ['status' => 'error', 'message' => '邀請碼無效，請重新寄送邀情函'];

        foreach(auth()->user()->invitationCodes as $invitationCode){
            if(Hash::check($code.$invitationCode->user_id.$invitationCode->deployment_id.implode(',', $invitationCode->roles), $invitationCode->code)){

                if(Carbon::now()->diffInMinutes($invitationCode->created_at) > 20){
                    $response['message'] = '邀請碼已失效，請重新寄送邀情函';
                }else{
                    // 加入部署成員
                    auth()->user()->deployments()->attach($invitationCode->deployment_id);

                    // 授權身份
                    foreach($invitationCode->roles as $role){
                        auth()->user()->assignRole($role);
                    }

                    $deployment = Deployment::find($invitationCode->deployment_id);

                    $response['status'] = 'success';
                    $response['deployment'] = $deployment;
                    $response['message'] = '您已成功加入「'.$deployment->name.'」部署';
                }

                $invitationCode->delete();
                break;
            }
        };

        return view('deployment.invitation')->with(compact('response'));
    }

}
