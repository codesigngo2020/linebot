<?php

namespace App\Http\Controllers\Web\Paypal\Agreement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\Solution\Deployment;

// Helper
use App\Helpers\Basic\Paypal\AgreementHelper;

class AgreementController extends Controller
{

  public function execute(Request $request)
  {
    $deployment = Deployment::where('paypal_agreement_token', $request->token)->first();

    if($request->success && $deployment){
      $agreementHelper = new AgreementHelper;
      $agreement = $agreementHelper->execute($request->token);

      $deployment->paypal_agreement_id = $agreement->id;
      $deployment->save();
    }

    return redirect()->route('solution.'.$deployment->solution_id.'.deployment.show', $deployment);
  }


}
