<?php

namespace App\Http\Controllers\Web;

use Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Model
use App\Models\Platform\File;

class FileController extends Controller
{

  public function download(File $file)
  {
    return Storage::disk($file->disk)->download($file->path);
  }

}
