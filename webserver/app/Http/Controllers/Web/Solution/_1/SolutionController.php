<?php

namespace App\Http\Controllers\Web\Solution\_1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Request
use App\Http\Requests\Web\Solution\_1\TestDomainRequest;
use App\Http\Requests\Web\Solution\_1\TestKeyRequest;
use App\Http\Requests\Web\Solution\_1\StoreRequest;

// Service
use App\Services\Solution\_1\WordprssService;
use App\Services\Solution\_1\SolutionService;

// Helper
use App\Helpers\Solution\_1\YoutubeRequestHelper;

class SolutionController extends Controller
{
  public function store(StoreRequest $request)
  {
    $solution = $request['solution'];
    $this->solutionService = new SolutionService;
    $deployment = $this->solutionService->store($request, $solution);

    $response = [
      'status' => $deployment ? 'success' : 'error',
      'deployment' => $deployment->id,
    ];
    return response()->json($response, 200);
  }

  public function testDomain(TestDomainRequest $request)
  {
    $wordprssService = new WordprssService;
    $status = $wordprssService->associate($request->domain, $request->key);
    $status = $status ? 'associated' : 'error';

    return response()->json($status, 200);
  }

  public function testKey(TestKeyRequest $request)
  {
    $youtubeRequestHelper = new YoutubeRequestHelper;
    $status = $youtubeRequestHelper->testKey($request->key);// success or error

    return response()->json($status, 200);
  }

}
