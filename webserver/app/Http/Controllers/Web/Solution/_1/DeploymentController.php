<?php

namespace App\Http\Controllers\Web\Solution\_1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\Solution\Deployment;

// Repository
use App\Repositories\Platform\Solution\_1\CrawlerRepository;
use App\Repositories\Platform\Solution\_1\KeyRepository;


class DeploymentController extends Controller
{

  public function show(Request $request, Deployment $deployment)
  {
    $solution = $request['solution'];
    $this->crawlerRepository = new CrawlerRepository;
    $crawlers = $this->crawlerRepository->findAllByDeploymentWithUncrawelAndAllPostsCount($deployment);

    $this->keyRepository = new KeyRepository;
    $keys = $this->keyRepository->findAllByDeploymeny($deployment);
    return view('solution._1.show')->with(compact('solution', 'deployment', 'crawlers', 'keys'));
  }

  public function posts(Request $request, Deployment $deployment)
  {
    $solution = $request['solution'];
    $this->crawlerRepository = new CrawlerRepository;
    $crawlers = $deployment->solution_1_crawlers;

    $crawlers->map(function($crawler){
      $crawler->posts = $crawler->posts()
      ->select('id', 'crawler_id', 'post_id', 'video_id')
      ->with('video')
      ->orderBy('created_at', 'asc')
      ->offset(0)
      ->limit(10)
      ->get();
    });
    return view('solution._1.posts')->with(compact('solution', 'deployment', 'crawlers'));
  }


}
