<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Trait
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\ActiveableTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\CategoryableTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\ControllerTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\QueryableTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\SelectableTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\VersionableTrait;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\User;
use App\Models\Platform\Solution\_4\Script;
use App\Models\Platform\Solution\_4\Liff;

// Service
use App\Services\Solution\_4\ScriptService;

// Request
use App\Http\Requests\Web\Solution\_4\Deployment\Script\NewVersionRequest;
use App\Http\Requests\Web\Solution\_4\Deployment\Script\GetChartDataRequest;

class ScriptController extends Controller
{

    use ActiveableTrait, CategoryableTrait, ControllerTrait, QueryableTrait, SelectableTrait;

    protected $solution = 4;
    protected $type = 'script';
    protected $versionable = true;
    protected $scriptService;

    public function __construct(ScriptService $scriptService)
    {
        $this->scriptService = $scriptService;
        parent::__construct();
    }

    public function getScripts(Request $request, Deployment $deployment)
    {
        $users_count = User::count();

        // 頁碼
        $lastPage = Script::whereNull('deleted_all_at');
        if($request->q) $lastPage = $this->query($lastPage, $request->q);
        if($request->category != 0) $this->fileterByCategory($lastPage, $request->category);
        $lastPage = ceil($lastPage->distinct()->count('parent_id') / 5);

        $page = intval($request->page);
        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        // 取得 scripts id
        $parentsId = DB::connection('solution_4')
        ->table('scripts')
        ->select('parent_id');

        if($request->q) $parentsId = $this->query($parentsId, $request->q);
        if($request->category != 0) $this->fileterByCategory($parentsId, $request->category);

        $parentsId = $parentsId->whereNull('deleted_all_at')
        ->orderBy('parent_id', 'desc')
        ->offset(($page - 1) * 5)
        ->groupBy('parent_id')
        ->limit(5)
        ->pluck('parent_id');

        $scriptsId = DB::connection('solution_4')
        ->table('scripts')
        ->select('id', 'parent_id')
        ->whereIn('parent_id', $parentsId)
        ->orderBy('parent_id', 'desc')
        ->orderBy('is_current_version', 'desc')
        ->orderBy('id', 'desc')
        ->get()
        ->groupBy('parent_id')
        ->map(function($data){
            return $data[0]->id;
        });

        // 取得 scripts 資料
        $scripts = Script::select('scripts.id', 'scripts.name', 'scripts.description', 'scripts.active', 'scripts.parent_id', 'scripts.version', 'scripts.is_current_version')
        ->selectRaw('FLOOR(COUNT(DISTINCT(users.id))*100/'.$users_count.') as users_percentage')
        ->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as total_count')

        ->join('tags as script_tag', function($join){
            $join->on('script_tag.taggable_id', '=', 'scripts.id')
            ->where('script_tag.taggable_type', 'App\Models\Platform\Solution\_4\Script')
            ->where('script_tag.data->isRelatedToScript', 1)
            ->limit(1);
        })
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'script_tag.id')
        ->leftJoin('users', 'users.id', '=', 'tag_user.user_id')
        ->whereIn('scripts.id', $scriptsId);

        if($request->category != 0) $this->fileterByCategory($scripts, $request->category);

        if($request->q) $scripts = $this->query($scripts, $request->q);

        $scripts = $scripts->groupBy('scripts.id')
        ->orderBy('scripts.parent_id', 'desc')
        ->get()
        ->each(function($script){
            $script->versions_count = $script->getVersionsCount();
        });

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('scripts', 'paginator'), 200);
    }

    public function getChartData(GetChartDataRequest $request, Deployment $deployment)
    {
        $tag = DB::connection('solution_4')->table('tags');

        if($request->type == 'week'){
            $tag = $tag->selectRaw('DATE_FORMAT(tag_user.created_at, \'%Y-%m-%d\') as date');
        }else{
            $tag = $tag->selectRaw('HOUR(tag_user.created_at) as hour');
        }

        $tag = $tag->selectRaw('COUNT(tag_user.id) as count')
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'tags.id')
        ->where('tags.taggable_id', $request->script)
        ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\Script')
        ->where('tags.data->isRelatedToScript', 1);

        if($request->type == 'week'){
            $tag = $tag->where('tag_user.created_at', '>=', $request->start)
            ->where('tag_user.created_at', '<', Carbon::createFromFormat('Y-m-d', $request->start)->addDays(7)->format('Y-m-d'));
        }else{
            $tag = $tag->whereDate('tag_user.created_at', $request->start);
        }

        $tag = $tag->groupBy($request->type == 'week' ? 'date' : 'hour')
        ->get()
        ->pluck('count', $request->type == 'week' ? 'date' : 'hour');

        if($request->type == 'week'){
            $carbon = Carbon::createFromFormat('Y-m-d', $request->start);

            if(!isset($tag[$request->start])) $tag->put($request->start, 0);
            for($i = 0; $i < 6; $i ++){
                $date = $carbon->addDay()->format('Y-m-d');
                if(!isset($tag[$date])) $tag->put($date, 0);
            }
        }else{
            for($i = 0; $i < 24; $i ++){
                if(!isset($tag[$i])) $tag->put($i, 0);
            }
        }

        return response()->json($tag->sortKeys()->values(), 200);
    }

    public function create(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $liffSizes = Liff::getLiffs();

        Session::flash('alerts', [
            [
                'class' => 'light',
                'text' => '腳本建立完成後，須由關鍵字、主選單按鈕、推播訊息等訊息按鈕觸發來開啟腳本',
            ],
        ]);

        return view('solution._4.scripts.create')->with(compact('solution', 'deployment', 'liffSizes'));
    }

    public function edit(Request $request, Deployment $deployment, $scriptId)
    {
        $script = Script::where('id', $scriptId)
        ->whereNull('deleted_all_at')
        ->first();
        if(!$script) abort(404);

        $solution = $request['solution'];
        $liffSizes = Liff::getLiffs();

        return view('solution._4.scripts.edit')->with(compact('solution', 'deployment', 'script', 'liffSizes'));
    }

    public function show(Request $request, Deployment $deployment, $scriptId)
    {
        $script = Script::where('id', $scriptId)
        ->with(['nodes' => function($q){
            $q->select('id', 'script_id', 'start', 'end', 'messages');
        }])
        ->first();
        if(!$script) abort(404);

        $solution = $request['solution'];

        $script->nodes_form_data = $script->refreshNodesFromData();
        $script->total_users_count = User::count();

        $using_data = DB::connection('solution_4')
        ->table('tags')
        ->select('tags.id as tag_id')
        ->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as users_count')
        ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'tags.id')
        ->where('tags.taggable_id', $script->id)
        ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\Script')
        ->where('tags.data->isRelatedToScript', 1)
        ->first();

        $script->tag_id = $using_data->tag_id;
        $script->users_count = $using_data->users_count;
        $script->total_count = $using_data->total_count;

        $script->nodeTagsData = $this->getTagsData('node', $script->nodes->pluck('id'));
        $script->actionTagsData = $this->getTagsData('action', $script->nodes->pluck('id'), $script->nodes_form_data);

        $script->versions = $script->getVersions();

        return view('solution._4.scripts.show')->with(compact('solution', 'deployment', 'script'));
    }

    /* ========== show 輔助函示 ========== */

    private function getTagsData($type, $nodesId, $nodes_form_data = NULL)
    {
        $tags = collect();

        if($type == 'action'){

            function recurseImagemapArea(&$tags, $areas){
                foreach($areas as $area){
                    if(isset($area->id)){
                        if(isset($area->tag)) {
                            $tags->push($area->tag);
                        }
                    }else if(isset($area[0])){
                        recurseImagemapArea($tags, $area);
                    }
                }
            };

            // 取出所有tag id
            foreach($nodes_form_data as $node){
                foreach($node->messages->messages as $message){
                    switch($message->type){
                        case 'imagemap':
                        recurseImagemapArea($tags, $message->message->area->area);
                        break;

                        case 'buttons':
                        case 'confirm':
                        foreach($message->message->actions as $action){
                            if(isset($action->tag)) $tags->push($action->tag);
                        }
                        break;

                        case 'carousel':
                        case 'imagecarousel':
                        case 'quickreply':
                        $columnName = $message->type == 'quickreply' ? 'items' : 'columns';
                        foreach($message->message->{$columnName}->{$columnName} as $column){
                            foreach($column->actions as $action){
                                if(isset($action->tag)) $tags->push($action->tag);
                            }
                        }
                        break;
                    }
                }
            }
        }

        if($type == 'action' && $tags->isEmpty()){
            return [];
        }else{
            $data = DB::connection('solution_4')->table('tags');

            if($type == 'node'){
                $data = $data->select('tags.id as tag_id', 'tags.taggable_id as node_id');
            }else if($type == 'action'){
                $data = $data->select('tags.id');
            }

            $data = $data->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as users_count')
            ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')
            ->leftJoin('tag_user as tag_user', 'tag_user.tag_id', '=', 'tags.id');

            if($type == 'node'){
                $data = $data->whereIn('tags.taggable_id', $nodesId)
                ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\ScriptNode')
                ->where('tags.data->isRelatedToNode', 1);
            }else if($type == 'action'){
                $data = $data->whereIn('tags.id', $tags);
            }

            $data = $data->groupBy('tags.id')->get();

            if($type == 'node'){
                return $data->keyBy('node_id');
            }else if($type == 'action'){
                return $data->keyBy('id');
            }
        }
    }

}
