<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Trait
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\CategoryableTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\QueryableTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\VersionableTrait;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Message;
use App\Models\Platform\Solution\_4\Liff;
use App\Models\Platform\Solution\_4\Script;
use App\Models\Platform\Solution\_4\User;
use App\Models\Platform\Solution\_4\Table;

// Service
use App\Services\Solution\_4\MessagesPoolService;

// Request
use App\Http\Requests\Web\Solution\_4\Deployment\MessagesPool\StoreMessageRequest;
use App\Http\Requests\Web\Solution\_4\Deployment\MessagesPool\StoreTemplateRequest;
use App\Http\Requests\Web\Solution\_4\Deployment\MessagesPool\NewMessageVersionRequest;
use App\Http\Requests\Web\Solution\_4\Deployment\MessagesPool\NewTemplateVersionRequest;

class MessagesPoolController extends Controller
{
    use CategoryableTrait, QueryableTrait, VersionableTrait;

    protected $solution = 4;
    protected $type = 'messagesPool';
    protected $model = 'message';
    protected $table = 'messages';
    protected $view = 'messages-pool';
    protected $messagesPoolService;

    public function __construct(MessagesPoolService $messagesPoolService)
    {
        $this->messagesPoolService = $messagesPoolService;
        parent::__construct();
    }

    public function index(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        return view('solution._4.messages-pool.index')->with(compact('solution', 'deployment'));
    }

    public function getMessages(Request $request, Deployment $deployment)
    {
        // 頁碼
        $lastPage = Message::whereNull('deleted_all_at');
        if($request->q) $lastPage = $this->query($lastPage, $request->q);
        if($request->category != 0) $this->fileterByCategory($lastPage, $request->category);
        $lastPage = ceil($lastPage->distinct()->count('parent_id') / 5);

        $page = intval($request->page);
        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        // 取得 messages id
        $parentsId = DB::connection('solution_4')
        ->table('messages')
        ->select('messages.parent_id');

        if($request->q) $parentsId = $this->query($parentsId, $request->q);
        if($request->category != 0) $this->fileterByCategory($parentsId, $request->category);

        $parentsId = $parentsId->whereNull('messages.deleted_all_at')
        ->orderBy('messages.parent_id', 'desc')
        ->offset(($page - 1) * 5)
        ->groupBy('messages.parent_id')
        ->limit(5)
        ->pluck('messages.parent_id');

        $mesagesId = DB::connection('solution_4')
        ->table('messages')
        ->select('id', 'parent_id')
        ->whereIn('parent_id', $parentsId)
        ->orderBy('parent_id', 'desc')
        ->orderBy('is_current_version', 'desc')
        ->orderBy('id', 'desc')
        ->get()
        ->groupBy('parent_id')
        ->map(function($data){
            return $data[0]->id;
        });

        // 取得 messages 資料

        $messages = Message::select('messages.id', 'messages.name', 'messages.description', 'messages.type', 'messages.template_type', 'messages.messages', 'messages.parent_id', 'messages.version', 'messages.is_current_version')
        ->whereIn('messages.id', $mesagesId);

        if($request->category != 0) $this->fileterByCategory($messages, $request->category);

        $messages = $messages->orderBy('messages.parent_id', 'desc')
        ->get()
        ->each(function($message){
            $message->versions_count = $message->getVersionsCount();
        });

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('messages', 'paginator'), 200);
    }

    public function create(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $liffSizes = Liff::getLiffs();
        return view('solution._4.messages-pool.create')->with(compact('solution', 'deployment', 'liffSizes'));
    }

    public function storeMessage(StoreMessageRequest $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $message = $this->messagesPoolService->storeMessage($request, $solution, $deployment);

        $response = $message ? ['status' => 'success', 'messageId' => $message->id] : ['status' => 'error'];
        return response()->json($response, 200);
    }

    public function storeTemplate(StoreTemplateRequest $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $message = $this->messagesPoolService->storeTemplate($request, $solution, $deployment);

        $response = $message ? ['status' => 'success', 'messageId' => $message->id] : ['status' => 'error'];
        return response()->json($response, 200);
    }

    public function edit(Request $request, Deployment $deployment, $messageId)
    {
        $message = Message::where('id', $messageId)
        ->whereNull('deleted_all_at')
        ->first();
        if(!$message) abort(404);

        if($message->type == 'template' && $message->template_type == 'coupon'){
            $message->table = Table::select('id as value', 'name', 'settings')
            ->where('id', $message->settings->table)
            ->first();
        }

        $solution = $request['solution'];
        $liffSizes = Liff::getLiffs();

        return view('solution._4.messages-pool.edit')->with(compact('solution', 'deployment', 'message', 'liffSizes'));
    }

    public function newMessageVersion(NewMessageVersionRequest $request, Deployment $deployment, $messageId)
    {
        $message = Message::where('id', $messageId)
        ->whereNull('deleted_all_at')
        ->first();
        if(!$message) abort(404);

        $solution = $request['solution'];
        $message = $this->messagesPoolService->newMessageVersion($request, $solution, $deployment, $message);

        $response = $message ? ['status' => 'success', 'messageId' => $message->id] : ['status' => 'error'];
        return response()->json($response, 200);
    }

    public function newTemplateVersion(NewTemplateVersionRequest $request, Deployment $deployment, $messageId)
    {
        $message = Message::where('id', $messageId)
        ->whereNull('deleted_all_at')
        ->first();
        if(!$message) abort(404);

        $solution = $request['solution'];
        $message = $this->messagesPoolService->newTemplateVersion($request, $solution, $deployment, $message);

        $response = $message ? ['status' => 'success', 'messageId' => $message->id] : ['status' => 'error'];
        return response()->json($response, 200);
    }

    public function show(Request $request, Deployment $deployment, $messageId)
    {
        $message = Message::find($messageId);
        if(!$message) abort(404);

        $solution = $request['solution'];

        if($message->type == 'message'){
            $message->messages_form_data = $message->refreshMessagesFromData();
        }else{
            $message->nodes_form_data = $message->refreshNodesFromData();
            $message->total_users_count = User::count();

            $using_data = DB::connection('solution_4')
            ->table('tags')
            ->select('tags.id as tag_id')
            ->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as users_count')
            ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')
            ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'tags.id')
            ->where('tags.taggable_id', $message->id)
            ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\Message')
            ->where('tags.data->isRelatedToMessage', 1)
            ->first();

            $message->tag_id = $using_data->tag_id;
            $message->users_count = $using_data->users_count;
            $message->total_count = $using_data->total_count;

            $message->nodeTagsData = $this->getTagsData('node', $message->nodes->pluck('id'));
            $message->actionTagsData = $this->getTagsData('action', $message->nodes->pluck('id'), $message->nodes_form_data);
        }

        $message->versions = $message->getVersions();

        return view('solution._4.messages-pool.show')->with(compact('solution', 'deployment', 'message'));
    }

    public function getMessage(Request $request, Deployment $deployment, $messageId)
    {
        if($message = Message::select('messages_form_data')->where('parent_id', $messageId)->where('is_current_version', 1)->first()){
            return response()->json($message->messages_form_data, 200);
        }else{
            abort(404);
        }
    }

    public function queryForSelect(Request $request, Deployment $deployment)
    {
        $messages = Message::select('id', 'parent_id as value', 'name')
        ->where('is_current_version', 1);

        if($request->type) $messages = $messages->where('type', $request->type);
        if($request->q) $messages = $messages->where('name', 'like', '%'.$request->q.'%');

        $messages = $messages->orderBy('parent_id', 'desc')->limit(11)->distinct()->get();

        return response()->json($messages, 200);
    }

    /* ========== show 輔助函示 ========== */

    private function getTagsData($type, $nodesId, $nodes_form_data = NULL)
    {
        $tags = collect();

        if($type == 'action'){

            function recurseImagemapArea(&$tags, $areas){
                foreach($areas as $area){
                    if(isset($area->id)){
                        if(isset($area->tag)) {
                            $tags->push($area->tag);
                        }
                    }else if(isset($area[0])){
                        recurseImagemapArea($tags, $area);
                    }
                }
            };

            // 取出所有tag id
            foreach($nodes_form_data as $node){
                foreach($node->messages->messages as $message){
                    switch($message->type){
                        case 'imagemap':
                        recurseImagemapArea($tags, $message->message->area->area);
                        break;

                        case 'buttons':
                        case 'confirm':
                        foreach($message->message->actions as $action){
                            if(isset($action->tag)) $tags->push($action->tag);
                        }
                        break;

                        case 'carousel':
                        case 'imagecarousel':
                        case 'quickreply':
                        $columnName = $message->type == 'quickreply' ? 'items' : 'columns';
                        foreach($message->message->{$columnName}->{$columnName} as $column){
                            foreach($column->actions as $action){
                                if(isset($action->tag)) $tags->push($action->tag);
                            }
                        }
                        break;
                    }
                }
            }
        }

        if($type == 'action' && $tags->isEmpty()){
            return [];
        }else{
            $data = DB::connection('solution_4')->table('tags');

            if($type == 'node'){
                $data = $data->select('tags.id as tag_id', 'tags.taggable_id as node_id');
            }else if($type == 'action'){
                $data = $data->select('tags.id');
            }

            $data = $data->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as users_count')
            ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')
            ->leftJoin('tag_user as tag_user', 'tag_user.tag_id', '=', 'tags.id');

            if($type == 'node'){
                $data = $data->whereIn('tags.taggable_id', $nodesId)
                ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\MessageNode')
                ->where('tags.data->isRelatedToNode', 1);
            }else if($type == 'action'){
                $data = $data->whereIn('tags.id', $tags);
            }

            $data = $data->groupBy('tags.id')->get();

            if($type == 'node'){
                return $data->keyBy('node_id');
            }else if($type == 'action'){
                return $data->keyBy('id');
            }
        }
    }

}
