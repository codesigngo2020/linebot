<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Trait
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\ControllerTrait;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\WelcomeMessage;
use App\Models\Platform\Solution\_4\User;
use App\Models\Platform\Solution\_4\Liff;

// Service
use App\Services\Solution\_4\WelcomeMessageService;

class WelcomeMessageController extends Controller
{

    use ControllerTrait;

    protected $solution = 4;
    protected $type = 'welcomeMessage';
    protected $welcomeMessageService;

    public function __construct(WelcomeMessageService $welcomeMessageService)
    {
        $this->welcomeMessageService = $welcomeMessageService;
        parent::__construct();
    }

    public function index(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];

        /* ========== 當下的自動回覆 ========== */

        $users_count = User::count();

        $carbon = Carbon::now();
        $date = $carbon->format('Y-m-d');
        $time = $carbon->format('H:i:s');

        $currentWelcomeMessage = DB::connection('solution_4')
        ->table('periods')
        ->select('welcome_messages.id', 'welcome_messages.name', 'welcome_messages.description', 'welcome_messages.messages', 'welcome_messages.active')
        ->selectRaw('FLOOR(COUNT(DISTINCT(tag_user.user_id))*100/'.$users_count.') as users_percentage')
        ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')
        ->selectRaw('COUNT(DISTINCT(tags.id)) as tags_count')

        ->join('welcome_messages', function($join){
            $join->on('welcome_messages.id', '=', 'periods.periodable_id')
            ->where('welcome_messages.active', 1);
        })

        ->join('tags', function($join){
            $join->on('tags.taggable_id', '=', 'welcome_messages.id')
            ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\WelcomeMessage');
        })
        ->join('tags as welcomeMessage_tag', function($join){
            $join->on('welcomeMessage_tag.taggable_id', '=', 'welcome_messages.id')
            ->where('welcomeMessage_tag.taggable_type', 'App\Models\Platform\Solution\_4\WelcomeMessage')
            ->where('welcomeMessage_tag.data->isRelatedToWelcomeMessage_tag', 1)
            ->limit(1);
        })
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'welcomeMessage_tag.id')

        ->where('periods.periodable_type', 'App\Models\Platform\Solution\_4\WelcomeMessage')
        ->where('periods.start_date', '<=', $date)
        ->where(function($q) use($date){
            $q->whereNull('periods.end_date')
            ->orWhere('periods.end_date', '>=', $date);
        })
        ->where('periods.start_time', '<=', $time)
        ->where('periods.end_time', '>=', $time)

        ->orderBy('periods.id', 'desc')
        ->first();

        $alerts = [
            [
                'class' => 'light',
                'text' => '同一時間有多個歡迎訊息時，僅取最後建立的訊息回覆。',
            ],
        ];

        if(!$currentWelcomeMessage->id){
            $currentWelcomeMessage = NULL;
            array_unshift($alerts, [
                'class' => 'warning-white',
                'text' => '目前沒有任何歡迎訊息是有效的。',
            ]);
        }else{
            $currentWelcomeMessage->messages = json_decode($currentWelcomeMessage->messages);
        }

        Session::flash('alerts', $alerts);

        return view('solution._4.welcome-messages.index')->with(compact('solution', 'deployment', 'currentWelcomeMessage'));
    }

    public function getWelcomeMessageCalendar(Request $request, Deployment $deployment)
    {
        $welcomeMessages = DB::connection('solution_4')
        ->table('welcome_messages')
        ->select('welcome_messages.id', 'welcome_messages.name', 'welcome_messages.active')

        ->join('periods', function($join) use($request){
            $join->on('periods.periodable_id', '=', 'welcome_messages.id')
            ->where('periods.periodable_type', 'App\Models\Platform\Solution\_4\WelcomeMessage')
            ->where('periods.start_date', '<=', $request->end)
            ->where(function($q) use($request){
                $q->whereNull('periods.end_date')
                ->orWhere('periods.end_date', '>=', $request->start);
            })
            ->limit(1);
        })
        ->orderBy('welcome_messages.id')
        ->distinct()
        ->get()
        ->each(function($welcomeMessage) use($request){
            $welcomeMessage->periods = DB::connection('solution_4')
            ->table('periods')
            ->select('periods.start_date', 'periods.end_date')
            ->where('periods.periodable_id', $welcomeMessage->id)
            ->where('periods.periodable_type', 'App\Models\Platform\Solution\_4\WelcomeMessage')
            ->where('periods.start_date', '<=', $request->end)
            ->where(function($q) use($request){
                $q->whereNull('periods.end_date')
                ->orWhere('periods.end_date', '>=', $request->start);
            })
            ->get();
        });

        return response()->json(compact('welcomeMessages'), 200);
    }

    public function getWelcomeMessageList(Request $request, Deployment $deployment)
    {
        $users_count = User::count();

        $lastPage = WelcomeMessage::query();
        if($request->q) $lastPage = $this->query($lastPage, $request->q);
        $lastPage = ceil($lastPage->count() / 5);

        $page = intval($request->page);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $welcomeMessages = DB::connection('solution_4')
        ->table('welcome_messages')
        ->select('welcome_messages.id', 'welcome_messages.name', 'welcome_messages.description', 'welcome_messages.messages', 'welcome_messages.template_id', 'welcome_messages.active')

        ->selectRaw('FLOOR(COUNT(DISTINCT(tag_user.user_id))*100/'.$users_count.') as users_percentage')
        ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')
        ->selectRaw('COUNT(DISTINCT(tags.id)) as tags_count')

        ->join('tags', function($join){
            $join->on('tags.taggable_id', '=', 'welcome_messages.id')
            ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\WelcomeMessage');
        })
        ->join('tags as welcomeMessage_tag', function($join){
            $join->on('welcomeMessage_tag.taggable_id', '=', 'welcome_messages.id')
            ->where('welcomeMessage_tag.taggable_type', 'App\Models\Platform\Solution\_4\WelcomeMessage')
            ->where('welcomeMessage_tag.data->isRelatedToWelcomeMessage', 1)
            ->limit(1);
        })
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'welcomeMessage_tag.id');

        if($request->q) $welcomeMessages = $this->query($welcomeMessages, $request->q);

        $welcomeMessages = $welcomeMessages->groupBy('welcome_messages.id')
        ->orderBy('welcome_messages.id', 'desc')
        ->offset(($page - 1) * 5)
        ->limit(5)
        ->get()
        ->each(function($welcomeMessage){
            if($welcomeMessage->template_id){
                $welcomeMessage->messages = [
                    [
                        'type' => '訊息模組',
                        'hasQuickReplay' => false,
                    ]
                ];
            }else{
                $welcomeMessage->messages = array_map(function($message){
                    if($message->type == 'template'){
                        return [
                            'type' => $message->template->type == 'image_carousel' ? 'imagecarousel' : $message->template->type,
                            'hasQuickReplay' => isset($message->quickReply),
                        ];
                    }else{
                        return [
                            'type' => $message->type,
                            'hasQuickReplay' => isset($message->quickReply),
                        ];
                    }
                }, json_decode($welcomeMessage->messages));
            }
        });

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('welcomeMessages', 'paginator'), 200);
    }

    public function create(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $liffSizes = Liff::getLiffs();

        Session::flash('alerts', [
            [
                'class' => 'light',
                'text' => '同一時間有多個歡迎訊息時，僅取最後建立的訊息回覆。',
            ],
        ]);

        return view('solution._4.welcome-messages.create')->with(compact('solution', 'deployment', 'liffSizes'));
    }

    public function show(Request $request, Deployment $deployment, $welcomeMessageId)
    {
        $welcomeMessage = WelcomeMessage::where('id', $welcomeMessageId)
        ->with(['periods' => function($q){
            $q->select('id', 'periodable_id', 'periodable_type', 'start_date', 'end_date', 'start_time', 'end_time')
            ->limit(5);
        }])
        ->first();
        if(!$welcomeMessage) abort(404);

        $solution = $request['solution'];

        $welcomeMessage->total_users_count = User::count();
        $usingData = DB::connection('solution_4')
        ->table('tags')
        ->select('tags.id as tag_id')
        ->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as users_count')
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'tags.id')
        ->where('tags.taggable_id', $welcomeMessage->id)
        ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\WelcomeMessage')
        ->where('tags.data->isRelatedToWelcomeMessage', 1)
        ->first();

        $welcomeMessage->tag_id = $usingData->tag_id;
        $welcomeMessage->users_count = $usingData->users_count;

        if($welcomeMessage->template_id){
            $welcomeMessage->load('template');
        }else{
            $welcomeMessage->messages_form_data = $welcomeMessage->refreshMessagesFromData();
            $welcomeMessage->tagsData = $this->getTagsData($welcomeMessage->messages_form_data);
        }

        return view('solution._4.welcome-messages.show')->with(compact('solution', 'deployment', 'welcomeMessage'));
    }

    public function toggleActive(Request $request, Deployment $deployment, $welcomeMessageId)
    {
        $welcomeMessage = WelcomeMessage::find($welcomeMessageId);
        if(!$welcomeMessage) abort(404);

        $welcomeMessage->active = !$welcomeMessage->active;
        $welcomeMessage->save();

        return response()->json(['status' => 'success'], 200);
    }

    /* ========== getWelcomeMessageList 輔助函示 ========== */

    private function query($queryBuilder, $q)
    {
        $likeQ = '%'.$q.'%';
        return $queryBuilder->where('welcome_messages.id', $q)
        ->orWhere('welcome_messages.name', 'like', $likeQ)
        ->orWhere('welcome_messages.description', 'like', $likeQ);
    }

    /* ========== show 輔助函示 ========== */

    private function getTagsData($messages_form_data)
    {
        $tags = collect();

        function recurseImagemapArea(&$tags, $areas){
            foreach($areas as $area){
                if(isset($area->id)){
                    if(isset($area->tag)) {
                        $tags->push($area->tag);
                    }
                }else if(isset($area[0])){
                    recurseImagemapArea($tags, $area);
                }
            }
        };

        // 取出所有tag id
        foreach($messages_form_data as $message){
            switch($message->type){
                case 'imagemap':
                recurseImagemapArea($tags, $message->message->area->area);
                break;

                case 'buttons':
                case 'confirm':
                foreach($message->message->actions as $action){
                    if(isset($action->tag)) $tags->push($action->tag);
                }
                break;

                case 'carousel':
                case 'imagecarousel':
                case 'quickreply':
                $columnName = $message->type == 'quickreply' ? 'items' : 'columns';
                foreach($message->message->{$columnName}->{$columnName} as $column){
                    foreach($column->actions as $action){
                        if(isset($action->tag)) $tags->push($action->tag);
                    }
                }
                break;
            }
        }

        return DB::connection('solution_4')
        ->table('tags')
        ->select('tags.id')

        ->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as users_count')
        ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')

        ->leftJoin('tag_user as tag_user', 'tag_user.tag_id', '=', 'tags.id')

        ->whereIn('tags.id', $tags)
        ->groupBy('tags.id')
        ->get()
        ->keyBy('id');
    }

}
