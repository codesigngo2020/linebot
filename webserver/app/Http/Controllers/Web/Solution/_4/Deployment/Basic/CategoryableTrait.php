<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment\Basic;

use DB;

// Model
use App\Models\Platform\Solution\_4\Category;

trait CategoryableTrait
{

    private function fileterByCategory($queryBuilder, $category)
    {
        $table = ($this->table ?? $this->type.'s');
        if($category == -1){
            return $queryBuilder->leftJoin('categoryables', function($join) use($table){
                $join->on('categoryables.categoryable_id', '=', $table.'.id')
                ->where('categoryables.categoryable_type', 'App\Models\Platform\Solution\_4\\'.($this->model ?? ucfirst($this->type)));
            })
            ->whereNull('categoryables.category_id');
        }elseif($category > 0){
            $category = Category::find($category);
            $categories = DB::connection('solution_4')
            ->table('categories')
            ->select('id')
            ->where('parent_id', $category->parent_id)
            ->where('start', '>=', $category->start)
            ->where('end', '<=', $category->end)
            ->pluck('id');

            return $queryBuilder->join('categoryables', function($join) use($table, $categories){
                $join->on('categoryables.categoryable_id', '=', $table.'.id')
                ->where('categoryables.categoryable_type', 'App\Models\Platform\Solution\_4\\'.($this->model ?? ucfirst($this->type)))
                ->whereIn('categoryables.category_id', $categories);
            });
        }else{
            return $queryBuilder;
        }
    }

}
