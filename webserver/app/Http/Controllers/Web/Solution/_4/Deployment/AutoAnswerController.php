<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Trait
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\ControllerTrait;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\AutoAnswer;
use App\Models\Platform\Solution\_4\User;
use App\Models\Platform\Solution\_4\Liff;
use App\Models\Platform\Solution\_4\Script;
use App\Models\Platform\Solution\_4\Message;

// Service
use App\Services\Solution\_4\AutoAnswerService;

class AutoAnswerController extends Controller
{

    use ControllerTrait;

    protected $solution = 4;
    protected $type = 'autoAnswer';
    protected $autoAnswerService;

    public function __construct(AutoAnswerService $autoAnswerService)
    {
        $this->autoAnswerService = $autoAnswerService;
        parent::__construct();
    }

    public function index(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];

        /* ========== 當下的自動回覆 ========== */

        $users_count = User::count();

        $carbon = Carbon::now();
        $date = $carbon->format('Y-m-d');
        $time = $carbon->format('H:i:s');

        $currentAutoAnswer = DB::connection('solution_4')
        ->table('periods')
        ->select('auto_answers.id', 'auto_answers.name', 'auto_answers.description', 'auto_answers.messages', 'auto_answers.active')
        ->selectRaw('FLOOR(COUNT(DISTINCT(tag_user.user_id))*100/'.$users_count.') as users_percentage')
        ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')
        ->selectRaw('COUNT(DISTINCT(tags.id)) as tags_count')

        ->join('auto_answers', function($join){
            $join->on('auto_answers.id', '=', 'periods.periodable_id')
            ->where('auto_answers.active', 1);
        })

        ->join('tags', function($join){
            $join->on('tags.taggable_id', '=', 'auto_answers.id')
            ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\AutoAnswer');
        })
        ->join('tags as autoAnswer_tag', function($join){
            $join->on('autoAnswer_tag.taggable_id', '=', 'auto_answers.id')
            ->where('autoAnswer_tag.taggable_type', 'App\Models\Platform\Solution\_4\AutoAnswer')
            ->where('autoAnswer_tag.data->isRelatedToAutoAnswer', 1)
            ->limit(1);
        })
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'autoAnswer_tag.id')

        ->where('periods.periodable_type', 'App\Models\Platform\Solution\_4\AutoAnswer')
        ->where('periods.start_date', '<=', $date)
        ->where(function($q) use($date){
            $q->whereNull('periods.end_date')
            ->orWhere('periods.end_date', '>=', $date);
        })
        ->where('periods.start_time', '<=', $time)
        ->where('periods.end_time', '>=', $time)

        ->orderBy('periods.id', 'desc')
        ->first();

        $alerts = [
            [
                'class' => 'light',
                'text' => '同一時間有多個自動回覆時，僅取最後建立的訊息回覆。',
            ],
        ];

        if(!$currentAutoAnswer->id){
            $currentAutoAnswer = NULL;
            array_unshift($alerts, [
                'class' => 'warning-white',
                'text' => '目前沒有任何自動回覆是有效的。',
            ]);
        }else{
            $currentAutoAnswer->messages = json_decode($currentAutoAnswer->messages);
        }

        Session::flash('alerts', $alerts);

        return view('solution._4.auto-answers.index')->with(compact('solution', 'deployment', 'currentAutoAnswer'));
    }

    public function getAutoAnswerCalendar(Request $request, Deployment $deployment)
    {
        $autoAnswers = DB::connection('solution_4')
        ->table('auto_answers')
        ->select('auto_answers.id', 'auto_answers.name', 'auto_answers.active')

        ->join('periods', function($join) use($request){
            $join->on('periods.periodable_id', '=', 'auto_answers.id')
            ->where('periods.periodable_type', 'App\Models\Platform\Solution\_4\AutoAnswer')
            ->where('periods.start_date', '<=', $request->end)
            ->where(function($q) use($request){
                $q->whereNull('periods.end_date')
                ->orWhere('periods.end_date', '>=', $request->start);
            })
            ->limit(1);
        })
        ->orderBy('auto_answers.id')
        ->distinct()
        ->get()
        ->each(function($autoAnswer) use($request){
            $autoAnswer->periods = DB::connection('solution_4')
            ->table('periods')
            ->select('periods.start_date', 'periods.end_date')
            ->where('periods.periodable_id', $autoAnswer->id)
            ->where('periods.periodable_type', 'App\Models\Platform\Solution\_4\AutoAnswer')
            ->where('periods.start_date', '<=', $request->end)
            ->where(function($q) use($request){
                $q->whereNull('periods.end_date')
                ->orWhere('periods.end_date', '>=', $request->start);
            })
            ->get();
        });

        return response()->json(compact('autoAnswers'), 200);
    }

    public function getAutoAnswerList(Request $request, Deployment $deployment)
    {
        $users_count = User::count();

        $lastPage = AutoAnswer::query();
        if($request->q) $lastPage = $this->query($lastPage, $request->q);
        $lastPage = ceil($lastPage->count() / 5);

        $page = intval($request->page);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $autoAnswers = DB::connection('solution_4')
        ->table('auto_answers')
        ->select('auto_answers.id', 'auto_answers.name', 'auto_answers.description', 'auto_answers.messages', 'auto_answers.template_id', 'auto_answers.active')

        ->selectRaw('FLOOR(COUNT(DISTINCT(tag_user.user_id))*100/'.$users_count.') as users_percentage')
        ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')
        ->selectRaw('COUNT(DISTINCT(tags.id)) as tags_count')

        ->join('tags', function($join){
            $join->on('tags.taggable_id', '=', 'auto_answers.id')
            ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\AutoAnswer');
        })
        ->join('tags as autoAnswer_tag', function($join){
            $join->on('autoAnswer_tag.taggable_id', '=', 'auto_answers.id')
            ->where('autoAnswer_tag.taggable_type', 'App\Models\Platform\Solution\_4\AutoAnswer')
            ->where('autoAnswer_tag.data->isRelatedToAutoAnswer', 1)
            ->limit(1);
        })
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'autoAnswer_tag.id');

        if($request->q) $autoAnswers = $this->query($autoAnswers, $request->q);

        $autoAnswers = $autoAnswers->groupBy('auto_answers.id')
        ->orderBy('auto_answers.id', 'desc')
        ->offset(($page - 1) * 5)
        ->limit(5)
        ->get()
        ->each(function($autoAnswer){
            if($autoAnswer->template_id){
                $autoAnswer->messages = [
                    [
                        'type' => '訊息模組',
                        'hasQuickReplay' => false,
                    ]
                ];
            }else{
                $autoAnswer->messages = array_map(function($message){
                    if($message->type == 'template'){
                        return [
                            'type' => $message->template->type == 'image_carousel' ? 'imagecarousel' : $message->template->type,
                            'hasQuickReplay' => isset($message->quickReply),
                        ];
                    }else{
                        return [
                            'type' => $message->type,
                            'hasQuickReplay' => isset($message->quickReply),
                        ];
                    }
                }, json_decode($autoAnswer->messages));
            }
        });

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('autoAnswers', 'paginator'), 200);
    }

    public function create(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $liffSizes = Liff::getLiffs();

        Session::flash('alerts', [
            [
                'class' => 'light',
                'text' => '同一時間有多個自動回覆時，僅取最後建立的訊息回覆。',
            ],
        ]);

        return view('solution._4.auto-answers.create')->with(compact('solution', 'deployment', 'liffSizes'));
    }

    public function show(Request $request, Deployment $deployment, $autoAnswerId)
    {
        $autoAnswer = AutoAnswer::where('id', $autoAnswerId)
        ->with(['periods' => function($q){
            $q->select('id', 'periodable_id', 'periodable_type', 'start_date', 'end_date', 'start_time', 'end_time')
            ->limit(5);
        }])
        ->first();
        if(!$autoAnswer) abort(404);

        $solution = $request['solution'];

        $autoAnswer->total_users_count = User::count();
        $usingData = DB::connection('solution_4')
        ->table('tags')
        ->select('tags.id as tag_id')
        ->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as users_count')
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'tags.id')
        ->where('tags.taggable_id', $autoAnswer->id)
        ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\AutoAnswer')
        ->where('tags.data->isRelatedToAutoAnswer', 1)
        ->first();

        $autoAnswer->tag_id = $usingData->tag_id;
        $autoAnswer->users_count = $usingData->users_count;

        if($autoAnswer->template_id){
            $autoAnswer->load('template');
        }else{
            $autoAnswer->messages_form_data = $autoAnswer->refreshMessagesFromData();
            $autoAnswer->tagsData = $this->getTagsData($autoAnswer->messages_form_data);
        }

        return view('solution._4.auto-answers.show')->with(compact('solution', 'deployment', 'autoAnswer'));
    }

    public function toggleActive(Request $request, Deployment $deployment, $autoAnswerId)
    {
        $autoAnswer = AutoAnswer::find($autoAnswerId);
        if(!$autoAnswer) abort(404);

        $autoAnswer->active = !$autoAnswer->active;
        $autoAnswer->save();

        return response()->json(['status' => 'success'], 200);
    }

    /* ========== getAutoAnswerList 輔助函示 ========== */

    private function query($queryBuilder, $q)
    {
        $likeQ = '%'.$q.'%';
        return $queryBuilder->where('auto_answers.id', $q)
        ->orWhere('auto_answers.name', 'like', $likeQ)
        ->orWhere('auto_answers.description', 'like', $likeQ);
    }

    /* ========== show 輔助函示 ========== */

    private function getTagsData($messages_form_data)
    {
        $tags = collect();

        function recurseImagemapArea(&$tags, $areas){
            foreach($areas as $area){
                if(isset($area->id)){
                    if(isset($area->tag)) {
                        $tags->push($area->tag);
                    }
                }else if(isset($area[0])){
                    recurseImagemapArea($tags, $area);
                }
            }
        };

        // 取出所有tag id
        foreach($messages_form_data as $message){
            switch($message->type){
                case 'imagemap':
                recurseImagemapArea($tags, $message->message->area->area);
                break;

                case 'buttons':
                case 'confirm':
                foreach($message->message->actions as $action){
                    if(isset($action->tag)) $tags->push($action->tag);
                }
                break;

                case 'carousel':
                case 'imagecarousel':
                case 'quickreply':
                $columnName = $message->type == 'quickreply' ? 'items' : 'columns';
                foreach($message->message->{$columnName}->{$columnName} as $column){
                    foreach($column->actions as $action){
                        if(isset($action->tag)) $tags->push($action->tag);
                    }
                }
                break;
            }
        }

        return DB::connection('solution_4')
        ->table('tags')
        ->select('tags.id')

        ->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as users_count')
        ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')

        ->leftJoin('tag_user as tag_user', 'tag_user.tag_id', '=', 'tags.id')

        ->whereIn('tags.id', $tags)
        ->groupBy('tags.id')
        ->get()
        ->keyBy('id');
    }

}
