<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Trait
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\ActiveableTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\CategoryableTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\ControllerTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\QueryableTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\SelectableTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\VersionableTrait;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\User;
use App\Models\Platform\Solution\_4\Keyword;
use App\Models\Platform\Solution\_4\Script;
use App\Models\Platform\Solution\_4\Liff;
use App\Models\Platform\Solution\_4\Message;
use App\Models\Platform\Solution\_4\Richmenu;

// Service
use App\Services\Solution\_4\KeywordService;

// Request
use App\Http\Requests\Web\Solution\_4\Deployment\Keyword\GetChartDataRequest;

class KeywordController extends Controller
{
    use ActiveableTrait, CategoryableTrait, ControllerTrait, QueryableTrait, SelectableTrait, QueryableTrait, VersionableTrait;

    protected $solution = 4;
    protected $type = 'keyword';
    protected $versionable = true;
    protected $keywordService;

    public function __construct(KeywordService $keywordService)
    {
        $this->keywordService = $keywordService;
        parent::__construct();
    }

    public function getKeywords(Request $request, Deployment $deployment)
    {
        $users_count = User::count();

        // 頁碼
        $lastPage = keyword::whereNull('deleted_all_at');
        if($request->q) $lastPage = $this->query($lastPage, $request->q);
        if($request->category != 0) $this->fileterByCategory($lastPage, $request->category);
        $lastPage = ceil($lastPage->distinct()->count('parent_id') / 5);

        $page = intval($request->page);
        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        // 取得 keywords id
        $parentsId = DB::connection('solution_4')
        ->table('keywords')
        ->select('parent_id');

        if($request->q) $parentsId = $this->query($parentsId, $request->q);
        if($request->category != 0) $this->fileterByCategory($parentsId, $request->category);

        $parentsId = $parentsId->whereNull('deleted_all_at')
        ->orderBy('parent_id', 'desc')
        ->offset(($page - 1) * 5)
        ->groupBy('parent_id')
        ->limit(5)
        ->pluck('parent_id');

        $keywordsId = DB::connection('solution_4')
        ->table('keywords')
        ->select('id', 'parent_id')
        ->whereIn('parent_id', $parentsId)
        ->orderBy('parent_id', 'desc')
        ->orderBy('is_current_version', 'desc')
        ->orderBy('id', 'desc')
        ->get()
        ->groupBy('parent_id')
        ->map(function($data){
            return $data[0]->id;
        });

        // 取得 keywords 資料
        $keywords = Keyword::select('keywords.id', 'keywords.name', 'keywords.description', 'keywords.active', 'keywords.parent_id', 'keywords.version', 'keywords.is_current_version')
        ->selectRaw('FLOOR(COUNT(DISTINCT(users.id))*100/'.$users_count.') as users_percentage')
        ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')
        ->selectRaw(($request->category == 0 || $request->category == -1 ? $request->category : 'categoryables.category_id').' as category_id')
        ->join('tags as keyword_tag', function($join){
            $join->on('keyword_tag.taggable_id', '=', 'keywords.id')
            ->where('keyword_tag.taggable_type', 'App\Models\Platform\Solution\_4\Keyword')
            ->where('keyword_tag.data->isRelatedToKeyword', 1)
            ->limit(1);
        })
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'keyword_tag.id')
        ->leftJoin('users', 'users.id', '=', 'tag_user.user_id')
        ->whereIn('keywords.id', $keywordsId);

        if($request->category != 0) $this->fileterByCategory($keywords, $request->category);

        $keywords = $keywords->groupBy('keywords.id')
        ->orderBy('keywords.parent_id', 'desc')
        ->get()
        ->each(function($keyword){
            $keyword->versions_count = $keyword->getVersionsCount();
        });

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('keywords', 'paginator'), 200);
    }

    public function getChartData(GetChartDataRequest $request, Deployment $deployment)
    {
        $keyword = Keyword::find($request->keyword);

        $tag = DB::connection('solution_4')->table('tags');

        if($request->type == 'week'){
            $tag = $tag->selectRaw('DATE_FORMAT(tag_user.created_at, \'%Y-%m-%d\') as date');
        }else{
            $tag = $tag->selectRaw('HOUR(tag_user.created_at) as hour');
        }

        $tag = $tag->selectRaw('COUNT(tag_user.id) as count')
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'tags.id')
        ->where('tags.taggable_id', $keyword->id)
        ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\Keyword')
        ->where('tags.data->isRelatedToKeyword', 1);

        if($request->type == 'week'){
            $tag = $tag->where('tag_user.created_at', '>=', $request->start)
            ->where('tag_user.created_at', '<', Carbon::createFromFormat('Y-m-d', $request->start)->addDays(7)->format('Y-m-d'));
        }else{
            $tag = $tag->whereDate('tag_user.created_at', $request->start);
        }

        $tag = $tag->groupBy($request->type == 'week' ? 'date' : 'hour')
        ->get()
        ->pluck('count', $request->type == 'week' ? 'date' : 'hour');

        if($request->type == 'week'){
            $carbon = Carbon::createFromFormat('Y-m-d', $request->start);

            if(!isset($tag[$request->start])) $tag->put($request->start, 0);
            for($i = 0; $i < 6; $i ++){
                $date = $carbon->addDay()->format('Y-m-d');
                if(!isset($tag[$date])) $tag->put($date, 0);
            }
        }else{
            for($i = 0; $i < 24; $i ++){
                if(!isset($tag[$i])) $tag->put($i, 0);
            }
        }

        return response()->json($tag->sortKeys()->values(), 200);
    }

    public function create(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $liffSizes = Liff::getLiffs();

        return view('solution._4.keywords.create')->with(compact('solution', 'deployment', 'liffSizes'));
    }

    public function getRichmenu(Request $request, Deployment $deployment)
    {
        $richmenu = Richmenu::select('id', 'chatBarText', 'size')
        ->where('parent_id', $request->parent_id)
        ->where('is_current_version', 1)
        ->with(['image' => function($q){
            $q->select('disk', 'path');
        }])
        ->first();

        if($richmenu)$richmenu->image[0]->url = $richmenu->image[0]->getUrl();
        return response()->json($richmenu, 200);
    }

    public function edit(Request $request, Deployment $deployment, $keywordId)
    {
        $keyword = Keyword::where('id', $keywordId)
        ->with(['template' => function($q){
            $q->select('id', 'name', 'parent_id');
        }])
        ->whereNull('deleted_all_at')
        ->first();
        if(!$keyword) abort(404);

        $solution = $request['solution'];
        $liffSizes = Liff::getLiffs();

        return view('solution._4.keywords.edit')->with(compact('solution', 'deployment', 'keyword', 'liffSizes'));
    }

    public function show(Request $request, Deployment $deployment, $keywordId)
    {
        $keyword = Keyword::find($keywordId);
        if(!$keyword) abort(404);

        $solution = $request['solution'];

        if($keyword->trigger_type == 'reply'){
            if($keyword->template_id){
                $keyword->load('template');
            }else{
                $keyword->messages_form_data = $keyword->refreshMessagesFromData();
            }
        }

        $keyword->total_users_count = User::count();
        $using_data = DB::connection('solution_4')
        ->table('tags')
        ->select('tags.id as tag_id')
        ->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as users_count')
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'tags.id')
        ->where('tags.taggable_id', $keyword->id)
        ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\Keyword')
        ->where('tags.data->isRelatedToKeyword', 1)
        ->first();

        $keyword->tag_id = $using_data->tag_id;
        $keyword->users_count = $using_data->users_count;
        if($keyword->trigger_type == 'reply' && !$keyword->template_id){
            $keyword->tagsData = $this->getTagsData($keyword->messages_form_data);
        }elseif($keyword->trigger_type == 'richmenu'){
            $keyword->richmenu = Richmenu::where('parent_id', $keyword->richmenu_id)
            ->orderBy('parent_id', 'desc')
            ->orderBy('is_current_version', 'desc')
            ->orderBy('deleted_at')
            ->orderBy('id', 'desc')
            ->first();
        }

        $keyword->versions = $keyword->getVersions();

        return view('solution._4.keywords.show')->with(compact('solution', 'deployment', 'keyword'));
    }

    /* ========== show 輔助函示 ========== */

    private function getTagsData($messages_form_data)
    {
        $tags = collect();

        function recurseImagemapArea(&$tags, $areas){
            foreach($areas as $area){
                if(isset($area->id)){
                    if(isset($area->tag)) {
                        $tags->push($area->tag);
                    }
                }else if(isset($area[0])){
                    recurseImagemapArea($tags, $area);
                }
            }
        };

        // 取出所有tag id
        foreach($messages_form_data as $message){
            switch($message->type){
                case 'imagemap':
                recurseImagemapArea($tags, $message->message->area->area);
                break;

                case 'buttons':
                case 'confirm':
                foreach($message->message->actions as $action){
                    if(isset($action->tag)) $tags->push($action->tag);
                }
                break;

                case 'carousel':
                case 'imagecarousel':
                case 'quickreply':
                $columnName = $message->type == 'quickreply' ? 'items' : 'columns';
                foreach($message->message->{$columnName}->{$columnName} as $column){
                    foreach($column->actions as $action){
                        if(isset($action->tag)) $tags->push($action->tag);
                    }
                }
                break;
            }
        }

        return DB::connection('solution_4')
        ->table('tags')
        ->select('tags.id')

        ->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as users_count')
        ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')

        ->leftJoin('tag_user as tag_user', 'tag_user.tag_id', '=', 'tags.id')

        ->whereIn('tags.id', $tags)
        ->groupBy('tags.id')
        ->get()
        ->keyBy('id');
    }


}
