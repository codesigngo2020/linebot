<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment\Basic;

use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Deployment;

trait ActiveableTrait
{

    public function toggleActive(Request $request, Deployment $deployment, $modelId)
    {
        $model = 'App\Models\Platform\Solution\_4\\'.($this->model ?? ucfirst($this->type));
        $model = $model::where('id', $modelId)
        ->where('is_current_version', 1)
        ->whereNull('deleted_all_at')
        ->first();
        if(!$model) abort(404);

        return response()->json(['success' => $model->toggleActive()], 200);
    }

}
