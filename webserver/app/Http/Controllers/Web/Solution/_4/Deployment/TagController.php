<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Tag;
use App\Models\Platform\Solution\_4\User;

// Service
use App\Services\Solution\_4\TagService;

// Request
use App\Http\Requests\Web\Solution\_4\Deployment\Tag\GetChartDataRequest;
use App\Http\Requests\Web\Solution\_4\Deployment\Tag\NameRequest;

class TagController extends Controller
{
    public $tagService;

    public function __construct(Request $request, TagService $tagService)
    {
        $this->tagService = $tagService;
    }

    public function index(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $data = $this->tagService->index($request, $solution, $deployment);

        $tags = $data['tags'];
        $paginator = $data['paginator'];

        if($tags->isEmpty()){
            Session::flash('alerts', [
                [
                    'class' => 'light',
                    'text' => '行為標籤命名後才會顯示在列表，並能夠在「好友群組」搜尋到。',
                ],
            ]);
        }

        return view('solution._4.tags.index')->with(compact('solution', 'deployment', 'tags', 'paginator'));
    }

    public function getTags(Request $request, Deployment $deployment)
    {
        $total_users_count = User::count();

        $lastPage = Tag::whereNotNull('tags.name');
        if($request->q) $lastPage = $this->query($lastPage, $request->q);
        $lastPage = ceil($lastPage->count() / 5);

        $page = intval($request->page);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $tags = Tag::select('tags.id', 'tags.name', 'tags.taggable_id', 'tags.taggable_type', 'tags.data')
        ->selectRaw('FLOOR(COUNT(DISTINCT(tag_user.user_id))*100/'.$total_users_count.') as users_percentage')
        ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')

        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'tags.id')

        ->whereNotNull('tags.name');

        if($request->q) $tags = $this->query($tags, $request->q);

        $tags = $tags->groupBy('tags.id')
        ->orderBy('tags.id', 'desc')
        ->offset(($page - 1) * 5)
        ->limit(5)
        ->get()
        ->each(function($tag) use($deployment){
            $tag->description = $tag->getDescription($deployment);
        });

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('tags', 'paginator'), 200);
    }

    public function show(Request $request, Deployment $deployment, $tagId)
    {
        $tag = Tag::find($tagId);
        if(!$tag) abort(404);

        $solution = $request['solution'];

        $tag->description = $tag->getDescription($deployment);

        $trigger_data = DB::connection('solution_4')
        ->table('tag_user')
        ->selectRaw('COUNT(DISTINCT(user_id)) as users_count')
        ->selectRaw('COUNT(DISTINCT(id)) as total_count')
        ->where('tag_id', $tag->id)
        ->first();

        $tag->total_users_count = User::count();
        $tag->users_count = $trigger_data->users_count;
        $tag->total_count = $trigger_data->total_count;

        return view('solution._4.tags.show')->with(compact('solution', 'deployment', 'tag'));
    }

    public function getChartData(GetChartDataRequest $request, Deployment $deployment, $tagId)
    {
        $tag = Tag::find($tagId);
        if(!$tag) abort(404);

        if($request->chartName == 'cumulativeUsersCountPercentageChart'){
            $chartData = $this->getCumulativeUsersCountPercentageChartData($request, $tag);
        }else{
            $chartData = $this->getCumulativeUsersCountChartData($request, $tag);
        }

        return response()->json($chartData, 200);
    }

    public function name(NameRequest $request, Deployment $deployment, $tagId)
    {
        $tag = Tag::find($tagId);
        if(!$tag) abort(404);

        $tag->name = $request->form['name']['name'];
        $tag->save();

        return response()->json(['status' => 'success', 'name' => $tag->name], 200);
    }

    /* ========== getTags 輔助函示 ========== */

    private function query($queryBuilder, $_q)
    {
        $likeQ = '%'.$_q.'%';
        return $queryBuilder->where(function($q) use($_q, $likeQ){
            $q->where('tags.id', $_q)
            ->orWhere('tags.name', 'like', $likeQ);
        });
    }

    /* ========== getChartData 輔助函示 ========== */

    private function getCumulativeUsersCountPercentageChartData(&$request, &$tag)
    {
        function queryChartData(&$tag, $datetime)
        {
            return DB::connection('solution_4')
            ->table('tag_user')
            ->selectRaw('COUNT(DISTINCT(user_id)) as users_count')
            ->where('tag_id', $tag->id)
            ->where('created_at', '<', $datetime)
            ->first()->users_count;
        }

        $usersCount = User::count();

        $chartData = [];
        if($request->type == 'week'){

            $carbon = Carbon::createFromFormat('Y-m-d', $request->start);
            $i = 0;
            do{
                $chartData[] = ($usersCount == 0 ? 0 : floor(queryChartData($tag, $carbon->addDay()->format('Y-m-d'))*100/$usersCount));
                $i ++;
            }while($i < 14);

        }else{

            $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $request->start.' 00:00:00');
            $i = 1;
            do{
                $chartData[] = ($usersCount == 0 ? 0 : floor(queryChartData($tag, $carbon->addHour()->format('Y-m-d H:i:s'))*100/$usersCount));
                $i ++;
            }while($i <= 24);

        }
        return $chartData;
    }

    private function getCumulativeUsersCountChartData(&$request, &$tag)
    {
        /* ========== 起始日期以前的總和 ========== */

        if($request->chartType == 'cumulativeUsersCount'){
            $start_count = DB::connection('solution_4')
            ->table('tag_user')
            ->where('tag_id', $tag->id)
            ->where('created_at', '<', $request->start)
            ->count();
        }

        /* ========== 當日觸發人次 ========== */

        $query = DB::connection('solution_4')
        ->table('tag_user')
        ->selectRaw('COUNT(tag_user.id) as count');

        if($request->type == 'week'){
            $query = $query->selectRaw('DATE_FORMAT(tag_user.created_at, \'%Y-%m-%d\') as date');
        }else{
            $query = $query->selectRaw('HOUR(tag_user.created_at) as hour');
        }

        if($request->type == 'week'){
            $query = $query->where('tag_user.created_at', '>=', $request->start)
            ->where('tag_user.created_at', '<', Carbon::createFromFormat('Y-m-d', $request->start)->addDays(14)->format('Y-m-d'));
        }else{
            $query = $query->whereDate('tag_user.created_at', $request->start);
        }

        $query = $query->where('tag_id', $tag->id)
        ->groupBy($request->type == 'week' ? 'date' : 'hour')
        ->get()
        ->pluck('count', $request->type == 'week' ? 'date' : 'hour');

        $chartData = [];

        if($request->type == 'week'){

            $carbon = Carbon::createFromFormat('Y-m-d', $request->start);
            $i = 0;
            do{
                if($request->chartType == 'cumulativeUsersCount'){
                    if(isset($query[$carbon->format('Y-m-d')])) $start_count += $query[$carbon->format('Y-m-d')];
                    $chartData[] = $start_count;
                }else{
                    $chartData[] = $query[$carbon->format('Y-m-d')] ?? 0;
                }

                $carbon->addDay();
                $i ++;
            }while($i < 14);

        }else{

            for($i = 0; $i < 24; $i ++){
                if($request->chartType == 'cumulativeUsersCount'){
                    if(isset($query[$i])) $start_count += $query[$i];
                    $chartData[] = $start_count;
                }else{
                    $chartData[] = $query[$i] ?? 0;
                }
            }

        }
        return $chartData;
    }


}
