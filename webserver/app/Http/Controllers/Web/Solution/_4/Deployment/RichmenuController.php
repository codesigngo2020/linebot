<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\Solution\_4\Image;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\User;
use App\Models\Platform\Solution\_4\Richmenu;
use App\Models\Platform\Solution\_4\Liff;

// Service
use App\Services\Solution\_4\RichmenuService;

// Request
use App\Http\Requests\Web\Solution\_4\Deployment\Richmenu\StoreRequest;
use App\Http\Requests\Web\Solution\_4\Deployment\Richmenu\NewVersionRequest;
use App\Http\Requests\Web\Solution\_4\Deployment\Richmenu\GetChartDataRequest;

class RichmenuController extends Controller
{
    public $richmenuService;

    public function __construct(RichmenuService $richmenuService)
    {
        $this->richmenuService = $richmenuService;
    }

    public function index(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $this->richmenuService->index($request, $solution, $deployment);

        return view('solution._4.richmenus.index')->with(compact('solution', 'deployment'));
    }

    public function getRichmenus(Request $request, Deployment $deployment)
    {
        $users_count = User::count();

        // 頁碼
        $lastPage = Richmenu::whereNull('deleted_all_at');
        if($request->q) $lastPage = $this->query($lastPage, $request->q);
        $lastPage = ceil($lastPage->distinct()->count('parent_id') / 5);

        $page = intval($request->page);
        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        // 取得 richmenus id
        $parentsId = DB::connection('solution_4')
        ->table('richmenus')
        ->select('parent_id');

        if($request->q) $parentsId = $this->query($parentsId, $request->q);

        $parentsId = $parentsId->whereNull('deleted_all_at')
        ->orderBy('parent_id', 'desc')
        ->offset(($page - 1) * 5)
        ->groupBy('parent_id')
        ->limit(5)
        ->pluck('parent_id');

        $richmenusId = DB::connection('solution_4')
        ->table('richmenus')
        ->select('id', 'parent_id')
        ->whereIn('parent_id', $parentsId)
        ->orderBy('parent_id', 'desc')
        ->orderBy('is_current_version', 'desc')
        ->orderBy('deleted_at')
        ->orderBy('id', 'desc')
        ->get()
        ->groupBy('parent_id')
        ->map(function($data){
            return $data[0]->id;
        });

        // 取得 richmenus 資料
        $richmenus = Richmenu::select('richmenus.id', 'richmenus.name', 'richmenus.description', 'richmenus.chatBarText', 'richmenus.size', 'richmenus.default', 'richmenus.parent_id', 'richmenus.version', 'richmenus.is_current_version', 'richmenus.deleted_at')
        ->selectRaw('FLOOR(COUNT(DISTINCT(users.id))*100/'.$users_count.') as users_percentage')
        ->selectRaw('COUNT(DISTINCT(tags.id)) as tags_count')

        ->join('tags', function($join){
            $join->on('tags.taggable_id', '=', 'richmenus.id')
            ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\Richmenu');
        })
        ->leftJoin('users', 'users.richmenu_id', '=', 'richmenus.id')

        ->whereIn('richmenus.id', $richmenusId);

        if($request->q) $richmenus = $this->query($richmenus, $request->q);

        $richmenus = $richmenus
        ->groupBy('richmenus.id')
        ->orderBy('richmenus.parent_id', 'desc')
        ->get()
        ->each(function($richmenu) use($users_count){
            $image = $richmenu->getImage();
            $richmenu->imageUrl = $image->getUrl();
            $richmenu->versions_count = $richmenu->getVersionsCount();
        });

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('richmenus', 'paginator'), 200);
    }

    public function create(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $liffSizes = Liff::getLiffs();
        return view('solution._4.richmenus.create')->with(compact('solution', 'deployment', 'liffSizes'));
    }

    public function store(StoreRequest $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $richmenu = $this->richmenuService->store($request, $solution, $deployment);

        $response = $richmenu ? ['status' => 'success', 'richmenuId' => $richmenu->id] : ['status' => 'error'];
        return response()->json($response, 200);
    }

    public function edit(Request $request, Deployment $deployment, $richmenuId)
    {
        $richmenu = Richmenu::where('id', $richmenuId)
        ->where('is_created_by_platform', 1)
        ->whereNull('deleted_all_at')
        ->with('image')
        ->first();
        if(!$richmenu) abort(404);

        $solution = $request['solution'];
        $liffSizes = Liff::getLiffs();

        $richmenu->selected = $richmenu->selected ? true : false;
        $richmenu->imageUrl = $richmenu->image[0]->getUrl();

        return view('solution._4.richmenus.edit')->with(compact('solution', 'deployment', 'richmenu', 'liffSizes'));
    }

    public function newVersion(NewVersionRequest $request, Deployment $deployment, $richmenuId)
    {
        $richmenu = Richmenu::where('id', $richmenuId)
        ->where('is_created_by_platform', 1)
        ->whereNull('deleted_all_at')
        ->with('image')
        ->first();
        if(!$richmenu) abort(404);

        $solution = $request['solution'];
        $richmenu = $this->richmenuService->newVersion($request, $solution, $deployment, $richmenu);

        $response = $richmenu ? ['status' => 'success', 'richmenuId' => $richmenu->id] : ['status' => 'error'];
        return response()->json($response, 200);
    }

    public function show(Request $request, Deployment $deployment, $richmenuId)
    {
        $richmenu = Richmenu::where('id', $richmenuId)
        ->with(['image' => function($q){
            $q->select('disk', 'path');
        }])
        ->withCount('users')
        ->first();
        if(!$richmenu) abort(404);

        $solution = $request['solution'];

        // if($richmenu->is_created_by_platform){
        $richmenu->areas_form_data = $richmenu->refreshAreasFromData();
        $richmenu->total_users_count = User::count();
        $using_data = DB::connection('solution_4')
        ->table('tags')
        ->select('tags.id as tag_id')
        ->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as cumulative_users_count')
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'tags.id')
        ->where('tags.taggable_id', $richmenu->id)
        ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\Richmenu')
        ->where('tags.data->isRelatedToRichmenu', 1)
        ->first();

        $richmenu->tag_id = $using_data->tag_id;
        $richmenu->cumulative_users_count = $using_data->cumulative_users_count;

        $richmenu->imageUrl = $richmenu->image[0]->getUrl();
        $richmenu->tagsData = $this->getTagsData($richmenu->id, $richmenu->areas_form_data);

        $richmenu->versions = $richmenu->getVersions();

        // }

        return view('solution._4.richmenus.show')->with(compact('solution', 'deployment', 'richmenu'));
    }

    public function delete(Request $request, Deployment $deployment, $richmenuId)
    {
        $richmenu = Richmenu::where('id', $richmenuId)->whereNull('deleted_all_at')->whereNull('deleted_at')->first();
        if(!$richmenu) abort(404);
        return response()->json(['success' => $richmenu->deleteCompletely($deployment->data->channelAccessToken)], 200);
    }

    public function deleteAll(Request $request, Deployment $deployment, $richmenuId)
    {
        $richmenu = Richmenu::where('id', $richmenuId)->where('is_current_version', 1)->whereNull('deleted_all_at')->first();
        if(!$richmenu) abort(404);
        $res = $richmenu->deleteAllCompletely($deployment->data->channelAccessToken);
        return response()->json(['success' => $res], 200);
    }

    public function toggelDefault(Request $request, Deployment $deployment, $richmenuId)
    {
        $richmenu = Richmenu::where('id', $richmenuId)->where('is_current_version', 1)->first();
        if(!$richmenu) abort(404);

        $res = $this->richmenuService->toggleDefault($request, $deployment, $richmenu);
        return response()->json(['success' => $res], 200);
    }

    public function cancelBinding(Request $request, Deployment $deployment, $richmenuId)
    {
        $richmenu = Richmenu::where('id', $richmenuId)->where('is_current_version', 1)->first();
        if(!$richmenu) abort(404);

        $res = $this->richmenuService->cancelBinding($request, $deployment, $richmenu);
        return response()->json(['success' => $res], 200);
    }

    public function switchVersion(Request $request, Deployment $deployment, $richmenuId)
    {
        $richmenu = Richmenu::where('id', $richmenuId)->where('is_current_version', 0)->whereNull('deleted_all_at')->whereNull('deleted_at')->first();
        if(!$richmenu) abort(404);

        $res = $this->richmenuService->switchVersion($request, $deployment, $richmenu);
        return response()->json(['success' => $res], 200);
    }

    public function getChartData(GetChartDataRequest $request, Deployment $deployment, $richmenuId)
    {
        $richmenu = Richmenu::find($richmenuId);
        if(!$richmenu) abort(404);

        $tagId = DB::connection('solution_4')
        ->table('tags')
        ->select('id')
        ->where('tags.taggable_id', $richmenu->id)
        ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\Richmenu')
        ->where('tags.data->isRelatedToRichmenu', 1)
        ->first()->id;

        /* ========== 起始日期以前的總和 ========== */

        $start_count = DB::connection('solution_4')
        ->table('tag_user')
        ->where('tag_id', $tagId)
        ->where('created_at', '<', $request->start)
        ->count();

        /* ========== 起始日開始的總和 ========== */

        $tag = DB::connection('solution_4')
        ->table('tag_user')
        ->selectRaw('COUNT(tag_user.id) as count');

        if($request->type == 'week'){
            $tag = $tag->selectRaw('DATE_FORMAT(tag_user.created_at, \'%Y-%m-%d\') as date');
        }else{
            $tag = $tag->selectRaw('HOUR(tag_user.created_at) as hour');
        }

        if($request->type == 'week'){
            $tag = $tag->where('tag_user.created_at', '>=', $request->start)
            ->where('tag_user.created_at', '<', Carbon::createFromFormat('Y-m-d', $request->start)->addDays(14)->format('Y-m-d'));
        }else{
            $tag = $tag->whereDate('tag_user.created_at', $request->start);
        }

        $tag = $tag->where('tag_id', $tagId)
        ->groupBy($request->type == 'week' ? 'date' : 'hour')
        ->get()
        ->pluck('count', $request->type == 'week' ? 'date' : 'hour');

        $cumulative_counts = [];
        if($request->type == 'week'){
            $carbon = Carbon::createFromFormat('Y-m-d', $request->start);

            if(isset($tag[$request->start])) $start_count += $tag[$request->start];
            $cumulative_counts[] = $start_count;

            for($i = 0; $i < 13; $i ++){
                $date = $carbon->addDay()->format('Y-m-d');
                if(isset($tag[$date])) $start_count += $tag[$date];
                $cumulative_counts[] = $start_count;
            }
        }else{
            for($i = 0; $i < 24; $i ++){
                if(isset($tag[$i])) $start_count += $tag[$i];
                $cumulative_counts[] = $start_count;
            }
        }

        return response()->json($cumulative_counts, 200);
    }

    public function queryForSelect(Request $request, Deployment $deployment)
    {
        $richmenus = Richmenu::select('id', 'parent_id as value', 'name')
        ->where('is_current_version', 1);

        if($request->q) $richmenus = $richmenus->where('name', 'like', '%'.$request->q.'%');

        $richmenus = $richmenus->orderBy('parent_id', 'desc')->limit(11)->distinct()->get();

        return response()->json($richmenus, 200);
    }

    /* ========== getRichmenus 輔助函示 ========== */

    private function query($queryBuilder, $_q)
    {
        $likeQ = '%'.$_q.'%';
        return $queryBuilder->where(function($q) use($_q, $likeQ){
            $q->where('richmenus.parent_id', $_q)
            ->orWhere('richmenus.name', 'like', $likeQ)
            ->orWhere('richmenus.description', 'like', $likeQ);
        });
    }

    /* ========== show 輔助函示 ========== */

    private function getTagsData($richmenuId, $areas_form_data)
    {
        $tags = collect();

        function recurseArea(&$tags, $areas){
            foreach($areas as $area){
                if(isset($area->id)){
                    if(isset($area->tag)) $tags->push($area->tag);
                }else if(isset($area[0])){
                    recurseArea($tags, $area);
                }
            }
        };

        recurseArea($tags, $areas_form_data);

        return DB::connection('solution_4')
        ->table('tags')
        ->select('tags.id')

        ->selectRaw('COUNT(DISTINCT(tag_user_1.user_id)) as users_count')
        ->selectRaw('COUNT(DISTINCT(tag_user_1.id)) as total_count')

        ->selectRaw('COUNT(DISTINCT(tag_user_2.user_id)) as cumulative_users_count')
        ->selectRaw('COUNT(DISTINCT(tag_user_2.id)) as cumulative_total_count')

        ->leftJoin('tag_user as tag_user_1', function($join) use($richmenuId){
            $join->on('tag_user_1.tag_id', '=', 'tags.id')
            ->join('users', 'users.id', '=', 'tag_user_1.user_id')
            ->where('users.richmenu_id', $richmenuId);
        })
        ->leftJoin('tag_user as tag_user_2', 'tag_user_2.tag_id', '=', 'tags.id')

        ->whereIn('tags.id', $tags)
        ->groupBy('tags.id')
        ->get()
        ->keyBy('id');
    }
}
