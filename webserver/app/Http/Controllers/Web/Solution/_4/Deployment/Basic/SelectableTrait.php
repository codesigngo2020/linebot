<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment\Basic;

use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Deployment;

trait SelectableTrait
{

    public function queryForSelect(Request $request, Deployment $deployment)
    {
        $model = 'App\Models\Platform\Solution\_4\\'.($this->model ?? ucfirst($this->type));
        $models = $model::select('id', 'parent_id as value', 'name')
        ->where('is_current_version', 1);

        if($request->q) $models = $models->where('name', 'like', '%'.$request->q.'%');

        $models = $models->orderBy('parent_id', 'desc')->limit(11)->distinct()->get();

        return response()->json($models, 200);
    }

}
