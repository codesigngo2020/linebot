<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Category;

// Request
use App\Http\Requests\Web\Solution\_4\Deployment\Category\StoreRequest;
use App\Http\Requests\Web\Solution\_4\Deployment\Category\NameRequest;
use App\Http\Requests\Web\Solution\_4\Deployment\Category\DeleteRequest;

class CategoryController extends Controller
{

    public function index(Request $request, Deployment $deployment)
    {
        $categories = Category::select('id', 'name', 'parent_id', 'start', 'end')
        ->where('categoryable_type', 'App\Models\Platform\Solution\_4\\'.$request->type)
        ->orderBy('order')
        ->orderBy('start')
        ->get();

        return response()->json(compact('categories'), 200);
    }

    public function classify(Request $request, Deployment $deployment)
    {
        $model = 'App\Models\Platform\Solution\_4\\'.$request->categoryable_type;
        $model = $model::find($request->categoryable_id);
        if(!$model) abort(404);

        if($shouldVersion = $model->shouldVersion()){
            $models = $model->select('id')->where('parent_id', $model->parent_id)->get('id');
        }


        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{
            if($request->category_id > 0){
                $category = Category::find($request->category_id);
                if(!$category) abort(404);

                if($shouldVersion){
                    $models->each(function($model) use($category){
                        $model->category()->sync($category);
                    });
                    $res = true;
                }else{
                    $res = $model->category()->sync($category);
                }
            }else if($request->category_id == -1){
                if($shouldVersion){
                    $models->each(function($model){
                        $model->category()->detach();
                    });
                    $res = true;
                }else{
                    $res = $model->category()->detach();
                }
            }else{
                $res = false;
            }

            $solution4Connection->commit();

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();
            $res = false;
        }
        return response()->json(['success' => $res ? true : false], 200);
    }

    public function store(StoreRequest $request, Deployment $deployment)
    {
        $form = $request->createForm;
        $category = $form['category']['category'] == -1 ? NULL : Category::find($form['category']['category']);

        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{

            if($form['type']['type'] == 'insert'){
                if(!$category || $category->start == 1){
                    $queryBuilder = DB::connection('solution_4')
                    ->table('categories')
                    ->where('categoryable_type', 'App\Models\Platform\Solution\_4\\'.$request->type);

                    if($category) $queryBuilder = $queryBuilder->where('order', '>', $category->order);

                    $queryBuilder->update([
                        'order' => DB::raw('`order` + 1'),
                    ]);

                    $category = Category::create([
                        'name' => $form['name']['name'],
                        'categoryable_type' => 'App\Models\Platform\Solution\_4\\'.$request->type,
                        'parent_id' => 0,
                        'start' => 1,
                        'end' => 2,
                        'order' => $category ? $category->order + 1 : 1,
                    ]);

                    $category->parent_id = $category->id;
                    $category->save();

                }else{
                    DB::connection('solution_4')
                    ->table('categories')
                    ->where('parent_id', $category->parent_id)
                    ->where('start', '>', $category->end)
                    ->update([
                        'start' => DB::raw('`start` + 2'),
                        'end' => DB::raw('`end` + 2'),
                    ]);

                    DB::connection('solution_4')
                    ->table('categories')
                    ->where('parent_id', $category->parent_id)
                    ->where('start', '<', $category->start)
                    ->where('end', '>', $category->end)
                    ->update([
                        'end' => DB::raw('`end` + 2'),
                    ]);

                    Category::create([
                        'name' => $form['name']['name'],
                        'categoryable_type' => $category->categoryable_type,
                        'parent_id' => $category->parent_id,
                        'start' => $category->end + 1,
                        'end' => $category->end + 2,
                        'order' => $category->order,
                    ]);
                }
            }elseif($form['type']['type'] == 'child'){
                DB::connection('solution_4')
                ->table('categories')
                ->where('parent_id', $category->parent_id)
                ->where('start', '>', $category->end)
                ->update([
                    'start' => DB::raw('`start` + 2'),
                    'end' => DB::raw('`end` + 2'),
                ]);

                DB::connection('solution_4')
                ->table('categories')
                ->where('parent_id', $category->parent_id)
                ->where('start', '<=', $category->start)
                ->where('end', '>=', $category->end)
                ->update([
                    'end' => DB::raw('`end` + 2'),
                ]);

                Category::create([
                    'name' => $form['name']['name'],
                    'categoryable_type' => $category->categoryable_type,
                    'parent_id' => $category->parent_id,
                    'start' => $category->end,
                    'end' => $category->end + 1,
                    'order' => $category->order,
                ]);
            }

            $solution4Connection->commit();
            $res = true;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();
            $res = false;
        }

        return response()->json(['success' => $res], 200);


    }

    public function name(NameRequest $request, Deployment $deployment, $categoryId)
    {
        $category = Category::find($categoryId);
        if(!$category) abort(404);

        $category->name = $request->editForm['name']['name'];
        $category->save();

        return response()->json(['success' => true, 'name' => $category->name], 200);
    }

    public function delete(DeleteRequest $request, Deployment $deployment, $categoryId)
    {
        $category = Category::find($categoryId);
        if(!$category) abort(404);

        $transferCategory = $request->removeForm['category']['value']['value'];

        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{

            $queryBuilder = DB::connection('solution_4')
            ->table('categoryables');

            if($category->end - $category->start == 1){
                $queryBuilder = $queryBuilder->where('category_id', $category->id);
            }else{
                $categories = DB::connection('solution_4')
                ->table('categories')
                ->select('id')
                ->where('parent_id', $category->parent_id)
                ->where('start', '>=', $category->start)
                ->where('end', '<=', $category->end)
                ->pluck('id');
                $queryBuilder = $queryBuilder->whereIn('category_id', $categories);
            }

            if($transferCategory == -1){
                $queryBuilder->delete();
            }else{
                $queryBuilder->update([
                    'category_id' => $transferCategory,
                ]);
            }

            // 刪除自己以及子類別
            DB::connection('solution_4')
            ->table('categories')
            ->where('parent_id', $category->parent_id)
            ->where('start', '>=', $category->start)
            ->where('end', '<=', $category->end)
            ->delete();

            if($category->start == 1){
                DB::connection('solution_4')
                ->table('categories')
                ->where('order', '>', $category->order)
                ->update([
                    'order' => DB::raw('`order` - 1'),
                ]);
            }else{
                $diff = $category->end - $category->start + 1;

                DB::connection('solution_4')
                ->table('categories')
                ->where('parent_id', $category->parent_id)
                ->where('start', '>', $category->end)
                ->update([
                    'start' => DB::raw('`start` - '.$diff),
                    'end' => DB::raw('`end` - '.$diff),
                ]);

                DB::connection('solution_4')
                ->table('categories')
                ->where('parent_id', $category->parent_id)
                ->where('start', '<', $category->end)
                ->where('end', '>', $category->end)
                ->update([
                    'end' => DB::raw('`end` - '.$diff),
                ]);
            }

            $solution4Connection->commit();
            $res = true;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();
            $res = false;
        }

        return response()->json(['success' => $res ? true : false], 200);
    }

    public function queryForSelect(Request $request, Deployment $deployment)
    {
        $categories = DB::connection('solution_4')
        ->table('categories')
        ->select('id as value', 'name')
        ->where('categoryable_type', 'App\Models\Platform\Solution\_4\\'.$request->categoryable_type);

        if($request->q) $categories = $categories->where('name', 'like', '%'.$request->q.'%');

        if($request->exclude){
            $excludeCategory = Category::find($request->exclude);
            if($excludeCategory->end - $excludeCategory->start == 1){
                $categories = $categories->where('id', '<>', $excludeCategory->id);
            }else{
                $excludeCategories = DB::connection('solution_4')
                ->table('categories')
                ->select('id')
                ->where('parent_id', $excludeCategory->parent_id)
                ->where('start', '>=', $excludeCategory->start)
                ->where('end', '<=', $excludeCategory->end)
                ->pluck('id');
                $categories = $categories->whereNotIn('id', $excludeCategories);
            }
        }

        $categories = $categories->limit(21)->get();

        return response()->json($categories, 200);
    }

}
