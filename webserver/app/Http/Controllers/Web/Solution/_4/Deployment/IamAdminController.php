<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use Hash;
use Mail;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\User;
use App\Models\Platform\Solution\Deployment;

// Request
use App\Http\Requests\Web\Solution\_4\Deployment\IamAdmin\AssignRequest;
use App\Http\Requests\Web\Solution\_4\Deployment\IamAdmin\InviteRequest;

class IamAdminController extends Controller
{

    public function index(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];

        $users = $deployment->users()
        ->select('id', 'name', 'email')
        ->with(['roles' => function($q) use($deployment){
            $q->select('id as value', 'display_name as name')->where('deployment_id', $deployment->id);
        }])
        ->limit(10)
        ->get();

        $paginator = collect([
            'currentPage' => 1,
            'lastPage' => ceil($deployment->users()->count() / 10),
        ]);

        $roles = Role::select('id as value', 'display_name as name')
        ->where('deployment_id', $deployment->id)
        ->get();

        return view('solution._4.iam-admin.index')->with(compact('solution', 'deployment', 'users', 'paginator', 'roles'));
    }

    public function getUsers(Request $request, Deployment $deployment)
    {
        $lastPage = ceil($deployment->users()->count() / 10);
        $page = intval($request->page);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $users = $deployment->users()
        ->select('id', 'name', 'email')
        ->with(['roles' => function($q) use($deployment){
            $q->select('id as value', 'display_name as name')->where('deployment_id', $deployment->id);
        }])
        ->offset(($page - 1) * 10)
        ->limit(10)
        ->get();

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('users', 'paginator'), 200);
    }

    public function assign(AssignRequest $request, Deployment $deployment)
    {
        $user = User::find($request->editForm['user']['id']);

        foreach($request->editForm['roles']['value'] as $role){
            $user->assignRole($role['value']);
        }

        return response()->json(['status' => 'success'], 200);
    }

    public function invite(InviteRequest $request, Deployment $deployment)
    {
        $user = User::where('email', $request->addUserForm['account']['account'])->first();
        $roles = collect($request->addUserForm['roles']['value'])->pluck('value')->sort();

        $user->invitationCodes()->where('deployment_id', $deployment->id)->delete();
        $secret = Str::random(32);
        $hashedCode = Hash::make($secret.$user->id.$deployment->id.$roles->implode(','));

        $user->invitationCodes()->create([
            'user_id' => $user->id,
            'deployment_id' => $deployment->id,
            'roles' => $roles->values(),
            'code' => $hashedCode,
        ]);


        Mail::send('components/mails/solution/_4/invite', compact('deployment', 'user', 'secret'), function($message) use($deployment, $user){
            $message->to($user->email, $user->name)->subject('【部署成員邀請】'.auth()->user()->name.'邀請您共同管理「'.$deployment->name.'」部署。');
        });

        return response()->json(['status' => (count(Mail::failures()) == 0) ? 'success' : 'error'], 200);
    }

    public function delete(Request $request, Deployment $deployment)
    {
        $user = $deployment->users()->where('id', $request->userId)->first();

        $superadminRole = Role::where('name', 'deployment_'.$deployment->id.'_superadmin')->first();

        if($user->hasRole('deployment_'.$deployment->id.'_superadmin') && $superadminRole->users()->count() === 1){
            $response = ['status' => 'error'];
        }else{
            Role::select('id')
            ->where('deployment_id', $deployment->id)
            ->get()
            ->each(function($role) use($user){
                $user->removeRole($role);
            });

            $deployment->users()->detach($user);

            $response = ['status' => 'success'];
        }

        return response()->json($response, 200);
    }

}
