<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Liff;

// Service
use App\Services\Solution\_4\LiffService;

class LiffController extends Controller
{
    public $liffService;

    public function __construct(LiffService $liffService)
    {
        $this->liffService = $liffService;
    }

    public function index(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $data = $this->liffService->index($request, $solution, $deployment);

        $liffs = $data['liffs'];
        $paginator = $data['paginator'];

        Session::flash('alerts', [
            [
                'class' => 'light',
                'text' => '若您不小心從其他途徑刪除了由平台建立的 Liff App，僅需重整此畫面，便會自動為您新增回去。',
            ],
        ]);

        return view('solution._4.liffs.index')->with(compact('solution', 'deployment', 'liffs', 'paginator'));
    }

    public function getLiffs(Request $request, Deployment $deployment)
    {
        $page = intval($request->page);
        $lastPage = ceil(Liff::count() / 6);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $liffs = Liff::select('id', 'liff_id', 'description', 'view', 'features', 'is_created_by_platform')
        ->offset(($page - 1) * 6)
        ->limit(6)
        ->get();

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('liffs', 'paginator'), 200);
    }

    public function delete(Request $request, Deployment $deployment, $liffId)
    {
        $liff = Liff::find($liffId);
        if(!$liff) abort(404);

        $res = $liff->deleteCompletely($deployment->data->channelAccessToken);


        $response = ['status' => $res ? 'success' : 'error'];
        return response()->json($response, 200);
    }

}
