<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment\Basic;

use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Deployment;

trait ControllerTrait
{

    public function index(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        return view('solution._4.'.($this->view ?? $this->type.'s').'.index')->with(compact('solution', 'deployment'));
    }

    public function store(\StoreRequest $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $model = $this->{$this->type.'Service'}->store($request, $solution, $deployment);

        $response = $model ? ['status' => 'success', ($this->model ?? $this->type).'Id' => $model->id] : ['status' => 'error'];
        return response()->json($response, 200);
    }

}
