<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use DB;
use Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\Solution\_4\Image;
use App\Models\Platform\Solution\Deployment;

class ChatroomController extends Controller
{

    public function index(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];

        $users = DB::connection('solution_4')
        ->table('users')
        ->select('users.id', 'users.display_name', 'users.picture_url', 'users.status_message', 'users.is_follow')
        ->selectRaw('"" as reply_token')
        ->limit(21)
        ->get();

        $users = collect([
            'hasNext' => $users->count() > 20,
            'users' => $users->take(20),
        ]);

        $latestDialogsUser = DB::connection('solution_4')
        ->table('logs')
        ->selectRaw('MAX(logs.id) as id')
        ->whereNotNull('user_id')
        ->groupBy('user_id')
        ->orderBy('id', 'desc')
        ->limit(21);

        $dialogs = DB::connection('solution_4')
        ->table('logs')
        ->select('users.id', 'users.display_name', 'users.picture_url', 'users.is_follow', 'logs.id as last_log_id', 'logs.messages', 'logs.timestamp')
        ->selectRaw('"" as reply_token')

        ->joinSub($latestDialogsUser, 'latest_dialogs_user', function($join){
            $join->on('latest_dialogs_user.id', '=', 'logs.id');
        })
        ->join('users', function($join){
            $join->on('users.id', '=', 'logs.user_id');
        })

        ->groupBy('users.id')
        ->orderBy('logs.id', 'desc')
        ->limit(21)
        ->get()
        ->each(function($dialog){
            $dialog->timestamp = Carbon::parse($dialog->timestamp)->format('m/d');
        });

        $dialogs = collect([
            'hasNext' => $dialogs->count() > 20,
            'dialogs' => $dialogs->take(20),
        ]);


        return view('solution._4.chatroom.index')->with(compact('solution', 'deployment', 'users', 'dialogs'));
    }

    public function imageUpload(Request $request, Deployment $deployment)
    {
        $images = [];
        foreach($request['form']['images'] as $image){
            // 上傳圖片
            $disk = Storage::disk('gcs');
            $path = $disk->put('deployments/'.$deployment->id.'/chatroom', $image);

            $image = Image::create([
                'name' => 'LINE聊天室圖片',
                'disk' => 'gcs',
                'path' => $path,
            ]);

            $images[] = $disk->url($path);
        }

        return response()->json(['status' => 'success', 'images' => $images], 200);
    }

}
