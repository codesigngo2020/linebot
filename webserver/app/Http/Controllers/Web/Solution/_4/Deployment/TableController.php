<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Trait
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\CategoryableTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\ControllerTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\QueryableTrait;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Table;

// Service
use App\Services\Solution\_4\TableService;

// Request
use App\Http\Requests\Web\Solution\_4\Deployment\Table\RedeemRequest;

class TableController extends Controller
{

    use CategoryableTrait, ControllerTrait, QueryableTrait;

    protected $solution = 4;
    protected $type = 'table';
    protected $tableService;

    public function __construct(TableService $tableService)
    {
        $this->tableService = $tableService;
        parent::__construct();
    }

    public function create(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        return view('solution._4.tables.create')->with(compact('solution', 'deployment'));
    }

    public function getTables(Request $request, Deployment $deployment)
    {
        // 頁碼
        $lastPage = Table::whereNull('tables.deleted_at');
        if($request->q) $lastPage = $this->query($lastPage, $request->q);
        if($request->category != 0) $this->fileterByCategory($lastPage, $request->category);
        $lastPage = ceil($lastPage->count() / 5);

        $page = intval($request->page);
        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        // 取得 tables 資料
        $tables = Table::select('tables.id', 'tables.name', 'tables.description', 'tables.type', 'tables.table_name')
        ->whereNull('tables.deleted_at');

        if($request->q) $tables = $this->query($tables, $request->q);
        if($request->category != 0) $this->fileterByCategory($tables, $request->category);

        $tables = $tables->orderBy('tables.id', 'desc')
        ->offset(($page - 1) * 5)
        ->limit(5)
        ->get()
        ->each(function($table){
            $table->dataCounts = $table->getDataCounts();
        });

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('tables', 'paginator'), 200);
    }

    public function show(Request $request, Deployment $deployment, $tableId)
    {
        $table = Table::where('id', $tableId)->whereNull('deleted_at')->first();
        if(!$table) abort(404);

        $solution = $request['solution'];
        $table->dataCounts = $table->getDataCounts();
        $table->columnList = $table->getColumnList();

        return view('solution._4.tables.show')->with(compact('solution', 'deployment', 'table'));
    }

    public function getRows(Request $request, Deployment $deployment, $tableId)
    {
        $table = Table::where('id', $tableId)->whereNull('deleted_at')->first();
        if(!$table) abort(404);

        $table->columnList = $table->getColumnList();

        // 頁碼
        $lastPage = DB::connection('solution_4')
        ->table($table->table_name);
        if($request->q) $lastPage = $this->queryRows($lastPage, $table->columnList, $request->q);
        $lastPage = ceil($lastPage->count() / 10);

        $page = intval($request->page);
        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        // 取得 rows 資料
        $rows = DB::connection('solution_4')
        ->table($table->table_name);

        if($request->q) $tables = $this->queryRows($rows, $table->columnList, $request->q);

        $rows = $rows->orderBy('id')
        ->offset(($page - 1) * 10)
        ->limit(10)
        ->get();

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('rows', 'paginator'), 200);
    }

    public function delete(Request $request, Deployment $deployment, $tableId)
    {
        $table = Table::where('id', $tableId)->whereNull('deleted_at')->first();
        if(!$table) abort(404);

        return response()->json(['success' => $table->deleteCompletely()], 200);
    }

    public function redeem(Request $request, Deployment $deployment, $tableId)
    {
        $solution = $request['solution'];
        $res = $this->tableService->redeemCoupon($request, $tableId);
        return view('solution._4.tables.redeem')->with(compact('solution', 'deployment', 'res'));
    }

    public function queryForSelect(Request $request, Deployment $deployment)
    {
        $tables = Table::select('id as value', 'name');

        if($request->fields) $tables = $tables->selectRaw($request->fields);
        if($request->type) $tables = $tables->where('type', $request->type);
        if($request->q) $tables = $tables->where('name', 'like', '%'.$request->q.'%');

        $tables = $tables->orderBy('id', 'desc')->limit(11)->distinct()->get();
        return response()->json($tables, 200);
    }

    /* ========== getRows 輔助函示 ========== */

    private function queryRows($queryBuilder, $columnList, $_q)
    {
        $likeQ = '%'.$_q.'%';

        $queryBuilder = $queryBuilder->where('id', $_q);
        foreach($columnList as $column){
            if($column == 'id') continue;
            $queryBuilder = $queryBuilder->orWhere($column, 'like', $likeQ);
        }

        return $queryBuilder;
    }

}
