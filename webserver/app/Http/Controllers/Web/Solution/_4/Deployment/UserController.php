<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\User;
use App\Models\Platform\Solution\_4\Tag;

// Service
use App\Services\Solution\_4\UserService;

class UserController extends Controller
{

    public $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $data = $this->userService->index($request, $solution, $deployment);

        $users = $data['users'];
        $paginator = $data['paginator'];

        return view('solution._4.users.index')->with(compact('solution', 'deployment', 'users', 'paginator'));
    }

    public function getUsers(Request $request, Deployment $deployment)
    {
        $lastPage = User::query();
        if($request->q) $lastPage = $this->query($lastPage, $request->q);
        $lastPage = ceil($lastPage->count() / 5);

        $page = intval($request->page);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $users = DB::connection('solution_4')
        ->table('users')
        ->select('users.id', 'users.display_name', 'users.user_id', 'users.picture_url', 'users.status_message', 'users.is_follow');

        if($request->q) $users = $this->query($users, $request->q);

        $users = $users->orderBy('users.id', 'desc')
        ->offset(($page - 1) * 5)
        ->limit(5)
        ->get()
        ->each(function($user){
            $user->groups = DB::connection('solution_4')
            ->table('groups')
            ->select('groups.id', 'groups.name')
            ->join('group_user', function($join) use($user){
                $join->on('group_user.group_id', '=', 'groups.id')
                ->where('group_user.user_id', $user->id)
                ->whereNull('group_user.deleted_at');
            })
            ->distinct()
            ->get();
        });

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('users', 'paginator'), 200);
    }

    public function show(Request $request, Deployment $deployment, $userId)
    {
        $user = User::find($userId);
        if(!$user) abort(404);

        $solution = $request['solution'];

        // 好友群組
        $latestGroupUser = DB::connection('solution_4')
        ->table('group_user')
        ->select('group_user.group_id')
        ->selectRaw('MAX(group_user.id) as id')
        ->where('group_user.user_id', $user->id)
        ->groupBy('group_user.group_id');

        $user->groups = DB::connection('solution_4')
        ->table('groups')
        ->select('groups.id', 'groups.name', 'group_user.created_at', 'group_user.deleted_at')
        ->joinSub($latestGroupUser, 'latest_group_user', function($join){
            $join->on('latest_group_user.group_id', '=', 'groups.id');
        })
        ->join('group_user', function($join) use($user){
            $join->on('group_user.id', '=', 'latest_group_user.id');
        })
        ->orderBy('group_user.deleted_at')
        ->get();

        // 行為標籤
        $user->tags = DB::connection('solution_4')
        ->table('tags')
        ->select('tags.id', 'tags.name')
        ->join('tag_user', function($join) use($user){
            $join->on('tag_user.tag_id', '=', 'tags.id')
            ->where('tag_user.user_id', $user->id);
        })
        ->whereNotNull('tags.name')
        ->distinct()
        ->get();

        // 行為歷程
        $history = DB::connection('solution_4')
        ->table('tag_user')
        ->where('tag_user.user_id', $user->id)
        ->orderBy('tag_user.id', 'desc')
        ->limit(11)
        ->get();

        $user->historyHasNext = $history->count() > 10;
        $user->history = $history->take(10);

        $user->historyTags = Tag::select('tags.id', 'tags.taggable_id', 'tags.taggable_type', 'tags.data')
        ->whereIn('tags.id', $user->history->pluck('tag_id')->unique())
        ->get()
        ->keyBy('id')
        ->each(function($tag) use($deployment){
            $tag->historyDescription = $tag->getHistoryDescription($deployment);
        });

        return view('solution._4.users.show')->with(compact('solution', 'deployment', 'user'));
    }

    public function getHistory(Request $request, Deployment $deployment, $userId)
    {
        $user = User::find($userId);
        if(!$user) abort(404);

        $solution = $request['solution'];

        $history = DB::connection('solution_4')
        ->table('tag_user')
        ->where('tag_user.user_id', $user->id)
        ->where('tag_user.id', '<', $request->before)
        ->orderBy('tag_user.id', 'desc')
        ->limit(11)
        ->get();

        $hasNext = $history->count() > 10;
        $history = $history->take(10);

        $historyTags = Tag::select('tags.id', 'tags.taggable_id', 'tags.taggable_type', 'tags.data')
        ->whereIn('tags.id', $history->pluck('tag_id')->unique())
        ->get()
        ->keyBy('id')
        ->each(function($tag) use($deployment){
            $tag->historyDescription = $tag->getHistoryDescription($deployment);
        });

        return response()->json(compact('history', 'hasNext', 'historyTags'), 200);
    }

    public function queryForSelect(Request $request, Deployment $deployment)
    {
        $users = User::select('id as value', 'display_name as name');

        if($request->q) $users = $users->where('display_name', 'like', '%'.$request->q.'%');
        if($request->follow) $users = $users->where('is_follow', $request->follow ? 1 : 0);

        $users = $users->limit(11)
        ->get();

        return response()->json($users, 200);
    }

    /* ========== getUsers 輔助函示 ========== */

    private function query($queryBuilder, $q)
    {
        $likeQ = '%'.$q.'%';
        return $queryBuilder->where('users.id', $q)
        ->orWhere('users.display_name', 'like', $likeQ)
        ->orWhere('users.user_id', 'like', $likeQ)
        ->orWhere('users.status_message', 'like', $likeQ);
    }

}
