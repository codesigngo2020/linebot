<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Trait
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\CategoryableTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\ControllerTrait;
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\QueryableTrait;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\User;
use App\Models\Platform\Solution\_4\Broadcast;
use App\Models\Platform\Solution\_4\Liff;
use App\Models\Platform\Solution\_4\Message;
use App\Models\Platform\Solution\_4\Script;
use App\Models\Platform\Solution\_4\Group;

// Service
use App\Services\Solution\_4\BroadcastService;

class BroadcastController extends Controller
{

    use CategoryableTrait, ControllerTrait, QueryableTrait;

    protected $solution = 4;
    protected $type = 'broadcast';
    protected $versionable = false;
    protected $broadcastService;

    public function __construct(BroadcastService $broadcastService)
    {
        $this->broadcastService = $broadcastService;
        parent::__construct();
    }

    public function getBroadcastCalendar(Request $request, Deployment $deployment)
    {
        $broadcasts = Broadcast::select('id', 'name', 'appointed_at', 'send_status')
        ->selectRaw('DATE_FORMAT(appointed_at, \'%Y-%c-%e\') as date')
        ->whereDate('appointed_at', '>=', $request->start)
        ->whereDate('appointed_at', '<=', $request->end)
        ->orderBy('appointed_at')
        ->get();

        $broadcasts = $broadcasts->groupBy('date');

        return response()->json(compact('broadcasts'), 200);
    }

    public function getBroadcastList(Request $request, Deployment $deployment)
    {
        $users_count = User::count();

        $lastPage = Broadcast::query();
        if($request->q) $lastPage = $this->query($lastPage, $request->q);
        if($request->category != 0) $this->fileterByCategory($lastPage, $request->category);
        $lastPage = ceil($lastPage->count() / 5);

        $page = intval($request->page);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $broadcasts = DB::connection('solution_4')
        ->table('broadcasts')
        ->select('broadcasts.id', 'broadcasts.name', 'broadcasts.description', 'broadcasts.messages', 'broadcasts.template_id', 'broadcasts.appointed_at', 'broadcasts.target_type', 'broadcasts.send_status')
        ->selectRaw(($request->category == 0 || $request->category == -1 ? $request->category : 'categoryables.category_id').' as category_id')
        ->selectRaw('FLOOR(COUNT(DISTINCT(tag_user.user_id))*100/'.$users_count.') as users_percentage')
        ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')
        ->selectRaw('COUNT(DISTINCT(tags.id)) as tags_count');

        if($request->category != 0) $this->fileterByCategory($broadcasts, $request->category);

        $broadcasts = $broadcasts->join('tags', function($join){
            $join->on('tags.taggable_id', '=', 'broadcasts.id')
            ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\Broadcast');
        })
        ->join('tags as broadcast_tag', function($join){
            $join->on('broadcast_tag.taggable_id', '=', 'broadcasts.id')
            ->where('broadcast_tag.taggable_type', 'App\Models\Platform\Solution\_4\Broadcast')
            ->where('broadcast_tag.data->isRelatedToBroadcast', 1)
            ->limit(1);
        })
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'broadcast_tag.id');
        // ->leftJoin('users', 'users.id', '=', 'tag_user.user_id');

        if($request->q) $broadcasts = $this->query($broadcasts, $request->q);

        $broadcasts = $broadcasts->groupBy('broadcasts.id')
        ->orderBy('broadcasts.id', 'desc')
        ->offset(($page - 1) * 5)
        ->limit(5)
        ->get()
        ->each(function($broadcast){
            if($broadcast->template_id){
                $broadcast->messages = [
                    [
                        'type' => '訊息模組',
                        'hasQuickReplay' => false,
                    ],
                ];
            }else{
                $broadcast->messages = array_map(function($message){
                    if($message->type == 'template'){
                        return [
                            'type' => $message->template->type == 'image_carousel' ? 'imagecarousel' : $message->template->type,
                            'hasQuickReplay' => isset($message->quickReply),
                        ];
                    }else{
                        return [
                            'type' => $message->type,
                            'hasQuickReplay' => isset($message->quickReply),
                        ];
                    }
                }, json_decode($broadcast->messages));
            }
        });

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('broadcasts', 'paginator'), 200);
    }

    public function create(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];
        $liffSizes = Liff::getLiffs();
        return view('solution._4.broadcasts.create')->with(compact('solution', 'deployment', 'liffSizes'));
    }

    public function show(Request $request, Deployment $deployment, $broadcastId)
    {
        $broadcast = Broadcast::find($broadcastId);
        if(!$broadcast) abort(404);

        $solution = $request['solution'];

        if(!$broadcast->template_id){
            $broadcast->messages_form_data = $broadcast->refreshMessagesFromData();
        }

        $broadcast->total_users_count = User::count();
        $using_data = DB::connection('solution_4')
        ->table('tags')
        ->select('tags.id as tag_id')
        ->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as users_count')
        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'tags.id')
        ->where('tags.taggable_id', $broadcast->id)
        ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\Broadcast')
        ->where('tags.data->isRelatedToBroadcast', 1)
        ->first();

        $broadcast->tag_id = $using_data->tag_id;
        $broadcast->users_count = $using_data->users_count;

        if(!$broadcast->template_id){
            $broadcast->tagsData = $this->getTagsData($broadcast->messages_form_data);
        }

        $now = Carbon::now()->format('Y-m-d H:i:s');
        $broadcast->deleteable = (!$broadcast->sent_at && $broadcast->send_status == 'N' && $broadcast->appointed_at > $now && $broadcast->appointed_at->diffInMinutes($now) > 5);

        Session::flash('alerts', [
            [
                'class' => 'light',
                'text' => $this->getBroadcastStatusMessage($broadcast->appointed_at, $broadcast->send_status, $broadcast->sent_at)
            ],
        ]);
        return view('solution._4.broadcasts.show')->with(compact('solution', 'deployment', 'broadcast'));
    }

    public function delete(Request $request, Deployment $deployment, $broadcastId)
    {
        $broadcast = Broadcast::find($broadcastId);
        if(!$broadcast) abort(404);

        $now = Carbon::now();
        if(!$broadcast->sent_at && $broadcast->send_status == 'N' && $broadcast->appointed_at > $now && $broadcast->appointed_at->diffInMinutes($now) > 5){
            $res = $broadcast->deleteCompletely();
            $response = ['status' => $res ? 'success' : 'error'];
        }else{
            $response = ['status' => 'error'];
        }
        return response()->json($response, 200);
    }

    private function getBroadcastStatusMessage($appointed_at, $send_status, $sent_at)
    {
        $text = '。';

        switch($send_status){
            case 'S':
            $text = '，已於<span class="badge badge-soft-dark ml-2 mr-2">'.$sent_at.'</span>開始發送。';
            break;

            case 'F':
            $text = '，已於<span class="badge badge-soft-dark ml-2 mr-2">'.$sent_at.'</span>發送完成。';
            break;

            case 'P':
            $text = '，已於<span class="badge badge-soft-dark ml-2 mr-2">'.$sent_at.'</span>開始發送，部分成功，部分失敗。';
            break;

            case 'E':
            $text = '，已於<span class="badge badge-soft-dark ml-2 mr-2">'.$sent_at.'</span>開始發送，發送失敗。';
            break;
        }

        return '推播訊息預計於<span class="badge badge-soft-dark ml-2 mr-2">'.$appointed_at.'</span>發送'.$text;
    }

    /* ========== show 輔助函示 ========== */

    private function getTagsData($messages_form_data)
    {
        $tags = collect();

        function recurseImagemapArea(&$tags, $areas){
            foreach($areas as $area){
                if(isset($area->id)){
                    if(isset($area->tag)) {
                        $tags->push($area->tag);
                    }
                }else if(isset($area[0])){
                    recurseImagemapArea($tags, $area);
                }
            }
        };

        // 取出所有tag id
        foreach($messages_form_data as $message){
            switch($message->type){
                case 'imagemap':
                recurseImagemapArea($tags, $message->message->area->area);
                break;

                case 'buttons':
                case 'confirm':
                foreach($message->message->actions as $action){
                    if(isset($action->tag)) $tags->push($action->tag);
                }
                break;

                case 'carousel':
                case 'imagecarousel':
                case 'quickreply':
                $columnName = $message->type == 'quickreply' ? 'items' : 'columns';
                foreach($message->message->{$columnName}->{$columnName} as $column){
                    foreach($column->actions as $action){
                        if(isset($action->tag)) $tags->push($action->tag);
                    }
                }
                break;
            }
        }

        return DB::connection('solution_4')
        ->table('tags')
        ->select('tags.id')

        ->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as users_count')
        ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')

        ->leftJoin('tag_user as tag_user', 'tag_user.tag_id', '=', 'tags.id')

        ->whereIn('tags.id', $tags)
        ->groupBy('tags.id')
        ->get()
        ->keyBy('id');
    }

}
