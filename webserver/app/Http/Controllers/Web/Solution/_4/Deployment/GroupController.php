<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Trait
use App\Http\Controllers\Web\Solution\_4\Deployment\Basic\ControllerTrait;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\User;
use App\Models\Platform\Solution\_4\Group;
use App\Models\Platform\Solution\_4\Tag;

// Service
use App\Services\Solution\_4\GroupService;

// Helper
use App\Helpers\Solution\_4\Group\GroupHelper;

class GroupController extends Controller
{

    use ControllerTrait;

    protected $solution = 4;
    protected $type = 'group';
    protected $groupService;

    public function __construct(GroupService $groupService)
    {
        $this->groupService = $groupService;
        parent::__construct();
    }

    public function getGroups(Request $request, Deployment $deployment)
    {
        $total_users_count = User::count();

        $lastPage = Group::query();
        if($request->q) $lastPage = $this->query($lastPage, $request->q);
        $lastPage = ceil($lastPage->count() / 5);

        $page = intval($request->page);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $groups = DB::connection('solution_4')
        ->table('groups')
        ->select('groups.id', 'groups.name', 'groups.description', 'groups.updated_at')
        ->selectRaw('FLOOR(COUNT(DISTINCT(group_user.user_id))*100/'.$total_users_count.') as users_percentage')

        ->leftJoin('group_user', function($join){
            $join->on('group_user.group_id', '=', 'groups.id')
            ->whereNull('group_user.deleted_at');
        })

        ->groupBy('groups.id')
        ->orderBy('groups.id', 'desc');

        if($request->q) $groups = $this->query($groups, $request->q);

        $groups = $groups->orderBy($request->column, $request->dir)
        ->offset(($page - 1) * 5)
        ->limit(5)
        ->get();

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('groups', 'paginator'), 200);
    }

    public function create(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];

        return view('solution._4.groups.create')->with(compact('solution', 'deployment'));
    }

    public function getTags(Request $request, Deployment $deployment)
    {
        $lastPage = Tag::whereNotNull('name');
        if($request->q) $lastPage = $lastPage->where('name', 'like', '%'.$request->q.'%');
        $lastPage = ceil($lastPage->count() / 5);

        $page = intval($request->page);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $tags = Tag::select('id', 'name', 'taggable_id', 'taggable_type', 'data')
        ->whereNotNull('name');

        if($request->q) $tags = $tags->where('name', 'like', '%'.$request->q.'%');

        $tags = $tags->orderBy('id', 'desc')
        ->offset(($page - 1) * 5)
        ->limit(5)
        ->get()
        ->each(function($tag) use($deployment){
            $tag->description = $tag->getDescription($deployment);
        });

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('tags', 'paginator'), 200);
    }

    public function getUsers(Request $request, Deployment $deployment)
    {
        if($request->q){
            $users = DB::connection('solution_4')
            ->table('users')
            ->select('id as value', 'display_name as name')
            ->where('display_name', 'like', '%'.$request->q.'%')
            ->limit(51)
            ->get();
        }else{
            $users = [];
        }

        return response()->json($users, 200);
    }

    public function show(Request $request, Deployment $deployment, $groupId)
    {
        $group = Group::find($groupId);
        if(!$group) abort(404);

        $solution = $request['solution'];

        // 取得條件tags
        if($group->circles){
            $tags = collect();
            foreach($group->circles as $circle){
                $tags = $tags->merge($circle->tagsId);
            }

            $group->tags = DB::connection('solution_4')
            ->table('tags')
            ->select('id', 'name')
            ->whereIn('id', $tags->unique())
            ->get()
            ->keyBy('id');
        }else{
            $group->tags = [];
        }

        $group->total_users_count = User::count();
        $group->users_count = DB::connection('solution_4')
        ->table('group_user')
        ->selectRaw('COUNT(DISTINCT(group_user.user_id)) as users_count')
        ->where('group_user.group_id', $group->id)
        ->whereNull('group_user.deleted_at')
        ->first()->users_count;

        if($group->add_users_id){
            $group->addUsers = DB::connection('solution_4')
            ->table('users')
            ->select('users.id', 'users.display_name', 'users.picture_url', 'users.status_message')
            ->whereIn('users.id', $group->add_users_id)
            ->get();
        }else{
            $group->addUsers = [];
        }

        if($group->remove_users_id){
            $group->removeUsers = DB::connection('solution_4')
            ->table('users')
            ->select('users.id', 'users.display_name', 'users.picture_url', 'users.status_message')
            ->whereIn('users.id', $group->remove_users_id)
            ->get();
        }else{
            $group->removeUsers = [];
        }

        $group->users = DB::connection('solution_4')
        ->table('users')
        ->select('users.id', 'users.display_name', 'users.picture_url', 'users.status_message')
        ->join('group_user', function($join) use($group){
            $join->on('group_user.user_id', '=', 'users.id')
            ->where('group_user.group_id', $group->id)
            ->whereNull('group_user.deleted_at');
        })
        ->orderBy('users.id')
        ->limit(15)
        ->get();

        $group->paginator = collect([
            'currentPage' => 1,
            'lastPage' => ceil($group->users_count / 15),
        ]);

        return view('solution._4.groups.show')->with(compact('solution', 'deployment', 'group'));
    }

    public function sync(Request $request, Deployment $deployment, $groupId)
    {
        $group = Group::find($groupId);
        if(!$group) abort(404);

        // 好友群組關聯
        $groupHelper = new GroupHelper;
        $groupHelper->setDeployment($deployment)->setGroup($group)->update();

        return response()->json(['status' => 'success'], 200);
    }

    public function getGroupUsers(Request $request, Deployment $deployment, $groupId)
    {
        $group = Group::find($groupId);
        if(!$group) abort(404);

        $lastPage = DB::connection('solution_4')
        ->table('users')
        ->selectRaw('COUNT(DISTINCT(users.id)) as users_count')
        ->join('group_user', function($join) use($group){
            $join->on('group_user.user_id', '=', 'users.id')
            ->where('group_user.group_id', $group->id)
            ->whereNull('group_user.deleted_at');
        });

        if($request->q) $lastPage = $this->queryUsers($lastPage, $request->q);
        $lastPage = ceil($lastPage->first()->users_count / 15);

        $page = intval($request->page);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $users = DB::connection('solution_4')
        ->table('users')
        ->select('users.id', 'users.display_name', 'users.picture_url', 'users.status_message')
        ->join('group_user', function($join) use($group){
            $join->on('group_user.user_id', '=', 'users.id')
            ->where('group_user.group_id', $group->id)
            ->whereNull('group_user.deleted_at');
        });

        if($request->q) $users = $this->queryUsers($users, $request->q);

        $users = $users->orderBy('users.id')
        ->offset(($page - 1) * 15)
        ->limit(15)
        ->get();

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return response()->json(compact('users', 'paginator'), 200);
    }

    public function queryForSelect(Request $request, Deployment $deployment)
    {
        $groups = Group::select('id as value', 'name');

        if($request->q) $groups = $groups->where('name', 'like', '%'.$request->q.'%');

        $groups = $groups->limit(51)
        ->get();

        return response()->json($groups, 200);
    }

    /* ========== getGroups 輔助函示 ========== */

    private function query($queryBuilder, $q)
    {
        $likeQ = '%'.$q.'%';
        return $queryBuilder->where('groups.id', $q)
        ->orWhere('groups.name', 'like', $likeQ)
        ->orWhere('groups.description', 'like', $likeQ);
    }

    /* ========== getGroupUsers 輔助函示 ========== */

    private function queryUsers($queryBuilder, $q)
    {
        $likeQ = '%'.$q.'%';
        return $queryBuilder->where('users.id', $q)
        ->orWhere('users.display_name', 'like', $likeQ)
        ->orWhere('users.status_message', 'like', $likeQ);
    }

}
