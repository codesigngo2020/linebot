<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment\Basic;

trait QueryableTrait
{

    private function query($queryBuilder, $_q)
    {
        $table = ($this->table ?? $this->type.'s');
        $likeQ = '%'.$_q.'%';

        if($this->versionable ?? false){
            return $queryBuilder->where(function($q) use($table, $_q, $likeQ){
                $q->where($table.'.parent_id', $_q)
                ->orWhere($table.'.name', 'like', $likeQ)
                ->orWhere($table.'.description', 'like', $likeQ);
            });
        }else{
            return $queryBuilder->where($table.'.id', $_q)
            ->orWhere($table.'.name', 'like', $likeQ)
            ->orWhere($table.'.description', 'like', $likeQ);
        }
    }

}
