<?php

namespace App\Http\Controllers\Web\Solution\_4\Deployment\Basic;

use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Deployment;

trait VersionableTrait
{

    public function newVersion(\NewVersionRequest $request, Deployment $deployment, $modelId)
    {
        $model = 'App\Models\Platform\Solution\_4\\'.($this->model ?? ucfirst($this->type));
        $model = $model::where('id', $modelId)
        ->whereNull('deleted_all_at')
        ->first();
        if(!$model) abort(404);

        $solution = $request['solution'];
        $model = $this->{$this->type.'Service'}->newVersion($request, $solution, $deployment, $model);

        $response = $model ? ['status' => 'success', ($this->model ?? $this->type).'Id' => $model->id] : ['status' => 'error'];
        return response()->json($response, 200);
    }

    public function switchVersion(Request $request, Deployment $deployment, $modelId)
    {
        $model = 'App\Models\Platform\Solution\_4\\'.($this->model ?? ucfirst($this->type));
        $model = $model::where('id', $modelId)
        ->where('is_current_version', 0)
        ->whereNull('deleted_all_at')
        ->first();
        if(!$model) abort(404);

        $res = $this->{$this->type.'Service'}->switchVersion($request, $deployment, $model);
        return response()->json(['success' => $res], 200);
    }

    public function deleteAll(Request $request, Deployment $deployment, $modelId)
    {
        $model = 'App\Models\Platform\Solution\_4\\'.($this->model ?? ucfirst($this->type));
        $model = $model::where('id', $modelId)
        ->where('is_current_version', 1)
        ->whereNull('deleted_all_at')
        ->first();
        if(!$model) abort(404);

        return response()->json(['success' => $model->deleteAllCompletely()], 200);
    }

}
