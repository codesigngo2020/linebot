<?php

namespace App\Http\Controllers\Web\Solution\_4;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Statistics;

//Services
use App\Services\Solution\_4\DeploymentServices\GetStatisticsService;
use App\Services\Solution\_4\DeploymentServices\GetBroadcastsService;
use App\Services\Solution\_4\DeploymentServices\GetUsersChartDataService;
use App\Services\Solution\_4\DeploymentServices\GetBroadcastsAndAutoAnswerService;
use App\Services\Solution\_4\DeploymentServices\GetKeywordsAndscriptsChartDataService;

// Helper
use App\Helpers\Solution\_4\Statistics\StatisticsHelper;

class DeploymentController extends Controller
{

    public function show(Request $request, Deployment $deployment)
    {
        $solution = $request['solution'];

        $service = new GetStatisticsService;
        $statistics = $service->getStatistics($deployment);

        $service = new GetBroadcastsAndAutoAnswerService;
        $broadcasts = $service->getBroadcasts($deployment);
        $autoAnswers = $service->getAutoAnswers($deployment);

        return view('solution._4.show')->with(compact('solution', 'deployment', 'statistics', 'broadcasts', 'autoAnswers'));
    }

    public function getChartData(Request $request, Deployment $deployment)
    {
        switch($request->chartName){
            case 'usersChart':
            $chartService = new GetUsersChartDataService;
            $chartData = $chartService->getUsersChartData($request);
            break;

            case 'activeUsersChart':
            $chartService = new GetUsersChartDataService;
            $chartData = $chartService->getActiveUsersChartData($request);
            break;

            case 'keywordsChart':
            $chartService = new GetKeywordsAndscriptsChartDataService;
            $chartData = $chartService->getChartData($request, 'keyword');
            break;

            case 'scriptsChart':
            $chartService = new GetKeywordsAndscriptsChartDataService;
            $chartData = $chartService->getChartData($request, 'script');
            break;
        }

        return response()->json($chartData, 200);
    }
}
