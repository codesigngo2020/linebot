<?php

namespace App\Http\Controllers\Web\Solution\_2;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Request
use App\Http\Requests\Web\Solution\_2\StoreRequest;
use App\Http\Requests\Web\Solution\_2\TestDomainRequest;
use App\Http\Requests\Web\Solution\_2\AuthorizeRequest;

// Service
use App\Services\Solution\_2\SolutionService;
use App\Services\Solution\_2\WordprssService;
use App\Services\Solution\_2\InstagramService;


class SolutionController extends Controller
{
  public function store(StoreRequest $request)
  {

    $solution = $request['solution'];
    $this->solutionService = new SolutionService;
    $data = $this->solutionService->store($request, $solution);

    $response = [
      'status' => $data['deployment'] ? 'success' : 'error',
      'deployment' => $data['deployment']->id,
      'approvalUrl' => $data['approvalUrl'],
    ];
    return response()->json($response, 200);
  }

  public function testDomain(TestDomainRequest $request)
  {
    $wordprssService = new WordprssService;
    $status = $wordprssService->associate($request->domain, $request->key);
    $status = $status ? 'associated' : 'error';

    return response()->json($status, 200);
  }

  // instagram 授權
  #public function _authorize(AuthorizeRequest $request)
  #{
  #  // client_id 和 client_secret 寫入 session
  #  Session::forget('solution2');
  #  Session::put('solution2', [
  #    'clientId' => $request->clientId,
  #    'clientSecret' => $request->clientSecret,
  #    'accessToken' => '',
  #  ]);

  #  $query = collect([
  #    'client_id='.$request->clientId,
  #    'redirect_uri='.route('solution.2.callback'),
  #    'response_type=code',
  #  ]);
  #  return redirect()->away('https://www.instagram.com/oauth/authorize/?'.$query->implode('&'));
  #}

  // instagram 授權後重新導向
  #public function callback(Request $request)
  #{
  #  $this->instagramService = new InstagramService;
  #  $access_token = $this->instagramService->setAccessTokenToSession($request->code);

  #  Session::forget('solution2');
  #  return view('components.forms.solution._2.deploy-partials.callback')->with(compact('access_token'));
  #}

}
