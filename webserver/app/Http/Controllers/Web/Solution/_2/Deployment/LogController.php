<?php

namespace App\Http\Controllers\Web\Solution\_2\Deployment;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\Solution\Deployment;

class LogController extends Controller
{

  public function getLogs(Request $request, Deployment $deployment)
  {
    $lastPage = $deployment->solution_2_logs();
    if($request->q) $lastPage = $this->query($lastPage, $request->q);
    $lastPage = floor($lastPage->count() / 10) + 1;

    $page = intval($request->page);
    if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

    $logs = $deployment->solution_2_logs()
    ->select('id', 'type', 'class', 'message', 'created_at');

    if($request->q) $logs = $this->query($logs, $request->q);

    $logs = $logs->orderBy($request->column, $request->dir)
    ->offset(($page - 1) * 10)
    ->limit(10)
    ->get();

    $paginator = [
      'currentPage' => $page,
      'lastPage' => $lastPage,
    ];

    return response()->json(compact('logs', 'paginator'), 200);
  }

  public function query($queryBuilder, $q)
  {
    $q = '%'.$q.'%';
    return $queryBuilder->where('id', 'like', $q)
    ->orWhere('message', 'like', $q)
    ->orWhere('created_at', 'like', $q);
  }

}
