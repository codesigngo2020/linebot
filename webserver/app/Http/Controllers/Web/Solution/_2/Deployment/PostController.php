<?php

namespace App\Http\Controllers\Web\Solution\_2\Deployment;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_2\Crawler;

class PostController extends Controller
{

  public function getPosts(Request $request, Deployment $deployment, Crawler $crawler)
  {
    $lastPage = $crawler->posts();
    if($request->q) $lastPage = $this->query($lastPage, $request->q);
    $lastPage = floor($lastPage->count() / 10) + 1;

    $page = intval($request->page);
    if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

    $posts = $crawler->posts()
    ->select('id', 'crawler_id', 'post_id', 'shortcode', 'data->content as content', 'posted_at', 'status');

    if($request->q) $posts = $this->query($posts, $request->q);


    $posts = $posts->orderBy($request->column, $request->dir);
    if($request->column != 'id') $posts = $posts->orderBy('id', 'desc');
    $posts = $posts->offset(($page - 1) * 10)
    ->limit(10)
    ->get();

    //dd($posts);

    $paginator = [
      'currentPage' => $page,
      'lastPage' => $lastPage,
    ];

    return response()->json(compact('posts', 'paginator'), 200);
  }

  public function action(Request $request, Deployment $deployment)
  {
    switch($request->action){
      case 'repost':
      $data = [
        'posts.post_id' => NULL,
        'posts.posted_at' => NULL,
        'posts.status' => 'standBy',
      ];
      break;

      case 'ignore':
      $data = [
        'posts.status' => 'ignore',
      ];
      break;

    }
    if($data){
      $deployment->solution_2_posts()->whereIn('posts.id', $request->posts)->update($data);
    }
    return response()->json(['success' => true], 200);
  }


  /* ========== getPosts 輔助函示 ========== */

  public function query($queryBuilder, $q)
  {
    $q = '%'.$q.'%';
    return $queryBuilder->where('id', 'like', $q)
    ->orWhere('post_id', 'like', $q)
    ->orWhere('shortcode', 'like', $q)
    ->orWhere('data->content', 'like', $q)
    ->orWhere('posted_at', 'like', $q);
  }



}
