<?php

namespace App\Http\Controllers\Web\Solution\_2\Deployment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_2\Crawler;
use App\Models\Platform\Solution\_2\Account;

// Request
use App\Http\Requests\Web\Solution\_2\Deployment\Crawler\ActiveRequest;
use App\Http\Requests\Web\Solution\_2\Deployment\Crawler\UpdateRequest;



class CrawlerController extends Controller
{
  public function update(UpdateRequest $request, Deployment $deployment)
  {
    $originalCrawlers = $deployment->solution_2_crawlers()->where('status', '<>', 'destroyed')->get();

    // 更新crawler
    $crawlers = collect($request->crawlers)->each(function($data) use($deployment, &$originalCrawlers){
      $crawler = $deployment->solution_2_crawlers()
      ->where('id', $data['id'])
      ->first();

      $account = Account::firstOrCreate([
        'username' => $data['crawler']['username']['username'],
      ]);
      $duration = explode(' - ', $data['crawler']['duration']['duration']);

      $data = [
        'type' => $data['crawler_type']['value']['value'],
        'account_id' => $account->id,
        'format' => $data['crawler']['post']['format']['content'],
        'categories' => $data['crawler']['post']['category']['value'],
        'tags' => $data['crawler']['post']['tag']['value'],
        'frequency' => $data['crawler']['frequency']['value'],
        'posts_limit' => $data['crawler']['postsLimit']['value'],
        'options' => $data['crawler']['post']['options']['value'],
        'start' => str_replace(' ', '', $duration[0]),
        'end' => str_replace(' ', '', $duration[1]),
        'active' => 1,
        'status' => 'standBy',
      ];

      if($crawler){
        if($data['account_id'] != $crawler->account->id) $crawler->posts()->delete();
        $crawler->update($data);
      }else{
        $crawler = $deployment->solution_2_crawlers()->create($data);
      }

      $originalCrawlers = $originalCrawlers->diff([$crawler]);
    });

    $originalCrawlers->each(function($crawler){
      $crawler->status = 'destroyed';
      $crawler->save();
      $crawler->posts()->delete();
    });

    return response()->json(['success' => true], 200);
  }


  public function active(ActiveRequest $request, Deployment $deployment, Crawler $crawler)
  {
    $crawler->active = $request->active;
    $crawler->save();

    return response()->json($crawler->active, 200);
  }

}
