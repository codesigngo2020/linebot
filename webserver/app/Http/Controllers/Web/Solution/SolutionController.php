<?php

namespace App\Http\Controllers\Web\Solution;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\Platform\Solution\Solution;

// Service
use App\Services\Solution\_1\SolutionService as SolutionService_1;
use App\Services\Solution\_2\SolutionService as SolutionService_2;
use App\Services\Solution\_3\SolutionService as SolutionService_3;
use App\Services\Solution\_4\SolutionService as SolutionService_4;

class SolutionController extends Controller
{

    public function index()
    {
        $solutions = Solution::with('icon')->get();
        return view('solution.index')->with('solutions', $solutions);
    }

    public function detail(Solution $solution)
    {
        $user = auth()->user();
        $deployments_count = !$user ? 0 : $user->deployments()->where('solution_id', $solution->id)->count();

        return view('solution.detail')->with(compact('solution', 'deployments_count'));
    }

    public function deployments(Solution $solution)
    {
        $this->setSolutionService($solution);
        $data = $this->solutionService->index($solution);

        $deployments = $data['deployments'];
        $paginator = $data['paginator'];

        return view('solution.deplyments')->with(compact('solution', 'deployments', 'paginator'));
    }

    public function getDeployments(Request $request, Solution $solution)
    {
        $this->setSolutionService($solution);
        $data = $this->solutionService->getDeployments($request, $solution);

        $deployments = $data['deployments'];
        $paginator = $data['paginator'];

        return response()->json(compact('deployments', 'paginator'), 200);
    }

    public function deploy(Solution $solution)
    {
        $deploymentWithFreePlan = auth()->user()->deployments()
        ->where('solution_id', $solution->id)
        ->whereHas('plan', function($q){
            $q->where('price', 0);
        })
        ->first();

        $plans = $solution->plans;
        if($deploymentWithFreePlan) $plans->each(function($plan){
            $plan->disabled = $plan->price == 0;
        });

        return view('solution.deploy')->with(compact('solution', 'plans'));
    }



    // 設置solution service
    public function setSolutionService($solution)
    {
        switch($solution->id){
            case 1:
            $this->solutionService = new SolutionService_1;
            break;

            case 2:
            $this->solutionService = new SolutionService_2;
            break;

            case 3:
            $this->solutionService = new SolutionService_3;
            break;

            case 4:
            $this->solutionService = new SolutionService_4;
            break;
        }
    }
}
