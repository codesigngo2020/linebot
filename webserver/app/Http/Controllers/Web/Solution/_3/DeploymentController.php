<?php

namespace App\Http\Controllers\Web\Solution\_3;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Request

// Model
use App\Models\Platform\Solution\Deployment;

// Repository

// Service
use App\Services\Paypal\PaypalService;

// Helper
use App\Helpers\Basic\Paypal\AgreementHelper;

class DeploymentController extends Controller
{

  public function __construct()
  {
    // $this->middleware(function($request, $next){
    //   if(!$request->deployment->paypal_agreement_id) return $next($request);
    //   abort(404);
    //
    // })->only('updatePaymentLink');

  }

  public function show(Request $request, Deployment $deployment)
  {
    $solution = $request['solution'];
    // $this->crawlerRepository = new CrawlerRepository;
    // $crawlers = $this->crawlerRepository->findAllByDeploymentWithAccountAndUncrawelAndAllPostsCount($deployment);
    //
    // $this->logRepository = new LogRepository;
    // $posts_count = $crawlers->mapWithKeys(function($crawler){
    //   return ['#'.$crawler->id => $this->logRepository->countPostsInSpecificDays($crawler, 7)->pluck('count', 'date')];
    // });
    // $requests_count = $crawlers->mapWithKeys(function($crawler){
    //   return ['#'.$crawler->id => $this->logRepository->countRequestsInSpecificDays($crawler, 7)->pluck('count', 'date')];
    // });
    //
    // $mainChartData = [];
    // for($i = 6; $i >= 0; $i --){
    //   $date = Carbon::today()->subDays($i)->format('m/d');
    //   $mainChartData['date'][] = $date;
    //
    //   foreach($posts_count as $account => $countData){
    //     $mainChartData['posts_count'][$account][] = isset($countData[$date]) ? $countData[$date] : 0;
    //   }
    //
    //   foreach($requests_count as $account => $countData){
    //     $mainChartData['requests_count'][$account][] = isset($countData[$date]) ? $countData[$date] : 0;
    //   }
    // }

    return view('solution._3.show')->with(compact('solution', 'deployment'));
  }

  // public function settings(Request $request, Deployment $deployment)
  // {
  //   $solution = $request['solution'];
  //   $plan = $deployment->plan;
  //
  //   if($deployment->paypal_agreement_id){
  //     $agreementHelper = new AgreementHelper;
  //     $transactions = $agreementHelper->transactions($deployment->paypal_agreement_id);
  //
  //     $transactions = collect($transactions->agreement_transaction_list)->filter(function($transaction){
  //       return $transaction->status == 'Completed';
  //     })
  //     ->sortByDesc('time_stamp')
  //     ->values()
  //     ->map(function($transaction, $index){
  //       return [
  //         'id' => $index + 1,
  //         'amount' => '$'.$transaction->amount->value.' USD',
  //         'date' => Carbon::parse($transaction->time_stamp)->toDateTimeString(),
  //       ];
  //     });
  //   }else{
  //     $transactions = NUll;
  //   }
  //
  //   return view('solution._2.settings')->with(compact('solution', 'deployment', 'plan', 'transactions'));
  // }
  //
  // public function updateSettings(updateSettingsRequest $request, Deployment $deployment)
  // {
  //   $deployment->name = $request->form['name']['name'];
  //   $deployment->save();
  //
  //   return response()->json(['success' => true], 200);
  // }
  //
  // public function updatePaymentLink(Request $request, Deployment $deployment)
  // {
  //   $solution = $request['solution'];
  //
  //   $paypalService = new PaypalService;
  //   $token = $paypalService->createAgreementAndGetToken($solution, $deployment->plan);
  //
  //   $deployment->paypal_agreement_token = $token;
  //   $deployment->save();
  //
  //   return response()->json([
  //     'success' => true,
  //     'approvalUrl' => 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$token,
  //   ], 200);
  // }
  //
  // public function wordpress(Request $request, Deployment $deployment)
  // {
  //   $solution = $request['solution'];
  //   $order = $deployment->solution_2_crawlers()
  //   ->select('id', 'account_id')
  //   ->where('status', '<>', 'destroyed')
  //   ->get()
  //   ->map(function($crawler, $index){
  //     return [
  //       'id' => $crawler->id,
  //       'name' => '@'.$crawler->account->username,
  //     ];
  //   })
  //   ->sortBy(function($crawler) use($deployment){
  //     return array_search($crawler['id'], $deployment->data->post->order);
  //   })
  //   ->values();
  //
  //   return view('solution._2.wordpress')->with(compact('solution', 'deployment', 'order'));
  // }
  //
  // public function updateWordpress(updateWordpressRequest $request, Deployment $deployment)
  // {
  //   $post_duration = explode(' - ', $request->form[2]['duration']['duration']);
  //   $result = $deployment->update([
  //     'data' => [
  //       'domain' => $request->form[1]['domain']['domain'],
  //       'key' => $request->form[1]['key']['key'],
  //       'post' => [
  //         'start' => str_replace(' ', '', $post_duration[0]),
  //         'end' => str_replace(' ', '', $post_duration[1]),
  //         'frequency' => $request->form[2]['postFrequency']['value'],
  //         'order' => collect($request->form[2]['order']['order'])->pluck('id'),
  //       ],
  //     ],
  //   ]);
  //
  //   return response()->json(['success' => $result], 200);
  // }
  //
  // public function crawlers(Request $request, Deployment $deployment)
  // {
  //   $solution = $request['solution'];
  //   $crawlers = $deployment->solution_2_crawlers()
  //   ->where('status', '<>', 'destroyed')
  //   ->with(['account' => function($q){
  //     $q->select('id', 'username');
  //   }])
  //   ->get();
  //   return view('solution._2.crawlers')->with(compact('solution', 'deployment', 'crawlers'));
  // }
  //
  // public function posts(Request $request, Deployment $deployment)
  // {
  //   $solution = $request['solution'];
  //   $this->crawlerRepository = new CrawlerRepository;
  //   $crawlers = $deployment->solution_2_crawlers()
  //   ->select('id', 'deployment_id', 'type', 'account_id')
  //   ->where('status', '<>', 'destroyed')
  //   ->with(['account' => function($q){
  //     $q->select('id', 'username');
  //   }])
  //   ->withCount('posts')
  //   ->get()
  //   ->map(function($crawler){
  //     $crawler->posts = $crawler->posts()
  //     ->select('id', 'crawler_id', 'post_id', 'shortcode', 'data->content as content', 'posted_at', 'status')
  //     ->orderBy('id', 'desc')
  //     ->offset(0)
  //     ->limit(10)
  //     ->get();
  //
  //     $crawler->paginator = [
  //       'currentPage' => 1,
  //       'lastPage' => floor($crawler->posts_count / 10) + 1,
  //     ];
  //     return $crawler;
  //   });
  //
  //   return view('solution._2.posts')->with(compact('solution', 'deployment', 'crawlers'));
  // }
  //
  // public function logs(Request $request, Deployment $deployment)
  // {
  //   $solution = $request['solution'];
  //   $logs = $deployment->solution_2_logs()
  //   ->select('id', 'type', 'message', 'created_at')
  //   ->orderBy('id', 'desc')
  //   ->offset(0)
  //   ->limit(10)
  //   ->get();
  //
  //   $paginator = collect([
  //     'currentPage' => 1,
  //     'lastPage' => floor($deployment->solution_2_logs()->count() / 10) + 1,
  //   ]);
  //
  //   return view('solution._2.logs')->with(compact('solution', 'deployment', 'logs', 'paginator'));
  // }
  //
  // public function reactive(Request $request, Deployment $deployment)
  // {
  //   $deployment->status = 'active';
  //   $deployment->save();
  //   return redirect()->back();
  // }
  //
  // public function destroy(Request $request, Deployment $deployment)
  // {
  //   if($deployment->destroyed_at){
  //
  //     $agreementHelper = new AgreementHelper;
  //     $res = $agreementHelper->suspend($deployment->paypal_agreement_id);
  //
  //     if($res){
  //       $deployment->solution_2_posts()->delete();
  //       $deployment->solution_2_crawlers()->delete();
  //       $deployment->solution_2_logs()->delete();
  //       $deployment->delete();
  //     }else{
  //       return redirect()->back();
  //     }
  //
  //     return redirect()->route('solution.deployments', $deployment->solution_id);
  //   }else{
  //     $deployment->status = 'destroyed';
  //     $deployment->destroyed_at = date('Y-m-d H:i:s');
  //     $deployment->save();
  //
  //     return redirect()->back();
  //   }
  // }

}
