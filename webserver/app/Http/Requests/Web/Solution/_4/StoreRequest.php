<?php

namespace App\Http\Requests\Web\Solution\_4;

use App\Http\Requests\JsonRequest;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\Plan;

class StoreRequest extends JsonRequest
{
    public function rules()
    {
        $plans = Solution::find(4)->plans;
        $plan = Plan::find($this->input('form.1.plan'));

        return [
            // 付費方案
            'form.1.plan' => 'required|in:'.$plans->implode('id', ','),

            // 機器人名稱
            'form.2.name.name' => 'required|string|min:5|max:20',

            // 機器人描述
            'form.2.description.description' => 'required|string|min:15|max:35',

            // channel ID
            'form.2.channelId.channelId' => 'required|string',

            // channel secret
            'form.2.channelSecret.channelSecret' => 'required|string',

            // channel access token
            'form.2.channelAccessToken.channelAccessToken' => 'required|string',

            // basic ID
            'form.2.basicId.basicId' => 'required|string',

            // your user ID
            'form.2.yourUserID.yourUserID' => 'required|string',

            // 頭貼
            'form.2.image.file' => 'required|image|mimes:jpeg,png|dimensions:max_width=1024,max_height=1024,ratio:1|max:1024',
        ];
    }


    public function messages()
    {

        return [
            // 付費方案
            'form.1.plan.required' => '請選擇方案',
            'form.1.plan.in' => '方案不在允許中',

            // 機器人名稱
            'form.2.name.name.required' => '請輸入機器人名稱',
            'form.2.name.name.string' => '機器人名稱必須是文字',
            'form.2.name.name.min' => '機器人名稱長度下限是5',
            'form.2.name.name.max' => '機器人名稱長度上限是20',

            // 機器人描述
            'form.2.description.description.required' => '請輸入機器人描述',
            'form.2.description.description.string' => '機器人描述必須是文字',
            'form.2.description.description.min' => '機器人描述長度下限是15',
            'form.2.description.description.max' => '機器人描述長度上限是35',

            // channel ID
            'form.2.channelId.channelId.required' => '請輸入 Channel ID',
            'form.2.channelId.channelId.string' => 'Channel ID 必須是文字',


            // channel secret
            'form.2.channelSecret.channelSecret.required' => '請輸入 Channel Secret',
            'form.2.channelSecret.channelSecret.string' => 'Channel Secret 必須是文字',

            // channel access token
            'form.2.channelAccessToken.channelAccessToken.required' => '請輸入 Channel Access Token',
            'form.2.channelAccessToken.channelAccessToken.string' => 'Channel Access Token 必須是文字',

            // basic ID
            'form.2.basicId.basicId.required' => '請輸入 Basic ID',
            'form.2.basicId.basicId.string' => 'Basic ID 必須是文字',

            // your user ID
            'form.2.yourUserID.yourUserID.required' => '請輸入您的 user ID',
            'form.2.yourUserID.yourUserID.string' => '您的 user ID 必須是文字',

            'form.2.image.file.required' => '請上傳圖片',
            'form.2.image.file.image' => '上傳檔案必須是圖片檔',
            'form.2.image.file.mimes' => '上傳檔案必須是圖片檔',
            'form.2.image.file.dimensions' => '圖片尺寸必須符合：正方形、最大寬度1024、最大高度1024',
            'form.2.image.file.max' => '圖片大小必須小於1Mb',
        ];
    }


    public function withValidator($validator)
    {
        $validator->after(function($validator){

            /* ========== 檢查 Chatbot 有沒有建立了 ========== */

            $deployment = Deployment::select('id')
            ->where('solution_id', 4)
            ->where('data->channelId', $this->input('form.2.channelId.channelId'))
            ->first();

            // if($deployment) $validator->errors()->add('form.2.channelId.channelId', '機器人已經串接平台');
        });
    }

}
