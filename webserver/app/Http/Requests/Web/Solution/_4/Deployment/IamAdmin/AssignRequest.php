<?php

namespace App\Http\Requests\Web\Solution\_4\Deployment\IamAdmin;

use App\Http\Requests\JsonRequest;
use Spatie\Permission\Models\Role;

class AssignRequest extends JsonRequest
{

    public function rules()
    {
        $roles = Role::select('id')
        ->where('deployment_id', $this->deployment->id)
        ->pluck('id');

        return [
            // 使用者 ID
            'editForm.user.id' => 'required|exists:users,id',

            // 權限角色
            'editForm.roles.value' => 'required|array|min:1',
            'editForm.roles.value.*.value' => 'required|in:'.$roles->implode(','),
        ];
    }

    public function messages()
    {
        return [
            // 使用者 ID
            'editForm.user.id.required' => '請選擇使用者',
            'editForm.user.id.exists' => '查無使用者',

            // 權限角色
            'editForm.roles.value.required' => '請選擇角色',
            'editForm.roles.value.array' => '角色必須是陣列',
            'editForm.roles.value.min' => '請至少賦予一個角色',

            'editForm.roles.value.*.value.required' => '角色錯誤',
            'editForm.roles.value.*.value.in' => '角色無效',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function($validator){

            if(!$validator->errors()->first('editForm.user.id') && !$validator->errors()->first('editForm.roles.value')){
                $superadminRole = Role::where('name', 'deployment_'.$this->deployment->id.'_superadmin')->first();
                $superadmins = $superadminRole->users()->select('id')->pluck('id');

                if(!collect($this->input('editForm.roles.value'))->contains('value', $superadminRole->id) && $superadmins->count() == 1 && $superadmins->contains($this->input('editForm.user.id'))){
                    $validator->errors()->add('editForm.roles.value', '必須至少一位使用者擁有超級管理員權限');
                }

            }

        });
    }
}
