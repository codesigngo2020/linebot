<?php

namespace App\Http\Requests\Web\Solution\_4\Deployment\Category;

use App\Http\Requests\JsonRequest;

class NameRequest extends JsonRequest
{

    public function rules()
    {
        return [
            // 類別名稱
            'editForm.name.name' => 'required|string|min:1|max:20',
        ];
    }

    public function messages()
    {

        return [
            // 類別名稱
            'editForm.name.name.required' => '請輸入類別名稱',
            'editForm.name.name.string' => '類別名稱必須是文字',
            'editForm.name.name.min' => '類別名稱長度下限是1',
            'editForm.name.name.max' => '類別名稱長度上限是20',
        ];
    }
}
