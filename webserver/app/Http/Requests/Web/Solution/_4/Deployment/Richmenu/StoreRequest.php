<?php

namespace App\Http\Requests\Web\Solution\_4\Deployment\Richmenu;

use App\Http\Requests\JsonRequest;

// Model
use App\Models\Platform\Solution\_4\Liff;
use App\Models\Platform\Solution\_4\Script;
use App\Models\Platform\Solution\_4\Richmenu;
use App\Models\Platform\Solution\_4\Keyword;

class StoreRequest extends JsonRequest
{

    public function __construct()
    {
        $this->actionTypes = ['message', 'keyword', 'uri', 'liff', 'script', 'richmenu'];

        // 現有的Liff尺寸
        $this->liffSizes = Liff::where('is_created_by_platform', 1)
        ->get()
        ->map(function($liff){
            switch($liff->view->type){
                case 'full':
                return 100;
                break;

                case 'tall':
                return 80;
                break;

                case 'compact':
                return 50;
                break;

                default:
                # code...
                break;
            }
        })
        ->unique()
        ->toArray();
    }

    public function rules()
    {
        $rules = [
            // 選單名稱
            'form.name.name' => 'required|string|min:5|max:20',

            // 選單描述
            'form.description.description' => 'required|string|min:15|max:35',

            // 尺寸選擇
            'form.size.size' => 'required|in:2500x1686,2500x843',

            // 資訊條內容
            'form.chatBarText.chatBarText' => 'required|string|max:14',

            // 自動開啟
            'form.selected.selected' => 'required|in:true,false',
        ];

        // 圖片
        if($this->input('form.size.size') == '2500x843'){
            $rules['form.image.file'] = 'required|image|mimes:jpeg,png|dimensions:width=2500,height=843';
        }else{
            $rules['form.image.file'] = 'required|image|mimes:jpeg,png|dimensions:width=2500,height=1686';
        }

        return $rules;
    }


    public function messages()
    {

        return [
            // 選單名稱
            'form.name.name.required' => '請輸入選單名稱',
            'form.name.name.string' => '選單名稱必須是文字',
            'form.name.name.min' => '選單名稱長度下限是5',
            'form.name.name.max' => '選單名稱長度上限是20',

            // 選單描述
            'form.description.description.required' => '請輸入選單描述',
            'form.description.description.string' => '選單描述必須是文字',
            'form.description.description.min' => '選單描述長度下限是15',
            'form.description.description.max' => '選單描述長度上限是35',

            // 尺寸選擇
            'form.size.size.required' => '請選擇選單尺寸',
            'form.size.size.in' => '選單尺寸必須 2500x1686 或 2500x843',

            // 資訊條內容
            'form.chatBarText.chatBarText.required' => '請輸入資訊條內容',
            'form.chatBarText.chatBarText.string' => '資訊條內容必須是文字',
            'form.chatBarText.chatBarText.max' => '資訊條內容長度上限是14',

            // 自動開啟
            'form.selected.selected.required' => '請勾選自動開啟',
            'form.selected.selected.in' => '自動開啟必須是 true 或 false',

            // 圖片
            'form.image.file.required' => '請上傳選單圖片',
            'form.image.file.image' => '上傳檔案必須是圖片檔',
            'form.image.file.mimes' => '上傳檔案必須是圖片檔',
            'form.image.file.dimensions' => '選單圖片的尺寸必須是 2500x1686 或 2500x843，並符合您選擇的尺寸',
        ];
    }


    public function withValidator($validator)
    {
        $validator->after(function($validator){

            // 驗證名稱重複
            if(!$validator->errors()->first('form.name.name')){
                $richmenu = Richmenu::where('name', $this->input('form.name.name'))
                ->whereNull('deleted_all_at')
                ->first();

                if($richmenu) $validator->errors()->add('form.name.name', '選單名稱已經存在');
            }

            $this->validateArea($validator, 'form.area.area', $this->input('form.area.area'));
        });
    }

    public function validateArea($validator, $path, $areasData)
    {
        foreach($areasData as $key => $area){
            $currentPath = $path.'.'.$key;

            if(isset($area['id'])){
                if(!isset($area['type']) || !isset($area['type']['value']) || !in_array($area['type']['value'], $this->actionTypes)){
                    $validator->errors()->add($currentPath.'.type', '區域#'.$area['id'].'的行為類型錯誤');
                    continue;
                }

                $this->{'validate'.ucfirst($area['type']['value'])}($validator, $currentPath, $area);

            }else{
                $this->validateArea($validator, $currentPath, $area);
            }
        }
    }

    /* ========== 驗證actions ========== */

    private function validateMessage($validator, $path, $area)
    {
        $text = $this->input($path.'.action.text.text');
        if($text){
            if(strlen($text) > 240) $validator->errors()->add($path.'.action.text.text', '訊息內容長度上限是240');
        }else{
            $validator->errors()->add($path.'.action.text.text', '請輸入訊息內容');
        }
    }

    private function validateKeyword($validator, $path, $area)
    {
        $keyword = $this->input($path.'.action.keyword.value.value');
        $text = $this->input($path.'.action.text.text');

        if(!$keyword && !$text){
            $validator->errors()->add($path.'.action.keyword.value.value', '「選擇關鍵字」和「關鍵字名稱」不可同時為空');
            $validator->errors()->add($path.'.action.text.text', '「選擇關鍵字」和「關鍵字名稱」不可同時為空');
        }elseif($keyword){
            $keyword = Keyword::find($keyword);
            if(!$keyword) $validator->errors()->add($path.'.action.keyword.value.value', '查無關鍵字');
        }elseif($text && strlen($text) > 300){
            $validator->errors()->add($path.'.action.text.text', '關鍵字名稱長度上限是300');
        }
    }

    private function validateUri($validator, $path, $area)
    {
        $uri = $this->input($path.'.action.uri.uri');
        if($uri){
            if(strlen($uri) > 1000 ) $validator->errors()->add($path.'.action.uri.uri', '網址長度上限是1000');
        }else{
            $validator->errors()->add($path.'.action.uri.uri', '請輸入網址');
        }
    }

    private function validateLiff($validator, $path, $area)
    {
        $uri = $this->input($path.'.action.uri.uri');
        if($uri){
            if(strlen($uri) > 1000) $validator->errors()->add($path.'.action.uri.uri', '網址長度上限是1000');
        }else{
            $validator->errors()->add($path.'.action.uri.uri', '請輸入網址');
        }

        $size = $this->input($path.'.action.size.value.value');
        if($uri){
            if(!in_array($size, $this->liffSizes)) $validator->errors()->add($path.'.action.size.value.value', '查無 Liff App 尺寸');
        }else{
            $validator->errors()->add($path.'.action.size.value.value', '請選擇 Liff App 尺寸');
        }
    }

    private function validateScript($validator, $path, $area)
    {
        $script = $this->input($path.'.action.script.value.value');
        if($script = Script::find($script)){
            if(!$script) $validator->errors()->add($path.'.action.script.value.value', '查無訊息腳本');
        }else{
            $validator->errors()->add($path.'.action.script.value.value', '請選擇訊息腳本');
        }
    }

    private function validateRichmenu($validator, $path, $area)
    {
        $richmenu = $this->input($path.'.action.richmenu.value.value');
        if($richmenu = Richmenu::find($richmenu)){
            if(!$richmenu) $validator->errors()->add($path.'.action.richmenu.value.value', '查無主選單');
        }else{
            $validator->errors()->add($path.'.action.script.value.value', '請選擇主選單');
        }
    }



}
