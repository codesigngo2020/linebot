<?php

use Carbon\Carbon;
use App\Http\Requests\JsonRequest;

// // Model
// use App\Models\Platform\Solution\_4\Table;

class StoreRequest extends JsonRequest
{
    public function __construct()
    {
        $this->reservedColumns = [
            'normal' => 'id,created_at,updated_at',
            'coupon' => 'id,created_at,updated_at,user_id,redeemed_at,redemption_url,staff_member_id,qrcode_url,code',
        ];

        $this->mustContainColumns = [
            'coupon' => ['name'],
        ];

        $this->columnTypes = 'integer,unsignedInteger,float,varchar,text,date,timestamp';
    }

    public function rules()
    {
        $rules = [
            // 資料表名稱
            'form.name.name' => 'required|string|min:5|max:20',

            // 資料表描述
            'form.description.description' => 'required|string|min:15|max:35',

            // 資料表類型
            'form.type.type' => 'required|in:normal,coupon',

            // csv資料驗證
            'form.rows.rows' => 'required|array|min:1|max:10000',
        ];

        // 抽獎規則
        if($this->input('form.type.type') == 'coupon'){
            // 隨機領取
            $rules['form.random.random'] = 'required|in:true,false';

            // 數量限制
            $rules['form.limit.active.active'] = 'required|in:true,false';
            $rules['form.limit.count.count'] = 'required_if:form.limit.active.active,true|integer|min:1';

            // 時間限制
            $rules['form.issueDatetime.active.active'] = 'required|in:true,false';
            $rules['form.issueDatetime.date.date'] = [
                'required_if:form.issueDatetime.active.active,true',
                'regex:/^[0-9]{4}-[0-1][0-9]-[0-3][0-9] ~ [0-9]{4}-[0-1][0-9]-[0-3][0-9]$/',
            ];
            $rules['form.issueDatetime.time.time'] = [
                'required_if:form.issueDatetime.active.active,true',
                'regex:/^[0-2][0-9]:[0-5][0-9] ~ [0-2][0-9]:[0-5][0-9]$/',
            ];
        }

        // 欄位設定 & 新增欄位
        foreach(['columns', 'additionalColumns'] as $columns){
            $rules['form.'.$columns.'.'.$columns] = 'nullable|array';
            $rules['form.'.$columns.'.'.$columns.'.*.name.name'] = 'required|string|min:1|max:20|not_in:'.$this->reservedColumns[$this->input('form.type.type')];
            $rules['form.'.$columns.'.'.$columns.'.*.type.value.value'] = 'required|in:'.$this->columnTypes;
            $rules['form.'.$columns.'.'.$columns.'.*.canBeNull.canBeNull'] = 'required|in:true,false';
            $rules['form.'.$columns.'.'.$columns.'.*.isUnique.isUnique'] = 'required|in:true,false';
        }

        $rules['form.columns.columns.*.originalName.originalName'] = 'required|string|min:1|not_in:id,created_at,updated_at';
        $rules['form.columns.columns.*.deleted.deleted'] = 'required|in:true,false';

        // csv資料驗證
        $columns = $this->input('form.columns.columns');
        if(is_array($columns) && count($columns) > 0){
            foreach($columns as $column){
                if($column['deleted']['deleted'] == 'false'){
                    $rules['form.rows.rows.*.'.$column['originalName']['originalName']] = 'required|'.$this->getDataRule($column['type']['value']['value']);
                }
            }
        }

        return $rules;
    }

    private function getDataRule($type)
    {
        switch($type){
            case 'integer':
            return $type;
            break;

            case 'unsignedInteger':
            return 'integer|min:0';
            break;

            case 'float':
            return 'numeric';
            break;

            case 'varchar':
            return 'string|min:1|max:180';
            break;

            case 'text':
            return 'string|min:1';
            break;

            case 'date':
            return 'date_format:Y-m-d';
            break;

            case 'timestamp':
            return 'date_format:Y-m-d H:i:s';
            break;
        }
    }

    public function messages()
    {

        $messages = [
            // 資料表名稱
            'form.name.name.required' => '請輸入資料表名稱',
            'form.name.name.string' => '資料表名稱必須是文字',
            'form.name.name.min' => '資料表名稱長度下限是5',
            'form.name.name.max' => '資料表名稱長度上限是20',
            'form.name.name.unique' => '資料表名稱已經存在',

            // 資料表描述
            'form.description.description.required' => '請輸入資料表描述',
            'form.description.description.string' => '資料表描述必須是文字',
            'form.description.description.min' => '資料表描述長度下限是15',
            'form.description.description.max' => '資料表描述長度上限是35',

            // 抽獎規則->隨機抽取
            'form.random.random.required' => '請勾選是否啟用隨機領取',
            'form.random.random.in' => '啟用隨機領取必須是 true 或 false',

            // 抽獎規則->數量限制
            'form.limit.active.active.required' => '請勾選是否限制數量',
            'form.limit.active.active.in' => '是否限制數量必須是 true 或 false',

            'form.limit.count.count.required_if' => '請輸入限制數量',
            'form.limit.count.count.integer' => '限制數量必須是正整數',
            'form.limit.count.count.min' => '限制數量必須是正整數',

            // 抽獎規則->時間限制
            'form.issueDatetime.active.active.required' => '請勾選是否啟用時間限制',
            'form.issueDatetime.active.active.in' => '是否啟用時間限制必須是 true 或 false',

            'form.issueDatetime.date.date.required_if' => '請輸入日期區間',
            'form.issueDatetime.date.date.regex' => '請輸入正確日期',

            'form.issueDatetime.time.time.required_if' => '請輸入時間區間',
            'form.issueDatetime.time.time.regex' => '請輸入正確時間',
        ];

        // 欄位設定 & 新增欄位
        foreach(['columns', 'additionalColumns'] as $columns){
            $messages['form.'.$columns.'.'.$columns.'.*.name.name.required'] = '請輸入欄位名稱';
            $messages['form.'.$columns.'.'.$columns.'.*.name.name.string'] = '欄位名稱必須是文字';
            $messages['form.'.$columns.'.'.$columns.'.*.name.name.min'] = '欄位名稱長度下限是1';
            $messages['form.'.$columns.'.'.$columns.'.*.name.name.max'] = '欄位名稱長度上限是20';
            $messages['form.'.$columns.'.'.$columns.'.*.name.name.not_in'] = '此為系統保留欄位名稱，禁止使用';

            $messages['form.'.$columns.'.'.$columns.'.*.type.value.value.required'] = '請輸入欄位型態';
            $messages['form.'.$columns.'.'.$columns.'.*.type.value.value.in'] = '欄位型態不在允許值內';

            $messages['form.'.$columns.'.'.$columns.'.*.canBeNull.canBeNull.required'] = '請勾選是否可為空值';
            $messages['form.'.$columns.'.'.$columns.'.*.canBeNull.canBeNull.in'] = '必須是 true 或 false';

            $messages['form.'.$columns.'.'.$columns.'.*.isUnique.isUnique.required'] = '請勾選是否可為唯一值';
            $messages['form.'.$columns.'.'.$columns.'.*.isUnique.isUnique.in'] = '必須是 true 或 false';
        }

        $messages['form.columns.columns.*.originalName.originalName.required'] = '無欄位原始名稱';
        $messages['form.columns.columns.*.originalName.originalName.string'] = '欄位原始名稱必須是文字';
        $messages['form.columns.columns.*.originalName.originalName.min'] = '欄位原始名稱長度下限是1';
        $messages['form.columns.columns.*.originalName.originalName.not_in'] = '此為系統保留欄位名稱，禁止使用';

        $messages['form.columns.columns.*.deleted.deleted.required'] = '請勾選是否刪除欄位';
        $messages['form.columns.columns.*.deleted.deleted.in'] = '必須是 true 或 false';

        // csv資料驗證
        $columns = $this->input('form.columns.columns');
        if(is_array($columns) && count($columns) > 0){
            foreach($columns as $column){
                $this->setDataMessage($messages, $column['originalName']['originalName'], $column['type']['value']['value']);
            }
        }

        return $messages;
    }

    private function setDataMessage(&$messages, $name, $type)
    {
        $messages['form.rows.rows.*.'.$name.'.required'] = '不能為空';
        switch($type){
            case 'integer':
            $messages['form.rows.rows.*.'.$name.'.integer'] = '必須是整數';
            break;

            case 'unsignedInteger':
            $messages['form.rows.rows.*.'.$name.'.integer'] = '必須是整數';
            $messages['form.rows.rows.*.'.$name.'.min'] = '必須大於等於0';
            break;

            case 'float':
            $messages['form.rows.rows.*.'.$name.'.numeric'] = '必須是數字';
            break;

            case 'varchar':
            return 'string|min:1|max:180';
            $messages['form.rows.rows.*.'.$name.'.string'] = '必須是文字';
            $messages['form.rows.rows.*.'.$name.'.min'] = '文字長度下限是1';
            $messages['form.rows.rows.*.'.$name.'.max'] = '文字長度上限是180';
            break;

            case 'text':
            $messages['form.rows.rows.*.'.$name.'.string'] = '必須是文字';
            $messages['form.rows.rows.*.'.$name.'.min'] = '文字長度下限是1';
            break;

            case 'date':
            $messages['form.rows.rows.*.'.$name.'.date_format'] = '必須符合Y-m-d日期格式';
            break;

            case 'timestamp':
            $messages['form.rows.rows.*.'.$name.'.date_format'] = '必須符合Y-m-d H:i:s時間戳格式';
            break;
        }
    }

    public function withValidator($validator)
    {
        $validator->after(function($validator){

            $columnsName = [];
            $mustContainColumnsName = [];

            foreach(['columns', 'additionalColumns'] as $type){
                $columns = $this->input('form.'.$type.'.'.$type);
                if(is_array($columns)){
                    foreach($columns as $columnsIndex => $column){

                        if($this->input('form.type.type') == 'coupon'){
                            if($type == 'columns' && in_array($column['name']['name'], $this->mustContainColumns['coupon'])){
                                array_push($mustContainColumnsName, $column['name']['name']);
                            }
                        }

                        if(!preg_match('/^[a-z_]+$/', $column['name']['name'])){
                            $validator->errors()->add('form.'.$type.'.'.$type.'.'.$columnsIndex.'.name.name', '僅允許小寫英文、底線(_)');
                        }

                        if(in_array($column['name']['name'], $columnsName)){
                            $validator->errors()->add('form.'.$type.'.'.$type.'.'.$columnsIndex.'.name.name', '欄位名稱不能重複');
                        }

                        $columnsName[] = $column['name']['name'];

                    }
                }
            }

            if($this->input('form.type.type') == 'coupon'){
                // 驗證必須欄位
                if(count($this->mustContainColumns['coupon']) != count($mustContainColumnsName)){
                    $validator->errors()->add('form.columns.columns', '請確認建立所有必須欄位：'.implode(', ', $this->mustContainColumns['coupon']));
                }
                
                // 驗證時間限制
                if($this->input('form.issueDatetime.active.active') == 'true'){
                    foreach(['date', 'time'] as $type){
                        $input = str_replace(' ', '', $this->input('form.issueDatetime.'.$type.'.'.$type));
                        $dts = explode('~', $input);
                        foreach($dts as &$dt){
                            try{
                                $dt = Carbon::parse($dt);
                            }catch(\Exception $ex){
                                $validator->errors()->add('form.issueDatetime.'.$type.'.'.$type, '請輸入正確'.($type == 'date' ? '日期' : '時間'));
                                break;
                            }
                        }
                        if($dts[0] > $dts[1]) $validator->errors()->add('form.issueDatetime.'.$type.'.'.$type, '請輸入正確'.($type == 'date' ? '日期' : '時間').'順序');
                    }
                }
            }

        });
    }

}
