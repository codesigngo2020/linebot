<?php

// namespace App\Http\Requests\Web\Solution\_4\Deployment\Script;

use App\Http\Requests\JsonRequest;

// Model
use App\Models\Platform\Solution\_4\Liff;
use App\Models\Platform\Solution\_4\Keyword;

use App\Http\Requests\Web\Solution\_4\Deployment\basic\StoreScriptTrait;

class NewVersionRequest extends JsonRequest
{

    use StoreScriptTrait;

    public function __construct()
    {
        $this->nodesPath = 'form.script.script.nodes.nodes';
        $this->construct();
    }

    public function rules()
    {
        $rules = [

            // 節點
            'form.script.script.nodes.nodes' => 'required|array|min:1',

            // 節點 id
            'form.script.script.nodes.nodes.*.id' => 'required|integer|min:1',

            // 節點 start
            'form.script.script.nodes.nodes.*.start' => 'required|integer|min:1',

            // 節點 end
            'form.script.script.nodes.nodes.*.end' => 'required|integer|min:1',



            // 節點內訊息
            'form.script.script.nodes.nodes.*.messages.messages' => 'required:array|max:6',

            // 節點內訊息類型
            'form.script.script.nodes.nodes.*.messages.messages.*.type' => 'required|in:'.implode(',', $this->messageTypes),

        ];


        // 訊息格式驗證
        $this->validateMessages($rules);

        return $rules;
    }

    public function messages()
    {

        $messages = [
            // 節點
            'form.script.script.nodes.nodes.required' => '請建立節點',
            'form.script.script.nodes.nodes.array' => '節點必須是陣列',
            'form.script.script.nodes.nodes.min' => '請至少建立一個節點',

            // 節點 id
            'form.script.script.nodes.nodes.*.id.required' => '請輸入節點ID',
            'form.script.script.nodes.nodes.*.id.integer' => '節點ID必須是整數',
            'form.script.script.nodes.nodes.*.id.min' => '節點ID必須大於等於1',

            // 節點 start
            'form.script.script.nodes.nodes.*.start.required' => '節點的 start值 無效',
            'form.script.script.nodes.nodes.*.start.integer' => '節點的 start值 必須是整數',
            'form.script.script.nodes.nodes.*.start.min' => '節點的 start值 無效',

            // 節點 end
            'form.script.script.nodes.nodes.*.end.required' => '節點的 end值 無效',
            'form.script.script.nodes.nodes.*.end.integer' => '節點的 end值 必須是整數',
            'form.script.script.nodes.nodes.*.end.min' => '節點的 end值 無效',

            // 節點內訊息
            // 新增訊息
            'form.script.script.nodes.nodes.*.messages.messages.required' => '請輸入訊息',
            'form.script.script.nodes.nodes.*.messages.messages.array' => '訊息必須是陣列',
            'form.script.script.nodes.nodes.*.messages.messages.max' => '訊息長度上限是6',

            // 訊息類型
            'form.script.script.nodes.nodes.*.messages.messages.*.type.required' => '請選擇訊息類型',
            'form.script.script.nodes.nodes.*.messages.messages.*.type.in' => '訊息類型不在允許類型中',
        ];

        $this->setInvalidMessages($messages);
        return $messages;
    }

}
