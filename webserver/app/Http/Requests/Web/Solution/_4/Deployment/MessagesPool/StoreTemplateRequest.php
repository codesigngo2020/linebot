<?php

namespace App\Http\Requests\Web\Solution\_4\Deployment\MessagesPool;

use Illuminate\Validation\Rule;
use App\Http\Requests\JsonRequest;

// Model
use App\Models\Platform\Solution\_4\Liff;
use App\Models\Platform\Solution\_4\Keyword;

use App\Http\Requests\Web\Solution\_4\Deployment\basic\StoreTemplateTrait;

class StoreTemplateRequest extends JsonRequest
{

    use StoreTemplateTrait;

    public function __construct()
    {
        $this->nodesPath = 'form.template.template.nodes.nodes';
        $this->construct();
    }

    public function rules()
    {
        // 設定需要驗證的節點
        $this->setNonDisabledNodes();

        $rules = [
            // 訊息名稱
            'form.name.name' => 'required|string|min:5|max:20',

            // 訊息描述
            'form.description.description' => 'required|string|min:15|max:35',

            // 模組類型
            'form.templateType.value.value' => 'required|in:coupon',

            // 節點
            'form.template.template.nodes.nodes' => 'required|array|min:1',

            // 節點 id
            'form.template.template.nodes.nodes.*.id' => 'required|integer|min:1',

            // 節點 start
            'form.template.template.nodes.nodes.*.start' => 'required|integer|min:1',

            // 節點 end
            'form.template.template.nodes.nodes.*.end' => 'required|integer|min:1',



            // 節點內訊息
            'form.template.template.nodes.nodes.*.messages.messages' => 'required:array|max:6',

            // 節點內訊息類型
            'form.template.template.nodes.nodes.*.messages.messages.*.type' => 'required|in:'.implode(',', $this->messageTypes),
        ];

        // 驗證模組設定
        if($this->input('form.templateType.value.value') == 'coupon'){
            $rules['form.templateSettings.table.value.value'] = [
                'required',
                Rule::exists('solution_4.tables', 'id')->where(function($q){
                    $q->where('type', 'coupon');
                }),
            ];
        }

        // 訊息格式驗證
        $this->validateMessages($rules);

        return $rules;
    }

    public function messages()
    {

        $messages = [
            // 訊息名稱
            'form.name.name.required' => '請輸入訊息名稱',
            'form.name.name.string' => '訊息名稱必須是文字',
            'form.name.name.mix' => '訊息名稱長度下限是5',
            'form.name.name.max' => '訊息名稱長度上限是20',

            // 訊息描述
            'form.description.description.required' => '請輸入訊息描述',
            'form.description.description.string' => '訊息描述必須是文字',
            'form.description.description.mix' => '訊息描述長度下限是15',
            'form.description.description.max' => '訊息描述長度上限是35',

            // 模組類型
            'form.templateType.value.value.required' => '請選擇模組類型',
            'form.templateType.value.value.in' => '模組類型無效',

            // 節點
            'form.template.template.nodes.nodes.required' => '請建立節點',
            'form.template.template.nodes.nodes.array' => '節點必須是陣列',
            'form.template.template.nodes.nodes.min' => '請至少建立一個節點',

            // 節點 id
            'form.template.template.nodes.nodes.*.id.required' => '請輸入節點ID',
            'form.template.template.nodes.nodes.*.id.integer' => '節點ID必須是整數',
            'form.template.template.nodes.nodes.*.id.min' => '節點ID必須大於等於1',

            // 節點 start
            'form.template.template.nodes.nodes.*.start.required' => '節點的 start值 無效',
            'form.template.template.nodes.nodes.*.start.integer' => '節點的 start值 必須是整數',
            'form.template.template.nodes.nodes.*.start.min' => '節點的 start值 無效',

            // 節點 end
            'form.template.template.nodes.nodes.*.end.required' => '節點的 end值 無效',
            'form.template.template.nodes.nodes.*.end.integer' => '節點的 end值 必須是整數',
            'form.template.template.nodes.nodes.*.end.min' => '節點的 end值 無效',

            // 節點內訊息
            // 新增訊息
            'form.template.template.nodes.nodes.*.messages.messages.required' => '請輸入訊息',
            'form.template.template.nodes.nodes.*.messages.messages.array' => '訊息必須是陣列',
            'form.template.template.nodes.nodes.*.messages.messages.max' => '訊息長度上限是6',

            // 訊息類型
            'form.template.template.nodes.nodes.*.messages.messages.*.type.required' => '請選擇訊息類型',
            'form.template.template.nodes.nodes.*.messages.messages.*.type.in' => '訊息類型不在允許類型中',
        ];

        // 模組設定
        if($this->input('form.templateType.value.value') == 'coupon'){
            $messages['form.templateSettings.table.value.value.required'] = '請選擇優惠券資料表';
            $messages['form.templateSettings.table.value.value.exists'] = '查無優惠券資料表';
        }

        $this->setInvalidMessages($messages);
        return $messages;
    }

}
