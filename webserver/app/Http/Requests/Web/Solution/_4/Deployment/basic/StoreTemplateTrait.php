<?php

namespace App\Http\Requests\Web\Solution\_4\Deployment\basic;

// Model
use App\Models\Platform\Solution\_4\Liff;
use App\Models\Platform\Solution\_4\Keyword;
use App\Models\Platform\Solution\_4\Table;

trait StoreTemplateTrait
{
    private function construct()
    {
        $this->prefix = $this->nodesPath.'.';

        $this->messageTypes = ['text','image','video','imagemap','buttons','confirm','carousel','imagecarousel','quickreply','coupon'];

        $this->actionTypes = ['message','keyword','uri','liff','richmenu'];
        $this->implodedActionTypes = implode(',', $this->actionTypes);

        $this->quickReplyActionTypes = ['message','keyword','script','richmenu','camera','cameraRoll'];
        $this->implodedQuickReplyActionTypes = implode(',', $this->quickReplyActionTypes);

        $this->imagemapActionTypes = ['keyword', 'uri', 'liff'];

        // 現有的Liff尺寸
        $this->liffSizes = $this->getLiffs();
        $this->implodedLiffSizes = $this->liffSizes->implode(',');
    }

    private function setNonDisabledNodes()
    {
        if($this->input('form.templateType.value.value') == 'coupon'){
            $table = Table::find($this->input('form.templateSettings.table.value.value'));
            if($table){
                $disabledConditions = [];
                if(!$table->settings->issueDatetime->active) array_push($disabledConditions, 'invalid-time');
                if(!$table->settings->limit->active) array_push($disabledConditions, 'reach-limit');

                $this->nonDisabledNodes = array_filter($this->input('form.template.template.nodes.nodes'), function($node) use($disabledConditions){
                    return !in_array($node['condition']['value'], $disabledConditions);
                });
                return;
            }
        }

        $this->nonDisabledNodes = $this->input('form.template.template.nodes.nodes');
    }

    private function getLiffs()
    {
        return Liff::where('is_created_by_platform', 1)
        ->get()
        ->map(function($liff){
            switch($liff->view->type){
                case 'full':
                return 100;
                break;

                case 'tall':
                return 80;
                break;

                case 'compact':
                return 50;
                break;

                default:
                # code...
                break;
            }
        })
        ->unique();
    }

    private function validateMessages(&$rules)
    {
        if($this->input('form.template.template.nodes.nodes')){
            foreach($this->nonDisabledNodes as $nodeIndex => $node){
                foreach($node['messages']['messages'] as $messageIndex => $message){

                    if(in_array($message['type'], $this->messageTypes)){

                        switch($message['type']){
                            case 'text':
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.text.text'] = 'required|string|max:2000';
                            break;

                            case 'image':
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.file'] = 'required|image|dimensions:max_width=4096,max_height=4096|max:1024';
                            break;

                            case 'imagemap':
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.file'] = 'required|image|dimensions:width=1040,height=1040,ratio=1|max:1024';
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.altText.altText'] = 'required|string|max:400';
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.show.active'] = 'required|in:true,false';

                            break;

                            case 'buttons':
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.title.title'] = 'required|string|max:40';
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.altText.altText'] = 'required|string|max:400';
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.active.active'] = 'required|in:true,false';

                            if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.active.active') == 'true'){
                                $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.text.text'] = 'required|string|max:60';
                                $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.ratio.ratio'] = 'required|in:rectangle,square';

                                if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.ratio.ratio') == 'rectangle'){
                                    $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.file'] = 'required|image|dimensions:max_width=1024|max:1024';
                                }else{
                                    $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.file'] = 'required|image|dimensions:max_width=1024,ratio:1|max:1024';
                                }

                            }else{
                                $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.text.text'] = 'required|string|max:160';
                            }

                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.actions'] = 'required|array|min:1|max:4';
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.actions.*.type.value'] = 'required|in:'.$this->implodedActionTypes;

                            foreach($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.actions') as $actionIndex => $action){
                                $this->setActionRules($rules, $this->getMessagePrefix($nodeIndex, $messageIndex).'.actions.'.$actionIndex.'.action', $action);
                            }
                            break;

                            case 'confirm':
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.altText.altText'] = 'required|string|max:400';
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.text.text'] = 'required|string|max:240';
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.actions'] = 'required|array|size:2';
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.actions.*.type.value'] = 'required|in:'.$this->implodedActionTypes;

                            foreach($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.actions') as $actionIndex => $action){
                                $this->setActionRules($rules, $this->getMessagePrefix($nodeIndex, $messageIndex).'.actions.'.$actionIndex.'.action', $action);
                            }
                            break;

                            case 'carousel':
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.altText.altText'] = 'required|string|max:400';
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns'] = 'required|array|min:1|max:10';

                            // 標題
                            if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.title.active.active') == 'true'){
                                $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.*.title.title'] = 'required|string|max:40';
                            }

                            // 圖片尺寸
                            if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.active.active') == 'true'){
                                $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.ratio.ratio'] = 'required|in:rectangle,square';
                            }

                            // 說明文字
                            if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.title.active.active') == 'false' || $this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.active.active') == 'false'){
                                $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.*.text.text'] = 'required|string|max:120';
                            }else{
                                $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.*.text.text'] = 'required|string|max:60';
                            }

                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.*.actions'] = 'required|array|min:1|max:3';
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.*.actions.*.type.value'] = 'required|in:'.$this->implodedActionTypes;

                            foreach($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns') as $columnIndex => $column){

                                // 圖片
                                if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.active.active') == 'true'){
                                    if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.ratio.ratio') == 'rectangle'){
                                        $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.'.$columnIndex.'.image.file'] = 'required|image|dimensions:max_width=1024|max:1024';
                                    }else{
                                        $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.'.$columnIndex.'.image.file'] = 'required|image|dimensions:max_width=1024,ratio:1|max:1024';
                                    }
                                }

                                foreach($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.'.$columnIndex.'.actions') as $actionIndex => $action){
                                    $this->setActionRules($rules, $this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.'.$columnIndex.'.actions.'.$actionIndex.'.action', $action);
                                }
                            }
                            break;

                            case 'imagecarousel':
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.altText.altText'] = 'required|string|max:400';
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns'] = 'required|array|min:1|max:10';

                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.*.actions'] = 'required|array|size:1';
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.*.actions.*.type.value'] = 'required|in:'.$this->implodedActionTypes;

                            foreach($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns') as $columnIndex => $column){

                                // 圖片
                                if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.active.active') == 'true'){
                                    $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.'.$columnIndex.'.image.file'] = 'required|image|dimensions:max_width=1024,ratio:1|max:1024';
                                }

                                foreach($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.'.$columnIndex.'.actions') as $actionIndex => $action){
                                    $this->setActionRules($rules, $this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.'.$columnIndex.'.actions.'.$actionIndex.'.action', $action);
                                }
                            }
                            break;

                            case 'quickreply':
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.items.items'] = 'required|array|min:1|max:10';

                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.items.items.*.actions'] = 'required|array|size:1';
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.items.items.*.actions.*.type.value'] = 'required|in:'.$this->implodedQuickReplyActionTypes;

                            foreach($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.items.items') as $itemIndex => $item){
                                // 圖片
                                $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.items.items.'.$itemIndex.'.image.file'] = 'nullable|image|dimensions:ratio:1|max:1024';

                                foreach($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.items.items.'.$itemIndex.'.actions') as $actionIndex => $action){
                                    $this->setActionRules($rules, $this->getMessagePrefix($nodeIndex, $messageIndex).'.items.items.'.$itemIndex.'.actions.'.$actionIndex.'.action', $action);
                                }
                            }
                            break;

                            case 'coupon':
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.type.type'] = 'required|in:image,imagemap';
                            if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.type.type') == 'imagemap'){
                                $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.altText.altText'] = 'required|string|max:400';
                            }
                            $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.active.active'] = 'required|in:true,false';

                            if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.active.active') == 'true'){
                                if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.type.type') == 'imagemap'){
                                    $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.image.file'] = 'required|image|mimetypes:image/jpeg|dimensions:width=1040,height=1040,ratio=1|max:1024';
                                }else{
                                    $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.image.file'] = 'required|image|mimetypes:image/jpeg|dimensions:max_width=4096,max_height=4096|max:1024';
                                }

                                $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.position.x.x'] = 'required|numeric|min:0';
                                $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.position.y.y'] = 'required|numeric|min:0';
                                $rules[$this->getMessagePrefix($nodeIndex, $messageIndex).'.position.w.w'] = 'required|numeric|min:0';
                            }
                            break;

                            default:
                            # code...
                            break;
                        }

                    }
                }
            }
        }
    }

    private function setInvalidMessages(&$messages)
    {
        $messages = [

            // 共用
            $this->prefix.'*.message.altText.altText.required' => '請輸入替代文字',
            $this->prefix.'*.message.altText.altText.string' => '替代文字必須是文字',
            $this->prefix.'*.message.altText.altText.max' => '替代文字長度上限是400',

            $this->prefix.'*.message.image.file.required' => '請上傳圖片',
            $this->prefix.'*.message.image.file.image' => '上傳檔案必須是圖片檔',
            $this->prefix.'*.message.image.file.max' => '圖片大小必須小於1Mb',

            $this->prefix.'*.message.text.text.required' => '請輸入文字說明內容',
            $this->prefix.'*.message.text.text.string' => '文字說明內容必須是文字',

            $this->prefix.'*.message.actions.required' => '請建立按鈕',
            $this->prefix.'*.message.actions.array' => '按鈕必須是陣列',
            $this->prefix.'*.message.actions.min' => '請至少建立一個按鈕',

            $this->prefix.'*.message.actions.*.type.value.required' => '按鈕類型錯誤',
            $this->prefix.'*.message.actions.*.type.value.in' => '按鈕類型不在允許類型中',

            $this->prefix.'*.message.columns.columns.required' => '請建立卡片',
            $this->prefix.'*.message.columns.columns.array' => '卡片必須是陣列',
            $this->prefix.'*.message.columns.columns.min' => '請至少建立一張卡片',
            $this->prefix.'*.message.columns.columns.max' => '卡片數量上限是10',

            // imagemap
            $this->prefix.'*.message.show.active.required' => '請選擇是否啟用或隱藏點擊區域',
            $this->prefix.'*.message.show.active.required' => '啟用或隱藏點擊區域必須是 true 或 false',

            // buttons
            $this->prefix.'*.message.title.title.required' => '請輸入標題',
            $this->prefix.'*.message.title.title.string' => '標題必須是文字',
            $this->prefix.'*.message.title.title.max' => '標題長度上限是40',

            $this->prefix.'*.message.image.active.active.required' => '請勾選是否啟用圖片',
            $this->prefix.'*.message.image.active.active.in' => '啟用圖片必須是 true 或 false',

            $this->prefix.'*.message.image.ratio.ratio.required' => '請選擇圖片尺寸',
            $this->prefix.'*.message.image.ratio.ratio.in' => '圖片尺寸必須是「長方形」或「正方形」',

            // carousel
            $this->prefix.'*.message.columns.columns.*.title.title.required' => '請輸入標題',
            $this->prefix.'*.message.columns.columns.*.title.title.string' => '標題必須是文字',
            $this->prefix.'*.message.columns.columns.*.title.title.max' => '標題長度上限是40',

            $this->prefix.'*.message.columns.columns.*.text.text.required' => '請輸入文字說明內容',
            $this->prefix.'*.message.columns.columns.*.text.text.string' => '文字說明內容必須是文字',


            $this->prefix.'*.message.columns.columns.*.actions.required' => '請建立按鈕',
            $this->prefix.'*.message.columns.columns.*.actions.array' => '按鈕必須是陣列',
            $this->prefix.'*.message.columns.columns.*.actions.min' => '請至少建立一個按鈕',

            $this->prefix.'*.message.columns.columns.*.actions.*.type.value.required' => '按鈕類型錯誤',
            $this->prefix.'*.message.columns.columns.*.actions.*.type.value.in' => '按鈕類型不在允許類型中',

            $this->prefix.'*.message.columns.columns.*.image.file.required' => '請上傳圖片',
            $this->prefix.'*.message.columns.columns.*.image.file.image' => '上傳檔案必須是圖片檔',
            $this->prefix.'*.message.columns.columns.*.image.file.max' => '圖片大小必須小於1Mb',

            // quickreply
            $this->prefix.'*.message.items.items.required' => '請建立欄位',
            $this->prefix.'*.message.items.items.array' => '欄位必須是陣列',
            $this->prefix.'*.message.items.items.min' => '請至少建立一個欄位',
            $this->prefix.'*.message.items.items.max' => '欄位數量上限是10',


            $this->prefix.'*.message.items.items.*.actions.required' => '請建立按鈕',
            $this->prefix.'*.message.items.items.*.actions.array' => '按鈕必須是陣列',
            $this->prefix.'*.message.items.items.*.actions.size' => '請至少建立一個按鈕',

            $this->prefix.'*.message.items.items.*.actions.*.type.value.required' => '按鈕類型錯誤',
            $this->prefix.'*.message.items.items.*.actions.*.type.value.in' => '按鈕類型不在允許類型中',

            $this->prefix.'*.message.items.items.*.image.file.image' => '上傳檔案必須是圖片檔',
            $this->prefix.'*.message.items.items.*.image.file.image.dimensions' => '圖片尺寸必須符合：正方形',
            $this->prefix.'*.message.items.items.*.image.file.image.max' => '圖片大小必須小於1Mb',
            ] + $messages;

            // actions
            foreach([$this->prefix.'*.message.actions.', $this->prefix.'*.message.columns.columns.*.actions.', $this->prefix.'*.message.items.items.*.actions'] as $actionPrefix){
                $messages[$actionPrefix.'*.action.label.label.required'] = '請輸入按鈕名稱';
                $messages[$actionPrefix.'*.action.label.label.max'] = '按鈕名稱長度上限是20';

                $messages[$actionPrefix.'*.action.text.text.required'] = '請輸入訊息內容';
                // $messages[$actionPrefix.'*.action.text.text.max'] = '訊息內容長度上限是240';

                $messages[$actionPrefix.'*.action.keyword.value.value.required'] = '請選擇關鍵字';
                $messages[$actionPrefix.'*.action.keyword.value.value.exists'] = '查無關鍵字';

                $messages[$actionPrefix.'*.action.uri.uri.required'] = '請輸入網址';
                $messages[$actionPrefix.'*.action.uri.uri.url'] = '請輸入有效網址';

                $messages[$actionPrefix.'*.action.size.value.value.required'] = '請選擇 Liff App 尺寸';
                $messages[$actionPrefix.'*.action.size.value.value.in'] = 'Liff App 尺寸不在允許範圍';

                $messages[$actionPrefix.'*.action.script.value.value.required'] = '請選擇訊息腳本';
                $messages[$actionPrefix.'*.action.script.value.value.exists'] = '查無訊息腳本';
            }

            if($this->input('form.template.template.nodes.nodes')){
                foreach($this->nonDisabledNodes as $nodeIndex => $node){
                    foreach($node['messages']['messages'] as $messageIndex => $message){

                        if(in_array($message['type'], $this->messageTypes)){

                            switch($message['type']){
                                case 'text':
                                $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.text.text.required'] = '請輸入訊息內容';
                                $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.text.text.string'] = '訊息內容必須是文字';
                                $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.text.text.max'] = '訊息內容長度上限是2000';
                                break;

                                case 'image':
                                $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.file.dimensions'] = '圖片尺寸必須符合：最大寬度4096、最大高度4096';
                                break;

                                case 'imagemap':
                                $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.file.dimensions'] = '圖片尺寸必須符合：正方形、寬度1040、高度1040';
                                break;

                                case 'buttons':
                                if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.active.active') == 'true'){
                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.text.text.max'] = '文字說明內容長度上限是60';

                                    if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.ratio.ratio') == 'rectangle'){
                                        $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.file.dimensions'] = '圖片尺寸必須符合：最大寬度1024';
                                    }else{
                                        $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.file.dimensions'] = '圖片尺寸必須符合：正方形、最大寬度1024';
                                    }

                                }else{
                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.text.text.max'] = '文字說明內容長度上限是160';
                                }
                                $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.actions.max'] = '按鈕個數上限是4';
                                break;

                                case 'confirm':
                                $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.text.text.max'] = '文字說明內容長度上限是240';
                                $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.actions.size'] = '按鈕個數必須是2';
                                break;

                                case 'carousel':
                                // 說明文字
                                if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.title.active.active') == 'false' || $this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.active.active') == 'false'){
                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.*.text.text.max'] = '文字說明內容長度上限是120';
                                }else{
                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.*.text.text.max'] = '文字說明內容長度上限是60';
                                }

                                $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.*.actions.max'] = '按鈕數量數量上限是3';

                                foreach($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns') as $columnIndex => $column){

                                    // 圖片
                                    if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.active.active') == 'true'){
                                        if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.ratio.ratio') == 'rectangle'){
                                            $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.'.$columnIndex.'.image.file.dimensions'] = '圖片尺寸必須符合：最大寬度1024';
                                        }else{
                                            $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.'.$columnIndex.'.image.file.dimensions'] = '圖片尺寸必須符合：正方形、最大寬度1024';
                                        }
                                    }
                                }
                                break;

                                case 'imagecarousel':
                                $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.*.actions'] = '按鈕數量必須是1';
                                foreach($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns') as $columnIndex => $column){
                                    // 圖片
                                    if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.active.active') == 'true'){
                                        $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns.'.$columnIndex.'.image.file.dimensions'] = '圖片尺寸必須符合：正方形、最大寬度1024';
                                    }
                                }
                                break;

                                case 'coupon':
                                $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.type.type.required'] = '請選擇訊息類型';
                                $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.type.type.in'] = '訊息類型必須是 image 或 imagemap';

                                $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.active.active'] = 'required|in:true,false';

                                if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.image.active.active') == 'true'){

                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.image.file.required'] = '請上傳圖片';
                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.image.file.image'] = '上傳檔案須為圖片格式';
                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.image.file.mimetypes'] = '上傳檔案須為image/jpeg格式';
                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.image.file.max'] = '圖片大小必須小於1Mb';

                                    if($this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.type.type') == 'imagemap'){
                                        $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.image.file.dimensions'] = '圖片尺寸必須符合：1040x1040';
                                    }else{
                                        $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.image.image.file.dimensions'] = '圖片尺寸必須符合：最大寬度4096、最大高度4096';
                                    }


                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.position.x.x.required'] = '請輸入QRcode位置';
                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.position.x.x.numeric'] = 'x座標必須是數字';
                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.position.x.x.min'] = 'x座標必須大於等於0';

                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.position.y.y.required'] = '請輸入QRcode位置';
                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.position.y.y.numeric'] = 'y座標必須是數字';
                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.position.y.y.min'] = 'y座標必須大於等於0';

                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.position.w.w.required'] = '請輸入QRcode寬度';
                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.position.w.w.numeric'] = '寬度必須是數字';
                                    $messages[$this->getMessagePrefix($nodeIndex, $messageIndex).'.position.w.w.min'] = '寬度必須大於等於0';
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }

        /* ========== 產生 message prefix ========== */

        private function getMessagePrefix($nodeIndex, $messageIndex)
        {
            return $this->prefix.$nodeIndex.'.messages.messages.'.$messageIndex.'.message';
        }

        /* ========== 產生驗證 actions 的 rules ========== */

        private function setActionRules(&$rules, $actionPath, $action)
        {
            if(in_array($action['type']['value'], $this->actionTypes)){

                $rules[$actionPath.'.label.label'] = 'required|max:20';
                switch($action['type']['value']){
                    // case 'next':
                    // $rules[$actionPath.'.nextId.nextId'] = 'required|integer|min:1';
                    // break;

                    case 'message':
                    $rules[$actionPath.'.text.text'] = 'required';
                    break;

                    case 'keyword':
                    // $rules[$actionPath.'.keyword.value.value'] = 'required|exists:solution_4.keywords,id';
                    if($this->input($actionPath.'.keyword.value.value')){
                        $rules[$actionPath.'.keyword.value.value'] = 'required|exists:solution_4.keywords,id';
                    }elseif($this->input($actionPath.'.text.text')){
                        $rules[$actionPath.'.text.text'] = 'required';
                    }
                    break;

                    case 'uri':
                    $rules[$actionPath.'.uri.uri'] = 'required|url';
                    break;

                    case 'liff':
                        $rules[$actionPath.'.size.value.value'] = 'required|in:'.$this->implodedLiffSizes;
                        $rules[$actionPath.'.uri.uri'] = 'required|url';
                        break;

                        case 'richmenu':
                        $rules[$actionPath.'.richmenu.value.value'] = 'required|exists:solution_4.richmenus,id';
                        break;

                        default:
                        # code...
                        break;
                    }

                }
            }

            public function withValidator($validator)
            {
                $validator->after(function($validator){

                    if($this->input('form.template.template.nodes.nodes')){

                        $nodesId = [];
                        $startAndEnd = range(2, 2 * count($this->input('form.template.template.nodes.nodes')) + 1);

                        foreach($this->nonDisabledNodes as $nodeIndex => $node){

                            // 驗證節點 Id
                            if(in_array($node['id'], $nodesId)){
                                $validator->errors()->add($this->prefix.$nodeIndex.'.id', '節點ID必須唯一');
                                return;
                            }
                            array_push($nodesId, $node['id']);

                            // 驗證節點 Start End
                            foreach(['start', 'end'] as $se){
                                $key = array_search(intval($node[$se]), $startAndEnd);
                                if($key === false){
                                    $validator->errors()->add('form.template.template.nodes.nodes', '腳本架構錯誤');
                                    return;
                                }
                                unset($startAndEnd[$key]);
                            }

                            // 驗證節點訊息 Id
                            $messagesId = [];
                            foreach($node['messages']['messages'] as $messageIndex => $message){
                                preg_match('/^'.$node['id'].'-(\d+)$/', $message['id'], $match);
                                if(!$match){
                                    $validator->errors()->add($this->prefix.$nodeIndex.'.messages.messages.'.$messageIndex.'.id', '訊息ID錯誤');
                                    return;

                                }elseif(in_array($message['id'], $messagesId)){
                                    $validator->errors()->add($this->prefix.$nodeIndex.'.messages.messages.'.$messageIndex.'.id', '訊息ID必須唯一');
                                    return;
                                }
                                array_push($messagesId, $message['id']);

                                // 驗證 imagemap area & carousel buttons
                                if($message['type'] == 'imagemap' && $this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.show.active') == 'true'){
                                    $this->validateArea($validator, $this->getMessagePrefix($nodeIndex, $messageIndex).'.area.area', $message['message']['area']['area']);

                                }elseif(in_array($message['type'], ['buttons', 'confirm', 'carousel'])){
                                    $this->{'validate'.($message['type'] == 'carousel' ? 'Carousel' : 'ButtonsAndConfirm')}($validator, $nodeIndex, $messageIndex);
                                }


                                // 驗證 message & keyword action
                                if(in_array($message['type'], ['buttons', 'confirm'])){
                                    foreach($message['message']['actions'] as $actionIndex => $action){
                                        if($action['type']['value'] == 'message' && $action['action']['text']['text'] && strlen($action['action']['text']['text']) > 240){
                                            $validator->errors()->add($this->getMessagePrefix($nodeIndex, $messageIndex).'.actions.'.$actionIndex.'.action.text.text', '訊息內容長度上限是240');
                                        }

                                        if($action['type']['value'] == 'keyword' && !$action['action']['keyword']['value'] && !$action['action']['text']['text']){
                                            $validator->errors()->add($this->getMessagePrefix($nodeIndex, $messageIndex).'.actions.'.$actionIndex.'.action.keyword.value', '「選擇關鍵字」和「關鍵字名稱」不可同時為空');
                                            $validator->errors()->add($this->getMessagePrefix($nodeIndex, $messageIndex).'.actions.'.$actionIndex.'.action.text.text', '「選擇關鍵字」和「關鍵字名稱」不可同時為空');
                                        }

                                        if($action['type']['value'] == 'keyword' && $action['action']['text']['text'] && strlen($action['action']['text']['text']) > 300){
                                            $validator->errors()->add($this->getMessagePrefix($nodeIndex, $messageIndex).'.actions.'.$actionIndex.'.action.text.text', '關鍵字名稱長度上限是300');
                                        }
                                    };
                                }elseif(in_array($message['type'], ['carousel', 'imagecarousel', 'quickreply'])){

                                    $columnName = $message['type'] == 'quickreply' ? 'items' : 'columns';

                                    foreach($message['message'][$columnName][$columnName] as $columnIndex => $column){
                                        foreach($column['actions'] as $actionIndex => $action){
                                            if($action['type']['value'] == 'message' && $action['action']['text']['text'] && strlen($action['action']['text']['text']) > 240){
                                                $validator->errors()->add($this->getMessagePrefix($nodeIndex, $messageIndex).'.'.$columnName.'.'.$columnName.'.'.$columnIndex.'.actions.'.$actionIndex.'.action.text.text', '訊息內容長度上限是240');
                                            }

                                            if($action['type']['value'] == 'keyword' && !$action['action']['keyword']['value'] && !$action['action']['text']['text']){
                                                $validator->errors()->add($this->getMessagePrefix($nodeIndex, $messageIndex).'.'.$columnName.'.'.$columnName.'.'.$columnIndex.'.actions.'.$actionIndex.'.action.keyword.value', '「選擇關鍵字」和「關鍵字名稱」不可同時為空');
                                                $validator->errors()->add($this->getMessagePrefix($nodeIndex, $messageIndex).'.'.$columnName.'.'.$columnName.'.'.$columnIndex.'.actions.'.$actionIndex.'.action.text.text', '「選擇關鍵字」和「關鍵字名稱」不可同時為空');
                                            }

                                            if($action['type']['value'] == 'keyword' && $action['action']['text']['text'] && strlen($action['action']['text']['text']) > 300){
                                                $validator->errors()->add($this->getMessagePrefix($nodeIndex, $messageIndex).'.'.$columnName.'.'.$columnName.'.'.$columnIndex.'.actions.'.$actionIndex.'.action.text.text', '關鍵字名稱長度上限是300');
                                            }
                                        };
                                    }
                                }elseif($message['type'] == 'coupon'){
                                    $x = intval($message['message']['position']['x']['x']);
                                    $y = intval($message['message']['position']['y']['y']);
                                    $w = intval($message['message']['position']['w']['w']);
                                    $width = intval($message['message']['image']['image']['width']);
                                    $height = intval($message['message']['image']['image']['height']);

                                    if($x > $width - 1){
                                        $validator->errors()->add($this->getMessagePrefix($nodeIndex, $messageIndex).'.position.x.x', 'x座標必須小於圖片寬度');
                                    }
                                    if($y > $height - 1){
                                        $validator->errors()->add($this->getMessagePrefix($nodeIndex, $messageIndex).'.position.y.y', 'y座標必須小於圖片高度');
                                    }
                                    if($x + $w > $width){
                                        $validator->errors()->add($this->getMessagePrefix($nodeIndex, $messageIndex).'.position.w.w', 'QRcode超出圖片範圍');
                                    }
                                    if($y + $w > $height){
                                        $validator->errors()->add($this->getMessagePrefix($nodeIndex, $messageIndex).'.position.w.w', 'QRcode超出圖片範圍');
                                    }

                                }


                            }

                        }

                        // 驗證節點 Start End
                        if(count($startAndEnd) / 2 != count($this->input('form.template.template.nodes.nodes')) - count($this->nonDisabledNodes)){
                            $validator->errors()->add('form.template.template.nodes.nodes', '腳本架構錯誤');
                        }
                    }

                });
            }

            /* ========== 驗證imagemap ========== */

            private function validateArea($validator, $path, $areasData)
            {
                foreach($areasData as $key => $area){
                    $currentPath = $path.'.'.$key;

                    if(isset($area['id'])){
                        if(!isset($area['type']) || !isset($area['type']['value']) || !in_array($area['type']['value'], $this->imagemapActionTypes)){
                            $validator->errors()->add($currentPath.'.type', '區域#'.$area['id'].'的行為類型錯誤');
                            continue;
                        }

                        $this->{'validate'.ucfirst($area['type']['value'])}($validator, $currentPath, $area);

                    }else{
                        $this->validateArea($validator, $currentPath, $area);
                    }
                }
            }

            private function validateKeyword($validator, $path, $area)
            {
                $keyword = $this->input($path.'.action.keyword.value.value');
                if($keyword = Keyword::find($keyword)){
                    if(!$keyword) $validator->errors()->add($path.'.action.keyword.value.value', '查無關鍵字');
                }else{
                    $validator->errors()->add($path.'.action.keyword.value.value', '請選擇關鍵字');
                }
            }

            private function validateUri($validator, $path, $area)
            {
                $uri = $this->input($path.'.action.uri.uri');
                if($uri){
                    if(strlen($uri) > 1000 ) $validator->errors()->add($path.'.action.uri.uri', '網址長度上限是1000');
                }else{
                    $validator->errors()->add($path.'.action.uri.uri', '請輸入網址');
                }
            }

            private function validateLiff($validator, $path, $area)
            {
                $size = $this->input($path.'.action.size.value.value');
                if($size){
                    if(!$this->liffSizes->contains($size)) $validator->errors()->add($path.'.action.size.value.value', '查無 Liff App 尺寸');
                }else{
                    $validator->errors()->add($path.'.action.size.value.value', '請選擇 Liff App 尺寸');
                }

                $uri = $this->input($path.'.action.uri.uri');
                if($uri){
                    if(strlen($uri) > 1000 ) $validator->errors()->add($path.'.action.uri.uri', '網址長度上限是1000');
                }else{
                    $validator->errors()->add($path.'.action.uri.uri', '請輸入網址');
                }
            }

            /* ========== 驗證 carousel ========== */

            private function validateCarousel($validator, $nodeIndex, $messageIndex)
            {
                $columns = $this->input($this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns');
                $actionsCount = count(array_shift($columns)['actions']);

                foreach($columns as $column){

                    // 驗證每個 column 的 action 數是否一致
                    if(count($column['actions']) != $actionsCount){
                        $validator->errors()->add($this->getMessagePrefix($nodeIndex, $messageIndex).'.columns.columns', '每張卡片的按鈕數必須相同');
                    }

                }
            }
        }
