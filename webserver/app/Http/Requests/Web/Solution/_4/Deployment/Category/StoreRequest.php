<?php

namespace App\Http\Requests\Web\Solution\_4\Deployment\Category;

use App\Http\Requests\JsonRequest;

// Model
use App\Models\Platform\Solution\_4\Category;

class StoreRequest extends JsonRequest
{

    public function rules()
    {
        return [

            'createForm.type.type' => 'required|string|in:insert,child',

            'createForm.category.category' => 'required|integer|min:-1',

            // 類別名稱
            'createForm.name.name' => 'required|string|min:1|max:20',
        ];
    }

    public function messages()
    {

        return [
            
            'createForm.type.type.required' => '請選擇建立類型',
            'createForm.type.type.string' => '建立類型必須是文字',
            'createForm.type.type.in' => '建立類型必須是insert 或 child',

            'createForm.category.category.required' => '請選擇基準類別ID',
            'createForm.category.category.integer' => '基準類別ID必須整數',
            'createForm.category.category.min' => '基準類別ID必須大於等於-1',

            // 類別名稱
            'createForm.name.name.required' => '請輸入類別名稱',
            'createForm.name.name.string' => '類別名稱必須是文字',
            'createForm.name.name.min' => '類別名稱長度下限是1',
            'createForm.name.name.max' => '類別名稱長度上限是20',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function($validator){

            if(!$validator->errors()->first('createForm.category.category') && $this->input('createForm.category.category') != -1){
                $category = Category::find($this->input('createForm.category.category'));
                if(!$category) $validator->errors()->add('createForm.category.category', '查無類別');
            }

        });
    }
}
