<?php

namespace App\Http\Requests\Web\Solution\_4\Deployment\Category;

use App\Http\Requests\JsonRequest;

// Model
use App\Models\Platform\Solution\_4\Category;

class DeleteRequest extends JsonRequest
{

    public function rules()
    {
        return [];
    }

    public function withValidator($validator)
    {
        $validator->after(function($validator){

            $category = $this->input('removeForm.category.value.value');
            if(!$category){
                $validator->errors()->add('removeForm.category.value.value', '請選擇類別');
            }elseif($category != -1 && !$category = Category::find($category)){
                $validator->errors()->add('removeForm.category.value.value', '查無類別');
            }

        });
    }

}
