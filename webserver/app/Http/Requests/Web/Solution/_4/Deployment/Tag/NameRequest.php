<?php

namespace App\Http\Requests\Web\Solution\_4\Deployment\Tag;

use App\Http\Requests\JsonRequest;

class NameRequest extends JsonRequest
{

    public function rules()
    {
        return [
            // 標籤名稱
            'form.name.name' => 'required|string|min:5|max:20',
        ];
    }

    public function messages()
    {

        return [
            // 標籤名稱
            'form.name.name.required' => '請輸入標籤名稱',
            'form.name.name.string' => '標籤名稱必須是文字',
            'form.name.name.min' => '標籤名稱長度下限是5',
            'form.name.name.max' => '標籤名稱長度上限是20',
        ];
    }
}
