<?php

use App\Http\Requests\JsonRequest;

use Illuminate\Validation\Rule;

// trait
use App\Http\Requests\Web\Solution\_4\Deployment\basic\StoreMessageTrait;

class StoreRequest extends JsonRequest
{

    use StoreMessageTrait;

    public function __construct()
    {
        $this->messagesPath = 'form.messages.messages';
        $this->prefix = $this->messagesPath.'.';
        $this->imagePregMatch = '/^https:\/\/storage\.googleapis\.com\/'.env('GOOGLE_CLOUD_STORAGE_BUCKET').'\//';

        $this->messageTypes = ['text','image','imagemap','buttons','confirm','carousel', 'imagecarousel', 'quickreply'];

        $this->actionTypes = ['message','keyword','uri','liff','script','richmenu','camera','cameraRoll'];
        $this->implodedActionTypes = implode(',', $this->actionTypes);

        $this->imagemapActionTypes = ['keyword', 'uri', 'liff'];

        // 現有的Liff尺寸
        $this->liffSizes = $this->getLiffs();
        $this->implodedLiffSizes = $this->liffSizes->implode(',');
    }

    public function rules()
    {
        $rules = [
            // 歡迎訊息名稱
            'form.name.name' => 'required|string|min:5|max:20',

            // 歡迎訊息描述
            'form.description.description' => 'required|string|min:15|max:35',

            // 啟用狀態
            'form.active.active' => 'required|in:0,1',

            // 時間區段
            'form.periods.periods' => 'required|array|min:1',

            'form.periods.periods.*.hasEnd.hasEnd' => 'required|in:true,false',

            'form.periods.periods.*.date.start.start' => 'required|date_format:Y-m-d',

            'form.periods.periods.*.time.start.start' => 'required|date_format:H:i',
            'form.periods.periods.*.time.end.end' => 'required|date_format:H:i',

            // 訊息設定
            'form.messageType.messageType' => 'required|in:template,pool,new',
        ];

        foreach($this->input('form.periods.periods') as $periodIndex => $period){
            if($period['hasEnd']['hasEnd'] == 'true'){
                $rules['form.periods.periods.'.$periodIndex.'.date.end.end'] = 'required|date_format:Y-m-d';
            }
        }

        if($this->input('form.messageType.messageType') == 'pool' || $this->input('form.messageType.messageType') == 'template'){
            // 驗證訊息模組
            $rules['form.message.value.value'] = [
                'required',
                Rule::exists('solution_4.messages', 'parent_id')->where(function($q){
                    $q->where('type', $this->input('form.messageType.messageType') == 'pool' ? 'message' : 'template')
                    ->where('is_current_version', 1);
                }),
            ];
        }

        if($this->input('form.messageType.messageType') == 'pool' || $this->input('form.messageType.messageType') == 'new'){
            // 回覆訊息
            $rules['form.messages.messages'] = 'required|array|max:6';

            // 訊息類型
            $rules['form.messages.messages.*.type'] = 'required|in:'.implode(',', $this->messageTypes);
        }

        // 訊息格式驗證
        if($this->input('form.messageType.messageType') == 'pool' || $this->input('form.messageType.messageType') == 'new'){
            $this->validateMessages($rules);
        }

        return $rules;
    }

    public function messages()
    {

        $messages = [
            // 歡迎訊息名稱
            'form.name.name.required' => '請輸入歡迎訊息名稱',
            'form.name.name.string' => '歡迎訊息名稱必須是文字',
            'form.name.name.min' => '歡迎訊息名稱長度下限是5',
            'form.name.name.max' => '歡迎訊息名稱長度上限是20',

            // 歡迎訊息描述
            'form.description.description.required' => '請輸入歡迎訊息描述',
            'form.description.description.string' => '歡迎訊息描述必須是文字',
            'form.description.description.min' => '歡迎訊息描述長度下限是15',
            'form.description.description.max' => '歡迎訊息描述長度上限是35',

            // 啟用狀態
            'form.active.active.required' => '請選擇啟用狀態',
            'form.active.active.in' => '啟用狀態必須是 0 或 1',

            // 時間區段
            'form.periods.periods.required' => '請輸入時間區段',
            'form.periods.periods.array' => '時間區段必須是陣列',
            'form.periods.periods.min' => '請至少輸入一個時間區段',

            // 啟用結束時間
            'form.periods.periods.*.hasEnd.hasEnd.required' => '請勾選結束時間',
            'form.periods.periods.*.hasEnd.hasEnd.in' => '結束時間必須是 true 或 false',

            // 開始日期
            'form.periods.periods.*.date.start.start.required' => '請輸入開始日期',
            'form.periods.periods.*.date.start.start.date_format' => '開始日期必須符合日期格式：Y-m-d',

            // 結束日期
            'form.periods.periods.*.date.end.end.required' => '請輸入結束日期',
            'form.periods.periods.*.date.end.end.date_format' => '結束日期必須符合日期格式：Y-m-d',

            // 開始時間
            'form.periods.periods.*.time.start.start.required' => '請輸入開始時間',
            'form.periods.periods.*.time.start.start.date_format' => '開始時間必須符合時間格式：H:i',

            // 結束時間
            'form.periods.periods.*.time.end.end.required' => '請輸入結束時間',
            'form.periods.periods.*.time.end.end.date_format' => '結束時間必須符合時間格式：H:i',

            // 訊息設定
            'form.messageType.messageType.required' => '請選擇建立訊息方式',
            'form.messageType.messageType.in' => '建立訊息方式不在允許值內',

            // 訊息模組
            'form.message.value.value.required' => $this->input('form.messageType.messageType') == 'pool' ? '請選擇訊息庫訊息' : '請選擇訊息模組',
            'form.message.value.value.exists' => $this->input('form.messageType.messageType') == 'pool' ? '查無訊息' : '查無訊息模組',

            // 回覆訊息
            'form.messages.messages.required' => '請輸入訊息',
            'form.messages.messages.array' => '訊息必須是陣列',
            'form.messages.messages.max' => '訊息長度上限是6',

            // 訊息類型
            'form.messages.messages.*.type.required' => '請選擇訊息類型',
            'form.messages.messages.*.type.in' => '訊息類型不在允許類型中',
        ];

        if($this->input('form.messageType.messageType') == 'pool' || $this->input('form.messageType.messageType') == 'new'){
            $this->setInvalidMessages($messages);
        }

        return $messages;
    }


    public function withValidator($validator)
    {
        $validator->after(function($validator){

            foreach($this->input('form.periods.periods') as $periodIndex => $period){
                if($period['hasEnd']['hasEnd'] == 'true' && $period['date']['start']['start'] > $period['date']['end']['end']){
                    $validator->errors()->add('form.periods.periods.'.$periodIndex.'.date.end.end', '結束日期必須大於或等於開始日期');
                }

                if($period['time']['start']['start'] > $period['time']['end']['end']){
                    $validator->errors()->add('form.periods.periods.'.$periodIndex.'.time.end.end', '結束時間必須大於開始時間');
                }
            }

            if($this->input('form.messageType.messageType') == 'pool' || $this->input('form.messageType.messageType') == 'new'){
                // validateAll
                $this->validateAll($validator);
            }

        });
    }

}
