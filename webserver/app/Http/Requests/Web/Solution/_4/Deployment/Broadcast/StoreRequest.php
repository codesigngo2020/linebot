<?php

use App\Http\Requests\JsonRequest;

use Illuminate\Validation\Rule;

// trait
use App\Http\Requests\Web\Solution\_4\Deployment\basic\StoreMessageTrait;

class StoreRequest extends JsonRequest
{
    use StoreMessageTrait;

    public function __construct()
    {
        $this->messagesPath = 'form.messages.messages';
        $this->construct();
    }

    public function rules()
    {
        $rules = [
            // 推播訊息名稱
            'form.name.name' => 'required|string|min:5|max:20',

            // 推播訊息描述
            'form.description.description' => 'required|string|min:15|max:35',

            // 發送時間類型
            'form.timeType.timeType' => 'required|in:now,schedule',

            // 發送時間
            'form.datetime.datetime' => 'required_if:form.timeType.timeType,schedule',

            // 訊息設定
            'form.messageType.messageType' => 'required|in:template,pool,new',
        ];

        // 發送對象
        if($this->input('form.messageType.messageType') == 'template'){
            $rules['form.target.target_type'] = 'required|in:groups,users';
        }else{
            $rules['form.target.target_type'] = 'required|in:all,groups,users';
        }

        // 發送對象
        if($this->input('form.target.target_type') == 'groups'){
            // 發送好友群組
            $rules['form.usersGroups.value'] = 'required|array|min:1';
            $rules['form.usersGroups.value.*.value'] = 'required|exists:solution_4.groups,id';

        }elseif($this->input('form.target.target_type') == 'users'){
            // 發送特定好友
            $rules['form.users.value'] = 'required|array|min:1';
            $rules['form.users.value.*.value'] = [
                'required',
                Rule::exists('solution_4.users', 'id')->where(function($q){
                    $q->where('is_follow', 1);
                }),
            ];
        }


        if($this->input('form.messageType.messageType') == 'pool' || $this->input('form.messageType.messageType') == 'template'){
            // 驗證訊息模組
            $rules['form.message.value.value'] = [
                'required',
                Rule::exists('solution_4.messages', 'parent_id')->where(function($q){
                    $q->where('type', $this->input('form.messageType.messageType') == 'pool' ? 'message' : 'template')
                    ->where('is_current_version', 1);
                }),
            ];
        }

        if($this->input('form.messageType.messageType') == 'pool' || $this->input('form.messageType.messageType') == 'new'){
            // 推播訊息
            $rules['form.messages.messages'] = 'required|array|max:6';

            // 訊息類型
            $rules['form.messages.messages.*.type'] = 'required|in:'.implode(',', $this->messageTypes);
        }

        // 訊息格式驗證
        if($this->input('form.messageType.messageType') == 'pool' || $this->input('form.messageType.messageType') == 'new'){
            $this->validateMessages($rules);
        }

        return $rules;
    }

    public function messages()
    {

        $messages = [
            // 推播訊息名稱
            'form.name.name.required' => '請輸入推播訊息名稱',
            'form.name.name.string' => '推播訊息名稱必須是文字',
            'form.name.name.min' => '推播訊息名稱長度下限是5',
            'form.name.name.max' => '推播訊息名稱長度上限是20',

            // 推播訊息描述
            'form.description.description.required' => '請輸入推播訊息描述',
            'form.description.description.string' => '推播訊息描述必須是文字',
            'form.description.description.min' => '推播訊息描述長度下限是15',
            'form.description.description.max' => '推播訊息描述長度上限是35',

            // 發送對象
            'form.target.target_type.required' => '選選擇發送對象',
            'form.target.target_type.in' => '發送對象必須是「全部好友」、「好友群組」或「特定好友」',

            // 發送時間
            'form.timeType.timeType.required' => '請選擇發送時間',
            'form.timeType.timeType.in' => '發送時間必須是「立即發送」或「排程發送」',

            // 發送時間
            'form.datetime.datetime.required_if' => '請輸入發送時間',

            // 訊息設定
            'form.messageType.messageType.required' => '請選擇建立訊息方式',
            'form.messageType.messageType.in' => '建立訊息方式不在允許值內',

            // 訊息模組
            'form.message.value.value.required' => $this->input('form.messageType.messageType') == 'pool' ? '請選擇訊息庫訊息' : '請選擇訊息模組',
            'form.message.value.value.exists' => $this->input('form.messageType.messageType') == 'pool' ? '查無訊息' : '查無訊息模組',

            // 推播訊息
            'form.messages.messages.required' => '請輸入訊息',
            'form.messages.messages.array' => '訊息必須是陣列',
            'form.messages.messages.max' => '訊息長度上限是6',

            // 訊息類型
            'form.messages.messages.*.type.required' => '請選擇訊息類型',
            'form.messages.messages.*.type.in' => '訊息類型不在允許類型中',

            // 發送好友群組
            'form.usersGroups.value.required' => '請選擇好友群組',
            'form.usersGroups.value.array' => '好友群組必須是陣列',
            'form.usersGroups.value.min' => '請至少選擇一個好友群組',

            'form.usersGroups.value.*.value.required' => '請選擇有效好友群組',
            'form.usersGroups.value.*.value.exists' => '查無好友群組',

            // 發送特定好友
            'form.users.value.required' => '請選擇好友',
            'form.users.value.array' => '好友必須是陣列',
            'form.users.value.min' => '請至少選擇一個好友',

            'form.users.value.*.value.required' => '請選擇有效好友',
            'form.users.value.*.value.exists' => '查無好友',

        ];

        if($this->input('form.messageType.messageType') == 'pool' || $this->input('form.messageType.messageType') == 'new'){
            $this->setInvalidMessages($messages);
        }

        return $messages;
    }

    public function withValidator($validator)
    {
        if($this->input('form.messageType.messageType') == 'pool' || $this->input('form.messageType.messageType') == 'new'){
            $validator->after(function($validator){
                // validateAll
                $this->validateAll($validator);
            });
        }
    }

}
