<?php

namespace App\Http\Requests\Web\Solution\_4\Deployment\IamAdmin;

use App\Http\Requests\JsonRequest;
use Spatie\Permission\Models\Role;

class InviteRequest extends JsonRequest
{

    public function rules()
    {
        $roles = Role::select('id')
        ->where('deployment_id', $this->deployment->id)
        ->pluck('id');

        return [
            // 使用者 Email
            'addUserForm.account.account' => 'required|exists:users,email',

            // 權限角色
            'addUserForm.roles.value' => 'required|array|min:1',
            'addUserForm.roles.value.*.value' => 'required|in:'.$roles->implode(','),
        ];
    }

    public function messages()
    {
        return [
            // 使用者 Email
            'addUserForm.account.account.required' => '請輸入使用者Email',
            'addUserForm.account.account.exists' => '查無使用者',

            // 權限角色
            'addUserForm.roles.value.required' => '請選擇角色',
            'addUserForm.roles.value.array' => '角色必須是陣列',
            'addUserForm.roles.value.min' => '請至少賦予一個角色',

            'addUserForm.roles.value.*.value.required' => '角色錯誤',
            'addUserForm.roles.value.*.value.in' => '角色無效',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function($validator){

            if(!$validator->errors()->first('addUserForm.account.account')){
                if($this->deployment->users()->where('email', $this->input('addUserForm.account.account'))->first()){
                    $validator->errors()->add('addUserForm.account.account', '使用者已加入此部署');
                }
            }

        });
    }

}
