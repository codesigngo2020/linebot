<?php

namespace App\Http\Requests\Web\Solution\_4\Deployment\Tag;

use App\Http\Requests\JsonRequest;

class GetChartDataRequest extends JsonRequest
{

    public function rules()
    {
        return [
            'type' => 'required|in:week,hour',
            'start' => 'required|date_format:Y-m-d|before_or_equal:today'
        ];
    }


    public function messages()
    {
        return [];
    }

}
