<?php

namespace App\Http\Requests\Web\Solution\_4\Deployment\MessagesPool;

use App\Http\Requests\JsonRequest;

// trait
use App\Http\Requests\Web\Solution\_4\Deployment\basic\StoreMessageTrait;

class NewMessageVersionRequest extends JsonRequest
{

    use StoreMessageTrait;

    public function __construct()
    {
        $this->messagesPath = 'form.messages.messages';
        $this->construct();
    }

    public function rules()
    {
        $rules = [
            // 新增訊息
            'form.messages.messages' => 'required|array|max:6',

            // 訊息類型
            'form.messages.messages.*.type' => 'required|in:'.implode(',', $this->messageTypes),
        ];

        // 訊息格式驗證
        $this->validateMessages($rules);
        return $rules;
    }

    public function messages()
    {

        $messages = [
            // 新增訊息
            'form.messages.messages.required' => '請輸入訊息',
            'form.messages.messages.array' => '訊息必須是陣列',
            'form.messages.messages.max' => '訊息長度上限是6',

            // 訊息類型
            'form.messages.messages.*.type.required' => '請選擇訊息類型',
            'form.messages.messages.*.type.in' => '訊息類型不在允許類型中',
        ];

        $this->setInvalidMessages($messages);
        return $messages;
    }


    public function withValidator($validator)
    {
        $validator->after(function($validator){
            // validateAll
            $this->validateAll($validator);
        });
    }

}
