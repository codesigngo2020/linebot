<?php

use App\Http\Requests\JsonRequest;

// Model
use App\Models\Platform\Solution\_4\Liff;
use App\Models\Platform\Solution\_4\Keyword;

use App\Http\Requests\Web\Solution\_4\Deployment\basic\StoreScriptTrait;

class StoreRequest extends JsonRequest
{

    use StoreScriptTrait;

    public function __construct()
    {
        $this->nodesPath = 'form.script.script.nodes.nodes';
        $this->construct();
    }

    public function rules()
    {
        $rules = [
            // 訊息腳本名稱
            'form.name.name' => 'required|string|min:5|max:20',

            // 訊息腳本描述
            'form.description.description' => 'required|string|min:15|max:35',

            // 啟用狀態
            'form.active.active' => 'required|in:0,1',

            // 節點
            'form.script.script.nodes.nodes' => 'required|array|min:1',

            // 節點 id
            'form.script.script.nodes.nodes.*.id' => 'required|integer|min:1',

            // 節點 start
            'form.script.script.nodes.nodes.*.start' => 'required|integer|min:1',

            // 節點 end
            'form.script.script.nodes.nodes.*.end' => 'required|integer|min:1',



            // 節點內訊息
            'form.script.script.nodes.nodes.*.messages.messages' => 'required:array|max:6',

            // 節點內訊息類型
            'form.script.script.nodes.nodes.*.messages.messages.*.type' => 'required|in:'.implode(',', $this->messageTypes),

        ];


        // 訊息格式驗證
        $this->validateMessages($rules);

        return $rules;
    }

    public function messages()
    {

        $messages = [
            // 腳本名稱
            'form.name.name.required' => '請輸入腳本名稱',
            'form.name.name.string' => '腳本名稱必須是文字',
            'form.name.name.mix' => '腳本名稱長度下限是5',
            'form.name.name.max' => '腳本名稱長度上限是20',

            // 腳本描述
            'form.description.description.required' => '請輸入腳本描述',
            'form.description.description.string' => '腳本描述必須是文字',
            'form.description.description.min' => '腳本描述長度下限是15',
            'form.description.description.max' => '腳本描述長度上限是35',

            // 啟用狀態
            'form.active.active.required' => '請選擇啟用狀態',
            'form.active.active.in' => '啟用狀態必須是「Active」或「Inactive」',

            // 節點
            'form.script.script.nodes.nodes.required' => '請建立節點',
            'form.script.script.nodes.nodes.array' => '節點必須是陣列',
            'form.script.script.nodes.nodes.min' => '請至少建立一個節點',

            // 節點 id
            'form.script.script.nodes.nodes.*.id.required' => '請輸入節點ID',
            'form.script.script.nodes.nodes.*.id.integer' => '節點ID必須是整數',
            'form.script.script.nodes.nodes.*.id.min' => '節點ID必須大於等於1',

            // 節點 start
            'form.script.script.nodes.nodes.*.start.required' => '節點的 start值 無效',
            'form.script.script.nodes.nodes.*.start.integer' => '節點的 start值 必須是整數',
            'form.script.script.nodes.nodes.*.start.min' => '節點的 start值 無效',

            // 節點 end
            'form.script.script.nodes.nodes.*.end.required' => '節點的 end值 無效',
            'form.script.script.nodes.nodes.*.end.integer' => '節點的 end值 必須是整數',
            'form.script.script.nodes.nodes.*.end.min' => '節點的 end值 無效',

            // 節點內訊息
            // 新增訊息
            'form.script.script.nodes.nodes.*.messages.messages.required' => '請輸入訊息',
            'form.script.script.nodes.nodes.*.messages.messages.array' => '訊息必須是陣列',
            'form.script.script.nodes.nodes.*.messages.messages.max' => '訊息長度上限是6',

            // 訊息類型
            'form.script.script.nodes.nodes.*.messages.messages.*.type.required' => '請選擇訊息類型',
            'form.script.script.nodes.nodes.*.messages.messages.*.type.in' => '訊息類型不在允許類型中',
        ];

        $this->setInvalidMessages($messages);
        return $messages;
    }

}
