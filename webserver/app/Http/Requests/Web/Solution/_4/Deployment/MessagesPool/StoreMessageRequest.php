<?php

namespace App\Http\Requests\Web\Solution\_4\Deployment\MessagesPool;

use App\Http\Requests\JsonRequest;

// trait
use App\Http\Requests\Web\Solution\_4\Deployment\basic\StoreMessageTrait;

class StoreMessageRequest extends JsonRequest
{

    use StoreMessageTrait;

    public function __construct()
    {
        $this->messagesPath = 'form.messages.messages';
        $this->construct();
    }

    public function rules()
    {
        $rules = [
            // 推播訊息
            'form.name.name' => 'required|string|min:5|max:20',

            // 訊息描述
            'form.description.description' => 'required|string|min:15|max:35',

            // 新增訊息
            'form.messages.messages' => 'required|array|max:6',

            // 訊息類型
            'form.messages.messages.*.type' => 'required|in:'.implode(',', $this->messageTypes),
        ];

        // 訊息格式驗證
        $this->validateMessages($rules);
        return $rules;
    }

    public function messages()
    {

        $messages = [
            // 推播訊息名稱
            'form.name.name.required' => '請輸入訊息名稱',
            'form.name.name.string' => '訊息名稱必須是文字',
            'form.name.name.min' => '訊息名稱長度下限是5',
            'form.name.name.max' => '訊息名稱長度上限是20',

            // 推播訊息描述
            'form.description.description.required' => '請輸入訊息描述',
            'form.description.description.string' => '訊息描述必須是文字',
            'form.description.description.min' => '訊息描述長度下限是15',
            'form.description.description.max' => '訊息描述長度上限是35',

            // 新增訊息
            'form.messages.messages.required' => '請輸入訊息',
            'form.messages.messages.array' => '訊息必須是陣列',
            'form.messages.messages.max' => '訊息長度上限是6',

            // 訊息類型
            'form.messages.messages.*.type.required' => '請選擇訊息類型',
            'form.messages.messages.*.type.in' => '訊息類型不在允許類型中',
        ];

        $this->setInvalidMessages($messages);
        return $messages;
    }


    public function withValidator($validator)
    {
        $validator->after(function($validator){
            // validateAll
            $this->validateAll($validator);
        });
    }

}
