<?php

use App\Http\Requests\JsonRequest;

use Illuminate\Validation\Rule;

// trait
use App\Http\Requests\Web\Solution\_4\Deployment\basic\StoreMessageTrait;

class NewVersionRequest extends JsonRequest
{

    use StoreMessageTrait;

    public function __construct()
    {
        $this->messagesPath = 'form.trigger.trigger.messages.messages';
        $this->construct();
    }

    public function rules()
    {
        $rules = [
            // 觸發行為
            'form.triggerType.triggerType' => 'required|in:reply,script,richmenu',
        ];

        switch($this->input('form.triggerType.triggerType')){
            case 'reply':
            // 訊息設定
            $rules['form.trigger.trigger.messageType.messageType'] = 'required|in:template,pool,new';

            if($this->input('form.trigger.trigger.messageType.messageType') == 'pool' || $this->input('form.trigger.trigger.messageType.messageType') == 'template'){
                // 驗證訊息模組
                $rules['form.trigger.trigger.message.value.value'] = [
                    'required',
                    Rule::exists('solution_4.messages', 'parent_id')->where(function($q){
                        $q->where('type', $this->input('form.trigger.trigger.messageType.messageType') == 'pool' ? 'message' : 'template')
                        ->where('is_current_version', 1);
                    }),
                ];
            }

            if($this->input('form.trigger.trigger.messageType.messageType') == 'pool' || $this->input('form.trigger.trigger.messageType.messageType') == 'new'){
                // 推播訊息
                $rules['form.trigger.trigger.messages.messages'] = 'required|array|max:6';

                // 訊息類型
                $rules['form.trigger.trigger.messages.messages.*.type'] = 'required|in:'.implode(',', $this->messageTypes);
            }

            // 訊息格式驗證
            if($this->input('form.trigger.trigger.messageType.messageType') == 'pool' || $this->input('form.trigger.trigger.messageType.messageType') == 'new'){
                $this->validateMessages($rules);
            }
            break;

            case 'script':
            $rules['form.trigger.trigger.script.value.value'] = 'required|exists:solution_4.scripts,id';
            break;

            case 'richmenu':
            $rules['form.trigger.trigger.richmenu.value.value'] = 'required|exists:solution_4.richmenus,id';
            break;

            default:
            # code...
            break;
        }

        return $rules;
    }

    public function messages()
    {

        $messages = [
            // 觸發行為
            'form.triggerType.triggerType.required' => '請選擇觸發行為',
            'form.triggerType.triggerType.in' => '觸發行為必須是「回覆訊息」、「開啟腳本」或「切換選單」',

            // 訊息設定
            'form.trigger.trigger.messageType.messageType.required' => '請選擇建立訊息方式',
            'form.trigger.trigger.messageType.messageType.in' => '建立訊息方式不在允許值內',

            // 訊息模組
            'form.trigger.trigger.message.value.value.required' => $this->input('form.trigger.trigger.messageType.messageType') == 'pool' ? '請選擇訊息庫訊息' : '請選擇訊息模組',
            'for.trigger.triggerm.message.value.value.exists' => $this->input('form.trigger.trigger.messageType.messageType') == 'pool' ? '查無訊息' : '查無訊息模組',

            // 新增訊息
            'form.messages.messages.required' => '請輸入訊息',
            'form.messages.messages.array' => '訊息必須是陣列',
            'form.messages.messages.max' => '訊息長度上限是6',

            // 訊息類型
            'form.messages.messages.*.type.required' => '請選擇訊息類型',
            'form.messages.messages.*.type.in' => '訊息類型不在允許類型中',

            // 開啟腳本
            'form.trigger.trigger.script.value.value.required' => '請選擇開啟腳本',
            'form.trigger.trigger.script.value.value.exists' => '查無開啟腳本',

            // 切換選單
            'form.trigger.trigger.richmenu.value.value.required' => '請選擇主選單',
            'form.trigger.trigger.richmenu.value.value.exists' => '查無主選單',
        ];

        if($this->input('form.triggerType.triggerType') == 'reply' && $this->input('form.trigger.trigger.messageType.messageType') != 'template'){
            $this->setInvalidMessages($messages);
        }

        return $messages;
    }

    public function withValidator($validator)
    {
        if($this->input('form.triggerType.triggerType') == 'reply'){
            $validator->after(function($validator){

                if($this->input('form.triggerType.triggerType') == 'reply' && $this->input('form.trigger.trigger.messageType.messageType') != 'template'){
                    // validateAll
                    $this->validateAll($validator);
                }
            });
        }
    }

}
