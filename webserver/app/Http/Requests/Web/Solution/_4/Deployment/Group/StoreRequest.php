<?php

use App\Http\Requests\JsonRequest;

class StoreRequest extends JsonRequest
{

    public function __construct()
    {
        $this->circlesCount_selectedAreasCount = [
            1 => 1,
            2 => 3,
            3 => 7,
        ];
    }

    public function rules()
    {
        $rules = [
            // 群組名稱
            'form.name.name' => 'required|string|min:5|max:20',

            // 推播訊息描述
            'form.description.description' => 'required|string|min:15|max:35',

            // 群組條件
            'form.circles.circles' => 'nullable|array|max:3',

            // 群組條件圈圈數
            'form.circles.circles.*.circlesCount' => 'required|integer|min:1|max:3',

            // 群組條件標籤id
            'form.circles.circles.*.areas.areas.*.tagId' => 'required|exists:solution_4.tags,id',

            // 新增好友名單
            'form.addUsers.value' => 'nullable|array',
            'form.addUsers.value.*.value' => 'required|integer|exists:solution_4.users,id',

            // 移除好友名單
            'form.removeUsers.value' => 'nullable|array',
            'form.removeUsers.value.*.value' => 'required|integer|exists:solution_4.users,id',
        ];

        if($this->input('form.circles.circles')){
            foreach($this->input('form.circles.circles') as $circlesIndex => $circles){

                $circlesCount = intval($circles['circlesCount']);
                if($circlesCount >= 1 && $circlesCount <= 3){

                    // 群組條件標籤
                    $rules['form.circles.circles.'.$circlesIndex.'.areas.areas'] = 'required|array|size:'.$circlesCount;

                    // 群組條件選擇區域
                    $rules['form.circles.circles.'.$circlesIndex.'.selectedAreas.selectedAreas'] = 'required|array|max:'.$this->circlesCount_selectedAreasCount[$circlesCount];

                    // 群組條件選擇區域
                    $rules['form.circles.circles.'.$circlesIndex.'.selectedAreas.selectedAreas.*'] = 'required|integer|min:0|max:'.$this->circlesCount_selectedAreasCount[$circlesCount];

                }
            }
        }

        return $rules;
    }

    public function messages()
    {

        return [
            // 群組名稱
            'form.name.name.required' => '請輸入群組名稱',
            'form.name.name.string' => '群組名稱必須是文字',
            'form.name.name.min' => '群組名稱長度下限是5',
            'form.name.name.max' => '群組名稱長度上限是20',

            // 群組描述
            'form.description.description.required' => '請輸入群組描述',
            'form.description.description.string' => '群組描述必須是文字',
            'form.description.description.min' => '群組描述長度下限是15',
            'form.description.description.max' => '群組描述長度上限是35',

            // 群組條件
            'form.circles.circles.array' => '群組條件必須是陣列',
            'form.circles.circles.max' => '群組條件上限是3',

            // 群組條件圈圈數
            'form.circles.circles.*.circlesCount.required' => '請輸入篩選標籤數',
            'form.circles.circles.*.circlesCount.integer' => '篩選標籤數必須是整數',
            'form.circles.circles.*.circlesCount.min' => '篩選標籤數下限是1',
            'form.circles.circles.*.circlesCount.max' => '篩選標籤數上限是2',

            // 群組條件標籤id
            'form.circles.circles.*.areas.areas.*.tagId.required' => '請輸入篩選標籤',
            'form.circles.circles.*.areas.areas.*.tagId.exists' => '查無篩選標籤',

            // 新增好友名單
            'form.addUsers.value.array' => '新增好友名單必須是陣列',
            'form.addUsers.value.*.value.required' => '新增好友無效',
            'form.addUsers.value.*.value.exists' => '查無好友',

            // 移除好友名單
            'form.removeUsers.value.array' => '移除好友名單必須是陣列',
            'form.removeUsers.value.*.value.required' => '移除好友無效',
            'form.removeUsers.value.*.value.exists' => '查無好友',

            'form.circles.circles.*.areas.areas.required' => '集合區域無效',
            'form.circles.circles.*.areas.areas.array' => '集合區域必須是陣列',
            'form.circles.circles.*.areas.areas.size' => '集合區域數量不符',

            // 群組條件選擇區域
            'form.circles.circles.*.selectedAreas.selectedAreas.required' => '請選擇集合區域',
            'form.circles.circles.*.selectedAreas.selectedAreas.array' => '選擇集合區域必須是陣列',
            'form.circles.circles.*.selectedAreas.selectedAreas.max' => '選擇集合區域數量錯誤',

            // 群組條件選擇區域
            'form.circles.circles.*.selectedAreas.selectedAreas.*.required' => '選擇區域無效',
            'form.circles.circles.*.selectedAreas.selectedAreas.*.integer' => '選擇區域無效',
            'form.circles.circles.*.selectedAreas.selectedAreas.*.min' => '選擇區域無效',
            'form.circles.circles.*.selectedAreas.selectedAreas.*.max' => '選擇區域無效',
        ];
    }


    public function withValidator($validator)
    {
        $validator->after(function($validator){

            // 檢查每個標籤在條件群組中是唯一的
            if($this->input('form.circles.circles')){
                foreach($this->input('form.circles.circles') as $circlesIndex => $circles){
                    $tagsId = collect($circles['areas']['areas'])->pluck('tagId');
                    if($tagsId->count() != $tagsId->unique()->count()){
                        $validator->errors()->add('form.circles.circles'.$circlesIndex.'areas.areas', '行為標籤在單一條件群組內必須是唯一');
                    }
                }
            }

            // 檢查新增好友名單是唯一的
            if($this->input('form.addUsers.value')){
                $addUsers = collect($this->input('form.addUsers.value'))->pluck('value');
                if($addUsers->count() != $addUsers->unique()->count()){
                    $validator->errors()->add('form.addUsers.value', '新增好友必須是唯一');
                }
            }

            // 檢查移除好友名單是唯一的
            if($this->input('form.removeUsers.value')){
                $removeUsers = collect($this->input('form.removeUsers.value'))->pluck('value');
                if($removeUsers->count() != $removeUsers->unique()->count()){
                    $validator->errors()->add('form.removeUsers.value', '移除好友必須是唯一');
                }
            }

            // 群組條件 ＆ 新增好友名單不可以都為null
            if(!$this->input('form.circles.circles') && !$this->input('form.addUsers.value')){
                $validator->errors()->add('form.circles.circles', '「群組條件」和「新增好友名單」不能同時是空的');
                $validator->errors()->add('form.addUsers.value', '「群組條件」和「新增好友名單」不能同時是空的');
            }

        });
    }



}
