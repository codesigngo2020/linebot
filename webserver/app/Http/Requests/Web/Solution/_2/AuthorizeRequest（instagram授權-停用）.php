<?php

namespace App\Http\Requests\Web\Solution\_2;

use App\Http\Requests\JsonRequest;

class AuthorizeRequest extends JsonRequest
{

  public function rules()
  {
    return [
      'clientId' => 'required|string',
      'clientSecret' => 'required|string',
    ];
  }

  public function messages()
  {
    return [
      'clientId.required' => 'Client id is required',
      'clientId.string' => 'The client id must be a string',
      'clientSecret.required' => 'Client secret is required',
      'clientSecret.string' => 'The client secret must be a string',
    ];
  }

}
