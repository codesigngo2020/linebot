<?php

namespace App\Http\Requests\Web\Solution\_2;

use App\Http\Requests\JsonRequest;

class updateSettingsRequest extends JsonRequest
{
  public function rules()
  {
    return [
      // 部署名稱
      'form.name.name' => 'required|string',
    ];
  }


  public function messages()
  {
    return [
      // 部署名稱
      'form.name.name.required' => 'Deployment name is required',
      'form.name.name.string' => 'Deployment name must be a string',
    ];
  }
}
