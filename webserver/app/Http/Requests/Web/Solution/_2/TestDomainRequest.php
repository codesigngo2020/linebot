<?php

namespace App\Http\Requests\Web\Solution\_2;

use App\Http\Requests\JsonRequest;

class TestDomainRequest extends JsonRequest
{

  protected function validationData()
  {
    $domain = $this->input('domain');
    if(!(strpos($domain, 'http') === 0)){
      $this->merge(['domain' => 'http://'.$domain]);
    }
    return $this->all();
  }

  public function rules()
  {
    return [
      'domain' => 'required|url',
      'key' => 'required|string',
    ];
  }

  public function messages()
  {
    return [
      'domain.required' => 'Domain is required',
      'domain.url' => 'The domain format must be an url',
      'key.required' => 'Key is required',
      'key.string' => 'Key must be a string',
    ];
  }

}
