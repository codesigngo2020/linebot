<?php

namespace App\Http\Requests\Web\Solution\_2;

use App\Http\Requests\JsonRequest;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Plan;

// Service
use App\Services\Solution\_2\WordprssService;
use App\Services\Solution\_2\InstagramService;


class StoreRequest extends JsonRequest
{
  public function rules()
  {
    $plans = Solution::find(2)->plans;
    $plan = Plan::find($this->input('form.1.plan'));

    return [
      // 付費方案
      'form.1.plan' => 'required|in:'.$plans->implode('id', ','),

      // 部署名稱
      'form.2.name.name' => 'required|string',

      // wordpress domain
      'form.2.domain.domain' => 'required|string',

      // wordpress 金鑰
      'form.2.key.key' => 'required|string',

      // instagram 授權
      #'form.3.clientId.clientId' => 'required|string',
      #'form.3.clientSecret.clientSecret' => 'required|string',

      // 爬蟲數
      'form.3.crawlers' => 'required|array|min:1|max:'.$plan->data->crawlers_count,

      // 爬蟲類型
      'form.3.crawlers.*.crawler_type.value.value' => 'required|in:user',

      // instagram 帳號
      'form.3.crawlers.*.crawler.username.username' => 'required|string',

      // 執行期間
      'form.3.crawlers.*.crawler.duration.duration' => [
        'required',
        'regex:/^[0-2][0-9] : [0-6][0-9] - [0-2][0-9] : [0-6][0-9]$/u',
      ],

      // 爬蟲頻率
      'form.3.crawlers.*.crawler.frequency.value' => 'required_with:form.3.crawlers.*|integer|min:1|max:60',

      // 文章上限數
      'form.3.crawlers.*.crawler.postsLimit.value' => 'required_with:form.3.crawlers.*|integer|min:1|max:'.$plan->data->posts_count,

      // 文章格式
      'form.3.crawlers.*.crawler.post.format.content' => 'nullable|string',

      // 文章類別
      'form.3.crawlers.*.crawler.post.category.value' => 'nullable|array|max:5',
      'form.3.crawlers.*.crawler.post.category.value.*' => 'required|string',

      // 文章標籤
      'form.3.crawlers.*.crawler.post.tag.value' => 'nullable|array|max:5',
      'form.3.crawlers.*.crawler.post.tag.value.*' => 'required|string',

      // 其他文章設定
      'form.3.crawlers.*.crawler.post.options.value' => 'nullable|array|in:link-active,close-comment,remove-tags-from-post,include-origin-tags,publish-time-as-post-time',

      // 文章刊登時間
      'form.4.duration.duration' => [
        'required',
        'regex:/^[0-2][0-9] : [0-6][0-9] - [0-2][0-9] : [0-6][0-9]$/u',
      ],

      // 文章執行頻率
      'form.4.postFrequency.value' => 'required|integer|min:1|max:360',

      // 文章刊登順序
      'form.4.order.order' => 'required|array',

    ];
  }


  public function messages()
  {
    $plan = Plan::find($this->input('form.1.plan'));

    return [
      // 付費方案
      'form.1.plan.required' => 'Plan is required',
      'form.1.plan.in' => 'Plan is invalid',

      // 部署名稱
      'form.2.name.name.required' => 'Deployment name is required',
      'form.2.name.name.string' => 'Deployment name must be a string',

      // wordpress domain
      'form.2.domain.domain.required' => 'Website domain is required',
      'form.2.domain.domain.string' => 'Website domain must be a string',

      // wordpress 金鑰
      'form.2.key.key.required' => 'Wordpress plugin key is required',
      'form.2.key.key.string' => 'Wordpress plugin key must be a string',

      // instagram client id
      #'form.3.clientId.clientId.required' => 'Client id is required',
      #'form.3.clientId.clientId.string' => 'Client id must be a string',

      #'form.3.clientSecret.clientSecret.required' => 'Client secret is required',
      #'form.3.clientSecret.clientSecret.string' => 'Client secret must be a string',

      // 爬蟲數
      'form.3.crawlers.required' => 'Crawlers is required',
      'form.3.crawlers.array' => 'Crawlers must be an array',
      'form.3.crawlers.min' => 'One crawler at least',
      'form.3.crawlers.max' => 'Maximun of crawlers is '.$plan->data->crawlers_count,

      // 爬蟲類型
      'form.3.crawlers.*.crawler_type.value.value.required' => 'Crawler type is required',
      'form.3.crawlers.*.crawler_type.value.value.in' => 'Crawler type must be in user',

      // instagram 帳號
      'form.3.crawlers.*.crawler.username.username.required' => 'Instagram user name is required',
      'form.3.crawlers.*.crawler.username.username.string' => 'Instagram user name be a string',

      // 執行期間
      'form.3.crawlers.*.crawler.duration.duration.required' => 'Duration is required',
      'form.3.crawlers.*.crawler.duration.duration.regex' => 'Duration format is invalid',

      // 爬蟲頻率
      'form.3.crawlers.*.crawler.frequency.value.required_with' => 'Frequency is required',
      'form.3.crawlers.*.crawler.frequency.value.integer' => 'Frequency must be an integer',
      'form.3.crawlers.*.crawler.frequency.value.min' => 'Minimun frequency is once per minute',
      'form.3.crawlers.*.crawler.frequency.value.max' => 'Maximun frequency is once per 60 minutes',

      // 文章上限數
      'form.3.crawlers.*.crawler.postsLimit.value.required_with' => 'Posts limit is required',
      'form.3.crawlers.*.crawler.postsLimit.value.integer' => 'Posts limit must be an integer',
      'form.3.crawlers.*.crawler.postsLimit.value.min' => 'Miximun posts limit is 1',
      'form.3.crawlers.*.crawler.postsLimit.value.max' => 'Maximun posts limit is '.$plan->data->posts_count,

      // 文章格式
      'form.3.crawlers.*.crawler.post.format.content.string' => 'Post format must be a string',

      // 文章類別
      'form.3.crawlers.*.crawler.post.category.value.array' => 'Categories must be an array',
      'form.3.crawlers.*.crawler.post.category.value.max' => 'Limit of categories is 5',

      'form.3.crawlers.*.crawler.post.category.value.*.required' => 'Category is required',
      'form.3.crawlers.*.crawler.post.category.value.*.string' => 'Category must be an string',

      // 文章標籤
      'form.3.crawlers.*.crawler.post.tag.value.array' => 'Tags must be an array',
      'form.3.crawlers.*.crawler.post.tag.value.max' => 'Limit of tags is 5',

      'form.3.crawlers.*.crawler.post.tag.value.*.required' => 'Tag is required',
      'form.3.crawlers.*.crawler.post.tag.value.*.string' => 'Tag must be an string',

      // 其他文章設定
      'form.3.crawlers.*.crawler.post.options.value.array' => 'Post options must be an array',
      'form.3.crawlers.*.crawler.post.options.value.in' => 'Something wrong in post options',

      // 文章刊登時間
      'form.4.duration.duration.required' => 'Duration is required',
      'form.4.duration.duration.regex' => 'Duration format is invalid',

      // 文章執行頻率
      'form.4.postFrequency.value.required' => 'Frequency is required',
      'form.4.postFrequency.value.integer' => 'Frequency must be an integer',
      'form.4.postFrequency.frequency.value.min' => 'Minimun frequency is once per minute',
      'form.4.postFrequency.frequency.value.max' => 'Maximun frequency is once per 60 minutes',

      // 文章刊登順序
      'form.4.order.order.required' => 'Posting order is required',
      'form.4.order.order.array' => 'Posting order must be an array',
    ];
  }


  public function withValidator($validator)
  {
    $validator->after(function($validator){

      // 驗證免費方案用過了沒
      $plan = $this->input('form.1.plan');
      if(!$validator->errors()->first('form.1.plan') && $plan == 1){
        $count = auth()->user()->deployments()
        ->where('solution_id', 2)
        ->where('plan_id', 1)
        ->count();

        if($count > 0) $validator->errors()->add('form.1.plan', 'You only can use free plan once');
      }

      // 爬蟲數驗證
      if(!$validator->errors()->first('form.1.plan') && !$validator->errors()->first('form.3.crawlers')){
        $plan = Plan::find($this->input('form.1.plan'));
        $count = count($this->input('form.3.crawlers'));
        $limit_count = $plan->data->crawlers_count;
        if($count > $limit_count) $validator->errors()->add('form.3.crawlers', 'Limit of crawlers is '.$limit_count);
      }



      // 驗證網站連線，domain and key驗證過了，才驗證測試
      if(!$validator->errors()->first('form.2.domain.domain') && !$validator->errors()->first('form.2.key.key')){
        $wordprssService = new WordprssService;
        $domain = $this->input('form.2.domain.domain');
        $key = $this->input('form.2.key.key');
        $status = $wordprssService->associate($domain, $key);
        if(!$status){
          $validator->errors()->add('form.2.domain.domain', 'The domain or the key is wrong');
          $validator->errors()->add('form.2.key.key', 'The domain or the key is wrong');
        }
      }

      // 驗證執行時間
      for($i = 1; $i <= 10; $i ++){
        $duration = $this->input('form.3.crawlers.'.$i.'.crawler.duration.duration');
        if($duration && !$validator->errors()->first('form.3.crawlers.'.$i.'.crawler.duration.duration')){
          $duration = explode(' - ', $duration);

          $is_error = false;
          for($j = 0; $j < 2; $j ++){
            if(!$is_error){
              ${'time_'.$j} = str_replace(' ', '', $duration[$j]);
              ${'time_'.$j.'_ex'} = explode(':', ${'time_'.$j});

              if(${'time_'.$j.'_ex'}[0] > 23 || ${'time_'.$j.'_ex'}[1] > 60){
                $validator->errors()->add('form.3.crawlers.'.$i.'.crawler.duration.duration', 'Duration format is invalid');
                $is_error = true;
                break;
              }
            }
          }
          if(!$is_error && strtotime($time_0) >= strtotime($time_1)) $validator->errors()->add('form.3.crawlers.'.$i.'.crawler.duration.duration', 'Duration format is invalid');
          continue;
        }
        break;
      }


      // 驗證文章刊登順序
      if(!$validator->errors()->first('form.3.crawlers') && !$validator->errors()->first('form.4.order.order')){
        $crawlers = $this->input('form.3.crawlers');

        if(count($this->input('form.4.order.order')) != count($crawlers)){
          $validator->errors()->add('form.4.order.order', 'Posting order items number must equal to cralwers number');
        }elseif(collect($crawlers)->pluck('id')->sort()->values() != collect($this->input('form.4.order.order'))->pluck('id')->sort()->values()){
          $validator->errors()->add('form.4.order.order', 'Posting order items must match cralwers');
        }
      }






      // 驗證instagram授權
      #if(!$validator->errors()->first('form.3.clientId.clientId') && !$validator->errors()->first('form.3.clientSecret.clientSecret')){
      #  $accessToken = $this->input('form.3.accessToken.accessToken');
      #  if(!$accessToken || $accessToken == ''){
      #    $validator->errors()->add('form.3.accessToken.accessToken', 'Please click button to authorize client id and secret');
      #  }else{
      #    $instagramService = new InstagramService;
      #    $status = $instagramService->validateAccessToken($accessToken);
      #    if(!$status){
      #      $validator->errors()->add('form.3.accessToken.accessToken', 'Authorization failure');
      #    }
      #  }
      #}

    });
  }



}
