<?php

namespace App\Http\Requests\Web\Solution\_2\Deployment\Crawler;

use App\Http\Requests\JsonRequest;

class ActiveRequest extends JsonRequest
{
  public function rules()
  {
    return [
      'active' => 'required|boolean'
    ];
  }

}
