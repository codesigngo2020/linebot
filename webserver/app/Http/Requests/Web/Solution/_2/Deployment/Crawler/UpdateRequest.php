<?php

namespace App\Http\Requests\Web\Solution\_2\Deployment\Crawler;

use App\Http\Requests\JsonRequest;

class UpdateRequest extends JsonRequest
{
  public function rules()
  {
    $plan = $this->deployment->plan;
    return [
      // 爬蟲數
      'crawlers' => 'required|array|min:1|max:5',

      // 爬蟲類型
      'crawlers.*.crawler_type.value.value' => 'required|in:user',

      // instagram 帳號
      'crawlers.*.crawler.username.username' => 'required|string',

      // 執行期間
      'crawlers.*.crawler.duration.duration' => [
        'required',
        'regex:/^[0-2][0-9] : [0-6][0-9] - [0-2][0-9] : [0-6][0-9]$/u',
      ],

      // 爬蟲頻率
      'crawlers.*.crawler.frequency.value' => 'required_with:crawlers.*|integer|min:1|max:60',

      // 文章上限數
      'crawlers.*.crawler.postsLimit.value' => 'required_with:crawlers.*|integer|min:1|max:'.$plan->data->posts_count,

      // 文章格式
      'crawlers.*.crawler.post.format.content' => 'nullable|string',

      // 文章類別
      'crawlers.*.crawler.post.category.value' => 'nullable|array|max:5',
      'crawlers.*.crawler.post.category.value.*' => 'required|string',

      // 文章標籤
      'crawlers.*.crawler.post.tag.value' => 'nullable|array|max:5',
      'crawlers.*.crawler.post.tag.value.*' => 'required|string',

      // 其他文章設定
      'crawlers.*.crawler.post.options.value' => 'nullable|array|in:link-active,close-comment,remove-tags-from-post,include-origin-tags,publish-time-as-post-time',
    ];
  }


  public function messages()
  {
    $plan = $this->deployment->plan;
    return [
      // 爬蟲數
      'crawlers.required' => 'Crawlers is required',
      'crawlers.array' => 'Crawlers must be an array',
      'crawlers.min' => 'One crawler at least',

      // 爬蟲類型
      'crawlers.*.crawler_type.value.value.required' => 'Crawler type is required',
      'crawlers.*.crawler_type.value.value.in' => 'Crawler type must be in user',

      // instagram 帳號
      'crawlers.*.crawler.username.username.required' => 'Instagram user name is required',
      'crawlers.*.crawler.username.username.string' => 'Instagram user name be a string',

      // 執行期間
      'crawlers.*.crawler.duration.duration.required' => 'Duration is required',
      'crawlers.*.crawler.duration.duration.regex' => 'Duration format is invalid',

      // 爬蟲頻率
      'crawlers.*.crawler.frequency.value.required_with' => 'Frequency is required',
      'crawlers.*.crawler.frequency.value.integer' => 'Frequency must be an integer',
      'crawlers.*.crawler.frequency.value.min' => 'Minimun frequency is once per minute',
      'crawlers.*.crawler.frequency.value.max' => 'Maximun frequency is once per 60 minutes',

      // 文章上限數
      'crawlers.*.crawler.postsLimit.value.required_with' => 'Posts limit is required',
      'crawlers.*.crawler.postsLimit.value.integer' => 'Posts limit must be an integer',
      'crawlers.*.crawler.postsLimit.value.min' => 'Miximun posts limit is 1',
      'crawlers.*.crawler.postsLimit.value.max' => 'Maximun posts limit is '.$plan->data->posts_count,

      // 文章格式
      'crawlers.*.crawler.post.format.content.string' => 'Post format must be a string',

      // 文章類別
      'crawlers.*.crawler.post.category.value.array' => 'Categories must be an array',
      'crawlers.*.crawler.post.category.value.max' => 'Limit of categories is 5',

      'crawlers.*.crawler.post.category.value.*.required' => 'Category is required',
      'crawlers.*.crawler.post.category.value.*.string' => 'Category must be an string',

      // 文章標籤
      'crawlers.*.crawler.post.tag.value.array' => 'Tags must be an array',
      'crawlers.*.crawler.post.tag.value.max' => 'Limit of tags is 5',

      'crawlers.*.crawler.post.tag.value.*.required' => 'Tag is required',
      'crawlers.*.crawler.post.tag.value.*.string' => 'Tag must be an string',

      // 其他文章設定
      'crawlers.*.crawler.post.options.value.array' => 'Post options must be an array',
      'crawlers.*.crawler.post.options.value.in' => 'Something wrong in post options',
    ];
  }


  public function withValidator($validator)
  {
    // 驗證執行時間
    for($i = 1; $i <= 10; $i ++){
      $duration = $this->input('crawlers.'.$i.'.crawler.duration.duration');
      if($duration && !$validator->errors()->first('crawlers.'.$i.'.crawler.duration.duration')){
        $duration = explode(' - ', $duration);

        $is_error = false;
        for($j = 0; $j < 2; $j ++){
          if(!$is_error){
            ${'time_'.$j} = str_replace(' ', '', $duration[$j]);
            ${'time_'.$j.'_ex'} = explode(':', ${'time_'.$j});

            if(${'time_'.$j.'_ex'}[0] > 23 || ${'time_'.$j.'_ex'}[1] > 60){
              $validator->errors()->add('crawlers.'.$i.'.crawler.duration.duration', 'Duration format is invalid');
              $is_error = true;
              break;
            }
          }
        }
        if(!$is_error && strtotime($time_0) >= strtotime($time_1)) $validator->errors()->add('crawlers.'.$i.'.crawler.duration.duration', 'Duration format is invalid');
        continue;
      }
      break;
    }
  }



}
