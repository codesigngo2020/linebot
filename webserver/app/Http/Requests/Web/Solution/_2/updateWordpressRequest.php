<?php

namespace App\Http\Requests\Web\Solution\_2;

use App\Http\Requests\JsonRequest;

// Service
use App\Services\Solution\_2\WordprssService;

class updateWordpressRequest extends JsonRequest
{
  public function rules()
  {
    return [
      // wordpress domain
      'form.1.domain.domain' => 'required|string',

      // wordpress 金鑰
      'form.1.key.key' => 'required|string',

      // 文章刊登時間
      'form.2.duration.duration' => [
        'required',
        'regex:/^[0-2][0-9] : [0-6][0-9] - [0-2][0-9] : [0-6][0-9]$/u',
      ],

      // 文章執行頻率
      'form.2.postFrequency.value' => 'required|integer|min:1|max:360',

      // 文章刊登順序
      'form.2.order.order' => 'required|array',
    ];
  }


  public function messages()
  {
    return [
      // wordpress domain
      'form.1.domain.domain.required' => 'Website domain is required',
      'form.1.domain.domain.string' => 'Website domain must be a string',

      // wordpress 金鑰
      'form.1.key.key.required' => 'Wordpress plugin key is required',
      'form.1.key.key.string' => 'Wordpress plugin key must be a string',

      // 文章刊登時間
      'form.2.duration.duration.required' => 'Duration is required',
      'form.2.duration.duration.regex' => 'Duration format is invalid',

      // 文章執行頻率
      'form.2.postFrequency.value.required' => 'Frequency is required',
      'form.2.postFrequency.value.integer' => 'Frequency must be an integer',
      'form.2.postFrequency.frequency.value.min' => 'Minimun frequency is once per minute',
      'form.2.postFrequency.frequency.value.max' => 'Maximun frequency is once per 60 minutes',

      // 文章刊登順序
      'form.2.order.order.required' => 'Posting order is required',
      'form.2.order.order.array' => 'Posting order must be an array',
    ];
  }


  public function withValidator($validator)
  {
    $validator->after(function($validator){

      // 驗證網站連線，domain and key驗證過了，才驗證測試
      if(!$validator->errors()->first('form.1.domain.domain') && !$validator->errors()->first('form.1.key.key')){
        $wordprssService = new WordprssService;
        $domain = $this->input('form.1.domain.domain');
        $key = $this->input('form.1.key.key');
        $status = $wordprssService->associate($domain, $key);
        if(!$status){
          $validator->errors()->add('form.1.domain.domain', 'The domain or the key is wrong');
          $validator->errors()->add('form.1.key.key', 'The domain or the key is wrong');
        }
      }


      // 驗證文章刊登順序
      if(!$validator->errors()->first('form.2.order.order')){
        $crawlers = $this->deployment->solution_2_crawlers()->where('status', '<>', 'destroyed')->get();

        if(count($this->input('form.2.order.order')) != $crawlers->count('id')){
          $validator->errors()->add('form.2.order.order', 'Posting order items number must equal to cralwers number');
        }elseif(collect($crawlers)->pluck('id')->sort()->values() != collect($this->input('form.2.order.order'))->pluck('id')->sort()->values()){
          $validator->errors()->add('form.2.order.order', 'Posting order items must match cralwers');
        }
      }

    });

  }



}
