<?php

namespace App\Http\Requests\Web\Solution\_1;

use App\Http\Requests\JsonRequest;

class TestKeyRequest extends JsonRequest
{

  public function rules()
  {
    return [
      'key' => 'required|string',
    ];
  }

  public function messages()
  {
    return [
      'key.required' => 'Key is required',
      'key.string' => 'The key must be a string',
    ];
  }

}
