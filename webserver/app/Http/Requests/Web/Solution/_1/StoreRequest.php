<?php

namespace App\Http\Requests\Web\Solution\_1;

use App\Http\Requests\JsonRequest;

// Service
use App\Services\Solution\_1\WordprssService;

// Helper
use App\Helpers\Solution\_1\YoutubeRequestHelper;

class StoreRequest extends JsonRequest
{

  public function rules()
  {
    return [
      // 部署名稱
      'form.1.name.name' => 'required|string',

      // wordpress domain
      'form.1.domain.domain' => 'required|string',

      // wordpress 金鑰
      'form.1.key.key' => 'required|string',

      // youtube 金鑰
      'form.2.keys' => 'required|array|min:1|max:5',
      'form.2.keys.*.key.key' => 'required|string',

      // 爬蟲數
      'form.3.crawlers' => 'required|array|min:1|max:5',

      // 爬蟲類型
      'form.3.crawlers.*.crawler_type.value.value' => 'required|in:channel,list',

      // channel id & list id
      'form.3.crawlers.*.crawler.channel.id' => 'required_if:form.3.crawlers.*.crawler_type.value.value,channel,list|string',
      'form.3.crawlers.*.crawler.list.id' => 'required_if:form.3.crawlers.*.crawler_type.value.value,list|string',

      // 文章格式
      'form.3.crawlers.*.crawler.post.format.content' => 'nullable|string',

      // 文章類別
      'form.3.crawlers.*.crawler.post.category.value' => 'nullable|array|max:5',
      'form.3.crawlers.*.crawler.post.category.value.*' => 'required|string',

      // 文章標籤
      'form.3.crawlers.*.crawler.post.tag.value' => 'nullable|array|max:5',
      'form.3.crawlers.*.crawler.post.tag.value.*' => 'required|string',

      // 爬蟲頻率
      'form.3.crawlers.*.crawler.post.frequency.value' => 'required_if:form.3.crawlers.*.crawler_type.value.value,channel,list|integer|min:1|max:5',

      // 其他文章設定
      'form.3.crawlers.*.crawler.post.options.value' => 'nullable|array|in:link-active,close-comment,include-origin-tags,publish-time-as-post-time',
    ];
  }


  public function messages()
  {
    return [
      // 部署名稱
      'form.1.name.name.required' => 'Deployment name is required',
      'form.1.name.name.string' => 'Deployment name must be a string',

      // wordpress domain
      'form.1.domain.domain.required' => 'Website domain is required',
      'form.1.domain.domain.string' => 'Website domain must be a string',

      // wordpress 金鑰
      'form.1.key.key.required' => 'Wordpress plugin key is required',
      'form.1.key.key.string' => 'Wordpress plugin key must be a string',

      // youtube 金鑰
      'form.2.keys.required' => 'Youtube keys is required',
      'form.2.keys.array' => 'Youtube keys must be an array',
      'form.2.keys.min' => 'One youtube key at least',
      'form.2.keys.max' => 'Limit of youtube keys is 5',

      'form.2.keys.*.key.key.required' => 'Youtube key is required',
      'form.2.keys.*.key.key.string' => 'Youtube key must be a string',

      // 爬蟲數
      'form.3.crawlers.required' => 'Crawlers is required',
      'form.3.crawlers.array' => 'Crawlers must be an array',
      'form.3.crawlers.min' => 'One crawler at least',
      'form.3.crawlers.max' => 'Limit of crawlers is 5',

      // 爬蟲類型
      'form.3.crawlers.*.crawler_type.value.value.required' => 'Crawler type is required',
      'form.3.crawlers.*.crawler_type.value.value.in' => 'Crawler type must be in channel and list',

      // channel id & list id
      'form.3.crawlers.*.crawler.channel.id.required_if' => 'Channel id is required',
      'form.3.crawlers.*.crawler.channel.id.string' => 'Channel id must be a string',

      'form.3.crawlers.*.crawler.list.id.required_if' => 'List id is required',
      'form.3.crawlers.*.crawler.list.id.string' => 'List id must be a string',

      // 文章格式
      'form.3.crawlers.*.crawler.post.format.content.string' => 'Post format must be a string',

      // 文章類別
      'form.3.crawlers.*.crawler.post.category.value.array' => 'Categories must be an array',
      'form.3.crawlers.*.crawler.post.category.value.max' => 'Limit of categories is 5',

      'form.3.crawlers.*.crawler.post.category.value.*.required' => 'Category is required',
      'form.3.crawlers.*.crawler.post.category.value.*.string' => 'Category must be an string',

      // 文章標籤
      'form.3.crawlers.*.crawler.post.tag.value.array' => 'Tags must be an array',
      'form.3.crawlers.*.crawler.post.tag.value.max' => 'Limit of tags is 5',

      'form.3.crawlers.*.crawler.post.tag.value.*.required' => 'Tag is required',
      'form.3.crawlers.*.crawler.post.tag.value.*.string' => 'Tag must be an string',

      // 爬蟲頻率
      'form.3.crawlers.*.crawler.post.frequency.value.required_if' => 'Frequency is required',
      'form.3.crawlers.*.crawler.post.frequency.value.integer' => 'Frequency must be an integer',
      'form.3.crawlers.*.crawler.post.frequency.value.min' => 'Maximun frequency is once per minute',
      'form.3.crawlers.*.crawler.post.frequency.value.max' => 'Minimun frequency is once per 60 minutes',

      // 其他文章設定
      'form.3.crawlers.*.crawler.post.options.value.array' => 'Post options must be an array',
      'form.3.crawlers.*.crawler.post.options.value.in' => 'Something wrong in post options',
    ];
  }


  public function withValidator($validator)
  {
    $validator->after(function($validator){

      // 驗證網站連線，domain and key驗證過了，才驗證測試
      if(!$validator->errors()->first('form.1.domain.domain') && !$validator->errors()->first('form.1.key.key')){
        $wordprssService = new WordprssService;
        $domain = $this->input('form.1.domain.domain');
        $key = $this->input('form.1.key.key');
        $status = $wordprssService->associate($domain, $key);
        if(!$status){
          $validator->errors()->add('form.1.domain.domain', 'The domain or the key is wrong');
          $validator->errors()->add('form.1.key.key', 'The domain or the key is wrong');
        }
      }

      // 驗證youtube金鑰
      for($i = 1; $i <= 5; $i ++){
        $key = $this->input('form.2.keys.'.$i.'.key.key');
        if($key && !$validator->errors()->first('form.2.keys.'.$i.'.key.key')){
          $youtubeRequestHelper = new YoutubeRequestHelper;
          $status = $youtubeRequestHelper->testKey($key);// success or error
          if($status != 'success') $validator->errors()->add('form.2.keys.'.$i.'.key.key', 'The key is invalid');
        }else{
          break;
        }
      }

    });
  }


}
