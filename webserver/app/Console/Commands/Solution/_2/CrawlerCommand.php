<?php
namespace App\Console\Commands\Solution\_2;

use Illuminate\Console\Command;

// Service
use App\Services\Solution\_2\InstagramService;
use App\Services\Solution\_2\WordprssService;

class CrawlerCommand extends Command
{
  // 命令名稱
  protected $signature = 'solution2:crawler';

  // 說明文字
  protected $description = 'solution 2 crawler';

  public function __construct()
  {
    parent::__construct();
  }

  // Console 執行的程式
  public function handle()
  {
    $instagramService = new InstagramService;
    $instagramService->crawlAll();

    $wordprssService = new WordprssService;
    $wordprssService->createAll();
  }
}
