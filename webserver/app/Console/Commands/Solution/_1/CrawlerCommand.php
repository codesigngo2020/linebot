<?php
namespace App\Console\Commands\Solution\_1;

use Illuminate\Console\Command;

// Service
use App\Services\Solution\_1\YoutubeService;
use App\Services\Solution\_1\WordprssService;

class CrawlerCommand extends Command
{
  // 命令名稱
  protected $signature = 'solution1:crawler';

  // 說明文字
  protected $description = 'solution 1 crawler';

  public function __construct()
  {
    parent::__construct();
  }

  // Console 執行的程式
  public function handle()
  {
    $youtubeService = new YoutubeService;
    $youtubeService->crawlAll();

    $wordprssService = new WordprssService;
    $wordprssService->createAll();
  }
}
