<?php

namespace App\Repositories\Platform\Solution\_2;

use Carbon\Carbon;

// Model
use App\Models\Platform\Solution\_2\Crawler;

class LogRepository
{
  public function countRequestsInSpecificDays(Crawler $crawler, $day)
  {
    $start = Carbon::today()->subDays($day-1)->toDateTimeString();

    return $crawler->logs()
    ->selectRaw("count(logs.id) count, DATE_FORMAT(created_at, '%m/%d') date")
    ->where(function($q){
      $q->where('type', 'crawl-success')
      ->orWhere('type', 'crawl-fail');
    })
    ->whereDate('created_at', '>=', $start)
    ->groupBy('date')
    ->get();
  }

  public function countPostsInSpecificDays(Crawler $crawler, $day)
  {
    $start = Carbon::today()->subDays($day-1)->toDateTimeString();

    return $crawler->logs()
    ->selectRaw("count(logs.id) count, DATE_FORMAT(created_at, '%m/%d') date")
    ->where('type', 'post-success')
    ->whereDate('created_at', '>=', $start)
    ->groupBy('date')
    ->get();
  }
}
