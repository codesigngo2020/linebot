<?php

namespace App\Repositories\Platform\Solution\_2;

use Carbon\Carbon;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_2\Crawler;

class CrawlerRepository
{
  public function findAllByDeploymentWithAccountAndUncrawelAndAllPostsCount(Deployment $deployment)
  {
    return Crawler::whereHas('deployment', function($q) use($deployment){
      $q->where('id', $deployment->id);
    })
    ->where('status', '<>', 'destroyed')
    ->with('account')
    ->withCount('posts')
    ->withCount('uncrawl_posts')
    ->get();
  }

}
