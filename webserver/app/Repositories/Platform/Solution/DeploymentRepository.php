<?php

namespace App\Repositories\Platform\Solution;

//Model
use App\Models\Platform\Solution\Solution;

class DeploymentRepository
{

    public function findAllByUserAndSolutionWithPagination(Solution $solution)
    {
        return auth()->user()->deployments()
        ->where('solution_id', $solution->id)
        ->paginate(10);
    }


    // solution 1

    public function findAllByUserAndSolution1WithCrawlersCountAndPagination(Solution $solution)
    {
        return auth()->user()->deployments()
        ->where('solution_id', $solution->id)
        ->withCount('solution_1_crawlers')
        ->paginate(10);
    }

    // solution 2

    public function findAllByUserAndSolution2WithCrawlersCountAndPagination(Solution $solution)
    {
        return auth()->user()->deployments()
        ->where('solution_id', $solution->id)
        ->withCount(['solution_2_crawlers' => function($q){
            $q->where('status', '<>', 'destroyed');
        }])
        ->paginate(10);
    }

    // solution 3

    public function findAllByUserAndSolution3WithAccount(Solution $solution)
    {
        return auth()->user()->deployments()
        ->where('solution_id', $solution->id)
        ->with('solution_3_account')
        ->paginate(10);
    }

    // solution 4

    public function findAllByUserAndSolution4(Solution $solution)
    {
        return auth()->user()->deployments()
        ->where('solution_id', $solution->id)
        ->paginate(10);
    }
}
