<?php

namespace App\Repositories\Platform\Solution\_1;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_1\Crawler;

class CrawlerRepository
{
  public function findAllByDeploymentWithUncrawelAndAllPostsCount(Deployment $deployment)
  {
    return Crawler::whereHas('deployment', function($q) use($deployment){
      $q->where('id', $deployment->id);
    })
    ->withCount('posts')
    ->withCount('uncrawl_posts')
    ->get();
  }

}
