<?php

namespace App\Repositories\Platform\Solution\_1;

// Model
use App\Models\Platform\Solution\_1\Crawler;

class PostRepository
{
  public function findAllByCrawlerWithVideo(Crawler $crawler){
    return $crawler->posts()->with('video')->get();
  }
}
