<?php

namespace App\Repositories\Platform\Solution\_1;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_1\Key;

class KeyRepository
{
  public function findAllByDeploymeny(Deployment $deployment)
  {
    return Key::whereHas('crawlers', function($q) use($deployment){
      $q->whereHas('deployment', function($q) use($deployment){
        $q->where('id', $deployment->id);
      });
    })
    ->get();
  }


}
