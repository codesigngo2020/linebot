<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Form;

class FormMacroServiceProvider extends ServiceProvider
{
  /**
  * Bootstrap any application services.
  *
  * @return void
  */
  public function boot()
  {
    Form::macro('requiredLabel', function($name, $value){
      return '<label for='.$name.'>'.$value.'<span class="form-required">*</span></label>';
    });

    Form::macro('invalidFeedback', function($info){
      return '<span class="invalid-feedback" role="alert"><strong>'.$info.'</strong></span>';
    });

    Form::macro('ajaxInvalidFeedback', function($info_ref){
      return '<span class="invalid-feedback" role="alert"><strong>{{ '.$info_ref.' }}</strong></span>';
    });

    Form::macro('imageDropzone', function($options){
      return'<div id="'.$options['dropzone']['id'].'" class="'.$options['dropzone']['class'].'"><div id="dz-preview" class="dz-preview dz-preview-single"><div class="dz-preview-cover"><img id="preview-img" class="dz-preview-img" src="'.$options['image'].'" alt="..." data-dz-thumbnail/></div></div><div class="dz-default dz-message"><span>Drop files here to upload</span></div></div>';
    });

    Form::macro('selectWithArray', function($name, $collection, $keys, $value, $options){
      $collection = $collection->keyBy('id')
      ->map(function($item, $key){
        return $item->title;
      });

      return Form::select($name, $collection, $value, $options);
    });
  }

  /**
  * Register any application services.
  *
  * @return void
  */
  public function register()
  {

  }
}
