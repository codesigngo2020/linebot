<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Relations\Relation;

//Model
use App\Models\Platform\User;
use App\Models\Platform\Image;
use App\Models\Platform\File;
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Deployment;

// Observer
use App\Observers\Platform\Solution\DeploymentObserver;

class AppServiceProvider extends ServiceProvider
{
  /**
  * Register any application services.
  *
  * @return void
  */
  public function register()
  {
    Relation::morphMap([
      'User' => User::class,
      'Image' => Image::class,
      'File' => File::class,
      'Solution' => Solution::class,
      'Deployment' => Deployment::class,
    ]);
  }

  /**
  * Bootstrap any application services.
  *
  * @return void
  */
  public function boot()
  {
    Schema::defaultStringLength(191);

    // Deployment::observe(DeploymentObserver::class);
  }
}
