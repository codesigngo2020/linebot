<?php
namespace App\Helpers\Basic\Wordpress;

use App\Helpers\Basic\Request\RequestTrait;

class CreatePostHelper
{

  use RequestTrait;

  /* ========== wordpress 建立文章 ========== */

  // wordpress 建立文章 - 設置爬蟲以及相關資料
  public function setCrawler($crawler)
  {
    $this->crawler = $crawler;
    $deployment = $crawler->deployment;
    $this->domain = $deployment->data->domain;
    $this->key = $deployment->data->key;
    return $this;
  }

  // wordpress 建立文章 - 設置影片資料
  public function setVideoData($video_id, $video_data)
  {
    $options = $this->crawler->options;
    $data = [
      'title' => $video_data->title,
      'categories' => $this->crawler->categories,
      'comment_status' => ($options && in_array('close-comment', $options)) ? 'closed' : 'open',
    ];

    $content = $video_data->description;

    // 文章連結啟用
    if($options && in_array('link-active', $options)){
      $content = preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@u', '<a href="$1" target="_blank">$0</a>', $video_data->description);
    }

    // 設定video format
    if($this->crawler->format){
      $content = 'a';
    }else{
      $content = '<div class="i-youtube-player"><iframe src="https://www.youtube.com/embed/'.$video_id.'?autoplay=1&amodestbranding=1" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div><div class="i-youtube-desc">'.$content.'</div>';
      }
      $data['content'] = $content;

      // 設定tags
      if($options && in_array('include-video-tags', $options) && isset($video_data->tags)){
        $data['tags'] = array_merge($this->crawler->tags, $video_data->tags);
      }else{
        $data['tags'] = $this->crawler->tags;
      }

      // 設定文章建立時間 - youtube發布時間 or 現在時間
      if($options && in_array('publish-time-as-post-time', $options)){
        $data['created_at'] = Carbon::parse($video_data->publishedAt)->format('Y-m-d H:i:s');
      }else{
        $data['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
      }

      // 設定圖片抓取順序
      $thumbnails_order = collect(['maxres', 'standard', 'high', 'medium', 'default']);
      $thumbnails_order->each(function($type) use($video_data, &$data){
        if(isset($video_data->thumbnails->{$type})){
          $data['thumbnail'] = $video_data->thumbnails->{$type}->url;
          return false;
        }
      });
      $this->video_data = $data;
      return $this;
    }

    // post資料到wordpress建立文章
    public function createPost()
    {
      $requestUrl = url('http://'.$this->domain.'/wp-json/solution/v1/youtube-crawler/create-post');

      $options = [
        'form_params' => [
          'key' => $this->key,
          'video_data' => $this->video_data,
        ],
      ];

      $response = $this->request($requestUrl, 'POST', $options);
      dd($response);

      if($response['success']){
        $this->http_code = 200;
        $this->post_id = ($response['response']->status == 'success') ? $response['response']->post_id : false;
      }else{
        $this->http_code = $response['response']->getCode();
      }
      return $this;
    }

    public function getHttpCode()
    {
      return $this->http_code;
    }

    public function getPostId()
    {
      return $this->post_id;
    }

  }
