<?php
namespace App\Helpers\Basic\Paypal;

use Config;
use Carbon\Carbon;

// Paypal
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

use PayPal\Api\Agreement;
use PayPal\Api\AgreementStateDescriptor;
use PayPal\Api\Plan;
use PayPal\Api\Payer;
use PayPal\Exception\PayPalConnectionException;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Plan as _Plan;

class AgreementHelper
{
  public function __construct()
  {
    /* ========== PayPal api context ========== */

    $paypal_conf = Config::get('paypal');

    $this->_api_context = new ApiContext(new OAuthTokenCredential(
      $paypal_conf['sandbox_client_id'],
      $paypal_conf['sandbox_secret'])
    );
    $this->_api_context->setConfig($paypal_conf['settings']);
  }

  /* ========== 建立訂閱方案許可 ========== */

  public function createAgreement(Solution $solution, _Plan $_plan)
  {
    $agreement = new Agreement;
    $agreement->setName('BitTech Billing Agreement')
    ->setDescription('You are starting with solution #'.$solution->id.', plan #'.$_plan->id.'.')
    ->setStartDate(Carbon::now()->addSeconds(60)->toIso8601String());

    $plan = new Plan;
    $plan->setId($_plan->paypal_plan_id);
    $agreement->setPlan($plan);

    $payer = new Payer;
    $payer->setPaymentMethod('paypal');
    $agreement->setPayer($payer);

    try{
      $agreement = $agreement->create($this->_api_context);
      return $agreement->getApprovalLink();
    }catch(PayPalConnectionException $ex){
      return false;
    }
  }

  /* ========== 執行方案許可 ========== */

  public function execute($token)
  {
    $agreement = new Agreement;
    $agreement->execute($token, $this->_api_context);
    return $agreement;
  }

  /* ========== 取消許可 ========== */

  public function suspend($agreementId)
  {
    $agreementStateDescriptor = new AgreementStateDescriptor;
    $agreementStateDescriptor->setNote('Suspending the agreement');

    $agreement = Agreement::get($agreementId, $this->_api_context);
    return $agreement->suspend($agreementStateDescriptor, $this->_api_context);
  }

  /* ========== 取得交易資訊 ========== */

  public function transactions($agreementId)
  {
    $params = [
      'start_date' => date('Y-m-d', strtotime('-1 years')),
      'end_date' => date('Y-m-d'),
    ];

    return Agreement::searchTransactions($agreementId, $params, $this->_api_context);
  }
}
