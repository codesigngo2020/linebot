<?php
namespace App\Helpers\Basic\Paypal;

use Config;

// Paypal
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

use PayPal\Api\Currency;
use PayPal\Api\ChargeModel;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\Plan;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Plan as _Plan;

class PlanHelper
{
  public function __construct()
  {
    /* ========== PayPal api context ========== */

    $paypal_conf = Config::get('paypal');

    $this->_api_context = new ApiContext(new OAuthTokenCredential(
      $paypal_conf['sandbox_client_id'],
      $paypal_conf['sandbox_secret'])
    );
    $this->_api_context->setConfig($paypal_conf['settings']);
  }

  /* ========== 建立訂閱方案 ========== */

  public function createPlan(Solution $solution, _Plan $_plan)
  {
    $plan = new Plan;
    $plan->setName('Start solution '.$solution->id.' on BitTech')
    ->setDescription('You are starting with solution #'.$solution->id.', plan #'.$_plan->id.' and will pay '.$_plan->price.' USD per month.')
    ->setType('INFINITE');

    $paymentDefinition = new PaymentDefinition;
    $paymentDefinition->setName('Regular Payments')
    ->setType('REGULAR')
    ->setFrequency('Month')
    ->setFrequencyInterval('1')
    ->setCycles('0')
    ->setAmount(new Currency([
      'value' => $_plan->price,
      'currency' => 'USD',
    ]));

    $merchantPreferences = new MerchantPreferences;

    $merchantPreferences->setReturnUrl(route('paypal.agreement.execute', ['success' => true]))
    ->setCancelUrl(route('paypal.agreement.execute', ['success' => false]))
    ->setAutoBillAmount('YES')
    ->setInitialFailAmountAction('CONTINUE')
    ->setMaxFailAttempts('3')
    ->setSetupFee(new Currency([
      'value' => $_plan->price,
      'currency' => 'USD'
    ]));

    $plan->setPaymentDefinitions([$paymentDefinition]);
    $plan->setMerchantPreferences($merchantPreferences);

    try{
      return $plan->create($this->_api_context);
    }catch(Exception $ex){
      return false;
    }
  }

  /* ========== 啟用訂閱方案 ========== */

  public function activatePlan(_Plan $_plan)
  {
    $createdPlan = Plan::get($_plan->paypal_plan_id, $this->_api_context);
    if($createdPlan->state != 'ACTIVE'){
      $patch = new Patch;
      $value = new PayPalModel('{
        "state":"ACTIVE"
      }');

      $patch->setOp('replace')
      ->setPath('/')
      ->setValue($value);

      $patchRequest = new PatchRequest;
      $patchRequest->addPatch($patch);

      $createdPlan->update($patchRequest, $this->_api_context);
    }
    return $createdPlan;
  }


}
