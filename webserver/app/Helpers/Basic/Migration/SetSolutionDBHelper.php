<?php
namespace App\Helpers\Basic\Migration;

use Config;
use DB;
use PDO;
use PDOException;

// Model
use App\Models\Platform\Solution\Deployment;

class SetSolutionDBHelper
{
    public function setSolutionDB(Deployment $deployment)
    {
        
        foreach(['solution_4', 'deployment_'.$deployment->id] as $connection){
            Config::set('database.connections.'.$connection, [
                'driver' => env('DB_Solution_4_CONNECTION'),
                'host' => env('DB_Solution_4_HOST'),
                'port' => env('DB_Solution_4_PORT'),
                'database' => 'deployment_'.$deployment->id,
                'username' => env('DB_Solution_4_USERNAME'),
                'password' => env('DB_Solution_4_PASSWORD'),
                'unix_socket' => env('DB_SOCKET', ''),
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => false,
                'engine' => null,
                'options' => extension_loaded('pdo_mysql') ? array_filter([PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),]) : [],
            ]);
        }

        DB::reconnect('solution_4');
    }
}
