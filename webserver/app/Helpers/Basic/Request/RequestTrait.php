<?php
namespace App\Helpers\Basic\Request;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\RequestException;

trait RequestTrait
{
  /* ========== get response ========== */

  public function request($requestUrl, $method, $options = [])
  {
    $client = new Client;
    try{
      $response = $client->request($method, $requestUrl, $options)
      ->getBody()
      ->getContents();

      return [
        'success' => true,
        'response' => json_decode($response),
      ];
    }catch(RequestException $e){
      return [
        'success' => false,
        'response' => $e,
      ];
    }
  }

}
