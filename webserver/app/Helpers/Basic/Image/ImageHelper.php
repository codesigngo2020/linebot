<?php
namespace App\Helpers\Basic\Image;

use Carbon\Carbon;

// Model
use App\Models\Platform\Image;

class ImageHelper
{
    // 儲存圖片
    public function store($file, $path)
    {
        // 產生fileNmame
        $path = 'images/' . $path;
        $currentTime = Carbon::now()->timestamp;
        $fileName = $file->getClientOriginalName();
        $path_parts = $this->getPathParts($fileName);

        $fileName = $path_parts['filename'].'_'.$currentTime.'.'.$path_parts['extension'];

        // 儲存檔案
        $file->move(public_path($path), $fileName);

        return $fileName;
    }

    // 刪除圖片
    public function delete(Image $image)
    {
        // 如果圖片路徑還存在，刪除檔案
        if(preg_match('/images\/\S+/', $image->path, $match)){
            $path = public_path($match[0]);
            if(file_exists($path)) unlink($path);
        }
        return $image->delete();
    }


    /* ========== 路徑解析 ========== */

    public function getPathParts($path)
    {
        if (strpos($path, '/') === false) {
            $path_parts = pathinfo('a' . $path);
        } else {
            $path = str_replace('/', '/a', $path);
            $path_parts = pathinfo($path);
        }
        $explode = explode('.', $path);
        $extension = end($explode);

        return $path_parts = [
            'filename' => substr($path_parts["filename"], 1),
            'extension' => $extension
        ];
    }
}
