<?php
namespace App\Helpers\Solution\_2;

// Model
use App\Models\Platform\Solution\_2\Crawler;

// Helper
use App\Helpers\Basic\Request\RequestTrait;

class InstagramHelper
{

  use RequestTrait;

  /* ========== get access token ========== */

  #public function requestAccessToken($code)
  #{
  #  $requestUrl = 'https://api.instagram.com/oauth/access_token/';
  #  $form_params = [
  #    'client_id' => Session::get('solution2.clientId'),
  #    'client_secret' => Session::get('solution2.clientSecret'),
  #    'grant_type' => 'authorization_code',
  #    'redirect_uri' => route('solution.2.callback'),
  #    'code' => $code,
  #  ];
  #  $response = $this->getJsonResponseByPost($requestUrl, $form_params);
  #  return $response['success'] ? $response['response']->access_token : false;
  #}










  /* ========== validate access token ========== */

  #public function validateAccessToken($accessToken)
  #{
  #  $requestUrl = 'https://api.instagram.com/v1/users/self/';
  #  $query = [
  #    'access_token' => $accessToken,
  #  ];
  #  $response = $this->getJsonResponseByGet($requestUrl, $query);
  #  return $response['success'];
  #}










  /* ========== 定時爬取資料到資料庫 ========== */

  // 定時爬取資料到資料庫 - 設置爬蟲
  public function setCrawler(Crawler $crawler)
  {
    $this->crawler = $crawler;
    return $this;
  }

  // 定時爬取資料到資料庫 - 設置帳號ID
  public function setAccountId()
  {
    $account = $this->crawler->account;
    if(!$account->account_id || !$account->name){
      $response = $this->getAccountData($account->username);
      $account->update([
        'account_id' => $response['account_id'],
        'name' => $response['name'],
      ]);
    }
    $this->account_id = $account->account_id;
    return $this;
  }

  // 定時爬取資料到資料庫 - 取得所有應爬文章數
  public function setPostsCount()
  {
    $this->posts_count = $this->getPostsCount();
    return $this;
  }

  // 定時爬取資料到資料庫 - 取得所有已爬的文章數
  public function setCrawleredPostsCount()
  {
    $this->crawlered_posts_count = $this->crawler->posts()->count();
    return $this;
  }

  // 定時爬取資料到資料庫 - 把未爬的影片新增入資料庫
  public function pushToSchedule()
  {
    $crawler = $this->crawler;
    if($this->posts_count > $this->crawlered_posts_count){
      $media_list_data = $this->getMediaListData($crawler);
      $medias = collect($media_list_data['response']->data->user->edge_owner_to_timeline_media->edges);

      $instagram_ids = $crawler->posts->pluck('instagram_id');
      // 未爬的影片列入排程
      $medias->each(function($media, $key) use($crawler, $instagram_ids){
        $node = $media->node;
        if(!$instagram_ids->contains($node->id)){

          $data = [
            'publishedAt' => $node->taken_at_timestamp,
            'is_video' => $node->is_video,
            'display_url' => $node->display_url,
            'content' => isset($node->edge_media_to_caption->edges[0]) ? $node->edge_media_to_caption->edges[0]->node->text : '',
            'children' => [],
          ];
          if(isset($node->video_url)) $data['video_url'] = $node->video_url;

          if(isset($node->edge_sidecar_to_children)){
            collect($node->edge_sidecar_to_children->edges)->map(function($edge) use(&$data){
              $node = $edge->node;
              $child = [
                'is_video' => $node->is_video,
                'display_url' => $node->display_url,
              ];
              if(isset($node->video_url)) $child['video_url'] = $node->video_url;

              $data['children'][] = $child;
            });
          };

          $crawler->posts()->firstOrCreate([
            'instagram_id' => $node->id,
          ],[
            'shortcode' => $node->shortcode,
            'data' => $data,
            'status' => 'standBy',
          ]);
        }
      });

      $page_info = $media_list_data['response']->data->user->edge_owner_to_timeline_media->page_info;
      if($page_info->has_next_page){
        $crawler->update(['end_cursor' => $page_info->end_cursor]);
        $this->pushToSchedule();
      }else{
        $crawler->update(['end_cursor' => NULL]);
      }
    }

    $this->crawler->deployment->solution_2_logs()->create([
      'crawler_id' => $this->crawler->id,
      'type' => 'crawl-success',
      'class' => 'secondary',
      'message' => 'Crawler(#'.$this->crawler->id.') requests data from @'.$this->crawler->account->username.' successfully.',
    ]);

    return $this;
  }









  /* ========== Instagram帳號資料 ========== */

  public function getAccountData($username)
  {
    $requestUrl = 'https://www.instagram.com/'.$username.'?__a=1';

    $response = $this->request($requestUrl, 'GET');
    $user = $response['response']->graphql->user;
    return [
      'account_id' => $user->id,
      'name' => $user->full_name,
    ];
  }










  /* ========== Instagram文章數request ========== */

  public function getPostsCount()
  {
    $requestUrl = 'https://www.instagram.com/graphql/query';
    $options = [
      'query' => [
        'id' => $this->account_id,
        'first' => 0,
        'query_hash' => 'f2405b236d85e8296cf30347c9f08c2a',
      ]
    ];
    $response = $this->request($requestUrl, 'GET', $options);
    return $response['success'] ? $response['response']->data->user->edge_owner_to_timeline_media->count : 0;
  }










  /* ========== Instagram文章列表 request ========== */

  public function getMediaListData()
  {
    $requestUrl = 'https://www.instagram.com/graphql/query';

    $query = [
      'id' => $this->account_id,
      'first' => 20,
      'query_hash' => 'f2405b236d85e8296cf30347c9f08c2a',
    ];
    if($this->crawler->end_cursor) $query['after'] = $this->crawler->end_cursor;

    $domain = 'www.instagram.com';
    $cookies = [
      'sessionid' => '2257732090%3AlfAhv6P5myYfaq%3A16',
      'csrftoken' => 'iLm7IhuyJhwjkQeOXHSaLmp3qYSVmnOm',
    ];

    $options = [
      'query' => $query,
      'cookies' => \GuzzleHttp\Cookie\CookieJar::fromArray($cookies, $domain),
    ];

    $response = $this->request($requestUrl, 'GET', $options);
    return $response;
  }


}
