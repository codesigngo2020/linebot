<?php
namespace App\Helpers\Solution\_2;

use Carbon\Carbon;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_2\Crawler;

// Helper
use App\Helpers\Basic\Request\RequestTrait;

class WordpressCreatePostHelper
{

  use RequestTrait;


  public function __construct()
  {
    $this->associated = false;
  }

  /* ========== 新建部署 ========== */

  // 新建部署（測試wordpress連結）- 設置domain
  public function setDomain(String $domain)
  {
    $domain = (strpos($domain, 'http') === 0) ? $domain : 'http://'.$domain;
    $this->domain = parse_url($domain, PHP_URL_HOST);
    return $this;
  }

  // 新建部署（測試wordpress連結）- 設置key
  public function setKey(String $key)
  {
    $this->key = $key;
    return $this;
  }

  // 新建部署 - 測試wordpress連結
  public function associate()
  {
    $requestUrl = url('http://'.$this->domain.'/wp-json/solution/v1/instagram-crawler/associate');
    $options = [
      'form_params' => [
        'key' => $this->key,
      ]
    ];
    $response = $this->request($requestUrl, 'POST', $options);

    $this->associated = ($response['success'] && $response['response']->status == 'success') ? true : false;
    return $this;
  }

  public function status()
  {
    return $this->associated;
  }










  /* ========== wordpress 建立文章 ========== */

  // wordpress 建立文章 - 設置爬蟲以及相關資料
  public function setDeployment(Deployment $deployment)
  {
    $this->deployment = $deployment;
    $this->domain = $deployment->data->domain;
    $this->key = $deployment->data->key;
    return $this;
  }

  public function setCrawler(Crawler $crawler)
  {
    $this->crawler = $crawler;
    return $this;
  }

  // wordpress 建立文章 - 設置文章資料
  public function setPostData($post)
  {
    $post_data = $post->data;
    $content = $post_data->content;
    $options = $this->crawler->options;
    preg_match('/\\n/', $post_data->content, $title_match, PREG_OFFSET_CAPTURE);

    $data = [
      'title' => substr($post_data->content, 0, $title_match ? $title_match[0][1] : 15),
      'categories' => $this->crawler->categories,
      'comment_status' => ($options && in_array('close-comment', $options)) ? 'closed' : 'open',
      'thumbnail' => $post_data->display_url,
    ];

    // 文章連結啟用
    if($options && in_array('link-active', $options)){
      $content = preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@u', '<a href="$1" target="_blank">$0</a>', $content);
    }

    // 移除原有hashtag
    if($options && in_array('remove-tags-from-post', $options)){
      $content = preg_replace('/(#\w+\S*\s*\n*)/u', '', $content);
    }

    // 設定post format
    if($this->crawler->format){
      $content = 'a';
    }else{
      $content = '<div class="i-instagram-image"><a href="http://instagram.com/p/'.$post->shortcode.'"><img src="'.$post_data->display_url.'"/></a></div><div class="i-instagram-desc"><p>'.$content.'</p></div>';
    }
    $data['content'] = $content;

    // 設定tags
    if($options && in_array('include-origin-tags', $options)){
      preg_match_all('/#(\w+\S*)/u', $post_data->content, $tags_match);
      $data['tags'] = array_merge($this->crawler->tags, $tags_match[1]);
    }else{
      $data['tags'] = $this->crawler->tags;
    }

    // 設定文章建立時間 - instagram發布時間 or 現在時間
    if($options && in_array('publish-time-as-post-time', $options)){
      $data['created_at'] = Carbon::parse($post_data->publishedAt)->format('Y-m-d H:i:s');
    }else{
      $data['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
    }

    $this->post_data = $data;
    return $this;
  }

  // post資料到instagram建立文章
  public function createPost()
  {
    $requestUrl = url('http://'.$this->domain.'/wp-json/solution/v1/instagram-crawler/create-post');
    $options = [
      'connect_timeout' => 5,
      'form_params' => [
        'key' => $this->key,
        'post_data' => $this->post_data,
      ]
    ];
    $response = $this->request($requestUrl, 'POST', $options);

    if($response['success']){
      $this->http_code = 200;
      $this->post_id = ($response['response']->status == 'success') ? $response['response']->post_id : false;
    }else{
      $this->http_code = $response['response']->getCode();
      $this->errorMessage = $response['response']->getMessage();
      $this->errorHandlerContext = $response['response']->getHandlerContext();
    }
    return $this;
  }

  public function getHttpCode()
  {
    return $this->http_code;
  }

  public function getErrorMessage()
  {
    return $this->errorMessage;
  }

  public function getErrorHandlerContext()
  {
    return $this->errorHandlerContext;
  }

  public function getPostId()
  {
    return $this->post_id;
  }
}
