<?php
namespace App\Helpers\Solution\_3;

// Helper
use App\Helpers\Basic\Request\RequestTrait;

class ProfileHelper
{

  use RequestTrait;

  /* ========== 設置資料資料 ========== */

  public function setUserName($userName)
  {
    $this->userName = $userName;
    return $this;
  }


  /* ========== 取得帳號資料 ========== */

  public function get()
  {
    return $this->request('https://www.instagram.com/'.$this->userName.'?__a=1', 'GET');
  }



}
