<?php
namespace App\Helpers\Solution\_3;

// Model
use App\Models\Platform\Solution\_3\Account;

// Helper
use App\Helpers\Basic\Request\RequestTrait;

class FanHelper
{

  use RequestTrait;

  public function __construct()
  {
    $this->endCursor = NULL;
  }

  /* ========== 設置資料資料 ========== */

  public function setAccount(Account $account)
  {
    $this->account = $account;
    return $this;
  }

  public function setEndCursor($endCursor)
  {
    $this->endCursor = $endCursor;
    return $this;
  }


  /* ========== 取得帳號資料 ========== */

  public function get()
  {
    $requestUrl = 'https://www.instagram.com/graphql/query/?query_hash=c76146de99bb02f6415203be841dd25a&variables={"id":"'.$this->account->account_id.'","include_reel":true,"fetch_mutual":true,"first":50,"after":"'.$this->endCursor.'"}';

    $domain = 'www.instagram.com';
    $cookies = [
      'sessionid' => '2257732090%3AlfAhv6P5myYfaq%3A16',
      'csrftoken' => 'iLm7IhuyJhwjkQeOXHSaLmp3qYSVmnOm',
    ];

    $options = [
      'cookies' => \GuzzleHttp\Cookie\CookieJar::fromArray($cookies, $domain),
    ];

    return $this->request($requestUrl, 'GET', $options);
  }



}
