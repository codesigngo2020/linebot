<?php
namespace App\Helpers\Solution\_3;

// Helper
use App\Helpers\Basic\Request\RequestTrait;

class MediaHelper
{

  use RequestTrait;

  public function __construct()
  {
    $this->endCursor = NULL;
  }

  /* ========== 設置資料資料 ========== */

  public function setAccountId($accountId)
  {
    $this->accountId = $accountId;
    return $this;
  }

  public function setEndCursor($endCursor)
  {
    $this->endCursor = $endCursor;
    return $this;
  }


  /* ========== 取得帳號資料 ========== */

  public function get()
  {
    $query = [
      'id' => $this->accountId,
      'first' => 50,
      'query_hash' => 'f2405b236d85e8296cf30347c9f08c2a',
    ];

    if($this->endCursor) $query['after'] = $this->endCursor;

    $domain = 'www.instagram.com';
    $cookies = [
      'sessionid' => '2257732090%3AlfAhv6P5myYfaq%3A16',
      'csrftoken' => 'iLm7IhuyJhwjkQeOXHSaLmp3qYSVmnOm',
    ];

    $options = [
      'query' => $query,
      'cookies' => \GuzzleHttp\Cookie\CookieJar::fromArray($cookies, $domain),
    ];

    return $this->request('https://www.instagram.com/graphql/query', 'GET', $options);
  }



}
