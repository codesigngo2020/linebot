<?php
namespace App\Helpers\Solution\_3;

// Helper
use App\Helpers\Basic\Request\RequestTrait;

class FirstCommentHelper
{

  use RequestTrait;


  public function __construct()
  {
    $this->endCursor = '';
  }


  /* ========== 設置資料資料 ========== */

  public function setShortCode($shortCode)
  {
    $this->shortCode = $shortCode;
    return $this;
  }

  public function setEndCursor($endCursor)
  {
    $this->endCursor = $endCursor;
    return $this;
  }


  /* ========== 取得帳號資料 ========== */

  public function get()
  {
    $requestUrl = 'https://www.instagram.com/graphql/query/?query_hash=97b41c52301f77ce508f55e66d17620e&variables={"shortcode":"'.$this->shortCode.'","first":50,"after":"'.$this->endCursor.'"}';

    $domain = 'www.instagram.com';
    $cookies = [
      'sessionid' => '2257732090%3AlfAhv6P5myYfaq%3A16',
      'csrftoken' => 'iLm7IhuyJhwjkQeOXHSaLmp3qYSVmnOm',
    ];

    $options = [
      'cookies' => \GuzzleHttp\Cookie\CookieJar::fromArray($cookies, $domain),
    ];

    return $this->request($requestUrl, 'GET', $options);
  }



}
