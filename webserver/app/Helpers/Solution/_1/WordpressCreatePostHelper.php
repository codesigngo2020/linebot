<?php
namespace App\Helpers\Solution\_1;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class WordpressCreatePostHelper
{
  public function __construct()
  {
    $this->associated = false;
  }

  /* ========== 新建部署 ========== */

  // 新建部署（測試wordpress連結）- 設置domain
  public function setDomain(String $domain)
  {
    $domain = (strpos($domain, 'http') === 0) ? $domain : 'http://'.$domain;
    $this->domain = parse_url($domain, PHP_URL_HOST);
    return $this;
  }

  // 新建部署（測試wordpress連結）- 設置key
  public function setKey(String $key)
  {
    $this->key = $key;
    return $this;
  }

  // 新建部署 - 測試wordpress連結
  public function associate()
  {
    $request_url = url('http://'.$this->domain.'/wp-json/solution/v1/youtube-crawler/associate');
    $form_params = [
      'key' => $this->key,
    ];
    $response = $this->getJsonResponse($request_url, $form_params);

    $this->associated = ($response['success'] && $response['response']->status == 'success') ? true : false;
    return $this;
  }

  public function status()
  {
    return $this->associated;
  }










  /* ========== wordpress 建立文章 ========== */

  // wordpress 建立文章 - 設置爬蟲以及相關資料
  public function setCrawler($crawler)
  {
    $this->crawler = $crawler;
    $deployment = $crawler->deployment;
    $this->domain = $deployment->data->domain;
    $this->key = $deployment->data->key;
    return $this;
  }

  // wordpress 建立文章 - 設置影片資料
  public function setVideoData($video_id, $video_data)
  {
    $options = $this->crawler->options;
    $content = $video_data->description;

    $data = [
      'title' => $video_data->title,
      'categories' => $this->crawler->categories,
      'comment_status' => ($options && in_array('close-comment', $options)) ? 'closed' : 'open',
    ];

    // 文章連結啟用
    if($options && in_array('link-active', $options)){
      $content = preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@u', '<a href="$1" target="_blank">$0</a>', $content);
    }

    // 設定video format
    if($this->crawler->format){
      $content = 'a';
    }else{
      $content = '<div class="i-youtube-player"><iframe src="https://www.youtube.com/embed/'.$video_id.'?autoplay=1&amodestbranding=1" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div><div class="i-youtube-desc">'.$content.'</div>';
      }
      $data['content'] = $content;

      // 設定tags
      if($options && in_array('include-origin-tags', $options) && isset($video_data->tags)){
        $data['tags'] = array_merge($this->crawler->tags, $video_data->tags);
      }else{
        $data['tags'] = $this->crawler->tags;
      }

      // 設定文章建立時間 - youtube發布時間 or 現在時間
      if($options && in_array('publish-time-as-post-time', $options)){
        $data['created_at'] = Carbon::parse($video_data->publishedAt)->format('Y-m-d H:i:s');
      }else{
        $data['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
      }

      // 設定圖片抓取順序
      $thumbnails_order = collect(['maxres', 'standard', 'high', 'medium', 'default']);
      $thumbnails_order->each(function($type) use($video_data, &$data){
        if(isset($video_data->thumbnails->{$type})){
          $data['thumbnail'] = $video_data->thumbnails->{$type}->url;
          return false;
        }
      });
      $this->video_data = $data;
      return $this;
    }

    // post資料到wordpress建立文章
    public function createPost()
    {
      $request_url = url('http://'.$this->domain.'/wp-json/solution/v1/youtube-crawler/create-post');
      $form_params = [
        'key' => $this->key,
        'video_data' => $this->video_data,
      ];
      $response = $this->getJsonResponse($request_url, $form_params);

      if($response['success']){
        $this->http_code = 200;
        $this->post_id = ($response['response']->status == 'success') ? $response['response']->post_id : false;
      }else{
        $this->http_code = $response['response']->getCode();
      }
      return $this;
    }

    public function getHttpCode()
    {
      return $this->http_code;
    }

    public function getPostId()
    {
      return $this->post_id;
    }










    /* ========== get response ========== */

    public function getJsonResponse($request_url, $form_params)
    {
      $client = new Client;
      try{
        $response = $client->request('POST', $request_url, [
          'form_params' => $form_params,
        ])
        ->getBody()
        ->getContents();

        return [
          'success' => true,
          'response' => json_decode($response),
        ];
      }catch(RequestException $e){
        dd($e);
        return [
          'success' => false,
          'response' => $e,
        ];
      }
    }


  }
