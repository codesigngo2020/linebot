<?php
namespace App\Helpers\Solution\_1;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

// Model
use App\Models\Platform\Solution\_1\Crawler;
use App\Models\Platform\Solution\_1\Video;

class YoutubeRequestHelper
{
  /* ========== 新建部署 ========== */

  // 新建部署 - 測試youtube金鑰
  public function testKey(String $key)
  {
    $response = $this->getJsonResponse('https://www.googleapis.com/youtube/v3/channels', [
      'part' => 'id',
      'maxResults' => 1,
      'id' => 'UCsT0YIqwnpJCM-mx7-gSA4Q',
      'key' => $key,
    ]);
    return $response ? 'success' : 'error';
  }

  // 新建部署 - 取得channel名稱
  public function getChannelTitle(String $key, String $id)
  {
    $response = $this->getJsonResponse('https://www.googleapis.com/youtube/v3/channels', [
      'part' => 'snippet',
      'maxResults' => 1,
      'id' => $id,
      'key' => $key
    ]);
    return $response->items[0]->snippet->title;
  }

  // 新建部署 - 取得list名稱
  public function getListTitle(String $key, String $id)
  {
    $response = $this->getJsonResponse('https://www.googleapis.com/youtube/v3/playlists', [
      'part' => 'snippet',
      'maxResults' => 1,
      'id' => $id,
      'key' => $key
    ]);
    return $response->items[0]->snippet->title;
  }

  // 新建部署 - 取得channel 全部影片的list
  public function getChannelUploadListId(String $key, String $id)
  {
    $response = $this->getJsonResponse('https://www.googleapis.com/youtube/v3/channels', [
      'part' => 'contentDetails',
      'maxResults' => 1,
      'id' => $id,
      'key' => $key
    ]);
    return $response->items[0]->contentDetails->relatedPlaylists->uploads;
  }










  /* ========== 定時爬取資料到資料庫 ========== */

  // 定時爬取資料到資料庫 - 設置爬蟲
  public function setCrawler(Crawler $crawler)
  {
    $this->crawler = $crawler;
    return $this;
  }

  // 定時爬取資料到資料庫 - 取得所有應爬影片數
  public function setVideosCount()
  {
    $this->videos_count = $this->getVideosCount();
    return $this;
  }

  // 定時爬取資料到資料庫 - 取得所有已爬的文章數
  public function setCrawleredPostsCount()
  {
    $this->crawlered_posts_count = $this->crawler->posts()->count();
    return $this;
  }

  // 定時爬取資料到資料庫 - 把未爬的影片新增入資料庫

  // 以爬過得移除
  public function pushToSchedule()
  {
    $crawler = $this->crawler;
    if($this->videos_count > $this->crawlered_posts_count){
      $video_list_data = $this->getVideoListData($crawler);
      $videos = collect($video_list_data->items);

      //未爬的影片列入排程
      $videos->each(function($video, $key) use($crawler){
        $video_id = $video->snippet->resourceId->videoId;
        $title = $video->snippet->title;

        $video = Video::firstOrCreate(['video_id' => $video_id], ['title' => $title]);
        $crawler->posts()->firstOrCreate(['video_id' => $video->id]);
      });

      if(isset($video_list_data->nextPageToken)){
        $crawler->update(['page_token' => $video_list_data->nextPageToken]);
        $this->pushToSchedule();
      }else{
        $crawler->update(['page_token' => NULL]);
      }
    }
    return $this;
  }










  /* ========== 影片數request ========== */

  public function getVideosCount()
  {
    $requestUrl = $this->getRequestVideoCountUrl();
    $query = $this->getVideoCountRequestQuery();
    $response = $this->getJsonResponse($requestUrl, $query);

    return $response->items[0]->contentDetails->itemCount;
  }

  public function getRequestVideoCountUrl()
  {
    switch($this->crawler->type){
      case 'channel':
      case 'list':
      $url = 'https://www.googleapis.com/youtube/v3/playlists';
      break;

      default:
      return false;
      break;
    }

    return $url;
  }

  public function getVideoCountRequestQuery()
  {
    $crawler = $this->crawler;

    switch($crawler->type){
      case 'channel':
      case 'list':
      $query = [
        'part' => 'contentDetails',
        'id' => $crawler->list->list_id,
        'key' => $crawler->valid_keys()->first()->key,
      ];
    }

    return $query;
  }









  /* ========== 影片列表 request ========== */

  public function getVideoListData()
  {
    $requestUrl = $this->getRequestVideoListDataUrl();
    $query = $this->getVideoListRequestQuery();
    $response = $this->getJsonResponse($requestUrl, $query);
    return $response;
  }

  public function getRequestVideoListDataUrl()
  {
    switch($this->crawler->type){
      case 'channel':
      case 'list':
      $url = 'https://www.googleapis.com/youtube/v3/playlistItems';
      break;

      default:
      return false;
      break;
    }

    return $url;
  }

  public function getVideoListRequestQuery()
  {
    $crawler = $this->crawler;

    switch($crawler->type){
      case 'channel':
      case 'list':
      $query = [
        'part' => 'snippet',
        'channelId' => $crawler->channel->channel_id,
        'playlistId' => $crawler->list->list_id,
        'key' => $crawler->valid_keys()->first()->key,
        'pageToken' => $crawler->page_token,
        'maxResults' => 50,
        'order' => 'date',
      ];
    }

    return $query;
  }










  /* ========== 取得單一影片資訊 ========== */

  public function getVideoData(Video $video)
  {
    $requestUrl = 'https://www.googleapis.com/youtube/v3/videos';
    $query = [
      'part' => 'snippet',
      'key' => $this->crawler->valid_keys()->first()->key,
      'id' => $video->video_id,
      'maxResults' => 1,
    ];
    $response = $this->getJsonResponse($requestUrl, $query);
    return $response ? $response->items[0]->snippet : false;
  }





  /* ========== get response ========== */

  public function getJsonResponse($requestUrl, $query)
  {
    $client = new Client;
    try{
      $response = $client->request('GET', $requestUrl, [
        'query' => $query,
      ])
      ->getBody()
      ->getContents();

      return json_decode($response);
    }catch(RequestException $e){
      //dd($e);
      return false;
    }
  }


}
