<?php
namespace App\Helpers\Solution\_4\Liff;

// Model
use App\Models\Platform\Solution\_4\Liff;

// Trait
use App\Helpers\Solution\_4\Basic\RequestTrait;

class LiffHelper
{
    use RequestTrait;

    /* ========== 設置資料 ========== */

    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    public function setLiff(Liff $liff)
    {
        $this->liff = $liff;
        return $this;
    }

    /* ========== 發送訊息  ========== */

    public function getAll()
    {
        return $this->request('https://api.line.me/liff/v1/apps', 'GET');
    }

    public function add($liffData)
    {
        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '.$this->accessToken,
            ],
            'json' => [
                'view' => [
                    'type' => $liffData['view']['type'],
                    'url' => $liffData['view']['url'],
                ],
                'description' => $liffData['description'],
            ],
        ];
        return $this->request('https://api.line.me/liff/v1/apps', 'POST', $options);
    }

    public function delete()
    {
        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '.$this->accessToken,
            ],
        ];
        return $this->request('https://api.line.me/liff/v1/apps/'.$this->liff->liff_id, 'DELETE', $options);
    }

}
