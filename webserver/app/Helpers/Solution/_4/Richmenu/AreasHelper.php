<?php
namespace App\Helpers\Solution\_4\Richmenu;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Liff;
use App\Models\Platform\Solution\_4\Tag;
use App\Models\Platform\Solution\_4\Script;
use App\Models\Platform\Solution\_4\Keyword;
use App\Models\Platform\Solution\_4\Richmenu;

class AreasHelper
{
    public function __construct()
    {
        $this->areas = [];
        $this->tags = collect();
    }

    public function setDeployment(Deployment $deployment)
    {
        $this->deployment = $deployment;
        return $this;
    }

    public function setAreasData($data)
    {
        $this->data = $data;
        return $this;
    }

    public function setWidthAndHeight($width, $height)
    {
        $this->width = $width;
        $this->height = $height;
        return $this;
    }

    public function get()
    {
        $this->parseArea();
        return $this->areas;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function getParsedAreasData()
    {
        return $this->data;
    }

    /* ========== 解析區域函式 ========== */

    private function parseArea()
    {
        $this->parseRow(0, 0, $this->width, $this->height, $this->data);
    }

    private function parseRow($startX, $startY, $maxWidth, $maxHeight, &$rows)
    {
        $unitHeight = $maxHeight / count($rows);
        foreach($rows as $key => &$row){
            $this->parseColumn($startX, $startY + ($key * $unitHeight), $maxWidth, $unitHeight, $row);
        }
    }

    private function parseColumn($startX, $startY, $maxWidth, $maxHeight, &$columns)
    {
        $unitWidth = $maxWidth / count($columns);
        foreach($columns as $key => &$column){
            if(isset($column['id'])){
                $this->areas[] = [
                    'bounds' => [
                        'x' => $startX + ($unitWidth * $key),
                        'y' => $startY,
                        'width' => $unitWidth,
                        'height' => $maxHeight,
                    ],
                    'action' => $this->{'get'.ucfirst($column['type']['value'])}($column),
                ];
            }else{
                $this->parseRow($startX + ($unitWidth * $key), $startY, $unitWidth, $maxHeight, $column);
            }
        }
    }

    /* ========== 解析Action ========== */

    private function getMessage(&$column)
    {
        $tag = Tag::create();
        $this->tags->push($tag);

        $column['tag'] = $tag->id;

        return [
            'type' => 'postback',
            'data' => json_encode([
                'type' => 'message',
                'tag_id' => $tag->id,
                'text' => $column['action']['text']['text'],
            ], JSON_UNESCAPED_UNICODE),
        ];
    }

    private function getKeyword(&$column)
    {
        if($column['action']['keyword']['value']){
            $text = Keyword::find($column['action']['keyword']['value']['value'])->name;
        }else{
            $text = $column['action']['text']['text'];
        }

        return [
            'type' => 'message',
            'text' => $text,
        ];
    }

    private function getUri(&$column)
    {
        return [
            'type' => 'uri',
            'uri' => $column['action']['uri']['uri'],
        ];
    }

    private function getLiff(&$column)
    {
        $sizeTransfer = [
            50 => 'compact',
            80 => 'tall',
            100 => 'full',
        ];

        $liff = Liff::where('is_created_by_platform', 1)
        ->where('view->type', $sizeTransfer[$column['action']['size']['value']['value']])
        ->first();

        $tag = Tag::create();
        $this->tags->push($tag);

        $column['tag'] = $tag->id;

        return [
            'type' => 'uri',
            'uri' => 'line://app/'.$liff->liff_id.'?solution='.$this->deployment->solution_id.'&deployment='.$this->deployment->id.'&tag='.$tag->id.'&redirectUrl='.$column['action']['uri']['uri'],
        ];
    }

    private function getScript(&$column)
    {
        $script = Script::find($column['action']['script']['value']['value']);

        $tag = Tag::create();
        $this->tags->push($tag);

        $column['tag'] = $tag->id;

        return [
            'type' => 'postback',
            'data' => json_encode([
                'type' => 'script',
                'next' => $script->nodes()->where('start', 1)->first()->id,
                'tag_id' => $tag->id,
            ], JSON_NUMERIC_CHECK),
        ];
    }

    private function getRichmenu(&$column)
    {
        $richmenu = Richmenu::find($column['action']['richmenu']['value']['value']);

        $tag = Tag::create();
        $this->tags->push($tag);

        $column['tag'] = $tag->id;

        return [
            'type' => 'postback',
            'data' => json_encode([
                'type' => 'richmenu',
                'tag_id' => $tag->id,
                'richmenu_id' => $richmenu->id,
            ], JSON_NUMERIC_CHECK),
        ];
    }

}
