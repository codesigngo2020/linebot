<?php
namespace App\Helpers\Solution\_4\Richmenu;

use Storage;
use Carbon\Carbon;
use Illuminate\Http\File;

// Trait
use App\Helpers\Solution\_4\Basic\RequestTrait;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Image;
use App\Models\Platform\Solution\_4\Richmenu;

class RichmenuHelper
{

    use RequestTrait;

    /* ========== 設置資料 ========== */

    public function setDeployment(Deployment $deployment)
    {
        $this->deployment = $deployment;
        return $this;
    }

    public function setRichmenu(Richmenu $richmenu)
    {
        $this->richmenu = $richmenu;
        return $this;
    }

    public function setRichmenuId($richmenuId)
    {
        $this->richmenuId = $richmenuId;
        return $this;
    }


    /* ========== 執行動作 ========== */

    public function getList()
    {
        return $this->request('https://api.line.me/v2/bot/richmenu/list', 'GET');
    }

    public function get()
    {
        return $this->request('https://api.line.me/v2/bot/richmenu/'.$this->richmenuId, 'GET');
    }

    public function setDefault()
    {
        return $this->request('https://api.line.me/v2/bot/user/all/richmenu/'.$this->richmenuId, 'POST');
    }

    public function getDefault()
    {
        return $this->request('https://api.line.me/v2/bot/user/all/richmenu/', 'GET');
    }

    public function cancelDefault()
    {
        return $this->request('https://api.line.me/v2/bot/user/all/richmenu/', 'DELETE');
    }

    public function linkToUser($userToken)
    {
        return $this->request('https://api.line.me/v2/bot/user/'.$userToken.'/richmenu/'.$this->richmenuId, 'POST');
    }

    // public function linkToUsers($users)
    // {
    //     $users = array_chunk($users, 150);
    //     foreach ($users as $_users) {
    //         $this->request('https://api.line.me/v2/bot/richmenu/bulk/link', 'POST', [
    //             'headers' => [
    //                 'Authorization' => 'Bearer ' . $this->chatbotToken,
    //                 'Content-Type' => 'application/json',
    //             ],
    //             'json' => [
    //                 'richmenuId' => $this->richmenuId,
    //                 'userIds' => $_users
    //             ]
    //         ]);
    //     }
    // }

    public function getByUser($userToken)
    {
        return $this->request('https://api.line.me/v2/bot/user/'.$userToken.'/richmenu', 'GET');
    }

    public function unlinkFromUser($userToken)
    {
        return $this->request('https://api.line.me/v2/bot/user/'.$userToken.'/richmenu', 'DELETE');
    }

    // public function unlinkFromUsers($users)
    // {
    //     $users = array_chunk($users, 150);
    //     foreach ($users as $_users) {
    //         $this->request('https://api.line.me/v2/bot/richmenu/bulk/unlink', 'POST', [
    //             'headers' => [
    //                 'Authorization' => 'Bearer ' . $this->chatbotToken,
    //                 'Content-Type' => 'application/json',
    //             ],
    //             'json' => [
    //                 'userIds' => $_users
    //             ]
    //         ]);
    //     }
    // }

    public function create($richmenuData)
    {
        return $this->request('https://api.line.me/v2/bot/richmenu', 'POST', [
            'headers' => [
                'Authorization' => 'Bearer '.$this->accessToken,
                'Content-Type' => 'application/json',
            ],
            'json' => $richmenuData,
        ]);
    }

    public function uploadRichmenuImage($file)
    {
        $disk = Storage::disk('gcs');

        if($file){
            $path = $disk->put('deployments/'.$this->deployment->id.'/richmenus', $file);
            $mimeType = $file->getMimeType();

            $image = Image::create([
                'name' => 'LINE主選單',
                'disk' => 'gcs',
                'path' => $path,
            ]);
        }else{
            $image = $this->richmenu->image[0];
            $path = $image->path;
        }

        $ch = curl_init('https://api.line.me/v2/bot/richmenu/'.$this->richmenuId.'/content');

        $options = [
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$this->accessToken,
                'Content-Type: '.($mimeType ?? 'image/jpeg'),
            ],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_BINARYTRANSFER => true,
            CURLOPT_HEADER => true,
            CURLOPT_PUT => true,
            CURLOPT_INFILE => fopen($disk->url($path), 'r'),
            CURLOPT_INFILESIZE => $disk->size($path),
        ];
        curl_setopt_array($ch, $options);

        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        $start = $info['header_size'];
        $body = substr($result, $start, strlen($result) - $start);

        if($body == '{}'){
            return $image;
        }else{
            $image->deleteCompletely();
            \Log::error($body);
            return false;
        }
    }

    public function downloadRichmenuImage()
    {
        $disk = Storage::disk('gcs');

        $fileName = 'richmenu_'.Carbon::now()->timestamp.'.jpeg';
        $path = 'deployments/'.$this->deployment->id.'/richmenus/'.$fileName;

        $res = $this->request('https://api.line.me/v2/bot/richmenu/'.$this->richmenuId.'/content', 'GET', [
            'headers' => [
                'Authorization' => 'Bearer '.$this->accessToken,
            ],
        ]);

        // 沒有圖片
        if($res['success'] === false) return false;

        $disk->put($path, $res['response']);

        return $path;
    }

    public function createAndUploadImage($richmenuData, $image)
    {
        $res = $this->create($richmenuData);
        if(!$res['success']) return false;
        $res = json_decode($res['response']);

        $richmenuId = $res->richMenuId ?? NULL;
        if($richmenuId){
            $image = $this->setRichmenuId($richmenuId)
            ->uploadRichmenuImage($image);

            if($image){
                return compact('richmenuId', 'image');
            }else{
                $this->delete();
            }
        }else{
            return false;
        }
    }

    // public function updateRichmenu()
    // {
    //     $responses['deleteRichmenu'] = $this->deleteRichmenu();
    //     if($responses['deleteRichmenu']['code'] == 200 || $responses['deleteRichmenu']['code'] == 404){
    //         $responses['createRichmenu'] = $this->createRichmenu();
    //     }
    //
    //     return $responses;
    // }

    public function delete()
    {
        return $this->request('https://api.line.me/v2/bot/richmenu/'.$this->richmenuId, 'DELETE');
    }

    public function switch(Richmenu $currentRichmenu)
    {
        return $this->request('https://'.$this->deployment->data->webhookDomain.'/richmenu/switch', 'POST', [
            'json' => [
                'solution' => 4,
                'deployment' => $this->deployment->id,
                'from' => $currentRichmenu->id,
                'to' => $this->richmenu->id,
            ],
        ]);
    }

    public function cancelBinding(Richmenu $currentRichmenu)
    {
        return $this->request('https://'.$this->deployment->data->webhookDomain.'/richmenu/cancelBinding', 'POST', [
            'json' => [
                'solution' => 4,
                'deployment' => $this->deployment->id,
                'richmenu' => $currentRichmenu->id,
            ],
        ]);
    }

}
