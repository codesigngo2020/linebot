<?php
namespace App\Helpers\Solution\_4\Statistics;

// Trait
use App\Helpers\Solution\_4\Basic\RequestTrait;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Statistics;

class StatisticsHelper
{

    use RequestTrait;

    /* ========== 執行動作 ========== */

    public function getNumberOfFollowers($date)
    {
        return $this->request('https://api.line.me/v2/bot/insight/followers?date='.$date, 'GET');
    }

    public function getNumberOfMessageDeliveries($date)
    {
        return $this->request('https://api.line.me/v2/bot/insight/message/delivery?date='.$date, 'GET');
    }

    public function getNumberOfMessagesSentThisMonth()
    {
        return $this->request('https://api.line.me/v2/bot/message/quota/consumption', 'GET');
    }

    // public function setDefault()
    // {
    //     return $this->request('https://api.line.me/v2/bot/user/all/richmenu/'.$this->richmenuId, 'POST');
    // }
    //
    // public function getDefault()
    // {
    //     return $this->request('https://api.line.me/v2/bot/user/all/richmenu/', 'GET');
    // }
    //
    // public function cancelDefault()
    // {
    //     return $this->request('https://api.line.me/v2/bot/user/all/richmenu/', 'DELETE');
    // }
    //
    // public function linkToUser($userToken)
    // {
    //     return $this->request('https://api.line.me/v2/bot/user/'.$userToken.'/richmenu/'.$this->richmenuId, 'POST');
    // }
    //
    // public function getByUser($userToken)
    // {
    //     return $this->request('https://api.line.me/v2/bot/user/'.$userToken.'/richmenu', 'GET');
    // }
    //
    // public function unlinkFromUser($userToken)
    // {
    //     return $this->request('https://api.line.me/v2/bot/user/'.$userToken.'/richmenu', 'DELETE');
    // }
}
