<?php
namespace App\Helpers\Solution\_4\Group;

// Model
use App\Models\Platform\Solution\_4\Group;

// Helper
use App\Helpers\Solution\_4\Group\Circles1Helper;

// Trait
use App\Helpers\Solution\_4\Group\CirclesTrait;

class Circles3Helper
{
    use CirclesTrait;

    /* ========== 設置資料 ========== */

    public function setGroup(Group $group)
    {
        $this->group = $group;

        $this->remove_users_id = $this->group->remove_users_id ? implode(',', $this->group->remove_users_id) : NULL;

        return $this;
    }

    // 第一次建立使用
    public function getSQL()
    {

        $sql = '';

        foreach($this->group->circles[$this->circlesIndex]->selectedAreas as $index => $selectedAreaNum){
            $sql .= ($index == 0) ? '(' : ' UNION (';

            if($selectedAreaNum <= 3){
                $sql .= $this->getSQL123(intval($selectedAreaNum));
            }elseif($selectedAreaNum <= 6){
                $sql .= $this->getSQL456(intval($selectedAreaNum));
            }else{
                $sql .= $this->getSQL7(intval($selectedAreaNum));
            }

            $sql .= ')';
        }

        return $sql;
    }

    /* ========== 只有一個區域被選取 ========== */

    private function getSQL123($selectedAreaNum)
    {
        $sql = 'SELECT DISTINCT '.$this->group->id.' AS `group_id`, `users`.`id`  AS `user_id`
        FROM `users`

        INNER JOIN `tag_user` as `tag_user_1` ON `tag_user_1`.`user_id` = `users`.`id` AND `tag_user_1`.`tag_id` = '.$this->group->circles[$this->circlesIndex]->tagsId[$selectedAreaNum - 1];

        $i = 2;
        foreach(array_diff($this->group->circles[$this->circlesIndex]->tagsId, [$this->group->circles[$this->circlesIndex]->tagsId[$selectedAreaNum - 1]]) as $tagId){
            $sql .= ' LEFT JOIN `tag_user` as `tag_user_'.$i.'` ON `tag_user_'.$i.'`.`user_id` = `users`.`id` AND `tag_user_'.$i.'`.`tag_id` = '.$tagId;
            $i ++;
        }

        $sql .= ' WHERE `tag_user_2`.`user_id` IS NULL AND `tag_user_3`.`user_id` IS NULL';

        if($this->remove_users_id) $sql .= ' AND `users`.`id` NOT IN ('.$this->remove_users_id.')';

        return $sql;
    }

    private function getSQL456($selectedAreaNum)
    {
        $sql = 'SELECT DISTINCT '.$this->group->id.' AS `group_id`, `users`.`id`  AS `user_id`
        FROM `users`';

        for($i = 1; $i <= 2; $i ++){
            $tagIndex = $selectedAreaNum - 2 - $i;
            $sql .= ' INNER JOIN `tag_user` as `tag_user_'.$i.'` ON `tag_user_'.$i.'`.`user_id` = `users`.`id` AND `tag_user_'.$i.'`.`tag_id` = '.$this->group->circles[$this->circlesIndex]->tagsId[$tagIndex >= 3 ? $tagIndex - 3 : $tagIndex];
        }

        $tagIndex = $selectedAreaNum - 2;
        $sql .= ' LEFT JOIN `tag_user` as `tag_user_3` ON `tag_user_3`.`user_id` = `users`.`id` AND `tag_user_3`.`tag_id` = '.$this->group->circles[$this->circlesIndex]->tagsId[$tagIndex >= 3 ? $tagIndex - 3 : $tagIndex].
        ' WHERE `tag_user_3`.`user_id` IS NULL';

        if($this->remove_users_id) $sql .= ' AND `users`.`id` NOT IN ('.$this->remove_users_id.')';

        return $sql;
    }

    private function getSQL7($selectedAreaNum)
    {
        $sql = 'SELECT DISTINCT '.$this->group->id.' AS `group_id`, `users`.`id`  AS `user_id`
        FROM `users`';

        for($i = 1; $i <= 3; $i ++){
            $sql .= ' INNER JOIN `tag_user` as `tag_user_'.$i.'` ON `tag_user_'.$i.'`.`user_id` = `users`.`id` AND `tag_user_'.$i.'`.`tag_id` = '.$this->group->circles[$this->circlesIndex]->tagsId[$i - 1];
        }

        if($this->remove_users_id) $sql .= ' WHERE `users`.`id` NOT IN ('.$this->remove_users_id.')';

        return $sql;
    }

}
