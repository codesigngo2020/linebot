<?php
namespace App\Helpers\Solution\_4\Group;

// Model
use App\Models\Platform\Solution\_4\Group;

// Trait
use App\Helpers\Solution\_4\Group\CirclesTrait;

class Circles1Helper
{
    use CirclesTrait;

    /* ========== 設置資料 ========== */

    public function setGroup(Group $group)
    {
        $this->group = $group;
        $this->remove_users_id = $this->group->remove_users_id ? implode(',', $this->group->remove_users_id) : NULL;

        return $this;
    }

    // 第一次建立使用
    public function getSQL($tagId = NULL)
    {
        $sql = 'SELECT DISTINCT '.$this->group->id.' AS `group_id`, `users`.`id`  AS `user_id`
        FROM `users`

        INNER JOIN `tag_user` ON `tag_user`.`user_id` = `users`.`id`
        AND `tag_user`.`tag_id` = '.($tagId ? $tagId : $this->group->circles[$this->circlesIndex]->tagsId[0]);

        if($this->remove_users_id) $sql .= ' WHERE `users`.`id` NOT IN ('.$this->remove_users_id.')';

        return $sql;
    }

}
