<?php
namespace App\Helpers\Solution\_4\Group;

use DB;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Group;

// Helper
use App\Helpers\Solution\_4\Group\Circles1Helper;

class GroupHelper
{

    /* ========== 設置資料 ========== */

    public function setDeployment(Deployment $deployment)
    {
        $this->deployment = $deployment;
        return $this;
    }

    public function setGroup(Group $group)
    {
        $this->group = $group;

        if($this->group->circles){

            $this->circles1Helper = new Circles1Helper;
            $this->circles2Helper = new Circles2Helper;
            $this->circles3Helper = new Circles3Helper;

            for($i = 1; $i <= 3; $i ++){
                $this->{'circles'.$i.'Helper'}->setGroup($this->group);
            }
        }

        return $this;
    }


    /* ========== 執行動作 ========== */

    // 初次關聯
    public function set()
    {
        $sql = 'INSERT INTO `group_user` ( `group_id`, `user_id` ) ';
        $this->{ $this->group->circles ? 'getCirclesSQL': 'getAddUsersSQL' }($sql);

        DB::connection('solution_4')->insert($sql);
    }

    // 更新關聯：移除舊關聯->新增新的關聯
    public function update()
    {
        $this->updateDelete();
        $this->updateInsert();
    }

    private function updateDelete()
    {
        $sql = 'UPDATE `group_user` LEFT JOIN '.'( ';
        $this->{ $this->group->circles ? 'getCirclesSQL': 'getAddUsersSQL' }($sql);

        $sql .= ') AS `sync_group_user` ON `sync_group_user`.`user_id` = `group_user`.`user_id` SET `group_user`.`deleted_at` = CURRENT_TIMESTAMP WHERE `group_user`.`group_id` = '.$this->group->id.' AND `sync_group_user`.`user_id` IS NULL';

        DB::connection('solution_4')->update($sql);
    }

    private function updateInsert()
    {
        $sql = 'INSERT INTO `group_user` ( `group_id`, `user_id` ) SELECT `sync_group_user`.`group_id`, `sync_group_user`.`user_id` FROM '.'( ';
        $this->{ $this->group->circles ? 'getCirclesSQL': 'getAddUsersSQL' }($sql);

        $sql .= ') AS `sync_group_user` LEFT JOIN `group_user` ON `group_user`.`user_id` = `sync_group_user`.`user_id` AND `group_user`.`group_id` = '.$this->group->id.' AND `group_user`.`deleted_at` IS NULL WHERE `group_user`.`user_id` IS NULL';

        DB::connection('solution_4')->insert($sql);
    }

    /* ========== 有標籤條件時，取得好友名單的SQL ========== */

    private function getCirclesSQL(&$sql)
    {
        foreach($this->group->circles as $circlesIndex => $circles){
            if($circlesIndex != 0) $sql .= ' UNION ';
            $sql .= $this->{'circles'.count($circles->tagsId).'Helper'}->setCirclesIndex($circlesIndex)->getSQL();
        }

        $this->getAddUsersSQLWithCircles($sql);
    }

    /* ========== 沒有標籤條件時，取得新增好友名單的SQL ========== */

    private function getAddUsersSQL(&$sql)
    {
        $sql .= 'SELECT '.$this->group->id.' AS `group_id`, `users`.`id`  AS `user_id` FROM `users` WHERE `users`.`id` IN ('.implode(',', $this->group->add_users_id).')';

        if($this->group->remove_users_id) $sql .= ' AND `users`.`id` NOT IN ('.implode(',', $this->group->remove_users_id).')';
    }

    private function getAddUsersSQLWithCircles(&$sql)
    {
        if($this->group->add_users_id){
            $sql .= ' UNION
            SELECT DISTINCT '.$this->group->id.' AS `group_id`, `users`.`id`  AS `user_id` FROM `users` WHERE `users`.`id` IN ('.implode(',', $this->group->add_users_id).')';

            if($this->group->remove_users_id) $sql .= ' AND `users`.`id` NOT IN ('.implode(',', $this->group->remove_users_id).')';
        }
    }


}
