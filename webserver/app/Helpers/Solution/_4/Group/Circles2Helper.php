<?php
namespace App\Helpers\Solution\_4\Group;

// Model
use App\Models\Platform\Solution\_4\Group;

// Helper
use App\Helpers\Solution\_4\Group\Circles1Helper;

// Trait
use App\Helpers\Solution\_4\Group\CirclesTrait;

class Circles2Helper
{
    use CirclesTrait;

    public function __construct()
    {
        $this->circles1Helper = new Circles1Helper;
    }

    /* ========== 設置資料 ========== */

    public function setGroup(Group $group)
    {
        $this->group = $group;

        $this->circles1Helper->setGroup($this->group);

        $this->remove_users_id = $this->group->remove_users_id ? implode(',', $this->group->remove_users_id) : NULL;

        return $this;
    }

    // 第一次建立使用
    public function getSQL()
    {
        $selectedAreasNum = intval(implode('', $this->group->circles[$this->circlesIndex]->selectedAreas));

        if($selectedAreasNum < 10){ // 只有一個區域被選取
            if($selectedAreasNum == 2){
                return $this->getSQL2();
            }else{
                return $this->getSQL1And3($selectedAreasNum);
            }
        }elseif($selectedAreasNum < 100){ // 兩個區域被選取
            if($selectedAreasNum == 13){
                return $this->getSQL13();
            }else{
                return $this->getSQL12And23($selectedAreasNum);
            }
        }else{
            return $this->getSQL123();
        }

    }

    /* ========== 只有一個區域被選取 ========== */

    private function getSQL1And3($selectedAreasNum)
    {
        $sql = 'SELECT DISTINCT '.$this->group->id.' AS `group_id`, `users`.`id`  AS `user_id`
        FROM `users`

        INNER JOIN `tag_user` as `tag_user_1` ON `tag_user_1`.`user_id` = `users`.`id`
        AND `tag_user_1`.`tag_id` = '.$this->group->circles[$this->circlesIndex]->tagsId[$selectedAreasNum == 1 ? 0 : 1].'
        LEFT JOIN `tag_user` as `tag_user_2` ON `tag_user_2`.`user_id` = `users`.`id`
        AND `tag_user_2`.`tag_id` = '.$this->group->circles[$this->circlesIndex]->tagsId[$selectedAreasNum == 1 ? 1 : 0].'
        WHERE `tag_user_2`.`user_id` IS NULL';

        if($this->remove_users_id) $sql .= ' AND `users`.`id` NOT IN ('.$this->remove_users_id.')';

        // $this->getAddUsersSQL($sql);

        return $sql;
    }

    private function getSQL2()
    {
        $sql = 'SELECT DISTINCT '.$this->group->id.' AS `group_id`, `users`.`id`  AS `user_id`
        FROM `users`

        INNER JOIN `tag_user` as `tag_user_1` ON `tag_user_1`.`user_id` = `users`.`id`
        AND `tag_user_1`.`tag_id` = '.$this->group->circles[$this->circlesIndex]->tagsId[0].'
        INNER JOIN `tag_user` as `tag_user_2` ON `tag_user_2`.`user_id` = `users`.`id`
        AND `tag_user_2`.`tag_id` = '.$this->group->circles[$this->circlesIndex]->tagsId[1];

        if($this->remove_users_id) $sql .= ' WHERE `users`.`id` NOT IN ('.$this->remove_users_id.')';

        return $sql;
    }

    /* ========== 兩個區域被選取 ========== */

    private function getSQL12And23($selectedAreasNum)
    {
        $tagId = $this->group->circles[$this->circlesIndex]->tagsId[$selectedAreasNum == 12 ? 0 : 1];
        return $this->circles1Helper->setCirclesIndex($this->circlesIndex)->getSQL($tagId);
    }

    private function getSQL13()
    {
        $sql = 'SELECT DISTINCT '.$this->group->id.' AS `group_id`, `users`.`id`  AS `user_id`
        FROM `users`

        LEFT JOIN `tag_user` as `tag_user_1` ON `tag_user_1`.`user_id` = `users`.`id`
        AND `tag_user_1`.`tag_id` = '.$this->group->circles[$this->circlesIndex]->tagsId[0].'
        LEFT JOIN `tag_user` as `tag_user_2` ON `tag_user_2`.`user_id` = `users`.`id`
        AND `tag_user_2`.`tag_id` = '.$this->group->circles[$this->circlesIndex]->tagsId[1].'
        WHERE `tag_user_1`.`user_id` IS NULL OR `tag_user_2`.`user_id` IS NULL';

        if($this->remove_users_id) $sql .= ' WHERE `users`.`id` NOT IN ('.$this->remove_users_id.')';

        return $sql;
    }

    /* ========== 三個區域被選取 ========== */

    private function getSQL123()
    {
        $sql = 'SELECT DISTINCT '.$this->group->id.' AS `group_id`, `users`.`id`  AS `user_id`
        FROM `users`

        INNER JOIN `tag_user` ON `tag_user`.`user_id` = `users`.`id`
        AND `tag_user`.`tag_id` IN ('.implode(',', $this->group->circles[$this->circlesIndex]->tagsId).')';

        if($this->remove_users_id) $sql .= ' WHERE `users`.`id` NOT IN ('.$this->remove_users_id.')';

        return $sql;
    }



}
