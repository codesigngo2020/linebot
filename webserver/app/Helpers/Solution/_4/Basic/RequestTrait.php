<?php
namespace App\Helpers\Solution\_4\Basic;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

trait RequestTrait
{
    /* ========== 設置資料 ========== */

    public function setChannelSecret($channelSecret)
    {
        $this->channelSecret = $channelSecret;
        return $this;
    }

    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /* ========== request ========== */

    public function request($requestUrl, $method, $options = NULL)
    {
        if(!$options) $options = [
            'headers' => [
                'Authorization' => 'Bearer '.$this->accessToken,
            ],
        ];

        $client = new Client;

        try{
            $response = $client->request($method, $requestUrl, $options)
            ->getBody()
            ->getContents();

            \Log::info($response);
            return [
                'success' => true,
                'response' => $response,
            ];

        }catch(\Exception $exception){
            \Log::error($exception);
            return [
                'success' => false,
                'response' => $exception,
            ];

        }
    }
}
