<?php
namespace App\Helpers\Solution\_4\Message;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Liff;
use App\Models\Platform\Solution\_4\Tag;
use App\Models\Platform\Solution\_4\Keyword;

class AreasHelper
{
    public function __construct()
    {
        $this->areas = [];
        $this->tags = collect();
        $this->shouldTag = true;
    }

    public function setDeployment(Deployment $deployment)
    {
        $this->deployment = $deployment;
        return $this;
    }

    public function setAreasData($data)
    {
        $this->data = $data;
        return $this;
    }

    public function setWidthAndHeight($width, $height)
    {
        $this->width = $width;
        $this->height = $height;
        return $this;
    }

    public function setShouldTag($shouldTag)
    {
        $this->shouldTag = $shouldTag;
        return $this;
    }

    public function get()
    {
        return $this->areas;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function getParsedAreasData()
    {
        return $this->data;
    }

    /* ========== 解析區域函式 ========== */

    public function getArea()
    {
        $this->parseRow(0, 0, $this->width, $this->height, $this->data);
    }

    public function parseRow($startX, $startY, $maxWidth, $maxHeight, &$rows)
    {
        $unitHeight = $maxHeight / count($rows);
        foreach($rows as $key => &$row){
            $this->parseColumn($startX, $startY + ($key * $unitHeight), $maxWidth, $unitHeight, $row);
        }
    }

    public function parseColumn($startX, $startY, $maxWidth, $maxHeight, &$columns)
    {
        $unitWidth = $maxWidth / count($columns);
        foreach($columns as $key => &$column){
            if(isset($column['id'])){

                $this->areas[] = array_merge([
                    'area' => [
                        'x' => $startX + ($unitWidth * $key),
                        'y' => $startY,
                        'width' => $unitWidth,
                        'height' => $maxHeight,
                    ],
                ], $this->{'get'.ucfirst($column['type']['value']).'Action'}($column));
            }else{
                $this->parseRow($startX + ($unitWidth * $key), $startY, $unitWidth, $maxHeight, $column);
            }
        }
    }

    /* ========== 解析Action ========== */

    // private function getNextAction(&$column)
    // {
    //     $tag = Tag::create();
    //     $this->tags->push($tag);
    //
    //     $column['tag'] = $tag->id;
    //
    //     return [
    //         'type' => 'postback',
    //         'data' => json_encode([
    //             'type' => 'script',
    //             'next' => $column['action']['nextId']['nextId'],
    //             'tag_id' => $tag->id,
    //         ], JSON_NUMERIC_CHECK),
    //     ];
    // }

    // private function getMessageAction(&$column)
    // {
    //     $tag = Tag::create();
    //     $this->tags->push($tag);
    //
    //     $column['tag'] = $tag->id;
    //
    //     return [
    //         'type' => 'postback',
    //         'data' => json_encode([
    //             'type' => 'message',
    //             'tag_id' => $tag->id,
    //             'text' => $column['action']['text']['text'],
    //         ], JSON_UNESCAPED_UNICODE),
    //     ];
    // }

    private function getKeywordAction(&$action)
    {
        if($action['action']['keyword']['value']){
            $text = Keyword::find($action['action']['keyword']['value']['value'])->name;
        }else{
            $text = $action['action']['text']['text'];
        }

        return [
            'type' => 'message',
            'text' => $text,
        ];
    }

    private function getUriAction(&$column)
    {
        return [
            'type' => 'uri',
            'linkUri' => $column['action']['uri']['uri'],
        ];
    }

    private function getLiffAction(&$column)
    {
        $sizeTransfer = [
            50 => 'compact',
            80 => 'tall',
            100 => 'full',
        ];

        $liff = Liff::where('is_created_by_platform', 1)
        ->where('view->type', $sizeTransfer[$column['action']['size']['value']['value']])
        ->first();

        if($this->shouldTag){
            $tag = Tag::create();
            $this->tags->push($tag);

            $column['tag'] = $tag->id;
        }

        return [
            'type' => 'uri',
            'linkUri' => 'line://app/'.$liff->liff_id.'?solution='.$this->deployment->solution_id.'&deployment='.$this->deployment->id.'&tag='.$tag->id.'&redirectUrl='.$column['action']['uri']['uri'],
        ];
    }

}
