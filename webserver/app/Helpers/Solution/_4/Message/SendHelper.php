<?php
namespace App\Helpers\Solution\_4\Message;

// Trait
use App\Helpers\Solution\_4\Basic\RequestTrait;

class SendHelper
{
    use RequestTrait;

    /* ========== 設置資料 ========== */

    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    public function setTarget(Array $target)
    {
        $this->target = $target;
        return $this;
    }

    public function setMessage(Array $message)
    {
        $this->message = $message;
        return $this;
    }


    /* ========== quick reply ========== */

    public function addQuickReply($message)
    {
        // Log::info($this->message);
        // if($message) $this->message[count($this->message) - 1]['quickReply'] = $message;
        // return $this;
    }


    /* ========== 發送訊息  ========== */

    public function sendAll()
    {
        $this->sendType = 'broadcast';
        return $this->send();
    }

    public function reply($replyToken)
    {
        // $this->sendType = 'reply';
        // $this->replyToken = $replyToken;
        //
        // $response = $this->send();
        //
        // unset($this->replyToken);
        // unset($this->sendType);
        // return $response;
    }

    public function send()
    {
        if(!isset($this->sendType)) $this->setSendType();

        $requestUrl = 'https://api.line.me/v2/bot/message/'.$this->sendType;
        return $this->request($requestUrl, 'POST', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '.$this->accessToken,
            ],
            'json' => $this->getJson(),
        ]);
    }

    public function setSendType()
    {
        if(count($this->target) == 1){
            $this->sendType = 'push';
            $this->target = $this->target[0];
        }else{
            $this->sendType = 'multicast';
        }
    }

    public function getJson()
    {
        $json = [
            'messages' => $this->message,
        ];

        if($this->sendType == 'reply'){
            $json['replyToken'] = $this->replyToken;

        }elseif(in_array($this->sendType, ['push', 'multicast'])){
            $json['to'] = $this->target;

        }

        return $json;
    }

}
