<?php
namespace App\Helpers\Solution\_4\Message;

use Guid;
use Storage;
use ImageProcessor;
use Illuminate\Http\File;
use Illuminate\Filesystem\Filesystem;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Image;
use App\Models\Platform\Solution\_4\Video;
use App\Models\Platform\Solution\_4\Script;
use App\Models\Platform\Solution\_4\Liff;
use App\Models\Platform\Solution\_4\Tag;
use App\Models\Platform\Solution\_4\Keyword;
use App\Models\Platform\Solution\_4\Richmenu;

// Helper
use App\Helpers\Solution\_4\Message\AreasHelper;

class FormaterHelper
{

    public function __construct()
    {
        $this->imagePregMatch = '/^https:\/\/storage\.googleapis\.com\/'.env('GOOGLE_CLOUD_STORAGE_BUCKET').'\//';
        $this->images = collect();
        $this->videos = collect();
        $this->tags = collect();
        $this->children = collect();

        $this->shouldTag = true;
    }

    /* ========== 設置資料 ========== */

    public function setDeployment(Deployment $deployment)
    {
        $this->deployment = $deployment;
        return $this;
    }

    public function refreshImagesAndTags()
    {
        $this->images = collect();
        $this->tags = collect();
        $this->children = collect();
        return $this;
    }

    // form message data
    public function setMessageData($messageData)
    {
        $this->messageData = $messageData;
        return $this;
    }

    // form node id => db node id
    public function setNodeIdComparison($nodeIdComparison)
    {
        $this->nodeIdComparison = $nodeIdComparison;
        return $this;
    }

    // form node id => next message
    public function setNodeIdMessageComparison($nodeIdMessageComparison)
    {
        $this->nodeIdMessageComparison = $nodeIdMessageComparison;
        return $this;
    }

    public function setShouldTag($shouldTag){
        $this->shouldTag = $shouldTag;
    }

    /* ========== 執行動作 ========== */

    public function get()
    {
        $this->format();
        return $this->formatedMessage;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function getVideos()
    {
        return $this->videos;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function getNodeIdMessageComparison()
    {
        return $this->nodeIdMessageComparison;
    }

    public function getParsedMessagesData()
    {
        return $this->messageData;
    }

    public function format()
    {
        $quickReply = NULL;
        $this->formatedMessage = [];

        foreach($this->messageData as &$message){
            if($message['type'] != 'quickreply'){
                $this->formatedMessage[] = $this->{'get'.ucfirst($message['type']).'Message'}($message['message']);
            }else{
                $quickReply = $this->{'get'.ucfirst($message['type']).'Message'}($message['message']);
            }
        }

        if($quickReply){
            $this->formatedMessage[count($this->formatedMessage)-1]['quickReply'] = $quickReply['quickReply'];
        }

    }

    /* ========== 訊息格式 ========== */

    public function getTextMessage(&$message)
    {
        return [
            'type' => 'text',
            'text' => $message['text']['text'],
        ];
    }

    public function getImageMessage(&$message)
    {
        if(!preg_match($this->imagePregMatch, $message['image']['src'])){
            // 上傳圖片
            $storage = Storage::disk('gcs');
            $path = $storage->put('deployments/'.$this->deployment->id.'/messages', $message['image']['file']);

            $image = Image::create([
                'name' => 'LINE訊息圖片',
                'disk' => 'gcs',
                'path' => $path,
            ]);

            $this->images->push($image);
            $message['image']['src'] = $storage->url($path);
        }

        return [
            'type' => 'image',
            'originalContentUrl' => $message['image']['src'],
            'previewImageUrl' => $message['image']['src'],
        ];
    }

    public function getVideoMessage(&$message)
    {
        $storage = Storage::disk('gcs');

        if(!preg_match($this->imagePregMatch, $message['video']['src'])){
            // 上傳影片
            $path = $storage->putFile('deployments/'.$this->deployment->id.'/messages', $message['video']['file']);

            $video = Video::create([
                'name' => 'LINE訊息影片',
                'disk' => 'gcs',
                'path' => $path,
            ]);

            $this->videos->push($video);
            $message['video']['src'] = $storage->url($path);
        }

        if(!preg_match($this->imagePregMatch, $message['image']['src'])){
            // 根據影片裁切
            if($message['fixedRatio']['active'] == 'true'){
                $image = ImageProcessor::make($message['image']['file']->getRealPath());

                $width = $image->width();
                $height = $image->height();
                $imageRatio = $width / $height;
                $videoRatio = $message['video']['ratio'];

                if($imageRatio != $videoRatio){

                    if($imageRatio > $videoRatio){
                        $image->crop(floor($height * $videoRatio), $height);

                    }elseif($imageRatio < $videoRatio){
                        $image->crop($width, floor($width / $videoRatio));
                    }
                    $image->resize($message['video']['width'], $message['video']['height']);

                    $basePath = $image->basePath();
                    $format = pathinfo($basePath, PATHINFO_EXTENSION);
                    $data = $image->encode($format, null);
                }
            }

            if(isset($data)){
                $path = 'deployments/'.$this->deployment->id.'/messages/'.Guid::create().'.jpg';
                $storage->put($path, $data);
            }else{
                $path = $storage->putFile('deployments/'.$this->deployment->id.'/messages', $message['image']['file']);
            }

            $image = Image::create([
                'name' => 'LINE訊息圖片',
                'disk' => 'gcs',
                'path' => $path,
            ]);

            $this->images->push($image);
            $message['image']['src'] = $storage->url($path);
        }

        return [
            'type' => 'video',
            'originalContentUrl' => $message['video']['src'],
            'previewImageUrl' => $message['image']['src'],
        ];
    }

    public function getImagemapMessage(&$message)
    {
        if(preg_match('/^(https:\/\/storage\.googleapis\.com\/'.env('GOOGLE_CLOUD_STORAGE_BUCKET').'\/.*)\/1040/', $message['image']['src'], $match)){
            $baseUrl = $match[1];
        }else{
            // 上傳圖片
            $storage = Storage::disk('gcs');

            $baseUrl = 'deployments/'.$this->deployment->id.'/messages/'.Guid::create();
            $storage->makeDirectory($baseUrl);

            foreach([240, 300, 460, 700, 1040] as $size){
                $image = ImageProcessor::make($message['image']['file']->getRealPath())->resize($size, $size);
                $storage->put($baseUrl.'/'.$size, $image);

                $path = $image->basePath();
                $format = pathinfo($path, PATHINFO_EXTENSION);
                $data = $image->encode($format, null);

                $storage->put($baseUrl.'/'.$size, $data);
            }

            // 以圖片資料夾當作圖片路徑
            $image = Image::create([
                'name' => 'LINE訊息圖片',
                'disk' => 'gcs',
                'path' => $baseUrl,
            ]);

            $this->images->push($image);
            $baseUrl = $image->getUrl();
            $message['image']['src'] = $baseUrl.'/1040';
        }

        // 區域解析
        if($message['show']['active'] == 'true'){
            $areasHelper = new AreasHelper;
            $areasHelper
            ->setDeployment($this->deployment)
            ->setAreasData($message['area']['area'])
            ->setWidthAndHeight(1040, 1040)
            ->setShouldTag($this->shouldTag)
            ->getArea();

            if($this->shouldTag){
                foreach($areasHelper->getTags() as $tag){
                    $this->tags->push($tag);
                };
            }

            $message['area']['area'] = $areasHelper->getParsedAreasData();
        }else{
            $message['area']['area'] = [];
        }

        return [
            'type' => 'imagemap',
            'baseUrl' => $baseUrl,
            'altText' => $message['altText']['altText'],
            'baseSize' => [
                'width' => 1040,
                'height' => 1040,
            ],
            'actions' => $message['show']['active'] == 'true' ? $areasHelper->get() : [
                [
                    'type' => 'message',
                    'text' => '(hidden area)',
                    'area' => [
                        'x' => 0,
                        'y' => 0,
                        'width' => 1,
                        'height' => 1,
                    ]
                ]
            ],
        ];
    }

    public function getButtonsMessage(&$message)
    {
        $formatedMessage = [
            'type' => 'template',
            'altText' => $message['altText']['altText'],
            'template' => [
                'type' => 'buttons',
                'title' => $message['title']['title'],
                'text' => $message['text']['text'],
                // 'defaultAction' => [
                //     "type": "uri",
                //     "label": "View detail",
                //     "uri": "http://example.com/page/123"
                // ],
                'actions' => [],
            ]
        ];

        if($message['image']['active']['active'] == 'true'){
            if(!preg_match($this->imagePregMatch, $message['image']['src'])){
                // 上傳圖片
                $storage = Storage::disk('gcs');
                $path = $storage->put('deployments/'.$this->deployment->id.'/messages', $message['image']['file']);

                $image = Image::create([
                    'name' => 'LINE訊息圖片',
                    'disk' => 'gcs',
                    'path' => $path,
                ]);

                $this->images->push($image);
                $message['image']['src'] = $storage->url($path);
            }

            $formatedMessage['template']['thumbnailImageUrl'] = $message['image']['src'];
            $formatedMessage['template']['imageAspectRatio'] = $message['image']['ratio']['ratio'];
            $formatedMessage['template']['imageSize'] = 'cover';
            $formatedMessage['template']['imageBackgroundColor'] = '#FFFFFF';
        }

        foreach($message['actions'] as &$action){
            $formatedMessage['template']['actions'][] = $this->{'get'.str_replace('-', '', ucwords($action['type']['value'], '-')).'Action'}($action);
        }

        return $formatedMessage;
    }

    public function getConfirmMessage(&$message)
    {
        $formatedMessage = [
            'type' => 'template',
            'altText' => $message['altText']['altText'],
            'template' => [
                'type' => 'confirm',
                'text' => $message['text']['text'],
                'actions' => [],
            ]
        ];

        foreach($message['actions'] as &$action){
            $formatedMessage['template']['actions'][] = $this->{'get'.str_replace('-', '', ucwords($action['type']['value'], '-')).'Action'}($action);
        }

        return $formatedMessage;
    }

    public function getCarouselMessage(&$message)
    {
        $formatedMessage = [
            'type' => 'template',
            'altText' => $message['altText']['altText'],
            'template' => [
                'type' => 'carousel',
                'columns' => [],
            ],
        ];

        foreach($message['columns']['columns'] as &$column){

            $columnData = [
                'text' => $column['text']['text'],
                // 'defaultAction' => [
                //     'type' => 'uri',
                //     'label' => '',
                //     'uri' => ''
                // ],
                'actions' => [],
            ];

            if($message['image']['active']['active'] == 'true'){
                if(!preg_match($this->imagePregMatch, $column['image']['src'])){
                    // 上傳圖片
                    $storage = Storage::disk('gcs');
                    $path = $storage->put('deployments/'.$this->deployment->id.'/messages', $column['image']['file']);

                    $image = Image::create([
                        'name' => 'LINE訊息圖片',
                        'disk' => 'gcs',
                        'path' => $path,
                    ]);

                    $this->images->push($image);
                    $column['image']['src'] = $storage->url($path);
                }

                $columnData['thumbnailImageUrl'] = $column['image']['src'];
                $columnData['imageBackgroundColor'] = '#FFFFFF';
                $formatedMessage['template']['imageAspectRatio'] = $message['image']['ratio']['ratio'];
                $formatedMessage['template']['imageSize'] = 'cover';
            }

            if($message['title']['active']['active'] == 'true') $columnData['title'] = $column['title']['title'];

            foreach($column['actions'] as &$action){
                $columnData['actions'][] = $this->{'get'.str_replace('-', '', ucwords($action['type']['value'], '-')).'Action'}($action);
            }

            $formatedMessage['template']['columns'][] = $columnData;
        }

        return $formatedMessage;
    }

    public function getImagecarouselMessage(&$message)
    {
        $formatedMessage = [
            'type' => 'template',
            'altText' => $message['altText']['altText'],
            'template' => [
                'type' => 'image_carousel',
                'columns' => [],
            ],
        ];

        foreach($message['columns']['columns'] as &$column){
            if(!preg_match($this->imagePregMatch, $column['image']['src'])){
                // 上傳圖片
                $storage = Storage::disk('gcs');
                $path = $storage->put('deployments/'.$this->deployment->id.'/messages', $column['image']['file']);

                $image = Image::create([
                    'name' => 'LINE訊息圖片',
                    'disk' => 'gcs',
                    'path' => $path,
                ]);

                $this->images->push($image);
                $column['image']['src'] = $storage->url($path);
            }

            $formatedMessage['template']['columns'][] = [
                'imageUrl' => $column['image']['src'],
                'action' => $this->{'get'.ucfirst($column['actions'][0]['type']['value']).'Action'}($column['actions'][0]),
            ];
        }

        return $formatedMessage;
    }

    public function getQuickreplyMessage(&$message)
    {
        $formatedMessage = [
            'quickReply' => [
                'items' => []
            ]
        ];

        foreach($message['items']['items'] as &$item){
            $itemData = [
                'type' => 'action',
            ];

            if(preg_match($this->imagePregMatch, $item['image']['src'])){
                $itemData['imageUrl'] = $item['image']['src'];
            }elseif($item['image']['file']){
                // 上傳圖片
                $storage = Storage::disk('gcs');
                $path = $storage->put('deployments/'.$this->deployment->id.'/messages', $item['image']['file']);

                $image = Image::create([
                    'name' => 'LINE訊息圖片',
                    'disk' => 'gcs',
                    'path' => $path,
                ]);

                $this->images->push($image);
                $item['image']['src'] = $storage->url($path);
                $itemData['imageUrl'] = $item['image']['src'];
            }

            $itemData['action'] = $this->{'get'.ucfirst($item['actions'][0]['type']['value']).'Action'}($item['actions'][0]);
            $formatedMessage['quickReply']['items'][] = $itemData;
        }

        return $formatedMessage;
    }

    public function getCouponMessage(&$message)
    {
        $formatedMessage = [
            'type' => 'coupon',
            'messageType' => $message['type']['type'],
            'imageActive' => $message['image']['active']['active'] == 'true',
        ];

        if($formatedMessage['messageType'] == 'imagemap'){
            $formatedMessage['altText'] = $message['altText']['altText'];
        }

        if($formatedMessage['imageActive']){
            $formatedMessage['x'] = intval($message['position']['x']['x']);
            $formatedMessage['y'] = intval($message['position']['y']['y']);
            $formatedMessage['w'] = intval($message['position']['w']['w']);

            // 上傳圖片
            if(preg_match($this->imagePregMatch, $message['image']['image']['src'])){
                $formatedMessage['imageUrl'] = $message['image']['image']['src'];
            }elseif($message['image']['image']['file']){
                // 上傳圖片
                $storage = Storage::disk('gcs');
                $path = $storage->put('deployments/'.$this->deployment->id.'/messages', $message['image']['image']['file']);

                $image = Image::create([
                    'name' => 'LINE訊息圖片',
                    'disk' => 'gcs',
                    'path' => $path,
                ]);

                $this->images->push($image);
                $message['image']['image']['src'] = $storage->url($path);
                $formatedMessage['imageUrl'] = $message['image']['image']['src'];
            }
        }

        return $formatedMessage;
    }

    /* ========== action格式 ========== */

    private function getNextAction(&$action)
    {
        if($this->shouldTag){
            $tag = Tag::create();
            $this->tags->push($tag);
            $action['tag'] = $tag->id;
        }

        $this->children->push($this->nodeIdComparison[$action['action']['nextId']['nextId']]);

        return [
            'type' => 'postback',
            'label' => $action['action']['label']['label'],
            'data' => json_encode([
                'type' => 'script',
                'next' => $this->nodeIdComparison[$action['action']['nextId']['nextId']],
                'tag_id' => $this->shouldTag ? $tag->id : NULL,
            ], JSON_NUMERIC_CHECK),
        ];
    }

    private function getNextWithMessageAction(&$action)
    {
        $this->children->push($this->nodeIdComparison[$action['action']['nextId']['nextId']]);
        $this->nodeIdMessageComparison[$action['action']['nextId']['nextId']] = $action['action']['text']['text'];

        return [
            'type' => 'message',
            'label' => $action['action']['label']['label'],
            'text' => $action['action']['text']['text'],
        ];
    }

    private function getMessageAction(&$action)
    {
        if($this->shouldTag){
            $tag = Tag::create();
            $this->tags->push($tag);

            $action['tag'] = $tag->id;
        }

        return [
            'type' => 'postback',
            'label' => $action['action']['label']['label'],
            'data' => json_encode([
                'type' => 'message',
                'tag_id' => $this->shouldTag ? $tag->id : NULL,
                'text' => $action['action']['text']['text'],
            ], JSON_UNESCAPED_UNICODE),
        ];
    }

    private function getKeywordAction(&$action)
    {
        if($action['action']['keyword']['value']){
            $text = Keyword::find($action['action']['keyword']['value']['value'])->name;
        }else{
            $text = $action['action']['text']['text'];
        }

        return [
            'type' => 'message',
            'label' => $action['action']['label']['label'],
            'text' => $text,
        ];
    }

    private function getUriAction(&$action)
    {
        return [
            'type' => 'uri',
            'label' => $action['action']['label']['label'],
            'uri' => $action['action']['uri']['uri'],
        ];
    }

    private function getLiffAction(&$action)
    {
        $sizeTransfer = [
            50 => 'compact',
            80 => 'tall',
            100 => 'full',
        ];

        $liff = Liff::where('is_created_by_platform', 1)
        ->where('view->type', $sizeTransfer[$action['action']['size']['value']['value']])
        ->first();

        if($this->shouldTag){
            $tag = Tag::create();
            $this->tags->push($tag);

            $action['tag'] = $tag->id;
            $url = 'line://app/'.$liff->liff_id.'?solution='.$this->deployment->solution_id.'&deployment='.$this->deployment->id.'&tag='.$tag->id.'&redirectUrl='.$action['action']['uri']['uri'];
        }else{
            'line://app/'.$liff->liff_id.'?solution='.$this->deployment->solution_id.'&deployment='.$this->deployment->id.'&redirectUrl='.$action['action']['uri']['uri'];
        }

        return [
            'type' => 'uri',
            'label' => $action['action']['label']['label'],
            'uri' => $url,
        ];
    }

    private function getScriptAction(&$action)
    {
        $script = Script::find($action['action']['script']['value']['value']);

        if($this->shouldTag){
            $tag = Tag::create();
            $this->tags->push($tag);

            $action['tag'] = $tag->id;
        }

        return [
            'type' => 'postback',
            'label' => $action['action']['label']['label'],
            'data' => json_encode([
                'type' => 'script',
                'next' => $script->nodes()->where('start', 1)->first()->id,
                'tag_id' => $this->shouldTag ? $tag->id : NULL,
            ], JSON_NUMERIC_CHECK),
        ];
    }

    private function getRichmenuAction(&$action)
    {
        $richmenu = Richmenu::find($action['action']['richmenu']['value']['value']);

        if($this->shouldTag){
            $tag = Tag::create();
            $this->tags->push($tag);

            $action['tag'] = $tag->id;
        }

        return [
            'type' => 'postback',
            'label' => $action['action']['label']['label'],
            'data' => json_encode([
                'type' => 'richmenu',
                'tag_id' => $this->shouldTag ? $tag->id : NULL,
                'richmenu_id' => $richmenu->id,
            ], JSON_NUMERIC_CHECK),
        ];
    }

    private function getCameraAction(&$action)
    {
        return [
            'type' => 'camera',
            'label' => $action['action']['label']['label'],
        ];
    }

    private function getCameraRollAction(&$action)
    {
        return [
            'type' => 'cameraRoll',
            'label' => $action['action']['label']['label'],
        ];
    }

}
