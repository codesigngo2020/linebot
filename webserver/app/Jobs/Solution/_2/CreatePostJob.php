<?php

namespace App\Jobs\Solution\_2;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_2\Crawler;
use App\Models\Platform\Solution\_2\Post;

// Helper
use App\Helpers\Solution\_2\WordpressCreatePostHelper;

class CreatePostJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
  * Create a new job instance.
  *
  * @return void
  */

  protected $deployment;
  protected $crawler;
  protected $post;
  protected $wordpressCreatePostHelper;

  public function __construct(Deployment $deployment, Crawler $crawler, Post $post)
  {
    $this->deployment = $deployment;
    $this->crawler = $crawler;
    $this->post = $post;
    $this->wordpressCreatePostHelper = new WordpressCreatePostHelper;
  }

  /**
  * Execute the job.
  *
  * @return void
  */
  public function handle()
  {
    $post = Post::find($this->post->id);
    $this->wordpressCreatePostHelper = $this->wordpressCreatePostHelper->setDeployment($this->deployment)
    ->setCrawler($this->crawler)
    ->setPostData($this->post)
    ->createPost();

    $httpCode = $this->wordpressCreatePostHelper->getHttpCode();

    if($httpCode == 200){
      $post_id = $this->wordpressCreatePostHelper->getPostId();

      $post->update([
        'post_id' => $post_id,
        'posted_at' => date('Y-m-d H:i:s'),
        'status' => 'posted',
      ]);

      $this->deployment->solution_2_logs()->create([
        'crawler_id' => $this->crawler->id,
        'type' => 'post-success',
        'class' => 'secondary',
        'message' => 'Create post(#'.$this->post->id.') on wordpress successfully.',
      ]);
    }else{
      $post->status = 'standBy';
      $post->save();

      if($httpCode && $httpCode != 404){
        $errorMessage = $this->wordpressCreatePostHelper->getErrorMessage();
      }else{

        if($httpCode == 404){
          $this->deployment->notifications()->create([
            'type' => 'solution3-404',
            'class' => 'danger',
            'title' => '無法連線上您的 Wordpress 網站',
            'text' => '您的網站回應404，該頁面不存在，已經暫停您的部署，請確認您的網站正常運作且正確啟用外掛後（在「Wordpress設定」可以測試您的外掛是否安裝，且能與網站連線），前往「部署設定」再次啟用您的部署。',
            'start_at' => date('Y-m-d H:i:s'),
          ]);
          $errorMessage = $this->wordpressCreatePostHelper->getErrorMessage();
        }else{
          $errorHandlerContext = $this->wordpressCreatePostHelper->getErrorHandlerContext();

          if($errorHandlerContext['errno'] == 28){
            $this->deployment->notifications()->create([
              'type' => 'solution3-28',
              'class' => 'danger',
              'title' => '無法連線上您的 Wordpress 網站',
              'text' => '因為無法在有效時間(5秒)內連接上您的網站，已經暫停您的部署，請確認您的網站正常運作後，前往「部署設定」再次啟用您的部署。',
              'start_at' => date('Y-m-d H:i:s'),
            ]);
            $errorMessage = $errorHandlerContext['error'];
          }else{
            $errorMessage = $this->wordpressCreatePostHelper->getErrorMessage();
          }
        }

        $this->deployment->status = 'suspend';
        $this->deployment->save();
      }

      $this->deployment->solution_2_logs()->create([
        'crawler_id' => $this->crawler->id,
        'type' => 'post-fail',
        'class' => 'danger',
        'message' => "Create post(#".$this->post->id.") on wordpress unsuccessfully,\nerror message: ".$errorMessage.".",
      ]);
    }
  }

  public function failed()
  {
    $post = Post::find($this->post->id);
    $post->status = 'standBy';
    $post->save();
  }
}
