<?php

namespace App\Jobs\Solution\_2;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

// Model
use App\Models\Platform\Solution\_2\Crawler;

// Helper
use App\Helpers\Solution\_2\InstagramHelper;

class RequestInstagramJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
  * Create a new job instance.
  *
  * @return void
  */

  protected $crawler;
  protected $instagramHelper;

  public function __construct(Crawler $crawler)
  {
    $this->crawler = $crawler;
    $this->instagramHelper = new InstagramHelper;
  }

  /**
  * Execute the job.
  *
  * @return void
  */
  public function handle()
  {
    $this->instagramHelper->setCrawler($this->crawler)
    ->setAccountId()
    ->setPostsCount()
    ->setCrawleredPostsCount()
    ->pushToSchedule();
  }
}
