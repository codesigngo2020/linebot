<?php

namespace App\Jobs\Solution\_3;

use Carbon\Carbon;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

// Model
use App\Models\Platform\Solution\_3\Account;
use App\Models\Platform\Solution\_3\Media;

// Helper
use App\Helpers\Solution\_3\MediaHelper;

// Job
use App\Jobs\Solution\_3\CrawlFirstCommentJob;


class CrawlMediaJob //implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
  * Create a new job instance.
  *
  * @return void
  */

  protected $account;

  public function __construct(Account $account)
  {
    $this->pageInfo = NULL;
    $this->account = $account;
    $this->mediaHelper = new MediaHelper;
  }

  /**
  * Execute the job.
  *
  * @return void
  */

  public function handle()
  {
    $this->mediaHelper->setAccountId($this->account->account_id);

    while(!$this->pageInfo || $this->pageInfo->has_next_page){
      $this->crawlMedias();
    }
  }


  public function failed()
  {
    // 執行失敗
  }



  /* ========== 執行爬蟲 ========== */

  public function crawlMedias()
  {
    if($this->pageInfo && $this->pageInfo->has_next_page){
      $this->mediaHelper = $this->mediaHelper->setEndCursor($this->pageInfo->end_cursor);
    }
    $response = $this->mediaHelper->get();

    if($response['success']){
      $edge_owner_to_timeline_media = $response['response']->data->user->edge_owner_to_timeline_media;
      $this->pageInfo = $edge_owner_to_timeline_media->page_info;

      collect($edge_owner_to_timeline_media->edges)->each(function($media){
        $media = $media->node;

        $media = Media::firstOrCreate(
          ['node_id' => $media->id],
          [
            'account_id' => $this->account->id,
            'is_video' => $media->is_video,
            'text' => $media->edge_media_to_caption->edges[0]->node->text,
            'shortcode' => $media->shortcode,
            'posted_at' => Carbon::parse($media->taken_at_timestamp)->format('Y-m-d H:i:s'),
            'comments_disabled' => $media->comments_disabled,
          ]
        );

        CrawlFirstCommentJob::dispatch($media);//->onQueue('solution2-RequestInstagramJob-'.($crawler->id % $queuesCount + 1));
      });

    }else{
      // 請求失敗
    }
  }


}
