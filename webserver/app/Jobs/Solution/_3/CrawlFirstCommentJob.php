<?php

namespace App\Jobs\Solution\_3;

use Carbon\Carbon;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

// Model
use App\Models\Platform\Solution\_3\Account;
use App\Models\Platform\Solution\_3\Media;
use App\Models\Platform\Solution\_3\Comment;

// Helper
use App\Helpers\Solution\_3\FirstCommentHelper;

// Job
use App\Jobs\Solution\_3\CrawlSecondCommentJob;

class CrawlFirstCommentJob //implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
  * Create a new job instance.
  *
  * @return void
  */

  protected $media;

  public function __construct(Media $media)
  {
    $this->pageInfo = NULL;
    $this->media = $media;
    $this->firstCommentHelper = new FirstCommentHelper;
  }

  /**
  * Execute the job.
  *
  * @return void
  */

  public function handle()
  {
    $this->firstCommentHelper = $this->firstCommentHelper->setShortCode($this->media->shortcode);

    while(!$this->pageInfo || $this->pageInfo->has_next_page){
      $this->crawlComments();
    }
  }


  public function failed()
  {
    // 執行失敗
  }



  /* ========== 執行爬蟲 ========== */

  public function crawlComments()
  {
    if($this->pageInfo && $this->pageInfo->has_next_page){
      $this->mediaHelper = $this->mediaHelper->setEndCursor($this->pageInfo->end_cursor);
    }
    $response = $this->firstCommentHelper->get();

    if($response['success']){
      $edge_media_to_parent_comment = $response['response']->data->shortcode_media->edge_media_to_parent_comment;
      $this->pageInfo = $edge_media_to_parent_comment->page_info;

      collect($edge_media_to_parent_comment->edges)->each(function($comment){
        $comment = $comment->node;

        $account = Account::firstOrCreate(
          ['account_id' => $comment->owner->id],
          [
            'username' => $comment->owner->username,
            'profile_pic' => $comment->owner->profile_pic_url,
          ]
        );

        $_comment = Comment::firstOrCreate(
          ['node_id' => $comment->id],
          [
            'account_id' => $account->id,
            'media_id' => $this->media->id,
            'parent_id' => NULL,
            'text' => $comment->text,
            'posted_at' => Carbon::parse($comment->created_at)->format('Y-m-d H:i:s'),
          ]
        );

        if($comment->edge_threaded_comments->count > 0){
          CrawlSecondCommentJob::dispatch($_comment);//->onQueue('solution2-RequestInstagramJob-'.($crawler->id % $queuesCount + 1));
        }

      });

    }else{
      // 請求失敗
    }
  }


}
