<?php

namespace App\Jobs\Solution\_3;

use Carbon\Carbon;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

// Model
use App\Models\Platform\Solution\_3\Account;
use App\Models\Platform\Solution\_3\Comment;

// Helper
use App\Helpers\Solution\_3\SecondCommentHelper;

class CrawlSecondCommentJob //implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
  * Create a new job instance.
  *
  * @return void
  */

  protected $comment;

  public function __construct(Comment $comment)
  {
    $this->pageInfo = NULL;
    $this->comment = $comment;
    $this->secondCommentHelper = new SecondCommentHelper;
  }

  /**
  * Execute the job.
  *
  * @return void
  */

  public function handle()
  {
    $this->secondCommentHelper = $this->secondCommentHelper->setCommentId($this->comment->node_id);

    while(!$this->pageInfo || $this->pageInfo->has_next_page){
      $this->crawlComments();
    }
  }


  public function failed()
  {
    // 執行失敗
  }



  /* ========== 執行爬蟲 ========== */

  public function crawlComments()
  {
    if($this->pageInfo && $this->pageInfo->has_next_page){
      $this->secondCommentHelper = $this->secondCommentHelper->setEndCursor($this->pageInfo->end_cursor);
    }
    $response = $this->secondCommentHelper->get();

    if($response['success']){
      $edge_threaded_comments = $response['response']->data->comment->edge_threaded_comments;
      $this->pageInfo = $edge_threaded_comments->page_info;

      collect($edge_threaded_comments->edges)->each(function($comment){
        $comment = $comment->node;

        $account = Account::firstOrCreate(
          ['account_id' => $comment->owner->id],
          [
            'username' => $comment->owner->username,
            'profile_pic' => $comment->owner->profile_pic_url,
          ]
        );

        $comment = Comment::firstOrCreate(
          ['node_id' => $comment->id],
          [
            'account_id' => $account->id,
            'media_id' => $this->comment->media_id,
            'parent_id' => $this->comment->id,
            'text' => $comment->text,
            'posted_at' => Carbon::parse($comment->created_at)->format('Y-m-d H:i:s'),
          ]
        );
      });

    }else{
      // 請求失敗
    }
  }


}
