<?php

namespace App\Jobs\Solution\_3;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

// Model
use App\Models\Platform\Solution\_3\Account;

// Helper
use App\Helpers\Solution\_3\ProfileHelper;

// Job
use App\Jobs\Solution\_3\CrawlMediaJob;
use App\Jobs\Solution\_3\CrawlFollowerJob;
use App\Jobs\Solution\_3\CrawlFanJob;

class CrawlAccountJob //implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
  * Create a new job instance.
  *
  * @return void
  */

  protected $account;

  public function __construct(Account $account)
  {
    $this->account = $account;
    $this->profileHelper = new ProfileHelper;
  }

  /**
  * Execute the job.
  *
  * @return void
  */
  public function handle()
  {
    $response = $this->profileHelper->setUserName($this->account->username)->get();
    if($response['success']){
      $profile = $response['response']->graphql->user;

      $this->account->update([
        'account_id' => $profile->id,
        'full_name' => $profile->full_name,
        'is_business_account' => $profile->is_business_account,
        'medias_count' => $profile->edge_owner_to_timeline_media->count,
        'follow_count' => $profile->edge_follow->count,
        'followed_count' => $profile->edge_followed_by->count,
        'is_private' => $profile->is_private,
        'profile_pic' => $profile->profile_pic_url,
        'profile_pic_hd' => $profile->profile_pic_url_hd,
      ]);

      // if(!$this->account->is_private) 設隱私就略過
      // CrawlMediaJob::dispatch($this->account);//->onQueue('solution2-RequestInstagramJob-'.($crawler->id % $queuesCount + 1));
      // CrawlFollowerJob::dispatch($this->account);
      // CrawlFanJob::dispatch($this->account);


    }else{
      // 請求失敗
    }
  }

  public function failed()
  {
    // 執行失敗
  }
}
