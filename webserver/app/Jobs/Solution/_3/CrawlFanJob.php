<?php

namespace App\Jobs\Solution\_3;

use Carbon\Carbon;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

// Model
use App\Models\Platform\Solution\_3\Account;

// Helper
use App\Helpers\Solution\_3\FanHelper;

class CrawlFanJob //implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
  * Create a new job instance.
  *
  * @return void
  */

  protected $account;

  public function __construct(Account $account)
  {
    $this->currentTime = date('Y-m-d H:i:s');
    $this->pageInfo = NULL;
    $this->account = $account;
    $this->fanHelper = new FanHelper;
  }

  /**
  * Execute the job.
  *
  * @return void
  */

  public function handle()
  {
    $this->fanHelper->setAccount($this->account);

    while(!$this->pageInfo || $this->pageInfo->has_next_page){
      $this->crawlFans();
    }

    // $this->account->followers()->wherePivot('updated_at', '<', $this->currentTime)->detach();
  }


  public function failed()
  {
    // 執行失敗
  }



  /* ========== 執行爬蟲 ========== */

  public function crawlFans()
  {
    if($this->pageInfo && $this->pageInfo->has_next_page){
      $this->fanHelper = $this->fanHelper->setEndCursor($this->pageInfo->end_cursor);
    }
    $response = $this->fanHelper->get();

    if($response['success']){
      $edge_followed_by = $response['response']->data->user->edge_followed_by;
      $this->pageInfo = $edge_followed_by->page_info;

      $edge_followed_by = collect($edge_followed_by->edges)->map(function($fan){
        $fan = $fan->node;

        return Account::firstOrCreate([
          'accounts.account_id' => $fan->id,
        ],[
          'username' => $fan->username,
          'full_name' => $fan->full_name,
          'profile_pic' => $fan->profile_pic_url,
          'is_private' => $fan->is_private,
        ]
        )->id;
      });
      $this->account->fans()->syncWithoutDetaching($edge_followed_by);
      $this->account->fans()->updateExistingPivot($edge_followed_by, ['updated_at' => $this->currentTime]);
    }else{
      // 請求失敗
    }
  }


}
