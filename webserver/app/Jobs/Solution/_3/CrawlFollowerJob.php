<?php

namespace App\Jobs\Solution\_3;

use Carbon\Carbon;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

// Model
use App\Models\Platform\Solution\_3\Account;

// Helper
use App\Helpers\Solution\_3\FollowerHelper;

class CrawlFollowerJob //implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
  * Create a new job instance.
  *
  * @return void
  */

  protected $account;

  public function __construct(Account $account)
  {
    $this->currentTime = date('Y-m-d H:i:s');
    $this->pageInfo = NULL;
    $this->account = $account;
    $this->followerHelper = new FollowerHelper;
  }

  /**
  * Execute the job.
  *
  * @return void
  */

  public function handle()
  {
    $this->followerHelper->setAccount($this->account);

    while(!$this->pageInfo || $this->pageInfo->has_next_page){
      $this->crawlFollowers();
    }

    $this->account->followers()->wherePivot('updated_at', '<', $this->currentTime)->detach();
  }


  public function failed()
  {
    // 執行失敗
  }



  /* ========== 執行爬蟲 ========== */

  public function crawlFollowers()
  {
    if($this->pageInfo && $this->pageInfo->has_next_page){
      $this->followerHelper = $this->followerHelper->setEndCursor($this->pageInfo->end_cursor);
    }
    $response = $this->followerHelper->get();

    if($response['success']){
      $edge_follow = $response['response']->data->user->edge_follow;
      $this->pageInfo = $edge_follow->page_info;

      $edge_follow = collect($edge_follow->edges)->map(function($follower){
        $follower = $follower->node;

        return Account::firstOrCreate([
          'accounts.account_id' => $follower->id,
        ],[
          'username' => $follower->username,
          'full_name' => $follower->full_name,
          'profile_pic' => $follower->profile_pic_url,
          'is_private' => $follower->is_private,
        ]
        )->id;
      });
      $this->account->followers()->syncWithoutDetaching($edge_follow);
      $this->account->followers()->updateExistingPivot($edge_follow, ['updated_at' => $this->currentTime]);
    }else{
      // 請求失敗
    }
  }


}
