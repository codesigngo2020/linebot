<?php

namespace App\Models\Platform;

use Hoyvoy\CrossDatabase\Eloquent\Model;

class Category extends Model
{

  protected $connection = 'mysql';
  protected $table = 'categories';
  protected $guarded = ['id'];
  public $timestamps = false;

  public function solutions()
  {
    return $this->morphedByMany('App\Models\Platform\Solution\Solution', 'categoryable');
  }

  public function images()
  {
    return $this->morphedByMany('App\Models\Platform\Image', 'categoryable');
  }

  public function files()
  {
    return $this->morphedByMany('App\Models\Platform\File', 'categoryable');
  }

}
