<?php

namespace App\Models\Platform;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
  protected $connection = 'mysql';
  protected $table = 'files';
  protected $guarded = ['id'];

  public function solutions()
  {
    return $this->morphedByMany('App\Models\Platform\Solution\Solution', 'fileable');
  }

  public function categories()
  {
    return $this->morphToMany('App\Models\Platform\Category', 'categoryable');
  }
}
