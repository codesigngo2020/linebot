<?php

namespace App\Models\Platform\Solution;

use Hoyvoy\CrossDatabase\Eloquent\Model;

class Deployment extends Model
{

    protected $connection = 'mysql';
    protected $table = 'deployments';
    protected $guarded = ['id', 'paypal_agreement_id', 'paypal_agreement_state', 'paypal_agreement_token'];
    protected $casts = [
        'data' => 'object',
    ];


    public function owner()
    {
        return $this->belongsTo('App\Models\Platform\User');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\Platform\User')->withTimestamps();
    }

    public function solution()
    {
        return $this->belongsTo('App\Models\Platform\Solution\Solution');
    }

    public function plan()
    {
        return $this->belongsTo('App\Models\Platform\Solution\Plan');
    }

    // public function notifications()
    // {
    //   return $this->morphMany('App\Models\Platform\Notification', 'notificationable');
    // }


    // solution 1
    // public function solution_1_crawlers()
    // {
    //   return $this->hasMany('App\Models\Platform\Solution\_1\Crawler', 'deployment_id');
    // }

    // solution 2
    // public function solution_2_crawlers()
    // {
    //   return $this->hasMany('App\Models\Platform\Solution\_2\Crawler', 'deployment_id');
    // }
    //
    // public function solution_2_posts()
    // {
    //   return $this->hasManyThrough('App\Models\Platform\Solution\_2\Post', 'App\Models\Platform\Solution\_2\Crawler');
    // }
    //
    // public function solution_2_logs()
    // {
    //   return $this->hasMany('App\Models\Platform\Solution\_2\Log', 'deployment_id');
    // }

    // solution 3
    // public function solution_3_account()
    // {
    //   return $this->hasOne('App\Models\Platform\Solution\_3\Account', 'deployment_id');
    // }

}
