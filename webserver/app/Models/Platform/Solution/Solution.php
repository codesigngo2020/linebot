<?php

namespace App\Models\Platform\Solution;

use Illuminate\Database\Eloquent\Model;

class Solution extends Model
{

  protected $connection = 'mysql';
  protected $table = 'solutions';
  protected $guarded = ['id'];
  public $timestamps = false;

  public function icon()
  {
    return $this->morphToMany('App\Models\Platform\Image', 'imageable')->whereHas('categories', function($q){
      $q->where('name', 'icon');
    })
    ->latest();
  }

  public function files()
  {
    return $this->morphToMany('App\Models\Platform\File', 'fileable');
  }

  public function deployments()
  {
    return $this->hasMany('App\Models\Platform\Solution\Deployment');
  }

  public function categories()
  {
    return $this->morphToMany('App\Models\Platform\Category', 'categoryable');
  }

  public function plans()
  {
    return $this->hasMany('App\Models\Platform\Solution\Plan');
  }

}
