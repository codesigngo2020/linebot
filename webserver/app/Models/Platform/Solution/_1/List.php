<?php

namespace App\Models\Platform\Solution\_1;

use Illuminate\Database\Eloquent\Model;

class _List extends Model
{
  protected $connection = 'solution_1';
  protected $table = 'lists';
  protected $guarded = ['id'];
  public $timestamps = false;

  public function deployments()
  {
    return $this->hasMany('App\Models\Platform\Solution\Deployment');
  }

}
