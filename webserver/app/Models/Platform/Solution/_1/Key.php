<?php

namespace App\Models\Platform\Solution\_1;

use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
  protected $connection = 'solution_1';
  protected $table = 'keys';
  protected $guarded = ['id'];
  public $timestamps = false;

  public function crawlers()
  {
    return $this->belongsToMany('App\Models\Platform\Solution\_1\Crawler');
  }




}
