<?php

namespace App\Models\Platform\Solution\_1;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  protected $connection = 'solution_1';
  protected $table = 'posts';
  protected $guarded = ['id'];
  protected $dates = [
    'posted_at',
  ];

  public function crawler()
  {
    return $this->belongsTo('App\Models\Platform\Solution\_1\Crawler');
  }

  public function video()
  {
    return $this->belongsTo('App\Models\Platform\Solution\_1\Video');
  }



}
