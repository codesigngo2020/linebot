<?php

namespace App\Models\Platform\Solution\_1;

use Hoyvoy\CrossDatabase\Eloquent\Model;

class Crawler extends Model
{
  protected $connection = 'solution_1';
  protected $table = 'crawlers';
  protected $guarded = ['id'];
  protected $casts = [
    'categories' => 'array',
    'tags' => 'array',
    'options' => 'array',
  ];

  public function deployment()
  {
    return $this->belongsTo('App\Models\Platform\Solution\Deployment');
  }

  public function keys()
  {
    return $this->belongsToMany('App\Models\Platform\Solution\_1\Key');
  }

  public function valid_keys()
  {
    return $this->belongsToMany('App\Models\Platform\Solution\_1\Key')->where('status', 'U');
  }

  public function channel()
  {
    return $this->belongsTo('App\Models\Platform\Solution\_1\Channel');
  }

  public function list()
  {
    return $this->belongsTo('App\Models\Platform\Solution\_1\_List');
  }

  public function posts()
  {
    return $this->hasMany('App\Models\Platform\Solution\_1\Post');
  }

  public function uncrawl_posts()
  {
    return $this->hasMany('App\Models\Platform\Solution\_1\Post')->whereNull('post_id');
  }

}
