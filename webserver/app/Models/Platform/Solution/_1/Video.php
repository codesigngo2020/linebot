<?php

namespace App\Models\Platform\Solution\_1;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
  protected $connection = 'solution_1';
  protected $table = 'videos';
  protected $guarded = ['id'];
  public $timestamps = false;

  public function posts()
  {
    return $this->hasMany('App\Models\Platform\Solution\_1\Post');
  }



}
