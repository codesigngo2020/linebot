<?php

namespace App\Models\Platform\Solution\_4\basic;

// Model
use App\Models\Platform\Solution\_4\Keyword;
use App\Models\Platform\Solution\_4\Richmenu;

trait MessageTrait
{

    // 取得最新 messages_from_data
    public function refreshMessagesFromData()
    {
        $messages_form_data = $this->messages_form_data;

        foreach($messages_form_data as &$message){
            switch($message->type){
                case 'buttons':
                case 'confirm':

                foreach($message->message->actions as &$action){
                    if($action->type->value == 'keyword' && $action->action->keyword->value){
                        $action->action->keyword = $this->getCurrentKeyword($action);
                    }elseif($action->type->value == 'richmenu'){
                        $action->action->richmenu = $this->getCurrentRichmenu($action);
                    }
                }

                break;

                case 'carousel':
                case 'imagecarousel':
                case 'quickreply':

                $columnName = $message->type == 'quickreply' ? 'items' : 'columns';

                foreach($message->message->{$columnName}->{$columnName} as &$column){
                    foreach($column->actions as &$action){
                        if($action->type->value == 'keyword'){
                            $action->action->keyword = $this->getCurrentKeyword($action);
                        }elseif($action->type->value == 'richmenu'){
                            $action->action->richmenu = $this->getCurrentRichmenu($action);
                        }
                    }
                }

                break;

                default:
                # code...
                break;
            }
        }

        return $messages_form_data;
    }

    private function getCurrentKeyword($action)
    {
        if($this->table == 'broadcasts'){
            return Keyword::select('id', 'name')
            ->selectRaw('1 as `is_current_version`')
            ->where('id', $action->action->keyword->value->id)
            ->first();
        }else{
            return Keyword::select('id', 'name', 'is_current_version', 'deleted_all_at')
            ->where('parent_id', $action->action->keyword->value->value)
            ->orderBy('parent_id', 'desc')
            ->orderBy('is_current_version', 'desc')
            ->orderBy('id', 'desc')
            ->first();
        }
    }

    private function getCurrentRichmenu($action)
    {
        if($this->table == 'broadcasts'){
            return Richmenu::select('id', 'name')
            ->selectRaw('1 as `is_current_version`')
            ->where('id', $action->action->richmenu->value->id)
            ->first();
        }else{
            return Richmenu::select('id', 'name', 'is_current_version', 'deleted_all_at')
            ->where('parent_id', $action->action->richmenu->value->value)
            ->orderBy('parent_id', 'desc')
            ->orderBy('is_current_version', 'desc')
            ->orderBy('deleted_at')
            ->orderBy('id', 'desc')
            ->first();
        }
    }

}
