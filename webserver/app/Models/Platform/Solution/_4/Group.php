<?php

namespace App\Models\Platform\Solution\_4;

use DB;
use Hoyvoy\CrossDatabase\Eloquent\Model;

class Group extends Model
{
    protected $connection = 'solution_4';
    protected $table = 'groups';
    protected $guarded = ['id'];

    protected $casts = [
        'circles' => 'object',
        'add_users_id' => 'object',
        'remove_users_id' => 'object',
    ];

    public function users()
    {
        return $this->belongsToMany('App\Models\Platform\Solution\_4\User');
    }


}
