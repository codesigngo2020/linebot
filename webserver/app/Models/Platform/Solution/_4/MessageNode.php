<?php

namespace App\Models\Platform\Solution\_4;

use Hoyvoy\CrossDatabase\Eloquent\Model;

class MessageNode extends Model
{
    protected $connection = 'solution_4';
    protected $table = 'message_nodes';
    protected $guarded = ['id'];

    protected $casts = [
        'messages' => 'object',
    ];

    public function message()
    {
        return $this->belongsTo('App\Models\Platform\Solution\_4\Message');
    }

    public function images()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Image', 'imageable');
    }

    public function videos()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Video', 'videoable');
    }

    public function tags()
    {
        return $this->morphMany('App\Models\Platform\Solution\_4\Tag', 'taggable');
    }

}
