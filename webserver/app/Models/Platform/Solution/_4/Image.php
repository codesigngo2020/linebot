<?php

namespace App\Models\Platform\Solution\_4;

use DB;
use Storage;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $connection = 'solution_4';
    protected $table = 'images';
    protected $guarded = ['id'];

    public function deleteCompletely()
    {
        if($this->disk == 'asset'){
            if(file_exists($this->path)) unlink($this->path);
        }else{
            Storage::disk($this->disk)->delete($this->path);
        }

        DB::connection('solution_4')->table('imageables')->where('image_id', $this->id)->delete();
        return $this->delete();
    }

    // 取得網址
    public function getUrl()
    {
        return $this->disk == 'asset' ? asset($this->path) : Storage::disk($this->disk)->url($this->path);
    }
}
