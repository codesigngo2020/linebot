<?php

namespace App\Models\Platform\Solution\_4;

use DB;
use Hoyvoy\CrossDatabase\Eloquent\Model;

// Helper
use App\Helpers\Solution\_4\Liff\LiffHelper;

class Liff extends Model
{
    protected $connection = 'solution_4';
    protected $table = 'liffs';
    protected $guarded = ['id'];

    protected $casts = [
        'view' => 'object',
        'features' => 'object',
    ];

    public function deleteCompletely($channelAccessToken)
    {
        $liffHelper = new LiffHelper;
        $res = $liffHelper->setAccessToken($channelAccessToken)
        ->setLiff($this)
        ->delete();

        if(!$res['success']) return false;

        return $this->delete();
    }

    /* ========== functions ========== */

    public static function getLiffs()
    {
        return self::where('is_created_by_platform', 1)
        ->get()
        ->map(function($liff){
            switch($liff->view->type){
                case 'full':
                return [
                    'name' => '100%',
                    'value' => 100
                ];
                break;

                case 'tall':
                return [
                    'name' => '80%',
                    'value' => 80
                ];
                break;

                case 'compact':
                return [
                    'name' => '50%',
                    'value' => 50
                ];
                break;

                default:
                # code...
                break;
            }
        })
        ->unique();
    }

}
