<?php

namespace App\Models\Platform\Solution\_4;

use Hoyvoy\CrossDatabase\Eloquent\Model;

class User extends Model
{
    protected $connection = 'solution_4';
    protected $table = 'users';
    protected $guarded = ['id'];

    // 一個tag只取一次，查出這個user有哪些tag用
    public function tags()
    {
        return $this->belongsToMany('App\Models\Platform\Solution\_4\Tag')->distinct('tags.id');
    }

    public function richmenu()
    {
        return $this->belongs('App\Models\Platform\Solution\_4\Richmenu', 'id', 'richmenu_id');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Models\Platform\Solution\_4\Group');
    }

    public function logs()
    {
        return $this->hasMany('App\Models\Platform\Solution\_4\Log');
    }

}
