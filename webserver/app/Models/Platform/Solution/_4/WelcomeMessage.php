<?php

namespace App\Models\Platform\Solution\_4;

use Hoyvoy\CrossDatabase\Eloquent\Model;

// Trait
use App\Models\Platform\Solution\_4\basic\MessageTrait;

class WelcomeMessage extends Model
{

    use MessageTrait;

    protected $connection = 'solution_4';
    protected $table = 'welcome_messages';
    protected $guarded = ['id'];

    protected $casts = [
        'messages' => 'object',
        'messages_form_data' => 'object',
    ];

    public function template()
    {
        return $this->belongsTo('App\Models\Platform\Solution\_4\Message', 'template_id', 'parent_id')
        ->orderBy('is_current_version', 'desc')
        ->orderBy('id', 'desc')
        ->limit(1);
    }

    public function periods()
    {
        return $this->morphMany('App\Models\Platform\Solution\_4\Period', 'periodable');
    }

    public function images()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Image', 'imageable');
    }

    public function videos()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Video', 'videoable');
    }

    public function tags()
    {
        return $this->morphMany('App\Models\Platform\Solution\_4\Tag', 'taggable');
    }

}
