<?php

namespace App\Models\Platform\Solution\_4;

use DB;
use Hoyvoy\CrossDatabase\Eloquent\Model;

class Period extends Model
{
    protected $connection = 'solution_4';
    protected $table = 'periods';
    protected $guarded = ['id'];

    public function periodable()
    {
        return $this->morphTo();
    }

}
