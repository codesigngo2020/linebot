<?php

namespace App\Models\Platform\Solution\_4;

use DB;
use Hoyvoy\CrossDatabase\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class Table extends Model
{

    protected $connection = 'solution_4';
    protected $table = 'tables';
    protected $guarded = ['id'];

    protected $casts = [
        'editable_columns' => 'array',
        'settings' => 'object',
    ];

    /* ========== relations ========== */

    public function category()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Category', 'categoryable')->where('categories.categoryable_type', 'App\Models\Platform\Solution\_4\Keyword')->limit(1);
    }

    /* ========== attributes ========== */

    public static function shouldVersion()
    {
        return false;
    }

    public function getColumnList()
    {
        $orders = ['user_id', 'redeemed_at', 'redemption_url', 'staff_member_id', 'qrcode_url', 'created_at', 'updated_at'];
        return collect(Schema::Connection($this->connection)->getColumnListing($this->table_name))->sortBy(function($column) use($orders){
            if($column == 'id') return -1;

            $index = array_search($column, $orders);
            return $index ? $index : 0;
        })->values();
    }

    /* ========== functions ========== */

    public function getDataCounts()
    {
        return DB::connection($this->connection)
        ->table($this->table_name)
        ->count('id');
    }

    public function deleteCompletely()
    {
        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{

            $this->deleted_at = date('Y-m-d H:i:s');
            $this->save();

            Schema::connection('solution_4')->dropIfExists($this->table_name);

            $this->category()->detach();

            $solution4Connection->commit();
            return true;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }
    }

}
