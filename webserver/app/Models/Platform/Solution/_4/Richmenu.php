<?php

namespace App\Models\Platform\Solution\_4;

use DB;
use Hoyvoy\CrossDatabase\Eloquent\Model;

// Model
use App\Models\Platform\Solution\_4\Image;

// Helper
use App\Helpers\Solution\_4\Richmenu\RichmenuHelper;

class Richmenu extends Model
{
    protected $connection = 'solution_4';
    protected $table = 'richmenus';
    protected $guarded = ['id'];

    protected $casts = [
        'areas_form_data' => 'object',
    ];

    /* ========== relations ========== */

    public function image()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Image', 'imageable')->latest();
    }

    public function users()
    {
        return $this->hasMany('App\Models\Platform\Solution\_4\User', 'richmenu_id', 'id');
    }

    public function tags()
    {
        return $this->morphMany('App\Models\Platform\Solution\_4\Tag', 'taggable');
    }

    /* ========== get attributes ========== */

    public function getVersionsCount()
    {
        return DB::connection($this->connection)
        ->table($this->table)
        ->where('parent_id', $this->parent_id)
        ->count();
    }

    public function getVersions(){
        return DB::connection($this->connection)
        ->table($this->table)
        ->select('id as value', 'is_current_version as active')
        ->selectRaw('CONCAT("v", `version`) as name')
        ->where('parent_id', $this->parent_id)
        ->get();
    }

    public function getImage()
    {
        return Image::select('images.disk', 'images.path')
        ->join('imageables', function($join){
            $join->on('imageables.image_id', '=', 'images.id')
            ->where('imageables.imageable_type', 'App\Models\Platform\Solution\_4\Richmenu')
            ->where('imageables.imageable_id', $this->id);
        })
        ->first();
    }

    /* ========== functions ========== */

    public function deleteCompletely($channelAccessToken)
    {
        $richmenuHelper = new RichmenuHelper;
        $res = $richmenuHelper->setAccessToken($channelAccessToken)
        ->setRichmenuId($this->richmenuId)
        ->delete();

        DB::connection('solution_4')
        ->table('users')
        ->where('richmenu_id', $this->id)
        ->update([
            'richmenu_id' => 0,
        ]);

        $this->default = 0;
        $this->is_current_version = 0;
        $this->deleted_at = date('Y-m-d H:i:s');
        $this->save();

        return true;
    }

    public function deleteAllCompletely($channelAccessToken)
    {
        $richmenuHelper = new RichmenuHelper;
        $richmenuHelper->setAccessToken($channelAccessToken);

        $richmenus = self::where('parent_id', $this->parent_id)
        ->get()
        ->each(function($richmenu) use($richmenuHelper){
            $richmenuHelper->setRichmenuId($richmenu->richmenuId)
            ->delete();
        });

        DB::connection('solution_4')
        ->table('users')
        ->whereIn('richmenu_id', $richmenus->pluck('id'))
        ->update([
            'richmenu_id' => 0,
        ]);

        DB::connection('solution_4')
        ->table('richmenus')
        ->where('parent_id', $this->parent_id)
        ->update([
            'default' => 0,
            'is_current_version' => 0,
            'deleted_at' => date('Y-m-d H:i:s'),
            'deleted_all_at' => date('Y-m-d H:i:s'),
        ]);

        return true;
    }

    // 取得最新 areas_from_data
    public function refreshAreasFromData()
    {
        $areas_form_data = $this->areas_form_data;
        $this->parseRow($areas_form_data);

        return $areas_form_data;
    }

    private function parseRow(&$rows)
    {
        foreach($rows as &$row){
            $this->parseColumn($row);
        }
    }

    private function parseColumn(&$columns)
    {
        foreach($columns as &$column){
            if(isset($column->id)){

                if($column->type->value == 'richmenu'){
                    $column->action->richmenu = self::select('id', 'name', 'is_current_version', 'deleted_all_at')
                    ->where('parent_id', $column->action->richmenu->value->value)
                    ->orderBy('parent_id', 'desc')
                    ->orderBy('is_current_version', 'desc')
                    ->orderBy('deleted_at')
                    ->orderBy('id', 'desc')
                    ->first();
                }

            }else{
                $this->parseRow($column);
            }
        }
    }

}
