<?php

namespace App\Models\Platform\Solution\_4;

use DB;
use Hoyvoy\CrossDatabase\Eloquent\Model;

class Statistics extends Model
{
    protected $connection = 'solution_4';
    protected $table = 'statistics';
    protected $guarded = ['id'];

    protected $casts = [
        'timestamp' => 'datetime',
    ];

}
