<?php

namespace App\Models\Platform\Solution\_4;

use Hoyvoy\CrossDatabase\Eloquent\Model;

// Model
use App\Models\Platform\Solution\Deployment;

class Log extends Model
{
    protected $connection = 'solution_4';
    protected $table = 'logs';
    protected $guarded = ['id'];

    protected $casts = [
        'messages' => 'object',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\Platform\Solution\_4\User');
    }

}
