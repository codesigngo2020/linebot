<?php

namespace App\Models\Platform\Solution\_4;

use DB;
use Hoyvoy\CrossDatabase\Eloquent\Model;

// Model
use App\Models\Platform\Solution\_4\Richmenu;

// Trait
use App\Models\Platform\Solution\_4\basic\MessageTrait;

// Helper
use App\Helpers\Solution\_4\Message\SendHelper;

class Broadcast extends Model
{

    use MessageTrait;

    protected $connection = 'solution_4';
    protected $table = 'broadcasts';
    protected $guarded = ['id'];

    protected $casts = [
        'groups_id' => 'object',
        'users_id' => 'object',
        'messages' => 'object',
        'messages_form_data' => 'object',
        'appointed_at' => 'datetime',
    ];

    /* ========== relations ========== */

    public function images()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Image', 'imageable');
    }

    public function videos()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Video', 'videoable');
    }

    public function tags()
    {
        return $this->morphMany('App\Models\Platform\Solution\_4\Tag', 'taggable');
    }

    public function category()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Category', 'categoryable')->where('categories.categoryable_type', 'App\Models\Platform\Solution\_4\Broadcast')->limit(1);
    }

    /* ========== attributes ========== */

    public static function shouldVersion()
    {
        return false;
    }

    /* ========== functions ========== */

    public function deleteCompletely()
    {
        DB::connection('solution_4')
        ->table('tags')
        ->where('taggable_id', $this->id)
        ->where('taggable_type', 'App\Models\Platform\Solution\_4\Broadcast')
        ->delete();

        $this->category()->detach();

        $this->images->each(function($image){
            $image->deleteCompletely();
        });

        return $this->delete();
    }
}
