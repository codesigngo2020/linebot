<?php

namespace App\Models\Platform\Solution\_4;

use DB;
use Webpatser\Uuid\Uuid;
use Hoyvoy\CrossDatabase\Eloquent\Model;

// Model
use App\Models\Platform\Solution\Deployment;

class Tag extends Model
{
    protected $connection = 'solution_4';
    protected $table = 'tags';
    protected $guarded = ['id'];

    protected $casts = [
        'data' => 'object',
    ];

    // 一個user只取一次
    public function users()
    {
        return $this->belongsToMany('App\Models\Platform\Solution\_4\User')->distinct('users.id');
    }

    public function taggable()
    {
        return $this->morphTo();
    }

    // 自動產生 UUID
    public static function boot()
    {
        parent::boot();
        self::creating(function($model){
            $model->uuid = (string) Uuid::generate(4);
        });
    }

    /* ========== 標籤描述 ========== */

    public function getDescription(Deployment $deployment)
    {
        if(!preg_match('/^.*\\\\([a-z-A-Z]+)$/', $this->taggable_type, $matches)) return '';

        switch($matches[1]){
            case 'User':
            if($this->data->type == 'follow'){
                return '加入聊天機器人。';
            }elseif($this->data->type == 'unfollow'){
                return '封鎖聊天機器人。';
            }
            break;

            case 'Richmenu':
            if(isset($this->data->isRelatedToRichmenu) && $this->data->isRelatedToRichmenu == 1){
                $richmenu = $this->taggable()->select('id', 'name')->first();
                return '標籤由主選單<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.richmenus.show', ['deployment' => $deployment, 'richmenuId' => $richmenu, 'tagId' => $this]).'" target="_blank">#'.$richmenu->id.' '.$richmenu->name.'</a>建立，觸發時機：切換到該主選單的瞬間。';
            }else{
                $richmenu = $this->taggable()->select('id', 'name', 'areas_form_data')->first();
                $areaId = $this->getAreaIdByTagId($richmenu->areas_form_data, $this->id);

                return '標籤由主選單<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.richmenus.show', [$deployment, $richmenu]).'" target="_blank">#'.$richmenu->id.' '.$richmenu->name.'</a>'.
                '中的<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.richmenus.show', ['deployment' => $deployment, 'richmenu' => $richmenu, 'tagId' => $this]).'" target="_blank">區域 #'.$areaId.'</a>'.
                '建立，觸發時機：點擊區域的瞬間。';
            }
            break;

            case 'AutoAnswer':
            if(isset($this->data->isRelatedToAutoAnswer) && $this->data->isRelatedToAutoAnswer == 1){
                $autoAnswer = $this->taggable()->select('id', 'name')->first();
                return '標籤由自動回覆訊息<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.autoAnswers.show', ['deployment' => $deployment, 'autoAnswerId' => $autoAnswer, 'tagId' => $this]).'" target="_blank">#'.$autoAnswer->id.' '.$autoAnswer->name.'</a>建立，觸發時機：發出自動回覆訊息的瞬間。';
            }else{
                $autoAnswer = $this->taggable()->select('id', 'name', 'messages_form_data')->first();
                $indexAndType = $this->getMessageIndexAndTypeByTagId($autoAnswer->messages_form_data, $this->id);

                return '標籤由自動回覆訊息<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.autoAnswers.show', [$deployment, $autoAnswer]).'" target="_blank">#'.$autoAnswer->id.' '.$autoAnswer->name.'</a>'.
                '中的<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.autoAnswers.show', ['deployment' => $deployment, 'autoAnswer' => $autoAnswer, 'tagId' => $this]).'" target="_blank">'.
                ($indexAndType['type'] == 'Quick Reply' ? $indexAndType['type'] : '第'.($indexAndType['index']+1).'則訊息（'.$indexAndType['type'].'）').'</a>'.
                '建立，觸發時機：點擊按鈕的瞬間。';
            }
            break;

            case 'Broadcast':
            if(isset($this->data->isRelatedToBroadcast) && $this->data->isRelatedToBroadcast == 1){
                $broadcast = $this->taggable()->select('id', 'name')->first();
                return '標籤由推播訊息<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.broadcasts.show', ['deployment' => $deployment, 'broadcastId' => $broadcast, 'tagId' => $this]).'" target="_blank">#'.$broadcast->id.' '.$broadcast->name.'</a>建立，觸發時機：發出推播訊息的瞬間。';
            }else{
                $broadcast = $this->taggable()->select('id', 'name', 'messages_form_data')->first();
                $indexAndType = $this->getMessageIndexAndTypeByTagId($broadcast->messages_form_data, $this->id);

                return '標籤由推播訊息<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.broadcasts.show', [$deployment, $broadcast]).'" target="_blank">#'.$broadcast->id.' '.$broadcast->name.'</a>'.
                '中的<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.broadcasts.show', ['deployment' => $deployment, 'broadcast' => $broadcast, 'tagId' => $this]).'" target="_blank">'.
                ($indexAndType['type'] == 'Quick Reply' ? $indexAndType['type'] : '第'.($indexAndType['index']+1).'則訊息（'.$indexAndType['type'].'）').'</a>'.
                '建立，觸發時機：點擊按鈕的瞬間。';
            }
            break;

            case 'Script':
            $script = $this->taggable()->select('id', 'name')->first();
            return '標籤由訊息腳本<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.scripts.show', ['deployment' => $deployment, 'scriptId' => $script, 'tagId' => $this]).'" target="_blank">#'.$script->id.' '.$script->name.'</a>建立，觸發時機：一進入訊息腳本的瞬間。';
            break;

            case 'ScriptNode':
            if(isset($this->data->isRelatedToNode) && $this->data->isRelatedToNode == 1){
                $node = $this->taggable()->select('id', 'script_id')->with(['script' => function($q){
                    $q->select('id', 'name');
                }])->first();

                return '標籤由訊息腳本<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.scripts.show', [$deployment, $node->script]).'" target="_blank">#'.$node->script->id.' '.$node->script->name.'</a>'.
                '中的<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.scripts.show', ['deployment' => $deployment, 'scriptId' => $node->script, 'tagId' => $this, 'nodeId' => $node]).'" target="_blank">節點 #'.$node->id.'</a>'.
                '建立，觸發時機：一進入節點的瞬間。';
            }else{
                $node = $this->taggable()->select('id', 'script_id', 'start', 'end')->with(['script' => function($q){
                    $q->select('id', 'name', 'nodes_form_data');
                }])->first();

                foreach($node->script->nodes_form_data as $nodeFormData){

                    if($nodeFormData->start == $node->start && $nodeFormData->end == $node->end){
                        $indexAndType = $this->getMessageIndexAndTypeByTagId($nodeFormData->messages->messages, $this->id);
                        break;
                    }
                }

                return '標籤由訊息腳本<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.scripts.show', [$deployment, $node->script]).'" target="_blank">#'.$node->script->id.' '.$node->script->name.'</a>'.
                '的節點 #'.$node->id.' 中的<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.scripts.show', ['deployment' => $deployment, 'script' => $node->script, 'tagId' => $this, 'nodeId' => $node, 'expandScript' => true]).'" target="_blank">'.
                ($indexAndType['type'] == 'Quick Reply' ? $indexAndType['type'] : '第'.($indexAndType['index']+1).'則訊息（'.$indexAndType['type'].'）').'</a>'.
                '建立，觸發時機：一進入節點的瞬間。';
            }
            break;

            case 'Keyword':
            if(isset($this->data->isRelatedToKeyword) && $this->data->isRelatedToKeyword == 1){
                $keyword = $this->taggable()->select('id', 'name')->first();
                return '標籤由關鍵字<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.keywords.show', ['deployment' => $deployment, 'keywordId' => $keyword, 'tagId' => $this]).'" target="_blank">#'.$keyword->id.' '.$keyword->name.'</a>建立，觸發時機：好友發送關鍵字並觸發的瞬間。';
            }else{
                $keyword = $this->taggable()->select('id', 'name', 'messages_form_data')->first();
                $indexAndType = $this->getMessageIndexAndTypeByTagId($keyword->messages_form_data, $this->id);

                return '標籤由關鍵字<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.keywords.show', [$deployment, $keyword]).'" target="_blank">#'.$keyword->id.' '.$keyword->name.'</a>'.
                '中的<a class="pl-2 pr-2 text-muted text-decoration-underline" href="'.route('solution.4.deployment.keywords.show', ['deployment' => $deployment, 'keyword' => $keyword, 'tagId' => $this]).'" target="_blank">'.
                ($indexAndType['type'] == 'Quick Reply' ? $indexAndType['type'] : '第'.($indexAndType['index']+1).'則訊息（'.$indexAndType['type'].'）').'</a>'.
                '建立，觸發時機：點擊按鈕的瞬間。';
            }
            break;

            default:
            # code...
            break;
        }
    }

    /* ========== 行為歷程 ========== */

    public function getHistoryDescription(Deployment $deployment)
    {
        if(!preg_match('/^.*\\\\([a-z-A-Z]+)$/', $this->taggable_type, $matches)) return '';

        switch($matches[1]){
            case 'User':
            if($this->data->type == 'follow'){
                return [
                    'actionName' => '好友行為',
                    'text' => '加入聊天機器人。',
                ];
            }elseif($this->data->type == 'unfollow'){
                return [
                    'actionName' => '好友行為',
                    'text' => '封鎖聊天機器人。',
                ];
            }

            case 'Richmenu':
            if(isset($this->data->isRelatedToRichmenu) && $this->data->isRelatedToRichmenu == 1){
                $richmenu = $this->taggable()->select('id', 'name')->first();
                return [
                    'actionName' => '選單行為',
                    'text' => '切換主選單至<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.richmenus.show', ['deployment' => $deployment, 'richmenuId' => $richmenu, 'tagId' => $this]).'" target="_blank">#'.$richmenu->id.' '.$richmenu->name.'</a>。',
                ];
            }else{
                $richmenu = $this->taggable()->select('id', 'name', 'areas_form_data')->first();
                $areaId = $this->getAreaIdByTagId($richmenu->areas_form_data, $this->id);

                return [
                    'actionName' => '選單行為',
                    'text' => '點擊主選單<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.richmenus.show', [$deployment, $richmenu]).'" target="_blank">#'.$richmenu->id.' '.$richmenu->name.'</a>'.
                    '中的<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.richmenus.show', ['deployment' => $deployment, 'richmenu' => $richmenu, 'tagId' => $this]).'" target="_blank">區域 #'.$areaId.'</a>。',
                ];
            }
            break;

            case 'AutoAnswer':
            if(isset($this->data->isRelatedToAutoAnswer) && $this->data->isRelatedToAutoAnswer == 1){
                $autoAnswer = $this->taggable()->select('id', 'name')->first();
                return [
                    'actionName' => '自動回覆行為',
                    'text' => '收到自動回覆訊息<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.autoAnswers.show', ['deployment' => $deployment, 'autoAnswerId' => $autoAnswer, 'tagId' => $this]).'" target="_blank">#'.$autoAnswer->id.' '.$autoAnswer->name.'</a>。',
                ];
            }else{
                $autoAnswer = $this->taggable()->select('id', 'name', 'messages_form_data')->first();
                $indexAndType = $this->getMessageIndexAndTypeByTagId($autoAnswer->messages_form_data, $this->id);

                return [
                    'actionName' => '自動回覆行為',
                    'text' => '點擊自動回覆訊息<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.autoAnswers.show', [$deployment, $autoAnswer]).'" target="_blank">#'.$autoAnswer->id.' '.$autoAnswer->name.'</a>'.
                    '中<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.autoAnswers.show', ['deployment' => $deployment, 'autoAnswer' => $autoAnswer, 'tagId' => $this]).'" target="_blank">'.
                    ($indexAndType['type'] == 'Quick Reply' ? $indexAndType['type'] : '第'.($indexAndType['index']+1).'則訊息（'.$indexAndType['type'].'）').'</a>的按鈕。',
                ];
            }
            break;

            case 'Broadcast':
            if(isset($this->data->isRelatedToBroadcast) && $this->data->isRelatedToBroadcast == 1){
                $broadcast = $this->taggable()->select('id', 'name')->first();
                return [
                    'actionName' => '推播訊息行為',
                    'text' => '收到推播訊息<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.broadcasts.show', ['deployment' => $deployment, 'broadcastId' => $broadcast, 'tagId' => $this]).'" target="_blank">#'.$broadcast->id.' '.$broadcast->name.'</a>。',
                ];
            }else{
                $broadcast = $this->taggable()->select('id', 'name', 'messages_form_data')->first();
                $indexAndType = $this->getMessageIndexAndTypeByTagId($broadcast->messages_form_data, $this->id);

                return [
                    'actionName' => '推播訊息行為',
                    'text' => '點擊推播訊息<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.broadcasts.show', [$deployment, $broadcast]).'" target="_blank">#'.$broadcast->id.' '.$broadcast->name.'</a>'.
                    '中<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.broadcasts.show', ['deployment' => $deployment, 'broadcast' => $broadcast, 'tagId' => $this]).'" target="_blank">'.
                    ($indexAndType['type'] == 'Quick Reply' ? $indexAndType['type'] : '第'.($indexAndType['index']+1).'則訊息（'.$indexAndType['type'].'）').'</a>的按鈕。',
                ];
            }
            break;

            case 'Script':
            $script = $this->taggable()->select('id', 'name')->first();
            return [
                'actionName' => '訊息腳本行為',
                'text' => '開啟訊息腳本<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.scripts.show', ['deployment' => $deployment, 'scriptId' => $script, 'tagId' => $this]).'" target="_blank">#'.$script->id.' '.$script->name.'</a>。',
            ];
            break;

            case 'ScriptNode':
            if(isset($this->data->isRelatedToNode) && $this->data->isRelatedToNode == 1){
                $node = $this->taggable()->select('id', 'script_id')->with(['script' => function($q){
                    $q->select('id', 'name');
                }])->first();

                return [
                    'actionName' => '訊息腳本行為',
                    'text' => '進入訊息腳本<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.scripts.show', [$deployment, $node->script]).'" target="_blank">#'.$node->script->id.' '.$node->script->name.'</a>'.
                    '中的<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.scripts.show', ['deployment' => $deployment, 'scriptId' => $node->script, 'tagId' => $this, 'nodeId' => $node]).'" target="_blank">節點 #'.$node->id.'</a>。',
                ];
            }else{
                $node = $this->taggable()->select('id', 'script_id', 'start', 'end')->with(['script' => function($q){
                    $q->select('id', 'name', 'nodes_form_data');
                }])->first();

                foreach($node->script->nodes_form_data as $nodeFormData){

                    if($nodeFormData->start == $node->start && $nodeFormData->end == $node->end){
                        $indexAndType = $this->getMessageIndexAndTypeByTagId($nodeFormData->messages->messages, $this->id);
                        break;
                    }
                }

                return [
                    'actionName' => '訊息腳本行為',
                    'text' => '點擊訊息腳本<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.scripts.show', [$deployment, $node->script]).'" target="_blank">#'.$node->script->id.' '.$node->script->name.'</a>'.
                    '的節點 #'.$node->id.' 中<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.scripts.show', ['deployment' => $deployment, 'script' => $node->script, 'tagId' => $this, 'nodeId' => $node, 'expandScript' => true]).'" target="_blank">'.
                    ($indexAndType['type'] == 'Quick Reply' ? $indexAndType['type'] : '第'.($indexAndType['index']+1).'則訊息（'.$indexAndType['type'].'）').'</a>的按鈕。',
                ];
            }
            break;

            case 'Keyword':
            if(isset($this->data->isRelatedToKeyword) && $this->data->isRelatedToKeyword == 1){
                $keyword = $this->taggable()->select('id', 'name')->first();
                return [
                    'actionName' => '關鍵字行為',
                    'text' => '觸發關鍵字<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.keywords.show', ['deployment' => $deployment, 'keywordId' => $keyword, 'tagId' => $this]).'" target="_blank">#'.$keyword->id.' '.$keyword->name.'</a>。',
                ];
            }else{
                $keyword = $this->taggable()->select('id', 'name', 'messages_form_data')->first();
                $indexAndType = $this->getMessageIndexAndTypeByTagId($keyword->messages_form_data, $this->id);

                return [
                    'actionName' => '推播訊息行為',
                    'text' => '點擊關鍵字回覆訊息<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.keywords.show', [$deployment, $keyword]).'" target="_blank">#'.$keyword->id.' '.$keyword->name.'</a>'.
                    '中<a class="pl-2 pr-2 text-secondary text-decoration-underline" href="'.route('solution.4.deployment.keywords.show', ['deployment' => $deployment, 'keyword' => $keyword, 'tagId' => $this]).'" target="_blank">'.
                    ($indexAndType['type'] == 'Quick Reply' ? $indexAndType['type'] : '第'.($indexAndType['index']+1).'則訊息（'.$indexAndType['type'].'）').'</a>的按鈕。',
                ];
            }
            break;

            default:
            # code...
            break;
        }
    }

    private function recurseArea(&$areaId, $tagId, $areas){
        foreach($areas as $area){
            if(isset($area->id)){
                if(isset($area->tag) && $area->tag == $tagId){
                    $areaId = $area->id;
                    return;
                }
            }else if(isset($area[0])){
                $this->recurseArea($areaId, $tagId, $area);
            }
        }
    }

    private function getAreaIdByTagId($areas_form_data, $tagId)
    {
        $areaId = NULL;
        $this->recurseArea($areaId, $tagId, $areas_form_data);
        return $areaId;
    }

    private function recurseImagemapArea(&$messageData, $tagId, $messageIndex, $areas){
        foreach($areas as $area){
            if(isset($area->id)){
                if(isset($area->tag) && $area->tag == $tagId){
                    $messageData = [
                        'index' => $messageIndex,
                        'type' => 'Imagemap',
                    ];
                    return;
                }
            }else if(isset($area[0])){
                $this->recurseImagemapArea($messageData, $tagId, $messageIndex, $area);
            }
        }
    }

    private function getMessageIndexAndTypeByTagId($messages_form_data, $tagId)
    {
        foreach($messages_form_data as $messageIndex => &$message){
            switch($message->type){
                case 'imagemap':
                $messageData = [];
                $this->recurseImagemapArea($messageData, $tagId, $messageIndex, $message->message->area->area);
                return $messageData;
                break;

                case 'buttons':
                case 'confirm':
                foreach($message->message->actions as &$action){
                    if(isset($action->tag) && $action->tag == $tagId){
                        return [
                            'index' => $messageIndex,
                            'type' => ucfirst($message->type),
                        ];
                    };
                }
                break;

                case 'carousel':
                case 'imagecarousel':
                case 'quickreply':
                $columnName = $message->type == 'quickreply' ? 'items' : 'columns';
                foreach($message->message->{$columnName}->{$columnName} as $column){
                    foreach($column->actions as $action){
                        if(isset($action->tag) && $action->tag == $tagId){
                            return [
                                'index' => $messageIndex,
                                'type' => $message->type == 'quickreply' ? 'Quick Reply' : ucfirst($message->type),
                            ];
                        };
                    }
                }
                break;
            }
        }
    }

}
