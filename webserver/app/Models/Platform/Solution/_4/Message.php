<?php

namespace App\Models\Platform\Solution\_4;

use DB;
use Hoyvoy\CrossDatabase\Eloquent\Model;

// Trait
use App\Models\Platform\Solution\_4\basic\MessageTrait;

class Message extends Model
{

    use MessageTrait;

    protected $connection = 'solution_4';
    protected $table = 'messages';
    protected $guarded = ['id'];

    protected $casts = [
        'messages' => 'object',
        'messages_form_data' => 'object',
        'nodes_form_data' => 'object',
        'settings' => 'object',
    ];

    /* ========== relations ========== */

    public function nodes()
    {
        return $this->hasMany('App\Models\Platform\Solution\_4\MessageNode');
    }

    public function tags()
    {
        return $this->morphMany('App\Models\Platform\Solution\_4\Tag', 'taggable');
    }

    public function images()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Image', 'imageable');
    }

    public function videos()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Video', 'videoable');
    }

    public function category()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Category', 'categoryable')->where('categories.categoryable_type', 'App\Models\Platform\Solution\_4\Message')->limit(1);
    }

    /* ========== get attributes ========== */

    public static function shouldVersion()
    {
        return true;
    }

    public function getVersionsCount()
    {
        return DB::connection($this->connection)
        ->table($this->table)
        ->where('parent_id', $this->parent_id)
        ->count();
    }

    public function getVersions(){
        return DB::connection($this->connection)
        ->table($this->table)
        ->select('id as value', 'is_current_version as active')
        ->selectRaw('CONCAT("v", `version`) as name')
        ->where('parent_id', $this->parent_id)
        ->get();
    }

    /* ========== functions ========== */

    public function deleteAllCompletely()
    {
        return DB::connection($this->connection)
        ->table($this->table)
        ->where('parent_id', $this->parent_id)
        ->update([
            'is_current_version' => 0,
            'deleted_all_at' => date('Y-m-d H:i:s'),
        ]);
    }

    // 取得最新 nodes_from_data
    public function refreshNodesFromData()
    {
        $nodes_form_data = $this->nodes_form_data;

        foreach($nodes_form_data as &$node){
            foreach($node->messages->messages as &$message){
                switch($message->type){
                    case 'buttons':
                    case 'confirm':

                    foreach($message->message->actions as &$action){
                        if($action->type->value == 'keyword' && $action->action->keyword->value){
                            $action->action->keyword = $this->getCurrentKeyword($action);
                        }elseif($action->type->value == 'richmenu'){
                            $action->action->richmenu = $this->getCurrentRichmenu($action);
                        }
                    }

                    break;

                    case 'carousel':
                    case 'imagecarousel':
                    case 'quickreply':

                    $columnName = $message->type == 'quickreply' ? 'items' : 'columns';

                    foreach($message->message->actions as &$action){
                        if($action->type->value == 'keyword' && $action->action->keyword->value){
                            $action->action->keyword = $this->getCurrentKeyword($action);
                        }elseif($action->type->value == 'richmenu'){
                            $action->action->richmenu = $this->getCurrentRichmenu($action);
                        }
                    }

                    break;

                    default:
                    # code...
                    break;
                }
            }
        }

        return $nodes_form_data;
    }

    private function getCurrentKeyword($action)
    {
        return Keyword::select('id', 'name', 'is_current_version', 'deleted_all_at')
        ->where('parent_id', $action->action->keyword->value->value)
        ->orderBy('parent_id', 'desc')
        ->orderBy('is_current_version', 'desc')
        ->orderBy('id', 'desc')
        ->first();
    }

    private function getCurrentRichmenu($action)
    {
        return Richmenu::select('id', 'name', 'is_current_version', 'deleted_all_at')
        ->where('parent_id', $action->action->richmenu->value->value)
        ->orderBy('parent_id', 'desc')
        ->orderBy('is_current_version', 'desc')
        ->orderBy('deleted_at')
        ->orderBy('id', 'desc')
        ->first();
    }

}
