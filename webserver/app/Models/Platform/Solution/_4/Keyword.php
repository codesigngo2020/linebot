<?php

namespace App\Models\Platform\Solution\_4;

use DB;
use Hoyvoy\CrossDatabase\Eloquent\Model;

// Trait
use App\Models\Platform\Solution\_4\basic\MessageTrait;

class Keyword extends Model
{

    use MessageTrait;

    protected $connection = 'solution_4';
    protected $table = 'keywords';
    protected $guarded = ['id'];

    protected $casts = [
        'messages' => 'object',
        'messages_form_data' => 'object',
    ];

    /* ========== relations ========== */

    public function template()
    {
        return $this->belongsTo('App\Models\Platform\Solution\_4\Message', 'template_id', 'parent_id')
        ->orderBy('is_current_version', 'desc')
        ->orderBy('id', 'desc')
        ->limit(1);
    }

    public function images()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Image', 'imageable');
    }

    public function videos()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Video', 'videoable');
    }

    public function tags()
    {
        return $this->morphMany('App\Models\Platform\Solution\_4\Tag', 'taggable');
    }

    public function category()
    {
        return $this->morphToMany('App\Models\Platform\Solution\_4\Category', 'categoryable')->where('categories.categoryable_type', 'App\Models\Platform\Solution\_4\Keyword')->limit(1);
    }

    /* ========== get attributes ========== */

    public static function shouldVersion()
    {
        return true;
    }

    public function getVersionsCount()
    {
        return DB::connection($this->connection)
        ->table($this->table)
        ->where('parent_id', $this->parent_id)
        ->count();
    }

    public function getVersions(){
        return DB::connection($this->connection)
        ->table($this->table)
        ->select('id as value', 'is_current_version as active')
        ->selectRaw('CONCAT("v", `version`) as name')
        ->where('parent_id', $this->parent_id)
        ->get();
    }

    /* ========== functions ========== */

    public function toggleActive()
    {
        return DB::connection($this->connection)
        ->table($this->table)
        ->where('parent_id', $this->parent_id)
        ->update([
            'active' => !$this->active,
        ]);
    }

    public function deleteAllCompletely()
    {
        return DB::connection($this->connection)
        ->table($this->table)
        ->where('parent_id', $this->parent_id)
        ->update([
            'active' => 0,
            'is_current_version' => 0,
            'deleted_all_at' => date('Y-m-d H:i:s'),
        ]);
    }

}
