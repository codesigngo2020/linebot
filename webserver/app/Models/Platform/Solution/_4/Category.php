<?php

namespace App\Models\Platform\Solution\_4;

use Hoyvoy\CrossDatabase\Eloquent\Model;

class Category extends Model
{

    protected $connection = 'solution_4';
    protected $table = 'categories';
    protected $guarded = ['id'];

    public function categoryables()
    {
        return $this->morphedByMany($this->categoryable_type, 'categoryable');
    }

}
