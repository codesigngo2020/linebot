<?php

namespace App\Models\Platform\Solution\_2;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
  protected $connection = 'solution_2';
  protected $table = 'accounts';
  protected $guarded = ['id'];
  public $timestamps = false;

  public function crawlers()
  {
    return $this->hasMany('App\Models\Platform\Solution\_2\Crawler');
  }

}
