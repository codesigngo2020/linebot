<?php

namespace App\Models\Platform\Solution\_2;

use Hoyvoy\CrossDatabase\Eloquent\Model;

class Log extends Model
{
  protected $connection = 'solution_2';
  protected $table = 'logs';
  protected $guarded = ['id'];
  protected $casts = [
    'data' => 'object',
  ];

  public function deployment()
  {
    return $this->belongsTo('App\Models\Platform\Solution\Deployment');
  }

  public function crawler()
  {
    return $this->belongsTo('App\Models\Platform\Solution\_2\Crawler');
  }
}
