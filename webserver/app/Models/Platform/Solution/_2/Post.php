<?php

namespace App\Models\Platform\Solution\_2;

use Hoyvoy\CrossDatabase\Eloquent\Model;

class Post extends Model
{
  protected $connection = 'solution_2';
  protected $table = 'posts';
  protected $guarded = ['id'];
  protected $casts = [
    'data' => 'object',
  ];
  protected $dates = [
    'posted_at',
  ];

  public function crawler()
  {
    return $this->belongsTo('App\Models\Platform\Solution\_2\Crawler');
  }



}
