<?php

namespace App\Models\Platform\Solution\_2;

use Hoyvoy\CrossDatabase\Eloquent\Model;

class Crawler extends Model
{
  protected $connection = 'solution_2';
  protected $table = 'crawlers';
  protected $guarded = ['id'];
  protected $casts = [
    'categories' => 'array',
    'tags' => 'array',
    'options' => 'array',
  ];

  public function deployment()
  {
    return $this->belongsTo('App\Models\Platform\Solution\Deployment');
  }

  public function account()
  {
    return $this->belongsTo('App\Models\Platform\Solution\_2\Account');
  }

  public function posts()
  {
    return $this->hasMany('App\Models\Platform\Solution\_2\Post');
  }

  public function uncrawl_posts()
  {
    return $this->hasMany('App\Models\Platform\Solution\_2\Post')->whereNull('post_id');
  }

  public function logs()
  {
    return $this->hasMany('App\Models\Platform\Solution\_2\Log');
  }

}
