<?php

namespace App\Models\Platform\Solution\_3;

use Hoyvoy\CrossDatabase\Eloquent\Model;

class Comment extends Model
{
  protected $connection = 'solution_3';
  protected $table = 'comments';
  protected $guarded = ['id'];
  protected $dates = [
    'posted_at',
  ];

  public function account()
  {
    return $this->belongsTo('App\Models\Platform\Solution\_3\Account');
  }

  public function media()
  {
    return $this->belongsTo('App\Models\Platform\Solution\_3\Media');
  }
}
