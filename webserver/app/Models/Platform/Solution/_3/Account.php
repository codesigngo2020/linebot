<?php

namespace App\Models\Platform\Solution\_3;

use Hoyvoy\CrossDatabase\Eloquent\Model;

class Account extends Model
{
  protected $connection = 'solution_3';
  protected $table = 'accounts';
  protected $guarded = ['id'];

  public function deployment()
  {
    return $this->belongsTo('App\Models\Platform\Solution\Deployment');
  }

  public function medias()
  {
    return $this->hasMany('App\Models\Platform\Solution\_3\Media');
  }

  public function comments()
  {
    return $this->hasMany('App\Models\Platform\Solution\_3\Comment');
  }

  public function followers()
  {
    return $this->belongsToMany('App\Models\Platform\Solution\_3\Account', 'account_follower', 'account_id', 'follower_id')->withTimestamps();
  }

  public function fans()
  {
    return $this->belongsToMany('App\Models\Platform\Solution\_3\Account', 'account_fan', 'account_id', 'fan_id')->withTimestamps();
  }

}
