<?php

namespace App\Models\Platform\Solution\_3;

use Hoyvoy\CrossDatabase\Eloquent\Model;

class Media extends Model
{
  protected $connection = 'solution_3';
  protected $table = 'medias';
  protected $guarded = ['id'];
  protected $dates = [
    'posted_at',
  ];

  public function account()
  {
    return $this->belongsTo('App\Models\Platform\Solution\_3\Account');
  }



}
