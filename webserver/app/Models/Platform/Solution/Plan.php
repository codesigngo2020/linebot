<?php

namespace App\Models\Platform\Solution;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{

  protected $connection = 'mysql';
  protected $table = 'plans';
  protected $guarded = ['id'];
  public $timestamps = false;
  protected $casts = [
    'data' => 'object',
  ];

  public function solution()
  {
    return $this->belongsTo('App\Models\Platform\Solution\Solution');
  }

  public function deployments()
  {
    return $this->hasMany('App\Models\Platform\Solution\Deployment');
  }

}
