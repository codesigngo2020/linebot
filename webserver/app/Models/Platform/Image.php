<?php

namespace App\Models\Platform;

use DB;
use Storage;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $connection = 'mysql';
    protected $table = 'images';
    protected $guarded = ['id'];

    public function solutions()
    {
        return $this->morphedByMany('App\Models\Platform\Solution\Solution', 'imageable');
    }

    public function categories()
    {
        return $this->morphToMany('App\Models\Platform\Category', 'categoryable');
    }

    public function deleteCompletely()
    {
        if($this->disk == 'asset'){
            if(file_exists($this->path)) unlink($this->path);
        }else{
            Storage::disk($this->disk)->delete($this->path);
        }

        DB::table('imageables')->where('image_id', $this->id)->delete();
        return $this->delete();
    }

    // 取得網址
    public function getUrl()
    {
        return $this->disk == 'asset' ? asset($this->path) : Storage::disk($this->disk)->url($this->path);
    }
}
