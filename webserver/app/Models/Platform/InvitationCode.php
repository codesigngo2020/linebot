<?php

namespace App\Models\Platform;

use Illuminate\Database\Eloquent\Model;

class InvitationCode extends Model
{

    protected $connection = 'mysql';
    protected $table = 'invitation_codes';
    protected $guarded = ['id'];
    protected $casts = [
        'roles' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\Platform\User');
    }

    public function deployment()
    {
        return $this->belongsTo('App\Models\Platform\Solution\Deployment');
    }

}
