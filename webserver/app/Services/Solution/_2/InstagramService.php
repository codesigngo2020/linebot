<?php

namespace App\Services\Solution\_2;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

// Model
use App\Models\Platform\Option;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_2\Crawler;

// Job
use App\Jobs\Solution\_2\RequestInstagramJob;

// Helper
use App\Helpers\Solution\_2\InstagramHelper;

class InstagramService
{
  public function __construct()
  {
    $this->today = Carbon::today();
    $this->now = Carbon::now();
    $this->diff_in_minutes = $this->now->diffInMinutes($this->today);
  }

  /* ========== 部署表單 ========== */

  #public function setAccessTokenToSession($code)
  #{
  #  $this->instagramHelper = new InstagramHelper;
  #  return $this->instagramHelper->requestAccessToken($code);
  #}

  #public function validateAccessToken($accessToken)
  #{
  #  $this->instagramHelper = new InstagramHelper;
  #  return $this->instagramHelper->validateAccessToken($accessToken);
  #}



  /* ========== 執行爬蟲 ========== */

  public function crawlAll($force = false)
  {
    // $force：true => 忽略頻率限制

    $queuesCount = Option::where('name', 'solution2-RequestInstagramJob-queues-count')->first()->value;
    $today = Carbon::now()->format('Y-m-d');
    $currentTime = Carbon::now()->format('H:i');

    Deployment::where('solution_id', 2)
    ->where('status', 'active')
    ->where(function($q){
      $q->where('paypal_agreement_state', 'Active')
      ->orWhere('plan_id', 1);
    })
    ->with(['solution_2_crawlers' => function($q) use($today){
      $q->select('id', 'deployment_id', 'posts_limit')
      ->where('status', '<>', 'destroyed')
      ->withCount(['logs as posts_count' => function($q) use($today){
        $q->whereDate('created_at', '>=', $today)
        ->where('type', 'post-success');
      }]);
    }])
    ->each(function($deployment) use($force, $currentTime, $queuesCount){
      $postsCount = $deployment->solution_2_crawlers->sum('posts_count');

      if($postsCount < $deployment->plan->data->posts_count){
        $deployment->solution_2_crawlers()
        ->where('active', 1)
        ->where('status', '<>', 'destroyed')
        ->where('status', '<>', 'full')
        ->whereTime('start', '<', $currentTime)
        ->whereTime('end', '>', $currentTime)
        ->with(['deployment' => function($q){
          $q->select('id', 'plan_id');
        }])
        ->each(function($crawler) use($force, $queuesCount){

          if($this->diff_in_minutes % $crawler->frequency == 0 || $force){
            // 驗證請求數
            $requests_limit = $crawler->deployment->plan->data->instagram_requests_count;
            $requests_count = $crawler->deployment->solution_2_logs()
            ->where(function($q){
              $q->where('type', 'crawl-success')
              ->orWhere('type', 'crawl-fail');
            })
            ->whereDate('created_at', '>=', $this->today)
            ->count();

            if($requests_count < $requests_limit || $force){
              RequestInstagramJob::dispatch($crawler)->onQueue('solution2-RequestInstagramJob-'.($crawler->id % $queuesCount + 1));
            }
          }
        });
      }
    });
  }

  public function crawlOne(Crawler $crawler)
  {
    #if($crawler->crawlering == 0){
    #  $crawler->update(['crawlering' => 1]);
    #  $instagramHelper = new InstagramHelper;
    #  $instagramHelper->setCrawler($crawler)->setAccountId()->setPostsCount()->setCrawleredPostsCount()->pushToSchedule();
    #  $crawler->update(['crawlering' => 0]);
    #}
  }








}
