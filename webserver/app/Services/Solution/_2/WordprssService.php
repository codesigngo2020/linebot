<?php

namespace App\Services\Solution\_2;

use DB;
use Carbon\Carbon;

// Model
use App\Models\Platform\Option;
use App\Models\Platform\Solution\Deployment;

// Job
use App\Jobs\Solution\_2\CreatePostJob;

// Helper
use App\Helpers\Solution\_2\WordpressCreatePostHelper;

class WordprssService
{

  public function associate($domain, $key)
  {
    $wordpressCreatePostHelper = new WordpressCreatePostHelper;
    return $wordpressCreatePostHelper->setDomain($domain)
    ->setKey($key)
    ->associate()
    ->status();
  }

  public function createAll()
  {
    $today = Carbon::now()->format('Y-m-d');
    $currentTime = Carbon::now()->format('H:i');
    $queuesCount = Option::where('name', 'solution2-CreatePostJob-queues-count')->first()->value;

    $deployments = Deployment::where('solution_id', 2)
    ->where('status', 'active')
    ->whereTime('data->post->start', '<', $currentTime)
    ->whereTime('data->post->end', '>', $currentTime)
    ->where(function($q){
      $q->where('paypal_agreement_state', 'Active')
      ->orWhere('plan_id', 1);
    })
    ->with('plan')
    ->with(['solution_2_crawlers' => function($q) use($today){
      $q->select('id', 'deployment_id', 'posts_limit')
      ->where('status', '<>', 'destroyed')
      ->withCount(['logs as posts_count' => function($q) use($today){
        $q->whereDate('created_at', '>=', $today)
        ->where('type', 'post-success');
      }]);
    }])
    ->get()
    ->each(function($deployment) use($currentTime, $queuesCount){

      $postsCount = $deployment->solution_2_crawlers->sum('posts_count');
      $startTime = Carbon::parse($deployment->data->post->start);

      if($startTime->diffInMinutes($currentTime) % $deployment->data->post->frequency == 0 && $postsCount < $deployment->plan->data->posts_count){
        $unfilledCrawlers = $deployment->solution_2_crawlers->filter(function($crawler){
          return $crawler->posts_count < $crawler->posts_limit;
        })->pluck('id');

        // 爬蟲文章都達到上限
        if($unfilledCrawlers->isEmpty()) return false;

        $unfilledCrawlers = collect($deployment->data->post->order)->intersect($unfilledCrawlers);

        $post = $deployment->solution_2_posts()
        ->where('posts.status', 'standBy')
        ->whereIn('crawlers.id', $unfilledCrawlers)
        ->orderByRaw("FIELD(crawlers.id, ".$unfilledCrawlers->implode(',').")")
        ->first();

        if($post){
          $post->status = 'posting';
          $post->save();
          CreatePostJob::dispatch($deployment, $post->crawler, $post)->onQueue('solution2-CreatePostJob-'.($deployment->id % $queuesCount + 1));
        }
      }
    });
  }

  public function createAllByCrawler(Crawler $crawler)
  {
    // if($crawler->posting == 0){
    //   $crawler->update(['posting' => 1]);
    //   $this->wordpressCreatePostHelper = new WordpressCreatePostHelper;
    //
    //   $this->wordpressCreatePostHelper->setCrawler($crawler);
    //
    //   $crawler->uncrawl_posts->each(function($post){
    //     $http_code = $this->wordpressCreatePostHelper->setPostData($post)->createPost()->getHttpCode();
    //
    //     if($http_code == 200){
    //       $post_id = $this->wordpressCreatePostHelper->getPostId();
    //       $post->update([
    //         'post_id' => $post_id,
    //         'posted_at' => date('Y-m-d H:i:s'),
    //       ]);
    //     }else{
    //       return false;
    //     }
    //   });
    //   $crawler->update(['posting' => 0]);
    // }
  }


}
