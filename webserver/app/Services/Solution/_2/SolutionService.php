<?php

namespace App\Services\Solution\_2;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\_2\Account;

// Repository
use App\Repositories\Platform\Solution\DeploymentRepository;

// Service
use App\Services\Paypal\PaypalService;

class SolutionService
{
  public function index(Solution $solution)
  {
    $this->deploymentRepository = new DeploymentRepository;
    $deployments = $this->deploymentRepository->findAllByUserAndSolution2WithCrawlersCountAndPagination($solution);
    return $deployments;
  }

  public function store(Request $request, Solution $solution){
    $form = $request->form;

    $domain = $form[2]['domain']['domain'];
    $domain = (strpos($domain, 'http') === 0) ? $domain : 'http://'.$domain;
    $domain = parse_url($domain, PHP_URL_HOST);

    // 寫入deployment
    $post_duration = explode(' - ', $form[4]['duration']['duration']);
    $deployment = auth()->user()->deployments()->create([
      'owner' => auth()->user()->id,
      'solution_id' => $solution->id,
      'plan_id' => $form[1]['plan'],
      'name' => $form[2]['name']['name'],
      'data' => [
        'domain' => $domain,
        'key' => $form[2]['key']['key'],
        'post' => [
          'frequency' => $form[4]['postFrequency']['value'],
          'start' => str_replace(' ', '', $post_duration[0]),
          'end' => str_replace(' ', '', $post_duration[1]),
        ],
      ],
      'status' => 'active',
    ]);

    $order = collect($form[4]['order']['order'])->keyBy('id');

    // 寫入crawler
    $crawlers = collect($form[3]['crawlers'])->each(function($data) use($deployment, &$order){
      $account = Account::firstOrCreate([
        'username' => $data['crawler']['username']['username'],
      ]);

      $duration = explode(' - ', $data['crawler']['duration']['duration']);

      $crawler = $deployment->solution_2_crawlers()->create([
        'type' => $data['crawler_type']['value']['value'],
        'account_id' => $account->id,
        'format' => $data['crawler']['post']['format']['content'],
        'categories' => $data['crawler']['post']['category']['value'],
        'tags' => $data['crawler']['post']['tag']['value'],
        'frequency' => $data['crawler']['frequency']['value'],
        'posts_limit' => $data['crawler']['postsLimit']['value'],
        'options' => $data['crawler']['post']['options']['value'],
        'start' => str_replace(' ', '', $duration[0]),
        'end' => str_replace(' ', '', $duration[1]),
        'active' => 1,
        'status' => 'standBy',
      ]);

      $order[$data['id']] = $crawler;
    });

    $data = $deployment->data;
    $data->post->order = $order->pluck('id')->toArray();
    $deployment->data = $data;
    $deployment->save();

    // create permissions
    $edit_permission = Permission::create([ 'name' => 'edit_deployment_'.$deployment->id]);
    $destroy_permission = Permission::create(['name' => 'destroy_deployment_'.$deployment->id]);

    // create roles
    $superadmin = Role::create(['name' => 'superadmin_for_deployment_'.$deployment->id])->givePermissionTo($edit_permission, $destroy_permission);
    $admin = Role::create(['name' => 'admin_for_deployment_'.$deployment->id])->givePermissionTo($edit_permission);

    // assign user role
    auth()->user()->assignRole($superadmin);

    // create paypal agreement
    $plan = $deployment->plan;
    $approvalUrl = NULL;
    if($plan->price != 0){
      $paypalService = new PaypalService;
      $token = $paypalService->createAgreementAndGetToken($solution, $plan);
      $approvalUrl = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$token;

      $deployment->paypal_agreement_token = $token;
      $deployment->save();
    }

    return compact('deployment', 'approvalUrl');
  }



}
