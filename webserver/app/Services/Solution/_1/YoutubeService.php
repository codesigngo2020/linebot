<?php

namespace App\Services\Solution\_1;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\_1\Crawler;

// Helper
use App\Helpers\Solution\_1\YoutubeRequestHelper;

class YoutubeService
{
  public function __construct()
  {
    $this->today = Carbon::today();
    $this->now = Carbon::now();
    $this->diff_in_minutes = $this->now->diffInMinutes($this->today);
  }

  public function crawlAll($force = false)
  {
    // $force：true => 忽略頻率限制
    Crawler::whereHas('deployment', function($q){
      $q->where('active', 1);
    })
    ->where('active', 1)
    ->get()
    ->each(function($crawler) use($force){
      if($this->diff_in_minutes % $crawler->frequency == 0 || $force) $this->crawlOne($crawler);
    });
  }

  public function crawlOne(Crawler $crawler)
  {
    if($crawler->crawlering == 0){
      $crawler->update(['crawlering' => 1]);
      $youtubeRequestHelper = new YoutubeRequestHelper;
      $youtubeRequestHelper->setCrawler($crawler)->setVideosCount()->setCrawleredPostsCount()->pushToSchedule();
      $crawler->update(['crawlering' => 0]);
    }
  }

}
