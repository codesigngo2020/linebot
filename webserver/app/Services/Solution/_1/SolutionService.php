<?php

namespace App\Services\Solution\_1;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\_1\Channel;
use App\Models\Platform\Solution\_1\_List;
use App\Models\Platform\Solution\_1\Key;

// Repository
use App\Repositories\Platform\Solution\DeploymentRepository;

// Helper
use App\Helpers\Solution\_1\YoutubeRequestHelper;

class SolutionService
{
  public function index(Solution $solution)
  {
    $this->deploymentRepository = new DeploymentRepository;
    $deployments = $this->deploymentRepository->findAllByUserAndSolution1WithCrawlersCountAndPagination($solution);
    return $deployments;
  }

  public function store(Request $request, Solution $solution){
    $this->youtubeRequestHelper = new YoutubeRequestHelper;
    $form = $request->form;

    // 寫入deployment
    $deployment = auth()->user()->deployments()->create([
      'owner' => auth()->user()->id,
      'solution_id' => $solution->id,
      'name' => $form[1]['name']['name'],
      'data' => [
        'domain' => $form[1]['domain']['domain'],
        'key' => $form[1]['key']['key'],
      ],
      'active' => 1,
    ]);

    // 寫入youtube key
    $youtube_keys = collect($form[2]['keys'])->map(function($key){
      return Key::firstOrCreate(
        ['key' => $key['key']['key']],
        ['status' => 'U'],
      );
    });

    // 寫入crawler
    $crawlers = collect($form[3]['crawlers'])->map(function($data) use($deployment, $youtube_keys){
      $crawler_type = $data['crawler_type']['value']['value'];
      $channel_id = $data['crawler']['channel']['id'];

      if($crawler_type == 'channel'){
        $list_id = $this->youtubeRequestHelper->getChannelUploadListId($youtube_keys->first()->key, $channel_id);
      }else{
        $list_id = $data['crawler']['list']['id'];
      }

      $channel = Channel::firstOrCreate(
        ['channel_id' => $channel_id],
        ['title' => $this->youtubeRequestHelper->getChannelTitle($youtube_keys->first()->key, $channel_id)]
      );

      $list = _List::firstOrCreate(
        ['list_id' => $list_id],
        ['title' => $this->youtubeRequestHelper->getListTitle($youtube_keys->first()->key, $list_id)],
      );

      $crawler = $deployment->solution_1_crawlers()->create([
        'type' => $crawler_type,
        'channel_id' => $channel->id,
        'list_id' => $list->id,
        'format' => $data['crawler']['post']['format']['content'],
        'categories' => $data['crawler']['post']['category']['value'],
        'tags' => $data['crawler']['post']['tag']['value'],
        'frequency' => $data['crawler']['post']['frequency']['value'],
        'options' => $data['crawler']['post']['options']['value'],
        'crawlering' => 0,
        'posting' => 0,
        'active' => 0,
      ]);

      $crawler->keys()->syncWithoutDetaching($youtube_keys->pluck('id'));
      return $crawler;
    });

    // create permissions
    $edit_permission = Permission::create([ 'name' => 'edit_deployment_'.$deployment->id]);
    $destroy_permission = Permission::create(['name' => 'destroy_deployment_'.$deployment->id]);

    // create roles
    $superadmin = Role::create(['name' => 'superadmin_for_deployment_'.$deployment->id])->givePermissionTo($edit_permission, $destroy_permission);
    $admin = Role::create(['name' => 'admin_for_deployment_'.$deployment->id])->givePermissionTo($edit_permission);

    // assign user role
    auth()->user()->assignRole($superadmin);

    return $deployment;
  }


}
