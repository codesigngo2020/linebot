<?php

namespace App\Services\Solution\_1;

// Model
use App\Models\Platform\Solution\_1\Crawler;

//Helper
use App\Helpers\Solution\_1\WordpressCreatePostHelper;
use App\Helpers\Solution\_1\YoutubeRequestHelper;

class WordprssService
{

  public function associate($domain, $key)
  {
    $wordpressCreatePostHelper = new WordpressCreatePostHelper;
    return $wordpressCreatePostHelper->setDomain($domain)
    ->setKey($key)
    ->associate()
    ->status();
  }

  public function createAll()
  {
    Crawler::whereHas('deployment', function($q){
      $q->where('active', 1);
    })
    ->where('active', 1)
    ->get()
    ->each(function($crawler){
      $this->createAllByCrawler($crawler);
    });
  }

  public function createAllByCrawler(Crawler $crawler)
  {
    if($crawler->posting == 0){
      $crawler->update(['posting' => 1]);
      $this->wordpressCreatePostHelper = new WordpressCreatePostHelper;
      $this->youtubeRequestHelper = new YoutubeRequestHelper;

      $this->wordpressCreatePostHelper->setCrawler($crawler);
      $this->youtubeRequestHelper->setCrawler($crawler);

      $crawler->uncrawl_posts->each(function($post){
        $video_data = $this->youtubeRequestHelper->getVideoData($post->video);
        $http_code = $this->wordpressCreatePostHelper->setVideoData($post->video->video_id, $video_data)->createPost()->getHttpCode();
        if($http_code == 200){
          $post_id = $this->wordpressCreatePostHelper->getPostId();
          $post->update([
            'post_id' => $post_id,
            'posted_at' => date('Y-m-d H:i:s'),
          ]);
        }else{
          return false;
        }
      });
      $crawler->update(['posting' => 0]);
    }
  }


}
