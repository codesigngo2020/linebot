<?php

namespace App\Services\Solution\_4;

use Config;
use DB;
use PDO;
use PDOException;
use QrCode;
use Storage;
use Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

// Model
use App\Models\Platform\User;
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Image;
use App\Models\Platform\Solution\_4\Liff;
use App\Models\Platform\Solution\_4\Richmenu;
use App\Models\Platform\Solution\_4\Tag;

// Service
use App\Services\Paypal\PaypalService;

// Helper
use App\Helpers\Solution\_4\Liff\LiffHelper;
use App\Helpers\Solution\_4\Richmenu\RichmenuHelper;

class SolutionService
{

    public function __construct()
    {
        $this->roles = ['superadmin', 'social_media_manager', 'customer_service_agent'];
    }

    public function index(Solution $solution)
    {
        $deployments_count = auth()->user()
        ->deployments()
        ->where('solution_id', $solution->id)
        ->count();

        $deployments = auth()->user()
        ->deployments()
        ->select('id', 'name', 'description', 'data->basicId as basic_id', 'data->pictureUrl as picture_url', 'data->QRcodeUrl as QRcodeUrl')
        ->where('solution_id', $solution->id)
        ->limit(5)
        ->get();

        $paginator = collect([
            'currentPage' => 1,
            'lastPage' => ceil($deployments_count / 5),
        ]);

        return compact('deployments', 'paginator');
    }

    public function getDeployments(Request $request, Solution $solution)
    {
        $lastPage = auth()->user()->deployments()->where('solution_id', $solution->id);
        if($request->q) $lastPage = $this->query($lastPage, $request->q);
        $lastPage = ceil($lastPage->count() / 5);

        $page = intval($request->page);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $deployments = auth()->user()
        ->deployments()
        ->select('id', 'name', 'description', 'data->basicId as basic_id', 'data->pictureUrl as picture_url', 'data->QRcodeUrl as QRcodeUrl')
        ->where('solution_id', $solution->id);

        if($request->q) $deployments = $this->query($deployments, $request->q);

        $deployments = $deployments->offset(($page - 1) * 5)
        ->limit(5)
        ->get();

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return compact('deployments', 'paginator');
    }

    public function store(Request $request, Solution $solution)
    {
        DB::beginTransaction();
        $form = $request->form;

        /* ========== 建立 deployment ========== */

        $deployment = auth()->user()->deployments()->create([
            'owner' => auth()->user()->id,
            'solution_id' => $solution->id,
            'plan_id' => $form[1]['plan'],
            'name' => $form[2]['name']['name'],
            'description' => $form[2]['description']['description'],
            'data' => [
                'channelId' => $form[2]['channelId']['channelId'],
                'channelSecret' => $form[2]['channelSecret']['channelSecret'],
                'channelAccessToken' => $form[2]['channelAccessToken']['channelAccessToken'],
                'basicId' => $form[2]['basicId']['basicId'],
                'yourUserID' => $form[2]['yourUserID']['yourUserID'],

                'webhookDomain' => env('APP_STATUS') == 'online' ? env('WEBHOOK_DOMAIN') : env('WEBHOOK_DOMAIN_DEV'),
                'websocketDomain' => env('APP_STATUS') == 'online' ? env('WEBSOCKET_DOMAIN') : env('WEBSOCKET_DOMAIN_DEV'),
            ],
            'status' => 'active',
        ]);

        if($deployment){

            try{

                /* ========== 建立 deployment db ========== */
                $this->createDBAndTables($deployment);

                /* ========== 建立 角色權限 & 授權 ========== */
                $this->createAndAssignRoles($deployment);

                /* ========== 建立 deployment 資料夾 ========== */
                $this->createDirectory($deployment);

                /* ========== 儲存頭貼 & QR code ========== */
                $this->saveProfileAndCreateQRcode($deployment, $form[2]['image']['file']);

                /* ========== 建立 Liff App ========== */
                // $this->createLiffApp($deployment);

                /* ========== 建立 follow & unfollow 行為標籤 ========== */
                $this->createTags();

                /* ========== 同步 Richmenu ========== */
                // $this->syncRichmenu($deployment);

                DB::commit();

                return compact('deployment');

            }catch(\Exception $ex){

                $pdo = $this->getPDOConnection(env('DB_Solution_4_HOST'), env('DB_Solution_4_PORT'), env('DB_Solution_4_USERNAME'), env('DB_Solution_4_PASSWORD'));
                $pdo->exec(sprintf(
                    'DROP DATABASE IF EXISTS deployment_'.$deployment->id
                ));

                DB::rollback();

                return false;
            }
        }


    }

    /* ========== getDeployments 輔助函示 ========== */

    private function query($queryBuilder, $_q)
    {
        $likeQ = '%'.$_q.'%';
        return $queryBuilder->where(function($q) use($_q, $likeQ){
            $q->where('id', $_q)
            ->orWhere('name', 'like', $likeQ)
            ->orWhere('description', 'like', $likeQ)
            ->orWhere('data->basicId', 'like', $likeQ);
        });
    }

    /* ========== store 輔助函式 ========== */

    private function createDBAndTables(Deployment $deployment)
    {
        $pdo = $this->getPDOConnection(env('DB_Solution_4_HOST'), env('DB_Solution_4_PORT'), env('DB_Solution_4_USERNAME'), env('DB_Solution_4_PASSWORD'));
        $pdo->exec(sprintf(
            'CREATE DATABASE IF NOT EXISTS %s CHARACTER SET %s COLLATE %s;',
            'deployment_'.$deployment->id,
            'utf8',
            'utf8_general_ci'
        ));

        Config::set('database.connections.solution_4', [
            'driver' => env('DB_Solution_4_CONNECTION'),
            'host' => env('DB_Solution_4_HOST'),
            'port' => env('DB_Solution_4_PORT'),
            'database' => 'deployment_'.$deployment->id,
            'username' => env('DB_Solution_4_USERNAME'),
            'password' => env('DB_Solution_4_PASSWORD'),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => false,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),]) : [],
        ]);

        try{
            $this->up();

        }catch(PDOException $ex){
            // $this->down();
            // throw $ex;
        }
    }

    private function up()
    {
        Schema::connection('solution_4')->create('auto_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('description')->index();
            $table->unsignedSmallInteger('types')->index();
            $table->json('messages');
            $table->json('messages_form_data')->nullable();
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('broadcasts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('description')->index();
            $table->enum('target_type', ['all', 'groups', 'users']);
            $table->json('groups_id')->nullable();
            $table->json('users_id')->nullable();
            $table->timestamp('appointed_at')->useCurrent();
            $table->timestamp('sent_at')->nullable();
            $table->enum('send_status', ['E','F','N','P','S'])->index();
            $table->json('messages');
            $table->json('messages_form_data')->nullable();
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('group_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('group_id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
        });

        Schema::connection('solution_4')->create('groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('description')->index();
            $table->json('circles')->nullable();
            $table->json('add_users_id')->nullable();
            $table->json('remove_users_id')->nullable();
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('imageables', function (Blueprint $table) {
            $table->unsignedInteger('image_id')->index();
            $table->string('imageable_type')->index();
            $table->unsignedInteger('imageable_id')->index();
        });

        Schema::connection('solution_4')->create('images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('path');
            $table->enum('disk', ['asset','local','public','s3','gcs']);
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('keywords', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('description')->index();
            $table->enum('trigger_type', ['reply','richmenu','script'])->index();
            $table->unsignedInteger('script_id')->nullable()->index();
            $table->unsignedInteger('richmenu_id')->nullable()->index();
            $table->json('messages')->nullable();
            $table->json('messages_form_data')->nullable();
            $table->string('QRcode_url');
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('liffs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('liff_id')->index();
            $table->string('description');
            $table->json('view');
            $table->json('features')->nullable();
            $table->boolean('is_created_by_platform')->index();
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('sender', ['bot','user'])->index();
            $table->enum('recipient', ['bot','user'])->index();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->unsignedInteger('group_id')->nullable()->index();
            $table->unsignedInteger('room_id')->nullable()->index();
            $table->enum('type', ['message','postback','beacon'])->index();
            $table->json('messages')->nullable();
            $table->json('postback')->nullable();
            $table->json('beacon')->nullable();
            $table->timestamp('timestamp')->useCurrent();
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('description')->index();
            $table->json('messages');
            $table->json('messages_form_data');
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('nodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('script_id')->index();
            $table->unsignedInteger('start')->index();
            $table->unsignedInteger('end')->index();
            $table->json('messages');
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('periods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('periodable_id')->index();
            $table->string('periodable_type')->index();
            $table->dateTime('start_date')->index();
            $table->dateTime('end_date')->nullable()->index();
            $table->time('start_time')->index();
            $table->time('end_time')->index();
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('richmenus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('description')->index();
            $table->enum('size', ['2500x843','2500x1686']);
            $table->string('chatBarText')->index();
            $table->boolean('selected');
            $table->boolean('default');
            $table->string('richmenuId')->index();
            $table->json('areas_form_data')->nullable();
            $table->boolean('is_created_by_platform');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });

        Schema::connection('solution_4')->create('scripts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('description')->index();
            $table->json('nodes_form_data');
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('statistics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('followers_count');
            $table->unsignedInteger('blocks_count');
            $table->unsignedInteger('active_users_count');
            $table->unsignedInteger('broadcasts_count');
            $table->unsignedInteger('pushes_count');
            $table->unsignedInteger('multicasts_count');
            $table->unsignedInteger('replies_count');
            $table->timestamp('timestamp')->unique()->useCurrent();
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('tag_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('tag_id')->index();
            $table->timestamp('created_at');
        });

        Schema::connection('solution_4')->create('tags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('taggable_id')->index();
            $table->string('taggable_type')->index();
            $table->string('uuid');
            $table->string('name')->nullable()->index();
            $table->json('data')->nullable();
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('display_name')->index();
            $table->string('user_id')->index();
            $table->string('picture_url');
            $table->string('status_message')->index();
            $table->unsignedInteger('node_id')->default(0)->nullable();
            $table->boolean('is_follow');
            $table->string('richmenu_id')->default(0)->index();
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('videoables', function (Blueprint $table) {
            $table->unsignedInteger('video_id')->index();
            $table->string('videoable_type')->index();
            $table->unsignedInteger('videoable_id')->index();
        });

        Schema::connection('solution_4')->create('videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('path');
            $table->enum('disk', ['asset','local','public','s3','gcs']);
            $table->timestamps();
        });

        Schema::connection('solution_4')->create('welcome_messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('description')->index();
            $table->json('messages');
            $table->json('messages_form_data')->nullable();
            $table->boolean('active');
            $table->timestamps();
        });

    }

    private function getPDOConnection($host, $port, $username, $password)
    {
        return new PDO(sprintf('mysql:host=%s;port=%d;', $host, $port), $username, $password);
    }

    private function createAndAssignRoles(Deployment $deployment)
    {
        // create roles
        foreach($this->roles as $role){
            Role::create(['name' => 'deployment_'.$deployment->id.'_'.$role, 'display_name' => $role], $deployment->id);
        }

        // assign superadmin to user
        auth()->user()->assignRole('deployment_'.$deployment->id.'_superadmin');
    }

    private function createDirectory(Deployment $deployment)
    {
        $storage = env('APP_STATUS') == 'online' ? Storage::disk('gcs') : Storage::disk('local');

        $directories = ['chatroom','messages','richmenus','settings','webhook','keywords'];
        foreach($directories as $directory){
            $storage->makeDirectory('deployments/'.$deployment->id.'/'.$directory);
        }
    }

    private function saveProfileAndCreateQRcode(Deployment $deployment, $file)
    {
        $storage = env('APP_STATUS') == 'online' ? Storage::disk('gcs') : Storage::disk('local');

        // 儲存頭貼
        $path = $storage->putFile('deployments/'.$deployment->id.'/settings', $file);
        $pictureUrl = $storage->url($path);

        // 產生 QR code
        $QrCode = QrCode::format('png')->size(500)->margin(0)
        ->generate('https://line.me/R/ti/p/%40'.substr($deployment->data->basicId, 1));
        $fileName = 'deployments/'.$deployment->id.'/settings/'.Uuid::generate(4).'.png';
        $path = $storage->put($fileName, $QrCode);
        $QRcodeUrl = $storage->url($fileName);

        $deployment->update([
            'data->pictureUrl' => $pictureUrl,
            'data->QRcodeUrl' => $QRcodeUrl,
        ]);
    }

    private function createLiffApp(Deployment $deployment)
    {
        $liffHelper = new LiffHelper;
        $liffHelper->setAccessToken($deployment->data->channelAccessToken);

        foreach(['compact', 'tall', 'full'] as $type){
            $res = $liffHelper->add([
                'view' => [
                    'type' => $type,
                    'url' => 'https://'.$deployment->data->webhookDomain.'/liff/redirect',
                ],
                'description' => 'BitTech',
            ]);
            if($res['success']){
                Liff::create([
                    'liff_id' => json_decode($res['response'])->liffId,
                    'description' => 'BitTech',
                    'view' => [
                        'type' => $type,
                        'url' => 'https://'.$deployment->data->webhookDomain.'/liff/redirect',
                    ],
                    'features' => NULL,
                    'is_created_by_platform' => true,
                ]);
            }
        }
    }

    private function createTags()
    {
        Tag::create([
            'taggable_id' => 0,
            'taggable_type' => 'App\Models\Platform\Solution\_4\User',
            'name' => '追蹤聊天機器人',
            'data' => [
                'type' => 'follow',
            ],
        ]);

        Tag::create([
            'taggable_id' => 0,
            'taggable_type' => 'App\Models\Platform\Solution\_4\User',
            'name' => '封鎖聊天機器人',
            'data' => [
                'type' => 'unfollow',
            ],
        ]);
    }

    private function syncRichmenu(Deployment $deployment)
    {
        $richmenuHelper = new RichmenuHelper;
        $response = $richmenuHelper->setDeployment($deployment)
        ->setAccessToken($deployment->data->channelAccessToken)
        ->getList();

        $richmenus = $response['success'] ? json_decode($response['response'])->richmenus : [];

        foreach($richmenus as $_richmenu){
            $myprojectConnection = DB::connection('mysql');
            $solution4Connection = DB::connection('solution_4');

            $myprojectConnection->beginTransaction();
            $solution4Connection->beginTransaction();

            try{
                $richmenu = Richmenu::create([
                    'richmenuId' => $_richmenu->richMenuId,
                    'name' => $_richmenu->name,
                    'description' => '此選單不由平台建立，無選單描述，亦無區域行為資料。',
                    'size' => $_richmenu->size->width.'x'.$_richmenu->size->height,
                    'chatBarText' => $_richmenu->chatBarText,
                    'selected' => $_richmenu->selected,
                    'areas_form_data' => [],
                    'is_created_by_platform' => 0,
                ]);

                $richmenu->tags()->create([
                    'data' => ['isRelatedToRichmenu' => 1],
                ]);

                $path = $richmenuHelper->setRichmenuId($richmenu->richmenuId)->downloadRichmenuImage();

                if($path){
                    $image = Image::create([
                        'name' => 'LINE主選單',
                        'disk' => 'gcs',
                        'path' => $path,
                    ]);
                    $richmenu->image()->sync($image->id);
                }

                $myprojectConnection->commit();
                $solution4Connection->commit();

            }catch(\Exception $ex){

                \Log::info($ex);
                $myprojectConnection->rollBack();
                $solution4Connection->rollBack();
            }
        }
    }

}
