<?php

namespace App\Services\Solution\_4;

use DB;
use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Script;

// Helper
use App\Helpers\Solution\_4\Message\FormaterHelper;

class ScriptService
{

    public function store(Request $request, Solution $solution, Deployment $deployment){
        $form = $request->form;
        $this->formaterHelper = new FormaterHelper;
        $this->formaterHelper->setDeployment($deployment);


        // 新建腳本
        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try {
            $script = Script::create([
                'name' => $form['name']['name'],
                'description' => $form['description']['description'],
                'active' => $form['active']['active'],
                'nodes_form_data' => [],

                'parent_id' => 0,
                'version' => 1,
                'is_current_version' => 1,
            ]);

            $script->tags()->create([
                'data' => ['isRelatedToScript' => 1],
            ]);

            $nodeChildrenComparison = collect();
            $nodeIdComparison = [];
            $nodeIdMessageComparison = [];

            $formNodes = array_reverse($form['script']['script']['nodes']['nodes']);
            foreach($formNodes as $formNodeIndex => &$formNode){

                // 建立節點
                $node = $script->nodes()->create([
                    'start' => $formNode['start'],
                    'end' => $formNode['end'],
                    'messages' => $this->formaterHelper->refreshImagesAndTags()
                    ->setMessageData($formNode['messages']['messages'])
                    ->setNodeIdComparison($nodeIdComparison)
                    ->setNodeIdMessageComparison($nodeIdMessageComparison)
                    ->get(),
                ]);

                $formNode['messages']['messages'] = $this->formaterHelper->getParsedMessagesData();
                $nodeIdMessageComparison = $this->formaterHelper->getNodeIdMessageComparison();

                $tag = $node->tags()->create([
                    'data' => ['isRelatedToNode' => 1],
                ]);
                $formNode['tag'] = $tag->id;

                $nodeIdComparison[$formNode['id']] = $node->id;

                $nodeChildrenComparison[$node->id] = $this->formaterHelper->getChildren();

                // 圖片關聯
                $images =  $this->formaterHelper->getImages();
                if($images->isNotEmpty()) $node->images()->sync($images->pluck('id'));

                // 影片關聯
                $videos = $this->formaterHelper->getVideos();
                if($videos->isNotEmpty()) $node->videos()->sync($videos->pluck('id'));

                // 標籤關聯
                $tags = $this->formaterHelper->getTags();
                if($tags->isNotEmpty()) $node->tags()->saveMany($tags);
            }

            foreach($nodeChildrenComparison as $nodeId => $children){
                if($children->count() > 0){
                    DB::connection('solution_4')
                    ->table('script_nodes')
                    ->whereIn('id', $children)
                    ->update([
                        'parent_id' => $nodeId,
                    ]);
                }
            }

            foreach($nodeIdMessageComparison as $nodeId => $message){
                DB::connection('solution_4')
                ->table('script_nodes')
                ->where('id', $nodeIdComparison[$nodeId])
                ->update([
                    'next_message' => $message,
                ]);
            }

            DB::connection('solution_4')
            ->table('scripts')
            ->where('id', $script->id)
            ->update([
                'parent_id' => $script->id,
                'nodes_form_data' => preg_replace('/"(invalid|active|id|start|end|show|disabled)"\s*:\s*"(true|false|\d*)"/', '"$1":$2', json_encode($formNodes)),
            ]);

            $solution4Connection->commit();
            return $script;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }
    }

    public function newVersion(Request $request, Solution $solution, Deployment $deployment, Script $script){
        $form = $request->form;
        $this->formaterHelper = new FormaterHelper;
        $this->formaterHelper->setDeployment($deployment);

        // 新建腳本
        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try {
            $version = DB::connection('solution_4')
            ->table('scripts')
            ->select('version')
            ->where('parent_id', $script->parent_id)
            ->orderBy('version', 'desc')
            ->first();

            $newScript = Script::create([
                'name' => $script->name,
                'description' => $script->description,
                'active' => $script->active,
                'nodes_form_data' => [],

                'parent_id' => $script->parent_id,
                'version' => $version->version + 1,
                'is_current_version' => 0,
            ]);

            if($category = $script->category){
                $newScript->category()->attach($category);
            }

            $newScript->tags()->create([
                'data' => ['isRelatedToScript' => 1],
            ]);

            $nodeIdComparison = [];

            $formNodes = array_reverse($form['script']['script']['nodes']['nodes']);
            foreach($formNodes as $formNodeIndex => &$formNode){

                // 建立節點
                $node = $newScript->nodes()->create([
                    'start' => $formNode['start'],
                    'end' => $formNode['end'],
                    'messages' => $this->formaterHelper->refreshImagesAndTags()
                    ->setMessageData($formNode['messages']['messages'])
                    ->setNodeIdComparison($nodeIdComparison)
                    ->get(),
                ]);

                $formNode['messages']['messages'] = $this->formaterHelper->getParsedMessagesData();

                $tag = $node->tags()->create([
                    'data' => ['isRelatedToNode' => 1],
                ]);
                $formNode['tag'] = $tag->id;

                $nodeIdComparison[$formNode['id']] = $node->id;

                // 圖片關聯
                $images =  $this->formaterHelper->getImages();
                if($images->isNotEmpty()) $node->images()->sync($images->pluck('id'));

                // 影片關聯
                $videos = $this->formaterHelper->getVideos();
                if($videos->isNotEmpty()) $node->videos()->sync($videos->pluck('id'));

                // 標籤關聯
                $tags = $this->formaterHelper->getTags();
                if($tags->isNotEmpty()) $node->tags()->saveMany($tags);

            }

            DB::connection('solution_4')
            ->table('scripts')
            ->where('id', $newScript->id)
            ->update([
                'nodes_form_data' => preg_replace('/"(invalid|active|id|start|end|show|disabled)"\s*:\s*"(true|false|\d*)"/', '"$1":$2', json_encode($formNodes)),
            ]);

            $solution4Connection->commit();
            return $newScript;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }
    }

    public function switchVersion(Request $request, Deployment $deployment, Script $script)
    {
        $currentScript = Script::select('id')
        ->where('is_current_version', 1)
        ->where('parent_id', $script->parent_id)
        ->first();

        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{
            if($currentScript){
                $currentScript->is_current_version = 0;
                $currentScript->save();
            }

            $script->is_current_version = 1;
            $script->save();

            $solution4Connection->commit();
            return true;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }

    }

}
