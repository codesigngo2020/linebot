<?php

namespace App\Services\Solution\_4\DeploymentServices;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GetKeywordsAndscriptsChartDataService
{
    public function getChartData(Request $request, $type)
    {
        $this->type = $type;
        $this->usersType = $request->usersType;
        $this->typeStr = [ucfirst($this->type), $this->type.'s'];

        if($request->chartType == 'count'){
            return $this->getCountChartData();
        }else{
            return $this->getRateChartData();
        }
    }

    private function getCountChartData()
    {
        $tag_user = DB::connection('solution_4')
        ->table('tag_user')
        ->select('tag_user.tag_id')
        ->selectRaw('COUNT(DISTINCT(tag_user.'.($this->usersType == 'distinct' ? 'user_id' : 'id').')) as count')
        ->groupBy('tag_user.tag_id');

        $tags = DB::connection('solution_4')
        ->table('tags')
        ->select('tags.id as tag_id', 'tags.taggable_id as '.$this->type.'_id', 'tag_user.count')
        ->joinSub($tag_user, 'tag_user', function($join){
            $join->on('tag_user.tag_id', '=', 'tags.id')
            ->where('tags.taggable_type', 'App\Models\Platform\Solution\_4\\'.$this->typeStr[0])
            ->where('tags.data->isRelatedTo'.ucfirst($this->type), 1);
        })
        ->orderBy('count', 'desc')
        ->limit(5);

        $data = DB::connection('solution_4')
        ->table($this->typeStr[1])
        ->select($this->typeStr[1].'.id', $this->typeStr[1].'.name', 'tags.tag_id')
        ->joinSub($tags, 'tags', function($join){
            $join->on('tags.'.$this->type.'_id', '=', $this->typeStr[1].'.id');
        })
        ->orderBy('tags.count', 'desc')
        ->limit(5)
        ->get()
        ->each(function($d){
            $d->data = [];
        });

        $carbon = Carbon::now()->subDay(14);

        for($i = 0; $i < 15; $i ++){

            $usage_count = DB::connection('solution_4')
            ->table('tags')
            ->select('taggable_id as '.$this->type.'_id')
            ->selectRaw('COUNT(DISTINCT(tag_user.'.($this->usersType == 'distinct' ? 'user_id' : 'id').')) as count')
            ->join('tag_user', 'tag_user.tag_id', '=', 'tags.id')
            ->whereIn('tags.id', $data->pluck('tag_id'))
            ->where('tag_user.created_at', '<=', $carbon->format('Y-m-d 23:59:59'))
            ->groupBy('tags.id')
            ->pluck('count', $this->type.'_id');

            foreach($data as $d){
                array_push($d->data, $usage_count[$d->id] ?? 0);
            };

            $carbon->addDay(1);
        }

        return $data;
    }

    private function getRateChartData()
    {
        $today = Carbon::now();
        $carbon = Carbon::now()->subDay(14);

        $sql = 'SELECT `start`.*, `last`.`last_count`, ROUND( POWER(`last`.`last_count` - `start`.`start_count`, 1 / `start`.`days`), 2 ) AS `avg_rate` FROM '.
        '( SELECT `tags`.`tag_id`, `tags`.`'.$this->type.'_id` AS `id`, `tags`.`first_datetime`, `tags`.`days`, COUNT( DISTINCT ( `tag_user`.`'.($this->usersType == 'distinct' ? 'user_id' : 'id').'` ) ) AS `start_count` FROM `tag_user` INNER JOIN ( '.
        'SELECT `tags`.`id` AS `tag_id`, `tags`.`taggable_id` AS `'.$this->type.'_id`, `tag_user`.`first_datetime` AS `first_datetime`, '.
        ' ( CASE WHEN DATEDIFF( \'2019-10-29\', `tag_user`.`first_datetime` ) <= 14 THEN DATEDIFF( \''.$today->format('Y-m-d').'\', `tag_user`.`first_datetime` ) ELSE 14 END ) AS `days` FROM `tags` INNER JOIN ( '.

        'SELECT `tag_user`.`tag_id`, MIN( `tag_user`.`created_at` ) AS `first_datetime` FROM `tag_user` GROUP BY `tag_user`.`tag_id` ) AS `tag_user` ON `tag_user`.`tag_id` = `tags`.`id` '.
        'WHERE `tags`.`taggable_type` = \'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\'.$this->typeStr[0].'\' AND `tags`.`data` -> \'$.isRelatedTo'.$this->typeStr[0].'\' = 1 ) AS `tags` ON `tags`.`tag_id` = `tag_user`.`tag_id` AND `tags`.`days` != 0 '.
        'WHERE `tag_user`.`created_at` <= `tags`.`first_datetime` OR `tag_user`.`created_at` <= \''.$carbon->format('Y-m-d').' 00:00:00\' GROUP BY `tag_user`.`tag_id` ) AS `start` INNER JOIN ( '.
        'SELECT `tags`.`tag_id`, `tags`.`'.$this->type.'_id`, COUNT( DISTINCT ( `tag_user`.`'.($this->usersType == 'distinct' ? 'user_id' : 'id').'` ) ) AS `last_count` FROM `tag_user` INNER JOIN '.
        '( SELECT `tags`.`id` AS `tag_id`, `tags`.`taggable_id` AS `'.$this->type.'_id` FROM `tags` WHERE `tags`.`taggable_type` = \'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\'.$this->typeStr[0].'\' AND `tags`.`data` -> \'$.isRelatedTo'.$this->typeStr[0].'\' = 1 ) '.
        'AS `tags` ON `tags`.`tag_id` = `tag_user`.`tag_id` GROUP BY `tag_user`.`tag_id` ) AS `last` ON `start`.`tag_id` = `last`.`tag_id` ORDER BY `avg_rate` desc, `'.$this->type.'_id` desc LIMIT 5';

        $data = collect(DB::connection('solution_4')->select($sql));

        $dataName = DB::connection('solution_4')
        ->table($this->typeStr[1])
        ->select('id', 'name')
        ->whereIn('id', $data->pluck('id'))
        ->pluck('name', 'id');

        foreach($data as $d){
            $d->name = $dataName[$d->id];

            $d->data = [];
            $d->counts = [];
        };

        $carbon->subDay(1);

        for($i = 0; $i < 16; $i ++){
            $usage_count = DB::connection('solution_4')
            ->table('tags')
            ->select('taggable_id as '.$this->type.'_id')
            ->selectRaw('COUNT(DISTINCT(tag_user.'.($this->usersType == 'distinct' ? 'user_id' : 'id').')) as count')
            ->join('tag_user', 'tag_user.tag_id', '=', 'tags.id')
            ->whereIn('tags.id', $data->pluck('tag_id'))
            ->where('tag_user.created_at', '<=', $carbon->format('Y-m-d 23:59:59'))
            ->groupBy('tags.id')
            ->pluck('count', $this->type.'_id');

            foreach($data as $d){
                if($i != 0){
                    if($d->counts[$i - 1] && isset($usage_count[$d->id])){
                        array_push($d->data, Floor(($usage_count[$d->id] - $d->counts[$i - 1])*100/$d->counts[$i - 1]));
                    }else{
                        array_push($d->data, NULL);
                    }
                }
                array_push($d->counts, $usage_count[$d->id] ?? 0);
            };

            $carbon->addDay(1);
        }

        return $data;
    }
}
