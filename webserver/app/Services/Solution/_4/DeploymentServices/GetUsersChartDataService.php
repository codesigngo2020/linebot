<?php

namespace App\Services\Solution\_4\DeploymentServices;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GetUsersChartDataService
{
    public function getUsersChartData(Request $request)
    {
        $data = DB::connection('solution_4')->table('statistics');

        switch($request->chartType){
            case 'users':
            $data = $data->select('followers_count');
            break;

            case 'followers':
            $data = $data->select('followers_count', 'blocks_count');
            break;

            case 'blocks':
            $data = $data->select('blocks_count');
            break;
        }

        $data = $data->where('timestamp', '>=', Carbon::now()->subDay(7)->format('Y-m-d 00:00:00'))
        ->orderBy('timestamp')
        ->limit(8)
        ->get();

        switch($request->chartType){
            case 'users':
            return $data->pluck('followers_count');

            case 'followers':
            return $data->map(function($d){
                return $d->followers_count - $d->blocks_count;
            });

            case 'blocks':
            return $data->pluck('blocks_count');
        }

    }

    public function getActiveUsersChartData(Request $request)
    {
        return DB::connection('solution_4')
        ->table('statistics')
        ->select('active_users_count')
        ->where('timestamp', '>=', Carbon::now()->subDay(7)->format('Y-m-d 00:00:00'))
        ->orderBy('timestamp')
        ->limit(8)
        ->pluck('active_users_count');
    }
}
