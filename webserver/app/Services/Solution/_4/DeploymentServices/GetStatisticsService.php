<?php

namespace App\Services\Solution\_4\DeploymentServices;

use DB;
use Carbon\Carbon;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Statistics;

// Helper
use App\Helpers\Solution\_4\Statistics\StatisticsHelper;

class GetStatisticsService
{
    public function getStatistics(Deployment $deployment)
    {
        $this->statisticsHelper = new StatisticsHelper;
        $this->statisticsHelper->setAccessToken($deployment->data->channelAccessToken);

        $carbon = Carbon::now()->subDay(14);

        /* ========== 確定過去13天有數據 ========== */

        $existsDates = DB::connection('solution_4')
        ->table('statistics')
        ->select('timestamp')
        ->whereBetween('timestamp', [$carbon->format('Y-m-d 00:00:00'), $carbon->addDay(13)->format('Y-m-d 00:00:00')])
        ->orderBy('timestamp')
        ->limit(14)
        ->pluck('timestamp');

        if($existsDates->count() < 14){

            $i = 0;
            $carbon = Carbon::now()->subDay(14);
            $earlier_7_days_carbon = Carbon::now()->subDay(20);

            do{
                if($existsDates->first() != $carbon->format('Y-m-d 00:00:00')){

                    $data = [
                        'followers_count' => 0,
                        'blocks_count' => 0,
                        'active_users_count' => 0,

                        'broadcasts_count' => 0,
                        'pushes_count' => 0,
                        'multicasts_count' => 0,
                        'replies_count' => 0,
                    ];

                    // followers_count & blocks_count
                    $res = $this->statisticsHelper->getNumberOfFollowers($carbon->format('Ymd'));

                    if($res['success']){
                        $res = json_decode($res['response']);
                        if($res->status == 'ready'){
                            $data['followers_count'] = $res->followers;
                            $data['blocks_count'] = $res->blocks;
                        }
                    }

                    // active_users_count
                    $active_users_from_log = DB::connection('solution_4')
                    ->table('logs')
                    ->selectRaw('DISTINCT(user_id)')
                    ->whereBetween('created_at', [$earlier_7_days_carbon->format('Y-m-d 00:00:00'), $carbon->format('Y-m-d 23:59:59')]);

                    $data['active_users_count'] = DB::connection('solution_4')
                    ->table('tag_user')
                    ->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as count')
                    ->leftJoinSub($active_users_from_log, 'logs', function($join){
                        $join->on('logs.user_id', '=', 'tag_user.user_id');
                    })
                    ->whereBetween('created_at', [$earlier_7_days_carbon->format('Y-m-d 00:00:00'), $carbon->format('Y-m-d 23:59:59')])
                    ->first()->count;

                    // broadcasts_count & pushes_count & replies_count
                    $res = $this->statisticsHelper->getNumberOfMessageDeliveries($carbon->format('Ymd'));

                    if($res['success']){
                        $res = json_decode($res['response']);
                        if($res->status == 'ready'){
                            $data['broadcasts_count'] = $res->apiBroadcast ?? 0;
                            $data['pushes_count'] = $res->apiPush ?? 0;
                            $data['multicasts_count'] = $res->apiMulticast ?? 0;
                            $data['replies_count'] = $res->apiReply ?? 0;
                        }
                    }



                    $a = Statistics::create([
                        'followers_count' => $data['followers_count'],
                        'blocks_count' => $data['blocks_count'],
                        'active_users_count' => $data['active_users_count'],

                        'broadcasts_count' => $data['broadcasts_count'],
                        'pushes_count' => $data['pushes_count'],
                        'multicasts_count' => $data['multicasts_count'],
                        'replies_count' => $data['replies_count'],

                        'timestamp' => $carbon->format('Y-m-d 00:00:00'),
                    ]);
                    \Log::info($a);
                }else{
                    $existsDates->shift();
                }

                $carbon->addDay(1);
                $earlier_7_days_carbon->addDay(1);
                $i ++;
            }while($i < 14);
        }

        /* ========== 取得當天數據：前天23:00～當天23:00 ========== */

        $today_start = Carbon::now()->subDay(1)->format('Y-m-d 23:00:00');

        // 前天數據
        $yesterday_statistics = Statistics::where('timestamp', Carbon::now()->subDay(1)->format('Y-m-d 00:00:00'))->first();

        // 當天數據
        $today_users_count = DB::connection('solution_4')
        ->table('users')
        ->where('created_at', '>=', $today_start)
        ->count();

        $today_followers_count = DB::connection('solution_4')
        ->table('tag_user')
        ->where('tag_id', 1)
        ->where('created_at', '>=', $today_start)
        ->count();

        $today_blocks_count = DB::connection('solution_4')
        ->table('tag_user')
        ->where('tag_id', 2)
        ->where('created_at', '>=', $today_start)
        ->count();

        $today_active_users_from_log = DB::connection('solution_4')
        ->table('logs')
        ->selectRaw('DISTINCT(user_id)')
        ->where('created_at', '>=', Carbon::now()->subDay(7)->format('Y-m-d H:i:s'));

        $today_active_users_count = DB::connection('solution_4')
        ->table('tag_user')
        ->selectRaw('COUNT(DISTINCT(tag_user.user_id)) as count')
        ->leftJoinSub($today_active_users_from_log, 'logs', function($join){
            $join->on('logs.user_id', '=', 'tag_user.user_id');
        })
        ->where('tag_user.created_at', '>=', Carbon::now()->subDay(7)->format('Y-m-d H:i:s'))
        ->first()->count;

        $today_broadcasts_count = DB::connection('solution_4')
        ->table('tag_user')
        ->join('tags', function($join){
            $join->on('tags.id', '=', 'tag_user.tag_id')
            ->where('tags.data->isRelatedToBroadcast', 1);
        })
        ->where('tag_user.created_at', '>=', $today_start)
        ->count();

        $followers_count = $yesterday_statistics->followers_count + $today_users_count;
        $blocks_count = $yesterday_statistics->blocks_count + $today_users_count + $today_blocks_count - $today_followers_count;

        $statistics = Statistics::updateOrCreate([
            'timestamp' => Carbon::now()->format('Y-m-d 00:00:00'),
        ],[
            'followers_count' => $followers_count,
            'blocks_count' => $blocks_count,
            'active_users_count' => $today_active_users_count,

            'broadcasts_count' => $today_broadcasts_count, // 當天的都先歸到broadcasts_count
            'pushes_count' => 0,
            'multicasts_count' => 0,
            'replies_count' => 0, // 暫時不會用到
        ]);

        // 好友成長率
        $statistics->followers_count_rate = (!$yesterday_statistics->followers_count) ? 100 : number_format(($statistics->followers_count - $yesterday_statistics->followers_count)*100/$yesterday_statistics->followers_count, 1);
        // 活躍好友成長率
        $statistics->active_users_count_rate = (!$yesterday_statistics->active_users_count) ? 100 : number_format(($statistics->active_users_count - $yesterday_statistics->active_users_count)*100/$yesterday_statistics->active_users_count, 1);

        // 推播訊息成長率
        $today_broadcasts_count = $statistics->broadcasts_count + $statistics->pushes_count + $statistics->multicasts_count;
        $yesterday_broadcasts_count = $yesterday_statistics->broadcasts_count + $yesterday_statistics->pushes_count + $yesterday_statistics->multicasts_count;

        $statistics->broadcasts_count_rate = ($yesterday_broadcasts_count == 0) ? 0 : number_format(($today_broadcasts_count - $yesterday_broadcasts_count)*100/$yesterday_broadcasts_count, 1);

        // 本月推播訊息總數
        $res = $this->statisticsHelper->getNumberOfMessagesSentThisMonth();
        if($res['success']){
            $res = json_decode($res['response']);
            $statistics->this_month_broadcasts_count = $res->totalUsage;
        }

        return $statistics;
    }


}
