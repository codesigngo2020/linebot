<?php

namespace App\Services\Solution\_4\DeploymentServices;

use DB;
use Carbon\Carbon;

// Model
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Broadcast;

class GetBroadcastsAndAutoAnswerService
{
    public function getBroadcasts(Deployment $deployment)
    {
        $carbon = Carbon::now();

        return Broadcast::select('id', 'name', 'messages', 'appointed_at')
        ->whereBetween('appointed_at', [$carbon->format('Y-m-d 00:00:00'), $carbon->addDay(2)->format('Y-m-d 23:59:59')])
        ->get();
    }

    public function getAutoAnswers(Deployment $deployment)
    {
        $carbon = Carbon::now()->addDay(2);

        return DB::connection('solution_4')
        ->table('auto_answers')
        ->select('auto_answers.id', 'auto_answers.name')

        ->join('periods', function($join) use($carbon){
            $join->on('periods.periodable_id', '=', 'auto_answers.id')
            ->where('periods.periodable_type', 'App\Models\Platform\Solution\_4\AutoAnswer')
            ->where('periods.start_date', '<=', $carbon->format('Y-m-d 23:59:59'))
            ->where(function($q) use($carbon){
                $q->whereNull('periods.end_date')
                ->orWhere('periods.end_date', '>=', $carbon->subDay(2)->format('Y-m-d 00:00:00'));
            })
            ->limit(1);
        })

        ->where('auto_answers.active', 1)
        ->orderBy('auto_answers.id')
        ->distinct()
        ->get()
        ->each(function($autoAnswer){

            $carbon = Carbon::now()->addDay(2);

            $autoAnswer->periods = DB::connection('solution_4')
            ->table('periods')
            ->select('periods.start_date', 'periods.end_date')
            ->where('periods.periodable_id', $autoAnswer->id)
            ->where('periods.periodable_type', 'App\Models\Platform\Solution\_4\AutoAnswer')
            ->where('periods.start_date', '<=', $carbon->format('Y-m-d 23:59:59'))
            ->where(function($q) use($carbon){
                $q->whereNull('periods.end_date')
                ->orWhere('periods.end_date', '>=', $carbon->subDay(2)->format('Y-m-d 00:00:00'));
            })
            ->get();
        });

    }

}
