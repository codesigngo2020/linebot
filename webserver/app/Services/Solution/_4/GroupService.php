<?php

namespace App\Services\Solution\_4;

use DB;
use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\User;
use App\Models\Platform\Solution\_4\Group;

// Helper
use App\Helpers\Solution\_4\Group\GroupHelper;

class GroupService
{

    public function index(Request $request, Solution $solution, Deployment $deployment)
    {
        $total_users_count = User::count();

        $page = intval($request->page);
        $lastPage = ceil(Group::count() / 5);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $groups = DB::connection('solution_4')
        ->table('groups')
        ->select('groups.id', 'groups.name', 'groups.description', 'groups.updated_at')
        ->selectRaw('FLOOR(COUNT(DISTINCT(group_user.user_id))*100/'.$total_users_count.') as users_percentage')

        ->leftJoin('group_user', function($join){
            $join->on('group_user.group_id', '=', 'groups.id')
            ->whereNull('group_user.deleted_at');
        })

        ->groupBy('groups.id')
        ->orderBy('groups.id', 'desc')
        ->offset(($page - 1) * 5)
        ->limit(5)
        ->get();

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return compact('groups', 'paginator');
    }

    public function store(Request $request, Solution $solution, Deployment $deployment){
        $form = $request->form;

        // 好友群組關聯
        $groupHelper = new GroupHelper;

        // 建立群組
        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{
            $circelsData = [];
            if(isset($form['circles']['circles'])){
                foreach($form['circles']['circles'] as $circles){
                    $circelsData[] = [
                        'tagsId' => collect($circles['areas']['areas'])->pluck('tagId')->toArray(),
                        'selectedAreas' => $circles['selectedAreas']['selectedAreas'],
                    ];
                }
            }

            $group = Group::create([
                'name' => $form['name']['name'],
                'description' => $form['description']['description'],
                'circles' => isset($form['circles']['circles']) ? $circelsData : NULL,
                'add_users_id' => isset($form['addUsers']['value']) ? collect($form['addUsers']['value'])->pluck('value') : NULL,
                'remove_users_id' => isset($form['removeUsers']['value']) ? collect($form['removeUsers']['value'])->pluck('value') : NULL,
            ]);

            $groupHelper->setDeployment($deployment)->setGroup($group)->set();

            $solution4Connection->commit();
            return $group;

        }catch(\Exception $ex){
            \Log::info($ex);
            dd($ex);
            $solution4Connection->rollBack();

            return false;
        }
    }

}
