<?php

namespace App\Services\Solution\_4;

use DB;
use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Broadcast;

// Helper
use App\Helpers\Solution\_4\Message\FormaterHelper;

class BroadcastService
{

    public function store(Request $request, Solution $solution, Deployment $deployment){
        $form = $request->form;

        // 新建訊息
        if($form['messageType']['messageType'] != 'template'){
            $formaterHelper = new FormaterHelper;
        }

        // 建立推播訊息
        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{
            $broadcast = Broadcast::create([
                'name' => $form['name']['name'],
                'description' => $form['description']['description'],
                'target_type' => $form['target']['target_type'],
                'groups_id' => $form['target']['target_type'] == 'groups' ? collect($form['usersGroups']['value'])->pluck('value')->toArray() : NULL,
                'users_id' => $form['target']['target_type'] == 'users' ? collect($form['users']['value'])->pluck('value')->toArray() : NULL,
                'appointed_at' => $form['timeType']['timeType'] == 'schedule' ? $form['datetime']['datetime'].':00' : date('Y-m-d H:i:s'),

                'template_id' => $form['messageType']['messageType'] == 'template' ? $form['message']['value']['value'] : NULL,
                'messages' => $form['messageType']['messageType'] != 'template' ? $formaterHelper->setDeployment($deployment)
                ->setMessageData($form['messages']['messages'])
                ->get() : NULL,
                'messages_form_data' => $form['messageType']['messageType'] != 'template' ? $formaterHelper->getParsedMessagesData() : NULL,
                'send_status' => 'N',
            ]);

            if($form['messageType']['messageType'] != 'template'){
                DB::connection('solution_4')
                ->table('broadcasts')
                ->where('id', $broadcast->id)
                ->update([
                    'messages_form_data' => preg_replace('/"(invalid|active|id|start|end|show|disabled)"\s*:\s*"(true|false|\d*)"/', '"$1":$2', json_encode($formaterHelper->getParsedMessagesData())),
                ]);
            }

            $broadcast->tags()->create([
                'data' => ['isRelatedToBroadcast' => 1],
            ]);

            if($form['messageType']['messageType'] != 'template'){
                // 圖片關聯
                $images = $formaterHelper->getImages();
                if($images->isNotEmpty()) $broadcast->images()->sync($images->pluck('id'));

                // 影片關聯
                $videos = $formaterHelper->getVideos();
                if($videos->isNotEmpty()) $broadcast->videos()->sync($videos->pluck('id'));

                // 標籤關聯
                $tags = $formaterHelper->getTags();
                if($tags->isNotEmpty()) $broadcast->tags()->saveMany($tags);
            }

            $solution4Connection->commit();
            return $broadcast;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }

    }

}
