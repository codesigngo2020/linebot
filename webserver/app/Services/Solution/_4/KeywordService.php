<?php

namespace App\Services\Solution\_4;

use DB;
use Storage;
use QrCode;
use Uuid;
use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Keyword;

// Helper
use App\Helpers\Solution\_4\Message\FormaterHelper;

class KeywordService
{

    public function store(Request $request, Solution $solution, Deployment $deployment){
        $form = $request->form;

        // 新建訊息
        $formaterHelper = new FormaterHelper;

        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{
            // 產生 QR code
            $QrCode = QrCode::format('png')->size(500)->margin(0)
            ->generate('line://oaMessage/'.$deployment->data->basicId.'/?'.$form['name']['name']);
            $fileName = 'deployments/'.$deployment->id.'/keywords/'.Uuid::generate(4).'.png';

            $storage = Storage::disk('gcs');
            $storage->put($fileName, $QrCode);

            // 建立關鍵字
            $keyword = Keyword::create([
                'name' => $form['name']['name'],
                'description' => $form['description']['description'],
                'QRcode_url' => $storage->url($fileName),
                'active' => $form['active']['active'],
                'trigger_type' => $form['triggerType']['triggerType'],
                'script_id' => $form['triggerType']['triggerType'] == 'script' ? $form['trigger']['trigger']['script']['value']['value'] : NULL,
                'richmenu_id' => $form['triggerType']['triggerType'] == 'richmenu' ? $form['trigger']['trigger']['richmenu']['value']['value'] : NULL,

                'template_id' => $form['triggerType']['triggerType'] == 'reply' && $form['trigger']['trigger']['messageType']['messageType'] == 'template' ? $form['trigger']['trigger']['message']['value']['value'] : NULL,
                'messages' => $form['triggerType']['triggerType'] == 'reply' && $form['trigger']['trigger']['messageType']['messageType'] != 'template' ? $formaterHelper->setDeployment($deployment)
                ->setMessageData($form['trigger']['trigger']['messages']['messages'])
                ->get() : NULL,
                'messages_form_data' => $form['triggerType']['triggerType'] == 'reply' && $form['trigger']['trigger']['messageType']['messageType'] != 'template' ? $formaterHelper->getParsedMessagesData() : NULL,

                'parent_id' => 0,
                'version' => 1,
                'is_current_version' => 1,
            ]);

            if($form['triggerType']['triggerType'] == 'reply' && $form['trigger']['trigger']['messageType']['messageType'] != 'template'){
                DB::connection('solution_4')
                ->table('keywords')
                ->where('id', $keyword->id)
                ->update([
                    'parent_id' => $keyword->id,
                    'messages_form_data' => preg_replace('/"(invalid|active|id|start|end|show|disabled)"\s*:\s*"(true|false|\d*)"/', '"$1":$2', json_encode($formaterHelper->getParsedMessagesData())),
                ]);
            }else{
                $keyword->parent_id = $keyword->id;
                $keyword->save();
            }

            $keyword->tags()->create([
                'data' => ['isRelatedToKeyword' => 1],
            ]);

            if($form['triggerType']['triggerType'] == 'reply' && $form['trigger']['trigger']['messageType']['messageType'] != 'template'){
                // 圖片關聯
                $images =  $formaterHelper->getImages();
                if($images->isNotEmpty()) $keyword->images()->sync($images->pluck('id'));

                // 影片關聯
                $videos = $formaterHelper->getVideos();
                if($videos->isNotEmpty()) $keyword->videos()->sync($videos->pluck('id'));

                // 標籤關聯
                $tags = $formaterHelper->getTags();
                if($tags->isNotEmpty()) $keyword->tags()->saveMany($tags);
            }

            $solution4Connection->commit();
            return $keyword;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }
    }

    public function newVersion(Request $request, Solution $solution, Deployment $deployment, Keyword $keyword){
        $form = $request->form;

        // 新建訊息
        $formaterHelper = new FormaterHelper;

        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{
            $version = DB::connection('solution_4')
            ->table('keywords')
            ->select('version')
            ->where('parent_id', $keyword->parent_id)
            ->orderBy('version', 'desc')
            ->first();

            // 建立關鍵字
            $newKeyword = Keyword::create([
                'name' => $keyword->name,
                'description' => $keyword->description,
                'QRcode_url' => $keyword->QRcode_url,
                'active' => $keyword->active,
                'trigger_type' => $form['triggerType']['triggerType'],
                'script_id' => $form['triggerType']['triggerType'] == 'script' ? $form['trigger']['trigger']['script']['value']['value'] : NULL,
                'richmenu_id' => $form['triggerType']['triggerType'] == 'richmenu' ? $form['trigger']['trigger']['richmenu']['value']['value'] : NULL,

                'template_id' => $form['triggerType']['triggerType'] == 'reply' && $form['trigger']['trigger']['messageType']['messageType'] == 'template' ? $form['trigger']['trigger']['message']['value']['value'] : NULL,
                'messages' => $form['triggerType']['triggerType'] == 'reply' && $form['trigger']['trigger']['messageType']['messageType'] != 'template' ? $formaterHelper->setDeployment($deployment)
                ->setMessageData($form['trigger']['trigger']['messages']['messages'])
                ->get() : NULL,
                'messages_form_data' => $form['triggerType']['triggerType'] == 'reply' && $form['trigger']['trigger']['messageType']['messageType'] != 'template' ? $formaterHelper->getParsedMessagesData() : NULL,

                'parent_id' => $keyword->parent_id,
                'version' => $version->version + 1,
                'is_current_version' => 0,
            ]);

            if($form['triggerType']['triggerType'] == 'reply' && $form['trigger']['trigger']['messageType']['messageType'] != 'template'){
                DB::connection('solution_4')
                ->table('keywords')
                ->where('id', $newKeyword->id)
                ->update([
                    'messages_form_data' => preg_replace('/"(invalid|active|id|start|end|show|disabled)"\s*:\s*"(true|false|\d*)"/', '"$1":$2', json_encode($formaterHelper->getParsedMessagesData())),
                ]);
            }

            if($category = $keyword->category){
                $newKeyword->category()->attach($category);
            }

            $newKeyword->tags()->create([
                'data' => ['isRelatedToKeyword' => 1],
            ]);

            if($form['triggerType']['triggerType'] == 'reply'){
                // 圖片關聯
                $images =  $formaterHelper->getImages();
                if($images->isNotEmpty()) $newKeyword->images()->sync($images->pluck('id'));

                // 影片關聯
                $videos = $formaterHelper->getVideos();
                if($videos->isNotEmpty()) $newKeyword->videos()->sync($videos->pluck('id'));

                // 標籤關聯
                $tags = $formaterHelper->getTags();
                if($tags->isNotEmpty()) $newKeyword->tags()->saveMany($tags);
            }

            $solution4Connection->commit();
            return $newKeyword;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }
    }

    public function switchVersion(Request $request, Deployment $deployment, Keyword $keyword)
    {
        $currentKeyword = Keyword::select('id')
        ->where('is_current_version', 1)
        ->where('parent_id', $keyword->parent_id)
        ->first();

        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{
            if($currentKeyword){
                $currentKeyword->is_current_version = 0;
                $currentKeyword->save();
            }

            $keyword->is_current_version = 1;
            $keyword->save();

            $solution4Connection->commit();
            return true;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }

    }

}
