<?php

namespace App\Services\Solution\_4;

use DB;
use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Message;
use App\Models\Platform\Solution\_4\Table;

// Helper
use App\Helpers\Solution\_4\Message\FormaterHelper;

class MessagesPoolService
{

    public function index(Request $request, Solution $solution, Deployment $deployment)
    {
        $page = intval($request->page);
        $lastPage = ceil(Message::count() / 5);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $messages = Message::select('id', 'name', 'description', 'messages')
        ->orderBy('id', 'desc')
        ->offset(($page - 1) * 5)
        ->limit(5)
        ->get()
        ->each(function($message){
            $message->messages = array_map(function($message){
                if($message->type == 'template'){
                    return [
                        'type' => $message->template->type == 'image_carousel' ? 'imagecarousel' : $message->template->type,
                        'hasQuickReplay' => isset($message->quickReply),
                    ];
                }else{
                    return [
                        'type' => $message->type,
                        'hasQuickReplay' => isset($message->quickReply),
                    ];
                }
            }, $message->messages);
        });

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return compact('messages', 'paginator');
    }

    public function storeMessage(Request $request, Solution $solution, Deployment $deployment){
        $form = $request->form;

        // 新建訊息
        $formaterHelper = new FormaterHelper;
        $formatedMessage = $formaterHelper->setDeployment($deployment)
        ->setMessageData($form['messages']['messages'])
        ->setShouldTag(false)
        ->get();

        // 建立訊息
        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{
            $message = Message::create([
                'name' => $form['name']['name'],
                'description' => $form['description']['description'],
                'type' => 'message',

                'messages' => $formatedMessage,
                'messages_form_data' => $form['messages']['messages'],

                'parent_id' => 0,
                'version' => 1,
                'is_current_version' => 1,
            ]);

            DB::connection('solution_4')
            ->table('messages')
            ->where('id', $message->id)
            ->update([
                'parent_id' => $message->id,
                'messages_form_data' => preg_replace('/"(invalid|active|id|start|end|show|disabled)"\s*:\s*"(true|false|\d*)"/', '"$1":$2', json_encode($formaterHelper->getParsedMessagesData())),
            ]);

            // 圖片關聯
            $images = $formaterHelper->getImages();
            if($images->isNotEmpty()) $message->images()->sync($images->pluck('id'));

            // 影片關聯
            $videos = $formaterHelper->getVideos();
            if($videos->isNotEmpty()) $message->videos()->sync($videos->pluck('id'));

            $solution4Connection->commit();
            return $message;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }

    }

    public function storeTemplate(Request $request, Solution $solution, Deployment $deployment)
    {
        $form = $request->form;
        $this->formaterHelper = new FormaterHelper;
        $this->formaterHelper->setDeployment($deployment);


        // 新建訊息
        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try {
            $message = Message::create([
                'name' => $form['name']['name'],
                'description' => $form['description']['description'],
                'type' => 'template',
                'template_type' => $form['templateType']['value']['value'],

                'nodes_form_data' => [],
                'settings' => [
                    'table' => intval($form['templateSettings']['table']['value']['value']),
                ],

                'parent_id' => 0,
                'version' => 1,
                'is_current_version' => 1,
            ]);

            $message->tags()->create([
                'data' => ['isRelatedToMessage' => 1],
            ]);

            $nodes = collect();
            $formNodes = $this->getNonDisabledNodes($form);

            foreach($formNodes as &$formNode){

                // 取得parent
                $parent = $nodes->last(function($node) use($formNode){
                    return $node->start < $formNode['start'] &&  $node->end > $formNode['end'];
                });

                // 建立節點
                $node = $message->nodes()->create([
                    'start' => $formNode['start'],
                    'end' => $formNode['end'],
                    'parent_id' => $parent ? $parent->id : NULL,
                    'condition' => $formNode['condition']['value'],
                    'messages' => $this->formaterHelper->refreshImagesAndTags()
                    ->setMessageData($formNode['messages']['messages'])
                    ->get(),
                ]);

                $nodes->push($node);

                $formNode['messages']['messages'] = $this->formaterHelper->getParsedMessagesData();

                $tag = $node->tags()->create([
                    'data' => ['isRelatedToNode' => 1],
                ]);
                $formNode['tag'] = $tag->id;

                // 圖片關聯
                $images =  $this->formaterHelper->getImages();
                if($images->isNotEmpty()) $node->images()->sync($images->pluck('id'));

                // 影片關聯
                $videos = $this->formaterHelper->getVideos();
                if($videos->isNotEmpty()) $node->videos()->sync($videos->pluck('id'));

                // 標籤關聯
                $tags = $this->formaterHelper->getTags();
                if($tags->isNotEmpty()) $node->tags()->saveMany($tags);
            }

            $formNodes = collect($formNodes)->keyBy('id');
            foreach($form['template']['template']['nodes']['nodes'] as &$node){
                if(isset($formNodes[$node['id']])) $node = $formNodes[$node['id']];
            };

            DB::connection('solution_4')
            ->table('messages')
            ->where('id', $message->id)
            ->update([
                'parent_id' => $message->id,
                'nodes_form_data' => preg_replace('/"(invalid|active|id|start|end|show|disabled)"\s*:\s*"(true|false|\d*)"/', '"$1":$2', json_encode($form['template']['template']['nodes']['nodes'])),
            ]);

            $solution4Connection->commit();
            return $message;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }
    }

    public function newMessageVersion(Request $request, Solution $solution, Deployment $deployment, Message $message)
    {
        $form = $request->form;

        // 新建訊息
        $formaterHelper = new FormaterHelper;
        $formatedMessage = $formaterHelper->setDeployment($deployment)
        ->setMessageData($form['messages']['messages'])
        ->setShouldTag(false)
        ->get();

        // 建立訊息
        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{
            $version = DB::connection('solution_4')
            ->table('messages')
            ->select('version')
            ->where('parent_id', $message->parent_id)
            ->orderBy('version', 'desc')
            ->first();

            $newMessage = Message::create([
                'name' => $message->name,
                'description' => $message->description,
                'type' => $message->type,

                'messages' => $formatedMessage,
                'messages_form_data' => $form['messages']['messages'],

                'parent_id' => $message->parent_id,
                'version' => $version->version + 1,
                'is_current_version' => 0,
            ]);

            DB::connection('solution_4')
            ->table('messages')
            ->where('id', $newMessage->id)
            ->update([
                'messages_form_data' => preg_replace('/"(invalid|active|id|start|end|show|disabled)"\s*:\s*"(true|false|\d*)"/', '"$1":$2', json_encode($formaterHelper->getParsedMessagesData())),
            ]);

            // 圖片關聯
            $images = $formaterHelper->getImages();
            if($images->isNotEmpty()) $newMessage->images()->sync($images->pluck('id'));

            // 影片關聯
            $videos = $formaterHelper->getVideos();
            if($videos->isNotEmpty()) $newMessage->videos()->sync($videos->pluck('id'));

            $solution4Connection->commit();
            return $newMessage;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }
    }

    public function newTemplateVersion(Request $request, Solution $solution, Deployment $deployment, Message $message)
    {
        $form = $request->form;
        $this->formaterHelper = new FormaterHelper;
        $this->formaterHelper->setDeployment($deployment);


        // 新建訊息
        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try {
            $version = DB::connection('solution_4')
            ->table('messages')
            ->select('version')
            ->where('parent_id', $message->parent_id)
            ->orderBy('version', 'desc')
            ->first();

            $newMessage = Message::create([
                'name' => $message->name,
                'description' => $message->description,
                'type' => 'template',
                'template_type' => $form['templateType']['value']['value'],

                'nodes_form_data' => [],
                'settings' => [
                    'table' => intval($form['templateSettings']['table']['value']['value']),
                ],

                'parent_id' => $message->parent_id,
                'version' => $version->version + 1,
                'is_current_version' => 0,
            ]);

            $newMessage->tags()->create([
                'data' => ['isRelatedToMessage' => 1],
            ]);

            $nodes = collect();
            $formNodes = $this->getNonDisabledNodes($form);

            foreach($formNodes as &$formNode){

                // 取得parent
                $parent = $nodes->last(function($node) use($formNode){
                    return $node->start < $formNode['start'] &&  $node->end > $formNode['end'];
                });

                // 建立節點
                $node = $newMessage->nodes()->create([
                    'start' => $formNode['start'],
                    'end' => $formNode['end'],
                    'parent_id' => $parent ? $parent->id : NULL,
                    'condition' => $formNode['condition']['value'],
                    'messages' => $this->formaterHelper->refreshImagesAndTags()
                    ->setMessageData($formNode['messages']['messages'])
                    ->get(),
                ]);

                $formNode['messages']['messages'] = $this->formaterHelper->getParsedMessagesData();

                $tag = $node->tags()->create([
                    'data' => ['isRelatedToNode' => 1],
                ]);
                $formNode['tag'] = $tag->id;

                // 圖片關聯
                $images =  $this->formaterHelper->getImages();
                if($images->isNotEmpty()) $node->images()->sync($images->pluck('id'));

                // 影片關聯
                $videos = $this->formaterHelper->getVideos();
                if($videos->isNotEmpty()) $node->videos()->sync($videos->pluck('id'));

                // 標籤關聯
                $tags = $this->formaterHelper->getTags();
                if($tags->isNotEmpty()) $node->tags()->saveMany($tags);
            }

            $formNodes = collect($formNodes)->keyBy('id');
            foreach($form['template']['template']['nodes']['nodes'] as &$node){
                if(isset($formNodes[$node['id']])) $node = $formNodes[$node['id']];
            };

            DB::connection('solution_4')
            ->table('messages')
            ->where('id', $newMessage->id)
            ->update([
                'nodes_form_data' => preg_replace('/"(invalid|active|id|start|end|show|disabled)"\s*:\s*"(true|false|\d*)"/', '"$1":$2', json_encode($form['template']['template']['nodes']['nodes'])),
            ]);

            $solution4Connection->commit();
            return $newMessage;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }
    }

    public function switchVersion(Request $request, Deployment $deployment, Message $message)
    {
        $currentMessage = Message::select('id')
        ->where('is_current_version', 1)
        ->where('parent_id', $message->parent_id)
        ->first();

        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{
            if($currentMessage){
                $currentMessage->is_current_version = 0;
                $currentMessage->save();
            }

            $message->is_current_version = 1;
            $message->save();

            $solution4Connection->commit();
            return true;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }

    }

    // 取得沒被disabled的nodes
    private function getNonDisabledNodes($form)
    {
        $templateType = $form['templateType']['value']['value'];
        $filteredNodes = $form['template']['template']['nodes']['nodes'];

        if($templateType){
            $table = Table::find($form['templateSettings']['table']['value']['value']);
            if($table){
                $disabledConditions = [];
                if(!$table->settings->issueDatetime->active) array_push($disabledConditions, 'invalid-time');
                if(!$table->settings->limit->active) array_push($disabledConditions, 'reach-limit');

                $filteredNodes = array_filter($filteredNodes, function($node) use($disabledConditions){
                    return !in_array($node['condition']['value'], $disabledConditions);
                });
            }
        }

        usort($filteredNodes, function($a, $b){
            return $a['start'] > $b['start'];
        });

        return $filteredNodes;
    }

}
