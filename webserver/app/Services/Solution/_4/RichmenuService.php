<?php

namespace App\Services\Solution\_4;

use DB;
use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Image;
use App\Models\Platform\Solution\_4\Richmenu;

// Helper
use App\Helpers\Solution\_4\Richmenu\AreasHelper;
use App\Helpers\Solution\_4\Richmenu\RichmenuHelper;

class RichmenuService
{

    public $richmenuHelper;

    public function __construct(RichmenuHelper $richmenuHelper)
    {
        $this->richmenuHelper = $richmenuHelper;
    }

    public function index(Request $request, Solution $solution, Deployment $deployment)
    {
        $response = $this->richmenuHelper->setDeployment($deployment)
        ->setAccessToken($deployment->data->channelAccessToken)
        ->getList();

        $richmenus = $response['success'] ? json_decode($response['response'])->richmenus : [];

        // 刪除不再LINE的選單
        Richmenu::whereNotIn('richmenuId', collect($richmenus)->pluck('richMenuId'))
        ->update([
            'deleted_at' => date('Y-m-d H:i:s')
        ]);

        // 更新選單資訊
        foreach($richmenus as $_richmenu){
            $richmenu = Richmenu::where('richmenuId', $_richmenu->richMenuId)->whereNull('deleted_at')->first();

            if(!$richmenu){
                $solution4Connection = DB::connection('solution_4');
                $solution4Connection->beginTransaction();

                try{
                    // 取得 richmenu 名稱
                    $name = $_richmenu->name;
                    $i = 1;
                    if(Richmenu::select('id')->where('name', $name)->whereNull('deleted_all_at')->first()){
                        while(true){
                            $name = $_richmenu->name.'-'.$i;
                            if(!Richmenu::select('id')->where('name', $name)->whereNull('deleted_all_at')->first()){
                                break;
                            }
                            $i ++;
                        }
                    }

                    $richmenu = Richmenu::create([
                        'richmenuId' => $_richmenu->richMenuId,
                        'name' => $name,
                        'description' => '此選單不由平台建立，無選單描述，亦無區域行為資料。',
                        'size' => $_richmenu->size->width.'x'.$_richmenu->size->height,
                        'chatBarText' => $_richmenu->chatBarText,
                        'selected' => $_richmenu->selected,
                        'areas_form_data' => [],
                        'is_created_by_platform' => 0,

                        'parent_id' => 0,
                        'version' => 1,
                        'is_current_version' => 1,
                    ]);

                    $richmenu->parent_id = $richmenu->id;
                    $richmenu->save();

                    $richmenu->tags()->create([
                        'data' => ['isRelatedToRichmenu' => 1],
                    ]);

                    $path = $this->richmenuHelper->setRichmenuId($richmenu->richmenuId)->downloadRichmenuImage();

                    if($path){
                        $image = Image::create([
                            'name' => 'LINE主選單',
                            'disk' => 'gcs',
                            'path' => $path,
                        ]);
                        $richmenu->image()->sync($image->id);
                    }else{
                        $this->richmenuHelper->setAccessToken($channelAccessToken)
                        ->setRichmenuId($richmenu->richmenuId)
                        ->delete();

                        $richmenu->delete();
                    }

                    $solution4Connection->commit();

                }catch(\Exception $ex){

                    \Log::info($ex);
                    $solution4Connection->rollBack();
                }
            }
        }

        // 更新預設選單
        $response = $this->richmenuHelper->getDefault();
        if($response['success'] && ($defaultRichmenuId = $response['response'])){
            $defaultRichmenuId = json_decode($defaultRichmenuId)->richMenuId;
            $defaultRichmenu = Richmenu::where('richmenuId', $defaultRichmenuId)->whereNull('deleted_at')->first();

            Richmenu::where('richmenuId', '<>', $defaultRichmenuId)
            ->where('default', 1)
            ->whereNull('deleted_at')
            ->update([
                'default' => false,
            ]);

            if($defaultRichmenu){
                $defaultRichmenu->default = true;
                $defaultRichmenu->save();
            }
        }
    }

    public function store(Request $request, Solution $solution, Deployment $deployment){
        $areasHelper = new AreasHelper;
        $areasHelper->setDeployment($deployment)->setAreasData($request->form['area']['area']);

        $richmenuData = [
            'size' => [
                'width' => 2500,
            ],
            'selected' => $request->form['selected']['selected'],
            'name' => $request->form['name']['name'],
            'chatBarText' => $request->form['chatBarText']['chatBarText'],
            'areas' => NULL,
        ];

        if($request->form['size']['size'] == '2500x1686'){
            $richmenuData['size']['height'] = 1686;
            $areasHelper->setWidthAndHeight(2500, 1686);
        }else{
            $richmenuData['size']['height'] = 843;
            $areasHelper->setWidthAndHeight(2500, 843);
        }

        $richmenuData['areas'] = $areasHelper->get();

        $data = $this->richmenuHelper->setDeployment($deployment)
        ->setAccessToken($deployment->data->channelAccessToken)
        ->createAndUploadImage($richmenuData, $request->form['image']['file']);

        if($data['richmenuId']){

            $solution4Connection = DB::connection('solution_4');
            $solution4Connection->beginTransaction();

            try{
                $richmenu = Richmenu::create([
                    'name' => $request->form['name']['name'],
                    'description' => $request->form['description']['description'],
                    'richmenuId' => $data['richmenuId'],
                    'size' => $request->form['size']['size'],
                    'chatBarText' => $request->form['chatBarText']['chatBarText'],
                    'selected' => $request->form['selected']['selected'] == 'true',
                    'areas_form_data' => $areasHelper->getParsedAreasData(),
                    'is_created_by_platform' => 1,

                    'parent_id' => 0,
                    'version' => 1,
                    'is_current_version' => 1,
                ]);

                DB::connection('solution_4')
                ->table('richmenus')
                ->where('id', $richmenu->id)
                ->update([
                    'parent_id' => $richmenu->id,
                    'areas_form_data' => preg_replace('/"(invalid|active|id|start|end|show|disabled)"\s*:\s*"(true|false|\d*)"/', '"$1":$2', json_encode($areasHelper->getParsedAreasData())),
                ]);

                $richmenu->tags()->create([
                    'data' => ['isRelatedToRichmenu' => 1],
                ]);

                $richmenu->image()->syncWithoutDetaching($data['image']->id);

                // 標籤關聯
                $tags = $areasHelper->getTags();
                if($tags->isNotEmpty()) $richmenu->tags()->saveMany($tags);

                $solution4Connection->commit();

                return $richmenu;
            }catch(\Exception $ex){
                \Log::info($ex);

                $solution4Connection->rollBack();

                // 刪除選單
                $this->richmenuHelper->setRichmenuId($data['richmenuId'])->delete();

                // 刪除標籤
                $areasHelper->getTags()
                ->each(function($tag){
                    $tag->delete();
                });

                return false;
            }
        }else{
            return false;
        }
    }

    public function newVersion(Request $request, Solution $solution, Deployment $deployment, Richmenu $richmenu){
        $areasHelper = new AreasHelper;
        $areasHelper->setDeployment($deployment)->setAreasData($request->form['area']['area']);

        $richmenuData = [
            'size' => [
                'width' => 2500,
            ],
            'selected' => $request->form['selected']['selected'],
            'name' => $richmenu->name,
            'chatBarText' => $request->form['chatBarText']['chatBarText'],
            'areas' => NULL,
        ];

        if($request->form['size']['size'] == '2500x1686'){
            $richmenuData['size']['height'] = 1686;
            $areasHelper->setWidthAndHeight(2500, 1686);
        }else{
            $richmenuData['size']['height'] = 843;
            $areasHelper->setWidthAndHeight(2500, 843);
        }

        $richmenuData['areas'] = $areasHelper->get();

        $data = $this->richmenuHelper->setDeployment($deployment)
        ->setAccessToken($deployment->data->channelAccessToken)
        ->setRichmenu($richmenu)
        ->createAndUploadImage($richmenuData, $request->form['image']['file']);

        if($data['richmenuId']){

            $solution4Connection = DB::connection('solution_4');
            $solution4Connection->beginTransaction();

            try{
                $version = DB::connection('solution_4')
                ->table('richmenus')
                ->select('version')
                ->where('parent_id', $richmenu->parent_id)
                ->orderBy('version', 'desc')
                ->first();

                $richmenu = Richmenu::create([
                    'name' => $richmenu->name,
                    'description' => $richmenu->description,
                    'richmenuId' => $data['richmenuId'],
                    'size' => $request->form['size']['size'],
                    'chatBarText' => $request->form['chatBarText']['chatBarText'],
                    'selected' => $request->form['selected']['selected'] == 'true',
                    'areas_form_data' => $areasHelper->getParsedAreasData(),
                    'is_created_by_platform' => 1,

                    'parent_id' => $richmenu->parent_id,
                    'version' => $version->version + 1,
                    'is_current_version' => 0,
                ]);

                $richmenu->tags()->create([
                    'data' => ['isRelatedToRichmenu' => 1],
                ]);

                $richmenu->image()->syncWithoutDetaching($data['image']->id);

                // 標籤關聯
                $tags = $areasHelper->getTags();
                if($tags->isNotEmpty()) $richmenu->tags()->saveMany($tags);

                $solution4Connection->commit();

                return $richmenu;
            }catch(\Exception $ex){
                \Log::info($ex);
                $solution4Connection->rollBack();

                // 刪除選單
                $this->richmenuHelper->setRichmenuId($data['richmenuId'])->delete();

                // 刪除標籤
                $areasHelper->getTags()
                ->each(function($tag){
                    $tag->delete();
                });

                return false;
            }
        }else{
            return false;
        }
    }

    public function toggleDefault(Request $request, Deployment $deployment, Richmenu $richmenu)
    {
        $res = $this->richmenuHelper->setAccessToken($deployment->data->channelAccessToken)
        ->setRichmenuId($richmenu->richmenuId)
        ->{$richmenu->default ? 'cancelDefault' : 'setDefault'}();

        if($res['success']){
            if(!$richmenu->default && ($oldRichmenu = Richmenu::where('default', 1)->first())){
                $oldRichmenu->default = false;
                $oldRichmenu->save();
            }

            $richmenu->default = !$richmenu->default;
            $richmenu->save();
            return true;

        }else{
            return false;
        }
    }

    public function cancelBinding(Request $request, Deployment $deployment, Richmenu $richmenu)
    {
        $currentRichmenu = Richmenu::select('id', 'default')
        ->where('is_current_version', 1)
        ->where('parent_id', $richmenu->parent_id)
        ->first();

        // 沒有正在使用的版本
        if(!$currentRichmenu){
            $richmenu->is_current_version = 1;
            $richmenu->save();
            return true;
        }

        // 有正在使用的版本
        $res = $this->richmenuHelper->setDeployment($deployment)->cancelBinding($currentRichmenu);
        return $res['success'];
    }

    public function switchVersion(Request $request, Deployment $deployment, Richmenu $richmenu)
    {
        $currentRichmenu = Richmenu::select('id', 'default')
        ->where('is_current_version', 1)
        ->where('parent_id', $richmenu->parent_id)
        ->first();

        // 沒有正在使用的版本
        if(!$currentRichmenu){
            $richmenu->is_current_version = 1;
            $richmenu->save();
            return true;
        }

        // 有正在使用的版本
        $res = $this->richmenuHelper->setDeployment($deployment)
        ->setRichmenu($richmenu)
        ->switch($currentRichmenu);

        if($res['success']){
            DB::connection('solution_4')
            ->table('richmenus')
            ->where('parent_id', $richmenu->parent_id)
            ->update([
                'is_current_version' => 0,
            ]);

            if($currentRichmenu->default){
                $res = $this->richmenuHelper->setAccessToken($deployment->data->channelAccessToken)
                ->setRichmenuId($richmenu->richmenuId)
                ->setDefault();

                if($res['success']){
                    $currentRichmenu->default = 0;
                    $currentRichmenu->save();
                    $richmenu->default = 1;
                }
            }

            $richmenu->is_current_version = 1;
            $richmenu->save();
            return true;

        }else{
            return false;
        }
    }
}
