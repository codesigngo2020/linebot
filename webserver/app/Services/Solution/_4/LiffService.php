<?php

namespace App\Services\Solution\_4;

use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Liff;

// Helper
use App\Helpers\Solution\_4\Liff\LiffHelper;

class LiffService
{

    public $liffHelper;

    public function __construct(LiffHelper $liffHelper)
    {
        $this->liffTypes = ['compact', 'tall', 'full'];
        $this->liffHelper = $liffHelper;
    }

    public function index(Request $request, Solution $solution, Deployment $deployment)
    {
        $res = $this->liffHelper->setAccessToken($deployment->data->channelAccessToken)->getAll();


        if(!$res['success']){
            if(strpos($res['response']->getMessage(), 'no LIFF app found for channel') !== false){

                // 沒有任何liff，建立liff
                Liff::query()->delete();

                foreach($this->liffTypes as $type){
                    $res = $this->liffHelper->add([
                        'view' => [
                            'type' => $type,
                            'url' => 'https://'.$deployment->data->webhookDomain.'/liff/redirect',
                        ],
                        'description' => 'BitTech',
                    ]);

                    if($res['success']){
                        Liff::create([
                            'liff_id' => json_decode($res['response'])->liffId,
                            'description' => 'BitTech',
                            'view' => [
                                'type' => $type,
                                'url' => 'https://'.$deployment->data->webhookDomain.'/liff/redirect',
                            ],
                            'features' => NULL,
                            'is_created_by_platform' => 1,
                        ]);
                    }
                }
            }else{
                // /////???????
            }
        }else{
            // API回應成功
            $liffTypes = collect($this->liffTypes);
            $liffs = collect(json_decode($res['response'])->apps);

            // 更新已有選單資訊
            $liffs = $liffs->map(function($liff) use(&$liffTypes){
                $liffInDB = Liff::firstOrCreate([
                    'liff_id' => $liff->liffId,
                ],[
                    'description' => $liff->description,
                    'view' => $liff->view,
                    'features' => $liff->features ?? NULL,
                ]);

                if($liffInDB->is_created_by_platform){
                    $index = $liffTypes->search($liffInDB->view->type);
                    if($index !== false) $liffTypes->forget($index);
                }

                return $liffInDB->id;
            });

            // 刪除資料庫中存在，但不存在的liff
            $removeLiffsId = Liff::all()->pluck('id')->diff($liffs);
            if($removeLiffsId->isNotEmpty()){
                Liff::whereIN('id', $removeLiffsId)->delete();
            }

            // 建立平台沒有的高度
            foreach($liffTypes as $type){
                $res = $this->liffHelper->add([
                    'view' => [
                        'type' => $type,
                        'url' => 'https://'.$deployment->data->webhookDomain.'/liff/redirect',
                    ],
                    'description' => 'BitTech',
                ]);

                if($res['success']){
                    Liff::create([
                        'liff_id' => json_decode($res['response'])->liffId,
                        'description' => 'BitTech',
                        'view' => [
                            'type' => $type,
                            'url' => 'https://'.$deployment->data->webhookDomain.'/liff/redirect',
                        ],
                        'features' => NULL,
                        'is_created_by_platform' => 1,
                    ]);
                }
            }

            $page = intval($request->page);
            $lastPage = ceil(Liff::count() / 6);

            if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

            $liffs = Liff::select('id', 'liff_id', 'description', 'view', 'features', 'is_created_by_platform')
            ->offset(($page - 1) * 6)
            ->limit(6)
            ->get();

            $paginator = collect([
                'currentPage' => $page,
                'lastPage' => $lastPage,
            ]);
        }

        return compact('liffs', 'paginator');
    }


}
