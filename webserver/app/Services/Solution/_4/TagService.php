<?php

namespace App\Services\Solution\_4;

use DB;
use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Tag;
use App\Models\Platform\Solution\_4\User;

// Helper
use App\Helpers\Solution\_4\Message\FormaterHelper;

class TagService
{

    public function index(Request $request, Solution $solution, Deployment $deployment)
    {
        $total_users_count = User::count();

        $page = intval($request->page);
        $lastPage = ceil(Tag::whereNotNull('tags.name')->count() / 5);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $tags = Tag::select('tags.id', 'tags.name', 'tags.taggable_id', 'tags.taggable_type', 'tags.data')
        ->selectRaw('FLOOR(COUNT(DISTINCT(tag_user.user_id))*100/'.$total_users_count.') as users_percentage')
        ->selectRaw('COUNT(DISTINCT(tag_user.id)) as total_count')

        ->leftJoin('tag_user', 'tag_user.tag_id', '=', 'tags.id')

        ->whereNotNull('tags.name')

        ->groupBy('tags.id')
        ->orderBy('tags.id', 'desc')
        ->offset(($page - 1) * 5)
        ->limit(5)
        ->get()
        ->each(function($tag) use($deployment){
            $tag->description = $tag->getDescription($deployment);
        });

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return compact('tags', 'paginator');
    }

}
