<?php

namespace App\Services\Solution\_4;

use DB;
use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\User;

class UserService
{

    public function index(Request $request, Solution $solution, Deployment $deployment)
    {
        $page = intval($request->page);
        $lastPage = ceil(User::count() / 5);

        if(!$page || $lastPage < 1 || $page > $lastPage) $page = 1;

        $users = User::select('users.id', 'users.display_name', 'users.user_id', 'users.picture_url', 'users.status_message', 'users.is_follow')
        ->orderBy('users.id', 'desc')
        ->offset(($page - 1) * 5)
        ->limit(5)
        ->get()
        ->each(function($user){
            $user->groups = DB::connection('solution_4')
            ->table('groups')
            ->select('groups.id', 'groups.name')
            ->join('group_user', function($join) use($user){
                $join->on('group_user.group_id', '=', 'groups.id')
                ->where('group_user.user_id', $user->id)
                ->whereNull('group_user.deleted_at');
            })
            ->distinct()
            ->get();
        });

        $paginator = collect([
            'currentPage' => $page,
            'lastPage' => $lastPage,
        ]);

        return compact('users', 'paginator');
    }

}
