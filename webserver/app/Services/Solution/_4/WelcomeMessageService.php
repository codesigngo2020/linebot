<?php

namespace App\Services\Solution\_4;

use DB;
use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\WelcomeMessage;

// Helper
use App\Helpers\Solution\_4\Message\FormaterHelper;

class WelcomeMessageService
{

    public function store(Request $request, Solution $solution, Deployment $deployment){
        $form = $request->form;

        // 新建訊息
        if($form['messageType']['messageType'] != 'template'){
            $formaterHelper = new FormaterHelper;
        }

        // 建立推播訊息
        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{
            $welcomeMessage = WelcomeMessage::create([
                'name' => $form['name']['name'],
                'description' => $form['description']['description'],

                'template_id' => $form['messageType']['messageType'] == 'template' ? $form['message']['value']['value'] : NULL,
                'messages' => $form['messageType']['messageType'] != 'template' ? $formaterHelper->setDeployment($deployment)
                ->setMessageData($form['messages']['messages'])
                ->get(): NULL,
                'messages_form_data' => $form['messageType']['messageType'] != 'template' ? $formaterHelper->getParsedMessagesData() : NULL,
                'active' => $form['active']['active'],
            ]);

            if($form['messageType']['messageType'] != 'template'){
                DB::connection('solution_4')
                ->table('welcome_messages')
                ->where('id', $welcomeMessage->id)
                ->update([
                    'messages_form_data' => preg_replace('/"(invalid|active|id|start|end|show|disabled)"\s*:\s*"(true|false|\d*)"/', '"$1":$2', json_encode($formaterHelper->getParsedMessagesData())),
                ]);
            }

            foreach($form['periods']['periods'] as $period){
                $welcomeMessage->periods()->create([
                    'start_date' => $period['date']['start']['start'],
                    'end_date' => $period['hasEnd']['hasEnd'] == 'true' ? $period['date']['end']['end'] : NULL,
                    'start_time' => $period['time']['start']['start'],
                    'end_time' => $period['time']['end']['end'],
                ]);
            }

            $welcomeMessage->tags()->create([
                'data' => ['isRelatedToWelcomeMessage' => 1],
            ]);

            if($form['messageType']['messageType'] != 'template'){
                // 圖片關聯
                $images = $formaterHelper->getImages();
                if($images->isNotEmpty()) $welcomeMessage->images()->sync($images->pluck('id'));

                // 影片關聯
                $videos = $formaterHelper->getVideos();
                if($videos->isNotEmpty()) $welcomeMessage->videos()->sync($videos->pluck('id'));

                // 標籤關聯
                $tags = $formaterHelper->getTags();
                if($tags->isNotEmpty()) $welcomeMessage->tags()->saveMany($tags);
            }

            $solution4Connection->commit();
            return $welcomeMessage;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }

    }

}
