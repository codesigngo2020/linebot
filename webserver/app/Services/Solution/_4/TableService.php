<?php

namespace App\Services\Solution\_4;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Deployment;
use App\Models\Platform\Solution\_4\Table;
use App\Models\Platform\Solution\_4\User;

class TableService
{

    public function store(Request $request, Solution $solution, Deployment $deployment){
        $form = $request->form;

        $solution4Connection = DB::connection('solution_4');
        $solution4Connection->beginTransaction();

        try{

            $columnsName = [];
            foreach(['columns', 'additionalColumns'] as $type){
                if($type == 'additionalColumns' && !isset($form[$type][$type])) break;
                foreach($form[$type][$type] as $column){
                    $columnsName[] = $column['name']['name'];
                }
            }

            $table = Table::create([
                'name' => $form['name']['name'],
                'description' => $form['description']['description'],
                'type' => $form['type']['type'],
                'table_name' => '',
                'editable_columns' => $columnsName,
            ]);

            $table->table_name = 'custom_table_'.$table->id;
            if($table->type == 'coupon'){
                $settings = [
                    'random' => $form['random']['random'] == 'true',
                    'limit' => [
                        'active' => $form['limit']['active']['active'] == 'true',
                    ],
                    'issueDatetime' => [
                        'active' => $form['issueDatetime']['active']['active'] == 'true',
                    ],
                ];

                // 數量限制
                if($settings['limit']['active']) $settings['limit']['count'] = intval($form['limit']['count']['count']);

                // 時間限制
                if($settings['issueDatetime']['active']){
                    foreach(['date', 'time'] as $type){
                        $dts = explode('~', str_replace(' ', '', $form['issueDatetime'][$type][$type]));
                        $settings['issueDatetime'][$type] = [
                            'from' => $dts[0],
                            'to' => $dts[1],
                        ];
                    }
                }

                $table->settings = $settings;
            }
            $table->save();

            // 建立資料表
            Schema::connection('solution_4')->create($table->table_name, function (Blueprint $table) use($form){
                $table->bigIncrements('id');

                foreach($form['columns']['columns'] as $column){
                    $this->createColumn($table, $column, 'originalName');
                }

                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            });

            // 新增 csv 資料
            DB::connection('solution_4')
            ->table($table->table_name)
            ->insert($form['rows']['rows']);

            // 修正欄位名稱
            Schema::connection('solution_4')->table($table->table_name, function (Blueprint $table) use($form){
                foreach($form['columns']['columns'] as $column){
                    if($column['name']['name'] != $column['originalName']['originalName']){
                        $table->renameColumn($column['originalName']['originalName'], $column['name']['name']);
                    }
                }

                if(isset($form['additionalColumns']['additionalColumns'])){
                    foreach($form['additionalColumns']['additionalColumns'] as $column){
                        $this->createColumn($table, $column, 'name');
                    }
                }

                if($form['type']['type'] == 'coupon'){
                    $table->unsignedInteger('user_id')->nullable()->index();
                    $table->timestamp('issued_at')->nullable()->index();
                    $table->timestamp('redeemed_at')->nullable()->index();
                    $table->string('redemption_url')->nullable();
                    $table->unsignedInteger('staff_member_id')->nullable();
                    $table->string('qrcode_url')->nullable();
                    $table->string('code')->nullable()->unique();
                }
            });

            $solution4Connection->commit();
            return $table;

        }catch(\Exception $ex){
            \Log::info($ex);
            $solution4Connection->rollBack();

            return false;
        }

    }

    private function createColumn(&$table, $column, $nameType)
    {
        switch($column['type']['value']['value']){
            case 'integer':
            case 'unsignedInteger':
            case 'date':
            $col = $table->{$column['type']['value']['value']}($column[$nameType][$nameType]);

            break;

            case 'float':
            $col = $table->decimal($column[$nameType][$nameType], 10, 2);
            break;

            case 'varchar':
            $col = $table->string($column[$nameType][$nameType]);
            break;

            case 'text':
            $col = $table->text($column[$nameType][$nameType]);
            break;

            case 'timestamp':
            $col = $table->timestamp($column[$nameType][$nameType]);
            break;
        };

        if($column['canBeNull']['canBeNull'] == 'true' || $column['type']['value']['value'] == 'timestamp') $col->nullable();
        if($column['type']['value']['value'] != 'text'){
            $col->{ $column['isUnique']['isUnique'] == 'true' ? 'unique' : 'index' }();
        }
    }

    public function redeemCoupon(Request $request, $tableId)
    {
        $res = ['success' => false, 'message' => ''];

        // 驗證參數
        $table = Table::where('id', $tableId)->where('type', 'coupon')->whereNull('deleted_at')->first();
        if(!$table || !$request->user_id || !$request->coupon_id || !$request->code){
            $res['message'] = '很抱歉！您的優惠券是無效的。';
            return $res;
        }

        // 驗證優惠券
        $coupon = DB::connection('solution_4')
        ->table('custom_table_'.$table->id)
        ->select('id', 'user_id', 'name', 'code', 'issued_at', 'redeemed_at')
        ->where('id', $request->coupon_id)
        ->where('user_id', $request->user_id)
        ->where('code', $request->code)
        ->first();

        if(!$coupon){
            $res['message'] = '很抱歉！您的優惠券是無效的。';
            return $res;
        }
        $user = User::find($request->user_id);
        if(!$user){
            $res['message'] = '很抱歉！您的優惠券是無效的。';
            return $res;
        }

        if($coupon->redeemed_at){
            $res['message'] = '很抱歉！您的優惠券已經兌換完畢。';
            return $res;
        }

        // 兌換優惠券
        $now = date('Y-m-d H:i:s');
        $redeemRes = DB::connection('solution_4')
        ->table('custom_table_'.$table->id)
        ->where('id', $coupon->id)
        ->update([
            'staff_member_id' => auth()->user()->id,
            'redeemed_at' => $now,
        ]);

        if(!$redeemRes){
            $res['message'] = '很抱歉！您的優惠券兌換失敗，請重新嘗試。';
            return $res;
        }

        $coupon->redeemed_at = $now;

        // 兌換成功
        $res['success'] = true;
        $res['message'] = '恭喜您完成兌換！';
        $res['coupon'] = $coupon;
        $res['user'] = $user;
        return $res;
    }

}
