<?php

namespace App\Services\Paypal;

use Illuminate\Http\Request;

// Model
use App\Models\Platform\Solution\Solution;
use App\Models\Platform\Solution\Plan;

// Helper
use App\Helpers\Basic\Paypal\AgreementHelper;

class PaypalService
{
  public function createAgreementAndGetToken(Solution $solution, Plan $plan)
  {
    $agreementHelper = new AgreementHelper;
    $approvalUrl = $agreementHelper->createAgreement($solution, $plan);

    preg_match('/&?token=([0-9a-zA-Z-]+)&?/', parse_url($approvalUrl, PHP_URL_QUERY), $match);
    return $match[1];
  }

}
