// Replace all SVG images with inline SVG
Vue.component('v-svg', {
    props: {
        src: String,
        width: String,
        height: String,
    },
    data: function(){
        return {
            xmlns: null,
            viewBox: null,
        }
    },
    template: '<svg xmlns="xmlns" :viewBox="viewBox" :width="width" :height="height"></svg>',
    created(){
        axios
        .get(this.src)
        .then(response => {
            var tmp = document.createElement('div');
            tmp.innerHTML = response.data;
            var svg = tmp.getElementsByTagName('svg')[0];

            this.xmlns = svg.getAttribute('xmlns');
            this.viewBox = svg.getAttribute('viewBox');
            this.$el.innerHTML = svg.innerHTML;
        });
    }
});

// 字串全部取代
String.prototype.replaceAll = function(search, replacement){
    var target = this;
    target = target.replace(search, replacement);
    if(target.match(search)){
        return target.replaceAll(search, replacement);
    }else{
        return target;
    }
};

// 字串第一個字母大寫
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

// Array chunk 分割
Object.defineProperty(Array.prototype, 'chunk', {value: function(n) {
    return Array.from(Array(Math.ceil(this.length/n)), (_,i)=>this.slice(i*n,i*n+n));
}});

// 防止上一頁或下一頁
function disablePageSwitch(){
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };

    location.hash = "prev";
    location.hash = "curr";
    history.go(-1);
};
