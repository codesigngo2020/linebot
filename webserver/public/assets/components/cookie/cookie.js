function setCookie(cname, cvalue){
  document.cookie = cname + '=' + JSON.stringify(cvalue) + '; expires=0; path=/';
}

function getCookie(cname){
  var name = cname + '=',
  decodedCookie = decodeURIComponent(document.cookie),
  ca = decodedCookie.split(';');

  for(var i = 0; i < ca.length; i ++){
    var c = ca[i];
    while(c.charAt(0) == ' '){
      c = c.substring(1);
    }

    if(c.indexOf(name) == 0) return c.substring(name.length, c.length);
  }
  return '';
}
