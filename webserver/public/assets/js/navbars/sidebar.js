var sidebar = new Vue({
    el: '#sidebar',
    data: {
        show: true,
    },
    methods: {
        showSidebar(show){
            this.show = show;
        }
    }
});
