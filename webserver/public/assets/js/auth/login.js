var login_form = new Vue({
  el: '#login_form',
  data: {
    pass: {
      show: false
    }
  },
  methods: {
    showPass: function(){
      this.$refs.password.type = 'text';
      this.pass.show = true;
    }
  }
});
