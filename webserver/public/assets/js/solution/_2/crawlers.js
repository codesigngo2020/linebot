var app = new Vue({
  el: '#app',
  data: {
    plan: args.plan,
    crawlers_count: args.crawlers_count,
    posts_count: args.posts_count,

    crawlers: {},

    submitting: false,
    is_success: false,
  },
  methods: {
    AllInvalidToFalse(obj){
      for(key in obj){
        if(key == 'invalid'){
          obj[key] = false;
          obj['info'] = '';
        }else if(typeof(obj[key]) === 'object'){
          this.AllInvalidToFalse(obj[key]);
        }
      }
    },
    updateFormData(index, data){
      this.crawlers = data.crawlers;
    },
    submit(){
      if(!this.submitting && !this.is_success){
        var vm = this;
        this.submitting = true;

        this.AllInvalidToFalse(this.crawlers);
        axios
        .post(route('solution.2.deployment.crawlers.update', {
          deployment: args.deployment
        }), {
          _token: args._token,
          crawlers: this.crawlers,
        })
        .then(response => {
          console.log(response);
          if(response.data.success == true){
            this.is_success = true;
            setTimeout(() => {
              vm.is_success = false;
            }, 2000);
          };
          this.submitting = false;
        }).catch(error => {
          var errors = error.response.data.errors;
          this.submitting = false;
          var invalid_crawler = null;
          for(error in errors){
            console.log(error);
            var invalid_crawler_index = error.match(/^crawlers\.([0-9]+)\./)[1];
            if(!invalid_crawler || invalid_crawler_index < invalid_crawler) invalid_crawler = invalid_crawler_index;

            // 找出最後一個「.」後的字串
            var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
            // 如果是value，表示是select，再往前找一個value
            if(last == 'value'){
              select_last = error.match(/\.(value\.value)$/);
              // 如果符合「.value.value」，表示是多層value
              if(select_last) last = select_last.slice(-1)[0];
            }

            // 替換數字成以[]包覆表示
            error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

            info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
            invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
            try{
              this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
              this.$set(eval('this.'+invalid_ref), 'invalid', true);
            }catch(e){

            }
          }

          if(invalid_crawler) $(app.$refs.form3.$refs['crawler'+invalid_crawler]).tab('show');
        });

        this.$emit('update-form-data', null, this._form);
      }
    }
  }
});
