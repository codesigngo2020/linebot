var deploymentName = new Vue({
  el: '#deployment-name',
  data: {
    is_success: false,
    submitting: false,
    form: {
      name: {
        name: args.name,
        info: '',
        invalid: false,
      },
    },
  },
  methods: {
    AllInvalidToFalse(obj){
      for(key in obj){
        if(key == 'invalid'){
          obj[key] = false;
          obj['info'] = '';
        }else if(typeof(obj[key]) === 'object'){
          this.AllInvalidToFalse(obj[key]);
        }
      }
    },
    submit(){
      if(!this.submitting && !this.is_success){
        var vm = this;
        this.submitting = true;

        this.AllInvalidToFalse(this.form);
        axios
        .patch(route('solution.2.deployment.settings.update', {
          deployment: args.deployment
        }), {
          _token: args._token,
          form: this.form,
        })
        .then(response => {
          console.log(response);
          if(response.data.success == true){
            this.is_success = true;
            setTimeout(() => {
              vm.is_success = false;
            }, 2000);
          };
          this.submitting = false;
        }).catch(error => {
          var errors = error.response.data.errors;
          this.submitting = false;
          var invalid_crawler = null;
          for(error in errors){
            console.log(error);

            // 找出最後一個「.」後的字串
            var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
            // 如果是value，表示是select，再往前找一個value
            if(last == 'value'){
              select_last = error.match(/\.(value\.value)$/);
              // 如果符合「.value.value」，表示是多層value
              if(select_last) last = select_last.slice(-1)[0];
            }

            // 替換數字成以[]包覆表示
            error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

            info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
            invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
            try{
              this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
              this.$set(eval('this.'+invalid_ref), 'invalid', true);
            }catch(e){

            }
          }
        });
      }
    }
  },
});

var plan = new Vue({
  el: '#plan',
});

var payment = new Vue({
  el: '#payment',
  data: {
    is_success: false,
    is_error: false,
    submitting: false,
  },
  methods: {
    submit(){
      if(!this.submitting && !this.is_success){
        var vm = this;
        this.submitting = true;

        axios
        .patch(route('solution.2.deployment.settings.updatePaymentLink', {
          deployment: args.deployment
        }), {
          _token: args._token,
        })
        .then(response => {
          console.log(response);
          if(response.data.success == true){
            this.is_success = true;
            this.$refs.link.href = response.data.approvalUrl;
            setTimeout(() => {
              vm.is_success = false;
            }, 2000);
          };
          this.submitting = false;
        }).catch(error => {
          this.is_error = true;
          this.submitting = false;
          setTimeout(() => {
            vm.is_error = false;
          }, 2000);
        });
      }
    }
  }
});
