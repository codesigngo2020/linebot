var logs_table = new Vue({
  el: '#logs-table',
  data: {
    query: '',
    logs: args.logs,
    paginator: args.paginator,
    is_loading: false,
    order: {
      column: 'id',
      dir: 'desc',
    },
    typeFormat: {
      'deploy-success': '部署成功',

      'deployment-active': '啟用部署',
      'deployment-suspend': '暫停部署',
      'deployment-destroyed': '刪除部署',

      'crawl-success': '請求成功',
      'crawl-fail': '請求失敗',

      'post-success': '刊登成功',
      'post-fail': '刊登失敗',
    },
  },
  methods: {
    changePage(page){
      this.getLogs(page);
    },
    search(){
      this.getLogs(1);
    },
    sort(column){
      if(column != this.order.column){
        this.order.column = column;
        this.order.dir = 'asc';
      }else{
        this.order.dir = this.order.dir == 'asc' ? 'desc' : 'asc';
      }
      this.getLogs(1);
    },
    getLogs(page){
      this.is_loading = true;
      axios
      .get(route('solution.2.deployment.logs.getLogs', {
        deployment: args.deployment,
        page: page,
        column: this.order.column,
        dir: this.order.dir,
        q: this.query,
      }))
      .then(response => {
        this.paginator = response.data.paginator;
        this.logs = response.data.logs;
        this.is_loading = false;
      }).catch(error => {
        this.is_loading = false;
        console.log(error);
      });
    }
  },
});
