var backgroundColors = ['#1F5192', '#2C7BE5', '#A6C5F7', '#d5e5fa', '#D2DDEC'],
crawlers_key = Object.keys(args['posts-doughnut-chart-data']['data']),
crawlers_count = crawlers_key.length,
start = crawlers_count < 3 ? 1 : 0,
selectedColors = backgroundColors.splice(start, crawlers_count);


var posts_count_datasets = [],
requests_count_datasets = [],
i = 0;
Object.keys(args['main-chart-data'].data.posts_count).forEach(function(id){
  posts_count_datasets.push({
    label: id,
    backgroundColor: selectedColors[i],
    data: args['main-chart-data'].data.posts_count[id],
  });
  i ++;
});
i = 0;
Object.keys(args['main-chart-data'].data.requests_count).forEach(function(id){
  requests_count_datasets.push({
    label: id,
    backgroundColor: selectedColors[i],
    data: args['main-chart-data'].data.requests_count[id],
  });
  i ++;
});

var posts_count_chart = new Chart(document.getElementById('posts-count-chart'), {
  type: 'bar',
  data: {
    labels: args['main-chart-data'].data.date,
    datasets: posts_count_datasets,
  },
  options: {
    scales: {
      yAxes: [{
        gridLines: {
          display: true,
          lineWidth: 1,
          color: '#e3ebf6',
          zeroLineWidth: 1,
          zeroLineColor: '#e3ebf6',
        },
        allowDecimals: false,
        stacked: true,
      }],
      xAxes: [{
        stacked: true,
      }],
    }
  }
}),
requests_count_chart = new Chart(document.getElementById('requests-count-chart'), {
  type: 'bar',
  data: {
    labels: args['main-chart-data'].data.date,
    datasets: requests_count_datasets,
  },
  options: {
    scales: {
      yAxes: [{
        gridLines: {
          display: true,
          lineWidth: 1,
          color: '#e3ebf6',
          zeroLineWidth: 1,
          zeroLineColor: '#e3ebf6',
        },
        allowDecimals: false,
        stacked: true,
      }],
      xAxes: [{
        stacked: true,
      }],
    }
  }
});

var posts_doughnut_chart = new Chart(document.getElementById('posts-doughnut-chart'), {
  type: 'doughnut',
  data: {
    datasets: [
      {
        data: Object.values(args['posts-doughnut-chart-data']['data']),
        backgroundColor: selectedColors,
        borderWidth: Array(crawlers_count).fill(2),
        hoverBorderWidth: Array(crawlers_count).fill(2),
        hoverBorderColor: Array(crawlers_count).fill('#fff'),
      }
    ],

    labels: Array.from(Array(crawlers_count), (d, i) => '爬蟲'+crawlers_key[i]),
  }
  //options: options
});


var crawlers_table = new Vue({
  el: '#crawlers_table',
  methods: {
    crawlerActiveChange(crawler_id, checked){

      axios
      .put(route('solution.2.deployment.crawlers.active', {
        deployment: args.deployment,
        crawler: crawler_id,
      }), {
        _token: args._token,
        active: checked,
      })
      .then(response => {
        this.$refs['crawler-'+crawler_id].checked = response.data;
      }).catch(error => {
        this.$refs['crawler-'+crawler_id].checked = !checked;
      });
    }
  }
});
