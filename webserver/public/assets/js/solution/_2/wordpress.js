var form = new Vue({
  el: '#form',
  data: {
    is_success: false,
    submitting: false,
    form: {
      1: {
        is_success: false,
        domain: {
          domain: args.form[1].domain,
          info: '',
          invalid: false,
        },
        key: {
          key: args.form[1].key,
          info: '',
          invalid: false,
        },
        testing: false,
      },
      2: {
        duration: {
          duration: args.form[2].start.replace(':', ' : ')+' - '+args.form[2].end.replace(':', ' : '),
          info: '',
          invalid: false,
        },
        postFrequency: {
          value: args.form[2].frequency,
          info: '',
          invalid: false,
        },
        order: {
          order: args.form[2].order,
          info: '',
          invalid: false,
        }
      },
    },
  },
  methods: {
    AllInvalidToFalse(obj){
      for(key in obj){
        if(key == 'invalid'){
          obj[key] = false;
          obj['info'] = '';
        }else if(typeof(obj[key]) === 'object'){
          this.AllInvalidToFalse(obj[key]);
        }
      }
    },
    testDomain(){
      this.form[1].testing = true;
      this.form[1].domain.info = '';
      this.form[1].key.info = '';
      this.form[1].domain.invalid = false;
      this.form[1].key.invalid = false;
      axios
      .post(route('solution.2.testDomain'), {
        _token: args._token,
        domain: this.form[1].domain.domain,
        key: this.form[1].key.key,
      })
      .then(response => {
        if(response.data == 'associated'){
          this.form[1].testing = false;
          this.form[1].is_success = true;
          this.form[1].domain.info = '';
          this.form[1].key.info = '';
          this.form[1].domain.invalid = false;
          this.form[1].key.invalid = false;
        }else{
          this.form[1].testing = false;
          this.form[1].domain.info = 'The domain or the key is wrong';
          this.form[1].key.info = 'The domain or the key is wrong';
          this.form[1].domain.invalid = true;
          this.form[1].key.invalid = true;
        }
      }).catch(error => {
        this.form[1].testing = false;
        var errors = error.response.data.errors;
        if(errors.domain){
          this.form[1].domain.info = errors.domain[0];
          this.form[1].domain.invalid = true;
        }else{
          this.form[1].domain.info = '';
          this.form[1].domain.invalid = false;
        }
        if(errors.key){
          this.form[1].key.info = errors.key[0];
          this.form[1].key.invalid = true;
        }else{
          this.form[1].key.info = '';
          this.form[1].key.invalid = false;
        }
      });
    },
    submit(){
      if(!this.submitting && !this.is_success){
        var vm = this;
        this.submitting = true;

        this.AllInvalidToFalse(this.form);
        axios
        .patch(route('solution.2.deployment.wordpress.update', {
          deployment: args.deployment
        }), {
          _token: args._token,
          form: this.form,
        })
        .then(response => {
          console.log(response);
          if(response.data.success == true){
            this.is_success = true;
            setTimeout(() => {
              vm.is_success = false;
            }, 2000);
          };
          this.submitting = false;
        }).catch(error => {
          var errors = error.response.data.errors;
          this.submitting = false;
          var invalid_crawler = null;
          for(error in errors){
            console.log(error);

            // 找出最後一個「.」後的字串
            var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
            // 如果是value，表示是select，再往前找一個value
            if(last == 'value'){
              select_last = error.match(/\.(value\.value)$/);
              // 如果符合「.value.value」，表示是多層value
              if(select_last) last = select_last.slice(-1)[0];
            }

            // 替換數字成以[]包覆表示
            error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

            info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
            invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
            try{
              this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
              this.$set(eval('this.'+invalid_ref), 'invalid', true);
            }catch(e){

            }
          }
        });
      }
    }
  },
  mounted(){
    var vm = this;
    $(this.$refs.duration).mask(this.$refs.duration.getAttribute('data-mask'), {
      translation: {
        'A': {
          pattern: /[0-2]/
        },
        'B': {
          pattern: /[0-9]/
        },
        'C': {
          pattern: /[0-6]/
        }
      },
      onChange(cep){
        console.log(cep)
        vm.form[2].duration.duration = cep;
      },
    });
    $(this.$refs.duration).val(this.form[2].duration.duration);
  },
});
