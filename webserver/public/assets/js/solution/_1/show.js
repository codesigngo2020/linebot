var main_chart = new Chart(document.getElementById('main-chart'), {
  type: 'line',
  data: {
    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    datasets: [{
      label: 'Performance',
      borderColor: '#2C7BE5',
      pointBackgroundColor: '#2C7BE5',
      data: [0,10,5,15,10,20,15,25,20,30,25,40]
    },
    {
      label: 'Performance1',
      borderColor: '#2C7BE5',
      pointBackgroundColor: '#2C7BE5',
      data: [0,10,5,15,10,20,15,25,20,30,25,40]
    }]
  },
  options: {
    scales: {
      yAxes: [{
        gridLines: {
          display: true,
          lineWidth: 1,
          color: '#e3ebf6',
          zeroLineWidth: 1,
          zeroLineColor: '#e3ebf6',
        }
      }]
    }
  }
});

var backgroundColors = ['#1F5192', '#2C7BE5', '#A6C5F7', '#d5e5fa', '#D2DDEC'],
start = args['posts-doughnut-chart-data']['data'].length < 3 ? 1 : 0,
selectedColors = backgroundColors.splice(start, args['posts-doughnut-chart-data']['data'].length);

var posts_doughnut_chart = new Chart(document.getElementById('posts-doughnut-chart'), {
  type: 'doughnut',
  data: {
    datasets: [
      {
        data: args['posts-doughnut-chart-data']['data'],
        backgroundColor: selectedColors,
        borderWidth: Array(args['posts-doughnut-chart-data']['data'].length).fill(2),
        hoverBorderWidth: Array(args['posts-doughnut-chart-data']['data'].length).fill(2),
        hoverBorderColor: Array(args['posts-doughnut-chart-data']['data'].length).fill('#fff'),
      }
    ],

    labels: Array.from(Array(args['posts-doughnut-chart-data']['data'].length), (d, i) => '爬蟲'+ (i + 1)),
  }
  //options: options
});


var crawlers_table = new Vue({
  el: '#crawlers_table',
});

var keys_table = new Vue({
  el: '#keys_table',
});
