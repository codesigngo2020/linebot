var deploymentContent = new Vue({
    el: '#deployment-content',
    data: {
        statistics: args.statistics,
        broadcasts: args.broadcasts,
        autoAnswers: args.autoAnswers,

        showAll: {
            broadcasts: false,
            autoAnswers: false,
        },

        // datetimeSetting: {
        //     days: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        //     startData: {year: 2001, month: 1, day: 1},
        //     today: {year: 2001, month: 1, day: 1},
        //     // thisWeekOffset: 0,
        // },

        dates: [],


        borderColors: ['#2C7BE5', '#BAE5FF', '#97DCE3', '#f6c343', '#6e84a3'],
        charts: [
            {
                isLoading: true,

                col: 6,
                height: 250,
                cardName: '好友成長曲線',
                cardFooter: false,

                name: 'usersChart',
                id: 'users-chart',

                daysCount: 7,
                type: 'users',
                showTypesBtn: true,

                types: {
                    users: {
                        name: '總數',
                        borderColor: '#2c7be5',
                        labelString: '好友人數',
                    },
                    followers: {
                        name: '追蹤中',
                        borderColor: '#2c7be5',
                        labelString: '好友人數',
                    },
                    blocks: {
                        name: '封鎖',
                        borderColor: '#f6c343',
                        labelString: '好友人數',
                    }
                },
            },
            {
                isLoading: true,

                col: 6,
                height: 250,
                cardName: '活躍人數',
                cardFooter: false,

                name: 'activeUsersChart',
                id: 'active-users-chart',

                daysCount: 7,
                type: 'users',
                showTypesBtn: false,

                types: {
                    users: {
                        name: '總數',
                        borderColor: '#2c7be5',
                        labelString: '好友人數',
                    },
                },
            },
            {
                isLoading: true,

                col: 12,
                height: 250,
                cardName: '關鍵字成長曲線',
                cardFooter: true,

                name: 'keywordsChart',
                id: 'keywords-chart',
                urlPrefix: 'keyword',

                daysCount: 14,
                type: 'count',
                usersType: 'cumulative',
                showTypesBtn: true,

                types: {
                    count: {
                        name: '總數 Top5',
                        borderColor: '#2c7be5',
                        labelString: '好友人數',
                    },
                    rate: {
                        name: '成長率 Top5',
                        borderColor: '#2c7be5',
                        labelString: '成長百分比',
                    },
                },

                usersTypes: {
                    distinct: {
                        name: '好友人數',
                    },
                    cumulative: {
                        name: '累積好友人次',
                    },
                },

                data: [],
            },
            {
                isLoading: true,

                col: 12,
                height: 250,
                cardName: '訊息腳本成長曲線',
                cardFooter: true,

                name: 'scriptsChart',
                id: 'scripts-chart',
                urlPrefix: 'script',

                daysCount: 14,
                type: 'count',
                usersType: 'cumulative',
                showTypesBtn: true,

                types: {
                    count: {
                        name: '總數 Top5',
                        borderColor: '#2c7be5',
                        labelString: '好友人數',
                    },
                    rate: {
                        name: '成長率 Top5',
                        borderColor: '#2c7be5',
                        labelString: '成長百分比',
                    },
                },

                usersTypes: {
                    distinct: {
                        name: '好友人數',
                    },
                    cumulative: {
                        name: '累積好友人次',
                    },
                },

                data: [],
            },
        ],
    },
    methods: {
        getUrl(type, id){
            return route('solution.4.deployment.'+type+'s.show', {
                deployment: args.deployment,
                [type+'Id']: id,
            });
        },
        setShowAll(type, dateIndex){
            this.showAll[type] = true;
        },
        changeChartType(index, type){
            this.charts[index].type = type;
            this.updateChart(index);
        },
        changeChartUserType(index, type){
            this.charts[index].usersType = type;
            this.updateChart(index);
        },
        changeDatetimeType(index, type){
            this.charts[index].datetime.type = type;
        },
        isLeapYear(year){
            return (year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0));
        },
        getWeekDayIndex(year, month, day){
            var yearsCount = year - this.datetimeSetting.startData.year - 1,
            leapYearsCount = Math.floor(yearsCount/4) - Math.floor(yearsCount/100) + Math.floor(yearsCount/400);

            // 2001/01/01到前年底的總天數
            daysCount = yearsCount * 365 + leapYearsCount

            // 今年到上個月的總天數
            for(var i = 0; i < month - 1; i ++){
                daysCount += this.datetimeSetting.days[i];
            }
            // 閏年月份大於2者，+1天
            if(month > 2 && this.isLeapYear(year)) daysCount ++;

            // 加上本月天數
            daysCount += day;
            return daysCount%7;
        },
        getChartLabels(index){
            labels = [];
            d = new Date();

            labels.push((d.getMonth() + 1)+'/'+d.getDate());
            for(var i = 0; i < this.charts[index].daysCount; i ++){
                d.setDate(d.getDate() - 1);
                labels.unshift((d.getMonth() + 1)+'/'+d.getDate());
            }
            return labels;
        },
        getChartData(index){
            return new Promise((resolve, reject)=>{
                axios
                .get(route('solution.4.deployment.getChartData', {
                    deployment: args.deployment,
                    chartName: this.charts[index].name,
                    chartType: this.charts[index].type,
                    usersType: this.charts[index].usersType,
                }))
                .then(response => {
                    console.log(response.data)
                    resolve(response.data);
                }).catch(error => {
                    console.log(error);
                    reject([]);
                });
            });
        },
        async updateChart(index){
            this.charts[index].isLoading = true;
            let res = await this.getChartData(index);

            this[this.charts[index].name].data.labels = this.getChartLabels(index);

            var vm = this;

            if(this.charts[index].name == 'keywordsChart' || this.charts[index].name == 'scriptsChart'){
                this.charts[index].data = res;
                this[this.charts[index].name].data.datasets = [];

                this[this.charts[index].name].options.scales.yAxes[0].scaleLabel.labelString = this.charts[index].types[this.charts[index].type].labelString;

                this.charts[index].data.forEach(function(data, dataIndex){
                    vm[vm.charts[index].name].data.datasets.push({
                        data: data.data,
                        lineTension: 0,
                        borderColor: vm.borderColors[dataIndex],
                        pointBackgroundColor: vm.borderColors[dataIndex],
                    });
                });
            }else{
                this[this.charts[index].name].data.datasets[0].data = res;
                this[this.charts[index].name].data.datasets[0].borderColor = this.charts[index].types[this.charts[index].type].borderColor;
            }

            this[this.charts[index].name].update();
            this.charts[index].isLoading = false;
        },
        setAutoAnswers(){
            var vm = this;
            this.autoAnswers.forEach(function(autoAnswer, autoAnswerIndex){

                var started = 0,
                existsInThisWeek = 0,
                emptyDays = [];

                vm.dates.forEach(function(date, dateIndex){

                    // if(dateIndex == 0){
                    //     started = 0;
                    //     existsInThisWeek = 0;
                    // }
                    existsToday = 0;

                    autoAnswer.periods.every(function(period){
                        datetime = date.year+'-'+('0'+date.month).slice(-2)+'-'+('0'+date.day).slice(-2)+' 00:00:00';

                        if(dateIndex == 2){
                            console.log(autoAnswer);
                        }

                        if(period.start_date <= datetime && (!period.end_date || period.end_date >= datetime)){

                            date.autoAnswers.push({
                                index: autoAnswerIndex,
                                b: [started, dateIndex % 3 == 2 ? 0 : 1],
                            });

                            started = 1;
                            existsInThisWeek = 1;
                            existsToday = 1;
                            return false;
                        }
                        return true;
                    });

                    if(!existsToday){
                        emptyDays.push(dateIndex);
                        if(started){
                            vm.dates[dateIndex-1].autoAnswers[vm.dates[dateIndex-1].autoAnswers.length-1].b[1] = 0;
                            started = 0;
                        }
                    }

                    if(dateIndex % 3 == 2 && existsInThisWeek){
                        emptyDays.forEach(function(day){
                            vm.dates[day].autoAnswers.push({
                                index: -1,
                                b: [0, 0],
                            });
                        });
                        // emptyDays = [];
                    }

                });

            });
        }
    },
    created(){
        d = new Date();
        for(var i = 0; i < 3; i ++){
            date = d.getFullYear()+'-'+(d.getMonth() + 1)+'-'+d.getDate();

            this.dates.push({
                year: d.getFullYear(),
                month: d.getMonth() + 1,
                day: d.getDate(),
                broadcasts: this.broadcasts.filter(broadcast => broadcast.appointed_at >= date+' 00:00:00' && broadcast.appointed_at <= date+' 23:59:59'),
                autoAnswers: [],
            });

            d.setDate(d.getDate() + 1);
        }

        this.setAutoAnswers();

        var vm = this;
        this.charts.forEach(function(chart, index){
            // vm.charts[index].datetime.date.offset = vm.datetimeSetting.thisWeekOffset;
            // vm.updateDate(index);


            vm.$nextTick(async function(){
                chartData = {
                    type: 'line',
                    data: {
                        labels: vm.getChartLabels(index),
                        datasets: [{
                            data: [...Array(chart.daysCount+1).keys()].map(x => x * 10),
                            lineTension: 0,
                            borderColor: '#2c7be5',
                            pointBackgroundColor: chart.types[chart.type].borderColor,
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                gridLines: {
                                    display: true,
                                    lineWidth: 1,
                                    zeroLineWidth: 1,
                                },
                                scaleLabel: {
                                    display: true,
                                    labelString: chart.types[chart.type].labelString,
                                },
                                allowDecimals: false,
                                stacked: false,
                            }],
                            xAxes: [{
                                stacked: false,
                            }],
                        }
                    }
                };

                if(chart.name == 'cumulativeUsersCountPercentageChart'){
                    chartData.options.scales.yAxes[0].ticks = {
                        beginAtZero: true,
                        max: 100,
                        min: 0,
                        stepSize: 10
                    };
                }else{
                    chartData.options.scales.yAxes[0].ticks = {
                        beginAtZero: true,
                        suggestedMin: 10,
                    };
                }

                vm[chart.name] = new Chart(chart.id, chartData);

                // vm[chart.name].data.datasets[0].data = await vm.getChartData(index);
                vm.updateChart(index);
            });
        });
    },
    // watch: {
    //     'charts.0.type': function(){
    //         this.updateChart(0);
    //     },
    //     'charts.2.type': function(){
    //         this.updateChart(2);
    //     },
    //     'charts.3.type': function(){
    //         this.updateChart(3);
    //     }
    // }
});
