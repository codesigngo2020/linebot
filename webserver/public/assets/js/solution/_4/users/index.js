var usersTable = new Vue({
    el: '#users-table',
    data: {
        query: '',
        users: args.users,
        paginator: args.paginator,
        is_loading: false,
        order: {
            column: 'id',
            dir: 'desc',
        },
    },
    methods: {
        changePage(page){
            this.getUsers(page);
        },
        search(){
            this.getUsers(1);
        },
        sort(column){
            if(column != this.order.column){
                this.order.column = column;
                this.order.dir = 'asc';
            }else{
                this.order.dir = this.order.dir == 'asc' ? 'desc' : 'asc';
            }
            this.getUsers(1);
        },
        getUsers(page){
            this.is_loading = true;
            axios
            .get(route('solution.4.deployment.users.getUsers', {
                deployment: args.deployment,
                page: page,
                column: this.order.column,
                dir: this.order.dir,
                q: this.query,
            }))
            .then(response => {
                this.paginator = response.data.paginator;
                this.users = response.data.users;
                this.is_loading = false;
            }).catch(error => {
                this.is_loading = false;
                console.log(error);
            });
        },
        getUrl(id){
            return route('solution.4.deployment.users.show', {
                deployment: args.deployment,
                userId: id,
            });
        },
    },
});
