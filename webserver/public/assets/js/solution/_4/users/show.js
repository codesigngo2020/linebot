var userContent = new Vue({
    el: '#user-content',
    data: {
        user: args.user,

        groups: {
            holding: [],
            removed: [],
        },
        history: {
            isLoading: false,
        }
    },
    methods: {
        getGroupUrl(id){
            return route('solution.4.deployment.groups.show', {
                deployment: args.deployment,
                groupId: id,
            });
        },
        getTagUrl(id){
            return route('solution.4.deployment.tags.show', {
                deployment: args.deployment,
                tagId: id,
            });
        },
        classifyGroups(){
            var vm = this;
            this.user.groups.forEach(function(group){
                if(group.deleted_at){
                    vm.groups.removed.push(group);
                }else{
                    vm.groups.holding.push(group);
                }
            });
        },
        handleScroll(event){
            if(this.user.historyHasNext && !this.history.isLoading && event.target.scrollTop + event.target.clientHeight >= this.$refs.scrollInner.clientHeight - 15){
                this.history.isLoading = true;
                axios
                .get(route('solution.4.deployment.users.getHistory', {
                    deployment: args.deployment,
                    userId: this.user.id,
                    before: this.user.history[this.user.history.length-1].id,
                }))
                .then(response => {
                    this.user.historyHasNext = response.data.hasNext;
                    this.user.history = this.user.history.concat(response.data.history);
                    this.user.historyTags = Object.assign(this.user.historyTags, response.data.historyTags);
                    this.history.isLoading = false;
                }).catch(error => {
                    this.history.isLoading = false;
                    console.log(error);
                });
            }
        }
    },
    created(){
        this.classifyGroups();
    }
});
