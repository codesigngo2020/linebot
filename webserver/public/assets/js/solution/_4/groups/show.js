var groupContent = new Vue({
    el: '#group-content',
    data: {
        group: args.group,

        sync: {
            is_loading: false,
        },

        users: {
            query: '',
            is_loading: false,
            order: {
                column: 'id',
                dir: 'desc',
            },
        }
    },
    methods: {
        syncGroup(){
            if(!this.sync.is_loading){
                this.sync.is_loading = true;
                axios
                .post(route('solution.4.deployment.groups.sync', {
                    deployment: args.deployment,
                    groupId: this.group.id,
                }), {
                    _token: args._token,
                })
                .then(response => {
                    if(response.data.status == 'success'){
                        window.open(window.location.href, '_self');
                    };
                    this.sync.is_loading = false;
                }).catch(error => {
                    this.sync.is_loading = false;
                    console.log(error);
                });
            }
        },
        getUserUrl(id){
            return route('solution.4.deployment.users.show', {
                deployment: args.deployment,
                userId: id,
            });
        },
        changePage(page){
            this.getUsers(page);
        },
        searchUsers(){
            this.getUsers(1);
        },
        getUsers(page){
            this.users.is_loading = true;
            axios
            .get(route('solution.4.deployment.groups.getGroupUsers', {
                deployment: args.deployment,
                groupId: this.group.id,
                page: page,
                column: this.users.order.column,
                dir: this.users.order.dir,
                q: this.users.query,
            }))
            .then(response => {
                this.group.paginator = response.data.paginator;
                this.group.users = response.data.users;
                this.users.is_loading = false;
            }).catch(error => {
                this.users.is_loading = false;
                console.log(error);
            });
        },
    }
});
