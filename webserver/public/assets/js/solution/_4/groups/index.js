var newGroup = new Vue({
    el: '#new-group',
});

var groupsTable = new Vue({
    el: '#groups-table',
    data: {
        query: '',
        groups: [],
        paginator: {
            currentPage: 1,
            lastPage: 1,
        },
        is_loading: false,
        order: {
            column: 'id',
            dir: 'desc',
        },
    },
    methods: {
        changePage(page){
            this.getGroups(page);
        },
        search(){
            this.getGroups(1);
        },
        getGroups(page){
            this.is_loading = true;
            axios
            .get(route('solution.4.deployment.groups.getGroups', {
                deployment: args.deployment,
                page: page,
                column: this.order.column,
                dir: this.order.dir,
                q: this.query,
            }))
            .then(response => {
                this.paginator = response.data.paginator;
                this.groups = response.data.groups;
                this.is_loading = false;
            }).catch(error => {
                this.is_loading = false;
                console.log(error);
            });
        },
        getUrl(id){
            return route('solution.4.deployment.groups.show', {
                deployment: args.deployment,
                groupId: id,
            });
        },
    },
    created(){
        this.getGroups(1);
    }
});
