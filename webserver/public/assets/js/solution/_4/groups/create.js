disablePageSwitch();

var groupForm = new Vue({
    el: '#group-form',
    data: {
        submitting: false,

        draggingTag: {
            id: 0,
            name: '',
        },

        addUsers: {
            isLoading: false,
            options: [],
        },
        removeUsers: {
            isLoading: false,
            options: [],
        },

        form: {
            name: {
                name: '',
                info: '',
                invalid: false,
            },
            description: {
                description: '',
                info: '',
                invalid: false,
            },
            circles: {
                circles: [
                    // {
                    //     id: 1,
                    //     circlesCount: 3,
                    //     selectedAreas: [],
                    // }
                ],
                info: '',
                invalid: false,
            },

            addUsers: {
                value: null,
                info: '',
                invalid: false,
            },
            removeUsers: {
                value: null,
                info: '',
                invalid: false,
            },
        }
    },
    methods: {
        updateDraggingTag(id, name){
            this.draggingTag.id = id;
            this.draggingTag.name = name;
        },
        limitText(user){
            return `and ${user} other users`;
        },
        getAddUsers(query){
            this.getUsers('add', query);
        },
        getRemoveUsers(query){
            this.getUsers('remove', query);
        },
        getUsers(type, q){
            this[type+'Users'].isLoading = true;
            axios
            .get(route('solution.4.deployment.groups.getUsers', {
                deployment: args.deployment,
                q: q,
            }))
            .then(response => {
                this[type+'Users'].options = response.data;
                this[type+'Users'].isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this[type+'Users'].options = [];
                this[type+'Users'].isLoading = false;
                console.log(error);
            });
        },
        AllInvalidToFalse(obj){
            for(key in obj){
                if(key == 'invalid'){
                    obj[key] = false;
                    obj['info'] = '';
                }else if(typeof(obj[key]) === 'object'){
                    this.AllInvalidToFalse(obj[key]);
                }
            }
        },
        buildFormData(formData, data, parentKey) {
            if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                Object.keys(data).forEach(key => {
                    groupForm.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                });
            } else {
                const value = data == null ? '' : data;
                formData.append(parentKey, value);
            }
        },
        jsonToFormData(data){
            let formData = new FormData();
            this.buildFormData(formData, data, 'form');
            return formData;
        },
        submit(){

            if(!this.submitting){
                this.submitting = true;
                this.AllInvalidToFalse(this.form);

                formData = this.jsonToFormData(this.form);
                formData.append('_token', args._token);

                axios.post(route('solution.4.deployment.groups.store', args.deployment),
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    console.log(response);
                    if(response.data.status == 'success'){
                        window.open(route('solution.4.deployment.groups.show', [args.deployment, response.data.groupId]), '_self');
                    }else{
                        this.submitting = false;
                    }
                })
                .catch(error => {
                    var errors = error.response.data.errors;
                    this.submitting = false;
                    var invalid_crawler = null;
                    for(error in errors){
                        console.log(error);

                        // 找出最後一個「.」後的字串
                        var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                        // 如果是value，表示是select，再往前找一個value
                        if(last == 'value'){
                            select_last = error.match(/\.(value\.value)$/);
                            // 如果符合「.value.value」，表示是多層value
                            if(select_last) last = select_last.slice(-1)[0];
                        }

                        // 替換數字成以[]包覆表示
                        error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                        // 如果是area，更新invalid = true
                        if(area = error_ref.match(/^(.*)\.action/)){
                            eval('this.'+area.slice(-1)[0]).invalid = true;
                        }

                        info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        try{
                            this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                            this.$set(eval('this.'+invalid_ref), 'invalid', true);
                        }catch(e){

                        }
                    }
                });
            }
        }
    },
});
