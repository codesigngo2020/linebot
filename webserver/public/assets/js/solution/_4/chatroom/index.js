var chatroom = new Vue({
    el: '#chatroom',
    data: {
        listType: 'dialogs',
        user: {
            id: 0,
            display_name: '',
            is_follow: false,
            log_id: 0,
            message_index: 0,
            picture_url: '',
            reply_token: '',
            status_message: '',
        },

        websocketDomain: args.websocketDomain,
        websocketStatus: '',

        ws: null,
        wsResponse: {
            getUsers: null,
            getDialogs: null,
            getLogs: null,
            webhook: null,
        }
    },
    methods: {
        changeListType(type){
            this.listType = type;
        },
        updateUser(user){
            this.user = user;
        },
        initWebsocket(){
            if(!this.ws){
                var vm = this;
                // this.ws = new WebSocket("ws://127.0.0.1:8080/solution/4/deployment/1/websocket");
                this.ws = new WebSocket('wss://'+this.websocketDomain+'/solution/4/deployment/'+args.deployment+'/websocket');

                this.ws.onopen = function(event){
                    console.log("websocket open");
                }

                this.ws.onclose = function(event){
                    console.log("websocket close");
                    vm.websocketStatus = '連線中斷，請刷新頁面重新連線',
                    vm.ws = null;
                }

                this.ws.onmessage = function(event){
                    res = JSON.parse(event.data);
                    console.log(res)
                    if(res.type == 'getResultDialogs'){
                        vm.wsResponse['getDialogs'] = res;
                    }else{
                        vm.wsResponse[res.type] = res;
                    }

                    if(res.type == 'webhook' && res.reply_token && vm.user.id == res.user.id) vm.user.reply_token = res.reply_token;
                }

                this.ws.onerror = function(event){
                    console.log("ERROR: " + event.data);
                }
            }
        },
        sendToWebsocket(message){
            if(!this.ws) return false;

            console.log("SEND: " + message);
            this.ws.send(message);
            return false;
        }
    },
    created(){
        this.initWebsocket();
    }
});
