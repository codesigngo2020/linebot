disablePageSwitch();

var messageForm = new Vue({
    el: '#message-form',
    data: {
        init: true,

        submitting: false,

        newMessageType: null,

        hasQuickReply: false,

        templateType: {
            options: [
                {
                    name: 'Coupon template',
                    value: 'coupon',
                },
            ],
        },

        form: {
            name: {
                name: args.message.name,
                info: '',
                invalid: false,
            },
            description: {
                description: args.message.description,
                info: '',
                invalid: false,
            },
            type: {
                type: args.message.type,
                info: '',
                invalid: false,
            },
            templateType: {
                value: null,
                info: '',
                invalid: false,
            },
            templateSettings: null,
            messages: {
                messages: args.message.messages_form_data ? args.message.messages_form_data : [
                    {
                        id: 1,
                        type: 'text',
                        message: {
                            text: {
                                text: '',
                                info: '',
                                invalid: false,
                            }
                        }
                    },
                ],
                info: '',
                invalid: false,
            },
            template: {
                template: {
                    nodes: {
                        nodes: args.message.nodes_form_data ? args.message.nodes_form_data : [],
                    }
                }
            }
        }
    },
    methods: {
        // updateMessagesData(data){
        //     this.$set(this.form.messages, 'messages', data);
        // },
        updateHasQuickReply(hasQuickReply){
            this.hasQuickReply = hasQuickReply;
        },
        updateNewMessageType(type){
            this.newMessageType = type;
        },
        AllInvalidToFalse(obj){
            for(key in obj){
                if(key == 'invalid'){
                    obj[key] = false;
                    obj['info'] = '';
                }else if(typeof(obj[key]) === 'object'){
                    this.AllInvalidToFalse(obj[key]);
                }
            }
        },
        buildFormData(formData, data, parentKey) {
            if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                Object.keys(data).forEach(key => {
                    messageForm.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                });
            } else {
                const value = data == null ? '' : data;
                formData.append(parentKey, value);
            }
        },
        jsonToFormData(data){
            let formData = new FormData();
            this.buildFormData(formData, data, 'form');
            return formData;
        },
        submit(){
            if(!this.submitting){
                this.submitting = true;
                this.AllInvalidToFalse(this.form);

                formData = this.jsonToFormData(this.form);
                formData.append('_token', args._token);

                axios.post(route('solution.4.deployment.messagesPool.new'+this.form.type.type.capitalize()+'Version', [args.deployment, args.message.id]),
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    console.log(response);
                    if(response.data.status == 'success'){
                        window.open(route('solution.4.deployment.messagesPool.show', [args.deployment, response.data.messageId]), '_self');
                    }else{
                        this.submitting = false;
                    }
                })
                .catch(error => {
                    var errors = error.response.data.errors;
                    this.submitting = false;
                    var invalid_crawler = null;
                    for(error in errors){
                        console.log(error);

                        // 找出最後一個「.」後的字串
                        var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                        // 如果是value，表示是select，再往前找一個value
                        if(last == 'value'){
                            select_last = error.match(/\.(value\.value)$/);
                            // 如果符合「.value.value」，表示是多層value
                            if(select_last) last = select_last.slice(-1)[0];
                        }

                        // 替換數字成以[]包覆表示
                        error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                        // 如果是area，更新invalid = true
                        if(area = error_ref.match(/^(.*)\.action/)){
                            eval('this.'+area.slice(-1)[0]).invalid = true;
                        }

                        info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');

                        var action_prefix = invalid_ref.match(/(.*actions\[\d\])/);
                        if(action_prefix) this.$set(eval('this.'+action_prefix[0]), 'invalid', true);

                        try{
                            this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                            this.$set(eval('this.'+invalid_ref), 'invalid', true);
                        }catch(e){

                        }
                    }
                });
            }
        }
    },
    created(){
        if(args.message.type == 'template'){
            option = this.templateType.options.find(option => option.value == args.message.template_type);
            this.form.templateType.value = option;

            if(args.message.template_type == 'coupon'){
                this.form.templateSettings = {
                    table: {
                        value: args.message.table,
                        info: '',
                        invalid: false,
                    }
                }
            }
        }

        var vm = this;
        this.$nextTick(() => {
            vm.init = false;
        });
    },
    watch: {
        'form.type.type': function(newValue){
            if(newValue == 'message'){
                this.form.templateType.value = null;
                this.form.templateSettings = null;
            }
        },
        'form.templateType.value': function(newValue){
            if(!this.init && newValue){
                if(newValue.value == 'coupon'){
                    this.form.templateSettings = {
                        table: {
                            value: null,
                            info: '',
                            invalid: false,
                        }
                    }
                }
            }
        },
        'form.templateSettings': {
            handler(newValue){
                if(this.form.templateType.value && this.form.templateType.value.value == 'coupon' && newValue.table.value){
                    // 沒有領取時間限制
                    node = this.form.template.template.nodes.nodes.find(function(node){
                        return node.condition.value == 'invalid-time';
                    });
                    node.disabled = !newValue.table.value.settings.issueDatetime.active;

                    // 沒有數量限制
                    node = this.form.template.template.nodes.nodes.find(function(node){
                        return node.condition.value == 'reach-limit';
                    });
                    node.disabled = !newValue.table.value.settings.limit.active;
                }
            },
            deep: true,
        }
    }
});
