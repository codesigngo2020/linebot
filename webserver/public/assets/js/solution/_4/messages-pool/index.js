var newMessage = new Vue({
    el: '#new-message',
});

var messagesTable = new Vue({
    el: '#messages-table',
    data: {
        isCategoryLoading: false,
        category: 0,

        draggable: null,

        query: '',
        messages: [],
        paginator: {
            currentPage: 1,
            lastPage: 1,
        },
        is_loading: false,
        order: {
            column: 'id',
            dir: 'desc',
        },
    },
    methods: {
        changePage(page){
            this.getMessages(page);
        },
        search(){
            this.getMessages(1);
        },
        refresh(){
            this.getMessages(this.paginator.currentPage);
        },
        getMessages(page){
            this.is_loading = true;
            axios
            .get(route('solution.4.deployment.messagesPool.getMessages', {
                deployment: args.deployment,
                page: page,
                category: this.category,
                column: this.order.column,
                dir: this.order.dir,
                q: this.query,
            }))
            .then(response => {
                this.paginator = response.data.paginator;
                this.messages = response.data.messages;
                this.is_loading = false;
            }).catch(error => {
                this.is_loading = false;
                console.log(error);
            });
        },
        getUrl(id){
            return route('solution.4.deployment.messagesPool.show', {
                deployment: args.deployment,
                messageId: id,
            });
        },

        // 訊息設定
        deleteAll(id){
            this.is_loading = true;
            axios({
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': args._token,
                },
                url: route('solution.4.deployment.messagesPool.deleteAll', {
                    deployment: args.deployment,
                    messageId: id,
                }),
            })
            .then(response => {
                if(response.data.success){
                    this.getMessages(this.paginator.currentPage);
                }
            })
            .catch(error => {
                this.is_loading = false;
            });
        },

        // category
        setIsCategoryLoading(isLoading){
            this.isCategoryLoading = isLoading;
        },
        changeCategory(id){
            this.category = id;
            this.getMessages(1);
        },
        mouseenter(keyword){
            this.draggable = keyword;
        },
        mouseleave(){
            this.draggable = null;
        },
    },
    created(){
        this.getMessages(1);
    }
});
