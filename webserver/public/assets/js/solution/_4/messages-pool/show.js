var messageVersion = new Vue({
    el: '#message-version',
    data: {
        version: {
            value: {
                name: 'v'+args.message.version,
                value: args.message.id,
                active: args.message.is_current_version,
            },
            options: args.message.versions,
        }
    },
    watch: {
        'version.value.value': function(newValue){
            window.open(route('solution.4.deployment.messagesPool.show', [args.deployment, newValue]), '_self');
        }
    }
});

var messageContent = new Vue({
    el: '#message-content',
    data: {
        message: args.message,
        tagId: args.tagId,
        nodeId: args.nodeId,

        expandMessage: args.expandMessage,
        flowType: 'users',

        showNodeDetailId: 1,

        display: 'message',
    },
    methods: {
        switchVersion(id){
            axios
            .post(route('solution.4.deployment.messagesPool.switchVersion', {
                deployment: args.deployment,
                messageId: id,
            }), {
                _token: args._token,
            })
            .then(response => {
                if(response.data.success) location.reload();
            })
            .catch(error => {

            });
        },

        // 訊息模組
        toggleExpandMessage(isExpand){
            this.expandMessage = isExpand;
        },
        toggleFlowType(type){
            this.flowType = type;
        },
        changeDisplay(display){
            this.display = display;
        },
        showNodeDetail(nodeId){
            this.showNodeDetailId = nodeId;
        },
        getNodeMessages(){
            nodeFormData = this.message.nodes_form_data.find(node => node.id == this.showNodeDetailId);
            return this.message.nodes.find(node => node.start == nodeFormData.start && node.end == nodeFormData.end).messages;
        },
        getNodeMessagesFormData(){
            return this.message.nodes_form_data.find(node => node.id == this.showNodeDetailId).messages.messages;
        },
        getTagIdByNodeId(id){

        },
        getTagIdByNodeFromDataId(id){
            return this.message.nodes_form_data.find(node => node.id == this.showNodeDetailId).tag;
        },
        getTagUrl(id){
            return route('solution.4.deployment.tags.show', {
                deployment: args.deployment,
                tagId: id,
            });
        },
    },
    created(){
        if(this.message.type == 'template'){
            if(this.nodeId){
                node = this.message.nodes.find(node => node.id == this.nodeId);
                nodeFormData = this.message.nodes_form_data.find(_node => _node.start == node.start && _node.end == node.end);
                this.showNodeDetailId = nodeFormData.id;
            }else{
                this.showNodeDetailId = this.message.nodes_form_data.find(node => !node.disabled).id;
            }
        }
    }
});
