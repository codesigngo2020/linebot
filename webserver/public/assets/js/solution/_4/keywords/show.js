var keywordVersion = new Vue({
    el: '#keyword-version',
    data: {
        version: {
            value: {
                name: 'v'+args.keyword.version,
                value: args.keyword.id,
                active: args.keyword.is_current_version,
            },
            options: args.keyword.versions,
        }
    },
    watch: {
        'version.value.value': function(newValue){
            window.open(route('solution.4.deployment.keywords.show', [args.deployment, newValue]), '_self');
        }
    }
});

var keywordContent = new Vue({
    el: '#keyword-content',
    data: {
        keyword: args.keyword,
        tagId: args.tagId,

        triggerTypes: {
            reply: '回覆訊息',
            script: '開啟腳本',
            richmenu: '切換選單',
        },
    },
    methods: {
        getTagUrl(id){
            return route('solution.4.deployment.tags.show', {
                deployment: args.deployment,
                tagId: id,
            });
        },
        getRichmenuMessage(){
            if(this.keyword.richmenu.deleted_all_at){
                return '選單已刪除';
            }else if(!this.keyword.richmenu.is_current_version){
                return '未指定選單版本';
            }else{
                return '前往選單內容';
            }
        },

        // 關鍵字設定
        toggleActive(id){
            this.is_loading = true;

            axios
            .post(route('solution.4.deployment.keywords.toggleActive', {
                deployment: args.deployment,
                keywordId: id,
            }), {
                _token: args._token,
            })
            .then(response => {
                if(response.data.success){
                    location.reload();
                }else{
                    this.is_loading = false;
                }
            })
            .catch(error => {
                this.is_loading = false;
            });
        },
        switchVersion(id){
            axios
            .post(route('solution.4.deployment.keywords.switchVersion', {
                deployment: args.deployment,
                keywordId: id,
            }), {
                _token: args._token,
            })
            .then(response => {
                if(response.data.success) location.reload();
            })
            .catch(error => {

            });
        },
    },
    mounted(){
        this.$refs.QRcode.style.width = this.$refs.cardBody.offsetHeight+'px';
    }
});
