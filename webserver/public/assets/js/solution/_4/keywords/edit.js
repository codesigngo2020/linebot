disablePageSwitch();

var keywordForm = new Vue({
    el: '#keyword-form',
    data: {
        isInitial: true,
        submitting: false,

        form: {
            name: {
                name: args.keyword.name,
                info: '',
                invalid: false,
            },
            description: {
                description: args.keyword.description,
                info: '',
                invalid: false,
            },
            active: {
                active: args.keyword.active,
                info: '',
                invalid: false,
            },
            triggerType: {
                triggerType: args.keyword.trigger_type,
                info: '',
                invalid: false,
            },
            trigger: {
                trigger: {},
            },
        }
    },
    methods: {
        updateTrigger(data){
            this.form.trigger.trigger = data;
            this.isInitial = false;
        },
        AllInvalidToFalse(obj){
            for(key in obj){
                if(key == 'invalid'){
                    obj[key] = false;
                    obj['info'] = '';
                }else if(typeof(obj[key]) === 'object'){
                    this.AllInvalidToFalse(obj[key]);
                }
            }
        },
        buildFormData(formData, data, parentKey) {
            if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                Object.keys(data).forEach(key => {
                    keywordForm.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                });
            } else {
                const value = data == null ? '' : data;
                formData.append(parentKey, value);
            }
        },
        jsonToFormData(data){
            let formData = new FormData();
            this.buildFormData(formData, data, 'form');
            return formData;
        },
        submit(){
            if(!this.submitting){
                this.submitting = true;
                this.AllInvalidToFalse(this.form);

                formData = this.jsonToFormData(this.form);
                formData.append('_token', args._token);

                axios.post(route('solution.4.deployment.keywords.newVersion', [args.deployment, args.keyword.id]),
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    console.log(response);
                    if(response.data.status == 'success'){
                        window.open(route('solution.4.deployment.keywords.show', [args.deployment, response.data.keywordId]), '_self');
                    }else{
                        this.submitting = false;
                    }
                })
                .catch(error => {
                    var errors = error.response.data.errors;
                    this.submitting = false;
                    for(error in errors){
                        console.log(error);

                        // 找出最後一個「.」後的字串
                        var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                        // 如果是value，表示是select，再往前找一個value
                        if(last == 'value'){
                            select_last = error.match(/\.(value\.value)$/);
                            // 如果符合「.value.value」，表示是多層value
                            if(select_last) last = select_last.slice(-1)[0];
                        }

                        // 替換數字成以[]包覆表示
                        error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                        // 如果是area，更新invalid = true
                        if(area = error_ref.match(/^(.*)\.action/)){
                            eval('this.'+area.slice(-1)[0]).invalid = true;
                        }

                        info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');

                        var action_prefix = invalid_ref.match(/(.*actions\[\d\])/);
                        if(action_prefix) this.$set(eval('this.'+action_prefix[0]), 'invalid', true);

                        try{
                            this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                            this.$set(eval('this.'+invalid_ref), 'invalid', true);
                        }catch(e){

                        }
                    }
                });
            }
        }
    },
    created(){
        if(args.keyword.trigger_type == 'reply'){
            this.form.trigger.trigger = {
                messageType: {
                    messageType: args.keyword.template_id ? 'template' : 'new',
                    info: '',
                    invalid: false,
                },
                message: {
                    value: args.keyword.template ? {
                        id: args.keyword.template.id,
                        value: args.keyword.template.parent_id,
                        name: args.keyword.template.name,
                    } : null,
                    info: '',
                    invalid: false,
                },
                messages: {
                    messages: args.keyword.messages_form_data ? args.keyword.messages_form_data : [
                        {
                            id: 1,
                            type: 'text',
                            message: {
                                text: {
                                    text: '',
                                    info: '',
                                    invalid: false,
                                }
                            }
                        },
                    ],
                    info: '',
                    invalid: false,
                }
            }
        }else if(args.keyword.trigger_type == 'richmenu'){
            this.form.trigger.trigger = {
                richmenu: {
                    value: {
                        value: args.keyword.richmenu_id,
                        name: 'xxx',
                    },
                    info: '',
                    invalid: false,
                },
            }
        }else if(args.keyword.trigger_type == 'script'){
            this.form.trigger = {
                trigger:{
                    script: {
                        value: {
                            value: args.keyword.script_id,
                            name: 'xxx',
                        },
                        info: '',
                        invalid: false,
                    },
                }
            }
        }
    }
});
