var newWelcomeMessage = new Vue({
    el: '#new-welcome-message',

});

var welcomeMessagesTable = new Vue({
    el: '#welcome-messages-table',
    data: {
        type: 'calendar',

        currentWelcomeMessage: args.currentWelcomeMessage,
    },
    methods: {
        changeType(type){
            this.type = type;
        },
        getUrl(id){
            return route('solution.4.deployment.welcomeMessages.show', {
                deployment: args.deployment,
                welcomeMessageId: id,
            });
        },
    }
});
