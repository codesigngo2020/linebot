disablePageSwitch();

var welcomeMessageForm = new Vue({
    el: '#welcome-message-form',
    data: {
        submitting: false,

        currentPeriodId: 1,

        message: {
            isLoading: false,
            options: [],
        },
        newMessages: {
            isLoading: false,
            hasQuickReply: false,
            newMessageType: null,
        },

        form: {
            name: {
                name: '',
                info: '',
                invalid: false,
            },
            description: {
                description: '',
                info: '',
                invalid: false,
            },
            active: {
                active: 1,
                info: '',
                invalid: false,
            },
            periods: {
                periods: [
                    {
                        id: 1,
                        hasEnd: {
                            hasEnd: true,
                            info: '',
                            invalid: false,
                        },
                        date: {
                            start: {
                                start: '',
                                info: '',
                                invalid: false,
                            },
                            end: {
                                end: '',
                                info: '',
                                invalid: false,
                            },
                        },
                        time: {
                            start: {
                                start: '',
                                info: '',
                                invalid: false,
                            },
                            end: {
                                end: '',
                                info: '',
                                invalid: false,
                            },
                        }
                    }
                ],
                info: '',
                invalid: false,
            },
            messageType: {
                messageType: 'new',
                info: '',
                invalid: false,
            },
            message: {
                value: null,
                info: '',
                invalid: false,
            },
            messages: {
                messages: [
                    {
                        id: 1,
                        type: 'text',
                        message: {
                            text: {
                                text: '',
                                info: '',
                                invalid: false,
                            }
                        }
                    },
                ],
                info: '',
                invalid: false,
            }
        }
    },
    methods: {
        activateInputMask(periodId){
            var vm = this;
            ['date', 'time'].forEach(function(dt){
                ['start', 'end'].forEach(function(se){
                    $(vm.$refs[dt+'-'+se+'-'+periodId][0]).mask(vm.$refs[dt+'-'+se+'-'+periodId][0].getAttribute('data-mask'), {
                        translation: {
                            'A': {
                                pattern: /[2]/
                            },
                            'B': {
                                pattern: /[0]/
                            },
                            'C': {
                                pattern: /[0-9]/
                            },
                            'D': {
                                pattern: /[0-2]/
                            },
                            'E': {
                                pattern: /[0-3]/
                            },
                            'F': {
                                pattern: /[0-2]/
                            },
                            'G': {
                                pattern: /[0-5]/
                            }
                        },
                        onChange(cep){
                            period = vm.form.periods.periods.find(period => period.id == periodId);
                            period[dt][se][se] = cep;
                        },
                    });
                });
            });
        },
        newPeriod(){
            this.currentPeriodId ++;
            this.form.periods.periods.push({
                id: this.currentPeriodId,
                hasEnd: {
                    hasEnd: true,
                    info: '',
                    invalid: false,
                },
                date: {
                    start: {
                        start: '',
                        info: '',
                        invalid: false,
                    },
                    end: {
                        end: '',
                        info: '',
                        invalid: false,
                    },
                },
                time: {
                    start: {
                        start: '',
                        info: '',
                        invalid: false,
                    },
                    end: {
                        end: '',
                        info: '',
                        invalid: false,
                    },
                }
            });

            var vm = this;
            this.$nextTick(function(){
                vm.activateInputMask(vm.currentPeriodId);
            });
        },
        removePeriod(periodIndex){
            if(this.form.periods.periods.length > 1) this.form.periods.periods.splice(periodIndex, 1);
        },

        updateIsLoading(isLoading){
            this.newMessages.isLoading = isLoading;
        },
        updateMessagesData(data){
            this.form.messages.messages = data;
        },
        updateHasQuickReply(hasQuickReply){
            this.newMessages.hasQuickReply = hasQuickReply;
        },
        updateNewMessageType(type){
            this.newMessages.newMessageType = type;
        },

        messageslimitText(message){
            return `and ${message} other messages`;
        },
        getMessages(query){
            console.log(query);
            this.message.isLoading = true;
            axios
            .get(route('solution.4.deployment.messagesPool.queryForSelect', {
                deployment: args.deployment,
                type: this.form.messageType.messageType == 'pool' ? 'message' : 'template',
                q: query,
            }))
            .then(response => {
                this.message.options = response.data;
                this.message.isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this.message.options = [];
                this.message.isLoading = false;
                console.log(error);
            });
        },
        getMessageUrl(id){
            return route('solution.4.deployment.messagesPool.show', {
                deployment: args.deployment,
                messageId: id,
            });
        },

        AllInvalidToFalse(obj){
            for(key in obj){
                if(key == 'invalid'){
                    obj[key] = false;
                    obj['info'] = '';
                }else if(typeof(obj[key]) === 'object'){
                    this.AllInvalidToFalse(obj[key]);
                }
            }
        },
        buildFormData(formData, data, parentKey) {
            if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                Object.keys(data).forEach(key => {
                    welcomeMessageForm.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                });
            } else {
                const value = data == null ? '' : data;
                formData.append(parentKey, value);
            }
        },
        jsonToFormData(data){
            let formData = new FormData();
            this.buildFormData(formData, data, 'form');
            return formData;
        },
        submit(){
            if(!this.submitting){
                this.submitting = true;
                this.AllInvalidToFalse(this.form);

                formData = this.jsonToFormData(this.form);
                formData.append('_token', args._token);

                axios.post(route('solution.4.deployment.welcomeMessages.store', args.deployment),
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    console.log(response);
                    if(response.data.status == 'success'){
                        window.open(route('solution.4.deployment.welcomeMessages.show', [args.deployment, response.data.welcomeMessageId]), '_self');
                    }else{
                        this.submitting = false;
                    }
                })
                .catch(error => {
                    var errors = error.response.data.errors;
                    this.submitting = false;
                    var invalid_crawler = null;
                    for(error in errors){
                        console.log(error);

                        // 找出最後一個「.」後的字串
                        var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                        // 如果是value，表示是select，再往前找一個value
                        if(last == 'value'){
                            select_last = error.match(/\.(value\.value)$/);
                            // 如果符合「.value.value」，表示是多層value
                            if(select_last) last = select_last.slice(-1)[0];
                        }

                        // 替換數字成以[]包覆表示
                        error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                        // 如果是area，更新invalid = true
                        if(area = error_ref.match(/^(.*)\.action/)){
                            eval('this.'+area.slice(-1)[0]).invalid = true;
                        }

                        info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');

                        var action_prefix = invalid_ref.match(/(.*actions\[\d\])/);
                        if(action_prefix) this.$set(eval('this.'+action_prefix[0]), 'invalid', true);

                        try{
                            this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                            this.$set(eval('this.'+invalid_ref), 'invalid', true);
                        }catch(e){

                        }
                    }
                });
            }
        }
    },
    mounted(){
        this.activateInputMask(1);
    },
    watch: {
        'form.messageType.messageType': function(newValue){
            this.message.options = [];
            this.form.message.value = null;
            if(newValue == 'new'){
                this.form.message.value = null;
            }else{
                this.getMessages('');
            }
            this.form.message.info = '';
            this.form.message.invalid = false;
        },
        'form.message.value': function(newValue){
            if(newValue && this.form.messageType.messageType == 'pool'){
                this.newMessages.isLoading = true;
                axios
                .get(route('solution.4.deployment.messagesPool.getMessage', {
                    deployment: args.deployment,
                    messageId: newValue.value,
                }))
                .then(response => {
                    console.log(response)
                    this.form.messages.messages = response['data'];

                    this.hasQuickReply = this.form.messages.messages[this.form.messages.messages.length - 1].type == 'quickreply';

                    var vm = this;
                    this.$nextTick(function(){
                        vm.newMessages.isLoading = false;
                    })
                }).catch(error => {
                    this.newMessages.isLoading = false;
                    console.log(error);
                });
            }
        },
    }
});
