Date.prototype.addDays = function(days) {
    this.setDate(this.getDate() + days);
    return this;
}

var autoAnswerContent = new Vue({
    el: '#auto-answer-content',
    data: {
        autoAnswer: args.autoAnswer,
        tagId: args.tagId,

        periodsChart: {
            labels: [],
            backgrounds: ['primary', 'secondary', 'info', 'warning', 'danger'],
        },

        types: {
            1: {
                name: '文字訊息',
                active: false,
            },
            2: {
                name: '圖片訊息',
                active: false,
            },
            3: {
                name: '影片訊息',
                active: false,
            },
        },
    },
    methods: {
        getTagUrl(id){
            return route('solution.4.deployment.tags.show', {
                deployment: args.deployment,
                tagId: id,
            });
        },
        setPeriodsChartLabels(){
            this.periodsChart.minDate = this.autoAnswer.periods.reduce(function(min, period){
                if(!min || period.start_date < min) return period.start_date;
                return min;
            }, null);

            this.periodsChart.maxDate = this.autoAnswer.periods.reduce(function(max, period){
                if(!max || period.end_date > max) return period.end_date;
                return max;
            }, null);

            // 如果結束日期都是null
            if(!this.periodsChart.maxDate){
                this.periodsChart.maxDate = new Date(this.periodsChart.minDate);
                this.periodsChart.maxDate.addDays(24);
            }else{
                this.periodsChart.maxDate = new Date(this.periodsChart.maxDate);
            }

            this.periodsChart.minDate = new Date(this.periodsChart.minDate.substr(0, 7)+'-01');
            diffDays = Math.ceil(Math.abs(this.periodsChart.maxDate - this.periodsChart.minDate) / (1000 * 60 * 60 * 24));

            if(diffDays < 29){ // 一個月以內

                if(this.periodsChart.minDate.getMonth() == this.periodsChart.maxDate.getMonth()){
                    this.pushPeriodsChartLabels('d-minDate', 'month', 1, 3);
                }else{
                    this.pushPeriodsChartLabels('d-minDate', 'month', 1, 4);
                }

            }else if(diffDays < 180){ // 半年內

                this.pushPeriodsChartLabels('d-minDate', 'month', 1, 3);

            }else if(diffDays < 365){ // 一年內

                if(this.periodsChart.minDate.getFullYear() == this.periodsChart.maxDate.getFullYear()){
                    this.pushPeriodsChartLabels('d-minDate', 'month', 3, 13);
                }else{
                    this.pushPeriodsChartLabels('d-minDate', 'year', 1, 4);
                }

            }else{

                diffYear = this.periodsChart.maxDate.getFullYear() - this.periodsChart.minDate.getFullYear() + 2;
                if(diffYear % 4 != 0) diffYear += 4 - (diffYear % 4);
                interval = diffYear / 4;

                this.pushPeriodsChartLabels('d-minDateYear', 'year', interval, diffYear);

            }
        },
        pushPeriodsChartLabels(dType, type, interval, condition){

            if(dType == 'd-minDate'){
                var d = new Date(this.periodsChart.minDate);
            }else{
                var d = new Date(this.periodsChart.minDate.getFullYear()+'-01-01');
            }

            for(var i = 0; i < condition; i ++){
                if(i % interval == 0) this.periodsChart.labels.push(d.getFullYear()+(d.getMonth() < 9 ? '-0' : '-')+(d.getMonth()+1));

                if(i == condition - 1){
                    this.periodsChart.diffDays = (d - this.periodsChart.minDate) / (1000 * 60 * 60 * 24) - 1;
                }

                if(type == 'year'){
                    d.setYear( d.getFullYear() + 1 );
                }else{
                    d.setMonth( d.getMonth() + 1 );
                }
            }
        },
        setPeriodsData(){
            var vm = this;

            this.autoAnswer.periods.forEach(function(period, periodIndex){
                startDate = new Date(period.start_date);
                offset = (startDate - vm.periodsChart.minDate) / (1000 * 60 * 60 * 24);
                vm.$set(vm.autoAnswer.periods[periodIndex], 'offset', offset);

                if(period.end_date){
                    endDate = new Date(period.end_date);
                    days = (endDate - startDate) / (1000 * 60 * 60 * 24) - 1;
                }else{
                    days = vm.periodsChart.diffDays - offset;
                }

                if(days < 0) days = 1;

                vm.$set(vm.autoAnswer.periods[periodIndex], 'days', days);
            });
        }
    },
    created(){
        this.setPeriodsChartLabels();
        this.setPeriodsData();

        var vm = this;
        this.autoAnswer.types = this.autoAnswer.types.toString();
        Object.keys(this.types).forEach(function(typeKey){
            if(vm.autoAnswer.types.indexOf(typeKey) != -1) vm.types[typeKey].active = true;
        });
    }
});
