var newAutoAnswer = new Vue({
    el: '#new-auto-answer',

});

var autoAnswersTable = new Vue({
    el: '#auto-answers-table',
    data: {
        type: 'calendar',

        currentAutoAnswer: args.currentAutoAnswer,
    },
    methods: {
        changeType(type){
            this.type = type;
        },
        getUrl(id){
            return route('solution.4.deployment.autoAnswers.show', {
                deployment: args.deployment,
                autoAnswerId: id,
            });
        },
    }
});
