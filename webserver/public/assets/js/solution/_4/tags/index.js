var tagsTable = new Vue({
    el: '#tags-table',
    data: {
        query: '',
        tags: args.tags,
        paginator: args.paginator,
        is_loading: false,
        order: {
            column: 'id',
            dir: 'desc',
        },
    },
    methods: {
        changePage(page){
            this.getTags(page);
        },
        search(){
            this.getTags(1);
        },
        // sort(column){
        //     if(column != this.order.column){
        //         this.order.column = column;
        //         this.order.dir = 'asc';
        //     }else{
        //         this.order.dir = this.order.dir == 'asc' ? 'desc' : 'asc';
        //     }
        //     this.getliffs(1);
        // },
        getTags(page){
            this.is_loading = true;
            axios
            .get(route('solution.4.deployment.tags.getTags', {
                deployment: args.deployment,
                page: page,
                column: this.order.column,
                dir: this.order.dir,
                q: this.query,
            }))
            .then(response => {
                this.paginator = response.data.paginator;
                this.tags = response.data.tags;
                this.is_loading = false;
            }).catch(error => {
                this.is_loading = false;
                console.log(error);
            });
        },
        getUrl(id){
            return route('solution.4.deployment.tags.show', {
                deployment: args.deployment,
                tagId: id,
            });
        },
    },
});
