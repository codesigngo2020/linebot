var tagContent = new Vue({
    el: '#tag-content',
    data: {
        tag: args.tag,

        submitting: false,
        form: {
            name: {
                name: args.tag.name ? args.tag.name : '',
                info: '',
                invalid: false,
            }
        },

        charts: [
            {
                isLoading: true,

                cardName: '累積觸發人數佔比',

                name: 'cumulativeUsersCountPercentageChart',
                id: 'cumulative-users-count-percentage-chart',
                borderColor: '#2c7be5',
                labelString: '累積觸發人數佔比',

                datetime: {
                    type: 'week',
                    date: {year: 2001, month: 1, day: 1, offset: 0},
                },
            },
            {
                isLoading: true,

                cardName: '觸發人次＆累積觸發人次',
                type: 'usersCount',

                name: 'cumulativeUsersCountChart',
                id: 'cumulative-users-count-chart',
                borderColor: '#f6c343',
                labelString: '觸發人次',

                datetime: {
                    type: 'week',
                    date: {year: 2001, month: 1, day: 1, offset: 0},
                },
            },
        ],

        datetimeSetting: {
            days: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
            startData: {year: 2001, month: 1, day: 1},
            today: {year: 2001, month: 1, day: 1},
            thisWeekOffset: 0,
        },
    },
    methods: {
        changeChartType(index, type){
            this.charts[index].type = type;
        },
        changeDatetimeType(index, type){
            this.charts[index].datetime.type = type;
        },
        isLeapYear(year){
            return (year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0));
        },
        getWeekDayIndex(year, month, day){
            var yearsCount = year - this.datetimeSetting.startData.year - 1,
            leapYearsCount = Math.floor(yearsCount/4) - Math.floor(yearsCount/100) + Math.floor(yearsCount/400);

            // 2001/01/01到前年底的總天數
            daysCount = yearsCount * 365 + leapYearsCount

            // 今年到上個月的總天數
            for(var i = 0; i < month - 1; i ++){
                daysCount += this.datetimeSetting.days[i];
            }
            // 閏年月份大於2者，+1天
            if(month > 2 && this.isLeapYear(year)) daysCount ++;

            // 加上本月天數
            daysCount += day;
            return daysCount%7;
        },
        getChartLabels(index){
            if(this.charts[index].datetime.type == 'week'){
                labels = [];
                d = new Date();
                d.setDate(d.getDate() + this.charts[index].datetime.date.offset);

                labels.push((d.getMonth() + 1)+'/'+d.getDate());
                for(var i = 0; i < 13; i ++){
                    d.setDate(d.getDate() + 1);
                    labels.push((d.getMonth() + 1)+'/'+d.getDate());
                }

                return labels;
            }else{
                return [...Array(24).keys()];
            }
        },
        getChartData(index){
            return new Promise((resolve, reject)=>{
                axios
                .get(route('solution.4.deployment.tags.getChartData', {
                    deployment: args.deployment,
                    tagId: this.tag.id,
                    chartName: this.charts[index].name,
                    chartType: this.charts[index].name == 'cumulativeUsersCountChart' ? this.charts[index].type : null,
                    type: this.charts[index].datetime.type,
                    start: this.charts[index].datetime.date.year+'-'+('0'+this.charts[index].datetime.date.month).slice(-2)+'-'+('0'+this.charts[index].datetime.date.day).slice(-2),
                }))
                .then(response => {
                    console.log(response.data)
                    resolve(response.data);
                }).catch(error => {
                    console.log(error);
                    reject([]);
                });
            });
        },
        async updateChart(index){
            let res = await this.getChartData(index);

            this[this.charts[index].name].data.labels = this.getChartLabels(index);
            this[this.charts[index].name].data.datasets[0].data = res;
            this[this.charts[index].name].update();
            this.charts[index].isLoading = false;
        },
        updateDate(index){
            d = new Date();
            d.setDate(d.getDate() + this.charts[index].datetime.date.offset);
            this.charts[index].datetime.date.year = d.getFullYear();
            this.charts[index].datetime.date.month = d.getMonth() + 1;
            this.charts[index].datetime.date.day = d.getDate();
        },
        toTodayOrThisWeek(index, isCreated){
            this.charts[index].isLoading = true;
            this.charts[index].datetime.date.offset = this.charts[index].datetime.type == 'week' ? this.datetimeSetting.thisWeekOffset : 0;
            this.updateDate(index);
            if(isCreated) this.updateChart(index);
        },
        last(index){
            this.charts[index].isLoading = true;
            this.charts[index].datetime.date.offset -= this.charts[index].datetime.type == 'week' ? 14 : 1;
            this.updateDate(index);
            this.updateChart(index);
        },
        next(index){
            console.log(index)
            this.charts[index].isLoading = true;
            this.charts[index].datetime.date.offset += this.charts[index].datetime.type == 'week' ? 14 : 1;
            this.updateDate(index);
            this.updateChart(index);
        },
        buildFormData(formData, data, parentKey) {
            if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                Object.keys(data).forEach(key => {
                    tagContent.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                });
            } else {
                const value = data == null ? '' : data;
                formData.append(parentKey, value);
            }
        },
        jsonToFormData(data){
            let formData = new FormData();
            this.buildFormData(formData, data, 'form');
            return formData;
        },
        submit(){
            if(!this.submitting){
                this.submitting = true;
                this.form.name.info = '';
                this.form.name.invalid = false;

                formData = this.jsonToFormData(this.form);
                formData.append('_token', args._token);

                axios.post(route('solution.4.deployment.tags.name', [args.deployment, this.tag.id]),
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    console.log(response);
                    if(response.data.status == 'success'){
                        this.submitting = false;
                        this.tag.name = response.data.name;
                        $("#tag-naming").modal('hide');
                    };
                })
                .catch(error => {
                    var errors = error.response.data.errors;
                    this.submitting = false;
                    var invalid_crawler = null;
                    for(error in errors){
                        console.log(error);

                        // 找出最後一個「.」後的字串
                        var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                        // 如果是value，表示是select，再往前找一個value
                        if(last == 'value'){
                            select_last = error.match(/\.(value\.value)$/);
                            // 如果符合「.value.value」，表示是多層value
                            if(select_last) last = select_last.slice(-1)[0];
                        }

                        // 替換數字成以[]包覆表示
                        error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                        // 如果是area，更新invalid = true
                        if(area = error_ref.match(/^(.*)\.action/)){
                            eval('this.'+area.slice(-1)[0]).invalid = true;
                        }

                        info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        try{
                            this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                            this.$set(eval('this.'+invalid_ref), 'invalid', true);
                        }catch(e){

                        }
                    }
                });
            }
        }
    },
    created(){
        // 累積觸及人數佔比圖
        d = new Date();
        this.datetimeSetting.today.year = d.getFullYear();
        this.datetimeSetting.today.month = d.getMonth() + 1;
        this.datetimeSetting.today.day = d.getDate();

        this.datetimeSetting.thisWeekOffset = -this.getWeekDayIndex(this.datetimeSetting.today.year, this.datetimeSetting.today.month, this.datetimeSetting.today.day) - 7;

        var vm = this;
        this.charts.forEach(function(chart, index){
            vm.charts[index].datetime.date.offset = vm.datetimeSetting.thisWeekOffset;
            vm.updateDate(index);

            vm.$nextTick(async function(){
                chartData = {
                    type: 'line',
                    data: {
                        labels: vm.getChartLabels(index),
                        datasets: [{
                            data: [0,10,20,30,40,50,60,70,80,90,100,110,120,130],
                            lineTension: 0,
                            borderColor: chart.borderColor,
                            pointBackgroundColor: chart.borderColor,
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                gridLines: {
                                    display: true,
                                    lineWidth: 1,
                                    zeroLineWidth: 1,
                                },
                                scaleLabel: {
                                    display: true,
                                    labelString: chart.labelString,
                                },
                                allowDecimals: false,
                                stacked: true,
                            }],
                            xAxes: [{
                                stacked: true,
                            }],
                        }
                    }
                };

                if(chart.name == 'cumulativeUsersCountPercentageChart'){
                    chartData.options.scales.yAxes[0].ticks = {
                        beginAtZero: true,
                        max: 100,
                        min: 0,
                        stepSize: 10
                    };
                }else{
                    chartData.options.scales.yAxes[0].ticks = {
                        beginAtZero: true,
                        suggestedMin: 10,
                    };
                }

                vm[chart.name] = new Chart(chart.id, chartData);

                vm[chart.name].data.datasets[0].data = await vm.getChartData(index);
                vm[chart.name].update();
                vm.charts[index].isLoading = false;
            });
        });
    },
    watch: {
        'charts.0.datetime.type': function(){
            this.toTodayOrThisWeek(0, true);
        },
        'charts.1.datetime.type': function(){
            this.toTodayOrThisWeek(1, true);
        },
        'charts.1.type': function(newValue){
            this[this.charts[1].name].options.scales.yAxes[0].scaleLabel.labelString = newValue == 'usersCount' ? '觸發人次' : '累積觸發人次';
            this.toTodayOrThisWeek(1, true);
        }
    }
});
