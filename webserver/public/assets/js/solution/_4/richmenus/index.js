var newRichmenu = new Vue({
    el: '#new-richmenu',

});

var richmenusTable = new Vue({
    el: '#richmenus-table',
    data: {
        query: '',
        richmenus: [],
        paginator: {
            currentPage: 1,
            lastPage: 1,
        },
        is_loading: false,
        order: {
            column: 'id',
            dir: 'desc',
        },
    },
    methods: {
        changePage(page){
            this.getRichmenus(page);
        },
        search(){
            this.getRichmenus(1);
        },
        sort(column){
            if(column != this.order.column){
                this.order.column = column;
                this.order.dir = 'asc';
            }else{
                this.order.dir = this.order.dir == 'asc' ? 'desc' : 'asc';
            }
            this.getRichmenus(1);
        },
        getRichmenus(page){
            this.is_loading = true;
            axios
            .get(route('solution.4.deployment.richmenus.getRichmenus', {
                deployment: args.deployment,
                page: page,
                column: this.order.column,
                dir: this.order.dir,
                q: this.query,
            }))
            .then(response => {
                this.paginator = response.data.paginator;
                this.richmenus = response.data.richmenus;
                this.$nextTick(function(){
                    $('.dropdown-toggle').dropdown();
                });
                this.is_loading = false;
            }).catch(error => {
                this.is_loading = false;
                console.log(error);
            });
        },
        getUrl(id){
            return route('solution.4.deployment.richmenus.show', {
                deployment: args.deployment,
                richmenuId: id,
            });
        },
        toggleDefault(id){
            this.is_loading = true;

            axios
            .post(route('solution.4.deployment.richmenus.toggelDefault', {
                deployment: args.deployment,
                richmenuId: id,
            }), {
                _token: args._token,
            })
            .then(response => {
                if(response.data.success){
                    this.getRichmenus(this.paginator.currentPage);
                }else{
                    this.is_loading = false;
                }
            })
            .catch(error => {
                this.is_loading = false;
            });
        },
        cancelBinding(id){
            this.is_loading = true;

            axios
            .post(route('solution.4.deployment.richmenus.cancelBinding', {
                deployment: args.deployment,
                richmenuId: id,
            }), {
                _token: args._token,
            })
            .then(response => {
                if(response.data.success){
                    this.getRichmenus(this.paginator.currentPage);
                }else{
                    this.is_loading = false;
                }
            })
            .catch(error => {
                this.is_loading = false;
            });
        },
        deleteAll(id){
            this.is_loading = true;
            axios({
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': args._token,
                },
                url: route('solution.4.deployment.richmenus.deleteAll', {
                    deployment: args.deployment,
                    richmenuId: id,
                }),
            })
            .then(response => {
                if(response.data.success) this.getRichmenus(this.paginator.currentPage);
            })
            .catch(error => {
                this.is_loading = false;
            });
        },
    },
    mounted(){
        $('.dropdown-toggle').dropdown();
    },
    created(){
        this.getRichmenus(1);
    }
});
