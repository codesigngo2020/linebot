disablePageSwitch();

var richmenuForm = new Vue({
    el: '#richmenu-form',
    data: {
        submitting: false,

        currentAreaId: 0,
        currentAreaCount: 0,
        gottenAreaPath: '',
        gottenAreaIndex: 0,

        areaActionId: 0,

        form: {
            name: {
                name: args.richmenu.name,
                info: '',
                invalid: false,
            },
            description: {
                description: args.richmenu.description,
                info: '',
                invalid: false,
            },
            size: {
                size: args.richmenu.size,
                info: '',
                invalid: false,
            },
            chatBarText: {
                chatBarText: args.richmenu.chatBarText,
                info: '',
                invalid: false,
            },
            selected: {
                selected: args.richmenu.selected,
                info: '',
                invalid: false,
            },
            image: {
                src: args.richmenu.imageUrl,
                alt: '',
                file: null,
                info: '',
                invalid: false,
            },
            area: {
                area: args.richmenu.areas_form_data,
                info: '',
                invalid: false,
            },
            actionType: {
                currentAreaId: null,
                value: null,
                options: [
                    {
                        name: '回覆好友訊息',
                        value: 'message',
                    },
                    {
                        name: '好友回覆關鍵字',
                        value: 'keyword',
                    },
                    {
                        name: '直接開啟網址',
                        value: 'uri',
                    },
                    {
                        name: '透過 Liff 開啟網址',
                        value: 'liff',
                    },
                    {
                        name: '開啟訊息腳本',
                        value: 'script',
                    },
                    {
                        name: '切換主選單',
                        value: 'richmenu',
                    },
                ],
                info: '',
                invalid: false,
            }
        }
    },
    methods: {
        dragstart(event){
            this.saveAndClearAction(null, false)
            newAreaType = event.currentTarget.id;
        },
        dragend(event){
            newAreaType = null;
        },
        newArea(areaId){
            if(this.currentAreaCount < 20){
                this.getArea(this.form.area.area, areaId, 'this.form.area.area');

                if(this.gottenAreaPath && this.gottenAreaIndex){
                    // 如果區域存在
                    newArea = null;
                    if(newAreaType){
                        if(typeMatch = newAreaType.match(/^new-row-(2|3|5)$/)){

                            newArea = [
                                [
                                    eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']'),
                                ],
                            ];

                            for(var i = 1; i < typeMatch[1]; i ++){
                                if(this.currentAreaCount >= 20) continue;

                                this.currentAreaId ++;
                                this.currentAreaCount ++;
                                newArea.push([
                                    {
                                        id: this.currentAreaId,
                                        type: {
                                            name: '回覆好友訊息',
                                            value: 'message',
                                        },
                                        action: {
                                            text: {
                                                text: '',
                                                info: '',
                                                invalid: false,
                                            }
                                        },
                                        selected: false,
                                        invalid: false,
                                    }
                                ]);
                            };

                            parentPath = this.gottenAreaPath.match(/^this.form.area.area\[([0-9]+)\]$/)
                            if(eval(this.gottenAreaPath).length == 1 && this.form.area.area.length == 1 && parentPath){
                                this.$set(this.form.area, 'area', newArea);
                            }else{
                                this.$set(eval(this.gottenAreaPath), this.gottenAreaIndex, newArea);
                            }

                        }else if(typeMatch = newAreaType.match(/^new-column-(2|3|5)$/)){

                            parentPath = this.gottenAreaPath.match(/^(.*)\[([0-9]+)\]$/)

                            newArea = [
                                eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']'),
                            ];

                            for(var i = 1; i < typeMatch[1]; i ++){
                                if(this.currentAreaCount >= 20) continue;

                                this.currentAreaId ++;
                                this.currentAreaCount ++;
                                newArea.push({
                                    id: this.currentAreaId,
                                    type: {
                                        name: '回覆好友訊息',
                                        value: 'message',
                                    },
                                    action: {
                                        text: {
                                            text: '',
                                            info: '',
                                            invalid: false,
                                        }
                                    },
                                    selected: false,
                                    invalid: false,
                                });
                            };
                            if(eval(this.gottenAreaPath).length == 1){
                                this.$set(eval(parentPath[1]), parentPath[2], newArea);
                            }else{
                                this.$set(eval(this.gottenAreaPath), this.gottenAreaIndex, [newArea]);
                            }
                        }
                    }
                };
            }
        },
        getArea(obj, areaId, path){
            for(key in obj){
                if(obj[key].id && obj[key].id == areaId){
                    this.gottenAreaPath = path;
                    this.gottenAreaIndex = key;
                }else if(obj[key][0]){
                    currentPath = path+'['+key+']';
                    this.getArea(obj[key], areaId, currentPath);
                }
            }
        },
        changeImage(){
            $("#image-input input").click();
        },
        removeArea(areaId){
            this.getArea(this.form.area.area, areaId, 'this.form.area.area');

            if(eval(this.gottenAreaPath).length == 1){
                // 1個tr刪掉，往上提一層
                if(parentPath = this.gottenAreaPath.match(/^this.form.area.area\[([0-9]+)\]$/)){
                    // 第一層
                    if(this.form.area.area.length > 1){
                        this.form.area.area.splice(parentPath[1], 1);
                        this.currentAreaCount --;
                    }

                }else if(parentPath = this.gottenAreaPath.match(/^(((.*)\[([0-9]+)\])\[([0-9]+)\])\[([0-9]+)\]$/)){
                    // 內層
                    if(eval(parentPath[1]).length == 2){
                        // 只有兩個tr
                        if(eval(parentPath[1]+'['+(1 - parseInt(parentPath[6]))+']').length == 1){
                            this.$set(eval(parentPath[2]), parentPath[5], eval(parentPath[1]+'['+(1 - parseInt(parentPath[6]))+'][0]'));
                        }else{
                            eval(parentPath[1]).splice(parentPath[6], 1);

                        }

                    }else{
                        // 兩個tr以上
                        eval(parentPath[1]).splice(parentPath[6], 1);
                    }
                    this.currentAreaCount --;
                }

            }else if(eval(this.gottenAreaPath).length == 2){
                // 2個td刪掉一個
                if(parentPath = this.gottenAreaPath.match(/^this.form.area.area\[([0-9]+)\]$/)){
                    // 第一層
                    eval(this.gottenAreaPath).splice(this.gottenAreaIndex, 1);
                }else if(parentPath = this.gottenAreaPath.match(/^((.*)\[([0-9]+)\])\[([0-9]+)\]$/)){
                    // 內層
                    if(eval(parentPath[1]).length == 1){
                        this.$set(eval(parentPath[2]), parentPath[3], eval(this.gottenAreaPath+'['+(1 - this.gottenAreaIndex)+']'));
                    }else{
                        eval(this.gottenAreaPath).splice(this.gottenAreaIndex, 1);
                    }

                }
                this.currentAreaCount --;

            }else{
                // 多個td刪除一個
                eval(this.gottenAreaPath).splice(this.gottenAreaIndex, 1);
                this.currentAreaCount --;
            }

            if(this.form.actionType.currentAreaId == areaId) this.saveAndClearAction(null, true);
        },
        saveAndClearAction(newId, removed){
            if(removed){
                // 原本的區域已刪除
                this.form.actionType.value = null;
            }else if(this.form.actionType.currentAreaId){
                // 原本的區域沒刪除，儲存類型
                this.getArea(this.form.area.area, this.form.actionType.currentAreaId, 'this.form.area.area');
                eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']').type = this.form.actionType.value;
                eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']').selected = false;
            }

            if(newId){
                this.form.actionType.currentAreaId = newId;
                // 新area的資料
                this.getArea(this.form.area.area, newId, 'this.form.area.area');
                newAreaData = eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']');

                eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']').invalid = false;
                eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']').selected = true;
                this.form.actionType.value = newAreaData.type;

            }else{
                this.form.actionType.currentAreaId = null;
            }
        },
        updateActionData(data){
            this.getArea(this.form.area.area, this.form.actionType.currentAreaId, 'this.form.area.area');
            this.$set(eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']'), 'action', data)
        },
        getActionData(){
            this.getArea(this.form.area.area, this.form.actionType.currentAreaId, 'this.form.area.area');
            return eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']').action;
        },
        changeActionType(){
            this.getArea(this.form.area.area, this.form.actionType.currentAreaId, 'this.form.area.area');

            switch(this.form.actionType.value.value){
                case 'message':
                action = {
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    }
                };
                break;

                case 'keyword':
                action = {
                    keyword: {
                        value: null,
                        info: '',
                        invalid: false,
                    },
                    text: {
                        text: '',
                        info: '',
                        invalid: false,
                    }
                };
                break;

                case 'uri':
                action = {
                    uri: {
                        uri: '',
                        info: '',
                        invalid: false,
                    }
                };
                break;

                case 'liff':
                action = {
                    size: {
                        value: null,
                        info: '',
                        invalid: false,
                    },
                    uri: {
                        uri: '',
                        info: '',
                        invalid: false,
                    }
                };
                break;

                case 'script':
                action = {
                    script: {
                        value: null,
                        info: '',
                        invalid: false,
                    },
                };
                break;

                case 'richmenu':
                action = {
                    richmenu: {
                        value: null,
                        info: '',
                        invalid: false,
                    },
                };
                break;

                default:
            }

            eval(this.gottenAreaPath+'['+this.gottenAreaIndex+']').action = action;
        },
        AllInvalidToFalse(obj){
            for(key in obj){
                if(key == 'invalid'){
                    obj[key] = false;
                    obj['info'] = '';
                }else if(typeof(obj[key]) === 'object'){
                    this.AllInvalidToFalse(obj[key]);
                }
            }
        },
        buildFormData(formData, data, parentKey) {
            if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                Object.keys(data).forEach(key => {
                    richmenuForm.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                });
            } else {
                const value = data == null ? '' : data;
                formData.append(parentKey, value);
            }
        },
        jsonToFormData(data){
            let formData = new FormData();
            this.buildFormData(formData, data, 'form');
            return formData;
        },
        submit(){
            this.saveAndClearAction(null, false);

            if(!this.submitting){
                this.submitting = true;
                this.AllInvalidToFalse(this.form);

                formData = this.jsonToFormData(this.form);
                formData.append('_token', args._token);

                axios.post(route('solution.4.deployment.richmenus.newVersion', [args.deployment, args.richmenu.id]),
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    console.log(response);
                    if(response.data.status == 'success'){
                        window.open(route('solution.4.deployment.richmenus.show', [args.deployment, response.data.richmenuId]), '_self');
                    }else{
                        this.submitting = false;
                    }
                })
                .catch(error => {
                    var errors = error.response.data.errors;
                    this.submitting = false;
                    var invalid_crawler = null;
                    for(error in errors){
                        console.log(error);

                        // 找出最後一個「.」後的字串
                        var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                        // 如果是value，表示是select，再往前找一個value
                        if(last == 'value'){
                            select_last = error.match(/\.(value\.value)$/);
                            // 如果符合「.value.value」，表示是多層value
                            if(select_last) last = select_last.slice(-1)[0];
                        }

                        // 替換數字成以[]包覆表示
                        error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                        // 如果是area，更新invalid = true
                        if(area = error_ref.match(/^(.*)\.action/)){
                            eval('this.'+area.slice(-1)[0]).invalid = true;
                        }

                        info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        try{
                            this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                            this.$set(eval('this.'+invalid_ref), 'invalid', true);
                        }catch(e){

                        }
                    }
                });
            }
        },
        recurseArea(action, obj){
            for(key in obj){
                if(obj[key].id){
                    if(action == 'init'){
                        if(obj[key].id > this.currentAreaId) this.currentAreaId = obj[key].id;
                        this.currentAreaCount++;
                    }
                }else if(obj[key][0]){
                    this.recurseArea(action, obj[key]);
                }
            }
        },
    },
    created(){
        this.recurseArea('init', this.form.area.area);
    },
    watch: {
        'form.size.size': function(newValue){
            this.form.image.src = '';
            this.form.image.alt = '';
            this.form.image.info = '';
            this.form.image.invalid = false;

            newActionData = {
                type: {
                    name: '回覆好友訊息',
                    value: 'message',
                },
                action: null,
            };

            if(newValue == '2500x1686'){
                newAreaData = [];
                for(var i = 0; i < 2; i ++){
                    newAreaData[i] = [];
                    for(var j = 0; j < 3; j ++){
                        newAreaData[i][j] = Object.assign({
                            id: (3*i)+j+1,
                        }, newActionData);
                    }
                }

                this.$set(this.form.area, 'area', newAreaData);
                this.currentAreaCount = 6;
                this.currentAreaId = 6;
            }else{
                newAreaData = [[]];
                for(var i = 0; i < 3; i ++){
                    newAreaData[0][i] = Object.assign({
                        id: i+1,
                    }, newActionData);
                }

                this.$set(this.form.area, 'area', newAreaData);
                this.currentAreaCount = 3;
                this.currentAreaId = 3;
            }
        },
    },
});

var templete = document.getElementById('dz-preview').innerHTML,
myDropzone = new Dropzone("#richmenuImage", {
    url: '#',
    autoProcessQueue: false,
    maxFiles: 1,
    acceptedFiles: ".jpeg,.jpg,.png,.JPEG,.JPG,.PNG",
    thumbnailWidth: null,
    thumbnailHeight: null,
    previewTemplate: templete,
    previewsContainer: "#dz-preview",
    hiddenInputContainer: "#image-input",
    addedfile: function(file){
        richmenuForm.form.image.info = '';
        richmenuForm.form.image.invalid = false;

        var img = new Image();

        img.src = window.URL.createObjectURL(file);
        img.onload = function(){
            var width = img.naturalWidth,
            height = img.naturalHeight;

            if(richmenuForm.form.size.size == '2500x1686'){
                if(width != 2500 || height != 1686){
                    richmenuForm.form.image.info = '請符合圖片尺寸：2500 x 1686';
                    richmenuForm.form.image.invalid = true;
                    return false;
                }
            }else if(richmenuForm.form.size.size == '2500x843'){
                if(width != 2500 || height != 843){
                    richmenuForm.form.image.info = '請符合圖片尺寸：2500 x 843';
                    richmenuForm.form.image.invalid = true;
                    return false;
                }
            }
            richmenuForm.form.image.src = window.URL.createObjectURL(file);
            richmenuForm.form.image.alt = file.name;
            richmenuForm.form.image.file = file;
        };

        return false;
    },
});
