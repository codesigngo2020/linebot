var richmenuVersion = new Vue({
    el: '#richmenu-version',
    data: {
        version: {
            value: {
                name: 'v'+args.richmenu.version,
                value: args.richmenu.id,
                active: args.richmenu.is_current_version,
            },
            options: args.richmenu.versions,
        }
    },
    watch: {
        'version.value.value': function(newValue){
            window.open(route('solution.4.deployment.richmenus.show', [args.deployment, newValue]), '_self');
        }
    }
});

var richmenuContent = new Vue({
    el: '#richmenu-content',
    data: {
        richmenu: args.richmenu,
        tagId: args.tagId,

        areaList: [],
        showDetailAreaList: [],
        showTagDetail: false,

        showGrids: true,
    },
    methods: {
        toggleShowGrids(){
            this.showGrids = !this.showGrids;
        },
        toggleShowAreaDetail(index){
            if(!this.areaList[index].show || this.showDetailAreaList.length > 1){
                this.areaList[index].show = !this.areaList[index].show;

                areaList = this.areaList.reduce(function(areaList, area, areaIndex){
                    if(area.show) areaList.push(areaIndex);
                    return areaList;
                }, []);

                this.showDetailAreaList = areaList;
            }
        },
        recurseArea(action, obj){
            for(key in obj){
                if(obj[key].id){
                    if(action == 'getAreaList'){
                        length = this.areaList.push($.extend(true, {show: false}, obj[key]));
                        if(obj[key].tag && !this.showTagDetail){
                            this.showTagDetail = true;
                            this.showDetailAreaList.push(length - 1);
                            this.areaList[length - 1].show = true;
                        }
                    }
                }else if(obj[key][0]){
                    this.recurseArea(action, obj[key]);
                }
            }
        },
        getTagUrl(id){
            return route('solution.4.deployment.tags.show', {
                deployment: args.deployment,
                tagId: id,
            });
        },
        getKeywordUrl(id){
            return route('solution.4.deployment.keywords.show', {
                deployment: args.deployment,
                keywordId: id,
            });
        },
        getScriptUrl(id){
            return route('solution.4.deployment.scripts.show', {
                deployment: args.deployment,
                scriptId: id,
            });
        },
        getRichmenuUrl(id){
            return route('solution.4.deployment.richmenus.show', {
                deployment: args.deployment,
                richmenuId: id,
            });
        },
        getRichmenuMessage(richmenu){
            if(richmenu.deleted_all_at){
                return '（選單已刪除）';
            }else if(!richmenu.is_current_version){
                return '（未指定選單版本）';
            }else{
                return '';
            }
        },
        toggleDefault(id){
            axios
            .post(route('solution.4.deployment.richmenus.toggelDefault', {
                deployment: args.deployment,
                richmenuId: id,
            }), {
                _token: args._token,
            })
            .then(response => {
                if(response.data.success) location.reload();
            })
            .catch(error => {

            });
        },
        switchVersion(id){
            axios
            .post(route('solution.4.deployment.richmenus.switchVersion', {
                deployment: args.deployment,
                richmenuId: id,
            }), {
                _token: args._token,
            })
            .then(response => {
                if(response.data.success) location.reload();
            })
            .catch(error => {

            });
        },
        remove(id){
            this.is_loading = true;
            axios({
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': args._token,
                },
                url: route('solution.4.deployment.richmenus.delete', {
                    deployment: args.deployment,
                    richmenuId: id,
                }),
            })
            .then(response => {
                if(response.data.success) location.reload();
            })
            .catch(error => {
                this.is_loading = false;
            });
        },
    },
    created(){
        // 觸發行為
        this.recurseArea('getAreaList', this.richmenu.areas_form_data);
        this.areaList.sort(function(a, b){
            return parseInt(a.id) > parseInt(b.id) ? 1 : -1;
        });
    }
});
