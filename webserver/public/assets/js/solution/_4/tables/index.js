var newTable = new Vue({
    el: '#new-table',
});

var tablesTable = new Vue({
    el: '#tables-table',
    data: {
        isCategoryLoading: false,
        category: 0,

        draggable: null,

        query: '',
        tables: [],
        paginator: {
            currentPage: 1,
            lastPage: 1,
        },
        is_loading: false,
        order: {
            column: 'id',
            dir: 'desc',
        },

        type: {
            normal: '一般',
            coupon: '優惠券',
        }
    },
    methods: {
        changePage(page){
            this.getTables(page);
        },
        search(){
            this.getTables(1);
        },
        refresh(){
            this.getTables(this.paginator.currentPage);
        },
        getTables(page){
            this.is_loading = true;
            axios
            .get(route('solution.4.deployment.tables.getTables', {
                deployment: args.deployment,
                page: page,
                category: this.category,
                column: this.order.column,
                dir: this.order.dir,
                q: this.query,
            }))
            .then(response => {
                this.paginator = response.data.paginator;
                this.tables = response.data.tables;
                this.is_loading = false;
            }).catch(error => {
                this.is_loading = false;
                console.log(error);
            });
        },
        getUrl(id){
            return route('solution.4.deployment.tables.show', {
                deployment: args.deployment,
                tableId: id,
            });
        },

        // 資料表設定
        remove(id){
            this.is_loading = true;
            axios({
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': args._token,
                },
                url: route('solution.4.deployment.tables.delete', {
                    deployment: args.deployment,
                    tableId: id,
                }),
            })
            .then(response => {
                if(response.data.success){
                    this.getTables(this.paginator.currentPage);
                }
            })
            .catch(error => {
                this.is_loading = false;
            });
        },

        // category
        setIsCategoryLoading(isLoading){
            this.isCategoryLoading = isLoading;
        },
        changeCategory(id){
            this.category = id;
            this.getTables(1);
        },
        mouseenter(keyword){
            this.draggable = keyword;
        },
        mouseleave(){
            this.draggable = null;
        },
    },
    created(){
        this.getTables(1);
    }
});
