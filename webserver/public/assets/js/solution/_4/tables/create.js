disablePageSwitch();

var tableForm = new Vue({
    el: '#table-form',
    data: {

        is_loading: false,

        submitting: false,

        file: {
            file: null,
            info: '',
            invalid: false,
        },

        dataErrors: [],

        options: {
            type: [
                {
                    group: '數字',
                    types: [
                        {
                            name: '整數',
                            value: 'integer',
                        },
                        {
                            name: '非負整數',
                            value: 'unsignedInteger',
                        },
                        {
                            name: '浮點數',
                            value: 'float',
                        },
                    ],
                },
                {
                    group: '文字',
                    types: [
                        {
                            name: '<= 180字',
                            value: 'varchar',
                        },
                        {
                            name: '> 180字',
                            value: 'text',
                        }
                    ],
                },
                {
                    group: '時間',
                    types: [
                        {
                            name: '日期（Y-m-d）',
                            value: 'date',
                        },
                        {
                            name: '時間戳（Y-m-d H:i:s）',
                            value: 'timestamp',
                        },
                    ],
                }
            ],
        },

        form: {
            name: {
                name: '',
                info: '',
                invalid: false,
            },
            description: {
                description: '',
                info: '',
                invalid: false,
            },
            type: {
                type: 'normal',
                info: '',
                invalid: false,
            },

            // coupon
            limit: {
                active: {
                    active: true,
                    info: '',
                    invalid: false,
                },
                count: {
                    count: 1,
                    info: '',
                    invalid: false,
                },
            },
            random: {
                random: true,
                info: '',
                invalid: false,
            },
            issueDatetime: {
                active: {
                    active: true,
                    info: '',
                    invalid: false,
                },
                date: {
                    date: '',
                    info: '',
                    invalid: false,
                },
                time: {
                    time: '',
                    info: '',
                    invalid: false,
                }
            },


            hasColumnName: {
                hasColumnName: true,
                info: '',
                invalid: false,
            },
            columns: {
                columns: [],
                info: '',
                invalid: false,
            },
            additionalColumns: {
                additionalColumns: [],
                info: '',
                invalid: false,
            },
            rows: {
                rows: [],
                info: '',
                invalid: false,
            }
        }
    },
    methods: {
        deleteColumn(column){
            if(this.form.columns.columns.filter(column => column.deleted.deleted == false).length > 1){
                column.type.value = null;
                column.canBeNull.canBeNull = false;
                column.isUnique.isUnique = false;
                column.deleted.deleted = true;
            }
        },
        restoreColumn(column){
            column.deleted.deleted = false;
        },
        newColumn(){
            this.form.additionalColumns.additionalColumns.push({
                name: {
                    name: '',
                    info: '',
                    invalid: false,
                },
                type: {
                    value: null,
                    info: '',
                    invalid: false,
                },
                canBeNull: {
                    canBeNull: false,
                    info: '',
                    invalid: false,
                },
                isUnique: {
                    isUnique: false,
                    info: '',
                    invalid: false,
                }
            });
        },
        deleteAdditionalColumn(index){
            this.form.additionalColumns.additionalColumns.splice(index, 1);
        },
        activateInputMask(ref, formPath){
            var vm = this;
            $(vm.$refs[ref]).mask(vm.$refs[ref].getAttribute('data-mask'), {
                translation: {
                    'A': {
                        pattern: /[2]/
                    },
                    'B': {
                        pattern: /[0]/
                    },
                    'C': {
                        pattern: /[0-9]/
                    },
                    'D': {
                        pattern: /[0-2]/
                    },
                    'E': {
                        pattern: /[0-3]/
                    },
                    'F': {
                        pattern: /[0-2]/
                    },
                    'G': {
                        pattern: /[0-5]/
                    }
                },
                onChange(cep){
                    eval(formPath + ' = cep');
                },
            });
        },




        AllInvalidToFalse(obj){
            for(key in obj){
                if(key == 'invalid'){
                    obj[key] = false;
                    obj['info'] = '';
                }else if(typeof(obj[key]) === 'object'){
                    this.AllInvalidToFalse(obj[key]);
                }
            }
        },
        buildFormData(formData, data, parentKey) {
            var vm = this;
            if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                Object.keys(data).forEach(key => {
                    vm.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                });
            } else {
                const value = data == null ? '' : data;
                formData.append(parentKey, value);
            }
        },
        jsonToFormData(data){
            let formData = new FormData();
            this.buildFormData(formData, data, 'form');
            return formData;
        },
        submit(){

            if(!this.submitting){
                this.submitting = true;
                this.dataErrors = [];
                this.AllInvalidToFalse(this.form);

                formData = this.jsonToFormData(this.form);
                formData.append('_token', args._token);

                axios.post(route('solution.4.deployment.tables.store', args.deployment),
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    console.log(response);
                    if(response.data.status == 'success'){
                        window.open(route('solution.4.deployment.tables.show', [args.deployment, response.data.tableId]), '_self');
                    }else{
                        this.submitting = false;
                    }
                })
                .catch(error => {
                    var errors = error.response.data.errors;
                    this.submitting = false;

                    var originalName_name = {};
                    this.form.columns.columns.forEach(function(column){
                        originalName_name[column.originalName.originalName] = column.name.name;
                    });

                    for(error in errors){

                        if(match = error.match(/form.rows.rows\.([0-9]+)\.(.*)$/)){
                            this.dataErrors.push('['+originalName_name[match[2]]+','+match[1]+']：'+errors[error][0]+'。');
                        }else{

                            // 找出最後一個「.」後的字串
                            var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                            // 如果是value，表示是select，再往前找一個value
                            if(last == 'value'){
                                select_last = error.match(/\.(value\.value)$/);
                                // 如果符合「.value.value」，表示是多層value
                                if(select_last) last = select_last.slice(-1)[0];
                            }

                            // 替換數字成以[]包覆表示
                            error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                            info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                            invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');

                            try{
                                this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                                this.$set(eval('this.'+invalid_ref), 'invalid', true);
                            }catch(e){

                            }
                        }
                    }
                });
            }
        }
    },
    mounted(){
        var vm = this;
        this.dropzone = new Dropzone(this.$refs.file, {
            url: "#",
            autoProcessQueue: false,
            maxFiles: 1,
            acceptedFiles: ".csv",
            thumbnailWidth: null,
            thumbnailHeight: null,
            addedfile: function(file){

                vm.is_loading = true;

                vm.file.info = '';
                vm.file.invalid = false;

                var reader = new FileReader();
                reader.readAsText(file);

                reader.onload = function(event) {
                    var csv = event.target.result,
                    rows = csv.split('\n');

                    if(rows.length == 0 || (rows.length == 1 && vm.form.hasColumnName.hasColumnName) || !rows[0].trim()){
                        vm.file.info = '須至少一筆資料';
                        vm.file.invalid = true;

                        vm.is_loading = false;
                        return false;
                    }

                    if(vm.form.hasColumnName.hasColumnName){
                        var isInvalid = false;
                        rows.splice(0, 1)[0].trim().split(',').some(function(column){
                            if(['id', 'created_at', 'updated_at'].includes(column)){
                                vm.file.info = 'id, created_at, updated_at 為系統保留欄位名稱，禁止使用';
                                vm.file.invalid = true;

                                vm.form.columns.columns = [];
                                isInvalid = true;
                                return true;
                            }

                            vm.form.columns.columns.push({
                                name: {
                                    name: column,
                                    info: '',
                                    invalid: false,
                                },
                                originalName: {
                                    originalName: column,
                                    info: '',
                                    invalid: false,
                                },
                                type: {
                                    value: null,
                                    info: '',
                                    invalid: false,
                                },
                                canBeNull: {
                                    canBeNull: false,
                                    info: '',
                                    invalid: false,
                                },
                                isUnique: {
                                    isUnique: false,
                                    info: '',
                                    invalid: false,
                                },
                                deleted: {
                                    deleted: false,
                                    info: '',
                                    invalid: false,
                                }
                            });
                        });

                        if(isInvalid) return false;
                    }else{
                        vm.form.columns.columns = [];
                        length = rows[0].split(',').length;
                        for(i = 1; i <= length; i ++){
                            vm.form.columns.columns.push({
                                name: {
                                    name: 'col_'+i,
                                    info: '',
                                    invalid: false,
                                },
                                originalName: {
                                    originalName: 'col_'+i,
                                    info: '',
                                    invalid: false,
                                },
                                type: {
                                    value: null,
                                    info: '',
                                    invalid: false,
                                },
                                canBeNull: {
                                    canBeNull: false,
                                    info: '',
                                    invalid: false,
                                },
                                isUnique: {
                                    isUnique: false,
                                    info: '',
                                    invalid: false,
                                },
                                deleted: {
                                    deleted: false,
                                    info: '',
                                    invalid: false,
                                }
                            });
                        }
                    }

                    vm.file.file = file;

                    rows.forEach(function(row){
                        r = {};
                        row.split(',').forEach((field, fieldIndex) => r[vm.form.columns.columns[fieldIndex].name.name] = field);

                        vm.form.rows.rows.push(r);
                    });

                    vm.is_loading = false;
                };

                return false;
            },
        });
    },
    watch: {
        'form.type.type': function(newValue){
            if(newValue == 'coupon'){
                var vm = this;
                this.$nextTick(function(){
                    ['date', 'time'].forEach(function(type){
                        vm.activateInputMask(type, 'vm.form.issueDatetime.'+type+'.'+type);
                    });
                });
            }
        }
    }
});
