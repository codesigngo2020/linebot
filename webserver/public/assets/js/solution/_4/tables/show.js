var tableContent = new Vue({
    el: '#table-content',
    data: {
        table: args.table,

        query: '',
        rows: [],
        paginator: {
            currentPage: 1,
            lastPage: 1,
        },
        is_loading: false,
        order: {
            column: 'id',
            dir: 'desc',
        },

        type: {
            normal: '一般',
            coupon: '優惠券',
        }
    },
    methods: {
        changePage(page){
            this.getRows(page);
        },
        search(){
            this.getRows(1);
        },
        refresh(){
            this.getRows(this.paginator.currentPage);
        },
        getRows(page){
            this.is_loading = true;
            axios
            .get(route('solution.4.deployment.tables.getRows', {
                deployment: args.deployment,
                tableId: args.table.id,
                page: page,
                column: this.order.column,
                dir: this.order.dir,
                q: this.query,
            }))
            .then(response => {
                this.paginator = response.data.paginator;
                this.rows = response.data.rows;
                this.is_loading = false;
            }).catch(error => {
                this.is_loading = false;
                console.log(error);
            });
        },


        remove(id){
            axios({
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': args._token,
                },
                url: route('solution.4.deployment.tables.delete', {
                    deployment: args.deployment,
                    tableId: id,
                }),
            })
            .then(response => {
                if(response.data.success){
                    window.open(route('solution.4.deployment.tables.index', args.deployment), '_self');
                }
            })
            .catch(error => {

            });
        },
    },
    created(){
        this.getRows(1);
    }
});
