var liffsTable = new Vue({
    el: '#liffs-table',
    data: {
        query: '',
        liffs: args.liffs,
        paginator: args.paginator,
        is_loading: false,
        order: {
            column: 'id',
            dir: 'desc',
        },
    },
    methods: {
        changePage(page){
            this.getLiffs(page);
        },
        search(){
            this.getLiffs(1);
        },
        sort(column){
            if(column != this.order.column){
                this.order.column = column;
                this.order.dir = 'asc';
            }else{
                this.order.dir = this.order.dir == 'asc' ? 'desc' : 'asc';
            }
            this.getLiffs(1);
        },
        getLiffs(page){
            this.is_loading = true;
            axios
            .get(route('solution.4.deployment.liffs.getLiffs', {
                deployment: args.deployment,
                page: page,
                column: this.order.column,
                dir: this.order.dir,
                q: this.query,
            }))
            .then(response => {
                this.paginator = response.data.paginator;
                this.liffs = response.data.liffs;
                this.is_loading = false;
            }).catch(error => {
                this.is_loading = false;
                console.log(error);
            });
        },
        setDefault(id, event){
            var checked = event.target.checked;
            this.is_loading = true;

            if(checked){
                var defaultRichmenu = $("#liffs-table input:checked").not(event.target)[0];
                if(defaultRichmenu) defaultRichmenu.checked = false;
            }

            axios
            .post(route('solution.4.deployment.liffs.toggelDefault', {
                deployment: args.deployment,
                richmenuId: id,
            }), {
                _token: args._token,
                default: checked,
            })
            .then(response => {
                if(!response){
                    if(checked && defaultRichmenu) defaultRichmenu.checked = true;
                    this.$refs['default-'+id][0].checked = !checked;
                }
                this.is_loading = false;
            })
            .catch(error => {
                if(checked && defaultRichmenu) defaultRichmenu.checked = true;
                this.$refs['default-'+id][0].checked = !checked;
                this.is_loading = false;
            });
        },
        remove(id){
            this.is_loading = true;
            axios({
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': args._token,
                },
                url: route('solution.4.deployment.liffs.delete', {
                    deployment: args.deployment,
                    liffId: id,
                }),
            })
            .then(response => {
                console.log(response.data)
                if(response.data.status == 'success'){
                    this.getLiffs(this.paginator.currentPage);
                }else{
                    this.is_loading = false;
                }
            })
            .catch(error => {
                this.is_loading = false;
            });
        }
    },
});
