var usersTable = new Vue({
    el: '#users-table',
    data: {
        query: '',
        users: args.users,
        paginator: args.paginator,
        is_loading: false,
        order: {
            column: 'id',
            dir: 'desc',
        },

        rolesName: {
            superadmin: '超級管理員',
            social_media_manager: '社群小編',
            customer_service_agent: '客服人員',
        },

        roles: {
            options: args.roles,
        },

        editFormSumitting: false,
        editForm: {
            user: null,
            roles: {
                value: null,
                info: '',
                invalid: false,
            }
        },

        addUserFormSumitting: false,
        addUserForm: {
            account: {
                account: '',
                info: '',
                invalid: false,
            },
            roles: {
                value: null,
                info: '',
                invalid: false,
            }
        }
    },
    methods: {
        changePage(page){
            this.getUsers(page);
        },
        search(){
            this.getUsers(1);
        },
        sort(column){
            if(column != this.order.column){
                this.order.column = column;
                this.order.dir = 'asc';
            }else{
                this.order.dir = this.order.dir == 'asc' ? 'desc' : 'asc';
            }
            this.getUsers(1);
        },
        getUsers(page){
            this.is_loading = true;

            var vm = this;

            axios
            .get(route('solution.4.deployment.iamAdmin.getUsers', {
                deployment: args.deployment,
                page: page,
                column: this.order.column,
                dir: this.order.dir,
                q: this.query,
            }))
            .then(response => {
                this.paginator = response.data.paginator;
                this.users = response.data.users;

                this.users.forEach(function(user){
                    user.roles.forEach(function(role){
                        role.name = vm.rolesName[role.name];
                    });
                });

                this.is_loading = false;
            }).catch(error => {
                this.is_loading = false;
                console.log(error);
            });
        },
        editUser(user){
            this.editForm.user = user;
            this.$set(this.editForm.roles, 'value', user.roles);
            $("#role-editting").modal('show');
        },
        buildFormData(formData, data, parentKey) {
            var vm = this;
            if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                Object.keys(data).forEach(key => {
                    vm.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                });
            } else {
                const value = data == null ? '' : data;
                formData.append(parentKey, value);
            }
        },
        jsonToFormData(data, name){
            let formData = new FormData();
            this.buildFormData(formData, data, name);
            return formData;
        },
        submitAddUserForm(){
            if(!this.addUserFormSumitting){
                this.addUserFormSumitting = true;

                this.addUserForm.account.info = '';
                this.addUserForm.account.invalid = false;
                this.addUserForm.roles.info = '';
                this.addUserForm.roles.invalid = false;

                formData = this.jsonToFormData(this.addUserForm, 'addUserForm');
                formData.append('_token', args._token);

                axios.post(route('solution.4.deployment.iamAdmin.invite', args.deployment),
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    console.log(response);
                    if(response.data.status == 'success'){
                        this.addUserFormSumitting = false;
                        $("#user-adding").modal('hide');
                        this.addUserForm.account.account = '';
                        this.addUserForm.roles.value = null;
                    };
                })
                .catch(error => {
                    var errors = error.response.data.errors;
                    this.addUserFormSumitting = false;
                    var invalid_crawler = null;
                    for(error in errors){
                        console.log(error);

                        // 找出最後一個「.」後的字串
                        var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                        // 如果是value，表示是select，再往前找一個value
                        if(last == 'value'){
                            select_last = error.match(/\.(value\.value)$/);
                            // 如果符合「.value.value」，表示是多層value
                            if(select_last) last = select_last.slice(-1)[0];
                        }

                        // 替換數字成以[]包覆表示
                        error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                        info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        try{
                            this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                            this.$set(eval('this.'+invalid_ref), 'invalid', true);
                        }catch(e){

                        }
                    }
                });
            }
        },
        submitEditFrom(){
            if(!this.editFormSumitting){
                this.editFormSumitting = true;
                this.editForm.roles.info = '';
                this.editForm.roles.invalid = false;

                formData = this.jsonToFormData(this.editForm, 'editForm');
                formData.append('_token', args._token);

                axios.post(route('solution.4.deployment.iamAdmin.assign', args.deployment),
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    console.log(response);
                    if(response.data.status == 'success'){
                        this.editFormSumitting = false;
                        this.getUsers(this.paginator.currentPage)
                        $("#role-editting").modal('hide');
                    };
                })
                .catch(error => {
                    var errors = error.response.data.errors;
                    this.editFormSumitting = false;
                    var invalid_crawler = null;
                    for(error in errors){
                        console.log(error);

                        // 找出最後一個「.」後的字串
                        var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                        // 如果是value，表示是select，再往前找一個value
                        if(last == 'value'){
                            select_last = error.match(/\.(value\.value)$/);
                            // 如果符合「.value.value」，表示是多層value
                            if(select_last) last = select_last.slice(-1)[0];
                        }

                        // 替換數字成以[]包覆表示
                        error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                        info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        try{
                            this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                            this.$set(eval('this.'+invalid_ref), 'invalid', true);
                        }catch(e){

                        }
                    }
                });
            }
        },
        removeUser(user){
            this.is_loading = true;
            axios({
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': args._token,
                },
                url: route('solution.4.deployment.iamAdmin.delete', {
                    deployment: args.deployment,
                    userId: user.id,
                }),
            })
            .then(response => {
                this.getUsers(this.paginator.currentPage);
            })
            .catch(error => {
                this.is_loading = false;
            });
        },
        addUser(){

        }
    },
    created(){
        var vm = this;

        this.users.forEach(function(user){
            user.roles.forEach(function(role){
                role.name = vm.rolesName[role.name];
            });
        });

        this.roles.options.forEach(function(role){
            role.name = vm.rolesName[role.name];
        });
    }
});
