var newBroadcast = new Vue({
    el: '#new-broadcast',

});

var broadcastsTable = new Vue({
    el: '#broadcasts-table',
    data: {
        type: 'calendar',
    },
    methods: {
        changeType(type){
            this.type = type;
        }
    }
});
