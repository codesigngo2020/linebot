disablePageSwitch();

var broadcastForm = new Vue({
    el: '#broadcast-form',
    data: {
        isLoading: false,
        submitting: false,

        newMessageType: null,

        hasQuickReply: false,

        message: {
            isLoading: false,
            options: [],
        },

        usersGroups: {
            isLoading: false,
            options: [],
        },
        users: {
            isLoading: false,
            options: [],
        },

        disableBroadcast: false,

        form: {
            name: {
                name: '',
                info: '',
                invalid: false,
            },
            description: {
                description: '',
                info: '',
                invalid: false,
            },
            target: {
                target_type: 'all',
                info: '',
                invalid: false,
            },
            usersGroups: {
                value: null,
                info: '',
                invalid: false,
            },
            users: {
                value: null,
                info: '',
                invalid: false,
            },
            timeType: {
                timeType: 'now',
                info: '',
                invalid: false,
            },
            datetime: {
                datetime: '',
                info: '',
                invalid: false,
            },
            messageType: {
                messageType: 'new',
                info: '',
                invalid: false,
            },
            message: {
                value: null,
                info: '',
                invalid: false,
            },
            messages: {
                messages: [
                    {
                        id: 1,
                        type: 'text',
                        message: {
                            text: {
                                text: '',
                                info: '',
                                invalid: false,
                            }
                        }
                    },
                ],
                info: '',
                invalid: false,
            }
        }
    },
    methods: {
        updateIsLoading(isLoading){
            this.isLoading = isLoading;
        },
        updateMessagesData(data){
            this.$set(this.form.messages, 'messages', data);
        },
        updateHasQuickReply(hasQuickReply){
            this.hasQuickReply = hasQuickReply;
        },
        updateNewMessageType(type){
            this.newMessageType = type;
        },

        usersGroupsLimitText(group){
            return `and ${group} other groups`;
        },
        getUsersGroups(query){
            this.usersGroups.isLoading = true;
            axios
            .get(route('solution.4.deployment.groups.queryForSelect', {
                deployment: args.deployment,
                q: query,
            }))
            .then(response => {
                this.usersGroups.options = response.data;
                this.usersGroups.isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this.usersGroups.options = [];
                this.usersGroups.isLoading = false;
                console.log(error);
            });
        },

        usersLimitText(user){
            return `and ${user} other users`;
        },
        getUsers(query){
            this.users.isLoading = true;
            axios
            .get(route('solution.4.deployment.users.queryForSelect', {
                deployment: args.deployment,
                follow: true,
                q: query,
            }))
            .then(response => {
                this.users.options = response.data;
                this.users.isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this.users.options = [];
                this.users.isLoading = false;
                console.log(error);
            });
        },

        messageslimitText(message){
            return `and ${message} other messages`;
        },
        getMessages(query){
            this.message.isLoading = true;
            axios
            .get(route('solution.4.deployment.messagesPool.queryForSelect', {
                deployment: args.deployment,
                type: this.form.messageType.messageType == 'pool' ? 'message' : 'template',
                q: query,
            }))
            .then(response => {
                this.message.options = response.data;
                this.message.isLoading = false;
                console.log(response.data);
            }).catch(error => {
                this.message.options = [];
                this.message.isLoading = false;
                console.log(error);
            });
        },
        getMessageUrl(id){
            return route('solution.4.deployment.messagesPool.show', {
                deployment: args.deployment,
                messageId: id,
            });
        },

        AllInvalidToFalse(obj){
            for(key in obj){
                if(key == 'invalid'){
                    obj[key] = false;
                    obj['info'] = '';
                }else if(typeof(obj[key]) === 'object'){
                    this.AllInvalidToFalse(obj[key]);
                }
            }
        },
        buildFormData(formData, data, parentKey) {
            if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                Object.keys(data).forEach(key => {
                    broadcastForm.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                });
            } else {
                const value = data == null ? '' : data;
                formData.append(parentKey, value);
            }
        },
        jsonToFormData(data){
            let formData = new FormData();
            this.buildFormData(formData, data, 'form');
            return formData;
        },
        submit(){
            if(!this.submitting){
                this.submitting = true;
                this.AllInvalidToFalse(this.form);

                formData = this.jsonToFormData(this.form);
                formData.append('_token', args._token);

                axios.post(route('solution.4.deployment.broadcasts.store', args.deployment),
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    console.log(response);
                    if(response.data.status == 'success'){
                        window.open(route('solution.4.deployment.broadcasts.show', [args.deployment, response.data.broadcastId]), '_self');
                    }else{
                        this.submitting = false;
                    }
                })
                .catch(error => {
                    var errors = error.response.data.errors;
                    this.submitting = false;
                    var invalid_crawler = null;
                    for(error in errors){
                        console.log(error);

                        // 找出最後一個「.」後的字串
                        var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                        // 如果是value，表示是select，再往前找一個value
                        if(last == 'value'){
                            select_last = error.match(/\.(value\.value)$/);
                            // 如果符合「.value.value」，表示是多層value
                            if(select_last) last = select_last.slice(-1)[0];
                        }

                        // 替換數字成以[]包覆表示
                        error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                        // 如果是area，更新invalid = true
                        if(area = error_ref.match(/^(.*)\.action/)){
                            eval('this.'+area.slice(-1)[0]).invalid = true;
                        }

                        info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');

                        var action_prefix = invalid_ref.match(/(.*actions\[\d\])/);
                        if(action_prefix) this.$set(eval('this.'+action_prefix[0]), 'invalid', true);

                        try{
                            this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                            this.$set(eval('this.'+invalid_ref), 'invalid', true);
                        }catch(e){

                        }
                    }
                });
            }
        }
    },
    created(){
        this.getUsers('');
        this.getUsersGroups('');
    },
    mounted(){
        $(this.$refs.datetime).mask(this.$refs.datetime.getAttribute('data-mask'), {
            translation: {
                'A': {
                    pattern: /[2]/
                },
                'B': {
                    pattern: /[0]/
                },
                'C': {
                    pattern: /[0-9]/
                },
                'D': {
                    pattern: /[0-2]/
                },
                'E': {
                    pattern: /[0-3]/
                },
                'F': {
                    pattern: /[0-2]/
                },
                'G': {
                    pattern: /[0-5]/
                }
            },
            onChange(cep){
                broadcastForm.form.datetime.datetime = cep;
            },
        });
    },
    watch: {
        'form.timeType.timeType': function(newValue){
            if(newValue == 'now'){
                this.form.datetime.datetime = '';
                $(this.$refs.datetime).val('');
            }
        },
        'form.messageType.messageType': function(newValue){
            this.message.options = [];
            this.form.message.value = null;

            if(newValue == 'template'){
                this.disableBroadcast = true;
                this.form.target.target_type = 'groups';
            }else{
                this.disableBroadcast = false;

            }
            if(newValue == 'new'){
                this.form.message.value = null;
            }else{
                this.getMessages('');
            }

            this.form.message.info = '';
            this.form.message.invalid = false;
        },
        'form.message.value': function(newValue){
            if(newValue && this.form.messageType.messageType == 'pool'){
                this.isLoading = true;
                axios
                .get(route('solution.4.deployment.messagesPool.getMessage', {
                    deployment: args.deployment,
                    messageId: newValue.value,
                }))
                .then(response => {
                    console.log(response)
                    this.form.messages.messages = response['data'];

                    this.hasQuickReply = this.form.messages.messages[this.form.messages.messages.length - 1].type == 'quickreply';

                    var vm = this;
                    this.$nextTick(function(){
                        vm.isLoading = false;
                    })
                }).catch(error => {
                    this.isLoading = false;
                    console.log(error);
                });
            }
        },
    }
});
