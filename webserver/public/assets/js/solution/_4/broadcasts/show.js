var broadcastContent = new Vue({
    el: '#broadcast-content',
    data: {
        broadcast: args.broadcast,
        tagId: args.tagId,

        targetTypes: {
            all: '全部好友',
            tags: '分眾標籤',
            users: '特定好友',
        },

        sendStatus: {
            N: {
                status: '未發送',
                class: 'secondary',
            },
            S: {
                status: '發送中',
                class: 'info',
            },
            F: {
                status: '發送完成',
                class: 'secondary',
            },
            P: {
                status: '部分發送',
                class: 'danger',
            },
            E: {
                status: '發送失敗',
                class: 'danger',
            },
        }
    },
    methods: {
        getTagUrl(id){
            return route('solution.4.deployment.tags.show', {
                deployment: args.deployment,
                tagId: id,
            });
        },
        remove(){
            axios({
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': args._token,
                },
                url: route('solution.4.deployment.broadcasts.delete', {
                    deployment: args.deployment,
                    broadcastId: this.broadcast.id,
                }),
            })
            .then(response => {
                if(response.data.status == 'success'){
                    window.open(route('solution.4.deployment.broadcasts.index', args.deployment), '_self');
                }
            })
            .catch(error => {

            });
        }
    }
});
