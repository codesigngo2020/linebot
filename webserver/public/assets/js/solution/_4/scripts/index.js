var newScript = new Vue({
    el: '#new-script',
});

var scriptsTable = new Vue({
    el: '#scripts-table',
    data: {
        isCategoryLoading: false,
        category: 0,

        draggable: null,

        query: '',
        scripts: [],
        paginator: {
            currentPage: 1,
            lastPage: 1,
        },
        is_loading: false,
        order: {
            column: 'id',
            dir: 'desc',
        },

        detail: {
            isLoading: false,
            id: 0,
            type: null,

            days: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],

            startData: {year: 2001, month: 1, day: 1},
            today: {year: 2001, month: 1, day: 1},
            thisWeekOffset: 0,

            date: {year: 2001, month: 1, day: 1, offset: 0},
        }
    },
    methods: {
        changePage(page){
            this.getScripts(page);
        },
        search(){
            this.getScripts(1);
        },
        refresh(){
            this.getScripts(this.paginator.currentPage);
        },
        getScripts(page){
            this.is_loading = true;
            axios
            .get(route('solution.4.deployment.scripts.getScripts', {
                deployment: args.deployment,
                page: page,
                category: this.category,
                column: this.order.column,
                dir: this.order.dir,
                q: this.query,
            }))
            .then(response => {
                this.paginator = response.data.paginator;
                this.scripts = response.data.scripts;
                this.is_loading = false;
            }).catch(error => {
                this.is_loading = false;
                console.log(error);
            });
        },
        getUrl(id){
            return route('solution.4.deployment.scripts.show', {
                deployment: args.deployment,
                scriptId: id,
            });
        },
        showDetail(id){
            this.closeDetail();
            var vm = this;
            this.$nextTick(function(){
                vm.detail.id = id;
            });
        },
        changeType(type){
            this.detail.type = type;
        },
        isLeapYear(year){
            return (year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0));
        },
        getWeekDayIndex(year, month, day){
            var yearsCount = year - this.detail.startData.year - 1,
            leapYearsCount = Math.floor(yearsCount/4) - Math.floor(yearsCount/100) + Math.floor(yearsCount/400);

            // 2001/01/01到前年底的總天數
            daysCount = yearsCount * 365 + leapYearsCount

            // 今年到上個月的總天數
            for(var i = 0; i < month - 1; i ++){
                daysCount += this.detail.days[i];
            }
            // 閏年月份大於2者，+1天
            if(month > 2 && this.isLeapYear(year)) daysCount ++;

            // 加上本月天數
            daysCount += day;
            return daysCount%7;
        },
        getChartLabels(){
            if(this.detail.type == 'week'){
                labels = [];
                d = new Date();
                d.setDate(d.getDate() + this.detail.date.offset);

                labels.push((d.getMonth() + 1)+'/'+d.getDate());
                for(var i = 0; i < 6; i ++){
                    d.setDate(d.getDate() + 1);
                    labels.push((d.getMonth() + 1)+'/'+d.getDate());
                }

                return labels;
            }else{
                return [...Array(24).keys()];
            }
        },
        getChartData(){
            return new Promise((resolve, reject)=>{
                axios
                .get(route('solution.4.deployment.scripts.getChartData', {
                    deployment: args.deployment,
                    script: this.detail.id,
                    type: this.detail.type,
                    start: this.detail.date.year+'-'+('0'+this.detail.date.month).slice(-2)+'-'+('0'+this.detail.date.day).slice(-2),
                }))
                .then(response => {
                    console.log(response.data)
                    resolve(response.data);
                }).catch(error => {
                    console.log(error);
                    reject([]);
                });
            });
        },
        async updateChart(){
            let res = await this.getChartData();

            this.chart.data.labels = this.getChartLabels();
            this.chart.data.datasets[0].data = res;
            this.chart.update();
            this.detail.isLoading = false;
        },
        updateDate(){
            d = new Date();
            d.setDate(d.getDate() + this.detail.date.offset);
            this.detail.date.year = d.getFullYear();
            this.detail.date.month = d.getMonth() + 1;
            this.detail.date.day = d.getDate();
        },
        toTodayOrThisWeek(isCreated){
            this.detail.isLoading = true;
            this.detail.date.offset = this.detail.type == 'week' ? this.detail.thisWeekOffset : 0;
            this.updateDate();
            if(isCreated) this.updateChart();
        },
        last(){
            this.detail.isLoading = true;
            this.detail.date.offset -= this.detail.type == 'week' ? 7 : 1;
            this.updateDate();
            this.updateChart();
        },
        next(){
            this.detail.isLoading = true;
            this.detail.date.offset += this.detail.type == 'week' ? 7 : 1;
            this.updateDate();
            this.updateChart();
        },
        closeDetail(){
            this.detail.id = 0;
            this.detail.type = null;
        },

        // 腳本設定
        toggleActive(id){
            this.is_loading = true;

            axios
            .post(route('solution.4.deployment.scripts.toggleActive', {
                deployment: args.deployment,
                scriptId: id,
            }), {
                _token: args._token,
            })
            .then(response => {
                if(response.data.success){
                    this.getScripts(this.paginator.currentPage);
                }else{
                    this.is_loading = false;
                }
            })
            .catch(error => {
                this.is_loading = false;
            });
        },
        deleteAll(id){
            this.is_loading = true;
            axios({
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': args._token,
                },
                url: route('solution.4.deployment.scripts.deleteAll', {
                    deployment: args.deployment,
                    scriptId: id,
                }),
            })
            .then(response => {
                if(response.data.success){
                    this.getScripts(this.paginator.currentPage);
                }
            })
            .catch(error => {
                this.is_loading = false;
            });
        },

        // category
        setIsCategoryLoading(isLoading){
            this.isCategoryLoading = isLoading;
        },
        changeCategory(id){
            this.category = id;
            this.getScripts(1);
        },
        mouseenter(keyword){
            this.draggable = keyword;
        },
        mouseleave(){
            this.draggable = null;
        },
        dragstart(){
            this.detail.id = 0;
        }
    },
    created(){
        this.getScripts(1);

        d = new Date();
        this.detail.today.year = d.getFullYear();
        this.detail.today.month = d.getMonth() + 1;
        this.detail.today.day = d.getDate();

        this.detail.thisWeekOffset = -this.getWeekDayIndex(this.detail.today.year, this.detail.today.month, this.detail.today.day);
        this.detail.date.offset = this.detail.thisWeekOffset;
        this.updateDate();
    },
    watch: {
        'detail.id': function(newValue){
            if(newValue){
                this.detail.type = 'week';

                var vm = this;
                this.$nextTick(async function(){
                    vm.chart = new Chart('script-chart', {
                        type: 'line',
                        data: {
                            labels: vm.getChartLabels(),
                            datasets: [{
                                data: [0,10,5,15,10,20,25],
                                lineTension: 0.1,
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    gridLines: {
                                        display: true,
                                        lineWidth: 1,
                                        color: '#e3ebf6',
                                        zeroLineWidth: 1,
                                        zeroLineColor: '#e3ebf6',
                                    },
                                    scaleLabel: {
                                        display: true,
                                        labelString: '使用人次',
                                    },
                                    allowDecimals: false,
                                    stacked: true,
                                }],
                                xAxes: [{
                                    stacked: true,
                                }],
                            }
                        }
                    });

                    vm.chart.data.datasets[0].data = await this.getChartData();
                    vm.chart.update();
                    vm.detail.isLoading = false;
                });
            }
        },
        'detail.type': function(newValue, oldValue){
            if(this.detail.id) this.toTodayOrThisWeek(oldValue !== null);
        },
    }
});
