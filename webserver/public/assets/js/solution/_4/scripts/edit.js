disablePageSwitch();

var scriptForm = new Vue({
    el: '#script-form',
    data: {
        submitting: false,

        newMessageType: '',

        form: {
            name: {
                name: args.script.name,
                info: '',
                invalid: false,
            },
            description: {
                description: args.script.description,
                info: '',
                invalid: false,
            },
            active: {
                active: args.script.active,
                info: '',
                invalid: false,
            },
            script: {
                script: {
                    nodes: {
                        nodes: args.script.nodes_form_data,
                    },
                },
            }
        }
    },
    methods: {
        sortNodes(){
            this.form.script.script.nodes.nodes.sort((a, b) => a.start - b.start);
        },
        // updateScriptData(data){
        //     this.$set(this.form.script, 'script', data);
        // },
        AllInvalidToFalse(obj){
            for(key in obj){
                if(key == 'invalid'){
                    obj[key] = false;
                    obj['info'] = '';
                }else if(typeof(obj[key]) === 'object'){
                    this.AllInvalidToFalse(obj[key]);
                }
            }
        },
        buildFormData(formData, data, parentKey) {
            if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                Object.keys(data).forEach(key => {
                    scriptForm.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                });
            } else {
                const value = data == null ? '' : data;
                formData.append(parentKey, value);
            }
        },
        jsonToFormData(data){
            let formData = new FormData();
            this.buildFormData(formData, data, 'form');
            return formData;
        },
        submit(){
            if(!this.submitting){
                this.submitting = true;
                this.AllInvalidToFalse(this.form);

                formData = this.jsonToFormData(this.form);
                formData.append('_token', args._token);

                axios.post(route('solution.4.deployment.scripts.newVersion', [args.deployment, args.script.id]),
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    console.log(response);
                    if(response.data.status == 'success'){
                        window.open(route('solution.4.deployment.scripts.show', [args.deployment, response.data.scriptId]), '_self');
                    }else{
                        this.submitting = false;
                    }
                })
                .catch(error => {
                    console.log(error);

                    var errors = error.response.data.errors;
                    this.submitting = false;
                    var invalid_crawler = null;
                    for(error in errors){
                        console.log(error);

                        // 找出最後一個「.」後的字串
                        var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                        // 如果是value，表示是select，再往前找一個value
                        if(last == 'value'){
                            select_last = error.match(/\.(value\.value)$/);
                            // 如果符合「.value.value」，表示是多層value
                            if(select_last) last = select_last.slice(-1)[0];
                        }

                        // 替換數字成以[]包覆表示
                        error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                        // 如果是area，更新invalid = true
                        if(area = error_ref.match(/^(.*)\.action/)){
                            eval('this.'+area.slice(-1)[0]).invalid = true;
                        }

                        info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                        invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');

                        var message_prefix = invalid_ref.match(/(.*messages\[\d\])/);
                        if(message_prefix) this.$set(eval('this.'+message_prefix[0]), 'invalid', true);

                        var action_prefix = invalid_ref.match(/(.*actions\[\d\])/);
                        if(action_prefix) this.$set(eval('this.'+action_prefix[0]), 'invalid', true);

                        try{
                            this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                            this.$set(eval('this.'+invalid_ref), 'invalid', true);
                        }catch(e){

                        }
                    }
                });
            }
        }
    },
    mounted(){
        this.sortNodes();
    }
});
