var scriptVersion = new Vue({
    el: '#script-version',
    data: {
        version: {
            value: {
                name: 'v'+args.script.version,
                value: args.script.id,
                active: args.script.is_current_version,
            },
            options: args.script.versions,
        }
    },
    watch: {
        'version.value.value': function(newValue){
            window.open(route('solution.4.deployment.scripts.show', [args.deployment, newValue]), '_self');
        }
    }
});

var scriptContent = new Vue({
    el: '#script-content',
    data: {
        script: args.script,
        tagId: args.tagId,
        nodeId: args.nodeId,

        expandScript: args.expandScript,
        flowType: 'users',

        showNodeDetailId: 1,

        display: 'script',
    },
    methods: {
        toggleExpandScript(isExpand){
            this.expandScript = isExpand;
        },
        toggleFlowType(type){
            this.flowType = type;
        },
        changeDisplay(display){
            this.display = display;
        },
        showNodeDetail(nodeId){
            this.showNodeDetailId = nodeId;
        },
        getNodeMessages(){
            nodeFormData = this.script.nodes_form_data.find(node => node.id == this.showNodeDetailId);
            return this.script.nodes.find(node => node.start == nodeFormData.start && node.end == nodeFormData.end).messages;
        },
        getNodeMessagesFormData(){
            return this.script.nodes_form_data.find(node => node.id == this.showNodeDetailId).messages.messages;
        },
        getTagIdByNodeId(id){

        },
        getTagIdByNodeFromDataId(id){
            return this.script.nodes_form_data.find(node => node.id == this.showNodeDetailId).tag;
        },
        getTagUrl(id){
            return route('solution.4.deployment.tags.show', {
                deployment: args.deployment,
                tagId: id,
            });
        },

        // 腳本設定
        toggleActive(id){
            this.is_loading = true;

            axios
            .post(route('solution.4.deployment.scripts.toggleActive', {
                deployment: args.deployment,
                scriptId: id,
            }), {
                _token: args._token,
            })
            .then(response => {
                if(response.data.success){
                    location.reload();
                }else{
                    this.is_loading = false;
                }
            })
            .catch(error => {
                this.is_loading = false;
            });
        },
        switchVersion(id){
            axios
            .post(route('solution.4.deployment.scripts.switchVersion', {
                deployment: args.deployment,
                scriptId: id,
            }), {
                _token: args._token,
            })
            .then(response => {
                if(response.data.success) location.reload();
            })
            .catch(error => {

            });
        },
    },
    created(){
        if(this.nodeId){
            node = this.script.nodes.find(node => node.id == this.nodeId);
            nodeFormData = this.script.nodes_form_data.find(_node => _node.start == node.start && _node.end == node.end);
            this.showNodeDetailId = nodeFormData.id;
        }
    }
});
