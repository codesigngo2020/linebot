disablePageSwitch();

var scriptForm = new Vue({
    el: '#script-form',
    data: {
        submitting: false,

        newMessageType: '',

        form: {
            name: {
                name: '',
                info: '',
                invalid: false,
            },
            description: {
                description: '',
                info: '',
                invalid: false,
            },
            active: {
                active: 1,
                info: '',
                invalid: false,
            },
            script: {
                script: {
                    nodes: {
                        nodes: [
                            {
                                id: 1,
                                start: 1,
                                end: 14,
                                type: 'message',
                                template: {
                                    value: null,
                                    info: '',
                                    invalid: false,
                                },
                                messages: {
                                    messages: [
                                        {
                                            id: '1-1',
                                            type: 'text',
                                            message: {
                                                text: {
                                                    text: '',
                                                    info: '',
                                                    invalid: false,
                                                }
                                            },
                                            info: '',
                                            invalid: false,
                                        },
                                        {
                                            id: '1-2',
                                            type: 'buttons',
                                            message: {
                                                title: {
                                                    title: '',
                                                    info: '',
                                                    invalid: false,
                                                },
                                                text: {
                                                    text: '',
                                                    info: '',
                                                    invalid: false,
                                                },
                                                altText: {
                                                    altText: '',
                                                    info: '',
                                                    invalid: false,
                                                },
                                                image: {
                                                    src: '',
                                                    alt: '',
                                                    file: null,
                                                    info: '',
                                                    invalid: false,
                                                    active: {
                                                        active: true,
                                                        info: '',
                                                        invalid: false,
                                                    },
                                                    ratio: {
                                                        ratio: 'rectangle',
                                                        info: '',
                                                        invalid: false,
                                                    }
                                                },
                                                actions: [
                                                    {
                                                        id: 1,
                                                        type: {
                                                            name: '前往下一個節點',
                                                            value: 'next',
                                                            show: false,
                                                        },
                                                        action: {
                                                            label: {
                                                                label: '',
                                                                info: '',
                                                                invalid: false,
                                                            },
                                                            nextId: {
                                                                nextId: 2,
                                                                info: '',
                                                                invalid: false,
                                                            }
                                                        },
                                                        selected: false,
                                                        invalid: false,
                                                    },
                                                    {
                                                        id: 2,
                                                        type: {
                                                            name: '前往下一個節點',
                                                            value: 'next',
                                                            show: false,
                                                        },
                                                        action: {
                                                            label: {
                                                                label: '',
                                                                info: '',
                                                                invalid: false,
                                                            },
                                                            nextId: {
                                                                nextId: 3,
                                                                info: '',
                                                                invalid: false,
                                                            }
                                                        },
                                                        selected: false,
                                                        invalid: false,
                                                    },
                                                ]                                            },
                                                info: '',
                                                invalid: false,
                                            },
                                        ],
                                    }
                                },
                                {
                                    id: 2,
                                    start: 2,
                                    end: 3,
                                    type: 'message',
                                    template: {
                                        value: null,
                                        info: '',
                                        invalid: false,
                                    },
                                    messages: {
                                        messages: [
                                            {
                                                id: '2-1',
                                                type: 'imagemap',
                                                message: {
                                                    altText: {
                                                        altText: '',
                                                        info: '',
                                                        invalid: false,
                                                    },
                                                    image: {
                                                        src: '',
                                                        alt: '',
                                                        file: null,
                                                        info: '',
                                                        invalid: false,
                                                    },
                                                    show: {
                                                        active: true,
                                                        info: '',
                                                        invalid: false,
                                                    },
                                                    area: {
                                                        area: [
                                                            [
                                                                {
                                                                    id: 1,
                                                                    type: {
                                                                        name: '好友回覆關鍵字',
                                                                        value: 'keyword',
                                                                    },
                                                                    action: {
                                                                        keyword: {
                                                                            value: null,
                                                                            info: '',
                                                                            invalid: false,
                                                                        },
                                                                        text: {
                                                                            text: '',
                                                                            info: '',
                                                                            invalid: false,
                                                                        },
                                                                    },
                                                                    selected: false,
                                                                    invalid: false,
                                                                },
                                                                {
                                                                    id: 2,
                                                                    type: {
                                                                        name: '好友回覆關鍵字',
                                                                        value: 'keyword',
                                                                    },
                                                                    action: {
                                                                        keyword: {
                                                                            value: null,
                                                                            info: '',
                                                                            invalid: false,
                                                                        },
                                                                        text: {
                                                                            text: '',
                                                                            info: '',
                                                                            invalid: false,
                                                                        },
                                                                    },
                                                                    selected: false,
                                                                    invalid: false,
                                                                },
                                                            ],
                                                            [
                                                                {
                                                                    id: 3,
                                                                    type: {
                                                                        name: '好友回覆關鍵字',
                                                                        value: 'keyword',
                                                                    },
                                                                    action: {
                                                                        keyword: {
                                                                            value: null,
                                                                            info: '',
                                                                            invalid: false,
                                                                        },
                                                                        text: {
                                                                            text: '',
                                                                            info: '',
                                                                            invalid: false,
                                                                        },
                                                                    },
                                                                    selected: false,
                                                                    invalid: false,
                                                                },
                                                                {
                                                                    id: 4,
                                                                    type: {
                                                                        name: '好友回覆關鍵字',
                                                                        value: 'keyword',
                                                                    },
                                                                    action: {
                                                                        keyword: {
                                                                            value: null,
                                                                            info: '',
                                                                            invalid: false,
                                                                        },
                                                                        text: {
                                                                            text: '',
                                                                            info: '',
                                                                            invalid: false,
                                                                        },
                                                                    },
                                                                    selected: false,
                                                                    invalid: false,
                                                                },
                                                            ]
                                                        ],
                                                        info: '',
                                                        invalid: false,
                                                    },
                                                },
                                                info: '',
                                                invalid: false,
                                            },
                                        ],
                                    }
                                },
                                {
                                    id: 3,
                                    start: 4,
                                    end: 13,
                                    type: 'message',
                                    template: {
                                        value: null,
                                        info: '',
                                        invalid: false,
                                    },
                                    messages: {
                                        messages: [
                                            {
                                                id: '3-1',
                                                type: 'text',
                                                message: {
                                                    text: {
                                                        text: '',
                                                        info: '',
                                                        invalid: false,
                                                    }
                                                },
                                                info: '',
                                                invalid: false,
                                            },
                                            {
                                                id: '3-2',
                                                type: 'confirm',
                                                message: {
                                                    text: {
                                                        text: '',
                                                        info: '',
                                                        invalid: false,
                                                    },
                                                    altText: {
                                                        altText: '',
                                                        info: '',
                                                        invalid: false,
                                                    },
                                                    actions: [
                                                        {
                                                            id: 1,
                                                            type: {
                                                                name: '前往下一個節點',
                                                                value: 'next',
                                                                show: false,
                                                            },
                                                            action: {
                                                                label: {
                                                                    label: '',
                                                                    info: '',
                                                                    invalid: false,
                                                                },
                                                                nextId: {
                                                                    nextId: 4,
                                                                    info: '',
                                                                    invalid: false,
                                                                }
                                                            },
                                                            selected: false,
                                                            invalid: false,
                                                        },
                                                        {
                                                            id: 2,
                                                            type: {
                                                                name: '前往下一個節點',
                                                                value: 'next',
                                                                show: false,
                                                            },
                                                            action: {
                                                                label: {
                                                                    label: '',
                                                                    info: '',
                                                                    invalid: false,
                                                                },
                                                                nextId: {
                                                                    nextId: 5,
                                                                    info: '',
                                                                    invalid: false,
                                                                }
                                                            },
                                                            selected: false,
                                                            invalid: false,
                                                        },
                                                    ],
                                                },
                                                info: '',
                                                invalid: false,
                                            },
                                            {
                                                id: '3-3',
                                                type: 'carousel',
                                                message: {
                                                    altText: {
                                                        altText: '',
                                                        info: '',
                                                        invalid: false,
                                                    },
                                                    columns: {
                                                        info: '',
                                                        invalid: false,
                                                        columns:[
                                                            {
                                                                id: 1,
                                                                title: {
                                                                    title: '',
                                                                    info: '',
                                                                    invalid: false,
                                                                },
                                                                text: {
                                                                    text: '',
                                                                    info: '',
                                                                    invalid: false,
                                                                },
                                                                image: {
                                                                    src: '',
                                                                    alt: '',
                                                                    file: null,
                                                                    info: '',
                                                                    invalid: false,
                                                                },
                                                                actions: [
                                                                    {
                                                                        id: 1,
                                                                        type: {
                                                                            name: '前往下一個節點',
                                                                            value: 'next',
                                                                            show: false,
                                                                        },
                                                                        action: {
                                                                            label: {
                                                                                label: '',
                                                                                info: '',
                                                                                invalid: false,
                                                                            },
                                                                            nextId: {
                                                                                nextId: 6,
                                                                                info: '',
                                                                                invalid: false,
                                                                            }
                                                                        },
                                                                        selected: false,
                                                                        invalid: false,
                                                                    },
                                                                ],
                                                            },
                                                            {
                                                                id: 2,
                                                                title: {
                                                                    title: '',
                                                                    info: '',
                                                                    invalid: false,
                                                                },
                                                                text: {
                                                                    text: '',
                                                                    info: '',
                                                                    invalid: false,
                                                                },
                                                                image: {
                                                                    src: '',
                                                                    alt: '',
                                                                    file: null,
                                                                    info: '',
                                                                    invalid: false,
                                                                },
                                                                actions: [
                                                                    {
                                                                        id: 2,
                                                                        type: {
                                                                            name: '前往下一個節點',
                                                                            value: 'next',
                                                                            show: false,
                                                                        },
                                                                        action: {
                                                                            label: {
                                                                                label: '',
                                                                                info: '',
                                                                                invalid: false,
                                                                            },
                                                                            nextId: {
                                                                                nextId: 7,
                                                                                info: '',
                                                                                invalid: false,
                                                                            }
                                                                        },
                                                                        selected: false,
                                                                        invalid: false,
                                                                    },
                                                                ],
                                                            }
                                                        ],
                                                    },
                                                    title: {
                                                        active: {
                                                            active: true,
                                                            info: '',
                                                            invalid: false,
                                                        },
                                                    },
                                                    image: {
                                                        active: {
                                                            active: true,
                                                            info: '',
                                                            invalid: false,
                                                        },
                                                        ratio: {
                                                            ratio: 'rectangle',
                                                            info: '',
                                                            invalid: false,
                                                        }
                                                    },
                                                },
                                                info: '',
                                                invalid: false,
                                            },
                                        ],
                                    }
                                },
                                {
                                    id: 4,
                                    start: 5,
                                    end: 6,
                                    type: 'message',
                                    template: {
                                        value: null,
                                        info: '',
                                        invalid: false,
                                    },
                                    messages: {
                                        messages: [
                                            {
                                                id: '4-1',
                                                type: 'text',
                                                message: {
                                                    text: {
                                                        text: '',
                                                        info: '',
                                                        invalid: false,
                                                    }
                                                },
                                                info: '',
                                                invalid: false,
                                            },
                                        ],
                                    }
                                },
                                {
                                    id: 5,
                                    start: 7,
                                    end: 8,
                                    type: 'message',
                                    template: {
                                        value: null,
                                        info: '',
                                        invalid: false,
                                    },
                                    messages: {
                                        messages: [
                                            {
                                                id: '5-1',
                                                type: 'text',
                                                message: {
                                                    text: {
                                                        text: '',
                                                        info: '',
                                                        invalid: false,
                                                    }
                                                },
                                                info: '',
                                                invalid: false,
                                            },
                                        ],
                                    }
                                },
                                {
                                    id: 6,
                                    start: 9,
                                    end: 10,
                                    type: 'message',
                                    template: {
                                        value: null,
                                        info: '',
                                        invalid: false,
                                    },
                                    messages: {
                                        messages: [
                                            {
                                                id: '6-1',
                                                type: 'text',
                                                message: {
                                                    text: {
                                                        text: '',
                                                        info: '',
                                                        invalid: false,
                                                    }
                                                },
                                                info: '',
                                                invalid: false,
                                            },
                                        ],
                                    }
                                },
                                {
                                    id: 7,
                                    start: 11,
                                    end: 12,
                                    type: 'message',
                                    template: {
                                        value: null,
                                        info: '',
                                        invalid: false,
                                    },
                                    messages: {
                                        messages: [
                                            {
                                                id: '7-1',
                                                type: 'text',
                                                message: {
                                                    text: {
                                                        text: '',
                                                        info: '',
                                                        invalid: false,
                                                    }
                                                },
                                                info: '',
                                                invalid: false,
                                            },
                                        ],
                                    }
                                },
                            ],
                        },
                    },
                }
            }
        },
        methods: {
            sortNodes(){
                this.form.script.script.nodes.nodes.sort((a, b) => a.start - b.start);
            },
            // updateScriptData(data){
            //     this.$set(this.form.script, 'script', data);
            // },
            AllInvalidToFalse(obj){
                for(key in obj){
                    if(key == 'invalid'){
                        obj[key] = false;
                        obj['info'] = '';
                    }else if(typeof(obj[key]) === 'object'){
                        this.AllInvalidToFalse(obj[key]);
                    }
                }
            },
            buildFormData(formData, data, parentKey) {
                if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                    Object.keys(data).forEach(key => {
                        scriptForm.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                    });
                } else {
                    const value = data == null ? '' : data;
                    formData.append(parentKey, value);
                }
            },
            jsonToFormData(data){
                let formData = new FormData();
                this.buildFormData(formData, data, 'form');
                return formData;
            },
            submit(){
                if(!this.submitting){
                    this.submitting = true;
                    this.AllInvalidToFalse(this.form);

                    formData = this.jsonToFormData(this.form);
                    formData.append('_token', args._token);

                    axios.post(route('solution.4.deployment.scripts.store', args.deployment),
                    formData,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(response => {
                        console.log(response);
                        if(response.data.status == 'success'){
                            window.open(route('solution.4.deployment.scripts.show', [args.deployment, response.data.scriptId]), '_self');
                        }else{
                            this.submitting = false;
                        }
                    })
                    .catch(error => {
                        console.log(error);

                        var errors = error.response.data.errors;
                        this.submitting = false;
                        var invalid_crawler = null;
                        for(error in errors){
                            console.log(error);

                            // 找出最後一個「.」後的字串
                            var last = error.match(/\.([a-zA-Z0-9]+)$/).slice(-1)[0];
                            // 如果是value，表示是select，再往前找一個value
                            if(last == 'value'){
                                select_last = error.match(/\.(value\.value)$/);
                                // 如果符合「.value.value」，表示是多層value
                                if(select_last) last = select_last.slice(-1)[0];
                            }

                            // 替換數字成以[]包覆表示
                            error_ref = error.replaceAll(/\.([0-9]+)\./g, '[$1].');

                            // 如果是area，更新invalid = true
                            if(area = error_ref.match(/^(.*)\.action/)){
                                eval('this.'+area.slice(-1)[0]).invalid = true;
                            }

                            info_ref = error_ref.replace(new RegExp('.'+last+'$'), '');
                            invalid_ref = error_ref.replace(new RegExp('.'+last+'$'), '');

                            var message_prefix = invalid_ref.match(/(.*messages\[\d\])/);
                            if(message_prefix) this.$set(eval('this.'+message_prefix[0]), 'invalid', true);

                            var action_prefix = invalid_ref.match(/(.*actions\[\d\])/);
                            if(action_prefix) this.$set(eval('this.'+action_prefix[0]), 'invalid', true);

                            try{
                                this.$set(eval('this.'+info_ref), 'info', errors[error][0]);
                                this.$set(eval('this.'+invalid_ref), 'invalid', true);
                            }catch(e){

                            }
                        }
                    });
                }
            }
        },
        mounted(){
            this.sortNodes();
        }
    });
