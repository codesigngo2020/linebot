var new_deployment = new Vue({
  el: '#new_deployment',
})

var deploymentsTable = new Vue({
    el: '#deployments-table',
    data: {
        query: '',
        deployments: args.deployments,
        paginator: args.paginator,
        is_loading: false,
        order: {
            column: 'id',
            dir: 'desc',
        },
    },
    methods: {
        changePage(page){
            this.getDeployments(page);
        },
        search(){
            this.getDeployments(1);
        },
        getDeployments(page){
            this.is_loading = true;
            axios
            .get(route('solution.getDeployments', {
                solution: 4,
                page: page,
                column: this.order.column,
                dir: this.order.dir,
                q: this.query,
            }))
            .then(response => {
                console.log(response.data)
                this.deployments = response.data.deployments;
                this.paginator = response.data.paginator;
                this.is_loading = false;
            }).catch(error => {
                this.is_loading = false;
                console.log(error);
            });
        },
        getUrl(id){
            return route('solution.4.deployment.show', {
                deployment: id,
            });
        },
    },
});
