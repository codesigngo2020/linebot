var deploy_form = new Vue({
  el: '#deploy-form',
  data(){
    return {
      form: {},
    };
  },
  methods: {
    updateFormData(index, data){
      if(index){
        this.$set(this.form, index, data);
      }else{
        this.$set(this, 'form', data);
        for(var i = 1; i <= 3; i ++){
          this.$refs['form'+i].form = Object.assign(this.$refs['form'+i].form, this.form[i]);
        }
      }
    }
  }
});
