var deploy_form = new Vue({
  el: '#deploy-form',
  data(){
    return {
      form: args.form,
    };
  },
  created(){
    delete args.form;
  },
  methods: {
    updateFormData(index, data){
      if(index){
        this.$set(this.form, index, data);
      }else{
        this.$set(this, 'form', data);
        for(var i = 1; i <= 4; i ++){
          if(i != 4){
            this.$refs['form'+i].form = Object.assign(this.$refs['form'+i].form, this.form[i]);
          }
        }
      }
    },
  }
});
