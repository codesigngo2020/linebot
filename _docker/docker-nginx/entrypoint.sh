#!/bin/bash

envsubst '$NGINX_FPM_PORT $NGINX_DOMAIN $NGINX_ROOT $NGINX_FPM_HOST' < /etc/nginx/fpm.tmpl > /etc/nginx/conf.d/default.conf
exec nginx -g "daemon off;"
