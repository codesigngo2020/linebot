server {
    # 預設監聽的 port 為 80
    listen 80;
    listen 443 ssl;
    server_name ${NGINX_DOMAIN};

    # this path MUST be exactly the same as your path in FPM even if it doesn't
    # exist here. Nginx will send the full path of the file to render to fpm.
    root ${NGINX_ROOT};

    gzip on;
    gzip_disable "msie6";
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_types text/html text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype image/svg+xml image/x-icon image/jpeg;

    ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
    ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;

    location / {
        try_files $uri /index.php$is_args$args;
    }

    client_max_body_size 2m;

    # pass the PHP scripts to FastCGI server
    # listening on 127.0.0.1:9000
    location ~ ^/.+\.php(/|$) {
        fastcgi_pass ${NGINX_FPM_HOST}:${NGINX_FPM_PORT};
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }
}
