package main

import (
	"websocket/controllers/message"
	"websocket/controllers/websocket"
	"websocket/database"
	"websocket/engine"

	"github.com/kataras/iris"
)

func main() {

	/* ========== database初始化 ========== */

	db, _ := database.Connect()
	defer db.Close()

	// 建立engine
	e := engine.ConcurrendEngine{
		RequestWorkersCount: 10,
	}
	e.Run()

	/* ========== 建立server ==========*/

	app := iris.New()

	// websocket route
	app.Get("/solution/{solutionID:int min(1)}/deployment/{deploymentID:int min(1)}/websocket", websocket.Websocket)
	app.Post("/solution/{solutionID:int min(1)}/deployment/{deploymentID:int min(1)}/message", message.Message)

	app.Run(iris.Addr(":8080"), iris.WithoutServerError(iris.ErrServerClosed))
}
