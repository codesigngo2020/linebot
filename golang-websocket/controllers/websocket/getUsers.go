package websocket

import (
	"log"
	"strconv"
	"websocket/database"
	typeUser "websocket/types/user"
	typeWebsocket "websocket/types/websocket"
	"websocket/types/websocket/response"
)

/* ========== websocket 請求數據 輔助函示 ========== */

func getUsers(deploymentID int, request *typeWebsocket.RawRequest) (response response.GetUsersResponse, err error) {

	response.Type = "getUsers"

	sql := "SELECT `id`, `display_name`, `picture_url`, `status_message`, `is_follow` FROM `deployment_" + strconv.Itoa(deploymentID) + "`.`users` "

	if request.Q != nil || request.After != nil {
		sql += "WHERE "

		if request.Q != nil {
			sql += "(`id` = '" + *request.Q + "' OR `display_name` like '%" + *request.Q + "%' OR `status_message` like '%" + *request.Q + "%') "
			if request.After != nil {
				sql += "AND "
			}
		}

		if request.After != nil {
			sql += "`id` > " + strconv.Itoa(*request.After) + " "
		}
	}

	sql += "LIMIT 21"

	rows, err := database.DB.Query(sql)
	if err != nil {
		log.Println(err)
		return
	}

	defer rows.Close()

	var (
		user    typeUser.User
		counter = 0
	)
	response.HasNext = false

	for rows.Next() {
		counter++
		if counter <= 20 {
			err = rows.Scan(&user.ID, &user.DisplayName, &user.PictureURL, &user.StatusMessage, &user.IsFollow)
			if err != nil {
				log.Println(err)
				continue
			}
			response.Users = append(response.Users, user)
		} else {
			response.HasNext = true
		}
	}
	return
}
