package websocket

import (
	"bytes"
	"encoding/json"
	"log"
	"os"
	"strconv"
	"websocket/engine/workers/request"
	"websocket/engine/workers/request/params"
	typeWebsocket "websocket/types/websocket"
)

/* ========== websocket 請求數據 輔助函示 ========== */

func sendMessage(deploymentID int, _request *typeWebsocket.RawRequest) {

	//  json 合成

	var buf bytes.Buffer
	enc := json.NewEncoder(&buf)
	enc.Encode(&struct {
		UserID     int             `json:"user_id"`
		ReplyToken string          `json:"reply_token"`
		Message    json.RawMessage `json:"message"`
	}{
		UserID:     *_request.UserID,
		ReplyToken: *_request.ReplyToken,
		Message:    *_request.Message,
	})
	log.Println(string(buf.Bytes()))

	// 打 API 請求 webhook 發送訊息

	request.RequestWorkerChan <- request.WorkerParams{
		Deployment: deploymentID,
		Method:     "POST",
		// ReqestURL:  "https://polished-scope-245621.appspot.com/websocket/sendMessage/" + strconv.Itoa(deploymentID),
		ReqestURL:        "https://" + os.Getenv("WEBHOOK_DOMAIN") + "/websocket/sendMessage/" + strconv.Itoa(deploymentID),
		OAuth2Authorized: false,
		HasBody:          true,
		Body:             buf,
		Params: params.SendMessageRequestParams{
			CallBack: false,
		},
	}

	return
}
