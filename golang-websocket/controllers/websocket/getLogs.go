package websocket

import (
	"log"
	"strconv"
	"websocket/database"
	typeLog "websocket/types/log"
	typeWebsocket "websocket/types/websocket"
	"websocket/types/websocket/response"
)

/* ========== websocket 請求數據 輔助函示 ========== */

func getLogs(deploymentID int, request *typeWebsocket.RawRequest) (response response.GetLogsResponse, err error) {

	response.Type = "getLogs"

	sql := "SELECT `id`, `sender`, `recipient`, `messages`, `timestamp` FROM `deployment_" + strconv.Itoa(deploymentID) + "`.`logs` WHERE `user_id` = " + strconv.Itoa(*request.UserID) + " AND `type` = \"message\" "

	if request.Before != nil {
		sql += "AND `id` < " + strconv.Itoa(*request.Before) + " "
	}

	sql += "ORDER BY `id` DESC LIMIT 21"

	rows, err := database.DB.Query(sql)
	if err != nil {
		log.Println(err)
		return
	}

	defer rows.Close()

	var (
		rLog    typeLog.Log
		counter = 0
	)
	response.HasNext = false

	for rows.Next() {
		counter++
		if counter <= 20 {
			err = rows.Scan(&rLog.ID, &rLog.Sender, &rLog.Recipient, &rLog.Messages, &rLog.Timestamp)
			if err != nil {
				log.Println(err)
				continue
			}
			response.Logs = append(response.Logs, rLog)
		} else {
			response.HasNext = true
		}
	}
	return
}
