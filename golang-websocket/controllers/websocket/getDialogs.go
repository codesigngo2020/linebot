package websocket

import (
	"log"
	"strconv"
	"websocket/database"
	typeDialog "websocket/types/dialog"
	typeWebsocket "websocket/types/websocket"
	"websocket/types/websocket/response"
)

/* ========== websocket 請求數據 輔助函示 ========== */

func getDialogs(deploymentID int, request *typeWebsocket.RawRequest) (response response.GetDialogsResponse, err error) {

	response.Type = "getDialogs"
	var sql string

	if request.Q != nil {
		sql = "SELECT `users`.`id`, `users`.`display_name`, `users`.`picture_url`, `users`.`is_follow`, COUNT(DISTINCT(`logs`.`id`)) AS `logs_count` FROM `deployment_12`.`logs` INNER JOIN `deployment_" + strconv.Itoa(deploymentID) + "`.`users` ON `users`.`id` = `logs`.`user_id` WHERE json_extract(`logs`.`messages`, \"$[*].text\") like \"%" + *request.Q + "%\" "

		if request.After != nil {
			sql += "AND `users`.`id` > " + strconv.Itoa(*request.After) + " "
		}

		sql += "GROUP BY `users`.`id`, `users`.`display_name`, `users`.`picture_url`, `users`.`is_follow` ORDER BY `users`.`id` ASC LIMIT 21"
	} else {
		sql = "SELECT `users`.`id`, `users`.`display_name`, `users`.`picture_url`, `users`.`is_follow`, `logs`.`messages` FROM `deployment_" + strconv.Itoa(deploymentID) + "`.`logs` INNER JOIN ( SELECT MAX( `logs`.`id` ) AS `id` FROM `deployment_12`.`logs` WHERE `id` BETWEEN " + strconv.Itoa(*request.Before-1000) + " AND " + strconv.Itoa(*request.Before) + " GROUP BY `user_id` ORDER BY `id` DESC ) AS `latest_dialogs_user` ON `latest_dialogs_user`.`id` = `logs`.`id` INNER JOIN `deployment_" + strconv.Itoa(deploymentID) + "`.`users` ON `users`.`id` = `logs`.`user_id` GROUP BY `users`.`id`, `users`.`display_name`, `users`.`picture_url`, `users`.`is_follow`, `logs`.`id`, `logs`.`messages` ORDER BY `logs`.`id` DESC"
	}

	log.Println(sql)

	rows, err := database.DB.Query(sql)

	if err != nil {
		log.Println(err)
		return
	}

	defer rows.Close()

	var (
		dialog  typeDialog.Dialog
		counter = 0
	)
	response.HasNext = false

	for rows.Next() {
		counter++
		if counter <= 20 {
			if request.Q != nil {
				err = rows.Scan(&dialog.ID, &dialog.DisplayName, &dialog.PictureURL, &dialog.IsFollow, &dialog.LogsCount)
			} else {
				err = rows.Scan(&dialog.ID, &dialog.DisplayName, &dialog.PictureURL, &dialog.IsFollow, &dialog.Messages)
			}
			if err != nil {
				log.Println(err)
				continue
			}
			response.Dialogs = append(response.Dialogs, dialog)
		} else {
			response.HasNext = true
		}
	}
	return
}
