package websocket

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strconv"
	"websocket/database"
	"websocket/engine/workers/request"
	"websocket/engine/workers/request/params"
	typeWebsocket "websocket/types/websocket"

	"github.com/gorilla/websocket"
	"github.com/kataras/iris"
)

var (
	upgrader = &websocket.Upgrader{
		//如果有 cross domain 的需求，可加入這個，不檢查 cross domain
		CheckOrigin: func(r *http.Request) bool { return true },
	}
	getChanIDChan   = make(chan int, 100)
	chanID          = 0
	DeploymentsChan = make(map[int](map[int](chan []byte)))
)

// Websocket controller
func Websocket(ctx iris.Context) {

	/* ========== 檢查並取得 solutionID & deploymentID ========== */

	solutionID, isInt := ctx.Params().GetIntUnslashed("solutionID")
	if !isInt && solutionID != 4 {
		log.Print("solutionID is invalid")
		return
	}

	deploymentID, isInt := ctx.Params().GetIntUnslashed("deploymentID")
	if !isInt {
		log.Print("deploymentID is invalid")
		return
	}

	var ID int
	err := database.DB.QueryRow("SELECT id FROM `myproject`.`deployments` WHERE `deployments`.`id` = " + strconv.Itoa(deploymentID) + " AND `deployments`.`solution_id` = 4 LIMIT 1").Scan(&ID)

	if err != nil {
		log.Println(err)
		return
	}

	/* ========== 啟動 websokect ========== */

	// 建立新的channel
	_, exists := DeploymentsChan[deploymentID]

	requestChanID()
	deploymentChanID := <-getChanIDChan

	if !exists {
		DeploymentsChan[deploymentID] = map[int](chan []byte){deploymentChanID: make(chan []byte)}

		// 打 API 通知 webhook 端 websocket已建立

		request.RequestWorkerChan <- request.WorkerParams{
			Deployment:       deploymentID,
			Method:           "GET",
			ReqestURL:        "https://" + os.Getenv("WEBHOOK_DOMAIN") + "/websocket/active/" + strconv.Itoa(deploymentID),
			OAuth2Authorized: false,
			HasBody:          false,
			Params: params.ActiveWebsocketRequestParams{
				CallBack: false,
			},
		}

	} else {
		DeploymentsChan[deploymentID][deploymentChanID] = make(chan []byte)
	}

	defer removeDeploymentChan(ID, deploymentChanID)

	log.Printf("新建 Deployment #" + strconv.Itoa(deploymentID) + " channel 完成，Channel ID #" + strconv.Itoa(deploymentChanID) + "。")

	// websocket 接收 channel 訊息並傳給 client

	c, err := upgrader.Upgrade(ctx.ResponseWriter(), ctx.Request(), nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()

	done := make(chan struct{})
	/* ========= 收到數據請求後回傳 ========= */

	go func() {
		defer close(done)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Println("readMessage:", err)
				break
			}

			request, err := typeWebsocket.UnmarshalRequestJSON(message)
			if err != nil {
				log.Println("readMessage:", err)
				break
			}

			switch request.Type {
			case typeWebsocket.RequestTypeGetUsers:
				response, err := getUsers(deploymentID, &request)
				if err != nil {
					log.Println("readMessage:", err)
				}
				websocketReply(c, response)

			case typeWebsocket.RequestTypeGetDialogs:
				response, err := getDialogs(deploymentID, &request)
				if err != nil {
					log.Println("readMessage:", err)
				}
				websocketReply(c, response)

			case typeWebsocket.RequestTypeGetLogs:
				response, err := getLogs(deploymentID, &request)
				if err != nil {
					log.Println("readMessage:", err)
				}
				websocketReply(c, response)

			case typeWebsocket.RequestTypeSendMessage:
				sendMessage(deploymentID, &request)

			}
		}
	}()

	/* ========= 收到 webhook 後，回覆數據 ========= */

	for {
		select {
		case <-done:
			return

		case message := <-DeploymentsChan[deploymentID][deploymentChanID]:
			err = c.WriteMessage(1, message)
			if err != nil {
				log.Println("writeMessage error:", err)
				break
			}
		}
	}
}

/* ========== websocket channer 輔助函式 ========== */

func requestChanID() {
	if chanID > 9999 {
		chanID = 1
	} else {
		chanID++
	}

	getChanIDChan <- chanID
}

func removeDeploymentChan(deploymentID int, deploymentChanID int) {
	delete(DeploymentsChan[deploymentID], deploymentChanID)

	if len(DeploymentsChan[deploymentID]) == 0 {
		delete(DeploymentsChan, deploymentID)

		// 打 API 通知 webhook 端 websocket已建立

		request.RequestWorkerChan <- request.WorkerParams{
			Deployment:       deploymentID,
			Method:           "GET",
			ReqestURL:        "https://" + os.Getenv("WEBHOOK_DOMAIN") + "/websocket/inactive/" + strconv.Itoa(deploymentID),
			OAuth2Authorized: false,
			HasBody:          false,
			Params: params.ActiveWebsocketRequestParams{
				CallBack: false,
			},
		}
	}
}

/* ========== websocket 回覆輔助函式 ========== */

func websocketReply(c *websocket.Conn, response interface{}) {
	responseBytes := new(bytes.Buffer)
	json.NewEncoder(responseBytes).Encode(response)

	err := c.WriteMessage(1, responseBytes.Bytes())
	if err != nil {
		log.Println("writeMessage error:", err)
		return
	}
}
