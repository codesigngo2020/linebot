package message

import (
	"encoding/json"
	"log"
	"websocket/controllers/websocket"

	"github.com/kataras/iris"
)

// FromType type
type FromType string

// TargetType constants
const (
	FromTypeBot  FromType = "bot"
	FromTypeUser FromType = "user"
)

// Message controller
func Message(ctx iris.Context) {

	/* ========== 檢查並取得 solutionID & deploymentID ========== */

	solutionID, isInt := ctx.Params().GetIntUnslashed("solutionID")
	if !isInt && solutionID != 4 {
		log.Print("solutionID is invalid")
		return
	}

	deploymentID, isInt := ctx.Params().GetIntUnslashed("deploymentID")
	if !isInt {
		log.Print("deploymentID is invalid")
		return
	}

	/* ========== 由 websocket 發送給 client ========== */

	_, exists := websocket.DeploymentsChan[deploymentID]

	if exists {
		var body json.RawMessage
		if err := ctx.ReadJSON(&body); err != nil {
			ctx.StatusCode(iris.StatusBadRequest)
			ctx.WriteString(err.Error())
			return
		}

		for _, channel := range websocket.DeploymentsChan[deploymentID] {
			channel <- body
		}
	}

}
