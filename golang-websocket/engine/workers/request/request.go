package request

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"

	"golang.org/x/oauth2"
)

// WorkerParams worker參數
type WorkerParams struct {
	Deployment         int
	Method             string
	ReqestURL          string
	OAuth2Authorized   bool
	ChannelAccessToken *string
	HasBody            bool
	Body               bytes.Buffer
	Params             RequestParams
}

// RequestParams interface
type RequestParams interface {
	RequestParams()
	ShouldCallBack() bool
	Callback(body *[]byte)
}

// RequestWorkerChan worker通道
var RequestWorkerChan = make(chan WorkerParams)

// Create 創建任務
func Create() {

	go func() {
		for {
			workerParams := <-RequestWorkerChan

			/* ========== 發送請求 ========== */

			Request(&workerParams)
		}
	}()
}

// Request 執行請求
func Request(workerParams *WorkerParams) (responseBody *[]byte) {
	var client = &http.Client{}

	if workerParams.OAuth2Authorized {
		tokenSource := oauth2.StaticTokenSource(
			&oauth2.Token{AccessToken: *workerParams.ChannelAccessToken},
		)
		client = oauth2.NewClient(oauth2.NoContext, tokenSource)
	}

	var (
		request *http.Request
		err     error
	)

	if workerParams.HasBody {
		request, err = http.NewRequest(workerParams.Method, workerParams.ReqestURL, &workerParams.Body)
		if err != nil {
			log.Println(err)
			return
		}
	} else {
		request, err = http.NewRequest(workerParams.Method, workerParams.ReqestURL, nil)
		if err != nil {
			log.Println(err)
			return
		}
	}
	request.Header.Set("Content-Type", "application/json; charset=UTF-8")

	res, err := client.Do(request)
	if err != nil {
		log.Println(err)
		return
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(string(body))

	if workerParams.Params.ShouldCallBack() {
		workerParams.Params.Callback(&body)
	}

	responseBody = &body
	return
}
