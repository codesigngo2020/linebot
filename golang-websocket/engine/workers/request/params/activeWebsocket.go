package params

/* ========== Websocket ========== */

// ActiveWebsocketRequestParams 請求參數
type ActiveWebsocketRequestParams struct {
	CallBack bool
}

// RequestParams implements RequestParams interface
func (p ActiveWebsocketRequestParams) RequestParams() {}

// ShouldCallBack implements RequestParams interface
func (p ActiveWebsocketRequestParams) ShouldCallBack() bool {
	return p.CallBack
}

// Callback implements RequestParams interface
func (p ActiveWebsocketRequestParams) Callback(body *[]byte) {}
