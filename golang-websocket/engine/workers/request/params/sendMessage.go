package params

/* ========== Websocket ========== */

// SendMessageRequestParams 請求參數
type SendMessageRequestParams struct {
	CallBack bool
}

// RequestParams implements RequestParams interface
func (p SendMessageRequestParams) RequestParams() {}

// ShouldCallBack implements RequestParams interface
func (p SendMessageRequestParams) ShouldCallBack() bool {
	return p.CallBack
}

// Callback implements RequestParams interface
func (p SendMessageRequestParams) Callback(body *[]byte) {}
