package engine

import (
	"websocket/engine/workers/request"
)

// ConcurrendEngine 併發引擎
type ConcurrendEngine struct {
	RequestWorkersCount int // 發送請求併發數量
}

// Run 執行任務
func (e *ConcurrendEngine) Run() {

	// 創建 goruntine
	for i := 0; i < e.RequestWorkersCount; i++ {
		request.Create()
	}
}
