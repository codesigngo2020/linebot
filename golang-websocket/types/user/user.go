package user

// User type
type User struct {
	ID            int    `json:"id"`
	DisplayName   string `json:"display_name"`
	PictureURL    string `json:"picture_url"`
	StatusMessage string `json:"status_message"`
	IsFollow      int8   `json:"is_follow"`
}
