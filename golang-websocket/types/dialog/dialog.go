package dialog

// Dialog type
type Dialog struct {
	ID          int     `json:"id"`
	DisplayName string  `json:"display_name"`
	PictureURL  string  `json:"picture_url"`
	IsFollow    int8    `json:"is_follow"`
	LogsCount   *int    `json:"logs_count,omitempty"`
	Messages    *string `json:"messages,omitempty"`
}
