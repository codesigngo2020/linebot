package log

// Log type
type Log struct {
	ID        int    `json:"id"`
	Sender    string `json:"sender"`
	Recipient string `json:"recipient"`
	Messages  string `json:"messages"`
	Timestamp string `json:"timestamp"`
}
