package websocket

import (
	"encoding/json"
	"log"
)

// RequestType type
type RequestType string

// RequestType constants
const (
	RequestTypeGetUsers    RequestType = "getUsers"
	RequestTypeGetDialogs  RequestType = "getDialogs"
	RequestTypeGetLogs     RequestType = "getLogs"
	RequestTypeSendMessage RequestType = "sendMessage"
)

// RawRequest type
type RawRequest struct {
	Type       RequestType      `json:"type"`
	Q          *string          `json:"q,omitempty"`
	Before     *int             `json:"before,omitempty"`
	After      *int             `json:"after,omitempty"`
	UserID     *int             `json:"userId,omitempty"`
	ReplyToken *string          `json:"replyToken,omitempty"`
	Message    *json.RawMessage `json:"message,omitempty"`
}

// UnmarshalRequestJSON func
func UnmarshalRequestJSON(request []byte) (unmarshaledRawRequest RawRequest, err error) {

	if err = json.Unmarshal(request, &unmarshaledRawRequest); err != nil {
		log.Println(err)
		return
	}

	return
}
