package response

import "websocket/types/user"

// GetUsersResponse type
type GetUsersResponse struct {
	Type    string      `json:"type"`
	Users   []user.User `json:"users"`
	HasNext bool        `json:"hasNext"`
}
