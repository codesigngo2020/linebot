package response

import "websocket/types/dialog"

// GetDialogsResponse type
type GetDialogsResponse struct {
	Type    string          `json:"type"`
	Dialogs []dialog.Dialog `json:"dialogs"`
	HasNext bool            `json:"hasNext"`
}
