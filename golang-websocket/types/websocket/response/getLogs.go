package response

import "websocket/types/log"

// GetLogsResponse type
type GetLogsResponse struct {
	Type    string    `json:"type"`
	Logs    []log.Log `json:"logs"`
	HasNext bool      `json:"hasNext"`
}
