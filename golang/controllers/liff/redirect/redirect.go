package redirect

import (
	"bytes"

	"webhook/templates/liff"

	"github.com/kataras/iris"
)

// Redirect controller
func Redirect(ctx iris.Context) {

	// 驗證request
	if redirectURL := ctx.URLParam("redirectUrl"); redirectURL == "" {
		ctx.WriteString("Invalid request")
		return
	}

	if _, err := ctx.URLParamInt("solution"); err != nil {
		ctx.WriteString("Invalid request")
		return
	}

	if _, err := ctx.URLParamInt("deployment"); err != nil {
		ctx.WriteString("Invalid request")
		return
	}

	if _, err := ctx.URLParamInt("tag"); err != nil {
		ctx.WriteString("Invalid request")
		return
	}

	// 驗證通過
	var viewData = liff.RedirectViewData{
		Host:        ctx.Host(),
		RedirectURL: ctx.URLParam("redirectUrl"),
		Solution:    ctx.URLParam("solution"),
		Deployment:  ctx.URLParam("deployment"),
		Tag:         ctx.URLParam("tag"),
	}

	buffer := new(bytes.Buffer)
	liff.Redirect(viewData, buffer)
	_, err := ctx.Write(buffer.Bytes())
	if err != nil {
		ctx.StatusCode(iris.StatusInternalServerError)
		ctx.WriteString(err.Error())
		return
	}

}
