package do

import (
	"webhook/engine/workers/updateOrCreateUser"
	updateOrCreateUserHelper "webhook/helpers/updateOrCreateUser"
	typeUser "webhook/types/user"

	"github.com/kataras/iris"
)

// FormData 表單資料
type FormData struct {
	UserID     string `form:"userId"`
	Solution   int    `form:"solution"`
	Deployment int    `form:"deployment"`
	Tag        int    `form:"tag"`
}

// Do controller
func Do(ctx iris.Context) {

	// 驗證request
	formData := FormData{}
	err := ctx.ReadForm(&formData)
	if err != nil {
		ctx.StatusCode(iris.StatusInternalServerError)
		ctx.WriteString(err.Error())
		return
	}

	if formData.UserID == "" {
		ctx.WriteString("Invalid request")
		return
	}

	var user *typeUser.User
	u, err := updateOrCreateUserHelper.GetUserByUserID(formData.Deployment, formData.UserID)
	if err == nil {
		user = &u
	}

	// 執行 updateOrCreateUserWorker
	updateOrCreateUser.UpdateOrCreateUserWorkerChan <- updateOrCreateUser.WorkerParams{
		Deployment: formData.Deployment,
		UserID:     formData.UserID,
		User:       user,
		TagID:      &formData.Tag,
	}
}
