package inactive

import (
	"webhook/database"
	"webhook/engine/workers/request"
	"log"
	"strconv"

	"github.com/kataras/iris"
)

// InActive controller
func InActive(ctx iris.Context) {

	deploymentID, isInt := ctx.Params().GetIntUnslashed("deployment")
	if !isInt {
		log.Println("deployment ID is invalid")
		return
	}

	var ID int
	err := database.DB.QueryRow("SELECT `id` FROM `myproject`.`deployments` where id = ? AND solution_id = 4", strconv.Itoa(deploymentID)).Scan(&ID)
	if err != nil {
		log.Println(err)
		return
	}

	request.WebsocketDeploymentsIDChan <- -deploymentID

}
