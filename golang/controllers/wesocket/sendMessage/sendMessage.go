package sendMessage

import (
	"webhook/database"
	"webhook/engine/workers/request"
	"webhook/engine/workers/request/params"
	"webhook/engine/workers/webhook"
	"webhook/types/message"
	typeMessage "webhook/types/message"
	typeUser "webhook/types/user"
	"bytes"
	"encoding/json"
	"log"
	"strconv"

	"github.com/kataras/iris"
)

// FormData 表單資料
type FormData struct {
	UserID     int             `json:"user_id"`
	ReplyToken string          `json:"reply_token"`
	Message    json.RawMessage `json:"message"`
}

// SendMessage controller
func SendMessage(ctx iris.Context) {

	/* ========== 驗證 deployment ID ========== */

	deploymentID, isInt := ctx.Params().GetIntUnslashed("deployment")
	if !isInt && deploymentID > 0 {
		log.Println("deployment ID is invalid")
		return
	}

	var ID int
	err := database.DB.QueryRow("SELECT `id` FROM `myproject`.`deployments` where id = ? AND solution_id = 4", strconv.Itoa(deploymentID)).Scan(&ID)
	if err != nil {
		log.Println(err)
		return
	}

	/* ========== 驗證 post body ========== */

	var formData FormData
	err = ctx.ReadJSON(&formData)
	if err != nil {
		log.Println(err)
		return
	}

	var channelAccessToken string
	err = database.DB.QueryRow("SELECT data->>'$.channelAccessToken' FROM myproject.deployments WHERE id = ? AND solution_id = 4", deploymentID).Scan(&channelAccessToken)
	if err != nil {
		log.Println(err)
		return
	}

	var user typeUser.User
	err = database.DB.QueryRow("SELECT `id`, `display_name`, `picture_url`, `status_message`, `node_id`, `user_id`, `is_follow` FROM `deployment_"+strconv.Itoa(deploymentID)+"`.`users` WHERE `id` = ?", formData.UserID).Scan(&user.ID, &user.DisplayName, &user.PictureURL, &user.StatusMessage, &user.NodeID, &user.UserID, &user.IsFollow)
	if err != nil {
		log.Println(err)
		return
	}

	if formData.ReplyToken != "" {
		webhook.ExecReply(deploymentID, user, formData.Message, formData.ReplyToken, channelAccessToken)

	} else {

		var unmarshaledMessages []typeMessage.Message
		unmarshaledMessages, err := typeMessage.UnmarshalMessagesJSON(formData.Message)
		if err != nil {
			log.Println(err)
			return
		}

		/* ========== 訊息 json 合成 ========== */

		var buf bytes.Buffer
		enc := json.NewEncoder(&buf)
		enc.Encode(&struct {
			To                   string            `json:"to"`
			Messages             []message.Message `json:"messages"`
			NotificationDisabled bool              `json:"notificationDisabled,omitempty"`
		}{
			To:                   user.UserID,
			Messages:             unmarshaledMessages,
			NotificationDisabled: false,
		})

		/* ========== 發送訊息 ========== */

		request.RequestWorkerChan <- request.WorkerParams{
			Type:               "reply",
			Deployment:         deploymentID,
			Method:             "POST",
			ReqestURL:          "https://api.line.me/v2/bot/message/push",
			OAuth2Authorized:   true,
			ChannelAccessToken: &channelAccessToken,
			HasBody:            true,
			Body:               buf, //需要調整
			Params: params.ReplyRequestParams{
				Deployment:         deploymentID,
				ChannelAccessToken: channelAccessToken,
				CallBack:           true,
				User:               user,
				Messages:           formData.Message,
			},
		}
	}

}
