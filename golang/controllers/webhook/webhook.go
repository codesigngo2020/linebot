package webhook

import (
	"crypto/hmac"
	"crypto/sha256"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"log"
	"strconv"
	"webhook/database"
	webhookWroker "webhook/engine/workers/webhook"
	webhookTypes "webhook/types/webhook"

	"github.com/kataras/iris"
)

// Webhook controller
func Webhook(ctx iris.Context) {

	/* ========== 驗證 webhook ========== */

	signature := ctx.GetHeader("X-Line-Signature")
	if signature == "" {
		ctx.StatusCode(iris.StatusUnauthorized)
		log.Println("Empty X-Line-Signature")
		return
	}

	body, err := ctx.GetBody()
	if err != nil {
		ctx.StatusCode(iris.StatusUnauthorized)
		log.Println(err)
		return
	}

	// 取得 channelSecret
	var (
		channelSecret      string
		channelAccessToken string
	)

	err = database.DB.QueryRow("select data->>'$.channelSecret', data->>'$.channelAccessToken' from myproject.deployments where id = ? AND solution_id = 4", ctx.Params().Get("deployment")).Scan(&channelSecret, &channelAccessToken)
	if err != nil {
		ctx.StatusCode(iris.StatusUnauthorized)
		if err == sql.ErrNoRows {
			log.Println("No matching row")
		} else {
			log.Println(err)
		}
		return
	}

	// 驗證 channelAccessToken
	if channelAccessToken == "" {
		ctx.StatusCode(iris.StatusUnauthorized)
		log.Println("Not found access token")
		return
	}

	// 驗證 Signature
	if !validateSignature(&channelSecret, signature, body) {
		ctx.StatusCode(iris.StatusUnauthorized)
		log.Println("Invalid X-Line-Signature")
		return
	}

	/* ========== 取出body中的event ========== */

	request := &struct {
		Events []*webhookTypes.Event `json:"events"`
	}{}

	if err = json.Unmarshal(body, request); err != nil {
		log.Println(err)
		return
	}
	log.Println(*request.Events[0])

	// 執行 webhookWorker
	deployment, err := strconv.Atoi(ctx.Params().Get("deployment"))
	if err != nil {
		log.Println(err)
		return
	}

	for _, event := range request.Events {
		if event.Source.Type == webhookTypes.EventSourceTypeUser && event.Source.UserID == "Udeadbeefdeadbeefdeadbeefdeadbeef" {
			continue
		}
		webhookWroker.WebhookWorkerChan <- webhookWroker.WorkerParams{
			Deployment:         deployment,
			ChannelAccessToken: channelAccessToken,
			Event:              *event,
		}
	}
}

func validateSignature(channelSecret *string, signature string, body []byte) bool {

	decoded, err := base64.StdEncoding.DecodeString(signature)
	if err != nil {
		return false
	}

	hash := hmac.New(sha256.New, []byte(*channelSecret))

	_, err = hash.Write(body)
	if err != nil {
		return false
	}

	return hmac.Equal(decoded, hash.Sum(nil))
}
