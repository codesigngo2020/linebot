package main

import (
	"webhook/controllers/liff/do"
	"webhook/controllers/liff/redirect"
	"webhook/controllers/webhook"
	"webhook/controllers/wesocket/active"
	"webhook/controllers/wesocket/inactive"
	"webhook/controllers/wesocket/sendMessage"
	"webhook/database"
	"webhook/engine"
	"webhook/engine/workers/request"
	"webhook/jobs"

	"github.com/kataras/iris"
)

func main() {

	/* ========== database初始化 ========== */

	db, _ := database.Connect()
	defer db.Close()

	// 建立engine
	e := engine.ConcurrendEngine{
		RequestWorkersCount:            10000,
		UpdateOrCreateUserWorkersCount: 1000,
		WebhookWorkersCount:            1000,
		SendBroadcastWorkersCount:      100,
	}
	e.Run()

	go request.ActiveAndInactiveWebsocket()

	/* ========== 啟動排程 ========== */

	jobs.Cron()
	// sendBroadcasts.Handle()

	/* ========== 建立server ==========*/

	app := iris.New()

	tmpl := iris.HTML("./templates", ".html")
	app.RegisterView(tmpl)

	// liff route
	liffRoute := app.Party("/liff")
	liffRoute.Get("/redirect", redirect.Redirect)
	liffRoute.Post("/do", do.Do)

	// webhook route
	app.Post("/webhook/{deployment:int}", webhook.Webhook)

	// websocket route
	websocketRoute := app.Party("/websocket")
	websocketRoute.Get("/active/{deployment:int}", active.Active)
	websocketRoute.Get("/inactive/{deployment:int}", inactive.InActive)
	websocketRoute.Post("/sendMessage/{deployment:int}", sendMessage.SendMessage)

	app.Run(iris.Addr(":8080"), iris.WithoutServerError(iris.ErrServerClosed))
}
