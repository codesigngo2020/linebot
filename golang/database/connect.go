package database

import (
	"database/sql"
	"log"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

// DB 資料庫連線
var DB *sql.DB

// Connect DB連線
func Connect() (db *sql.DB, err error) {

	DB, err = sql.Open("mysql", os.Getenv("MYSQL_CONNECTION"))
	// DB, err = sql.Open("mysql", "root:@tcp(104.199.166.27:3306)/?charset=utf8&parseTime=true")
	DB.SetMaxOpenConns(30)
	DB.SetMaxIdleConns(10)
	DB.SetConnMaxLifetime(time.Hour)

	if err != nil {
		log.Fatal(err)
	}

	err = DB.Ping()
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Successfully created connection to database.")

	return DB, nil
}
