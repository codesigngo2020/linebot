package jobs

import (
	"webhook/jobs/jobs/sendBroadcasts"

	"github.com/robfig/cron"
)

// Cron 執行排成設定
func Cron() {

	c := cron.New()
	c.AddFunc("0 * * * * *", sendBroadcasts.Handle)
	c.Start()

}
