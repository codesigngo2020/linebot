package sendBroadcasts

import (
	"webhook/database"
	"webhook/engine/workers/sendBroadcast"
	"webhook/types/broadcast"
	"log"
	"strconv"
	"time"
)

// Handle 執行 job
func Handle() {

	/* ========== 取得 solution 4 的 deployment ========== */

	var (
		ID                 int
		channelAccessToken string
	)

	deploymentRows, err := database.DB.Query("SELECT id, data->>'$.channelAccessToken' FROM myproject.deployments where solution_id = 4")
	if err != nil {
		log.Println(err)
		return
	}
	defer deploymentRows.Close()

	for deploymentRows.Next() {
		err = deploymentRows.Scan(&ID, &channelAccessToken)
		if err != nil {
			log.Println(err)
			continue
		}

		/* ========== 取得 broadcast ========== */

		var (
			broadcast broadcast.Broadcast
			loc, _    = time.LoadLocation("Asia/Taipei")
			now       = time.Now().In(loc).Format("2006-01-02 15:04:05")
		)

		broadcastRows, err := database.DB.Query("SELECT `id`, `name`, `target_type`, `groups_id`, `users_id`, `messages` FROM `deployment_"+strconv.Itoa(ID)+"`.`broadcasts` where `sent_at` IS NULL AND `send_status` = 'N' AND `appointed_at` <= ?", now)

		if err != nil {
			log.Println(err)
			continue
		}
		defer broadcastRows.Close()

		/* ========== 發送訊息 ========== */

		for broadcastRows.Next() {
			err = broadcastRows.Scan(&broadcast.ID, &broadcast.Name, &broadcast.TargetType, &broadcast.GroupsID, &broadcast.UsersID, &broadcast.Messages)
			if err != nil {
				log.Println(err)
				continue
			}

			/* ========== 更改狀態為發送中 ========== */

			_, err = database.DB.Exec(
				"UPDATE deployment_"+strconv.Itoa(ID)+".broadcasts SET send_status = 'S', sent_at = ? WHERE id = ?",
				time.Now().Format("2006-01-02 15:04:05"),
				broadcast.ID,
			)
			if err != nil {
				log.Println(err)
				continue
			}

			sendBroadcast.SendBroadcastWorkerChan <- sendBroadcast.WorkerParams{
				Deployment:         ID,
				Broadcast:          broadcast,
				ChannelAccessToken: channelAccessToken,
			}
		}
	}
}
