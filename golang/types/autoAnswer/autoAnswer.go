package autoAnswer

import "encoding/json"

// AutoAnswer struct
type AutoAnswer struct {
	ID       int              `json:"id"`
	Messages *json.RawMessage `json:"messages"`
}
