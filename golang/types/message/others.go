package message

/* ========== 訊息內容 format ========== */

// BaseSize struct
type BaseSize struct {
	Width  int `json:"width"`
	Height int `json:"height"`
}

// Video struct
type Video struct {
	OriginalContentURL string       `json:"originalContentUrl"`
	PreviewImageURL    string       `json:"previewImageUrl"`
	Area               Area         `json:"area"`
	ExternalLink       ExternalLink `json:"externalLink"`
}

// IsExisted 查看 Imagemap 是不是影片
func (v *Video) IsExisted() bool {
	return v.OriginalContentURL != ""
}

// Area struct
type Area struct {
	X      int `json:"x"`
	Y      int `json:"y"`
	Width  int `json:"width"`
	Height int `json:"height"`
}

// ExternalLink struct
type ExternalLink struct {
	LinkURI string `json:"linkUri,omitempty"`
	Label   string `json:"label,omitempty"`
}
