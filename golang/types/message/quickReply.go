package message

// RawQuickReplyItems struct
type RawQuickReplyItems struct {
	Items []RawQuickReplyButton `json:"items"`
}

// RawQuickReplyButton type
type RawQuickReplyButton struct {
	Type     string    `json:"type"`
	ImageURL *string   `json:"imageUrl,emitempty"`
	Action   RawAction `json:"action"`
}

// QuickReplyItems struct
type QuickReplyItems struct {
	Items []QuickReplyButton `json:"items"`
}

// QuickReplyButton type
type QuickReplyButton struct {
	Type     string  `json:"type"`
	ImageURL *string `json:"imageUrl,emitempty"`
	Action   Action  `json:"action"`
}
