package message

// TemplateType type
type TemplateType string

// TemplateType constants
const (
	TemplateTypeButtons       TemplateType = "buttons"
	TemplateTypeConfirm       TemplateType = "confirm"
	TemplateTypeCarousel      TemplateType = "carousel"
	TemplateTypeImageCarousel TemplateType = "image_carousel"
)

// ImageAspectRatioType type
type ImageAspectRatioType string

// ImageAspectRatioType constants
const (
	ImageAspectRatioTypeRectangle ImageAspectRatioType = "rectangle"
	ImageAspectRatioTypeSquare    ImageAspectRatioType = "square"
)

// ImageSizeType type
type ImageSizeType string

// ImageSizeType constants
const (
	ImageSizeTypeCover   ImageSizeType = "cover"
	ImageSizeTypeContain ImageSizeType = "contain"
)

// RawTemplate struct
type RawTemplate struct {
	Type              TemplateType          `json:"type"`
	ThumbnailImageURL *string               `json:"thumbnailImageUrl,omitempty"`
	ImageAspectRatio  *ImageAspectRatioType `json:"imageAspectRatio,omitempty"`
	Title             *string               `json:"title,omitempty"`
	Text              *string               `json:"text,omitempty"`
	DefaultAction     *Action               `json:"defaultAction,omitempty"`
	Actions           *[]RawAction          `json:"actions,omitempty"`
	Columns           *[]RawColumn          `json:"columns,omitempty"`
}

// RawColumn struct
type RawColumn struct {
	ThumbnailImageURL *string      `json:"thumbnailImageUrl,omitempty"`
	Title             *string      `json:"title,omitempty"`
	Text              *string      `json:"text"`
	DefaultAction     *Action      `json:"defaultAction,omitempty"`
	Actions           *[]RawAction `json:"actions,omitempty"`
	ImageURL          *string      `json:"imageUrl,omitempty"`
	Action            *RawAction   `json:"action,omitempty"`
}

/* ========== Template format ========== */

// Template interface
type Template interface {
	Template()
}

// ButtonsTemplate struct
type ButtonsTemplate struct {
	Type              TemplateType          `json:"type"`
	ThumbnailImageURL *string               `json:"thumbnailImageUrl,omitempty"`
	ImageAspectRatio  *ImageAspectRatioType `json:"imageAspectRatio,omitempty"`
	Title             string                `json:"title"`
	Text              *string               `json:"text,omitempty"`
	DefaultAction     *Action               `json:"defaultAction,omitempty"`
	Actions           []Action              `json:"actions"`
}

// ConfirmTemplate struct
type ConfirmTemplate struct {
	Type    TemplateType `json:"type"`
	Text    string       `json:"text"`
	Actions []Action     `json:"actions"`
}

// CarouselTemplate struct
type CarouselTemplate struct {
	Type             TemplateType          `json:"type"`
	ImageAspectRatio *ImageAspectRatioType `json:"imageAspectRatio,omitempty"`
	Columns          []CarouselColumn      `json:"columns"`
}

// CarouselColumn struct
type CarouselColumn struct {
	ThumbnailImageURL *string  `json:"thumbnailImageUrl,omitempty"`
	Title             *string  `json:"title,omitempty"`
	Text              string   `json:"text"`
	DefaultAction     *Action  `json:"defaultAction,omitempty"`
	Actions           []Action `json:"actions"`
}

// ImageCarouselTemplate struct
type ImageCarouselTemplate struct {
	Type    TemplateType          `json:"type"`
	Columns []ImageCarouselColumn `json:"columns"`
}

// ImageCarouselColumn struct
type ImageCarouselColumn struct {
	ImageURL string `json:"imageUrl"`
	Action   Action `json:"action"`
}

/* ========== 實現 Template interface ========== */

// Template implements Template interface
func (*ButtonsTemplate) Template() {}

// Template implements Template interface
func (*ConfirmTemplate) Template() {}

// Template implements Template interface
func (*CarouselTemplate) Template() {}

// Template implements Template interface
func (*ImageCarouselTemplate) Template() {}
