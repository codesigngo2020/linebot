package message

// ActionType type
type ActionType string

// ActionType constants
const (
	ActionTypeURI            ActionType = "uri"
	ActionTypeMessage        ActionType = "message"
	ActionTypePostback       ActionType = "postback"
	ActionTypeDatetimePicker ActionType = "datetimepicker"
	ActionTypeCamera         ActionType = "camera"
	ActionTypeCameraRoll     ActionType = "cameraRoll"
	ActionTypeLocation       ActionType = "location"
)

// RawAction struct
type RawAction struct {
	Type  ActionType `json:"type"`
	Label string     `json:"label"`
	URI   string     `json:"uri,omitempty"`
	Text  string     `json:"text,omitempty"`
	Data  string     `json:"data,omitempty"`
}

/* ========== Actions format ========== */

// Action interface
type Action interface {
	Action()
}

// URIAction struct
type URIAction struct {
	Type  ActionType `json:"type"`
	Label string     `json:"label"`
	URI   string     `json:"uri"`
}

// MessageAction struct
type MessageAction struct {
	Type  ActionType `json:"type"`
	Label string     `json:"label"`
	Text  string     `json:"text"`
}

// PostbackAction struct
type PostbackAction struct {
	Type  ActionType `json:"type"`
	Label string     `json:"label"`
	Data  string     `json:"data"`
}

/* ========== 實現 Action interface ========== */

// Action implements Action interface
func (*URIAction) Action() {}

// Action implements Action interface
func (*MessageAction) Action() {}

// Action implements Action interface
func (*PostbackAction) Action() {}
