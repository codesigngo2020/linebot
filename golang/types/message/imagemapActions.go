package message

// ImagemapActionType type
type ImagemapActionType string

// ImagemapActionType constants
const (
	ImagemapActionTypeURI      ImagemapActionType = "uri"
	ImagemapActionTypeMessage  ImagemapActionType = "message"
	ImagemapActionTypePostback ImagemapActionType = "postback"
)

// RawImagemapAction struct
type RawImagemapAction struct {
	Type    ImagemapActionType `json:"type"`
	LinkURI *string            `json:"linkUri,omitempty"`
	Text    *string            `json:"text,omitempty"`
	Area    Area               `json:"area,omitempty"`
	Data    *string            `json:"data,omitempty"`
}

/* ========== ImagemapAction format ========== */

// ImagemapAction interface
type ImagemapAction interface {
	ImagemapAction()
}

// ImagemapURIAction struct
type ImagemapURIAction struct {
	Type    ImagemapActionType `json:"type"`
	LinkURI string             `json:"linkUri"`
	Area    Area               `json:"area"`
}

// ImagemapMessageAction struct
type ImagemapMessageAction struct {
	Type ImagemapActionType `json:"type"`
	Text string             `json:"text"`
	Area Area               `json:"area"`
}

/* ========== 實現 ImagemapAction interface ========== */

// ImagemapAction implements Action interface
func (*ImagemapURIAction) ImagemapAction() {}

// ImagemapAction implements Action interface
func (*ImagemapMessageAction) ImagemapAction() {}
