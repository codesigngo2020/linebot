package message

import (
	"encoding/json"
	"log"
)

// MessageType type
type MessageType string

// MessageType constants
const (
	MessageTypeText     MessageType = "text"
	MessageTypeImage    MessageType = "image"
	MessageTypeVideo    MessageType = "video"
	MessageTypeAudio    MessageType = "audio"
	MessageTypeFile     MessageType = "file"
	MessageTypeLocation MessageType = "location"
	MessageTypeSticker  MessageType = "sticker"
	MessageTypeTemplate MessageType = "template"
	MessageTypeImagemap MessageType = "imagemap"
	MessageTypeFlex     MessageType = "flex"
)

// RawMessage struct
type RawMessage struct {
	Type               MessageType          `json:"type"`
	Text               *string              `json:"text,omitempty"`
	OriginalContentURL *string              `json:"originalContentUrl,omitempty"`
	PreviewImageURL    *string              `json:"previewImageUrl,omitempty"`
	BaseURL            *string              `json:"baseUrl,omitempty"`
	AltText            *string              `json:"altText,omitempty"`
	BaseSize           *BaseSize            `json:"baseSize,omitempty"`
	Video              *Video               `json:"video,omitempty"`
	Actions            *[]RawImagemapAction `json:"actions,omitempty"`
	Template           *RawTemplate         `json:"template,omitempty"`
	QuickReply         *RawQuickReplyItems  `json:"quickReply,emitempty"`
}

// UnmarshalImagemapActionsJSON method of RawMessage
func (m *RawMessage) UnmarshalImagemapActionsJSON() (parsedActions []ImagemapAction) {

	for _, unparseAction := range *m.Actions {
		parsedActions = append(parsedActions, m.UnmarshalImagemapActionJSON(&unparseAction))
	}

	return
}

// UnmarshalImagemapActionJSON method of RawMessage
func (m *RawMessage) UnmarshalImagemapActionJSON(action *RawImagemapAction) (parsedAction ImagemapAction) {
	switch action.Type {
	case ImagemapActionTypeURI:
		parsedAction = &ImagemapURIAction{
			Type:    action.Type,
			LinkURI: *action.LinkURI,
			Area:    action.Area,
		}

	case ImagemapActionTypeMessage:
		parsedAction = &ImagemapMessageAction{
			Type: action.Type,
			Text: *action.Text,
			Area: action.Area,
		}
	}
	return
}

// UnmarshalTemplateJSON method of RawMessage
func (m *RawMessage) UnmarshalTemplateJSON() (parsedTemplate Template) {

	switch m.Template.Type {
	case TemplateTypeButtons:
		parsedTemplate = &ButtonsTemplate{
			Type:              m.Template.Type,
			ThumbnailImageURL: m.Template.ThumbnailImageURL,
			ImageAspectRatio:  m.Template.ImageAspectRatio,
			Title:             *m.Template.Title,
			Text:              m.Template.Text,
			DefaultAction:     m.Template.DefaultAction,
			Actions:           m.UnmarshalActionsJSON(m.Template.Actions),
		}

	case TemplateTypeConfirm:
		parsedTemplate = &ConfirmTemplate{
			Type:    m.Template.Type,
			Text:    *m.Template.Text,
			Actions: m.UnmarshalActionsJSON(m.Template.Actions),
		}

	case TemplateTypeCarousel:
		parsedTemplate = &CarouselTemplate{
			Type:             m.Template.Type,
			ImageAspectRatio: m.Template.ImageAspectRatio,
			Columns:          m.UnmarshalCarouselColumnsJSON(),
		}

	case TemplateTypeImageCarousel:
		parsedTemplate = &ImageCarouselTemplate{
			Type:    m.Template.Type,
			Columns: m.UnmarshalImageCarouselColumnsJSON(),
		}
	}
	return
}

// UnmarshalCarouselColumnsJSON method of RawMessage
func (m *RawMessage) UnmarshalCarouselColumnsJSON() (parsedColumns []CarouselColumn) {

	for _, unparseColumn := range *m.Template.Columns {
		parsedColumns = append(parsedColumns, CarouselColumn{
			ThumbnailImageURL: unparseColumn.ThumbnailImageURL,
			Title:             unparseColumn.Title,
			Text:              *unparseColumn.Text,
			DefaultAction:     unparseColumn.DefaultAction,
			Actions:           m.UnmarshalActionsJSON(unparseColumn.Actions),
		})

	}

	return
}

// UnmarshalImageCarouselColumnsJSON method of RawMessage
func (m *RawMessage) UnmarshalImageCarouselColumnsJSON() (parsedColumns []ImageCarouselColumn) {

	for _, unparseColumn := range *m.Template.Columns {
		parsedColumns = append(parsedColumns, ImageCarouselColumn{
			ImageURL: *unparseColumn.ImageURL,
			Action:   m.UnmarshalActionJSON(unparseColumn.Action),
		})

	}

	return
}

// UnmarshalActionsJSON method of RawMessage
func (m *RawMessage) UnmarshalActionsJSON(actions *[]RawAction) (parsedActions []Action) {

	for _, unparseAction := range *actions {
		parsedActions = append(parsedActions, m.UnmarshalActionJSON(&unparseAction))
	}

	return
}

// UnmarshalActionJSON method of RawMessage
func (m *RawMessage) UnmarshalActionJSON(action *RawAction) (parsedAction Action) {

	switch action.Type {
	case ActionTypeURI:
		parsedAction = &URIAction{
			Type:  action.Type,
			Label: action.Label,
			URI:   action.URI,
		}

	case ActionTypeMessage:
		parsedAction = &MessageAction{
			Type:  action.Type,
			Label: action.Label,
			Text:  action.Text,
		}

	case ActionTypePostback:
		parsedAction = &PostbackAction{
			Type:  action.Type,
			Label: action.Label,
			Data:  action.Data,
		}
	}

	return
}

// UnmarshalQuickReplyJSON method of RawMessage
func (m *RawMessage) UnmarshalQuickReplyJSON() (parsedQuickReply *QuickReplyItems) {

	if m.QuickReply == nil {
		return nil
	}

	parsedQuickReply = &QuickReplyItems{}

	for _, unparseItem := range m.QuickReply.Items {

		parsedQuickReply.Items = append(parsedQuickReply.Items, QuickReplyButton{
			Type:     unparseItem.Type,
			ImageURL: unparseItem.ImageURL,
			Action:   m.UnmarshalActionJSON(&unparseItem.Action),
		})

	}

	return
}

/* ========== Messages format ========== */

// Message interface
type Message interface {
	Message()
}

// TextMessage struct
type TextMessage struct {
	Type            MessageType      `json:"type"`
	Text            string           `json:"text"`
	QuickReplyitems *QuickReplyItems `json:"quickReply,emitempty"`
}

// ImageMessage struct
type ImageMessage struct {
	Type               MessageType      `json:"type"`
	OriginalContentURL string           `json:"originalContentUrl"`
	PreviewImageURL    string           `json:"previewImageUrl"`
	QuickReplyitems    *QuickReplyItems `json:"quickReply,emitempty"`
}

// ImagemapMessage struct
type ImagemapMessage struct {
	Type            MessageType      `json:"type"`
	BaseURL         string           `json:"baseUrl"`
	AltText         string           `json:"altText"`
	BaseSize        BaseSize         `json:"baseSize"`
	Video           *Video           `json:"video,omitempty"`
	Actions         []ImagemapAction `json:"actions"`
	QuickReplyitems *QuickReplyItems `json:"quickReply,emitempty"`
}

// TemplateMessage struct
type TemplateMessage struct {
	Type            MessageType      `json:"type"`
	AltText         string           `json:"altText"`
	Template        Template         `json:"template"`
	QuickReplyitems *QuickReplyItems `json:"quickReply,emitempty"`
}

/* ========== 實現 Message interface ========== */

// Message implements Message interface
func (*TextMessage) Message() {}

// Message implements Message interface
func (*ImageMessage) Message() {}

// Message implements Message interface
func (*ImagemapMessage) Message() {}

// Message implements Message interface
func (*TemplateMessage) Message() {}

/* ========== 解析資料庫中的 messages ========== */

// UnmarshalMessagesJSON func
func UnmarshalMessagesJSON(messages []byte) (parsedMessages []Message, err error) {

	var unparseMessages []RawMessage
	if err = json.Unmarshal(messages, &unparseMessages); err != nil {
		log.Println(err)
		return
	}

	for _, unparseMessage := range unparseMessages {

		switch unparseMessage.Type {
		case MessageTypeText:
			parsedMessages = append(parsedMessages, &TextMessage{
				Type:            unparseMessage.Type,
				Text:            *unparseMessage.Text,
				QuickReplyitems: unparseMessage.UnmarshalQuickReplyJSON(),
			})

		case MessageTypeImage:
			parsedMessages = append(parsedMessages, &ImageMessage{
				Type:               unparseMessage.Type,
				OriginalContentURL: *unparseMessage.OriginalContentURL,
				PreviewImageURL:    *unparseMessage.PreviewImageURL,
			})

		case MessageTypeImagemap:
			parsedMessages = append(parsedMessages, &ImagemapMessage{
				Type:     unparseMessage.Type,
				BaseURL:  *unparseMessage.BaseURL,
				AltText:  *unparseMessage.AltText,
				BaseSize: *unparseMessage.BaseSize,
				Video:    unparseMessage.Video,
				Actions:  unparseMessage.UnmarshalImagemapActionsJSON(),
			})

		case MessageTypeTemplate:
			parsedMessages = append(parsedMessages, &TemplateMessage{
				Type:     unparseMessage.Type,
				AltText:  *unparseMessage.AltText,
				Template: unparseMessage.UnmarshalTemplateJSON(),
			})
		}

	}
	return
}
