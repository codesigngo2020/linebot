package broadcast

import (
	"encoding/json"
)

// TargetType type
type TargetType string

// TargetType constants
const (
	TargetTypeAll    TargetType = "all"
	TargetTypeGroups TargetType = "groups"
	TargetTypeUsers  TargetType = "users"
)

// Broadcast struct
type Broadcast struct {
	ID         int              `json:"id"`
	Name       string           `json:"name"`
	TargetType TargetType       `json:"target_type"`
	GroupsID   *json.RawMessage `json:"groups_id,emitempty"`
	UsersID    *json.RawMessage `json:"users_id,emitempty"`
	Messages   json.RawMessage  `json:"messages"`
}
