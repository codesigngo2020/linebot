package node

import (
	"encoding/json"
)

// Node struct
type Node struct {
	ID       int             `json:"id"`
	ScriptID int             `json:"script_id"`
	Start    int             `json:"start"`
	End      int             `json:"end"`
	Messages json.RawMessage `json:"messages"`
}
