package keyword

import "encoding/json"

// TriggerType type
type TriggerType string

// TriggerType constants
const (
	TriggerTypeReply    TriggerType = "reply"
	TriggerTypeScript   TriggerType = "script"
	TriggerTypeRichmenu TriggerType = "richmenu"
)

// Keyword struct
type Keyword struct {
	ID          int              `json:"id"`
	Name        string           `json:"name"`
	TriggerType TriggerType      `json:"trigger_type"`
	ScriptID    *int             `json:"script_id,omitempty"`
	Messages    *json.RawMessage `json:"messages,omitempty"`
	RichmenuID  *int             `json:"richmenu_id,omitempty"`
}
