package updateOrCreateUser

// ProfileResponse api回應
type ProfileResponse struct {
	DisplayName   string
	UserID        string
	PictureURL    string
	StatusMessage string
}

// User 好友資料
type User struct {
	DisplayName   string
	UserID        string
	PictureURL    string
	StatusMessage string
}
