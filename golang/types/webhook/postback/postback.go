package postback

import (
	"encoding/json"
	"log"
)

// PostbackDataType type
type PostbackDataType string

// PostbackDataType constants
const (
	PostbackDataTypeScript  PostbackDataType = "script"
	PostbackDataTypeMessage PostbackDataType = "message"
)

// RawPostback type
type RawPostback struct {
	Data   string  `json:"data"`
	Params *Params `json:"params,omitempty"`
}

// Postback type
type Postback struct {
	Data   PostbackData `json:"data"`
	Params *Params      `json:"params,omitempty"`
}

// RawPostbackData type
type RawPostbackData struct {
	Type  PostbackDataType `json:"type"`
	Next  *int             `json:"next"`
	TagID *int             `json:"tag_id,omitempty"`
	Text  *string          `json:"text,omitempty"`
}

// PostbackData interface
type PostbackData interface {
	PostbackData()
}

// PostbackDataScript type
type PostbackDataScript struct {
	Type  PostbackDataType `json:"type"`
	Next  int              `json:"next"`
	TagID *int             `json:"tag_id,omitempty"`
}

// PostbackDataMessage type
type PostbackDataMessage struct {
	Type  PostbackDataType `json:"type"`
	TagID *int             `json:"tag_id,omitempty"`
	Text  string           `json:"text,omitempty"`
}

// PostbackData implements PostbackData interface
func (PostbackDataScript) PostbackData() {}

// PostbackData implements PostbackData interface
func (PostbackDataMessage) PostbackData() {}

// Params type
type Params struct {
	Date     string `json:"date,omitempty"`
	Time     string `json:"time,omitempty"`
	Datetime string `json:"datetime,omitempty"`
}

// UnmarshalPostbackDataJSON func
func UnmarshalPostbackDataJSON(postbackData string) (unmarshaledPostbackData RawPostbackData, err error) {

	if err = json.Unmarshal([]byte(postbackData), &unmarshaledPostbackData); err != nil {
		log.Println(err)
		return
	}

	return
}
