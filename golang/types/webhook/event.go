package webhook

import (
	"webhook/types/webhook/message"
	"webhook/types/webhook/postback"
	"encoding/json"
	"log"
	"time"
)

// EventType type
type EventType string

// EventType constants
const (
	EventTypeMessage  EventType = "message"
	EventTypeFollow   EventType = "follow"
	EventTypeUnfollow EventType = "unfollow"
	EventTypeJoin     EventType = "join"
	EventTypeLeave    EventType = "leave"
	EventTypePostback EventType = "postback"
	EventTypeBeacon   EventType = "beacon"
)

// EventSourceType type
type EventSourceType string

// EventSourceType constants
const (
	EventSourceTypeUser  EventSourceType = "user"
	EventSourceTypeGroup EventSourceType = "group"
	EventSourceTypeRoom  EventSourceType = "room"
)

// EventSource type
type EventSource struct {
	Type    EventSourceType `json:"type"`
	UserID  string          `json:"userId,omitempty"`
	GroupID string          `json:"groupId,omitempty"`
	RoomID  string          `json:"roomId,omitempty"`
}

// Params type
type Params struct {
	Date     string `json:"date,omitempty"`
	Time     string `json:"time,omitempty"`
	Datetime string `json:"datetime,omitempty"`
}

// // BeaconEventType type
// type BeaconEventType string
//
// // BeaconEventType constants
// const (
// 	BeaconEventTypeEnter  BeaconEventType = "enter"
// 	BeaconEventTypeLeave  BeaconEventType = "leave"
// 	BeaconEventTypeBanner BeaconEventType = "banner"
// )
//
// // Beacon type
// type Beacon struct {
// 	Hwid          string
// 	Type          BeaconEventType
// 	DeviceMessage []byte
// }

// Event type
type Event struct {
	ReplyToken string
	Type       EventType
	Timestamp  string
	Source     *EventSource
	Message    message.Message
	Postback   *postback.Postback
	// Beacon   *Beacon
}

type rawEvent struct {
	ReplyToken string                `json:"replyToken,omitempty"`
	Type       EventType             `json:"type"`
	Timestamp  int64                 `json:"timestamp"`
	Source     *EventSource          `json:"source"`
	Message    *rawEventMessage      `json:"message,omitempty"`
	Postback   *postback.RawPostback `json:"postback,omitempty"`
	// Beacon     *rawBeaconEvent  `json:"beacon,omitempty"`
}

type rawEventMessage struct {
	ID              string                  `json:"id"`
	Type            message.MessageType     `json:"type"`
	Text            string                  `json:"text,omitempty"`
	ContentProvider message.ContentProvider `json:"contentProvider,omitempty"`
	Duration        int                     `json:"duration,omitempty"`
	Title           string                  `json:"title,omitempty"`
	Address         string                  `json:"address,omitempty"`
	FileName        string                  `json:"fileName,omitempty"`
	FileSize        int                     `json:"fileSize,omitempty"`
	Latitude        float64                 `json:"latitude,omitempty"`
	Longitude       float64                 `json:"longitude,omitempty"`
	PackageID       string                  `json:"packageId,omitempty"`
	StickerID       string                  `json:"stickerId,omitempty"`
}

// type rawBeaconEvent struct {
// 	Hwid string          `json:"hwid"`
// 	Type BeaconEventType `json:"type"`
// 	DM   string          `json:"dm,omitempty"`
// }

const (
	millisecPerSec     = int64(time.Second / time.Millisecond)
	nanosecPerMillisec = int64(time.Millisecond / time.Nanosecond)
)

// UnmarshalJSON method of Event
func (e *Event) UnmarshalJSON(body []byte) (err error) {
	rawEvent := rawEvent{}
	if err = json.Unmarshal(body, &rawEvent); err != nil {
		return
	}

	e.ReplyToken = rawEvent.ReplyToken
	e.Type = rawEvent.Type
	e.Timestamp = time.Unix(rawEvent.Timestamp/millisecPerSec, (rawEvent.Timestamp%millisecPerSec)*nanosecPerMillisec).Format("2006-01-02 15:04:05")
	e.Source = rawEvent.Source

	switch rawEvent.Type {
	case EventTypeMessage:

		switch rawEvent.Message.Type {
		case message.MessageTypeText:
			e.Message = &message.TextMessage{
				ID:   rawEvent.Message.ID,
				Text: rawEvent.Message.Text,
			}
		case message.MessageTypeImage:
			e.Message = &message.ImageMessage{
				ID:              rawEvent.Message.ID,
				ContentProvider: rawEvent.Message.ContentProvider,
			}
			// case MessageTypeVideo:
			// 	e.Message = &VideoMessage{
			// 		ID: rawEvent.Message.ID,
			// 	}
			// case MessageTypeAudio:
			// 	e.Message = &AudioMessage{
			// 		ID:       rawEvent.Message.ID,
			// 		Duration: rawEvent.Message.Duration,
			// 	}
			// case MessageTypeFile:
			// 	e.Message = &FileMessage{
			// 		ID:       rawEvent.Message.ID,
			// 		FileName: rawEvent.Message.FileName,
			// 		FileSize: rawEvent.Message.FileSize,
			// 	}
			// case MessageTypeLocation:
			// 	e.Message = &LocationMessage{
			// 		ID:        rawEvent.Message.ID,
			// 		Title:     rawEvent.Message.Title,
			// 		Address:   rawEvent.Message.Address,
			// 		Latitude:  rawEvent.Message.Latitude,
			// 		Longitude: rawEvent.Message.Longitude,
			// 	}
			// case MessageTypeSticker:
			// 	e.Message = &StickerMessage{
			// 		ID:        rawEvent.Message.ID,
			// 		PackageID: rawEvent.Message.PackageID,
			// 		StickerID: rawEvent.Message.StickerID,
			// }
		}
	case EventTypePostback:
		postbackData, err := postback.UnmarshalPostbackDataJSON(rawEvent.Postback.Data)
		if err != nil {
			log.Println(err)
			break
		}

		switch postbackData.Type {
		case postback.PostbackDataTypeScript:
			e.Postback = &postback.Postback{
				Data: &postback.PostbackDataScript{
					Type:  postbackData.Type,
					Next:  *postbackData.Next,
					TagID: postbackData.TagID,
				},
				Params: rawEvent.Postback.Params,
			}
		case postback.PostbackDataTypeMessage:
			e.Postback = &postback.Postback{
				Data: &postback.PostbackDataMessage{
					Type:  postbackData.Type,
					TagID: postbackData.TagID,
					Text:  *postbackData.Text,
				},
				Params: rawEvent.Postback.Params,
			}
		}

		// e.Postback = rawEvent.Postback
		// case EventTypeBeacon:
		// 	var deviceMessage []byte
		// 	deviceMessage, err = hex.DecodeString(rawEvent.Beacon.DM)
		// 	if err != nil {
		// 		return
		// 	}
		// 	e.Beacon = &Beacon{
		// 		Hwid:          rawEvent.Beacon.Hwid,
		// 		Type:          rawEvent.Beacon.Type,
		// 		DeviceMessage: deviceMessage,
		// }
	}
	return
}
