package message

import (
	"encoding/json"
)

// MessageType type
type MessageType string

// MessageType constants
const (
	MessageTypeText  MessageType = "text"
	MessageTypeImage MessageType = "image"
	// MessageTypeVideo    MessageType = "video"
	// MessageTypeAudio    MessageType = "audio"
	// MessageTypeFile     MessageType = "file"
	// MessageTypeLocation MessageType = "location"
	// MessageTypeSticker  MessageType = "sticker"
	// MessageTypeTemplate MessageType = "template"
	// MessageTypeImagemap MessageType = "imagemap"
	// MessageTypeFlex     MessageType = "flex"
)

// Message interface
type Message interface {
	Message()
}

// TextMessage type
type TextMessage struct {
	ID   string
	Text string
}

// MarshalJSON method of TextMessage
func (m *TextMessage) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		Type MessageType `json:"type"`
		Text string      `json:"text"`
	}{
		Type: MessageTypeText,
		Text: m.Text,
	})
}

// ImageMessage type
type ImageMessage struct {
	ID              string
	ContentProvider ContentProvider
}

// ContentProviderType type
type ContentProviderType string

// ContentProvider constants
const (
	ContentProviderTypeLine     ContentProviderType = "line"
	ContentProviderTypeExternal ContentProviderType = "external"
)

// ContentProvider type
type ContentProvider struct {
	Type               ContentProviderType `json:"type"`
	OriginalContentURL *string             `json:"originalContentUrl"`
	PreviewImageURL    *string             `json:"previewImageUrl"`
}

// MarshalJSON method of ImageMessage
func (m *ImageMessage) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		Type               MessageType `json:"type"`
		OriginalContentURL string      `json:"originalContentUrl"`
		PreviewImageURL    string      `json:"previewImageUrl"`
	}{
		Type:               MessageTypeImage,
		OriginalContentURL: *m.ContentProvider.OriginalContentURL,
		PreviewImageURL:    *m.ContentProvider.PreviewImageURL,
	})
}

// // VideoMessage type
// type VideoMessage struct {
// 	ID                 string
// 	OriginalContentURL string
// 	PreviewImageURL    string
// }
//
// // MarshalJSON method of VideoMessage
// func (m *VideoMessage) MarshalJSON() ([]byte, error) {
// 	return json.Marshal(&struct {
// 		Type               MessageType `json:"type"`
// 		OriginalContentURL string      `json:"originalContentUrl"`
// 		PreviewImageURL    string      `json:"previewImageUrl"`
// 	}{
// 		Type:               MessageTypeVideo,
// 		OriginalContentURL: m.OriginalContentURL,
// 		PreviewImageURL:    m.PreviewImageURL,
// 	})
// }
//
// // AudioMessage type
// type AudioMessage struct {
// 	ID                 string
// 	OriginalContentURL string
// 	Duration           int
// }
//
// // MarshalJSON method of AudioMessage
// func (m *AudioMessage) MarshalJSON() ([]byte, error) {
// 	return json.Marshal(&struct {
// 		Type               MessageType `json:"type"`
// 		OriginalContentURL string      `json:"originalContentUrl"`
// 		Duration           int         `json:"duration"`
// 	}{
// 		Type:               MessageTypeAudio,
// 		OriginalContentURL: m.OriginalContentURL,
// 		Duration:           m.Duration,
// 	})
// }
//
// // FileMessage type
// type FileMessage struct {
// 	ID       string
// 	FileName string
// 	FileSize int
// }
//
// // LocationMessage type
// type LocationMessage struct {
// 	ID        string
// 	Title     string
// 	Address   string
// 	Latitude  float64
// 	Longitude float64
// }
//
// // MarshalJSON method of LocationMessage
// func (m *LocationMessage) MarshalJSON() ([]byte, error) {
// 	return json.Marshal(&struct {
// 		Type      MessageType `json:"type"`
// 		Title     string      `json:"title"`
// 		Address   string      `json:"address"`
// 		Latitude  float64     `json:"latitude"`
// 		Longitude float64     `json:"longitude"`
// 	}{
// 		Type:      MessageTypeLocation,
// 		Title:     m.Title,
// 		Address:   m.Address,
// 		Latitude:  m.Latitude,
// 		Longitude: m.Longitude,
// 	})
// }
//
// // StickerMessage type
// type StickerMessage struct {
// 	ID        string
// 	PackageID string
// 	StickerID string
// }
//
// // MarshalJSON method of StickerMessage
// func (m *StickerMessage) MarshalJSON() ([]byte, error) {
// 	return json.Marshal(&struct {
// 		Type      MessageType `json:"type"`
// 		PackageID string      `json:"packageId"`
// 		StickerID string      `json:"stickerId"`
// 	}{
// 		Type:      MessageTypeSticker,
// 		PackageID: m.PackageID,
// 		StickerID: m.StickerID,
// 	})
// }

// // Message implements Message interface
// func (*FileMessage) Message() {}

// Message implements Message interface
func (*TextMessage) Message() {}

// Message implements Message interface
func (*ImageMessage) Message() {}

// // Message implements Message interface
// func (*VideoMessage) Message() {}
//
// // Message implements Message interface
// func (*AudioMessage) Message() {}
//
// // Message implements Message interface
// func (*LocationMessage) Message() {}
//
// // Message implements Message interface
// func (*StickerMessage) Message() {}
