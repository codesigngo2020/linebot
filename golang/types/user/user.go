package user

// User type
type User struct {
	ID            int    `json:"id"`
	DisplayName   string `json:"display_name"`
	PictureURL    string `json:"picture_url"`
	StatusMessage string `json:"status_message"`
	IsFollow      bool   `json:"is_follow"`
	NodeID        int    `json:"node_id"`
	UserID        string `json:"user_id"`
}
