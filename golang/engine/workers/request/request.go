package request

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
)

// WorkerParamsType type
type WorkerParamsType string

// WorkerParamsType constants
const (
	WorkerParamsTypeLinkRichmenu       WorkerParamsType = "linkRichmenu"
	WorkerParamsTypeReply              WorkerParamsType = "reply"
	WorkerParamsTypeSendBroadcast      WorkerParamsType = "sendBroadcast"
	WorkerParamsTypeUpdateOrCreateUser WorkerParamsType = "updateOrCreateUser"
	WorkerParamsTypeWebsocket          WorkerParamsType = "websocket"
	WorkerParamsTypeGetContent         WorkerParamsType = "getContent"
)

// WorkerParams worker參數
type WorkerParams struct {
	Type               WorkerParamsType
	Deployment         int
	Method             string
	ReqestURL          string
	OAuth2Authorized   bool
	ChannelAccessToken *string
	HasBody            bool
	Body               bytes.Buffer
	Params             RequestParams
}

// RequestParams interface
type RequestParams interface {
	RequestParams()
	ShouldCallBack() bool
	Callback(body *[]byte)
}

// RequestWorkerChan worker通道
var RequestWorkerChan = make(chan WorkerParams)

// WebsocketDeploymentsID 啟用中的 deployment ID
var WebsocketDeploymentsID = map[int]int8{}

// WebsocketDeploymentsIDChan websocket 啟用/停用 通道
var WebsocketDeploymentsIDChan = make(chan int)

// Create 創建任務
func Create() {

	go func() {
		for {
			workerParams := <-RequestWorkerChan

			switch workerParams.Type {
			case WorkerParamsTypeWebsocket:
				_, exists := WebsocketDeploymentsID[workerParams.Deployment]
				if !exists {
					break
				}
			}

			/* ========== 發送請求 ========== */

			Request(&workerParams)
		}
	}()
}

// Request 執行請求
func Request(workerParams *WorkerParams) (responseBody *[]byte) {

	var (
		client  = &http.Client{}
		request *http.Request
		err     error
	)

	if workerParams.HasBody {
		request, err = http.NewRequest(workerParams.Method, workerParams.ReqestURL, &workerParams.Body)
		if err != nil {
			log.Println(err)
			return
		}
	} else {
		request, err = http.NewRequest(workerParams.Method, workerParams.ReqestURL, nil)
		if err != nil {
			log.Println(err)
			return
		}
	}

	if workerParams.OAuth2Authorized {
		request.Header.Set("Authorization", "Bearer "+*workerParams.ChannelAccessToken)
	}
	request.Header.Set("Content-Type", "application/json; charset=UTF-8;")

	res, err := client.Do(request)
	if err != nil {
		log.Println(err)
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
		return
	}

	if workerParams.Type != WorkerParamsTypeGetContent {
		log.Println("{url: \"" + workerParams.ReqestURL + "\", response: \"" + string(body) + "\"}")
	}

	if workerParams.Params.ShouldCallBack() {
		workerParams.Params.Callback(&body)
	}

	responseBody = &body
	return
}

// ActiveAndInactiveWebsocket 啟用/停用 websocket
func ActiveAndInactiveWebsocket() {
	go func() {
		for {
			deploymentID := <-WebsocketDeploymentsIDChan

			if deploymentID > 0 {
				_, exists := WebsocketDeploymentsID[deploymentID]
				if !exists {
					WebsocketDeploymentsID[deploymentID] = 1
				}
			} else {
				_, exists := WebsocketDeploymentsID[-deploymentID]
				if exists {
					delete(WebsocketDeploymentsID, -deploymentID)
				}
			}
			log.Println(WebsocketDeploymentsID)
		}
	}()
}
