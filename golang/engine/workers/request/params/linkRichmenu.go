package params

import (
	"webhook/database"
	typeRichmenu "webhook/types/richmenu"
	typeUser "webhook/types/user"
	"log"
	"strconv"
)

/* ========== UpdateOrCreateUser ========== */

// LinkRichmenuRequestParams 請求參數
type LinkRichmenuRequestParams struct {
	CallBack   bool
	Deployment int
	User       typeUser.User
	Richmenu   typeRichmenu.Richmenu
}

// RequestParams implements RequestParams interface
func (p LinkRichmenuRequestParams) RequestParams() {}

// ShouldCallBack implements RequestParams interface
func (p LinkRichmenuRequestParams) ShouldCallBack() bool {
	return p.CallBack
}

// Callback implements RequestParams interface
func (p LinkRichmenuRequestParams) Callback(body *[]byte) {
	ParseResponseBodyAndUpdateUserRichmenuID(p.Deployment, p.User, p.Richmenu, body)
}

// ParseResponseBodyAndUpdateUserRichmenuID 解析api回應＆更新好友選單資料
func ParseResponseBodyAndUpdateUserRichmenuID(deployment int, user typeUser.User, richmenu typeRichmenu.Richmenu, body *[]byte) {
	_, err := database.DB.Exec(
		"UPDATE deployment_"+strconv.Itoa(deployment)+".users SET richmenu_id = ? WHERE id = ?",
		richmenu.ID,
		user.ID,
	)
	if err != nil {
		log.Println(err)
	}

	// 新增關鍵字標籤到標籤列表
	var tagID int
	err = database.DB.QueryRow("SELECT id FROM deployment_"+strconv.Itoa(deployment)+".tags WHERE tags.taggable_id = ? AND tags.taggable_type = 'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\Richmenu' AND tags.data->'$.isRelatedToRichmenu' = 1 LIMIT 1", richmenu.ID).Scan(&tagID)

	if err != nil {
		log.Println(err)
	} else {
		_, err = database.DB.Exec(
			"INSERT INTO deployment_"+strconv.Itoa(deployment)+".tag_user (user_id, tag_id) VALUES (?, ?)",
			user.ID,
			tagID,
		)

		if err != nil {
			log.Println(err)
		}
	}
}
