package params

/* ========== Websocket ========== */

// GetContentRequestParams 請求參數
type GetContentRequestParams struct {
	CallBack bool
}

// RequestParams implements RequestParams interface
func (p GetContentRequestParams) RequestParams() {}

// ShouldCallBack implements RequestParams interface
func (p GetContentRequestParams) ShouldCallBack() bool {
	return p.CallBack
}

// Callback implements RequestParams interface
func (p GetContentRequestParams) Callback(body *[]byte) {}
