package params

import (
	"webhook/database"
	"webhook/engine/workers/request"
	"webhook/types/broadcast"
	"webhook/types/user"
	"bytes"
	"encoding/json"
	"log"
	"os"
	"strconv"
	"time"
)

/* ========== SendBroadcast ========== */

// SendBroadcastRequestParams 請求參數
type SendBroadcastRequestParams struct {
	Deployment int
	CallBack   bool
	Broadcast  broadcast.Broadcast
	User       user.User
	Messages   *json.RawMessage
}

// RequestParams implements RequestParams interface
func (p SendBroadcastRequestParams) RequestParams() {}

// ShouldCallBack implements RequestParams interface
func (p SendBroadcastRequestParams) ShouldCallBack() bool {
	return p.CallBack
}

// Callback implements RequestParams interface
func (p SendBroadcastRequestParams) Callback(body *[]byte) {

	/* ========== 取得當前發送狀態 ========== */

	var (
		status    string
		newStatus string
	)

	err := database.DB.QueryRow("SELECT send_status FROM deployment_"+strconv.Itoa(p.Deployment)+".broadcasts WHERE id = ?", p.Broadcast.ID).Scan(&status)
	if err != nil {
		log.Println(err)
		return
	}

	if string(*body) == "{}" {
		loc, _ := time.LoadLocation("Asia/Taipei")
		now := time.Now().In(loc).Format("2006-01-02 15:04:05")

		if p.Broadcast.TargetType == broadcast.TargetTypeGroups || p.Broadcast.TargetType == broadcast.TargetTypeUsers {

			/* ========== log 記錄 ========== */

			res, err := database.DB.Exec(
				"INSERT INTO deployment_"+strconv.Itoa(p.Deployment)+".logs (sender, recipient, user_id, type, messages, timestamp, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
				"bot",
				"user",
				p.User.ID,
				"message",
				string(*p.Messages),
				now,
				now,
				now,
			)
			if err != nil {
				log.Println(err)
			}
			logID, err := res.LastInsertId()
			if err != nil {
				log.Println(err)
				return
			}

			/* ========== 關聯標籤 ========== */

			_, err = database.DB.Exec(
				"INSERT INTO `deployment_"+strconv.Itoa(p.Deployment)+"`.`tag_user` (`user_id`, `tag_id`) SELECT ? AS `user_id`, `tags`.`id` AS `tag_id` FROM `deployment_"+strconv.Itoa(p.Deployment)+"`.`tags` WHERE `tags`.`taggable_id` = ? AND `tags`.`taggable_type` = 'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\Broadcast' AND `tags`.`data`->'$.isRelatedToBroadcast' = 1",
				p.User.ID,
				p.Broadcast.ID,
			)
			if err != nil {
				log.Println(err)
			}

			/* ========== 傳訊給 websocket========== */

			// websocket json 合成

			var buf bytes.Buffer
			enc := json.NewEncoder(&buf)
			enc.Encode(&struct {
				Type      string    `json:"type"`
				LogID     int64     `json:"log_id"`
				Sender    string    `json:"sender"`
				Recipient string    `json:"recipient"`
				User      user.User `json:"user"`
				Messages  string    `json:"messages"`
				Timestamp string    `json:"timestamp"`
			}{
				Type:      "webhook",
				LogID:     logID,
				Sender:    "bot",
				Recipient: "user",
				User:      p.User,
				Messages:  string(*p.Messages),
				Timestamp: now,
			})

			// 發送 websocket
			// deploymentID, err := strconv.Atoi(p.Deployment)

			request.RequestWorkerChan <- request.WorkerParams{
				Type:             "websocket",
				Deployment:       p.Deployment,
				Method:           "POST",
				ReqestURL:        os.Getenv("WEBSOCKET_DOMAIN") + "/solution/4/deployment/" + strconv.Itoa(p.Deployment) + "/message",
				OAuth2Authorized: false,
				HasBody:          true,
				Body:             buf, //需要調整
				Params: WebsocketRequestParams{
					CallBack: false,
				},
			}

		} else {

			/* ========== log 記錄 ========== */

			_, err := database.DB.Exec(
				"INSERT INTO `deployment_"+strconv.Itoa(p.Deployment)+"`.`logs` (`sender`, `recipient`, `user_id`, `type`, `messages`, `timestamp`, `created_at`, `updated_at`) SELECT ? AS `sender`, ? AS `recipient`, `users`.`id` AS `user_id`, ? AS `type`, ? AS `messages`, ? AS `timestamp`, ? AS `created_at`, ? AS `updated_at` FROM `deployment_"+strconv.Itoa(p.Deployment)+"`.`users` WHERE `users`.`is_follow` = 1",
				"bot",
				"user",
				"message",
				string(*p.Messages),
				now,
				now,
				now,
			)
			if err != nil {
				log.Println(err)
			}

			/* ========== 關聯標籤 ========== */

			var tagID int

			err = database.DB.QueryRow("SELECT `tags`.`id` AS `tag_id` FROM `deployment_"+strconv.Itoa(p.Deployment)+"`.`tags` WHERE `tags`.`taggable_id` = ? AND `tags`.`taggable_type` = 'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\Broadcast' AND `tags`.`data` -> '$.isRelatedToBroadcast' = 1 LIMIT 1", p.Broadcast.ID).Scan(&tagID)
			if err != nil {
				log.Println(err)
			} else {
				_, err = database.DB.Exec(
					"INSERT INTO `deployment_"+strconv.Itoa(p.Deployment)+"`.`tag_user` (`user_id`, `tag_id`) SELECT `users`.`id` AS `user_id`, ? AS `tag_id` FROM `deployment_"+strconv.Itoa(p.Deployment)+"`.`users` WHERE `users`.`is_follow` = 1",
					tagID,
				)
				if err != nil {
					log.Println(err)
				}
			}

		}

		if status == "S" {
			newStatus = "F"
		} else if status == "E" {
			newStatus = "P"
		}

	} else {
		if status == "S" {
			newStatus = "E"
		} else if status == "F" {
			newStatus = "P"
		}
	}

	if newStatus != "" {
		_, err = database.DB.Exec(
			"UPDATE deployment_"+strconv.Itoa(p.Deployment)+".broadcasts SET sent_at = ?, send_status = ? where id = ?",
			time.Now().Format("2006-01-02 15:04:05"),
			newStatus,
			p.Broadcast.ID,
		)
		if err != nil {
			log.Println(err)
			return
		}
	}

}
