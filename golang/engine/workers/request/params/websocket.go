package params

/* ========== Websocket ========== */

// WebsocketRequestParams 請求參數
type WebsocketRequestParams struct {
	CallBack bool
}

// RequestParams implements RequestParams interface
func (p WebsocketRequestParams) RequestParams() {}

// ShouldCallBack implements RequestParams interface
func (p WebsocketRequestParams) ShouldCallBack() bool {
	return p.CallBack
}

// Callback implements RequestParams interface
func (p WebsocketRequestParams) Callback(body *[]byte) {}
