package params

import (
	"webhook/database"
	"webhook/engine/workers/request"
	"webhook/types/message"
	typeMessage "webhook/types/message"
	"webhook/types/user"
	"bytes"
	"encoding/json"
	"log"
	"os"
	"strconv"
	"time"
)

/* ========== Reply ========== */

// ReplyRequestParams 請求參數
type ReplyRequestParams struct {
	Deployment         int
	ChannelAccessToken string
	CallBack           bool
	User               user.User
	Messages           []byte
}

// RequestParams implements RequestParams interface
func (p ReplyRequestParams) RequestParams() {}

// ShouldCallBack implements RequestParams interface
func (p ReplyRequestParams) ShouldCallBack() bool {
	return p.CallBack
}

// Callback implements RequestParams interface
func (p ReplyRequestParams) Callback(body *[]byte) {

	if string(*body) == "{}" {
		loc, _ := time.LoadLocation("Asia/Taipei")
		now := time.Now().In(loc).Format("2006-01-02 15:04:05")

		res, err := database.DB.Exec(
			"INSERT INTO deployment_"+strconv.Itoa(p.Deployment)+".logs (sender, recipient, user_id, type, messages, timestamp, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
			"bot",
			"user",
			p.User.ID,
			"message",
			string(p.Messages),
			now,
			now,
			now,
		)
		if err != nil {
			log.Println(err)
			return
		}
		logID, err := res.LastInsertId()
		if err != nil {
			log.Println(err)
			return
		}

		/* ========== 傳訊給 websocket========== */

		// websocket json 合成

		var buf bytes.Buffer
		enc := json.NewEncoder(&buf)
		enc.Encode(&struct {
			Type      string    `json:"type"`
			LogID     int64     `json:"log_id"`
			Sender    string    `json:"sender"`
			Recipient string    `json:"recipient"`
			User      user.User `json:"user"`
			Messages  string    `json:"messages"`
			Timestamp string    `json:"timestamp"`
		}{
			Type:      "webhook",
			LogID:     logID,
			Sender:    "bot",
			Recipient: "user",
			User:      p.User,
			Messages:  string(p.Messages),
			Timestamp: now,
		})

		// 發送 websocket

		request.RequestWorkerChan <- request.WorkerParams{
			Type:             "websocket",
			Deployment:       p.Deployment,
			Method:           "POST",
			ReqestURL:        os.Getenv("WEBSOCKET_DOMAIN") + "/solution/4/deployment/" + strconv.Itoa(p.Deployment) + "/message",
			OAuth2Authorized: false,
			HasBody:          true,
			Body:             buf, //需要調整
			Params: WebsocketRequestParams{
				CallBack: false,
			},
		}

	} else if string(*body) == "{\"message\":\"Invalid reply token\"}" {

		var unmarshaledMessages []typeMessage.Message
		unmarshaledMessages, err := typeMessage.UnmarshalMessagesJSON(p.Messages)
		if err != nil {
			log.Println(err)
			return
		}

		/* ========== 訊息 json 合成 ========== */

		var buf bytes.Buffer
		enc := json.NewEncoder(&buf)
		enc.Encode(&struct {
			To                   string            `json:"to"`
			Messages             []message.Message `json:"messages"`
			NotificationDisabled bool              `json:"notificationDisabled,omitempty"`
		}{
			To:                   p.User.UserID,
			Messages:             unmarshaledMessages,
			NotificationDisabled: false,
		})

		/* ========== 發送訊息 ========== */

		request.RequestWorkerChan <- request.WorkerParams{
			Type:               "reply",
			Deployment:         p.Deployment,
			Method:             "POST",
			ReqestURL:          "https://api.line.me/v2/bot/message/push",
			OAuth2Authorized:   true,
			ChannelAccessToken: &p.ChannelAccessToken,
			HasBody:            true,
			Body:               buf, //需要調整
			Params: ReplyRequestParams{
				Deployment:         p.Deployment,
				ChannelAccessToken: p.ChannelAccessToken,
				CallBack:           true,
				User:               p.User,
				Messages:           p.Messages,
			},
		}
	}
}
