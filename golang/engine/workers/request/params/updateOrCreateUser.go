package params

import (
	"webhook/database"
	"webhook/types/updateOrCreateUser"
	typeUser "webhook/types/user"
	"encoding/json"
	"log"
	"regexp"
	"strconv"
	"time"
)

/* ========== UpdateOrCreateUser ========== */

// UpdateOrCreateUserRequestParams 請求參數
type UpdateOrCreateUserRequestParams struct {
	CallBack   bool
	Deployment int
	UserID     string
	User       *typeUser.User
}

// UpdateOrCreateUserResponse type
type UpdateOrCreateUserResponse struct {
	User typeUser.User
}

// RequestParams implements RequestParams interface
func (p UpdateOrCreateUserRequestParams) RequestParams() {}

// ShouldCallBack implements RequestParams interface
func (p UpdateOrCreateUserRequestParams) ShouldCallBack() bool {
	return p.CallBack
}

// Callback implements RequestParams interface
func (p UpdateOrCreateUserRequestParams) Callback(body *[]byte) {
	ParseResponseBodyAndUpdateOrCreateUser(p.Deployment, p.UserID, p.User, body)
}

// GetUserByUserID 取得好友
func GetUserByUserID(deployment int, userID string) (user typeUser.User, err error) {
	err = database.DB.QueryRow("SELECT `id`, `display_name`, `picture_url`, `status_message`, `node_id`, `user_id`, `is_follow` FROM `deployment_"+strconv.Itoa(deployment)+"`.`users` WHERE `user_id` = ? LIMIT 1", userID).Scan(&user.ID, &user.DisplayName, &user.PictureURL, &user.StatusMessage, &user.NodeID, &user.UserID, &user.IsFollow)
	return
}

// ParseResponseBodyAndUpdateOrCreateUser 解析api回應＆新增/更新好友
func ParseResponseBodyAndUpdateOrCreateUser(deployment int, userID string, paramsUser *typeUser.User, body *[]byte) (user *typeUser.User) {

	// 解析api回應
	var profileResponse updateOrCreateUser.ProfileResponse
	err := json.Unmarshal(*body, &profileResponse)

	if err != nil {
		log.Println(err)
		return
	}

	loc, _ := time.LoadLocation("Asia/Taipei")
	now := time.Now().In(loc).Format("2006-01-02 15:04:05")

	if paramsUser != nil { // 好友已經建立

		user = paramsUser

		if !paramsUser.IsFollow {
			var tagID int
			err = database.DB.QueryRow("SELECT `id` FROM `deployment_" + strconv.Itoa(deployment) + "`.`tags` WHERE `tags`.`taggable_type` = 'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\User' AND `tags`.data->'$.type' = 'follow' LIMIT 1").Scan(&tagID)

			if err != nil {
				log.Println(err)
			} else {
				_, err = database.DB.Exec(
					"INSERT INTO deployment_"+strconv.Itoa(deployment)+".tag_user (user_id, tag_id) VALUES (?, ?)",
					paramsUser.ID,
					tagID,
				)

				if err != nil {
					log.Println(err)
				}
			}
		}

		if profileResponse.DisplayName != paramsUser.DisplayName || profileResponse.PictureURL != paramsUser.PictureURL || profileResponse.StatusMessage != paramsUser.StatusMessage || !paramsUser.IsFollow {

			// 更新好友資料
			_, err = database.DB.Exec(
				"UPDATE deployment_"+strconv.Itoa(deployment)+".users SET display_name = ?, user_id = ?, picture_url = ?, status_message = ?, is_follow = ?, updated_at = ? WHERE id = ?",
				profileResponse.DisplayName,
				profileResponse.UserID,
				profileResponse.PictureURL,
				profileResponse.StatusMessage,
				1,
				now,
				paramsUser.ID,
			)
			if err != nil {
				log.Println(err)
				return
			}
		}
	} else { // 好友還沒建立
		res, err := database.DB.Exec(
			"INSERT INTO deployment_"+strconv.Itoa(deployment)+".users (display_name, user_id, picture_url, status_message, is_follow, created_at, updated_at) VALUES(?, ?, ?, ?, ?, ?, ?)",
			profileResponse.DisplayName,
			profileResponse.UserID,
			profileResponse.PictureURL,
			profileResponse.StatusMessage,
			1,
			now,
			now,
		)
		if err != nil { // 新增失敗
			log.Println(err)

			if regexp.MustCompile(`^Error 1062: Duplicate entry`).MatchString(err.Error()) { // 資料重複
				u, err := GetUserByUserID(deployment, userID)

				if err != nil {
					log.Println(err)
					return
				}
				user = &u

			} else {
				return
			}
		} else { // 新增成功

			var tagID int
			err = database.DB.QueryRow("SELECT `id` FROM `deployment_" + strconv.Itoa(deployment) + "`.`tags` WHERE `tags`.`taggable_type` = 'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\User' AND `tags`.data->'$.type' = 'follow' LIMIT 1").Scan(&tagID)

			if err != nil {
				log.Println(err)
				return
			}

			userInsertID, err := res.LastInsertId()
			if err != nil {
				log.Println(err)
				return
			}

			_, err = database.DB.Exec(
				"INSERT INTO deployment_"+strconv.Itoa(deployment)+".tag_user (user_id, tag_id) VALUES (?, ?)",
				userInsertID,
				tagID,
			)

			if err != nil {
				log.Println(err)
				return
			}

			user = &typeUser.User{
				ID:            int(userInsertID),
				DisplayName:   profileResponse.DisplayName,
				PictureURL:    profileResponse.PictureURL,
				StatusMessage: profileResponse.StatusMessage,
				IsFollow:      true,
				UserID:        userID,
			}

			return
		}
	}

	return
}
