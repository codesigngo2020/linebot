package updateOrCreateUser

import (
	"webhook/database"
	"webhook/helpers/updateOrCreateUser"
	typeUser "webhook/types/user"
	"log"
	"strconv"
)

// WorkerParams worker參數
type WorkerParams struct {
	Deployment int
	UserID     string
	User       *typeUser.User
	TagID      *int
}

// UpdateOrCreateUserWorkerChan worker通道
var UpdateOrCreateUserWorkerChan = make(chan WorkerParams)

// Create 創建任務
func Create() {

	go func() {
		for {
			workerParams := <-UpdateOrCreateUserWorkerChan

			var asyn bool
			if workerParams.TagID == nil {
				asyn = true
			} else {
				asyn = false
			}

			user, err := updateOrCreateUser.UpdateOrCreateUser(updateOrCreateUser.HelperParams{
				Asyn:       asyn,
				Deployment: workerParams.Deployment,
				UserID:     workerParams.UserID,
				User:       workerParams.User,
			})

			if err != nil {
				log.Println(err)
				break
			}

			if workerParams.TagID != nil {
				_, err := database.DB.Exec(
					"INSERT INTO deployment_"+strconv.Itoa(workerParams.Deployment)+".tag_user (user_id, tag_id) VALUES (?, ?)",
					user.ID,
					*workerParams.TagID,
				)

				if err != nil {
					log.Println(err)
					break
				}
			}
		}
	}()
}
