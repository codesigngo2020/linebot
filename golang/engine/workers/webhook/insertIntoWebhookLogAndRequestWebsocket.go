package webhook

import (
	"webhook/database"
	"webhook/engine/workers/request"
	"webhook/engine/workers/request/params"
	typeUser "webhook/types/user"
	"webhook/types/webhook"
	webhookMessage "webhook/types/webhook/message"
	"bytes"
	"encoding/json"
	"log"
	"os"
	"strconv"
	"time"
)

// insertIntoWebhookLogAndRequestWebsocket 寫入 logs table
func insertIntoWebhookLogAndRequestWebsocket(deployment int, user typeUser.User, e *webhook.Event) (err error) {

	loc, _ := time.LoadLocation("Asia/Taipei")
	now := time.Now().In(loc).Format("2006-01-02 15:04:05")

	switch e.Source.Type {
	case webhook.EventSourceTypeUser:

		switch e.Type {
		case webhook.EventTypeMessage:

			var (
				marshaledMessage []byte
				logID            int64
			)

			switch eventMessage := e.Message.(type) {
			case *webhookMessage.TextMessage:
				marshaledMessage, err = eventMessage.MarshalJSON()

			case *webhookMessage.ImageMessage:
				if eventMessage.ContentProvider.OriginalContentURL == nil {
					log.Println("Invalid OriginalContentURL")
					return
				}
				marshaledMessage, err = eventMessage.MarshalJSON()
			}

			if err != nil {
				log.Println(err)
				break
			}

			res, err := database.DB.Exec(
				"INSERT INTO deployment_"+strconv.Itoa(deployment)+".logs (sender, recipient, user_id, type, messages, timestamp, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
				e.Source.Type,
				"bot",
				user.ID,
				e.Type,
				"["+string(marshaledMessage)+"]",
				e.Timestamp,
				now,
				now,
			)
			if err != nil {
				log.Println(err)
				break
			}
			logID, err = res.LastInsertId()
			if err != nil {
				log.Println(err)
				break
			}

			/* ========== 傳訊給 websocket========== */

			// websocket json 合成

			var buf bytes.Buffer
			enc := json.NewEncoder(&buf)
			enc.Encode(&struct {
				Type       string        `json:"type"`
				LogID      int64         `json:"log_id"`
				Sender     string        `json:"sender"`
				Recipient  string        `json:"recipient"`
				User       typeUser.User `json:"user"`
				ReplyToken string        `json:"reply_token"`
				Messages   string        `json:"messages"`
				Timestamp  string        `json:"timestamp"`
			}{
				Type:       "webhook",
				LogID:      logID,
				Sender:     "user",
				Recipient:  "bot",
				User:       user,
				ReplyToken: e.ReplyToken,
				Messages:   "[" + string(marshaledMessage) + "]",
				Timestamp:  e.Timestamp,
			})

			// 發送 websocket

			request.RequestWorkerChan <- request.WorkerParams{
				Type:             "websocket",
				Deployment:       deployment,
				Method:           "POST",
				ReqestURL:        os.Getenv("WEBSOCKET_DOMAIN") + "/solution/4/deployment/" + strconv.Itoa(deployment) + "/message",
				OAuth2Authorized: false,
				HasBody:          true,
				Body:             buf, //需要調整
				Params: params.WebsocketRequestParams{
					CallBack: false,
				},
			}

		}

	case webhook.EventSourceTypeGroup:
	case webhook.EventSourceTypeRoom:
	}

	return
}
