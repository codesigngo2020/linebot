package webhook

import (
	"webhook/database"
	"webhook/types/node"
	typeUser "webhook/types/user"
	"log"
	"strconv"
)

// updateUserNodeID 更新好友節點位置
func updateUserNodeID(deployment int, user typeUser.User, node *node.Node) (err error) {

	// 最後一個節點清除好友節點位置
	nodeID := 0
	if node.End-node.Start != 1 {
		nodeID = node.ID
	}

	_, err = database.DB.Exec(
		"UPDATE deployment_"+strconv.Itoa(deployment)+".users SET node_id = ? WHERE id = ?",
		nodeID,
		user.ID,
	)
	if err != nil {
		log.Println(err)
	}

	return
}
