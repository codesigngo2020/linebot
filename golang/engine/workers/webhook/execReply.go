package webhook

import (
	"webhook/engine/workers/request"
	"webhook/engine/workers/request/params"
	typeMessage "webhook/types/message"
	typeUser "webhook/types/user"
	"bytes"
	"encoding/json"
	"log"
)

// ExecReply 回覆訊息
func ExecReply(deployment int, user typeUser.User, messages []byte, replyToken string, channelAccessToken string) {

	var unmarshaledMessages []typeMessage.Message
	unmarshaledMessages, err := typeMessage.UnmarshalMessagesJSON(messages)
	if err != nil {
		log.Println(err)
		return
	}

	/* ========== 訊息 json 合成 ========== */

	var buf bytes.Buffer
	enc := json.NewEncoder(&buf)
	enc.Encode(&struct {
		ReplyToken           string                `json:"replyToken"`
		Messages             []typeMessage.Message `json:"messages"`
		NotificationDisabled bool                  `json:"notificationDisabled,omitempty"`
	}{
		ReplyToken:           replyToken,
		Messages:             unmarshaledMessages,
		NotificationDisabled: false,
	})

	/* ========== 發送訊息 ========== */

	request.RequestWorkerChan <- request.WorkerParams{
		Type:               "reply",
		Deployment:         deployment,
		Method:             "POST",
		ReqestURL:          "https://api.line.me/v2/bot/message/reply",
		OAuth2Authorized:   true,
		ChannelAccessToken: &channelAccessToken,
		HasBody:            true,
		Body:               buf, //需要調整
		Params: params.ReplyRequestParams{
			Deployment:         deployment,
			ChannelAccessToken: channelAccessToken,
			CallBack:           true,
			User:               user,
			Messages:           messages,
		},
	}
}
