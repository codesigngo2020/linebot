package webhook

import (
	"webhook/database"
	"webhook/types/node"
	typeUser "webhook/types/user"
	"webhook/types/webhook/postback"
	"database/sql"
	"log"
	"strconv"
	"strings"
)

// 執行 postback
func execPostback(user typeUser.User, workerParams *WorkerParams, tagsID *[]int) {

	/* ========== 對 postbackData 類型分流 ========== */

	switch postbackData := workerParams.Event.Postback.Data.(type) {
	case *postback.PostbackDataScript:

		if postbackData.TagID != nil {
			*tagsID = append(*tagsID, *postbackData.TagID)
		}

		/* ========== 找出父節點以及子節點資料 ========== */

		var (
			parentNodeID sql.NullInt64
			nextNode     node.Node
		)
		err := database.DB.QueryRow("SELECT parentNodes.id AS parentNodeId, nextNodes.id AS nextNodeId, nextNodes.script_id AS nextNodeScriptId, nextNodes.start AS nextNodeStart, nextNodes.end AS nextNodeEnd, nextNodes.messages AS nextNodeMessages FROM deployment_"+strconv.Itoa(workerParams.Deployment)+".nodes AS nextNodes LEFT JOIN deployment_"+strconv.Itoa(workerParams.Deployment)+".nodes AS parentNodes ON parentNodes.script_id = nextNodes.script_id AND parentNodes.START < nextNodes.START AND parentNodes.END > nextNodes.END WHERE nextNodes.id = ? ORDER BY parentNodes.START DESC LIMIT 1", postbackData.Next).Scan(&parentNodeID, &nextNode.ID, &nextNode.ScriptID, &nextNode.Start, &nextNode.End, &nextNode.Messages)

		if err != nil {
			log.Println(err)
			return
		}

		/* ========== 比對父節點以及使用者當下節點：是初始節點或跟好友當下位置接續->執行 ========== */

		if !parentNodeID.Valid || user.NodeID == int(parentNodeID.Int64) {

			// 回覆節點訊息
			ExecReply(workerParams.Deployment, user, nextNode.Messages, workerParams.Event.ReplyToken, workerParams.ChannelAccessToken)

			// 更新好友節點位置
			updateUserNodeID(workerParams.Deployment, user, &nextNode)

			// 新增腳本以及第一個節點標籤到標籤列表
			var tagID int
			if !parentNodeID.Valid {
				// 第一個節點，進入新腳本
				rows, err := database.DB.Query(
					"SELECT id FROM deployment_"+strconv.Itoa(workerParams.Deployment)+".tags WHERE (tags.taggable_id = ? AND tags.taggable_type = 'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\Script' AND tags.data->'$.isRelatedToScript' = 1) OR (tags.taggable_id = ? AND tags.taggable_type = 'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\Node' AND tags.data->'$.isRelatedToNode' = 1) LIMIT 2",
					nextNode.ScriptID,
					nextNode.ID,
				)
				if err != nil {
					log.Println(err)
					return
				}
				defer rows.Close()

				for rows.Next() {
					err = rows.Scan(&tagID)
					if err != nil {
						log.Println(err)
						continue
					} else {
						*tagsID = append(*tagsID, tagID)
					}
				}
			} else {
				// 接續腳本
				err = database.DB.QueryRow("SELECT id FROM deployment_"+strconv.Itoa(workerParams.Deployment)+".tags WHERE tags.taggable_id = ? AND tags.taggable_type = 'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\Node' AND tags.data->'$.isRelatedToNode' = 1 LIMIT 1", nextNode.ID).Scan(&tagID)

				if err != nil {
					log.Println(err)
					return
				}
				*tagsID = append(*tagsID, tagID)
			}

		}
	case *postback.PostbackDataMessage:
		if postbackData.TagID != nil {
			*tagsID = append(*tagsID, *postbackData.TagID)
		}
		messages := []byte(`[{"type":"text","text":"` + strings.ReplaceAll(postbackData.Text, "\n", "\\n") + `"}]`)

		// 回覆訊息
		ExecReply(workerParams.Deployment, user, messages, workerParams.Event.ReplyToken, workerParams.ChannelAccessToken)
	}

	return
}
