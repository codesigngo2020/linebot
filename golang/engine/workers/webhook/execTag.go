package webhook

import (
	"webhook/database"
	typeUser "webhook/types/user"
	"log"
	"strconv"
)

// execTag 貼標
func execTag(deployment int, user typeUser.User, tagsID []int) {

	for _, tagID := range tagsID {
		_, err := database.DB.Exec(
			"INSERT INTO deployment_"+strconv.Itoa(deployment)+".tag_user (user_id, tag_id) VALUES (?, ?)",
			user.ID,
			tagID,
		)

		if err != nil {
			log.Println(err)
		}
	}
}
