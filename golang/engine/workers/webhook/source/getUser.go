package source

import (
	updateOrCreateUserWorker "webhook/engine/workers/updateOrCreateUser"
	updateOrCreateUserHelper "webhook/helpers/updateOrCreateUser"
	typeUser "webhook/types/user"
	"webhook/types/webhook"
	"database/sql"
)

// GetUser 取得好友 ID
func GetUser(deployment int, eventType webhook.EventType, userID string, tagsID *[]int) (user *typeUser.User, err error) {

	u, err := updateOrCreateUserHelper.GetUserByUserID(deployment, userID)

	if err == sql.ErrNoRows && eventType != webhook.EventTypeUnfollow { // 沒有好友資料，同步建立好友

		user, err = updateOrCreateUserHelper.UpdateOrCreateUser(updateOrCreateUserHelper.HelperParams{
			Asyn:       false,
			Deployment: deployment,
			UserID:     userID,
			User:       user,
		})

		if err != nil {
			return
		}

	} else if err == nil { // 已經有好友資料，送worker異步更新

		user = &u

		if eventType != webhook.EventTypeUnfollow { // Unfollow 直接忽略

			updateOrCreateUserWorker.UpdateOrCreateUserWorkerChan <- updateOrCreateUserWorker.WorkerParams{
				Deployment: deployment,
				UserID:     userID,
				User:       &u,
			}
		}
	}

	return
}
