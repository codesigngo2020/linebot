package webhook

import (
	"log"
	"strconv"
	"webhook/database"
	"webhook/engine/workers/webhook/source"
	"webhook/types/webhook"
	webhookMessage "webhook/types/webhook/message"
)

// WorkerParams worker參數
type WorkerParams struct {
	Deployment         int
	ChannelAccessToken string
	Event              webhook.Event
}

// WebhookWorkerChan worker通道
var WebhookWorkerChan = make(chan WorkerParams)

// Create 創建任務
func Create() {

	go func() {
		for {
			workerParams := <-WebhookWorkerChan

			var tagsID []int

			/* ========== 取得好友資料 ========== */

			user, err := source.GetUser(workerParams.Deployment, workerParams.Event.Type, workerParams.Event.Source.UserID, &tagsID)
			if err != nil {
				break
			}

			/* ========== Unfollow 直接離開 ========== */

			if workerParams.Event.Type == webhook.EventTypeUnfollow {

				if user != nil { // 資料庫有好友資料
					_, err := database.DB.Exec(
						"UPDATE `deployment_"+strconv.Itoa(workerParams.Deployment)+"`.`users` SET `is_follow` = 0 WHERE id = ?",
						user.ID,
					)
					if err != nil {
						log.Println(err)
					}

					var tagID int
					err = database.DB.QueryRow("SELECT `id` FROM `deployment_" + strconv.Itoa(workerParams.Deployment) + "`.`tags` WHERE `tags`.`taggable_type` = 'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\User' AND `tags`.data->'$.type' = 'unfollow' LIMIT 1").Scan(&tagID)

					if err != nil {
						log.Println(err)
					} else {
						tagsID = append(tagsID, tagID)
					}

				} else {
					break
				}

			} else {

				/* ========== 對使用者分流（user, group, room） ========== */

				switch workerParams.Event.Source.Type {
				case webhook.EventSourceTypeUser:

					/* ========== 對Event類別分流 ========== */

					switch workerParams.Event.Type {
					case webhook.EventTypeMessage:

						/* ========== 對訊息類別分流 ========== */

						switch eventMessage := workerParams.Event.Message.(type) {
						case *webhookMessage.TextMessage:

							/* ========== 判斷 & 執行關鍵字 ========== */

							isKeyword := execKeyword(*user, eventMessage.Text, &workerParams, &tagsID)

							if !isKeyword {
								execAutoAnswer(*user, &workerParams, &tagsID)
							}

						case *webhookMessage.ImageMessage:

							workerParams.Event.Message = execSaveImage(eventMessage, &workerParams)

						}

						/* ========== 寫入訊息記錄 & 傳訊給 websocket ========== */

						insertIntoWebhookLogAndRequestWebsocket(workerParams.Deployment, *user, &workerParams.Event)

					case webhook.EventTypePostback:

						/* ========== 執行 postback ========== */
						execPostback(*user, &workerParams, &tagsID)

					}

				case webhook.EventSourceTypeGroup:
				case webhook.EventSourceTypeRoom:
				}
			}

			/* ========== 好友行為貼標 ========== */

			execTag(workerParams.Deployment, *user, tagsID)

		}
	}()
}
