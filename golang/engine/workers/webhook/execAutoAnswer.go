package webhook

import (
	"webhook/database"
	typeAutoAnswer "webhook/types/autoAnswer"
	typeUser "webhook/types/user"
	"log"
	"strconv"
)

// execAutoAnswer 判斷&執行關鍵字
func execAutoAnswer(user typeUser.User, workerParams *WorkerParams, tagsID *[]int) {

	/* ========== 查詢自動回覆 ========== */

	var autoAnswer typeAutoAnswer.AutoAnswer
	err := database.DB.QueryRow("SELECT `auto_answers`.`id`, `auto_answers`.`messages` FROM `deployment_"+strconv.Itoa(workerParams.Deployment)+"`.`periods` INNER JOIN `deployment_"+strconv.Itoa(workerParams.Deployment)+"`.`auto_answers` ON `auto_answers`.`id` = `periods`.`auto_answer_id` AND `auto_answers`.`active` = 1 WHERE `periods`.`start_date` <= CURRENT_DATE AND (`periods`.`end_date` IS NULL OR `periods`.`end_date` >= CURRENT_DATE) AND `periods`.`start_time` <= CURRENT_TIME AND `periods`.`end_time` >= CURRENT_TIME ORDER BY `periods`.`id` DESC LIMIT 1").Scan(&autoAnswer.ID, &autoAnswer.Messages)

	if err != nil {
		log.Println(err)
		return
	}

	// 新增關鍵字標籤到標籤列表
	var tagID int
	err = database.DB.QueryRow("SELECT id FROM deployment_"+strconv.Itoa(workerParams.Deployment)+".tags WHERE tags.taggable_id = ? AND tags.taggable_type = 'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\AutoAnswer' AND tags.data->'$.isRelatedToAutoAnswer' = 1 LIMIT 1", autoAnswer.ID).Scan(&tagID)

	if err != nil {
		log.Println(err)
	} else {
		*tagsID = append(*tagsID, tagID)
	}

	/* ========== 自動回覆 ========== */

	ExecReply(workerParams.Deployment, user, *autoAnswer.Messages, workerParams.Event.ReplyToken, workerParams.ChannelAccessToken)

	return
}
