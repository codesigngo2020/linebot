package webhook

import (
	"webhook/engine/workers/request"
	"webhook/engine/workers/request/params"
	webhookMessage "webhook/types/webhook/message"
	"context"
	"log"
	"strconv"

	"cloud.google.com/go/storage"
	"github.com/google/uuid"
)

// execSaveImage 儲存圖片
func execSaveImage(eventMessage *webhookMessage.ImageMessage, workerParams *WorkerParams) (parsedEventMessage *webhookMessage.ImageMessage) {

	var fileName string

	/* ========== 請求圖片檔案 ========== */

	if eventMessage.ContentProvider.Type == webhookMessage.ContentProviderTypeLine {

		responseBody := request.Request(&request.WorkerParams{
			Type:               "getContent",
			Deployment:         workerParams.Deployment,
			Method:             "GET",
			ReqestURL:          "https://api.line.me/v2/bot/message/" + eventMessage.ID + "/content",
			OAuth2Authorized:   true,
			ChannelAccessToken: &workerParams.ChannelAccessToken,
			HasBody:            false,
			Params: params.GetContentRequestParams{
				CallBack: false,
			},
		})

		ctx := context.Background()
		client, err := storage.NewClient(ctx)
		if err != nil {
			log.Println(err)
			return
		}

		fileName = uuid.New().String()

		wc := client.Bucket("bittech-poc").Object("deployments/" + strconv.Itoa(workerParams.Deployment) + "/webhook/" + fileName + ".jpeg").NewWriter(ctx)
		wc.ContentType = "image/jpeg"
		wc.ACL = []storage.ACLRule{{Entity: storage.AllUsers, Role: storage.RoleReader}}
		if _, err := wc.Write(*responseBody); err != nil {
			log.Println(err)
			return
		}
		if err := wc.Close(); err != nil {
			log.Println(err)
			return
		}
		fileName = "https://storage.googleapis.com/bittech-poc/deployments/" + strconv.Itoa(workerParams.Deployment) + "/webhook/" + fileName + ".jpeg"
		log.Println("updated image: " + fileName)

	} else if eventMessage.ContentProvider.OriginalContentURL != nil {
		fileName = *eventMessage.ContentProvider.OriginalContentURL
	}

	if fileName != "" {
		parsedEventMessage = &webhookMessage.ImageMessage{
			ID: eventMessage.ID,
			ContentProvider: webhookMessage.ContentProvider{
				Type:               webhookMessage.ContentProviderTypeLine,
				OriginalContentURL: &fileName,
				PreviewImageURL:    &fileName,
			},
		}
	} else {
		parsedEventMessage = nil
	}

	return
}
