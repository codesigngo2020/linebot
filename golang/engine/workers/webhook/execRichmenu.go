package webhook

import (
	"webhook/engine/workers/request"
	"webhook/engine/workers/request/params"
	typeRichmenu "webhook/types/richmenu"
	typeUser "webhook/types/user"
)

// execRichmenu 執行切換選單
func execRichmenu(deployment int, user typeUser.User, richmenu typeRichmenu.Richmenu, channelAccessToken string) {

	request.RequestWorkerChan <- request.WorkerParams{
		Type:               "linkRichmenu",
		Deployment:         deployment,
		Method:             "POST",
		ReqestURL:          "https://api.line.me/v2/bot/user/" + user.UserID + "/richmenu/" + richmenu.RichmenuID,
		OAuth2Authorized:   true,
		ChannelAccessToken: &channelAccessToken,
		HasBody:            false,
		Params: params.LinkRichmenuRequestParams{
			CallBack:   true,
			Deployment: deployment,
			User:       user,
			Richmenu:   richmenu,
		},
	}
}
