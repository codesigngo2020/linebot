package webhook

import (
	"webhook/database"
	typeKeyword "webhook/types/keyword"
	"webhook/types/node"
	"webhook/types/richmenu"
	typeUser "webhook/types/user"
	"log"
	"strconv"
)

// execKeyword 判斷&執行關鍵字
func execKeyword(user typeUser.User, text string, workerParams *WorkerParams, tagsID *[]int) (isKeyword bool) {

	/* ========== 查詢關鍵字 ========== */

	var keyword typeKeyword.Keyword
	err := database.DB.QueryRow("SELECT id, name, trigger_type, script_id, messages, richmenu_id FROM deployment_"+strconv.Itoa(workerParams.Deployment)+".keywords WHERE keywords.name = binary '"+text+"' AND keywords.active = 1 LIMIT 1").Scan(&keyword.ID, &keyword.Name, &keyword.TriggerType, &keyword.ScriptID, &keyword.Messages, &keyword.RichmenuID)

	if err != nil { // 查無關鍵字就離開
		log.Println(err)
		return
	}
	isKeyword = true

	// 新增關鍵字標籤到標籤列表
	var tagID int
	err = database.DB.QueryRow("SELECT id FROM deployment_"+strconv.Itoa(workerParams.Deployment)+".tags WHERE tags.taggable_id = ? AND tags.taggable_type = 'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\Keyword' AND tags.data->'$.isRelatedToKeyword' = 1 LIMIT 1", keyword.ID).Scan(&tagID)

	if err != nil {
		log.Println(err)
	} else {
		*tagsID = append(*tagsID, tagID)
	}

	/* ========== 關鍵字觸發行為分流 ========== */

	switch keyword.TriggerType {
	case typeKeyword.TriggerTypeReply:

		// 回覆訊息
		ExecReply(workerParams.Deployment, user, *keyword.Messages, workerParams.Event.ReplyToken, workerParams.ChannelAccessToken)

	case typeKeyword.TriggerTypeScript:

		var node node.Node
		err := database.DB.QueryRow("SELECT nodes.id, nodes.script_id, nodes.start, nodes.end, nodes.messages FROM deployment_"+strconv.Itoa(workerParams.Deployment)+".scripts AS scripts, deployment_"+strconv.Itoa(workerParams.Deployment)+".nodes AS nodes WHERE scripts.id = "+strconv.Itoa(*keyword.ScriptID)+" AND nodes.start = 1 AND scripts.id = nodes.script_id LIMIT 1").Scan(&node.ID, &node.ScriptID, &node.Start, &node.End, &node.Messages)

		if err != nil {
			log.Println(err)
			return
		}

		// 新增腳本以及第一個節點標籤到標籤列表
		var tagID int
		rows, err := database.DB.Query(
			"SELECT id FROM deployment_"+strconv.Itoa(workerParams.Deployment)+".tags WHERE (tags.taggable_id = ? AND tags.taggable_type = 'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\Script' AND tags.data->'$.isRelatedToScript' = 1) OR (tags.taggable_id = ? AND tags.taggable_type = 'App\\\\Models\\\\Platform\\\\Solution\\\\_4\\\\Node' AND tags.data->'$.isRelatedToNode' = 1) LIMIT 2",
			node.ScriptID,
			node.ID,
		)
		if err != nil {
			log.Println(err)
			return
		}
		defer rows.Close()

		for rows.Next() {
			err = rows.Scan(&tagID)
			if err != nil {
				log.Println(err)
				continue
			} else {
				*tagsID = append(*tagsID, tagID)
			}
		}

		// 回覆節點訊息
		ExecReply(workerParams.Deployment, user, node.Messages, workerParams.Event.ReplyToken, workerParams.ChannelAccessToken)

		// 更新好友節點位置
		updateUserNodeID(workerParams.Deployment, user, &node)

	case typeKeyword.TriggerTypeRichmenu:

		var richmenu richmenu.Richmenu
		err = database.DB.QueryRow("SELECT richmenus.id, richmenus.richmenuId FROM deployment_"+strconv.Itoa(workerParams.Deployment)+".richmenus WHERE richmenus.id = ? LIMIT 1", *keyword.RichmenuID).Scan(&richmenu.ID, &richmenu.RichmenuID)

		if err != nil {
			log.Println(err)
			return
		}

		execRichmenu(workerParams.Deployment, user, richmenu, workerParams.ChannelAccessToken)
	}

	return
}
