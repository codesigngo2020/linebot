package sendBroadcast

import (
	"webhook/database"
	"webhook/engine/workers/request"
	"webhook/engine/workers/request/params"
	"webhook/types/broadcast"
	typeBroadcast "webhook/types/broadcast"
	"webhook/types/message"
	"webhook/types/user"
	"bytes"
	"database/sql"
	"encoding/json"
	"log"
	"strconv"
	"strings"
)

// WorkerParams worker參數
type WorkerParams struct {
	Deployment         int
	Broadcast          typeBroadcast.Broadcast
	ChannelAccessToken string
}

// SendBroadcastWorkerChan worker通道
var SendBroadcastWorkerChan = make(chan WorkerParams)

// Create 創建任務
func Create() {

	go func() {
		for {
			workerParams := <-SendBroadcastWorkerChan
			messages, err := message.UnmarshalMessagesJSON(workerParams.Broadcast.Messages)
			if err != nil {
				log.Println(err)
				continue
			}

			switch workerParams.Broadcast.TargetType {
			case typeBroadcast.TargetTypeAll:

				/* ========== 訊息 json 合成 ========== */

				var buf bytes.Buffer
				enc := json.NewEncoder(&buf)
				enc.Encode(&struct {
					Messages             []message.Message `json:"messages"`
					NotificationDisabled bool              `json:"notificationDisabled,omitempty"`
				}{
					Messages:             messages,
					NotificationDisabled: false,
				})

				/* ========== 發送訊息 ========== */

				request.RequestWorkerChan <- request.WorkerParams{
					Type:               "sendBroadcast",
					Deployment:         workerParams.Deployment,
					Method:             "POST",
					ReqestURL:          "https://api.line.me/v2/bot/message/broadcast",
					OAuth2Authorized:   true,
					ChannelAccessToken: &workerParams.ChannelAccessToken,
					HasBody:            true,
					Body:               buf, //需要調整
					Params: params.SendBroadcastRequestParams{
						Deployment: workerParams.Deployment,
						CallBack:   true,
						Broadcast:  workerParams.Broadcast,
						Messages:   &workerParams.Broadcast.Messages,
					},
				}
			case typeBroadcast.TargetTypeGroups, typeBroadcast.TargetTypeUsers:

				var (
					rows *sql.Rows
					user user.User
				)

				if workerParams.Broadcast.TargetType == broadcast.TargetTypeGroups {

					var groupsID []string
					if err = json.Unmarshal(*workerParams.Broadcast.GroupsID, &groupsID); err != nil {
						log.Println(err)
						return
					}

					rows, err = database.DB.Query("SELECT DISTINCT `users`.`id`, `users`.`display_name`, `users`.`picture_url`, `users`.`status_message`, `users`.`is_follow`, `users`.`user_id` FROM `deployment_" + strconv.Itoa(workerParams.Deployment) + "`.`groups` INNER JOIN `deployment_" + strconv.Itoa(workerParams.Deployment) + "`.`group_user` ON `group_user`.`group_id` = `groups`.`id` AND `group_user`.`deleted_at` IS NULL INNER JOIN `deployment_" + strconv.Itoa(workerParams.Deployment) + "`.`users` ON `users`.`id` = `group_user`.`user_id` WHERE `users`.`is_follow` = 1 AND `groups`.`id` IN (" + strings.Join(groupsID, ",") + ")")
					if err != nil {
						log.Println(err)
						continue
					}
					defer rows.Close()

				} else {

					var usersID []string
					if err = json.Unmarshal(*workerParams.Broadcast.UsersID, &usersID); err != nil {
						log.Println(err)
						return
					}

					rows, err = database.DB.Query("SELECT `id`, `display_name`, `picture_url`, `status_message`, `is_follow`, `user_id` FROM `deployment_" + strconv.Itoa(workerParams.Deployment) + "`.`users` where `id` IN (" + strings.Join(usersID, ",") + ") AND `is_follow` = 1")
					if err != nil {
						log.Println(err)
						continue
					}
					defer rows.Close()
				}

				/* ========== 發送訊息 ========== */

				for rows.Next() {
					err = rows.Scan(&user.ID, &user.DisplayName, &user.PictureURL, &user.StatusMessage, &user.IsFollow, &user.UserID)
					if err != nil {
						log.Println(err)
						continue
					}

					/* ========== 訊息 json 合成 ========== */

					var buf bytes.Buffer
					enc := json.NewEncoder(&buf)
					enc.Encode(&struct {
						To                   string            `json:"to"`
						Messages             []message.Message `json:"messages"`
						NotificationDisabled bool              `json:"notificationDisabled,omitempty"`
					}{
						To:                   user.UserID,
						Messages:             messages,
						NotificationDisabled: false,
					})

					/* ========== 發送訊息 ========== */

					request.RequestWorkerChan <- request.WorkerParams{
						Type:               "sendBroadcast",
						Deployment:         workerParams.Deployment,
						Method:             "POST",
						ReqestURL:          "https://api.line.me/v2/bot/message/push",
						OAuth2Authorized:   true,
						ChannelAccessToken: &workerParams.ChannelAccessToken,
						HasBody:            true,
						Body:               buf, //需要調整
						Params: params.SendBroadcastRequestParams{
							Deployment: workerParams.Deployment,
							CallBack:   true,
							Broadcast:  workerParams.Broadcast,
							User:       user,
							Messages:   &workerParams.Broadcast.Messages,
						},
					}

				}

			}

		}
	}()
}
