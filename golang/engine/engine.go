package engine

import (
	"webhook/engine/workers/request"
	"webhook/engine/workers/sendBroadcast"
	"webhook/engine/workers/updateOrCreateUser"
	"webhook/engine/workers/webhook"
)

// ConcurrendEngine 併發引擎
type ConcurrendEngine struct {
	RequestWorkersCount            int // 發送請求併發數量
	UpdateOrCreateUserWorkersCount int // 取得profile併發數量
	WebhookWorkersCount            int // 取得profile併發數量
	SendBroadcastWorkersCount      int // 推播訊息併發數量
}

// Run 執行任務
func (e *ConcurrendEngine) Run() {

	// 創建 goruntine
	for i := 0; i < e.RequestWorkersCount; i++ {
		request.Create()
	}

	for i := 0; i < e.UpdateOrCreateUserWorkersCount; i++ {
		updateOrCreateUser.Create()
	}

	for i := 0; i < e.WebhookWorkersCount; i++ {
		webhook.Create()
	}

	for i := 0; i < e.SendBroadcastWorkersCount; i++ {
		sendBroadcast.Create()
	}
}
