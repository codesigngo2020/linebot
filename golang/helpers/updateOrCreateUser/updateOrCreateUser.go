package updateOrCreateUser

import (
	"webhook/database"
	"webhook/engine/workers/request"
	"webhook/engine/workers/request/params"
	typeUser "webhook/types/user"
	"log"
	"strconv"
)

// HelperParams worker參數
type HelperParams struct {
	Asyn       bool
	Deployment int
	UserID     string
	User       *typeUser.User
}

// UpdateOrCreateUser 新增/更新使用者
func UpdateOrCreateUser(helperParams HelperParams) (user *typeUser.User, err error) {

	/* ========== 取得chatbot access token  ========== */

	var channelAccessToken string
	err = database.DB.QueryRow("select data->>'$.channelAccessToken' from `myproject`.`deployments` where `id` = ? AND `solution_id` = 4", helperParams.Deployment).Scan(&channelAccessToken)
	if err != nil {
		log.Println(err)
		return
	}

	/* ========== 取得user profile ========== */

	responseBody := request.Request(&request.WorkerParams{
		Type:               "updateOrCreateUser",
		Method:             "GET",
		Deployment:         helperParams.Deployment,
		ReqestURL:          "https://api.line.me/v2/bot/profile/" + helperParams.UserID,
		OAuth2Authorized:   true,
		ChannelAccessToken: &channelAccessToken,
		HasBody:            false,
		Params: params.UpdateOrCreateUserRequestParams{
			CallBack:   helperParams.Asyn,
			Deployment: helperParams.Deployment,
			UserID:     helperParams.UserID,
			User:       helperParams.User,
		},
	})

	/* ========== 新增好友並回傳 ========== */

	if !helperParams.Asyn {
		user = params.ParseResponseBodyAndUpdateOrCreateUser(helperParams.Deployment, helperParams.UserID, helperParams.User, responseBody)
	}
	return
}

// GetUserByUserID 取得好友
func GetUserByUserID(deployment int, userID string) (user typeUser.User, err error) {
	err = database.DB.QueryRow("SELECT `id`, `display_name`, `picture_url`, `status_message`, `node_id`, `user_id`, `is_follow` FROM `deployment_"+strconv.Itoa(deployment)+"`.`users` WHERE `user_id` = ? LIMIT 1", userID).Scan(&user.ID, &user.DisplayName, &user.PictureURL, &user.StatusMessage, &user.NodeID, &user.UserID, &user.IsFollow)
	return
}
