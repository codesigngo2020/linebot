package sendMessage

import (
	"webhook/database"
	"webhook/types/broadcast"
	"webhook/types/message"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"golang.org/x/oauth2"
)

// HelperParams helper參數
type HelperParams struct {
	Messages string
}

// Send 發送訊息
func Send() {

	/* ========== 取得 chatbot access token  ========== */

	var channelAccessToken string
	err := database.DB.QueryRow("select data->>'$.channelAccessToken' FROM myproject.deployments where id = 12 AND solution_id = 4").Scan(&channelAccessToken)
	if err != nil {
		log.Println(err)
		return
	}

	/* ========== 取得 broadcast ========== */

	var broadcast broadcast.Broadcast
	rows, err := database.DB.Query("SELECT id, name, messages FROM deployment_12.broadcasts where id = 49")
	if err != nil {
		log.Println(err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&broadcast.ID, &broadcast.Name, &broadcast.Messages)
		if err != nil {
			log.Println(err)
			return
		}

		messages, err := broadcast.UnmarshalMessagesToJSON()
		if err != nil {
			log.Println(err)
			return
		}

		/* ========== 發送訊息 ========== */

		tokenSource := oauth2.StaticTokenSource(
			&oauth2.Token{AccessToken: channelAccessToken},
		)
		client := oauth2.NewClient(oauth2.NoContext, tokenSource)

		var buf bytes.Buffer
		enc := json.NewEncoder(&buf)
		enc.Encode(&struct {
			To                   string            `json:"to"`
			Messages             []message.Message `json:"messages"`
			NotificationDisabled bool              `json:"notificationDisabled,omitempty"`
		}{
			To:                   "U1df6c5524cbb23263e09e81d06cef6cb",
			Messages:             messages,
			NotificationDisabled: false,
		})

		// 發送訊息
		reqest, err := http.NewRequest("POST", "https://api.line.me/v2/bot/message/push", &buf)
		if err != nil {
			log.Println(err)
			return
		}
		reqest.Header.Set("Content-Type", "application/json; charset=UTF-8")

		if err != nil {
			log.Println(err)
			return
		}

		res, err := client.Do(reqest)
		if err != nil {
			log.Println(err)
			return
		}

		defer res.Body.Close()

		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			log.Println(err)
			return
		}
		log.Println(string(body))

		if res.StatusCode != http.StatusOK {
			log.Println(res.StatusCode)
			return
		}

	}

}
